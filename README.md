Instrucciones de la l�nea de comando
Tambi�n puede subir archivos existentes desde su ordenador utilizando las instrucciones que se muestran a continuaci�n.


Configuraci�n global de Git
git config --global user.name "jose vasquez"
git config --global user.email "josevasquezc6@gmail.com"

Crear un nuevo repositorio
git clone https://gitlab.com/DesarrolloVzla/proyectosbase/crm/crm.git
cd crm
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Push a una carpeta existente
cd existing_folder
git init
git remote add origin https://gitlab.com/DesarrolloVzla/proyectosbase/crm/crm.git
git add .
git commit -m "Initial commit"
git push -u origin master

Push a un repositorio de Git existente
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/DesarrolloVzla/proyectosbase/crm/crm.git
git push -u origin --all
git push -u origin --tags