export class Linea {
  Etiquetas: string[] = [];
  DataSet: DataSetLinea[] = [];
}

export class DataSetLinea {

  Etiqueta: string = "";
  Color: string = "";
  Datos: string[] = [];

}

export class Barra{
  Etiquetas: string[] = [];
  DataSet: DataSetBarra[] = [];
}

export class DataSetBarra {

  Etiqueta: string = "";
  Color: string = "";
  Datos: string = "";

}

export class Pie{
  Etiquetas: string[] = [];
  DataSet: DataSetPie[] = [];
}

export class DataSetPie {

  Etiqueta: string = "";
  Color: string = "";
  Datos: string[] = [];

}
