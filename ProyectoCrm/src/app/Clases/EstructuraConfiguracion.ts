export class Configuracion{

    UrlWs : string = "";
    UrlPdf : string = "";
    UrlSocket: string = "";

}

export class FormularioConfiguracion {

    Titulo : string = "";
    ModalMensaje : ModalMensaje = new ModalMensaje;
    ModalConfirmacion : ModalConfirmacion = new ModalConfirmacion;
    
}

export class ModalMensaje{

    Titulo : string = "";

}

export class ModalConfirmacion{

    Titulo : string = "";
    Agregar : string = "";
    Edicion : string = "";
    Eliminacion : string = "";

}

export class ConfiguracionControl{
    
    Controles : ControlesConfiguracion[];

}

export class ControlesConfiguracion{

    "Id" : string = "";
    "Texto" : string = "";

}