
export class UsuarioEntrada {

    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codusuario: string = "";
    Descripcion: string = "";
    Clave: string = "";
    Cedula: string = "";
    Email: string = "";
    Url: string = "";
    Imagen64: string = "";
    Estatus: string = "";
    Conectado: number = 0;
    Token: string = "";
    Dispositivo: string = "";
    Tipo: string = "";
    Oculto: string = "";
    Cambiar_clave: string = "";
    Usuariocreador: string = "";
    Usuariomodificador: string = "";
    LUsuario_Empresas: UsuarioEmpresasEntrada[];

}

export class UsuarioEmpresasEntrada {

    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codusuario: string = "";
    Nativo: string = "";
    Usuariocreador: string = "";
    Usuariomodificador: string = "";
    LUsuario_Grupos: UsuarioGruposEntrada[];

}

export class UsuarioGruposEntrada {

    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codusuario: string = "";
    Codgrupo: string = "";
    Usuariocreador: string = "";
    Usuariomodificador: string = "";

}

export class GrupoEntradas {

    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codgrupo: string = "";
    Descripcion: string = "";
    Oculto: string = "";
    Usuariocreador: string = "";
    Usuariomodificador: string = "";
    LGrupoFormulario: GrupoFormulariosEntrada[];

}

export class GrupoFormulariosEntrada {

    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codgrupo: string = "";
    Codformulario: string = "";
    Agregar: number = 0;
    Editar: number = 0;
    Eliminar: number = 0;
    Imprimir: number = 0;
    Permiso: number = 0;
    Usuariocreador: string = "";
    Usuariomodificador: string = "";
    LTareas: GrupoFormulariosTareasEntrada[] = [];

}

export class GrupoFormulariosTareasEntrada{

    Id : number  = 0;
    Corporacion : string  = "";
    Empresa : string  = "";
    Codgrupo : string  = "";
    Codformulario : string  = "";
    Codtarea : string  = "";
    Asignado : number  = 0;
    Usuariocreador : string  = "";
    Usuariomodificador : string  = "";

}

export class Clasificacion1Entrada {

    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codclasificacion1: string = "";
    Descripcion: string = "";
    Url: string = "";
    Imagen64: string = "";
    Usuariocreador: string = "";
    Usuariomodificador: string = "";

}

export class Clasificacion2Entrada {

    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codclasificacion1: string = "";
    Codclasificacion2: string = "";
    Descripcion: string = "";
    Url: string = "";
    Imagen64: string = "";
    Usuariocreador: string = "";
    Usuariomodificador: string = "";

}

export class Clasificacion3Entrada {

    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codclasificacion1: string = "";
    Codclasificacion2: string = "";
    Codclasificacion3: string = "";
    Descripcion: string = "";
    Url: string = "";
    Imagen64: string = "";
    Usuariocreador: string = "";
    Usuariomodificador: string = "";

}

export class Clasificacion4Entrada {

    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codclasificacion1: string = "";
    Codclasificacion2: string = "";
    Codclasificacion3: string = "";
    Codclasificacion4: string = "";
    Descripcion: string = "";
    Url: string = "";
    Imagen64: string = "";
    Usuariocreador: string = "";
    Usuariomodificador: string = "";

}

export class ColorEntrada {

    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codcolor: string = "";
    Descripcion: string = "";
    Hexadecimal: string = "";
    Usuariocreador: string = "";
    Usuariomodificador: string = "";
    LHexadecimales: ColorHexadecimalesEntrada[] = [];

}

export class ColorHexadecimalesEntrada {

    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codcolor: string = "";
    Hexadecimal: string = "";
    Principal: number = 0;
    Nuevo: string = "";
    Usuariocreador: string = "";
    Usuariomodificador: string = "";

}

export class UnidadMedidaEntrada {

    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codunidadmedida: string = "";
    Descripcion: string = "";
    Simbolo: string = "";
    Factor_venta: number = 0;
    Usuariocreador: string = "";
    Usuariomodificador: string = "";

}

export class FabricanteEntrada {

    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codfabricante: string = "";
    Descripcion: string = "";
    Imagen64: string = "";
    Url: string = "";
    Usuariocreador: string = "";
    Usuariomodificador: string = "";

}

export class MarcaEntrada {

    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codfabricante: string = "";
    Codmarca: string = "";
    Descripcion: string = "";
    Usuariocreador: string = "";
    Usuariomodificador: string = "";

}

export class ModeloEntrada {

    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codfabricante: string = "";
    Codmarca: string = "";
    Codmodelo: string = "";
    Descripcion: string = "";
    Usuariocreador: string = "";
    Usuariomodificador: string = "";

}

export class CuponEntrada {

    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codcupon: string = "";
    Codcliente: string = "";
    Descripcion: string = "";
    Estatus: string = "";
    Tipo: string = "";
    Monto: number = 0;
    Fecha_vencimientol: number = 0;
    Usuariocreador: string = "";
    Usuariomodificador: string = "";

}

export class PromocionEntrada {

    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codpromocion: string = "";
    Titulo: string = "";
    Subtitulo: string = "";
    Url: string = "";
    Estatus: string = "";
    Fecha_vigencial: number = 0;
    Imagen64: string = "";
    Usuariocreador: string = "";
    Usuariomodificador: string = "";

}

export class ArticuloEntrada {

    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codarticulo: string = "";
    Referencia: string = "";
    Sku: string = "";
    Codalterno: string = "";
    Descripcion: string = "";
    Descripcion2: string = "";
    Tipo: string = "";
    Codfabricante: string = "";
    Codmarca: string = "";
    Codmodelo: string = "";
    Codclasificacion1: string = "";
    Codclasificacion2: string = "";
    Codclasificacion3: string = "";
    Codclasificacion4: string = "";
    Codcolor: string = "";
    Codunidadmedida: string = "";
    Url: string = "";
    Iva: number = 0;
    Costo: number = 0;
    Precio1: number = 0;
    Precio2: number = 0;
    Precio3: number = 0;
    Preciolista: number = 0;
    Peso: number = 0;
    Alto: number = 0;
    Ancho: number = 0;
    Largo: number = 0;
    Volumen: number = 0;
    Maneja_lotes: string = "N";
    Maneja_seriales: string = "N";
    Maneja_tallas: string = "N";
    Talla_inicial: string = "";
    Talla_final: string = "";
    Talla_intermedias: string = "N";
    Talla_sugerida: string = "";
    Maneja_vencimiento: string = "N";
    Dias_vencimiento_r: number = 0;
    Dias_vencimiento_d: number = 0;
    Usuariocreador: string = "";
    Usuariomodificador: string = "";
    LImagenes: ArticuloImagenEntrada[] = [];

}

export class ArticuloImagenEntrada {

    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codarticulo: string = "";
    Url: string = "";
    Principal: number = 0;
    Imagen64: string = "";
    Nuevo: string = "N";
    Tipo: string = "";
    Orden: number = 0;
    Usuariocreador: string = "";
    Usuariomodificador: string = "";

}

export class ClienteTelefonoEntrada {

    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codcliente: string = "";
    Telefono: string = "";
    Principal: number = 0;
    Nuevo: string = "N";
    Usuariocreador: string = "";
    Usuariomodificador: string = "";

}

export class ClienteEntrada {

    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codcliente: string = "";
    Descripcion: string = "";
    Tipocliente: string = "";
    Tipoprecio: string = "";
    Codocupacion: string = "";
    Codsituacion: string = "";
    Codcondicion: string = "";
    Codasesor: string = "";
    Codgenero: string = "";
    Codestadocivil: string = "";
    Email: string = "";
    Usuario: string = "";
    Clave: string = "";
    Codpais: string = "";
    Codestado: string = "";
    Codmunicipio: string = "";
    Codciudad: string = "";
    Direccion: string = "";
    Latitud: number = 0;
    Longitud: number = 0;
    Codpais_facturacion: string = "";
    Codestado_facturacion: string = "";
    Codmunicipio_facturacion: string = "";
    Codciudad_facturacion: string = "";
    Direccion_facturacion: string = "";
    Latitud_facturacion: number = 0;
    Longitud_facturacion: number = 0;
    Codpais_envio: string = "";
    Codestado_envio: string = "";
    Codmunicipio_envio: string = "";
    Codciudad_envio: string = "";
    Direccion_envio: string = "";
    Latitud_envio: number = 0;
    Longitud_envio: number = 0;
    Estatus: string = "";
    Fecha_nacimientol: number = 0;
    Url: string = "";
    Codgoogle: string = "";
    Urlgoogle: string = "";
    Codfacebook: string = "";
    Urlfacebook: string = "";
    Usuariocreador: string = "";
    Usuariomodificador: string = "";
    LTelefonos: ClienteTelefonoEntrada[] = [];
    LDocumentos: ClienteDocumentosEntrada[] = [];

}

export class MonedaEntrada {

    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codmoneda: string = "";
    Descripcion: string = "";
    Simbolo: string = "";
    Codigoiso: string = "";
    Principal: number = 0;
    Usuariocreador: string = "";
    Usuariomodificador: string = "";
    LDenominaciones: MonedaDenominacionEntrada[] = [];

}

export class MonedaDenominacionEntrada {

    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codmoneda: string = "";
    Coddenominacion: string = "";
    Descripcion: string = "";
    Factor: number = 0;
    Nuevo: string = "S";
    Usuariocreador: string = "";
    Usuariomodificador: string = "";
}

export class MonedaTasaEntrada {

    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codmonedaorigen: string = "";
    Codmonedadestino: string = "";
    Tasa: number = 0;
    Usuariocreador: string = "";
    Usuariomodificador: string = "";

}

export class PaisEntrada {
    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codpais: string = "";
    Descripcion: string = "";
    Abreviacion: string = "";
    Usuariocreador: string = "";
    Usuariomodificador: string = "";

}

export class EstadoEntrada {
    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codpais: string = "";
    Codestado: string = "";
    Descripcion: string = "";
    Abreviacion: string = "";
    Usuariocreador: string = "";
    Usuariomodificador: string = "";

}

export class PedidoEntrada {

    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codpedido: string = "";
    Codcliente: string = "";
    Tipo: string = "";
    Estatus: string = "";
    Usuariocreador: string = "";
    Usuariomodificador: string = "";
    LArticulos: PedidoArticuloEntrada[] = [];
    LCupones: PedidoCuponesEntrada[] = [];
    LColecciones: PedidoColeccionesEntrada[] = [];

}

export class PedidoArticuloEntrada {

    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codpedido: string = "";
    Codarticulo: string = "";
    Talla: string = "";
    Codalmacen: string = "";
    Cantidad: number = 0;
    Usuariocreador: string = "";
    Usuariomodificador: string = "";

}

export class PedidoCuponesEntrada {

    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codpedido: string = "";
    Codcupon: string = "";
    Estatus: string = "";
    Total: number = 0;
    Usuariocreador: string = "";
    Usuariomodificador: string = "";

}

export class PedidoColeccionesEntrada {

    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codpedido: string = "";
    Codcoleccion: string = "";
    Usuariocreador: string = "";
    Usuariomodificador: string = "";
    LArticulos: PedidoArticulosColeccionesEntrada[] = [];

}

export class PedidoArticulosColeccionesEntrada {

    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codpedido: string = "";
    Codcoleccion: string = "";
    Codarticulo: string = "";
    Talla: string = "";
    Usuariocreador: string = "";
    Usuariomodificador: string = "";

}

export class MunicipioEntrada {
    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codpais: string = "";
    Codestado: string = "";
    Codmunicipio: string = "";
    Descripcion: string = "";
    Abreviacion: string = "";
    Usuariocreador: string = "";
    Usuariomodificador: string = "";

}

export class CiudadEntrada {
    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codpais: string = "";
    Codestado: string = "";
    Codmunicipio: string = "";
    Codciudad: string = "";
    Descripcion: string = "";
    Abreviacion: string = "";
    Codigopostal: string = "";
    Usuariocreador: string = "";
    Usuariomodificador: string = "";
}

export class GeneroEntrada {
    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codgenero: string = "";
    Descripcion: string = "";
    Cliente: number = 0;
    Articulo: number = 0;
    Usuariocreador: string = "";
    Usuariomodificador: string = "";
}

export class EstadoCivilEntrada {
    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codestadocivil: string = "";
    Descripcion: string = "";
    Usuariocreador: string = "";
    Usuariomodificador: string = "";
}

export class Cliente_Telefonos {
    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codcliente: string = "";
    Telefono: string = "";
    Principal: number = 0;
    Usuariocreador: string = "";
    Usuariomodificador: string = "";
}

export class TipoAjustesInventarioEntrada {

    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codtipoajusteinv: string = "";
    Descripcion: string = "";
    Operacion: string = "";
    Usuariocreador: string = "";
    Usuariomodificador: string = "";
}

export class Forma_PagosEntrada {

    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codformapago: string = "";
    Descripcion: string = "";
    Crm: string = "N";
    Web: string = "N";
    App: string = "N";
    Duplicado: string = "N";
    Vuelto: string = "N";
    Referencia: string = "N";
    Tasa: string = "N";
    Aprobacion: string = "N";
    Usuariocreador: string = "";
    Usuariomodificador: string = "";
}

export class OcupacionEntrada {

    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codocupacion: string = "";
    Descripcion: string = "";
    Usuariocreador: string = "";
    Usuariomodificador: string = "";
}



export class CondicionEntrada {

    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codcondicion: string = "";
    Descripcion: string = "";
    Facturas_contado: string = "";
    Facturas_credito: string = "";
    Limite_credito: number = 0;
    Limite_facturas_credito: number = 0;
    Limite_facturas_vencidas: number = 0;
    Dias_credito: number = 0;
    Usuariocreador: string = "";
    Usuariomodificador: string = "";
}


export class SituacionEntrada {

    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codsituacion: string = "";
    Descripcion: string = "";
    Usuariocreador: string = "";
    Usuariomodificador: string = "";
}

export class VentaEntrada {

    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codventa: string = "";
    Codpedido: string = "";
    Tipo: string = "";
    Usuariocreador: string = "";
    Usuariomodificador: string = "";
    LFormasPagos: VentaFormasPagosEntrada[] = [];

}

export class VentaFormasPagosEntrada {

    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codventa: string = "";
    Codformapago: string = "";
    Item: number = 0;
    Monto: number = 0;
    Observacion: string = "";
    Usuariocreador: string = "";
    Usuariomodificador: string = "";
    FormaPagoReferencia: VentaFormasPagosReferenciaEntrada = new VentaFormasPagosReferenciaEntrada();
    FormaPagoTasa: VentaFormasPagosTasasEntrada = new VentaFormasPagosTasasEntrada();

}

export class VentaFormasPagosReferenciaEntrada {

    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codventa: string = "";
    Codformapago: string = "";
    Item: number = 0;
    Referencia: string = "";
    Usuariocreador: string = "";
    Usuariomodificador: string = "";

}

export class VentaFormasPagosTasasEntrada {

    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codventa: string = "";
    Codformapago: string = "";
    Item: number = 0;
    Codtasa: string = "";
    Codmonedaorigen: string = "";
    Codmonedadestino: string = "";
    Monto: number = 0;
    Usuariocreador: string = "";
    Usuariomodificador: string = "";

}

export class AjusteInventarioEntrada {

    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codajuste: string = "";
    Codtipoajusteinv: string = "";
    Codarticulo: string = "";
    Talla: string = "";
    Codalmacen: string = "";
    Cantidad: number = 0;
    Comentario: string = "";
    Usuariocreador: string = "";
    Usuariomodificador: string = "";

}

export class AlmacenEntrada {

    Id : number  = 0;
    Corporacion : string  = "";
    Empresa : string  = "";
    Codalmacen : string  = "";
    Descripcion : string  = "";
    Venta : number  = 0;
    Usuariocreador : string  = "";
    Usuariomodificador : string  = "";
}

export class ColeccionEntrada {

    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codcoleccion: string = "";
    Descripcion: string = "";
    Estatus: string = "";
    Descuento: number = 0;
    Tipodescuento: string = "";
    Fecha_iniciol: number = 0;
    Fecha_finl: number = 0;
    Usuariocreador: string = "";
    Usuariomodificador: string = "";
    LArticulos: ColeccionArticulosEntrada[] = [];

}

export class ColeccionArticulosEntrada {

    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codcoleccion: string = "";
    Codarticulo: string = "";
    Talla: string = "";
    Cantidad: number = 0;
    Usuariocreador: string = "";
    Usuariomodificador: string = "";

}

export class ConfiguracionEntrada {

    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Tabla: string = "";
    Columna: string = "";
    Descripcion: string = "";
    Valor: string = "";
    Tipo: string = "";
    Oculto: string = "";
    Editable: string = "";
    Expresion_regular: string = "";
    Usuariocreador: string = "";
    Usuariomodificador: string = "";

}

export class EstatusEntrada {

    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codestatus: string = "";
    Tabla: string = "";
    Descripcion: string = "";
    Secuencia: number = 0;
    Hexadecimal: string = "";
    Usuariocreador: string = "";
    Usuariomodificador: string = "";

}

export class TipoEntrada {

    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codtipo: string = "";
    Tabla: string = "";
    Columna: string = "";
    Descripcion: string = "";
    Abreviacion: string = "";
    Hexadecimal: string = "";
    Usuariocreador: string = "";
    Usuariomodificador: string = "";

}

export class ClienteDocumentosEntrada {

    Id: number = 0;
    Corporacion: string = "";
    Empresa: string = "";
    Codcliente: string = "";
    Documento: string = "";
    Tipodocumento: string = "";
    Principal: number = 0;
    Url: string = "";
    Imagen64: string = "";
    Nuevo: string = "";
    Usuariocreador: string = "";
    Usuariomodificador: string = "";

}

export class TicketEntrada {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codticket: string = "";
  Titulo: string = "";
  Codcliente: string = "";
  Tipo: string = "";
  Usuariocreador: string = "";
  Usuariomodificador: string = "";
  LUsuarios: TicketUsuarioEntradas[] = [];

}

export class TicketUsuarioEntradas{

  Id : number  = 0;
  Corporacion : string  = "";
  Empresa : string  = "";
  Codticket : string  = "";
  Codusuario : string  = "";
  Usuariocreador : string  = "";
  Usuariomodificador : string  = "";

}

export class MensajeEntrada {
  Corporacion: string = "";
  Empresa: string = "";
  Codticket: string = "";
  De: string = "";
  Texto: string = "";
}
