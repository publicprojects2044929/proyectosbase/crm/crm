
export class Respuesta {
  Mensaje: string = "";
  Resultado: string = "";
  Datos: any[
  ];
}

export class Reportes {

  Reporte: string = "";
  Formato: string = "";

}

export class Licencia {

  Fechal: number = 0;

}

export class Corporaciones {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Descripcion: string = "";
  Direccion: string = "";
  Telefono: string = "";
  Representante: string = "";
  Rif: string = "";
  Principal: number = 0;
  Longitud: number = 0;
  Latitud: number = 0;
  Prefijo: string = "";
  Sufijo: string = "";
  Dominio: string = "";
  Codmoneda: string = "";
  Simbolo: string = "";
  Codigoiso: string = "";
  Licencia: string = "";
  Fecha_licencia: Date = new Date();
  Fecha_licencial: number = 0;
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";
  LEmpresas: Empresas[] = [];
  LModulos: Formulario[] = [];
  LEsquemas: Esquema[] = [];

}

export class Empresas {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Descripcion: string = "";
  Direccion: string = "";
  Telefono: string = "";
  Representante: string = "";
  Rif: string = "";
  Principal: number = 0;
  Longitud: number = 0;
  Latitud: number = 0;
  Prefijo: string = "";
  Sufijo: string = "";
  Dominio: string = "";
  Codmoneda: string = "";
  Simbolo: string = "";
  Codigoiso: string = "";
  Licencia: string = "";
  Fecha_licencia: Date = new Date();
  Fecha_licencial: number = 0;
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";
  LEsquemas: Esquema[] = [];
  LModulos: Formulario[] = [];

}

export class Empresas_Usuario {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codusuario: string = "";
  Descripcion: string = "";
  Direccion: string = "";
  Telefono: string = "";
  Representante: string = "";
  Rif: string = "";
  Principal: number = 0;
  Longitud: number = 0;
  Latitud: number = 0;
  Prefijo: string = "";
  Sufijo: string = "";
  Dominio: string = "";
  Codmoneda: string = "";
  Simbolo: string = "";
  Codigoiso: string = "";
  Licencia: string = "";
  Fecha_licencia: Date = new Date();
  Fecha_licencial: number = 0;
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";
  LEsquemas: Esquema[] = [];
  LModulos: Formulario[] = [];

}

export class Esquema {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codesquema: string = "";
  Descripcion: string = "";
  Estatus: string = "";
  Tipo: string = "";
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";

}

export class Usuario {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Nativo: string = "";
  Codusuario: string = "";
  Descripcion: string = "";
  Clave: string = "";
  Cedula: string = "";
  Email: string = "";
  Estatus: string = "";
  Url: string = "";
  Conectado: number = 0;
  Token: string = "";
  TokenJwt: string = "";
  Dispositivo: string = "";
  Tipo: string = "";
  Cambiar_clave: string = "";
  Oculto: string = "";
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";
  LModulos: FormularioPadre[];
  LGrupos: GruposUsuario[];

}

export class FormularioPadre {


  Corporacion: string = "";
  Empresa: string = "";
  Codusuario: string = "";
  Codformulario: string = "";
  Descripcion: string = "";
  Descripcion2: string = "";
  Estilo: string = "";
  Url: string = "";
  Pertenece: string = "";
  LFormularios: Formulario[];

}

export class Formulario {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codformulario: string = "";
  Codesquema: string = "";
  Codformulariop: string = "";
  Descripcion: string = "";
  Descripcion2: string = "";
  Tipo: string = "";
  Estilo: string = "";
  Url: string = "";
  Agregar: number = 0;
  Editar: number = 0;
  Eliminar: number = 0;
  Imprimir: number = 0;
  Permiso: number = 0;
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";
  LTareas: FormularioTareas[] = [];

}

export class FormularioTareas {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codformulario: string = "";
  Codtarea: string = "";
  Descripcion: string = "";
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";

}

export class Tipo {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codtipo: string = "";
  Tabla: string = "";
  Columna: string = "";
  Descripcion: string = "";
  Abreviacion: string = "";
  Hexadecimal: string = "";
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";

}

export class Estatus {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codestatus: string = "";
  Tabla: string = "";
  Descripcion: string = "";
  Secuencia: number = 0;
  Hexadecimal: string = "";
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";

}

export class Filtros {

  Codfiltro: string = "";
  Filtro: string = "";

}

export class TiposOpcion {

  Codigo: string = "";
  Descripcion: string = "";

}

export class Grupos {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codgrupo: string = "";
  Descripcion: string = "";
  Oculto: string = "";
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";
  LFormularios: GrupoFormularios[] = [];

}

export class GrupoFormularios {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codgrupo: string = "";
  Codformulario: string = "";
  Codesquema: string = "";
  Codformulariop: string = "";
  Descripcion: string = "";
  Descripcion2: string = "";
  Tipo: string = "";
  Estilo: string = "";
  Url: string = "";
  Acceso: number = 0;
  Agregar: number = 0;
  Editar: number = 0;
  Eliminar: number = 0;
  Imprimir: number = 0;
  Permiso: number = 0;
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";
  LTareas: GrupoFormulariosTareas[] = [];

}

export class GrupoFormulariosTareas {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codgrupo: string = "";
  Codformulario: string = "";
  Codtarea: string = "";
  Descripcion: string = "";
  Asignado: number = 0;
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";

}

export class GruposUsuario {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codusuario: string = "";
  Codgrupo: string = "";
  Descripcion: string = "";
  Pertenece: number = 0;
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";

}

export class Clasificacion1 {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codclasificacion1: string = "";
  Descripcion: string = "";
  Url: string = "";
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";
  LClasificacion2: Clasificacion2[] = [];

}

export class Clasificacion2 {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codclasificacion1: string = "";
  Codclasificacion2: string = "";
  Descripcion: string = "";
  Url: string = "";
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";
  LClasificacion3: Clasificacion3[] = [];

}

export class Clasificacion3 {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codclasificacion1: string = "";
  Codclasificacion2: string = "";
  Codclasificacion3: string = "";
  Descripcion: string = "";
  Url: string = "";
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";
  LClasificacion4: Clasificacion4[] = [];

}

export class Clasificacion4 {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codclasificacion1: string = "";
  Codclasificacion2: string = "";
  Codclasificacion3: string = "";
  Codclasificacion4: string = "";
  Descripcion: string = "";
  Url: string = "";
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";
}

export class Color {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codcolor: string = "";
  Descripcion: string = "";
  Hexadecimal: string = "#FFFFFF";
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";
  LHexadecimales: ColorHexadecimales[] = [];

}

export class ColorHexadecimales {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codcolor: string = "";
  Hexadecimal: string = "";
  Principal: number = 0;
  Nuevo: string = "S";
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";

}

export class UnidadMedida {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codunidadmedida: string = "";
  Descripcion: string = "";
  Simbolo: string = "";
  Factor_venta: number = 0;
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";

}

export class Fabricante {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codfabricante: string = "";
  Descripcion: string = "";
  Url: string = "";
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";
  LMarcas: Marca[] = [];

}

export class Marca {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codfabricante: string = "";
  Codmarca: string = "";
  Descripcion: string = "";
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";
  LModelos: Modelo[] = [];

}

export class Modelo {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codfabricante: string = "";
  Codmarca: string = "";
  Codmodelo: string = "";
  Descripcion: string = "";
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";

}

export class Cupon {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codcupon: string = "";
  Descripcion: string = "";
  Estatus: string = "";
  Tipo: string = "";
  Monto: number = 0;
  Fecha_vencimiento: Date = new Date();
  Fecha_vencimientol: number = 0;
  Codcliente: string = "";
  Descripcion_cliente: string = "";
  Documento: string = "";
  Tipo_documento: string = "";
  Telefono: string = "";
  Email: string = "";
  Usuario: string = "";
  Clave: string = "";
  Estatus_cliente: string = "";
  Fecha_nacimiento: Date = new Date();
  Fecha_nacimientol: number = 0;
  Url: string = "";
  Codgoogle: string = "";
  Urlgoogle: string = "";
  Codfacebook: string = "";
  Urlfacebook: string = "";
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";

}

export class Cliente {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codcliente: string = "";
  Descripcion: string = "";
  Documento: string = "";
  Tipodocumento: string = "";
  Tipocliente: string = "";
  Tipoprecio: string = "";
  Codocupacion: string = "";
  Descripcion_ocupacion: string = "";
  Codsituacion: string = "";
  Descripcion_situacion: string = "";
  Codcondicion: string = "";
  Descripcion_condicion: string = "";
  Codasesor: string = "";
  Descripcion_asesor: string = "";
  Codgenero: string = "";
  Descripcion_genero: string = "";
  Codestadocivil: string = "";
  Descripcion_estado_civil: string = "";
  Email: string = "";
  Telefono: string = "";
  Usuario: string = "";
  Clave: string = "";
  Codpais: string = "";
  Codestado: string = "";
  Codmunicipio: string = "";
  Codciudad: string = "";
  Direccion: string = "";
  Latitud: number = 0;
  Longitud: number = 0;
  Facturas_contado: string = "N";
  Facturas_credito: string = "N";
  Limite_credito: number = 0;
  Credito_usado: number = 0;
  Limite_facturas_credito: number = 0;
  Facturas_credito_activas: number = 0;
  Limite_facturas_vencidas: number = 0;
  Facturas_credito_vencidas: number = 0;
  Dias_credito: number = 0;
  Codpais_facturacion: string = "";
  Codestado_facturacion: string = "";
  Codmunicipio_facturacion: string = "";
  Codciudad_facturacion: string = "";
  Direccion_facturacion: string = "";
  Latitud_facturacion: number = 0;
  Longitud_facturacion: number = 0;
  Codpais_envio: string = "";
  Codestado_envio: string = "";
  Codmunicipio_envio: string = "";
  Codciudad_envio: string = "";
  Direccion_envio: string = "";
  Latitud_envio: number = 0;
  Longitud_envio: number = 0;
  Estatus: string = "";
  Fecha_nacimiento: Date = new Date();
  Fecha_nacimientol: number = 0;
  Url: string = "";
  Codgoogle: string = "";
  Urlgoogle: string = "";
  Codfacebook: string = "";
  Urlfacebook: string = "";
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";
  LTelefonos: ClienteTelefonos[] = [];
  LDocumentos: ClienteDocumentos[] = [];

}

export class Promocion {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codpromocion: string = "";
  Titulo: string = "";
  Subtitulo: string = "";
  Url: string = "";
  Estatus: string = "";
  Fecha_vigencia: Date = new Date();
  Fecha_vigencial: number = 0;
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";

}

export class Articulo {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codarticulo: string = "";
  Sku: string = "";
  Codalterno: string = "";
  Referencia: string = "";
  Descripcion: string = "";
  Descripcion2: string = "";
  Tipo: string = "";
  Codalmacen: string = "";
  Desc_almacen: string = "";
  Codfabricante: string = "";
  Desc_fabricante: string = "";
  Codmarca: string = "";
  Desc_marca: string = "";
  Codmodelo: string = "";
  Desc_modelo: string = "";
  Codclasificacion1: string = "";
  Desc_clasificacion1: string = "";
  Codclasificacion2: string = "";
  Desc_clasificacion2: string = "";
  Codclasificacion3: string = "";
  Desc_clasificacion3: string = "";
  Codclasificacion4: string = "";
  Desc_clasificacion4: string = "";
  Codcolor: string = "";
  Desc_color: string = "";
  Codunidadmedida: string = "";
  Desc_unidad_medida: string = "";
  Factor_venta: number = 0;
  Url: string = "";
  Iva: number = 0;
  Costo: number = 0;
  Precio1: number = 0;
  Precio2: number = 0;
  Precio3: number = 0;
  Peso: number = 0;
  Alto: number = 0;
  Ancho: number = 0;
  Largo: number = 0;
  Volumen: number = 0;
  Maneja_lotes: string = "N";
  Maneja_seriales: string = "N";
  Maneja_tallas: string = "N";
  Talla_inicial: string = "";
  Talla_final: string = "";
  Talla_intermedias: string = "N";
  Talla_sugerida: string = "";
  Maneja_vencimiento: string = "N";
  Dias_vencimiento_r: number = 0;
  Dias_vencimiento_d: number = 0;
  Preciolista: number = 0;
  Existencia_general: number = 0;
  Reservado_general: number = 0;
  Disponible_general: number = 0;
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";
  LImagenes: ArticuloImagenes[] = [];
  LInventario: ArticuloInventario[] = [];

}

export class ArticuloImagenes {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codarticulo: string = "";
  Url: string = "";
  Principal: number = 0;
  Nuevo: string = "N";
  Tipo: string = "";
  Orden: number = 0;
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";

}

export class ArticuloInventario {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codarticulo: string = "";
  Descripcion: string = "";
  Talla: string = "";
  Codalmacen: string = "";
  Existencia: number = 0;
  Reservado: number = 0;
  Disponible: number = 0;
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";

}

export class Configuracion {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Tabla: string = "";
  Columna: string = "";
  Descripcion: string = "";
  Valor: string = "";
  Tipo: string = "";
  Oculto: string = "";
  Editable: string = "";
  Expresion_regular: string = "";
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";

}

export class Pais {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codpais: string = "";
  Descripcion: string = "";
  Abreviacion: string = "";
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";
  LEstados: Estado[];
}

export class Estado {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codpais: string = "";
  Codestado: string = "";
  Descripcion: string = "";
  Abreviacion: string = "";
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";
  LMunicipio: Municipio[];
}

export class Municipio {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codpais: string = "";
  Codestado: string = "";
  Codmunicipio: string = "";
  Descripcion: string = "";
  Abreviacion: string = "";
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";
  LCiudad: Ciudad[];

}

export class Ciudad {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codpais: string = "";
  Descripcion_pais: string = "";
  Abreviacion_pais: string = "";
  Codestado: string = "";
  Descripcion_estado: string = "";
  Abreviacion_estado: string = "";
  Codmunicipio: string = "";
  Descripcion_municipio: string = "";
  Abreviacion_municipio: string = "";
  Codciudad: string = "";
  Descripcion: string = "";
  Abreviacion: string = "";
  Codigopostal: string = "";
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";

}

export class Moneda {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codmoneda: string = "";
  Descripcion: string = "";
  Simbolo: string = "";
  Codigoiso: string = "";
  Principal: number = 0;
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";
  LDenominaciones: MonedaDenominacion[] = [];
  LTasas: MonedaTasas[] = [];

}

export class MonedaDenominacion {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codmoneda: string = "";
  Descripcion_moneda: string = "";
  Simbolo: string = "";
  Coddenominacion: string = "";
  Descripcion: string = "";
  Factor: number = 0;
  Nuevo: string = "S";
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";

}

export class MonedaTasas {
  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codtasa: string = "";
  Codmonedaorigen: string = "";
  Descripcion_origen: string = "";
  Codmonedadestino: string = "";
  Descripcion_destino: string = "";
  Fecha_vigencia: Date = new Date();
  Fecha_vigencial: number = 0;
  Estatus: string = "";
  Tasa: number = 0;
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";
}

export class Pedido {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codpedido: string = "";
  Tipo: string = "";
  Estatus: string = "";
  Total_lineas: number = 0;
  Total_cantidad: number = 0;
  Total_articulos: number = 0;
  Total_descuento: number = 0;
  Total: number = 0;
  Codcliente: string = "";
  Descripcion_cliente: string = "";
  Documento: string = "";
  Tipo_documento: string = "";
  Telefono: string = "";
  Email: string = "";
  Usuario: string = "";
  Clave: string = "";
  Estatus_cliente: string = "";
  Fecha_nacimiento: Date = new Date();
  Fecha_nacimientol: number = 0;
  Url: string = "";
  Codgoogle: string = "";
  Urlgoogle: string = "";
  Codfacebook: string = "";
  Urlfacebook: string = "";
  Codasesor: string = "";
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";
  LArticulos: PedidoArticulo[] = [];
  LCupones: PedidoCupones[] = [];
  LColecciones: PedidoColecciones[] = [];
  Venta: Venta = new Venta();

}

export class PedidoArticulo {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codpedido: string = "";
  Linea: number = 0;
  Codarticulo: string = "";
  Talla: string = "";
  Codalmacen: string = "";
  Sku: string = "";
  Descripcion: string = "";
  Descripcion2: string = "";
  Tipo: string = "";
  Codfabricante: string = "";
  Desc_fabricante: string = "";
  Codmarca: string = "";
  Desc_marca: string = "";
  Codmodelo: string = "";
  Desc_modelo: string = "";
  Codclasificacion1: string = "";
  Desc_clasificacion1: string = "";
  Codclasificacion2: string = "";
  Desc_clasificacion2: string = "";
  Codclasificacion3: string = "";
  Desc_clasificacion3: string = "";
  Codclasificacion4: string = "";
  Desc_clasificacion4: string = "";
  Codcolor: string = "";
  Desc_color: string = "";
  Codunidadmedida: string = "";
  Desc_unidad_medida: string = "";
  Factor_venta: number = 0;
  Url: string = "";
  Cantidad: number = 0;
  Cantidad_coleccion: number = 0;
  Precio: number = 0;
  Iva: number = 0;
  Total: number = 0;
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";

}

export class PedidoCupones {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codpedido: string = "";
  Codcupon: string = "";
  Estatus: string = "";
  Total: number = 0;
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";

}

export class PedidoColecciones {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codpedido: string = "";
  Codcoleccion: string = "";
  Descripcion: string = "";
  Descuento: number = 0;
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";
  LArticulos: PedidoArticulosColecciones[] = [];

}

export class PedidoArticulosColecciones {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codpedido: string = "";
  Codcoleccion: string = "";
  Codarticulo: string = "";
  Talla: string = "";
  Cantidad: number = 0;
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";

}

export class Genero {
  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codgenero: string = "";
  Descripcion: string = "";
  Cliente: number = 0;
  Articulo: number = 0;
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";
}

export class EstadoCivil {
  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codestadocivil: string = "";
  Descripcion: string = "";
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";

}

export class ClienteTelefonos {
  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codcliente: string = "";
  Telefono: string = "";
  Principal: number = 0;
  Nuevo: string = "";
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";

}

export class TipoAjustesInventario {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codtipoajusteinv: string = "";
  Descripcion: string = "";
  Operacion: string = "";
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";
}

export class FormaDePago {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codformapago: string = "";
  Descripcion: string = "";
  Crm: string = "";
  Web: string = "";
  App: string = "";
  Duplicado: string = "";
  Vuelto: string = "";
  Referencia: string = "";
  Tasa: string = "";
  Aprobacion: string = "";
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";

}


export class Ocupacion {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codocupacion: string = "";
  Descripcion: string = "";
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";

}

export class Situacion {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codsituacion: string = "";
  Descripcion: string = "";
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";

}

export class Condicion {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codcondicion: string = "";
  Descripcion: string = "";
  Facturas_contado: string = "N";
  Facturas_credito: string = "N";
  Limite_credito: number = 0;
  Limite_facturas_credito: number = 0;
  Limite_facturas_vencidas: number = 0;
  Dias_credito: number = 0;
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";

}

export class Venta {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codventa: string = "";
  Codpedido: string = "";
  Codcliente: string = "";
  Descripcion: string = "";
  Documento: string = "";
  Tipo: string = "";
  Estatus: string = "";
  Situacion: string = "";
  Total: number = 0;
  Pagado: number = 0;
  Vuelto: number = 0;
  Dias_vencimiento: number = 0;
  Fecha_vencimiento: Date = new Date();
  Fecha_vencimientol: number = 0;
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";
  LFormasPagos: VentaFormasPagos[] = [];

}

export class VentaFormasPagos {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codventa: string = "";
  Codformapago: string = "";
  Descripcion: string = "";
  Item: number = 0;
  Monto: number = 0;
  Estatus: string = "";
  Observacion: string = "";
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";
  FormaPagoReferencia: VentaFormasPagosReferencia = new VentaFormasPagosReferencia();
  FormaPagoTasa: VentaFormasPagosTasas = new VentaFormasPagosTasas();

}

export class VentaFormasPagosReferencia {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codventa: string = "";
  Codformapago: string = "";
  Item: number = 0;
  Referencia: string = "";
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";

}

export class VentaFormasPagosTasas {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codventa: string = "";
  Codformapago: string = "";
  Item: number = 0;
  Codtasa: string = "";
  Codmonedaorigen: string = "";
  Codmonedadestino: string = "";
  Monto: number = 0;
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";

}

export class AjusteInventario {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codajuste: string = "";
  Codtipoajusteinv: string = "";
  Operacion: string = "";
  Descripcion: string = "";
  Codarticulo: string = "";
  Talla: string = "";
  Codalmacen: string = "";
  Cantidad: number = 0;
  Existencia_actual: number = 0;
  Existencia_final: number = 0;
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";

}

export class Almacen {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codalmacen: string = "";
  Descripcion: string = "";
  Venta: number = 0;
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";
}

export class Coleccion {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codcoleccion: string = "";
  Descripcion: string = "";
  Estatus: string = "";
  Descuento: number = 0;
  Tipodescuento: string = "";
  Fecha_inicio: Date = new Date();
  Fecha_iniciol: number = 0;
  Fecha_fin: Date = new Date();
  Fecha_finl: number = 0;
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";
  LArticulos: ColeccionArticulos[] = [];

}

export class ColeccionArticulos {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codcoleccion: string = "";
  Codarticulo: string = "";
  Descripcion: string = "";
  Talla: string = "";
  Codcolor: string = "";
  Desc_color: string = "";
  Cantidad: number = 0;
  Existencia: number = 0;
  Reservado: number = 0;
  Disponible: number = 0;
  Url: string = "";
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";

}

export class Tabla {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Tabla: string = "";
  Descripcion: string = "";
  Descripcion2: string = "";
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";
  LTipos: Tipo[] = [];
  LEstatus: Estatus[] = [];
  LConfiguracion: Configuracion[] = [];

}

export class ClienteDocumentos {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codcliente: string = "";
  Documento: string = "";
  Tipodocumento: string = "";
  Principal: number = 0;
  Url: string = "";
  Nuevo: string = "";
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";

}

export class Ticket {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codticket: string = "";
  Titulo: string = "";
  Codcliente: string = "";
  Nombre_Cliente: string = "";
  Usuario_Cliente: string = "";
  Email: string = "";
  Estatus: string = "";
  Estatus_Hexadecimal: string = "";
  Tipo: string = "";
  Tipo_Hexadecimal: string = "";
  TotalMensajes: number = 0;
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";
  LUsuarios: TicketUsuarios[] = [];
  LMensajes: TicketMensajes[] = [];

}

export class TicketUsuarios {

  Id: number = 0;
  Corporacion: string = "";
  Empresa: string = "";
  Codticket: string = "";
  Codusuario: string = "";
  Fecha_creacion: Date = new Date();
  Fecha_creacionl: number = 0;
  Fecha_modificacion: Date = new Date();
  Fecha_modificacionl: number = 0;
  Usuariocreador: string = "";
  Usuariomodificador: string = "";

}

export class TicketMensajes {

  _id: string = "";
  Corporacion: string = "";
  Empresa: string = "";
  Codticket: string = "";
  De: string = "";
  Texto: string = "";
  Fecha: Date = new Date();
  Fechal: number = 0;

}
