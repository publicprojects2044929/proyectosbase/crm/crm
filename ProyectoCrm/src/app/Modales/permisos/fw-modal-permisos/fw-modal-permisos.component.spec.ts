import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwModalPermisosComponent } from './fw-modal-permisos.component';

describe('FwModalPermisosComponent', () => {
  let component: FwModalPermisosComponent;
  let fixture: ComponentFixture<FwModalPermisosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwModalPermisosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwModalPermisosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
