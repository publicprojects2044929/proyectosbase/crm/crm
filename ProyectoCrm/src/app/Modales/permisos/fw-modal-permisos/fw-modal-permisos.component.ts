import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Grupos, Filtros, GruposUsuario, Usuario, Empresas, Formulario, GrupoFormularios } from 'src/app/Clases/Estructura';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { ErroresService } from '../../mensajes/fw-modal-msj/errores.service';
import { MensajesService } from '../../mensajes/fw-modal-msj/mensajes.service';
import { environment } from 'src/environments/environment';
import { SessionService } from 'src/app/Core/Session/session.service';

@Component({
  selector: 'app-fw-modal-permisos',
  templateUrl: './fw-modal-permisos.component.html',
  styleUrls: ['./fw-modal-permisos.component.scss']
})
export class FwModalPermisosComponent implements OnInit, OnDestroy {

  @Input() tTituloModal: string = "";
  @Input() tMensajeModal: string = "";
  @Input() tGrupoModal: GruposUsuario = new GruposUsuario();

  tPosicion: number = 0;
  tPagina: number = 1;
  tTamanoPag: number = 5;

  tGrupoActual: Grupos = new Grupos();
  tFormulariosActual: Formulario[] = [];

  tFormularioActual: GrupoFormularios = new GrupoFormularios();

  tPaginaFormulario: number = 1;
  tTamanoPagFormulario: number = 8;

  tPaginaFormularioTareas: number = 1;
  tTamanoPagFormularioTareas: number = 4;

  tFiltros: Filtros[] = [];
  tFiltroActual: Filtros = new Filtros();
  tFiltroBusqueda: string = "";

  tHayConfiguracionWs$: Subscription;
  tUsuarioActual: Usuario = new Usuario();
  tEmpresaActual: Empresas = new Empresas();
  tEmpresa$: Subscription;
  tUsuario$: Subscription;

  constructor(public activeModal: NgbActiveModal,
    private tSession: SessionService,
    public Ws: ClsServiciosService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService) {

    this.tFiltroBusqueda = "";

    this.tFiltros = [

      { Codfiltro: "Descripcion", Filtro: "Descripción" },
      { Codfiltro: "Descripcion2", Filtro: "Descripción2" }

    ]

  }

  ngOnInit() {

    this.tEmpresa$ = this.tSession.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    })

    this.tUsuario$ = this.tSession.tUsuarioActual$.subscribe((tUsuario: Usuario) => {

      this.tUsuarioActual = tUsuario;

    })

    this.tFiltroActual = this.tFiltros[0];

    this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
      .subscribe((tHayConfig: boolean) => {

        if (tHayConfig === true) {

          this.BuscarGrupos();

        }

      });

  }

  ngOnDestroy() {

    this.tHayConfiguracionWs$.unsubscribe();
    this.tEmpresa$.unsubscribe();
    this.tUsuario$.unsubscribe();

  }

  BuscarGrupos(): void {

    this.tGrupoActual = new Grupos();

    this.Ws.Grupo()
      .Consumir_Obtener_Grupo_Full(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        this.tGrupoModal.Codgrupo)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tGrupoActual = Respuesta.Datos[0] || new Grupos();
          this.tFormulariosActual = this.tGrupoActual.LFormularios.filter(tFormulario => tFormulario.Acceso === 1) || [];
          this.tFiltroActual = this.tFiltros[0];

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  SeleccionarFormulario(tFormularioSel: GrupoFormularios) {

    this.tFormularioActual = tFormularioSel;

  }
}
