import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FwModalPermisosComponent } from '../fw-modal-permisos/fw-modal-permisos.component';
import { Grupos, GruposUsuario } from 'src/app/Clases/Estructura';

@Injectable({
  providedIn: 'root'
})

export class PermisosService {

  constructor(private modalService: NgbModal) { }

  AbrirModal(tGrupoModal: GruposUsuario): Promise<any> {

    return new Promise((resolve, reject) => {

      const modalRef = this.modalService.open(FwModalPermisosComponent, { size: 'xl' });
      modalRef.componentInstance.tTituloModal = "Modal Permisos";
      modalRef.componentInstance.tMensajeModal = "";
      modalRef.componentInstance.tGrupoModal = tGrupoModal;

      modalRef.result.then(result => {

        resolve(result);

      }, reason => {

        reject(reason);

      });

    })

  }

}
