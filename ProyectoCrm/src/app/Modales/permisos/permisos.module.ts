import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule, NgbActiveModal, NgbAlertConfig } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from 'src/app/Shared/shared.module';
import { FwModalPermisosComponent } from './fw-modal-permisos/fw-modal-permisos.component'

@NgModule({
  declarations: [
    FwModalPermisosComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    SharedModule
  ],
  exports: [
    FwModalPermisosComponent
  ],
  entryComponents: [
    FwModalPermisosComponent
  ],
  providers: [ NgbActiveModal, NgbAlertConfig ]
})
export class PermisosModalModule { }
