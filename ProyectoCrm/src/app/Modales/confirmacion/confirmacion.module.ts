import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgbModule, NgbActiveModal, NgbAlertConfig } from '@ng-bootstrap/ng-bootstrap';

//Modales
import { FwModalConfirmacionComponent } from 'src/app/Modales/confirmacion/fw-modal-confirmacion/fw-modal-confirmacion.component'

@NgModule({
  declarations: [
    FwModalConfirmacionComponent
  ],
  imports: [
    CommonModule,
    NgbModule
  ],
  exports: [
    FwModalConfirmacionComponent
  ],
  entryComponents: [
    FwModalConfirmacionComponent
  ],
  providers: [ NgbActiveModal, NgbAlertConfig ]
})
export class ConfirmacionModule { }
