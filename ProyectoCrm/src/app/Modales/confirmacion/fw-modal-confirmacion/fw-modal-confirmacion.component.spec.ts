import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwModalConfirmacionComponent } from './fw-modal-confirmacion.component';

describe('FwModalConfirmacionComponent', () => {
  let component: FwModalConfirmacionComponent;
  let fixture: ComponentFixture<FwModalConfirmacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwModalConfirmacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwModalConfirmacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
