import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FwModalConfirmacionComponent } from './fw-modal-confirmacion.component'

@Injectable({
  providedIn: 'root'
})

export class ConfirmacionService {

  public tTituloConf: string = "";

  constructor(private modalService: NgbModal) { }

  AbrirModalConf(tMensajeConf : string): Promise<string> {

    return new Promise((resolve, reject) => {

      const modalRef = this.modalService.open(FwModalConfirmacionComponent);
      modalRef.componentInstance.tTituloModal = this.tTituloConf;
      modalRef.componentInstance.tMensajeModal = tMensajeConf;
  
      modalRef.result.then(result => {

        resolve(result);
  
      }, reason => {
  
        reject(reason);
  
      });

    })

  }

}
