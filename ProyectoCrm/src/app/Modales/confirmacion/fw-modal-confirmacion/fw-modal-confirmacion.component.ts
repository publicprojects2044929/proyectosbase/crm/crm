import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  
  selector: 'app-fw-modal-confirmacion',
  templateUrl: './fw-modal-confirmacion.component.html',
  styleUrls: ['./fw-modal-confirmacion.component.scss']

})

export class FwModalConfirmacionComponent implements OnInit {

  @Input() tTituloModal: string;
  @Input() tMensajeModal: string;

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

}
