import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/Shared/shared.module'
import { NgbModule, NgbActiveModal, NgbAlertConfig } from '@ng-bootstrap/ng-bootstrap';

import { FwModalCargarArticulosComponent } from 'src/app/Modales/cargar-articulos/fw-modal-cargar-articulos/fw-modal-cargar-articulos.component'

@NgModule({
  declarations: [
    FwModalCargarArticulosComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    SharedModule
  ],
  exports: [
    FwModalCargarArticulosComponent
  ],
  entryComponents: [
    FwModalCargarArticulosComponent
  ],
  providers: [ NgbActiveModal, NgbAlertConfig ]
})
export class CargarArticuloModalModule { }
