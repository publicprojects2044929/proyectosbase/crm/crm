import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwModalCargarArticulosComponent } from './fw-modal-cargar-articulos.component';

describe('FwModalCargarArticulosComponent', () => {
  let component: FwModalCargarArticulosComponent;
  let fixture: ComponentFixture<FwModalCargarArticulosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwModalCargarArticulosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwModalCargarArticulosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
