import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Usuario, Empresas, Filtros } from 'src/app/Clases/Estructura';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service'
import { ArticuloEntrada } from 'src/app/Clases/EstructuraEntrada';
import { ToastrService } from 'ngx-toastr';
import { ManejarArchivo } from 'src/app/Manejadores/cls-procedimientos';
import { MensajesService } from 'src/app/Modales/mensajes/fw-modal-msj/mensajes.service';
import { ErroresService } from 'src/app/Modales/mensajes/fw-modal-msj/errores.service';
import { ConfirmacionService } from 'src/app/Modales/confirmacion/fw-modal-confirmacion/confirmacion.service';
import { isNumber } from 'util';
import { SessionService } from 'src/app/Core/Session/session.service';
import { environment } from 'src/environments/environment';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-fw-modal-cargar-articulos',
  templateUrl: './fw-modal-cargar-articulos.component.html',
  styleUrls: ['./fw-modal-cargar-articulos.component.scss']
})

export class FwModalCargarArticulosComponent implements OnInit, OnDestroy {

  @Input() tTituloModal: string;
  @Input() tMensajeModal: string;
  tPosicion: number = 0;
  tPagina: number = 1;
  tTamanoPag: number = 5;

  tArchivoCargado: boolean = false;

  tUsuarioActual: Usuario = new Usuario();
  tEmpresaActual: Empresas = new Empresas();
  tEmpresa$: Subscription;
  tHayConfiguracionWs$: Subscription;

  tFiltros: Filtros[] = [];
  tFiltroActual: Filtros = new Filtros;
  tFiltroBusqueda: string = "";

  tArticulosCargados: ArticuloEntrada[] = [];

  tExcel: ManejarArchivo = new ManejarArchivo();

  constructor(public activeModal: NgbActiveModal,
    public Ws: ClsServiciosService,
    private toastr: ToastrService,
    private tSesion: SessionService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService,
    private tConfServices: ConfirmacionService) {

    this.tArchivoCargado = false;

    this.tFiltros = [

      { Codfiltro: "Codarticulo", Filtro: "Artículo" },
      { Codfiltro: "Descripcion", Filtro: "Nombre" },

    ]

  }

  ngOnInit() {


    this.tEmpresa$ = this.tSesion.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    })

    this.tFiltroBusqueda = "";

    this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
      .subscribe((tHayConfig: boolean) => {

        if (tHayConfig === true) {

          this.tFiltroActual = this.tFiltros[0];

        }

      });

  }

  ngOnDestroy(): void {

    this.tEmpresa$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();

  }

  Nuevo_Articulo(): void {

    for (let tArticulo of this.tArticulosCargados) {

      tArticulo.Corporacion = this.tEmpresaActual.Corporacion;
      tArticulo.Empresa = this.tEmpresaActual.Empresa;
      tArticulo.Usuariocreador = this.tUsuarioActual.Codusuario;
      tArticulo.Usuariomodificador = this.tUsuarioActual.Codusuario;

    }

    this.Ws
      .Articulo()
      .Consumir_Crear_Articulo2(this.tArticulosCargados)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tMsjServices.AbrirModalMsj("Artículos cargados correctamente.");
          this.activeModal.close('S');

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  CargarExcel(tArchivo: any): void {

    this.tArticulosCargados = [];
    let tArticulosTemp: any = [];
    this.tExcel.Cargar_Archivo(tArchivo);

    this.tExcel
      .ObtenerDatos()
      .subscribe(Datos => {

        if (Datos["Articulos"] !== undefined) {

          let tHoja: any = Datos.Articulos;
          tHoja.map((tLinea: ArticuloEntrada, tIndex: number) => {

            let tSku: string = tLinea["Sku"] || tLinea["SKU"];
            let tReferencia: string = tLinea["Referencia"] || tLinea["REFERENCIA"];
            let tCodalterno: string = tLinea["Codalterno"] || tLinea["CODALTERNO"];
            let tDescripcion: string = tLinea["Descripcion"] || tLinea["DESCRIPCION"];
            let tDescripcion2: string = tLinea["Descripcion2"] || tLinea["DESCRIPCION2"];
            let tCodfabricante: string = tLinea["Codfabricante"] || tLinea["CODFABRICANTE"];
            let tCodmarca: string = tLinea["Codmarca"] || tLinea["CODMARCA"];
            let tCodmodelo: string = tLinea["Codmodelo"] || tLinea["CODMODELO"];
            let tCodclasificacion1: string = tLinea["Codclasificacion1"] || tLinea["CODCLASIFICACION1"];
            let tCodclasificacion2: string = tLinea["Codclasificacion2"] || tLinea["CODCLASIFICACION2"];
            let tCodclasificacion3: string = tLinea["Codclasificacion3"] || tLinea["CODCLASIFICACION3"];
            let tCodclasificacion4: string = tLinea["Codclasificacion4"] || tLinea["CODCLASIFICACION4"];
            let tCodcolor: string = tLinea["Codcolor"] || tLinea["CODCOLOR"];
            let tCodunidadmedida: string = tLinea["Codunidadmedida"] || tLinea["CODUNIDADMEDIDA"];
            let tIva: number = tLinea["Iva"] || tLinea["IVA"];
            let tCosto: number = tLinea["Costo"] || tLinea["COSTO"];
            let tPreciolista: number = tLinea["Preciolista"] || tLinea["PRECIOLISTA"];
            let tPrecio1: number = tLinea["Precio1"] || tLinea["PRECIO1"];
            let tPrecio2: number = tLinea["Precio2"] || tLinea["PRECIO2"];
            let tPeso: number = tLinea["Peso"] || tLinea["PESO"];
            let tAlto: number = tLinea["Alto"] || tLinea["ALTO"];
            let tAncho: number = tLinea["Ancho"] || tLinea["ANCHO"];
            let tLargo: number = tLinea["Largo"] || tLinea["LARGO"];
            let tVolumen: number = tLinea["Volumen"] || tLinea["VOLUMEN"];
            let tManejaLotes: string = tLinea["Maneja_Lotes"] || tLinea["MANEJA_LOTES"];
            let tManejaSeriales: string = tLinea["Maneja_Seriales"] || tLinea["MANEJA_SERIALES"];
            let tManejaTallas: string = tLinea["Maneja_Tallas"] || tLinea["MANEJA_TALLAS"];
            let tTallaInicial: string = tLinea["Talla_Inicial"] || tLinea["TALLA_INICIAL"];
            let tTallaFinal: string = tLinea["Talla_Final"] || tLinea["TALLA_FINAL"];
            let tTallaIntermedias: string = tLinea["Talla_Intermedias"] || tLinea["TALLA_INTERMEDIAS"];
            let tTallaSugerida: string = tLinea["Talla_Sugerida"] || tLinea["TALLA_SUGERIDA"];
            let tManejaVencimiento: string = tLinea["Maneja_Vencimiento"] || tLinea["MANEJA_VENCIMIENTO"];
            let tDiasVencimientoR: number = tLinea["Dias_Vencimiento_R"] || tLinea["DIAS_VENCIMIENTO_R"];
            let tDiasVencimientoD: number = tLinea["Dias_Vencimiento_D"] || tLinea["DIAS_VENCIMIENTO_D"];

            if (tDescripcion === undefined || tDescripcion === "") {

              this.toastr.clear();
              this.toastr.warning("Debe indicar una descripción. Linea: " + (tIndex + 1));
              return

            }

            if (tCodfabricante === undefined ||
              tCodfabricante === "") {

              this.toastr.clear();
              this.toastr.warning("El fabricante no esta cargado correctamente. Linea: " + (tIndex + 1));
              return

            }

            if (tCodmarca === undefined ||
              tCodmarca === "") {

              this.toastr.clear();
              this.toastr.warning("La marca no esta cargada correctamente. Linea: " + (tIndex + 1));
              return

            }

            if (tCodmodelo === undefined ||
              tCodmodelo === "") {

              this.toastr.clear();
              this.toastr.warning("El modelo no esta cargado correctamente. Linea: " + (tIndex + 1));
              return

            }

            if (tCodclasificacion1 === undefined ||
              tCodclasificacion1 === "") {

              this.toastr.clear();
              this.toastr.warning("La clasificacion1 no esta cargada correctamente. Linea: " + (tIndex + 1));
              return

            }

            if (tCodclasificacion2 === undefined ||
              tCodclasificacion2 === "") {

              this.toastr.clear();
              this.toastr.warning("La clasificacion2 no esta cargada correctamente. Linea: " + (tIndex + 1));
              return

            }

            if (tCodclasificacion3 === undefined ||
              tCodclasificacion3 === "") {

              this.toastr.clear();
              this.toastr.warning("La clasificacion3 no esta cargada correctamente. Linea: " + (tIndex + 1));
              return

            }

            if (tCodclasificacion4 === undefined ||
              tCodclasificacion4 === "") {

              this.toastr.clear();
              this.toastr.warning("La clasificacion4 no esta cargada correctamente. Linea: " + (tIndex + 1));
              return

            }

            if (tCodcolor === undefined ||
              tCodcolor === "") {

              this.toastr.clear();
              this.toastr.warning("El color no esta cargado correctamente. Linea: " + (tIndex + 1));
              return

            }

            if (tCodunidadmedida === undefined ||
              tCodunidadmedida === "") {

              this.toastr.clear();
              this.toastr.warning("La unidad de medida no esta registrada correctamente. Linea: " + (tIndex + 1));
              return

            }

            if (isNumber(tIva) === false) {

              tIva = 0;

            }

            if (tIva < 0) {

              tIva = 0;

            }

            if (isNumber(tCosto) === false) {

              tCosto = 0;

            }

            if (tCosto < 0) {

              tCosto = 0;

            }

            if (isNumber(tPreciolista) === false) {

              tPreciolista = 0;

            }

            if (tPreciolista < 0) {

              tPreciolista = 0;

            }

            if (isNumber(tPrecio1) === false) {

              tPrecio1 = 0;

            }

            if (tPrecio1 < 0) {

              tPrecio1 = 0;

            }

            if (isNumber(tPrecio2) === false) {

              tPrecio2 = 0;

            }

            if (tPrecio2 < 0) {

              tPrecio2 = 0;

            }

            if (tPreciolista <= 0 || tPrecio1 <= 0 || tPrecio2 <= 0) {

              this.toastr.clear();
              this.toastr.warning("Los precios cargados no son correctos. Linea: " + (tIndex + 1));
              return

            }

            if (isNumber(tPeso) === false) {

              tPeso = 0;

            }

            if (tPeso < 0) {

              tPeso = 0;

            }

            if (isNumber(tAlto) === false) {

              tAlto = 0;

            }

            if (tAlto < 0) {

              tAlto = 0;

            }

            if (isNumber(tAncho) === false) {

              tAncho = 0;

            }

            if (tAncho < 0) {

              tAncho = 0;

            }

            if (isNumber(tLargo) === false) {

              tLargo = 0;

            }

            if (tLargo < 0) {

              tLargo = 0;

            }

            if (isNumber(tVolumen) === false) {

              tVolumen = 0;

            }

            if (tVolumen < 0) {

              tVolumen = 0;

            }

            if (tManejaLotes === undefined ||
              tManejaLotes === "") {

              tManejaLotes = "N";

            }

            if (tManejaLotes !== "S" &&
              tManejaLotes !== "N") {

              this.toastr.clear();
              this.toastr.warning("El valor cargado para la columna de maneja lotes no es correcto. Linea: " + tLinea);
              return

            }

            if (tManejaSeriales === undefined ||
              tManejaSeriales === "") {

              tManejaSeriales = "N";

            }

            if (tManejaSeriales !== "S" &&
              tManejaSeriales !== "N") {

              this.toastr.clear();
              this.toastr.warning("El valor cargado para la columna de maneja seriales no es correcto. Linea: " + tLinea);
              return

            }

            if (tManejaTallas === undefined ||
              tManejaTallas === "") {

              tManejaTallas = "N";
              tTallaIntermedias = "S";
              tTallaInicial = "";
              tTallaFinal = "";
              tTallaSugerida = "";

            }

            if (tManejaTallas !== "S" &&
              tManejaTallas !== "N") {

              this.toastr.clear();
              this.toastr.warning("El valor cargado para la columna de maneja tallas no es correcto. Linea: " + tLinea);
              return

            }

            if (tTallaInicial === undefined) {

              tTallaInicial = "";

            }

            if (tTallaFinal === undefined) {

              tTallaFinal = "";

            }

            if (tTallaIntermedias === undefined ||
              tTallaIntermedias === "") {

              tTallaIntermedias = "N";

            }

            if (tTallaIntermedias !== "S" &&
              tTallaIntermedias !== "N") {

              this.toastr.clear();
              this.toastr.warning("El valor cargado para la columna de maneja tallas intermedias no es correcto. Linea: " + tLinea);
              return

            }

            if (tTallaSugerida === undefined) {

              tTallaSugerida = "";

            }

            if (tManejaVencimiento === undefined ||
              tManejaVencimiento === "") {

              tManejaVencimiento = "N";
              tDiasVencimientoD = 0;
              tDiasVencimientoR = 0;

            }

            if (tManejaVencimiento !== "S" &&
              tManejaVencimiento !== "N") {

              this.toastr.clear();
              this.toastr.warning("El valor cargado para la columna de maneja vencimiento no es correcto. Linea: " + tLinea);
              return

            }

            if (isNumber(tDiasVencimientoD) === false) {

              tDiasVencimientoD = 0;

            }

            if (tDiasVencimientoD < 0) {

              tDiasVencimientoD = 0;

            }

            if (isNumber(tDiasVencimientoR) === false) {

              tDiasVencimientoR = 0;

            }

            if (tDiasVencimientoR < 0) {

              tDiasVencimientoR = 0;

            }

            let tArticuloArch: ArticuloEntrada = new ArticuloEntrada();
            tArticuloArch.Corporacion = this.tEmpresaActual.Corporacion;
            tArticuloArch.Empresa = this.tEmpresaActual.Empresa;
            tArticuloArch.Descripcion = tDescripcion;
            tArticuloArch.Descripcion2 = tDescripcion2;
            tArticuloArch.Sku = tSku;
            tArticuloArch.Referencia = tReferencia;
            tArticuloArch.Codalterno = tCodalterno;
            tArticuloArch.Tipo = "A";
            tArticuloArch.Codfabricante = tCodfabricante;
            tArticuloArch.Codmarca = tCodmarca;
            tArticuloArch.Codmodelo = tCodmodelo;
            tArticuloArch.Codclasificacion1 = tCodclasificacion1;
            tArticuloArch.Codclasificacion2 = tCodclasificacion2;
            tArticuloArch.Codclasificacion3 = tCodclasificacion3;
            tArticuloArch.Codclasificacion4 = tCodclasificacion4;
            tArticuloArch.Codcolor = tCodcolor;
            tArticuloArch.Codunidadmedida = tCodunidadmedida;
            tArticuloArch.Iva = tIva;
            tArticuloArch.Costo = tCosto;
            tArticuloArch.Preciolista = tPreciolista;
            tArticuloArch.Precio1 = tPrecio1;
            tArticuloArch.Precio2 = tPrecio2;
            tArticuloArch.Peso = tPeso;
            tArticuloArch.Alto = tAlto;
            tArticuloArch.Ancho = tAncho;
            tArticuloArch.Largo = tLargo;
            tArticuloArch.Volumen = tVolumen;
            tArticuloArch.Maneja_lotes = tManejaLotes;
            tArticuloArch.Maneja_seriales = tManejaSeriales;
            tArticuloArch.Maneja_tallas = tManejaTallas;
            tArticuloArch.Talla_inicial = tTallaInicial;
            tArticuloArch.Talla_final = tTallaFinal;
            tArticuloArch.Talla_intermedias = tTallaIntermedias;
            tArticuloArch.Talla_sugerida = tTallaSugerida;
            tArticuloArch.Maneja_vencimiento = tManejaVencimiento;
            tArticuloArch.Dias_vencimiento_d = tDiasVencimientoD;
            tArticuloArch.Dias_vencimiento_r = tDiasVencimientoR;
            tArticuloArch.Usuariocreador = this.tUsuarioActual.Codusuario;
            tArticuloArch.Usuariomodificador = this.tUsuarioActual.Codusuario;

            tArticulosTemp.push(tArticuloArch);

          });

          this.tArticulosCargados = tArticulosTemp;
          this.tArchivoCargado = true;
          this.tFiltroActual = this.tFiltros[0];
          this.tFiltroBusqueda = "";

        }
        else {

          this.toastr.clear();
          this.toastr.warning("Los datos del archivo no fueron encontrado. Falta hoja de Ajuste");

        }

      });

    // var file = tArchivo.dataTransfer ? tArchivo.dataTransfer.files[0] : tArchivo.target.files[0];
    // var fr = new FileReader();
    // fr.onload = this.Procesar_Archivo.bind(this);
    // fr.readAsBinaryString(file);

  }

  // private Procesar_Archivo(e) {

  //   let workBook = null;
  //   let jsonData = null;
  //   let reader = e.target;
  //   const data = reader.result;
  //   workBook = XLSX.read(data, { type: 'binary' });
  //   jsonData = workBook.SheetNames.reduce((initial, name) => {
  //     const sheet = workBook.Sheets[name];
  //     initial[name] = XLSX.utils.sheet_to_json(sheet);
  //     return initial;
  //   }, {});

  //   this.tFiltroActual = this.tFiltros[0];
  //   this.tArticulosCargados = jsonData.Hoja1; //'JSON.stringify(jsonData);
  //   this.tArchivoCargado = true;

  // }

  Cancelar(): void {

    this.tArchivoCargado = false;
    this.tArticulosCargados = [];

  }

  Cargar(): void {

    this.tConfServices.AbrirModalConf("").then(Resultado => {

      switch (Resultado) {

        case "S": {

          this.Nuevo_Articulo();
          break;

        }

      };

    });

  }

}
