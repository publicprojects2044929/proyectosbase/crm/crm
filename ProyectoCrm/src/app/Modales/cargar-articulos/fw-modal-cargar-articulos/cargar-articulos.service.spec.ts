import { TestBed } from '@angular/core/testing';

import { CargarArticulosService } from './cargar-articulos.service';

describe('CargarArticulosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CargarArticulosService = TestBed.get(CargarArticulosService);
    expect(service).toBeTruthy();
  });
});
