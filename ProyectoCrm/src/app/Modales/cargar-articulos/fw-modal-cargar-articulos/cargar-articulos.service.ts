import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FwModalCargarArticulosComponent } from '../fw-modal-cargar-articulos/fw-modal-cargar-articulos.component'

@Injectable({
  providedIn: 'root'
})
export class CargarArticulosService {

  constructor(private modalService: NgbModal) { }

  AbrirModal() : Promise<any> {

    return new Promise((resolve, reject) => {

      const modalRef = this.modalService.open(FwModalCargarArticulosComponent, { size: 'lg' });
      modalRef.componentInstance.tTituloModal = "Carga masiva de artículos";
      modalRef.componentInstance.tMensajeModal = "";

      modalRef.result.then(result => {

        resolve(result);
  
      }, reason => {
  
        reject(reason);
  
      });

    })

  }
  
}
