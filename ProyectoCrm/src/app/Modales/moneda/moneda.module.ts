import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/Shared/shared.module'
import { NgbModule, NgbActiveModal, NgbAlertConfig } from '@ng-bootstrap/ng-bootstrap';

import { FwModalMonedasComponent } from 'src/app/Modales/moneda/fw-modal-monedas/fw-modal-monedas.component';

@NgModule({
  declarations: [
    FwModalMonedasComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    SharedModule
  ],
  exports: [
    FwModalMonedasComponent
  ],
  entryComponents: [
    FwModalMonedasComponent
  ],
  providers: [ NgbActiveModal, NgbAlertConfig ]
})
export class MonedaModalModule { }
