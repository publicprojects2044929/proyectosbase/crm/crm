import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwModalMonedasComponent } from './fw-modal-monedas.component';

describe('FwModalMonedasComponent', () => {
  let component: FwModalMonedasComponent;
  let fixture: ComponentFixture<FwModalMonedasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwModalMonedasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwModalMonedasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
