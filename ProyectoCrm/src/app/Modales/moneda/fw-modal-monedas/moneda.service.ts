import { Injectable } from '@angular/core';
import { FwModalMonedasComponent } from './fw-modal-monedas.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Injectable({
  providedIn: 'root'
})
export class MonedaService {

  constructor(private modalService: NgbModal) { }

  AbrirModal(): Promise<any> {

    return new Promise((resolve, reject) => {

      const modalRef = this.modalService.open(FwModalMonedasComponent, { size: 'lg' });
      modalRef.componentInstance.tTituloModal = "Modal de monedas";
      modalRef.componentInstance.tMensajeModal = "";

      modalRef.result.then(result => {

        resolve(result);

      }, reason => {

        reject(reason);

      });

    })

  }

}
