import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/Shared/shared.module'
import { NgbModule, NgbActiveModal, NgbAlertConfig } from '@ng-bootstrap/ng-bootstrap';

import { FwModalArticulosComponent } from 'src/app/Modales/articulo/fw-modal-articulos/fw-modal-articulos.component'

@NgModule({
  declarations: [
    FwModalArticulosComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    SharedModule
  ],
  exports: [
    FwModalArticulosComponent
  ],
  entryComponents: [
    FwModalArticulosComponent
  ],
  providers: [ NgbActiveModal, NgbAlertConfig ]
})
export class ArticuloModalModule { }
