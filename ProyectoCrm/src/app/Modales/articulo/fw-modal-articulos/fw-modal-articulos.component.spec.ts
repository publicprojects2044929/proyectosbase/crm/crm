import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwModalArticulosComponent } from './fw-modal-articulos.component';

describe('FwModalArticulosComponent', () => {
  let component: FwModalArticulosComponent;
  let fixture: ComponentFixture<FwModalArticulosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwModalArticulosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwModalArticulosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
