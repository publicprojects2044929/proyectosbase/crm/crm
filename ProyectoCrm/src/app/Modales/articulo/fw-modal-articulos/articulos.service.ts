import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FwModalArticulosComponent } from '../fw-modal-articulos/fw-modal-articulos.component'

@Injectable({
  providedIn: 'root'
})

export class ArticulosService {

  constructor(private modalService: NgbModal) { }

  AbrirModal(): Promise<any> {

    return new Promise((resolve, reject) => {

      const modalRef = this.modalService.open(FwModalArticulosComponent, { size: 'lg' });
      modalRef.componentInstance.tTituloModal = "Modal Articulos";
      modalRef.componentInstance.tMensajeModal = "";

      modalRef.result.then(result => {

        resolve(result);

      }, reason => {

        reject(reason);

      });

    })

  }

}
