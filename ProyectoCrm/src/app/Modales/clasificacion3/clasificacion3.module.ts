import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/Shared/shared.module'
import { NgbModule, NgbActiveModal, NgbAlertConfig } from '@ng-bootstrap/ng-bootstrap';

import { FwModalClasificaciones3Component } from 'src/app/Modales/clasificacion3/fw-modal-clasificaciones3/fw-modal-clasificaciones3.component'

@NgModule({
  declarations: [
    FwModalClasificaciones3Component
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    SharedModule
  ],
  exports: [
    FwModalClasificaciones3Component
  ],
  entryComponents: [
    FwModalClasificaciones3Component
  ],
  providers: [ NgbActiveModal, NgbAlertConfig ]
})
export class Clasificacion3ModalModule { }
