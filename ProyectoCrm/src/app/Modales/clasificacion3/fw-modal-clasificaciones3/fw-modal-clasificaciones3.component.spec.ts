import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwModalClasificaciones3Component } from './fw-modal-clasificaciones3.component';

describe('FwModalClasificaciones3Component', () => {
  let component: FwModalClasificaciones3Component;
  let fixture: ComponentFixture<FwModalClasificaciones3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwModalClasificaciones3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwModalClasificaciones3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
