import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { environment } from 'src/environments/environment';
import { Clasificacion2, Usuario, Empresas, Filtros, Clasificacion3 } from 'src/app/Clases/Estructura';
import { SessionService } from 'src/app/Core/Session/session.service';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { ErroresService } from '../../mensajes/fw-modal-msj/errores.service';
import { MensajesService } from '../../mensajes/fw-modal-msj/mensajes.service';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-fw-modal-clasificaciones3',
  templateUrl: './fw-modal-clasificaciones3.component.html',
  styleUrls: ['./fw-modal-clasificaciones3.component.scss']
})
export class FwModalClasificaciones3Component implements OnInit, OnDestroy {

  @Input() tTituloModal: string;
  @Input() tMensajeModal: string;
  @Input() tClasificacion2: Clasificacion2;

  tPosicion: number = 0;
  tPagina: number = 1;
  tTamanoPag: number = 5;

  tUsuarioActual: Usuario = new Usuario();
  tEmpresaActual: Empresas = new Empresas();
  tEmpresa$: Subscription;
  tHayConfiguracionWs$: Subscription;

  tFiltros: Filtros[] = [];
  tFiltroActual: Filtros = new Filtros();
  tFiltroBusqueda: string = "";

  tClasificaciones: Clasificacion3[] = [];

  constructor(public activeModal: NgbActiveModal,
    private tSession: SessionService,
    public Ws: ClsServiciosService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService) {

    this.tFiltros = [

      { Codfiltro: "Codclasificacion3", Filtro: "Código" },
      { Codfiltro: "Descripcion", Filtro: "Descripción" },

    ]

  }

  ngOnInit() {

    this.tEmpresa$ = this.tSession.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    })

    this.tFiltroBusqueda = "";

    this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
      .subscribe((tHayConfig: boolean) => {

        if (tHayConfig === true) {

          this.BuscarClasificaciones3();

        }

      });



  }

  ngOnDestroy() {

    this.tEmpresa$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();

  }

  BuscarClasificaciones3(): void {

    this.tClasificaciones = [];
    this.Ws.Clasificacion3()
      .Consumir_Obtener_Clasificacion3PorClasificacion1YClasificacion2(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        this.tClasificacion2.Codclasificacion1,
        this.tClasificacion2.Codclasificacion2)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tClasificaciones = Respuesta.Datos;
          this.tFiltroActual = this.tFiltros[0];

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

}
