import { TestBed } from '@angular/core/testing';

import { Clasificaciones3Service } from './clasificaciones3.service';

describe('Clasificaciones3Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Clasificaciones3Service = TestBed.get(Clasificaciones3Service);
    expect(service).toBeTruthy();
  });
});
