import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FwModalClasificaciones3Component } from '../fw-modal-clasificaciones3/fw-modal-clasificaciones3.component'
import { Clasificacion2 } from 'src/app/Clases/Estructura';

@Injectable({
  providedIn: 'root'
})
export class Clasificaciones3Service {

  constructor(private modalService: NgbModal) { }

  AbrirModal(tClasificacion2Actual: Clasificacion2): Promise<any> {

    return new Promise((resolve, reject) => {

      const modalRef = this.modalService.open(FwModalClasificaciones3Component, { size: 'lg' });
      modalRef.componentInstance.tTituloModal = "Modal de categoria";
      modalRef.componentInstance.tMensajeModal = "";
      modalRef.componentInstance.tClasificacion2 = tClasificacion2Actual;

      modalRef.result.then(result => {

        resolve(result);

      }, reason => {

        reject(reason);

      });

    })

  }

}
