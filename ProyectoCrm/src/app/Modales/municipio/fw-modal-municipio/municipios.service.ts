import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FwModalMunicipioComponent } from '../fw-modal-municipio/fw-modal-municipio.component'
import { Estado } from 'src/app/Clases/Estructura';


@Injectable({
  providedIn: 'root'
})
export class MunicipiosService {

  constructor(private modalService: NgbModal) { }

  AbrirModal(tEstadoActual: Estado): Promise<any> {

    return new Promise((resolve, reject) => {

      const modalRef = this.modalService.open(FwModalMunicipioComponent, { size: 'lg' });
      modalRef.componentInstance.tTituloModal = "Modal de Municipios";
      modalRef.componentInstance.tMensajeModal = "";
      modalRef.componentInstance.tEstadoActual = tEstadoActual;

      modalRef.result.then(result => {

        resolve(result);

      }, reason => {

        reject(reason);

      });

    })

  }

}
