import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwModalMunicipioComponent } from './fw-modal-municipio.component';

describe('FwModalMunicipioComponent', () => {
  let component: FwModalMunicipioComponent;
  let fixture: ComponentFixture<FwModalMunicipioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwModalMunicipioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwModalMunicipioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
