import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Estado, Usuario, Empresas, Filtros, Municipio } from 'src/app/Clases/Estructura';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { ErroresService } from '../../mensajes/fw-modal-msj/errores.service';
import { MensajesService } from '../../mensajes/fw-modal-msj/mensajes.service';
import { SessionService } from 'src/app/Core/Session/session.service';
import { environment } from 'src/environments/environment';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-fw-modal-municipio',
  templateUrl: './fw-modal-municipio.component.html',
  styleUrls: ['./fw-modal-municipio.component.scss']
})
export class FwModalMunicipioComponent implements OnInit, OnDestroy {

  @Input() tTituloModal: string;
  @Input() tMensajeModal: string;
  @Input() tEstadoActual: Estado;

  tPosicion: number = 0;
  tPagina: number = 1;
  tTamanoPag: number = 5;

  tUsuarioActual: Usuario = new Usuario();
  tUsuario$: Subscription;
  tEmpresaActual: Empresas = new Empresas();
  tEmpresa$: Subscription;
  tHayConfiguracionWs$: Subscription;

  tFiltros: Filtros[] = [];
  tFiltroActual: Filtros = new Filtros();
  tFiltroBusqueda: string = "";

  tMunicipios: Municipio[] = [];

  constructor(public activeModal: NgbActiveModal,
    private tSession: SessionService,
    public Ws: ClsServiciosService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService) {

    this.tFiltros = [

      { Codfiltro: "Codpais", Filtro: "País" },
      { Codfiltro: "Codestado", Filtro: "Estado" },
      { Codfiltro: "Codmunicipio", Filtro: "Municipio" },
      { Codfiltro: "Descripcion", Filtro: "Descripción" },

    ]

  }

  ngOnInit() {

    this.tEmpresa$ = this.tSession.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    });

    this.tUsuario$ = this.tSession.tUsuarioActual$.subscribe((tUsuario: Usuario) => {

      this.tUsuarioActual = tUsuario;

    });

    this.tFiltroBusqueda = "";

    this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
      .subscribe((tHayConfig: boolean) => {

        if (tHayConfig === true) {

          this.BuscarMunicipios();

        }

      });

  }

  ngOnDestroy() {

    this.tUsuario$.unsubscribe();
    this.tEmpresa$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();

  }

  BuscarMunicipios(): void {

    this.tMunicipios = [];

    this.Ws
      .Municipios()
      .Consumir_Obtener_MunicipioPorPaisYEstado(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        this.tEstadoActual.Codpais,
        this.tEstadoActual.Codestado)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tMunicipios = Respuesta.Datos;
          this.tFiltroActual = this.tFiltros[0];

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

}
