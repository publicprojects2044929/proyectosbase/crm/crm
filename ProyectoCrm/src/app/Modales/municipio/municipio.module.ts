import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/Shared/shared.module'
import { NgbModule, NgbActiveModal, NgbAlertConfig } from '@ng-bootstrap/ng-bootstrap';

import { FwModalMunicipioComponent } from './fw-modal-municipio/fw-modal-municipio.component';

@NgModule({
  declarations: [
    FwModalMunicipioComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    SharedModule
  ],
  exports: [
    FwModalMunicipioComponent
  ],
  entryComponents: [
    FwModalMunicipioComponent
  ],
  providers: [ NgbActiveModal, NgbAlertConfig ]
})
export class MunicipioModalModule { }
