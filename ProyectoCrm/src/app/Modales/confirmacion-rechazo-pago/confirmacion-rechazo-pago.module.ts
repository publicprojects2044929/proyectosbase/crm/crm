import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/Shared/shared.module'
import { NgbModule, NgbActiveModal, NgbAlertConfig } from '@ng-bootstrap/ng-bootstrap';

//Modales
import { FwModalConfirmacionRechazoPagoComponent } from 'src/app/Modales/confirmacion-rechazo-pago/fw-modal-confirmacion-rehazo-pago/fw-modal-confirmacion-rehazo-pago.component'

@NgModule({
  declarations: [
    FwModalConfirmacionRechazoPagoComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    SharedModule
  ],
  exports: [
    FwModalConfirmacionRechazoPagoComponent
  ],
  entryComponents: [
    FwModalConfirmacionRechazoPagoComponent
  ],
  providers: [ NgbActiveModal, NgbAlertConfig ]
})
export class ConfirmacionRechazoPagoModule { }
