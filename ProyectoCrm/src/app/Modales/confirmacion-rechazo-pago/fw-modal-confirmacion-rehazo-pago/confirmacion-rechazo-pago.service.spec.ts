import { TestBed } from '@angular/core/testing';

import { ConfirmacionRechazoPagoService } from './confirmacion-rechazo-pago.service';


describe('ConfirmacionRechazoPagoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ConfirmacionRechazoPagoService = TestBed.get(ConfirmacionRechazoPagoService);
    expect(service).toBeTruthy();
  });
});
