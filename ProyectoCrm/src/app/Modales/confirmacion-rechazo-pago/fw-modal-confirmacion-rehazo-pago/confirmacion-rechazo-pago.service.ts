import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FwModalConfirmacionRechazoPagoComponent } from './fw-modal-confirmacion-rehazo-pago.component'
import { VentaFormasPagosEntrada } from 'src/app/Clases/EstructuraEntrada';

@Injectable({
  providedIn: 'root'
})

export class ConfirmacionRechazoPagoService {

  public tTituloConf: string = "Modal rechazo de pago";

  constructor(private modalService: NgbModal) { }

  AbrirModalConf(tMensajeConf : string, tPago_Sel: VentaFormasPagosEntrada): Promise<string> {

    return new Promise((resolve, reject) => {

      const modalRef = this.modalService.open(FwModalConfirmacionRechazoPagoComponent);
      modalRef.componentInstance.tTituloModal = this.tTituloConf;
      modalRef.componentInstance.tMensajeModal = tMensajeConf;
      modalRef.componentInstance.tPago_Sel= tPago_Sel;
  
      modalRef.result.then(result => {

        resolve(result);
  
      }, reason => {
  
        reject(reason);
  
      });

    })

  }

}
