import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { ErroresService } from '../../mensajes/fw-modal-msj/errores.service';
import { ConfirmacionService } from '../../confirmacion/fw-modal-confirmacion/confirmacion.service';
import { MensajesService } from '../../mensajes/fw-modal-msj/mensajes.service';
import { VentaFormasPagosEntrada } from 'src/app/Clases/EstructuraEntrada';
import { environment } from 'src/environments/environment';
import { VentaFormasPagos } from 'src/app/Clases/Estructura';
import { Subscription } from 'rxjs';

@Component({

  selector: 'app-fw-modal-confirmacion-rechazo-pago',
  templateUrl: './fw-modal-confirmacion-rehazo-pago.component.html',
  styleUrls: ['./fw-modal-confirmacion-rehazo-pago.component.scss']

})

export class FwModalConfirmacionRechazoPagoComponent implements OnInit, OnDestroy {

  tRechazo: string = "N";
  tRechazoActual: VentaFormasPagos = new VentaFormasPagos();

  @Input() tTituloModal: string;
  @Input() tMensajeModal: string;
  @Input() tPago_Sel: VentaFormasPagosEntrada;

  tHayConfiguracionWs$: Subscription;

  constructor(public activeModal: NgbActiveModal,
    public Ws: ClsServiciosService,
    private tErroresServices: ErroresService,
    private tConfServices: ConfirmacionService,
    private tMsjServices: MensajesService) { }

  ngOnInit() {

    this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
      .subscribe((tHayConfig: boolean) => {

        if (tHayConfig === true) {



        }

      });

  }

  ngOnDestroy(){

    this.tHayConfiguracionWs$.unsubscribe();

  }

  IngresarMotivo(): void {

    this.tRechazo = "S";
    return;

  }

  RegistrarRechazo() {

    this.tPago_Sel.Observacion = this.tRechazoActual.Observacion;
    this.Ws
      .VentaFormasPagos()
      .Rechazar_VentaFormasPagos(this.tPago_Sel)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          return this.activeModal.close('S');

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

}
