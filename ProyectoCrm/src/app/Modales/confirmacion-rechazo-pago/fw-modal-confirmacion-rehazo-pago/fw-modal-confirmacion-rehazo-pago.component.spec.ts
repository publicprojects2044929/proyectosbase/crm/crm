import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwModalConfirmacionRechazoPagoComponent } from './fw-modal-confirmacion-rehazo-pago.component';

describe('FwModalConfirmacionRechazoPagoComponent', () => {
  let component: FwModalConfirmacionRechazoPagoComponent;
  let fixture: ComponentFixture<FwModalConfirmacionRechazoPagoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwModalConfirmacionRechazoPagoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwModalConfirmacionRechazoPagoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
