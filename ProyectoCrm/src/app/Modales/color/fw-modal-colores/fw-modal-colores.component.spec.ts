import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwModalColoresComponent } from './fw-modal-colores.component';

describe('FwModalColoresComponent', () => {
  let component: FwModalColoresComponent;
  let fixture: ComponentFixture<FwModalColoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwModalColoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwModalColoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
