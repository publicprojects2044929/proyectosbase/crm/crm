import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FwModalColoresComponent } from '../fw-modal-colores/fw-modal-colores.component'

@Injectable({
  providedIn: 'root'
})
export class ColoresService {

  constructor(private modalService: NgbModal) { }

  AbrirModal() : Promise<any> {

    return new Promise((resolve, reject) => {

      const modalRef = this.modalService.open(FwModalColoresComponent, { size: 'lg' });
      modalRef.componentInstance.tTituloModal = "Modal Colores";
      modalRef.componentInstance.tMensajeModal = "";

      modalRef.result.then(result => {

        resolve(result);
  
      }, reason => {
  
        reject(reason);
  
      });

    })

  }

}
