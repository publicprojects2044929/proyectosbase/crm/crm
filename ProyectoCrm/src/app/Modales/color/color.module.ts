import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/Shared/shared.module'
import { NgbModule, NgbActiveModal, NgbAlertConfig } from '@ng-bootstrap/ng-bootstrap';

import { FwModalColoresComponent } from 'src/app/Modales/color/fw-modal-colores/fw-modal-colores.component'

@NgModule({
  declarations: [
    FwModalColoresComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    SharedModule
  ],
  exports: [
    FwModalColoresComponent
  ],
  entryComponents: [
    FwModalColoresComponent
  ],
  providers: [ NgbActiveModal, NgbAlertConfig ]
})
export class ColorModalModule { }
