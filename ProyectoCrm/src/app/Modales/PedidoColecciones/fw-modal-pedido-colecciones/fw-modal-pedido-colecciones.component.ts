import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Pedido, Cliente, Usuario, Empresas, Tipo, Coleccion, ColeccionArticulos, PedidoColecciones } from 'src/app/Clases/Estructura';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SessionService } from 'src/app/Core/Session/session.service';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { ToastrService } from 'ngx-toastr';
import { ErroresService } from '../../mensajes/fw-modal-msj/errores.service';
import { MensajesService } from '../../mensajes/fw-modal-msj/mensajes.service';
import { ConfirmacionService } from '../../confirmacion/fw-modal-confirmacion/confirmacion.service';
import { environment } from 'src/environments/environment';
import { PedidoEntrada, PedidoColeccionesEntrada, PedidoArticulosColeccionesEntrada } from 'src/app/Clases/EstructuraEntrada';
import { Subscription } from 'rxjs';
import { ColeccionesService } from '../../colecciones/fw-modal-colecciones/colecciones.service';

@Component({
  selector: 'app-fw-modal-pedido-colecciones',
  templateUrl: './fw-modal-pedido-colecciones.component.html',
  styleUrls: ['./fw-modal-pedido-colecciones.component.scss']
})
export class FwModalPedidoColeccionesComponent implements OnInit, OnDestroy {

  @Input() tTituloModal: string = "";
  @Input() tMensajeModal: string = "";
  @Input() tPedidoModal: Pedido = new Pedido();
  @Input() tClienteModal: Cliente = new Cliente();

  tDescripcionPedido: string = "Sin pedido cargado";
  tDescripcionCliente: string = "";

  tPosicion: number = 0;
  tPagina: number = 1;
  tTamanoPag: number = 5;

  tUsuarioActual: Usuario = new Usuario();
  tUsuario$: Subscription;
  tEmpresaActual: Empresas = new Empresas();
  tEmpresa$: Subscription;
  tHayConfiguracionWs$: Subscription;

  tTiposPedido: Tipo[] = [];
  tTipoPedidoActual: Tipo = new Tipo();
  tNuevo: boolean = true;
  tNuevoArticulo: boolean = true;
  tPrecioCliente: string = "Preciolista";

  tColeccionActual: Coleccion = new Coleccion();

  constructor(public activeModal: NgbActiveModal,
    private tSession: SessionService,
    private Ws: ClsServiciosService,
    private toastr: ToastrService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService,
    private tConfServices: ConfirmacionService,
    private tColeccionesService: ColeccionesService) { }

  ngOnInit() {

    this.tEmpresa$ = this.tSession.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    });

    this.tUsuario$ = this.tSession.tUsuarioActual$.subscribe((tUsuario: Usuario) => {

      this.tUsuarioActual = tUsuario;

    });

    if (this.tClienteModal.Tipoprecio === "") {

      this.tPrecioCliente = "Preciolista";

    }
    else {

      var tPrimerCaracter: string = this.tClienteModal.Tipoprecio.charAt(0).toUpperCase() || "";
      var tOtrosCaracter: string = this.tClienteModal.Tipoprecio.slice(1).toLowerCase() || "";
      let tPrecioCap: string = tPrimerCaracter + tOtrosCaracter;
      this.tPrecioCliente = tPrecioCap;

    }

    this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
      .subscribe((tHayConfig: boolean) => {

        if (tHayConfig === true) {

          this.Buscar_Tipos();

        }

      });

    this.tDescripcionCliente = `[${this.tClienteModal.Codcliente}] - ${this.tClienteModal.Descripcion} `;

    if (this.tPedidoModal.Codpedido !== "") {

      this.tNuevo = false;
      this.tDescripcionPedido = `${this.tPedidoModal.Tipo} - ${this.tPedidoModal.Codpedido}`;

    }
    else {
      this.tDescripcionPedido = 'Sin pedido cargado';
    }

  }

  ngOnDestroy() {

    this.tUsuario$.unsubscribe();
    this.tEmpresa$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();

  }

  Buscar_Tipos() {

    this.Ws
      .Tipo()
      .Consumir_Obtener_Tipo(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        "TR_PEDIDO")
      .subscribe(Respuesta => {

        if (Respuesta.Resultado === "N") {


        }
        else if (Respuesta.Resultado === "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tTiposPedido = Respuesta.Datos;
          this.tTipoPedidoActual = this.tTiposPedido.find(tTipo => {

            if (tTipo.Codtipo === this.tPedidoModal.Tipo) {

              return tTipo;

            }

          }) || this.tTiposPedido[0];

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#region "CRUD"

  NuevoPedido() {

    let tNuevoPedido: PedidoEntrada = {

      "Id": 0,
      "Corporacion": this.tEmpresaActual.Corporacion,
      "Empresa": this.tEmpresaActual.Empresa,
      "Codpedido": "",
      "Codcliente": this.tClienteModal.Codcliente,
      "Tipo": this.tTipoPedidoActual.Codtipo,
      "Estatus": "ACTIVO",
      "Usuariocreador": this.tUsuarioActual.Codusuario,
      "Usuariomodificador": this.tUsuarioActual.Codusuario,
      "LArticulos": [],
      "LCupones": [],
      "LColecciones": []

    }

    let tColeccionPedido: PedidoColeccionesEntrada = {

      "Id": 0,
      "Corporacion": this.tEmpresaActual.Corporacion,
      "Empresa": this.tEmpresaActual.Empresa,
      "Codpedido": "",
      "Codcoleccion": this.tColeccionActual.Codcoleccion,
      "Usuariocreador": this.tUsuarioActual.Codusuario,
      "Usuariomodificador": this.tUsuarioActual.Codusuario,
      "LArticulos": []

    }

    this.tColeccionActual.LArticulos.forEach(tArticulo => {

      if (tArticulo.Cantidad <= tArticulo.Disponible) {

        let tNuevoArticuloColeccion: PedidoArticulosColeccionesEntrada = {

          "Id": 0,
          "Corporacion": this.tEmpresaActual.Corporacion,
          "Empresa": this.tEmpresaActual.Empresa,
          "Codpedido": "",
          "Codcoleccion": this.tColeccionActual.Codcoleccion,
          "Codarticulo": tArticulo.Codarticulo,
          "Talla": tArticulo.Talla,
          "Usuariocreador": this.tUsuarioActual.Codusuario,
          "Usuariomodificador": this.tUsuarioActual.Codusuario

        }

        tColeccionPedido.LArticulos.push(tNuevoArticuloColeccion)

      }

    });

    tNuevoPedido.LColecciones.push(tColeccionPedido);

    this.Ws
      .Pedidos()
      .Consumir_Crear_PedidoColecciones(tNuevoPedido)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tNuevo = false;
          this.toastr.success("El pedido se creo correctamente", "Pedido");
          this.tPedidoModal = Respuesta.Datos[0];
          this.Limpiar();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  NuevoArticulo() {

    let tColeccionPedido: PedidoColeccionesEntrada = {

      "Id": 0,
      "Corporacion": this.tEmpresaActual.Corporacion,
      "Empresa": this.tEmpresaActual.Empresa,
      "Codpedido": this.tPedidoModal.Codpedido,
      "Codcoleccion": this.tColeccionActual.Codcoleccion,
      "Usuariocreador": this.tPedidoModal.Usuariocreador,
      "Usuariomodificador": this.tUsuarioActual.Codusuario,
      "LArticulos": []

    }

    this.tColeccionActual.LArticulos.forEach(tArticulo => {

      if (tArticulo.Cantidad <= tArticulo.Disponible) {

        let tNuevoArticuloColeccion: PedidoArticulosColeccionesEntrada = {

          "Id": 0,
          "Corporacion": this.tEmpresaActual.Corporacion,
          "Empresa": this.tEmpresaActual.Empresa,
          "Codpedido": this.tPedidoModal.Codpedido,
          "Codcoleccion": this.tColeccionActual.Codcoleccion,
          "Codarticulo": tArticulo.Codarticulo,
          "Talla": tArticulo.Talla,
          "Usuariocreador": this.tUsuarioActual.Codusuario,
          "Usuariomodificador": this.tUsuarioActual.Codusuario

        }

        tColeccionPedido.LArticulos.push(tNuevoArticuloColeccion)

      }

    });

    this.Ws
      .PedidoColeccion()
      .Consumir_Crear_PedidoColecciones(tColeccionPedido)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado === "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado === "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.toastr.success("La colección se cargo correctamente.", "Pedido");
          this.tPedidoModal = Respuesta.Datos[0];
          this.Limpiar();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  
  AbrirModalColecciones(): void {

    this.tColeccionesService.AbrirModal().then(Resultado => {

      if (Resultado.Resultado == "S") {

        var tColeccionModal: Coleccion = Resultado.Coleccion;
        this.BuscarColeccion(tColeccionModal.Codcoleccion);

      }

    });

  }


  BuscarColeccion(tCodcoleccion: string) {

    let tColeccionExiste: PedidoColecciones = new PedidoColecciones();

    if (tCodcoleccion === "") {

      return;

    }

    tColeccionExiste = this.tPedidoModal.LColecciones.find(tColeccion => {

      if (tColeccion.Codcoleccion === tCodcoleccion) {

        return tColeccion;

      }

    }) || new PedidoColecciones();

    if (tColeccionExiste.Codcoleccion !== "") {

      this.toastr.clear();
      this.toastr.warning("La colección ya perteneces al pedido", "");
      return;

    }

    this.Ws
      .Coleccion()
      .Consumir_ObtenerColecciones_Vigentes(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        tCodcoleccion)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tColeccionActual = Respuesta.Datos[0];

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  ObtenerPrecio(tArticuloColeccion: ColeccionArticulos): number {

    return tArticuloColeccion[this.tPrecioCliente] || 0;

  }

  AceptarPedido() {

    if (this.tColeccionActual.Codcoleccion === "") {

      this.toastr.clear();
      this.toastr.warning("Debe indicar una colección", "Pedidos");
      return;

    }

    if (this.tPedidoModal.Codpedido == "") {

      this.tConfServices
        .AbrirModalConf("Desea generar un nuevo pedido")
        .then(Respuesta => {

          switch (Respuesta) {

            case "S": {

              this.NuevoPedido();
              break;

            }

          };

        });

    }
    else {

      if (this.tNuevoArticulo == false) {



      }
      else {

        this.tConfServices
          .AbrirModalConf("¿Desea agregar la colección al pedido?")
          .then(Respuesta => {

            switch (Respuesta) {

              case "S": {

                this.NuevoArticulo();
                break;

              }

            };

          });

      }

    }

  }

  Limpiar() {

    this.tColeccionActual = new Coleccion();

  }

  Seleccionar_Tipo(tTipoSel: Tipo){

    if(this.tNuevo === false) { return; }

    this.tTipoPedidoActual = tTipoSel;

  }

}
