import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwModalPedidoColeccionesComponent } from './fw-modal-pedido-colecciones.component';

describe('FwModalPedidoColeccionesComponent', () => {
  let component: FwModalPedidoColeccionesComponent;
  let fixture: ComponentFixture<FwModalPedidoColeccionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwModalPedidoColeccionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwModalPedidoColeccionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
