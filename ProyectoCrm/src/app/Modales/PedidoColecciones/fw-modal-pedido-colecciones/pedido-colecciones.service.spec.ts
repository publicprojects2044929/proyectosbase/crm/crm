import { TestBed } from '@angular/core/testing';

import { PedidoColeccionesService } from './pedido-colecciones.service';

describe('PedidoColeccionesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PedidoColeccionesService = TestBed.get(PedidoColeccionesService);
    expect(service).toBeTruthy();
  });
});
