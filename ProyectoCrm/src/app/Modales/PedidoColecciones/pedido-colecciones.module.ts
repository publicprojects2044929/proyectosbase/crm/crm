import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/Shared/shared.module'
import { NgbModule, NgbActiveModal, NgbAlertConfig } from '@ng-bootstrap/ng-bootstrap';

import { FwModalPedidoColeccionesComponent } from './fw-modal-pedido-colecciones/fw-modal-pedido-colecciones.component';

@NgModule({
  declarations: [
    FwModalPedidoColeccionesComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    SharedModule
  ],
  exports: [
    FwModalPedidoColeccionesComponent
  ],
  entryComponents: [
    FwModalPedidoColeccionesComponent
  ],
  providers: [ NgbActiveModal, NgbAlertConfig ]
})
export class PedidoColeccionesModalModule { }
