import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/Shared/shared.module'
import { NgbModule, NgbActiveModal, NgbAlertConfig } from '@ng-bootstrap/ng-bootstrap';

import { FwModalClasificaciones1Component } from 'src/app/Modales/clasificacion1/fw-modal-clasificaciones1/fw-modal-clasificaciones1.component'

@NgModule({
  declarations: [
    FwModalClasificaciones1Component
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    SharedModule
  ],
  exports: [
    FwModalClasificaciones1Component
  ],
  entryComponents: [
    FwModalClasificaciones1Component
  ],
  providers: [ NgbActiveModal, NgbAlertConfig ]
})
export class Clasificacion1ModalModule { }
