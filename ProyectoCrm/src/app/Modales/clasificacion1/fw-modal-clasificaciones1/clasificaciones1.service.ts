import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FwModalClasificaciones1Component } from '../fw-modal-clasificaciones1/fw-modal-clasificaciones1.component'

@Injectable({
  providedIn: 'root'
})
export class Clasificaciones1Service {

  constructor(private modalService: NgbModal) { }

  AbrirModal(): Promise<any> {

    return new Promise((resolve, reject) => {

      const modalRef = this.modalService.open(FwModalClasificaciones1Component, { size: 'lg' });
      modalRef.componentInstance.tTituloModal = "Modal de clase";
      modalRef.componentInstance.tMensajeModal = "";

      modalRef.result.then(result => {

        resolve(result);

      }, reason => {

        reject(reason);

      });

    })

  }
  
}
