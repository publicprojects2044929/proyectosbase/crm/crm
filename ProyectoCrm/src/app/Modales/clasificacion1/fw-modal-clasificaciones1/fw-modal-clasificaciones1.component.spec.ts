import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwModalClasificaciones1Component } from './fw-modal-clasificaciones1.component';

describe('FwModalClasificaciones1Component', () => {
  let component: FwModalClasificaciones1Component;
  let fixture: ComponentFixture<FwModalClasificaciones1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwModalClasificaciones1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwModalClasificaciones1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
