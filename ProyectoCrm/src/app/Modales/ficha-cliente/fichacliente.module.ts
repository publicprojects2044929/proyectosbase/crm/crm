import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/Shared/shared.module';
import { NgbModule, NgbActiveModal, NgbAlertConfig } from '@ng-bootstrap/ng-bootstrap';

import { FwModalFichaClienteComponent } from 'src/app/Modales/ficha-cliente/fw-modal-ficha-cliente/fw-modal-ficha-cliente.component'
import { BsDatepickerModule } from 'ngx-bootstrap';
import { AgmCoreModule } from '@agm/core'
import { environment } from 'src/environments/environment';

@NgModule({
  declarations: [
    FwModalFichaClienteComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    BsDatepickerModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: environment.api_key_maps
    }),
    SharedModule
  ],
  exports: [
    FwModalFichaClienteComponent
  ],
  entryComponents: [
    FwModalFichaClienteComponent
  ],
  providers: [ NgbActiveModal, NgbAlertConfig ]
})
export class FichaClienteModalModule { }
