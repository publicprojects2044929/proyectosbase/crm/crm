import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Cliente, Ciudad } from 'src/app/Clases/Estructura';

import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { ErroresService } from '../../mensajes/fw-modal-msj/errores.service';
import { MensajesService } from '../../mensajes/fw-modal-msj/mensajes.service';
import { environment } from 'src/environments/environment';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-fw-modal-ficha-cliente',
  templateUrl: './fw-modal-ficha-cliente.component.html',
  styleUrls: ['./fw-modal-ficha-cliente.component.scss']
})

export class FwModalFichaClienteComponent implements OnInit, OnDestroy {

  @Input() tTituloModal: string = "";
  @Input() tMensajeModal: string = "";
  @Input() tClienteModal: Cliente = new Cliente();

  tClienteActual: Cliente = new Cliente();
  tDatosNacimiento: Ciudad = new Ciudad();
  tDatosFacturacion: Ciudad = new Ciudad();
  tDatosEnvio: Ciudad = new Ciudad();

  tZoom: number = 20;

  tHayConfiguracionWs$: Subscription;

  constructor(public activeModal: NgbActiveModal,
    public Ws: ClsServiciosService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService) { }

  ngOnInit() {

    this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
      .subscribe((tHayConfig: boolean) => {

        if (tHayConfig === true) {

          this.BuscarClientes();

        }

      });

  }

  ngOnDestroy() {

    this.tHayConfiguracionWs$.unsubscribe();

  }

  BuscarClientes(): void {

    this.Ws.Cliente()
      .Consumir_Obtener_ClientesDetallado(this.tClienteModal.Corporacion,
        this.tClienteModal.Empresa,
        this.tClienteModal.Codcliente)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tClienteActual = Respuesta.Datos[0];
          this.BuscarCiudadNacimiento();
          this.BuscarCiudadFacturacion();
          this.BuscarCiudadEnvio();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  BuscarCiudadNacimiento(): void {

    this.tDatosNacimiento = new Ciudad;

    this.Ws
      .Ciudades()
      .Consumir_Obtener_CiudadPorPaisEstadoYMunicipio(this.tClienteActual.Corporacion,
        this.tClienteActual.Empresa,
        this.tClienteActual.Codpais,
        this.tClienteActual.Codestado,
        this.tClienteActual.Codmunicipio)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.tDatosNacimiento = Respuesta.Datos[0];

        }


      }, error => {

        this.tErroresServices.MostrarError(error);

      })

  }

  BuscarCiudadFacturacion(): void {

    this.tDatosFacturacion = new Ciudad;

    this.Ws
      .Ciudades()
      .Consumir_Obtener_CiudadPorPaisEstadoYMunicipio(this.tClienteActual.Corporacion,
        this.tClienteActual.Empresa,
        this.tClienteActual.Codpais_facturacion,
        this.tClienteActual.Codestado_facturacion,
        this.tClienteActual.Codmunicipio_facturacion)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.tDatosFacturacion = Respuesta.Datos[0];

        }


      }, error => {

        this.tErroresServices.MostrarError(error);

      })

  }

  BuscarCiudadEnvio(): void {

    this.tDatosEnvio = new Ciudad;

    this.Ws
      .Ciudades()
      .Consumir_Obtener_CiudadPorPaisEstadoYMunicipio(this.tClienteActual.Corporacion,
        this.tClienteActual.Empresa,
        this.tClienteActual.Codpais_envio,
        this.tClienteActual.Codestado_envio,
        this.tClienteActual.Codmunicipio_envio)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.tDatosEnvio = Respuesta.Datos[0];

        }


      }, error => {

        this.tErroresServices.MostrarError(error);

      })

  }

}
