import { TestBed } from '@angular/core/testing';

import { FichaClienteService } from './ficha-cliente.service';

describe('FichaClienteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FichaClienteService = TestBed.get(FichaClienteService);
    expect(service).toBeTruthy();
  });
});
