import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwModalFichaClienteComponent } from './fw-modal-ficha-cliente.component';

describe('FwModalFichaClienteComponent', () => {
  let component: FwModalFichaClienteComponent;
  let fixture: ComponentFixture<FwModalFichaClienteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwModalFichaClienteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwModalFichaClienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
