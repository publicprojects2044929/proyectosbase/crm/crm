import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FwModalFichaClienteComponent } from '../fw-modal-ficha-cliente/fw-modal-ficha-cliente.component'
import { Cliente } from '../../../Clases/Estructura';

@Injectable({
  providedIn: 'root'
})

export class FichaClienteService {

  constructor(private modalService: NgbModal) { }

  AbrirModal(tClienteActual: Cliente): Promise<any> {

    return new Promise((resolve, reject) => {

      const modalRef = this.modalService.open(FwModalFichaClienteComponent, { size: 'xl' });
      modalRef.componentInstance.tTituloModal = "Ficha de clientes";
      modalRef.componentInstance.tMensajeModal = "";
      modalRef.componentInstance.tClienteModal = tClienteActual;

      modalRef.result.then(result => {

        resolve(result);

      }, reason => {

        reject(reason);

      });

    })

  }

}
