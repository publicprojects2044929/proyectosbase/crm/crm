import { Component, OnInit, Input } from '@angular/core';
import { Usuario, Empresas, Filtros, Coleccion } from 'src/app/Clases/Estructura';
import { Subscription } from 'rxjs';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SessionService } from 'src/app/Core/Session/session.service';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { ErroresService } from '../../mensajes/fw-modal-msj/errores.service';
import { MensajesService } from '../../mensajes/fw-modal-msj/mensajes.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-fw-modal-colecciones',
  templateUrl: './fw-modal-colecciones.component.html',
  styleUrls: ['./fw-modal-colecciones.component.scss']
})
export class FwModalColeccionesComponent implements OnInit {

  @Input() tTituloModal: string;
  @Input() tMensajeModal: string;
  tPosicion: number = 0;
  tPagina: number = 1;
  tTamanoPag: number = 5;

  tUsuarioActual: Usuario = new Usuario();
  tEmpresaActual: Empresas = new Empresas();
  tEmpresa$: Subscription;
  tHayConfiguracionWs$: Subscription;

  tColecciones: Coleccion[] = [];

  tFiltros: Filtros[] = [];
  tFiltroActual: Filtros = new Filtros();
  tFiltroBusqueda: string = "";

  constructor(public activeModal: NgbActiveModal,
    private tSession: SessionService,
    public Ws: ClsServiciosService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService) {

    this.tFiltros = [

      { Codfiltro: "Codcoleccion", Filtro: "Coleccion" },
      { Codfiltro: "Descripcion", Filtro: "Descripcion" },

    ]

  }

  ngOnInit() {

    this.tEmpresa$ = this.tSession.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    })

    this.tFiltroBusqueda = "";

    this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
      .subscribe((tHayConfig: boolean) => {

        if (tHayConfig === true) {

          this.BuscarColecciones();

        }

      });

  }

  ngOnDestroy() {

    this.tEmpresa$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();

  }

  BuscarColecciones(): void {

    this.tColecciones = [];

    this.Ws.Coleccion()
      .Consumir_ObtenerColecciones(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          // this.tMensajeMsj = Respuesta.Mensaje;
          // this.AbrirModalMsj();

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tColecciones = Respuesta.Datos;
          this.tFiltroActual = this.tFiltros[0];

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

}
