import { TestBed } from '@angular/core/testing';

import { ColeccionesService } from './colecciones.service';

describe('ColeccionesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ColeccionesService = TestBed.get(ColeccionesService);
    expect(service).toBeTruthy();
  });
});
