import { Injectable } from '@angular/core';
import { FwModalColeccionesComponent } from './fw-modal-colecciones.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Injectable({
  providedIn: 'root'
})
export class ColeccionesService {

  constructor(private modalService: NgbModal) { }

  AbrirModal(): Promise<any> {

    return new Promise((resolve, reject) => {

      const modalRef = this.modalService.open(FwModalColeccionesComponent, { size: 'lg' });
      modalRef.componentInstance.tTituloModal = "Modal Colecciones";
      modalRef.componentInstance.tMensajeModal = "";

      modalRef.result.then(result => {

        resolve(result);

      }, reason => {

        reject(reason);

      });

    })

  }
  
}
