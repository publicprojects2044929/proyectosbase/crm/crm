import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwModalColeccionesComponent } from './fw-modal-colecciones.component';

describe('FwModalColeccionesComponent', () => {
  let component: FwModalColeccionesComponent;
  let fixture: ComponentFixture<FwModalColeccionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwModalColeccionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwModalColeccionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
