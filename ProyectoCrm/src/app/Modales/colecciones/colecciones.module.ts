import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/Shared/shared.module'
import { NgbModule, NgbActiveModal, NgbAlertConfig } from '@ng-bootstrap/ng-bootstrap';

import { FwModalColeccionesComponent } from './fw-modal-colecciones/fw-modal-colecciones.component';

@NgModule({
  declarations: [
    FwModalColeccionesComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    SharedModule
  ],
  exports: [
    FwModalColeccionesComponent
  ],
  entryComponents: [
    FwModalColeccionesComponent
  ],
  providers: [ NgbActiveModal, NgbAlertConfig ]
})
export class ColeccionesModalModule { }
