import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/Shared/shared.module'
import { NgbModule, NgbActiveModal, NgbAlertConfig } from '@ng-bootstrap/ng-bootstrap';

import { FwModalModelosComponent } from 'src/app/Modales/modelo/fw-modal-modelos/fw-modal-modelos.component'

@NgModule({
  declarations: [
    FwModalModelosComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    SharedModule
  ],
  exports: [
    FwModalModelosComponent
  ],
  entryComponents: [
    FwModalModelosComponent
  ],
  providers: [ NgbActiveModal, NgbAlertConfig ]
})
export class ModeloModalModule { }
