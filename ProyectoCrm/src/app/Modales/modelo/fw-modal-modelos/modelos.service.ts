import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FwModalModelosComponent } from '../fw-modal-modelos/fw-modal-modelos.component'
import { Marca } from 'src/app/Clases/Estructura';

@Injectable({
  providedIn: 'root'
})

export class ModelosService {

  constructor(private modalService: NgbModal) { }

  AbrirModal(tMarcaActual: Marca): Promise<any> {

    return new Promise((resolve, reject) => {

      const modalRef = this.modalService.open(FwModalModelosComponent, { size: 'lg' });
      modalRef.componentInstance.tTituloModal = "Modal de modelos";
      modalRef.componentInstance.tMensajeModal = "";
      modalRef.componentInstance.tMarca = tMarcaActual;

      modalRef.result.then(result => {

        resolve(result);

      }, reason => {

        reject(reason);

      });

    })

  }

}
