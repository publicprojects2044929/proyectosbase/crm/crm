import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwModalModelosComponent } from './fw-modal-modelos.component';

describe('FwModalModelosComponent', () => {
  let component: FwModalModelosComponent;
  let fixture: ComponentFixture<FwModalModelosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwModalModelosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwModalModelosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
