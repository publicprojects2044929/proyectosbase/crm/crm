import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwModalClientesComponent } from './fw-modal-clientes.component';

describe('FwModalClientesComponent', () => {
  let component: FwModalClientesComponent;
  let fixture: ComponentFixture<FwModalClientesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwModalClientesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwModalClientesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
