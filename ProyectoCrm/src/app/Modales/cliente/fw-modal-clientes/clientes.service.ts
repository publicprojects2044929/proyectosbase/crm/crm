import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FwModalClientesComponent } from '../fw-modal-clientes/fw-modal-clientes.component'

@Injectable({
  providedIn: 'root'
})
export class ClientesService {

  constructor(private modalService: NgbModal) { }

  AbrirModal(): Promise<any> {

    return new Promise((resolve, reject) => {

      const modalRef = this.modalService.open(FwModalClientesComponent, { size: 'lg' });
      modalRef.componentInstance.tTituloModal = "Modal Clientes";
      modalRef.componentInstance.tMensajeModal = "";

      modalRef.result.then(result => {

        resolve(result);

      }, reason => {

        reject(reason);

      });

    })

  }

}
