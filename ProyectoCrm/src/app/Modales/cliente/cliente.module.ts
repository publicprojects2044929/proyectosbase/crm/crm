import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/Shared/shared.module'
import { NgbModule, NgbActiveModal, NgbAlertConfig } from '@ng-bootstrap/ng-bootstrap';

import { FwModalClientesComponent } from 'src/app/Modales/cliente/fw-modal-clientes/fw-modal-clientes.component'

@NgModule({
  declarations: [
    FwModalClientesComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    SharedModule
  ],
  exports: [
    FwModalClientesComponent
  ],
  entryComponents: [
    FwModalClientesComponent
  ],
  providers: [ NgbActiveModal, NgbAlertConfig ]
})
export class ClienteModalModule { }
