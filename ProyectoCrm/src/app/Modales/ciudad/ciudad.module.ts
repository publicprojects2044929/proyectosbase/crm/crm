import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/Shared/shared.module'
import { NgbModule, NgbActiveModal, NgbAlertConfig } from '@ng-bootstrap/ng-bootstrap';

import { FwModalCiudadesComponent } from 'src/app/Modales/ciudad/fw-modal-ciudades/fw-modal-ciudades.component'

@NgModule({
  declarations: [
    FwModalCiudadesComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    SharedModule
  ],
  exports: [
    FwModalCiudadesComponent
  ],
  entryComponents: [
    FwModalCiudadesComponent
  ],
  providers: [ NgbActiveModal, NgbAlertConfig ]
})
export class CiudadModalModule { }
