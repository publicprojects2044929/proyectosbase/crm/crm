import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FwModalCiudadesComponent } from '../fw-modal-ciudades/fw-modal-ciudades.component'
import { Municipio } from 'src/app/Clases/Estructura';

@Injectable({
  providedIn: 'root'
})
export class CiudadesService {

  constructor(private modalService: NgbModal) { }

  AbrirModal(tMunicipioActual: Municipio): Promise<any> {

    return new Promise((resolve, reject) => {

      const modalRef = this.modalService.open(FwModalCiudadesComponent, { size: 'lg' });
      modalRef.componentInstance.tTituloModal = "Modal de Ciudades";
      modalRef.componentInstance.tMensajeModal = "";
      modalRef.componentInstance.tMunicipioActual = tMunicipioActual;

      modalRef.result.then(result => {

        resolve(result);

      }, reason => {

        reject(reason);

      });

    })

  }

}
