import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwModalCiudadesComponent } from './fw-modal-ciudades.component';

describe('FwModalCiudadesComponent', () => {
  let component: FwModalCiudadesComponent;
  let fixture: ComponentFixture<FwModalCiudadesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwModalCiudadesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwModalCiudadesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
