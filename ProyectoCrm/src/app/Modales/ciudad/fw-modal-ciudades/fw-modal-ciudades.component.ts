import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Usuario, Empresas, Filtros, Municipio, Ciudad } from 'src/app/Clases/Estructura';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { SessionService } from 'src/app/Core/Session/session.service';
import { environment } from 'src/environments/environment';
import { ErroresService } from '../../mensajes/fw-modal-msj/errores.service';
import { MensajesService } from '../../mensajes/fw-modal-msj/mensajes.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-fw-modal-ciudades',
  templateUrl: './fw-modal-ciudades.component.html',
  styleUrls: ['./fw-modal-ciudades.component.scss']
})

export class FwModalCiudadesComponent implements OnInit, OnDestroy {

  @Input() tTituloModal: string;
  @Input() tMensajeModal: string;
  @Input() tMunicipioActual: Municipio;

  tPosicion: number = 0;
  tPagina: number = 1;
  tTamanoPag: number = 5;

  tUsuarioActual: Usuario = new Usuario();
  tEmpresaActual: Empresas = new Empresas();
  tEmpresa$: Subscription;
  tHayConfiguracionWs$: Subscription;

  tFiltros: Filtros[] = [];
  tFiltroActual: Filtros = new Filtros;
  tFiltroBusqueda: string = "";

  tCiudades: Ciudad[] = [];
  tCiudadesFiltrados: Ciudad[] = [];

  constructor(public activeModal: NgbActiveModal,
    public Ws: ClsServiciosService,
    private tSesion: SessionService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService) {

    this.tFiltros = [

      { Codfiltro: "Codpais", Filtro: "País" },
      { Codfiltro: "Codestado", Filtro: "Estado" },
      { Codfiltro: "Codmunicipio", Filtro: "Municipio" },
      { Codfiltro: "Codciudad", Filtro: "Ciudad" },
      { Codfiltro: "Descripcion", Filtro: "Descripción" },

    ]

  }

  ngOnInit() {

    this.tEmpresa$ = this.tSesion.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    })

    this.tFiltroBusqueda = "";

    this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
      .subscribe((tHayConfig: boolean) => {

        if (tHayConfig === true) {

          this.BuscarCiudades();

        }

      });

  }

  ngOnDestroy(): void {

    this.tEmpresa$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();

  }

  BuscarCiudades(): void {

    this.tCiudades = [];
    this.Ws
      .Ciudades()
      .Consumir_Obtener_CiudadPorPaisEstadoYMunicipio(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        this.tMunicipioActual.Codpais,
        this.tMunicipioActual.Codestado,
        this.tMunicipioActual.Codmunicipio)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tCiudades = Respuesta.Datos;
          this.tFiltroActual = this.tFiltros[0];
          this.Buscar_Datos();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Buscar_Datos(): void {

    this.tCiudadesFiltrados = [];
    var tValorCampo: string;

    if (this.tFiltroBusqueda != "") {

      for (let tCiudad of this.tCiudades) {

        tValorCampo = tCiudad[this.tFiltroActual.Codfiltro];

        if (tValorCampo.startsWith(this.tFiltroBusqueda) == true) {

          this.tCiudadesFiltrados.push(tCiudad);

        }

      }

    }
    else {

      this.tCiudadesFiltrados = this.tCiudades;

    }

  }

}
