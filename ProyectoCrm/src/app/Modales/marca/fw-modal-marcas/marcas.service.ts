import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FwModalMarcasComponent } from '../fw-modal-marcas/fw-modal-marcas.component'
import { Fabricante } from 'src/app/Clases/Estructura';

@Injectable({
  providedIn: 'root'
})

export class MarcasService {

  constructor(private modalService: NgbModal) { }

  AbrirModal(tFabricanteActual : Fabricante): Promise<any> {

    return new Promise((resolve, reject) => {

      const modalRef = this.modalService.open(FwModalMarcasComponent, { size: 'lg' });
      modalRef.componentInstance.tTituloModal = "Modal de marcas";
      modalRef.componentInstance.tMensajeModal = "";
      modalRef.componentInstance.tFabricante = tFabricanteActual;

      modalRef.result.then(result => {

        resolve(result);

      }, reason => {

        reject(reason);

      });

    })

  }

}
