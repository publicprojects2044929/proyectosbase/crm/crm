import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwModalMarcasComponent } from './fw-modal-marcas.component';

describe('FwModalMarcasComponent', () => {
  let component: FwModalMarcasComponent;
  let fixture: ComponentFixture<FwModalMarcasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwModalMarcasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwModalMarcasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
