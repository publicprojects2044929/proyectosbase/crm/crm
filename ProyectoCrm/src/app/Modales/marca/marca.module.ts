import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/Shared/shared.module'
import { NgbModule, NgbActiveModal, NgbAlertConfig } from '@ng-bootstrap/ng-bootstrap';

import { FwModalMarcasComponent } from 'src/app/Modales/marca/fw-modal-marcas/fw-modal-marcas.component'

@NgModule({
  declarations: [
    FwModalMarcasComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    SharedModule
  ],
  exports: [
    FwModalMarcasComponent
  ],
  entryComponents: [
    FwModalMarcasComponent
  ],
  providers: [ NgbActiveModal, NgbAlertConfig ]
})
export class MarcaModalModule { }
