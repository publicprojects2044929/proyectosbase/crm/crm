import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/Shared/shared.module'
import { NgbModule, NgbActiveModal, NgbAlertConfig } from '@ng-bootstrap/ng-bootstrap';

import { FwModalAlmacenComponent } from 'src/app/Modales/almacen/fw-modal-almacen/fw-modal-almacen.component'

@NgModule({
  declarations: [
    FwModalAlmacenComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    SharedModule
  ],
  exports: [
    FwModalAlmacenComponent
  ],
  entryComponents: [
    FwModalAlmacenComponent
  ],
  providers: [ NgbActiveModal, NgbAlertConfig ]
})
export class AlmacenModalModule { }
