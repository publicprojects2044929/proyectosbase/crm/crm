import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FwModalAlmacenComponent } from './fw-modal-almacen.component'

@Injectable({
  providedIn: 'root'
})
export class AlmacenService {

  constructor(private modalService: NgbModal) { }

  AbrirModal(): Promise<any> {

    return new Promise((resolve, reject) => {

      const modalRef = this.modalService.open(FwModalAlmacenComponent, { size: 'lg' });
      modalRef.componentInstance.tTituloModal = "Modal de almacén";
      modalRef.componentInstance.tMensajeModal = "";

      modalRef.result.then(result => {

        resolve(result);

      }, reason => {

        reject(reason);

      });

    })

  }
  
}
