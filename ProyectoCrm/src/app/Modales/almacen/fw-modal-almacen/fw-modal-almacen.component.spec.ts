import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwModalAlmacenComponent } from './fw-modal-almacen.component';

describe('FwModalAlmacenComponent', () => {
  let component: FwModalAlmacenComponent;
  let fixture: ComponentFixture<FwModalAlmacenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwModalAlmacenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwModalAlmacenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
