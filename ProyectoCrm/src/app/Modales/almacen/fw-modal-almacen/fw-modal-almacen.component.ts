import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Usuario, Empresas, Filtros, Almacen } from 'src/app/Clases/Estructura';
import { SessionService } from 'src/app/Core/Session/session.service';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { ErroresService } from '../../mensajes/fw-modal-msj/errores.service';
import { MensajesService } from '../../mensajes/fw-modal-msj/mensajes.service';
import { environment } from 'src/environments/environment';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-fw-modal-almacen',
  templateUrl: './fw-modal-almacen.component.html',
  styleUrls: ['./fw-modal-almacen.component.scss']
})

export class FwModalAlmacenComponent implements OnInit, OnDestroy {

  @Input() tTituloModal: string;
  @Input() tMensajeModal: string;

  tPosicion: number = 0;
  tPagina: number = 1;
  tTamanoPag: number = 5;

  tUsuarioActual: Usuario = new Usuario();
  tEmpresaActual: Empresas = new Empresas();
  tEmpresa$: Subscription;
  tHayConfiguracionWs$: Subscription;

  tFiltros: Filtros[] = [];
  tFiltroActual: Filtros = new Filtros();
  tFiltroBusqueda: string = "";

  tAlmacenes: Almacen[] = [];

  constructor(public activeModal: NgbActiveModal,
    private tSession: SessionService,
    public Ws: ClsServiciosService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService) {

    this.tFiltros = [

      { Codfiltro: "Codalmacen", Filtro: "Código" },
      { Codfiltro: "Descripcion", Filtro: "Descripción" },

    ]

  }

  ngOnInit() {

    this.tEmpresa$ = this.tSession.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    })

    this.tFiltroBusqueda = "";

    this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
      .subscribe((tHayConfig: boolean) => {

        if (tHayConfig === true) {

          this.BuscarAlmacenes();

        }

      });

  }

  ngOnDestroy(): void {

    this.tEmpresa$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();

  }

  BuscarAlmacenes(): void {

    this.tAlmacenes = [];
    this.Ws
      .Almacen()
      .Consumir_Obtener_Almacen(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tAlmacenes = Respuesta.Datos;
          this.tFiltroActual = this.tFiltros[0];

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

}
