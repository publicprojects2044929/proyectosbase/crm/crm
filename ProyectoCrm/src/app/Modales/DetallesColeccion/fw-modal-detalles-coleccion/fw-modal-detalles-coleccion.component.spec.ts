import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwModalDetallesColeccionComponent } from './fw-modal-detalles-coleccion.component';

describe('FwModalDetallesColeccionComponent', () => {
  let component: FwModalDetallesColeccionComponent;
  let fixture: ComponentFixture<FwModalDetallesColeccionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwModalDetallesColeccionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwModalDetallesColeccionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
