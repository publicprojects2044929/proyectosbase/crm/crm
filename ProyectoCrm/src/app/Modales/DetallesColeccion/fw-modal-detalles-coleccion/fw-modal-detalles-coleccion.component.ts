import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { ErroresService } from '../../mensajes/fw-modal-msj/errores.service';
import { MensajesService } from '../../mensajes/fw-modal-msj/mensajes.service';
import { SessionService } from 'src/app/Core/Session/session.service';
import { environment } from 'src/environments/environment';
import { PedidoColecciones, Usuario, Empresas, Filtros, PedidoArticulosColecciones } from 'src/app/Clases/Estructura';
import { PedidoColeccionesEntrada, PedidoArticulosColeccionesEntrada } from 'src/app/Clases/EstructuraEntrada';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-fw-modal-detalles-coleccion',
  templateUrl: './fw-modal-detalles-coleccion.component.html',
  styleUrls: ['./fw-modal-detalles-coleccion.component.scss']
})
export class FwModalDetallesColeccionComponent implements OnInit, OnDestroy {

  @Input() tTituloModal: string;
  @Input() tMensajeModal: string;
  @Input() tColeccionModal: PedidoColecciones;

  tPosicion: number = 0;
  tPagina: number = 1;
  tTamanoPag: number = 5;

  tUsuarioActual: Usuario = new Usuario();
  tEmpresaActual: Empresas = new Empresas();
  tEmpresa$: Subscription;
  tHayConfiguracionWs$: Subscription;

  tFiltros: Filtros[] = [];
  tFiltroActual: Filtros = new Filtros();
  tFiltroBusqueda: string = "";

  tColeccionActual: PedidoColecciones = new PedidoColecciones();

  constructor(public activeModal: NgbActiveModal,
    private toastr: ToastrService,
    private tSession: SessionService,
    public Ws: ClsServiciosService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService) {

    this.tFiltros = [

      { Codfiltro: "Codpais", Filtro: "País" },
      { Codfiltro: "Codestado", Filtro: "Estado" },
      { Codfiltro: "Descripcion", Filtro: "Descripción" },

    ]

  }

  ngOnInit() {

    this.tEmpresa$ = this.tSession.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    })

    this.tFiltroBusqueda = "";

    this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
      .subscribe((tHayConfig: boolean) => {

        if (tHayConfig === true) {

          this.Ws.PedidoColeccion()
            .Consumir_Obtener_PedidoColeccionesFull(this.tEmpresaActual.Corporacion,
              this.tEmpresaActual.Empresa,
              this.tColeccionModal.Codpedido,
              this.tColeccionModal.Codcoleccion)
            .subscribe(Respuesta => {

              if (Respuesta.Resultado == "N") {


              }
              else if (Respuesta.Resultado == "E") {

                this.tMsjServices.AbrirModalMsj(environment.msjError);

              }
              else {

                this.tColeccionActual = Respuesta.Datos[0];

              }

            }, error => {

              this.tErroresServices.MostrarError(error);

            });

        }

      });



  }

  ngOnDestroy() {

    this.tEmpresa$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();


  }

  EliminarColeccion() {

    let tColeccionEliminar: PedidoColeccionesEntrada = {

      "Id": this.tColeccionActual.Id,
      "Corporacion": this.tColeccionActual.Corporacion,
      "Empresa": this.tColeccionActual.Empresa,
      "Codpedido": this.tColeccionActual.Codpedido,
      "Codcoleccion": this.tColeccionActual.Codcoleccion,
      "Usuariocreador": this.tColeccionActual.Usuariocreador,
      "Usuariomodificador": this.tUsuarioActual.Codusuario,
      "LArticulos": []

    };

    this.tColeccionActual.LArticulos.forEach(tArticulo => {

      let tArticuloColeccion: PedidoArticulosColeccionesEntrada = {

        "Id": tArticulo.Id,
        "Corporacion": tArticulo.Corporacion,
        "Empresa": tArticulo.Empresa,
        "Codpedido": tArticulo.Codpedido,
        "Codcoleccion": tArticulo.Codcoleccion,
        "Codarticulo": tArticulo.Codarticulo,
        "Talla": tArticulo.Talla,
        "Usuariocreador": tArticulo.Usuariocreador,
        "Usuariomodificador": this.tUsuarioActual.Codusuario

      }

      tColeccionEliminar.LArticulos.push(tArticulo);

    });

    this.Ws.PedidoColeccion()
      .Consumir_Eliminar_PedidoColecciones(tColeccionEliminar)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.toastr.clear();
          this.toastr.success("La colección se elimino del pedido.");
          this.activeModal.close({ 'Resultado': 'S', 'Coleccion': this.tColeccionModal });


        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

}
