import { Injectable } from '@angular/core';
import { PedidoColecciones } from 'src/app/Clases/Estructura';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FwModalDetallesColeccionComponent } from './fw-modal-detalles-coleccion.component';

@Injectable({
  providedIn: 'root'
})
export class ModalDetallesService {

  constructor(private modalService: NgbModal) { }

  AbrirModal(tPedidoColeccion: PedidoColecciones): Promise<any> {

    return new Promise((resolve, reject) => {

      const modalRef = this.modalService.open(FwModalDetallesColeccionComponent, { size: 'xl' });
      modalRef.componentInstance.tTituloModal = "Colecciones";
      modalRef.componentInstance.tMensajeModal = "";
      modalRef.componentInstance.tColeccionModal = tPedidoColeccion;

      modalRef.result.then(result => {

        resolve(result);

      }, reason => {

        reject(reason);

      });

    })

  }

}
