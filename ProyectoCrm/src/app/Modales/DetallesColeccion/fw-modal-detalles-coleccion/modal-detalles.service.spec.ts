import { TestBed } from '@angular/core/testing';

import { ModalDetallesService } from './modal-detalles.service';

describe('ModalDetallesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ModalDetallesService = TestBed.get(ModalDetallesService);
    expect(service).toBeTruthy();
  });
});
