import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/Shared/shared.module'
import { NgbModule, NgbActiveModal, NgbAlertConfig } from '@ng-bootstrap/ng-bootstrap';

import { FwModalDetallesColeccionComponent } from './fw-modal-detalles-coleccion/fw-modal-detalles-coleccion.component';

@NgModule({
  declarations: [
    FwModalDetallesColeccionComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    SharedModule
  ],
  exports: [
    FwModalDetallesColeccionComponent
  ],
  entryComponents: [
    FwModalDetallesColeccionComponent
  ],
  providers: [ NgbActiveModal, NgbAlertConfig ]
})
export class DetallesColeccionModule { }
