import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/Shared/shared.module'
import { NgbModule, NgbActiveModal, NgbAlertConfig } from '@ng-bootstrap/ng-bootstrap';
import { FwModalGruposComponent } from './fw-modal-grupos/fw-modal-grupos.component';

@NgModule({
  declarations: [
    FwModalGruposComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    SharedModule
  ],
  exports: [
    FwModalGruposComponent
  ],
  entryComponents: [
    FwModalGruposComponent
  ],
  providers: [ NgbActiveModal, NgbAlertConfig ]
})
export class GruposModalModule { }
