import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwModalGruposComponent } from './fw-modal-grupos.component';

describe('FwModalGruposComponent', () => {
  let component: FwModalGruposComponent;
  let fixture: ComponentFixture<FwModalGruposComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwModalGruposComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwModalGruposComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
