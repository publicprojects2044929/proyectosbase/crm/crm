import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/Shared/shared.module'
import { NgbModule, NgbActiveModal, NgbAlertConfig } from '@ng-bootstrap/ng-bootstrap';

import { FwModalDetallesVentaComponent } from 'src/app/Modales/detalles-venta/fw-modal-detalles-venta/fw-modal-detalles-venta.component'

@NgModule({
  declarations: [
    FwModalDetallesVentaComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    SharedModule
  ],
  exports: [
    FwModalDetallesVentaComponent
  ],
  entryComponents: [
    FwModalDetallesVentaComponent
  ],
  providers: [ NgbActiveModal, NgbAlertConfig ]
})
export class DetallesVentaModalModule { }
