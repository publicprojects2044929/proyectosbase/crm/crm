import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FwModalDetallesVentaComponent } from '../fw-modal-detalles-venta/fw-modal-detalles-venta.component'
import { Venta } from 'src/app/Clases/Estructura';


@Injectable({
  providedIn: 'root'
})

export class DetallesVentaService {

  constructor(private modalService: NgbModal) { }

  AbrirModalConf(tVenta: Venta): Promise<string> {

    return new Promise((resolve, reject) => {

      const modalRef = this.modalService.open(FwModalDetallesVentaComponent, { size: 'xl' });
      modalRef.componentInstance.tTituloModal = " Detalles de la venta";
      modalRef.componentInstance.tMensajeModal = "";
      modalRef.componentInstance.tVentaModal = tVenta;

      modalRef.result.then(result => {

        resolve(result);

      }, reason => {

        reject(reason);

      });

    })

  }


}
