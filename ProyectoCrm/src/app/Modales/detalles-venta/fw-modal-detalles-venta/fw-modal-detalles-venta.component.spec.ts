import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwModalDetallesVentaComponent } from './fw-modal-detalles-venta.component';

describe('FwModalDetallesVentaComponent', () => {
  let component: FwModalDetallesVentaComponent;
  let fixture: ComponentFixture<FwModalDetallesVentaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwModalDetallesVentaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwModalDetallesVentaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
