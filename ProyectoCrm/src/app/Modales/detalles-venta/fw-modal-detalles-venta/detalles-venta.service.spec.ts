import { TestBed } from '@angular/core/testing';

import { DetallesVentaService } from './detalles-venta.service';

describe('DetallesVentaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DetallesVentaService = TestBed.get(DetallesVentaService);
    expect(service).toBeTruthy();
  });
});
