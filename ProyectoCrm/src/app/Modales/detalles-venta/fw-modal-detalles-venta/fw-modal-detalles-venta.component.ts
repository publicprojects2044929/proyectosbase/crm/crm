import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { VentaFormasPagos, Venta, Reportes, Empresas, Usuario } from 'src/app/Clases/Estructura';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { VentaFormasPagosEntrada } from 'src/app/Clases/EstructuraEntrada';

import { MensajesService } from 'src/app/Modales/mensajes/fw-modal-msj/mensajes.service';
import { ErroresService } from 'src/app/Modales/mensajes/fw-modal-msj/errores.service';
import { ConfirmacionService } from 'src/app/Modales/confirmacion/fw-modal-confirmacion/confirmacion.service';
import { ConfirmacionRechazoPagoService } from 'src/app/Modales/confirmacion-rechazo-pago/fw-modal-confirmacion-rehazo-pago/confirmacion-rechazo-pago.service';
import { environment } from 'src/environments/environment';
import { Subscription } from 'rxjs';
import { ManejarPdf } from 'src/app/Manejadores/cls-procedimientos';
import { SessionService } from 'src/app/Core/Session/session.service';

@Component({
  selector: 'app-fw-modal-detalles-venta',
  templateUrl: './fw-modal-detalles-venta.component.html',
  styleUrls: ['./fw-modal-detalles-venta.component.scss']
})
export class FwModalDetallesVentaComponent implements OnInit, OnDestroy {

  @Input() tTituloModal: string = "";
  @Input() tMensajeModal: string = "";
  @Input() tVentaModal: Venta = new Venta();

  tVFormapagos: VentaFormasPagos[] = [];
  tVFormasPagosActual: VentaFormasPagos = new VentaFormasPagos();
  tPosicion: number = 0;
  tPagina: number = 1;
  tTamanoPag: number = 10;
  tEstatusPago: boolean = false;

  tUsuarioActual: Usuario = new Usuario();
  tUsuario$: Subscription;
  tEmpresaActual: Empresas = new Empresas();
  tEmpresa$: Subscription;
  
  tHayConfiguracionWs$: Subscription;
  tManejarPdf: ManejarPdf = new ManejarPdf();

  constructor(public activeModal: NgbActiveModal,
    public Ws: ClsServiciosService,
    private tErroresServices: ErroresService,
    private tSession: SessionService,
    private tConfServices: ConfirmacionService,
    private tConfRechazoService: ConfirmacionRechazoPagoService,
    private tMsjServices: MensajesService) { }

  ngOnInit() {

    this.tEmpresa$ = this.tSession.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    });

    this.tUsuario$ = this.tSession.tUsuarioActual$.subscribe((tUsuario: Usuario) => {

      this.tUsuarioActual = tUsuario;

    });
    this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
      .subscribe((tHayConfig: boolean) => {

        if (tHayConfig === true) {

          this.BuscarVentaFormasPagos();

        }

      });

  }

  ngOnDestroy() {

    this.tHayConfiguracionWs$.unsubscribe();

  }

  BuscarVentaFormasPagos() {

    this.tVFormapagos = [];

    this.Ws.VentaFormasPagos()
      .Consumir_Obtener_VentaFormasPagos(this.tVentaModal.Corporacion,
        this.tVentaModal.Empresa,
        this.tVentaModal.Codventa)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          Respuesta.Mensaje;

        }

        if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tVFormapagos = Respuesta.Datos;


        }


      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  AbrirReporte(tFormaPagoSel: VentaFormasPagos) {

    this.Ws.VentaFormasPagos()
      .Consumir_Obtener_VentaFormasPagosRpt(tFormaPagoSel.Corporacion,
        tFormaPagoSel.Empresa,
        tFormaPagoSel.Codventa,
        tFormaPagoSel.Codformapago,
        tFormaPagoSel.Item)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {



        }

        if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          var Reporte: Reportes = Respuesta.Datos[0];
          this.tManejarPdf.AbrirPdf(Reporte);

        }


      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  RechazarFormaPago(tPago_Sel: VentaFormasPagosEntrada) {

    this.tConfRechazoService.AbrirModalConf("¿Desea rechazar la Forma de pago seleccionada?", tPago_Sel).then(Resultado => {

      switch (Resultado) {

        case "S": {
          this.BuscarVentaFormasPagos();

        }

      };

    });


  }

  AprobarFormaPago(tPago_Sel: VentaFormasPagosEntrada) {

    this.tConfServices.AbrirModalConf("¿Desea aprobar la Forma de pago seleccionada?").then(Resultado => {

      switch (Resultado) {

        case "S": {

          this.Ws.VentaFormasPagos()
            .Aprobar_VentaFormasPagos(tPago_Sel)
            .subscribe(Respuesta => {


              if (Respuesta.Resultado == "N") {

                Respuesta.Mensaje;

              }

              if (Respuesta.Resultado == "E") {

                this.tMsjServices.AbrirModalMsj(environment.msjError);

              }
              else {

                this.tVFormapagos = Respuesta.Datos;

              }


            }, error => {

              this.tErroresServices.MostrarError(error);

            });

          break;

        }

      };

    });


  }


}
