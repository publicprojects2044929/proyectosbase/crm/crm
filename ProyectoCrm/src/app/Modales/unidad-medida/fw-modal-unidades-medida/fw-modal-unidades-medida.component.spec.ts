import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwModalUnidadesMedidaComponent } from './fw-modal-unidades-medida.component';

describe('FwModalUnidadesMedidaComponent', () => {
  let component: FwModalUnidadesMedidaComponent;
  let fixture: ComponentFixture<FwModalUnidadesMedidaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwModalUnidadesMedidaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwModalUnidadesMedidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
