import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FwModalUnidadesMedidaComponent } from '../fw-modal-unidades-medida/fw-modal-unidades-medida.component'

@Injectable({
  providedIn: 'root'
})
export class UnidadesMedidaService {

  constructor(private modalService: NgbModal) { }

  AbrirModal(): Promise<any> {

    return new Promise((resolve, reject) => {

      const modalRef = this.modalService.open(FwModalUnidadesMedidaComponent, { size: 'lg' });
      modalRef.componentInstance.tTituloModal = "Modal unidades de medida";
      modalRef.componentInstance.tMensajeModal = "";

      modalRef.result.then(result => {

        resolve(result);

      }, reason => {

        reject(reason);

      });

    })

  }

}
