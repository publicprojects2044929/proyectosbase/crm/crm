import { TestBed } from '@angular/core/testing';

import { UnidadesMedidaService } from './unidades-medida.service';

describe('UnidadesMedidaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UnidadesMedidaService = TestBed.get(UnidadesMedidaService);
    expect(service).toBeTruthy();
  });
});
