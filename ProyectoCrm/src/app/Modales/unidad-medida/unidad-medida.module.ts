import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/Shared/shared.module'
import { NgbModule, NgbActiveModal, NgbAlertConfig } from '@ng-bootstrap/ng-bootstrap';

import { FwModalUnidadesMedidaComponent } from 'src/app/Modales/unidad-medida/fw-modal-unidades-medida/fw-modal-unidades-medida.component'

@NgModule({
  declarations: [
    FwModalUnidadesMedidaComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    SharedModule
  ],
  exports: [
    FwModalUnidadesMedidaComponent
  ],
  entryComponents: [
    FwModalUnidadesMedidaComponent
  ],
  providers: [ NgbActiveModal, NgbAlertConfig ]
})
export class UnidadMedidaModalModule { }
