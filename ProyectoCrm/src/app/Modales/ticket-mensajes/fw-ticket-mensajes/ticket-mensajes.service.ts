import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FwTicketMensajesComponent } from '../fw-ticket-mensajes/fw-ticket-mensajes.component';
import { Ticket } from 'src/app/Clases/Estructura';

@Injectable({
  providedIn: 'root'
})
export class TicketMensajesService {

  constructor(private modalService: NgbModal) { }

  AbrirModal(tTicket: Ticket): Promise<any> {

    return new Promise((resolve, reject) => {

      const modalRef = this.modalService.open(FwTicketMensajesComponent, { size: 'xl' });
      modalRef.componentInstance.tTituloModal = "";
      modalRef.componentInstance.tMensajeModal = "";
      modalRef.componentInstance.tTicket = tTicket;

      modalRef.result.then(result => {

        resolve(result);

      }, reason => {

        reject(reason);

      });

    })

  }

}
