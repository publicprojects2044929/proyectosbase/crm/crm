import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwTicketMensajesComponent } from './fw-ticket-mensajes.component';

describe('FwTicketMensajesComponent', () => {
  let component: FwTicketMensajesComponent;
  let fixture: ComponentFixture<FwTicketMensajesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwTicketMensajesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwTicketMensajesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
