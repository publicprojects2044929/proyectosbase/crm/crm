import { TestBed } from '@angular/core/testing';

import { TicketMensajesService } from './ticket-mensajes.service';

describe('TicketMensajesService', () => {
  let service: TicketMensajesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TicketMensajesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
