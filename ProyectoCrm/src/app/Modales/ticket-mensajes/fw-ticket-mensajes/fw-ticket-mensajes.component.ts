import { Component, OnInit, Input } from '@angular/core';
import { Ticket, Cliente, TicketMensajes, Usuario, Empresas, Respuesta } from 'src/app/Clases/Estructura';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { ErroresService } from '../../mensajes/fw-modal-msj/errores.service';
import { MensajesService } from '../../mensajes/fw-modal-msj/mensajes.service';
import { environment } from 'src/environments/environment';
import { Subscription } from 'rxjs';
import { SessionService } from 'src/app/Core/Session/session.service';
import { MensajeEntrada } from 'src/app/Clases/EstructuraEntrada';
import { insertMensaje } from 'src/app/Core/GraphQL/mutation.service';
import { SocketService } from 'src/app/Core/Socket/socket.service';

@Component({
  selector: 'app-fw-ticket-mensajes',
  templateUrl: './fw-ticket-mensajes.component.html',
  styleUrls: ['./fw-ticket-mensajes.component.scss']
})
export class FwTicketMensajesComponent implements OnInit {

  @Input() tTituloModal: string = "";
  @Input() tMensajeModal: string = "";
  @Input() tTicket: Ticket = new Ticket();

  tCodcliente: string = "";
  tClienteActual: Cliente = new Cliente();
  tNuevoMensaje: MensajeEntrada = new MensajeEntrada();
  LMensajes: TicketMensajes[] = [];

  tUsuarioActual: Usuario = new Usuario();
  tEmpresaActual: Empresas = new Empresas();
  tUsuario$: Subscription;
  tEmpresa$: Subscription;

  tHayConfiguracionWs$: Subscription;
  tHayConfiguracionSocket$: Subscription;
  tSocketTickets$: Subscription;
  // tMensajesQuery$: Subscription;

  constructor(public activeModal: NgbActiveModal,
    public Ws: ClsServiciosService,
    private tSesion: SessionService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService,
    private tMensajeMutation: insertMensaje,
    private tSocket: SocketService) { }

  ngOnInit(): void {

    this.tCodcliente = this.tTicket.Codcliente;

    this.tUsuario$ = this.tSesion.tUsuarioActual$.subscribe((tUsuario: Usuario) => {

      this.tUsuarioActual = tUsuario;

    });

    this.tEmpresa$ = this.tSesion.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    });

    this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
      .subscribe((tHayConfig: boolean) => {

        if (tHayConfig === true) {

          this.Buscar_Cliente();
          this.Buscar_Ticket();

        }

      });

    this.tHayConfiguracionSocket$ = this.tSocket.tHayConfig$
      .subscribe((tHayConfig: boolean) => {

        if (tHayConfig === true) {

          this.tSocket.emit("Ticket:TicketMensajes", this.tTicket.Codticket);

          this.tSocketTickets$ = this.tSocket
            .listen("Ticket:TicketMensajes")
            .subscribe((tRespuesta: TicketMensajes[]) => {
              this.LMensajes = tRespuesta;
            });

        }

      });

  }

  ngOnDestroy() {

    this.tHayConfiguracionWs$.unsubscribe();
    this.tHayConfiguracionSocket$.unsubscribe();
    this.tSocketTickets$.unsubscribe();
    this.tUsuario$.unsubscribe();
    this.tEmpresa$.unsubscribe();
    this.tSocket.disconnect();

  }

  Buscar_Cliente(): void {

    this.Ws
      .Cliente()
      .Consumir_Obtener_ClientesDetallado(this.tTicket.Corporacion,
        this.tTicket.Empresa,
        this.tTicket.Codcliente)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado === "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado === "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tClienteActual = Respuesta.Datos[0];

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Buscar_Ticket(): void {

    this.Ws
      .Ticket()
      .Consumir_Obtener_Ticket_Full(this.tTicket.Corporacion,
        this.tTicket.Empresa,
        this.tTicket.Codticket)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado === "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado === "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tTicket = Respuesta.Datos[0];

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Crear_Mensaje() {

    if (this.tNuevoMensaje.Texto.trim() === '') { return };

    this.tNuevoMensaje.Corporacion = this.tTicket.Corporacion;
    this.tNuevoMensaje.Empresa = this.tTicket.Empresa;
    this.tNuevoMensaje.Codticket = this.tTicket.Codticket;
    this.tNuevoMensaje.De = this.tUsuarioActual.Codusuario;
    this.tNuevoMensaje.Texto = this.tNuevoMensaje.Texto.trim();

    this.tMensajeMutation
      .mutate({
        Mensaje: this.tNuevoMensaje,
      })
      .subscribe(tResultado => {
        this.tNuevoMensaje = new MensajeEntrada();
      }, error => {

      });

  }

}
