import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule, NgbActiveModal, NgbAlertConfig } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from 'src/app/Shared/shared.module';
import { FwTicketMensajesComponent } from './fw-ticket-mensajes/fw-ticket-mensajes.component';

@NgModule({
  declarations: [
    FwTicketMensajesComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    SharedModule
  ],
  exports: [
    FwTicketMensajesComponent
  ],
  entryComponents: [
    FwTicketMensajesComponent
  ],
  providers: [ NgbActiveModal, NgbAlertConfig ]
})
export class TicketMensajesModule { }
