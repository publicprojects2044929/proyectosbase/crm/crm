import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgbModule, NgbActiveModal, NgbAlertConfig } from '@ng-bootstrap/ng-bootstrap';

//Modales
import { FwModalMsjComponent } from 'src/app/Modales/mensajes/fw-modal-msj/fw-modal-msj.component'

@NgModule({
  declarations: [
    FwModalMsjComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
  ],
  exports: [
    FwModalMsjComponent
  ],
  entryComponents: [
    FwModalMsjComponent
  ],
  providers: [ NgbActiveModal, NgbAlertConfig ]
})
export class MensajesModule { }
