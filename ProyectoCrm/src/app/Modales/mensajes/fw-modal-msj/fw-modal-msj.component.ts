import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-fw-modal-msj',
  templateUrl: './fw-modal-msj.component.html',
  styleUrls: ['./fw-modal-msj.component.scss']
})
export class FwModalMsjComponent implements OnInit {

  @Input() tTituloModal: string;
  @Input() tMensajeModal: string;

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

}
