import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwModalMsjComponent } from './fw-modal-msj.component';

describe('FwModalMsjComponent', () => {
  let component: FwModalMsjComponent;
  let fixture: ComponentFixture<FwModalMsjComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwModalMsjComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwModalMsjComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
