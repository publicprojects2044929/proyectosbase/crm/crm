import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FwModalMsjComponent } from '../fw-modal-msj/fw-modal-msj.component'

@Injectable({
  providedIn: 'root'
})

export class ErroresService {

  public tTituloMsj: string = "";
  private tMensajeMsj: string = "";

  constructor(private modalService: NgbModal) { }

  MostrarError(tError: any) : void{

    this.tMensajeMsj = tError.message;
    this.AbrirModalMsj();

  }

  private AbrirModalMsj() : void {

    const modalRef = this.modalService.open(FwModalMsjComponent);
    modalRef.componentInstance.tTituloModal = this.tTituloMsj;
    modalRef.componentInstance.tMensajeModal = this.tMensajeMsj;

  }

}
