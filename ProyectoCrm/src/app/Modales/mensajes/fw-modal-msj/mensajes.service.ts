import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FwModalMsjComponent } from '../fw-modal-msj/fw-modal-msj.component'

@Injectable({
  providedIn: 'root'
})
export class MensajesService {

  public tTituloMsj: string = "";

  constructor(private modalService: NgbModal) { }

  AbrirModalMsj(tMsj : string) : void {

    const modalRef = this.modalService.open(FwModalMsjComponent);
    modalRef.componentInstance.tTituloModal = this.tTituloMsj;
    modalRef.componentInstance.tMensajeModal = tMsj;

  }

}
