import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/Shared/shared.module'
import { NgbModule, NgbActiveModal, NgbAlertConfig } from '@ng-bootstrap/ng-bootstrap';

import { FwModalAjusteInventarioPorArchivoComponent } from 'src/app/Modales/AjusteInventarioPorArchivo/fw-modal-ajuste-inventario-por-archivo/fw-modal-ajuste-inventario-por-archivo.component';

@NgModule({
  declarations: [
    FwModalAjusteInventarioPorArchivoComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    SharedModule,
    ReactiveFormsModule
  ],
  exports: [
    FwModalAjusteInventarioPorArchivoComponent
  ],
  entryComponents: [
    FwModalAjusteInventarioPorArchivoComponent
  ],
  providers: [ NgbActiveModal, NgbAlertConfig ]
})
export class AjusteInventarioPorArchivoModalModule { }
