import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FwModalAjusteInventarioPorArchivoComponent } from '../fw-modal-ajuste-inventario-por-archivo/fw-modal-ajuste-inventario-por-archivo.component'
import { TipoAjustesInventario, Almacen } from 'src/app/Clases/Estructura';

@Injectable({
  providedIn: 'root'
})
export class AjusteInventarioPorArchivoService {

  constructor(private modalService: NgbModal) { }

  AbrirModal(tTiposAjustesInv: TipoAjustesInventario[], tAlmacenes: Almacen[]): Promise<any> {

    return new Promise((resolve, reject) => {

      const modalRef = this.modalService.open(FwModalAjusteInventarioPorArchivoComponent, { size: 'xl' });
      modalRef.componentInstance.tTituloModal = "Ajuste de inventario por archivo";
      modalRef.componentInstance.tMensajeModal = "";
      modalRef.componentInstance.tTiposAjustesInv = tTiposAjustesInv;
      modalRef.componentInstance.tAlmacenes = tAlmacenes;

      modalRef.result.then(result => {

        resolve(result);

      }, reason => {

        reject(reason);

      });

    })

  }

}
