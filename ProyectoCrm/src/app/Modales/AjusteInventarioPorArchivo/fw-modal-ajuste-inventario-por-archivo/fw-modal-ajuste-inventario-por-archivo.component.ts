import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { TipoAjustesInventario, Usuario, Empresas, Filtros, Almacen } from 'src/app/Clases/Estructura';
import { AjusteInventarioEntrada } from 'src/app/Clases/EstructuraEntrada';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SessionService } from 'src/app/Core/Session/session.service';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { ToastrService } from 'ngx-toastr';
import { ConfirmacionService } from '../../confirmacion/fw-modal-confirmacion/confirmacion.service';
import { isNumber } from 'util';
import { ManejarArchivo } from 'src/app/Manejadores/cls-procedimientos';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-fw-modal-ajuste-inventario-por-archivo',
  templateUrl: './fw-modal-ajuste-inventario-por-archivo.component.html',
  styleUrls: ['./fw-modal-ajuste-inventario-por-archivo.component.scss']
})

export class FwModalAjusteInventarioPorArchivoComponent implements OnInit, OnDestroy {

  @Input() tTituloModal: string = "";
  @Input() tMensajeModal: string = "";
  @Input() tTiposAjustesInv: TipoAjustesInventario[] = [];
  @Input() tAlmacenes: Almacen[] = [];


  tPosicion: number = 0;
  tPagina: number = 1;
  tTamanoPag: number = 5;

  tUsuarioActual: Usuario = new Usuario;
  tEmpresaActual: Empresas = new Empresas;

  tArticulosAjuste: AjusteInventarioEntrada[] = [];

  tArchivoCargado: boolean = false;

  tFiltros: Filtros[] = [];
  tFiltroActual: Filtros = new Filtros();
  tFiltroBusqueda: string = "";

  tExcel: ManejarArchivo = new ManejarArchivo();
  tEmpresa$: Subscription;
  tUsuario$: Subscription;
  tHayConfiguracionWs$: Subscription;

  constructor(public activeModal: NgbActiveModal,
    private tSession: SessionService,
    private Ws: ClsServiciosService,
    private toastr: ToastrService,
    private tConfServices: ConfirmacionService) { }

  ngOnInit() {

    this.tEmpresa$ = this.tSession.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    });

    this.tUsuario$ = this.tSession.tUsuarioActual$.subscribe((tUsuario: Usuario) => {

      this.tUsuarioActual = tUsuario;

    });


    this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
      .subscribe((tHayConfig: boolean) => {

        if (tHayConfig === true) {



        }

      });


    this.tArchivoCargado = false;

    this.tFiltros = [

      { Codfiltro: "Codarticulo", Filtro: "Artículo" },
      { Codfiltro: "Descripcion", Filtro: "Nombre" },

    ]


  }

  ngOnDestroy(): void {

    this.tEmpresa$.unsubscribe();
    this.tUsuario$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();

  }

  Cancelar(): void {

    this.tArchivoCargado = false;

  }

  Cargar(): void {

    this.tConfServices.AbrirModalConf("").then(Resultado => {

      switch (Resultado) {

        case "S": {

          // this.Nuevo_Articulo();
          break;

        }

      };

    });

  }

  CargarExcel(tArchivo: any): void {

    this.tArticulosAjuste = [];
    let tAjustesTemp: any = [];
    this.tExcel.Cargar_Archivo(tArchivo);

    this.tExcel
      .ObtenerDatos()
      .subscribe(Datos => {

        if (Datos["Ajuste"] !== undefined) {

          let tHoja: any = Datos.Ajuste;
          tHoja.map((tLinea: AjusteInventarioEntrada, tIndex: number) => {

            let tCodTipoAjuste: string = tLinea["Codtipoajusteinv"] || tLinea["CODTIPOAJUSTEINV"];
            let tCodarticulo: string = tLinea["Codarticulo"] || tLinea["CODARTICULO"];
            let tTalla: string = tLinea["Talla"] || tLinea["TALLA"];
            let tCodalmacen: string = tLinea["Codalmacen"] || tLinea["Codalmacen"];
            let tCantidad: number = tLinea["Cantidad"] || tLinea["CANTIDAD"];

            if (tCodTipoAjuste === undefined) {

              this.toastr.clear();
              this.toastr.warning("Falta el tipo de ajuste. Linea: " + (tIndex + 1));
              return;

            }

            if (tCodarticulo === undefined) {

              this.toastr.clear();
              this.toastr.warning("Falta el articulo. Linea: " + (tIndex + 1));
              return;

            }

            if (tTalla === undefined) {

              tTalla = "";

            }

            if (tCodalmacen === undefined) {

              this.toastr.clear();
              this.toastr.warning("Falta el almacén. Linea: " + (tIndex + 1));
              return;

            }


            if (tCantidad === undefined) {

              this.toastr.clear();
              this.toastr.warning("Falta la cantidad. Linea: " + (tIndex + 1));
              return;

            }

            if (isNumber(tCantidad) === false) {

              this.toastr.clear();
              this.toastr.warning("La cantidad esta mal registrada. Linea: " + (tIndex + 1));
              return;

            }

            if (tCantidad <= 0) {

              this.toastr.clear();
              this.toastr.warning("La cantidad del ajuste tiene que ser un número positivo y mayor a cero. Linea: " + (tIndex + 1));
              return;

            }

            let tAjusteInv: AjusteInventarioEntrada = new AjusteInventarioEntrada();
            tAjusteInv.Corporacion = this.tEmpresaActual.Corporacion;
            tAjusteInv.Empresa = this.tEmpresaActual.Empresa;
            tAjusteInv.Codtipoajusteinv = tCodTipoAjuste;
            tAjusteInv.Codarticulo = tCodarticulo;
            tAjusteInv.Talla = tTalla;
            tAjusteInv.Codalmacen = tCodalmacen;
            tAjusteInv.Cantidad = tCantidad;
            tAjusteInv.Usuariocreador = this.tUsuarioActual.Codusuario;
            tAjusteInv.Usuariomodificador = this.tUsuarioActual.Codusuario;
            tAjustesTemp.push(tAjusteInv);

          });

          this.tArticulosAjuste = tAjustesTemp;
          this.tArchivoCargado = true;
          this.tFiltroActual = this.tFiltros[0];
          this.tFiltroBusqueda = "";

        }
        else {

          this.toastr.clear();
          this.toastr.warning("Los datos del archivo no fueron encontrado. Falta hoja de Ajuste");

        }

      });

  }

}
