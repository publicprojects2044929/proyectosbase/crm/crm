import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwModalAjusteInventarioPorArchivoComponent } from './fw-modal-ajuste-inventario-por-archivo.component';

describe('FwModalAjusteInventarioPorArchivoComponent', () => {
  let component: FwModalAjusteInventarioPorArchivoComponent;
  let fixture: ComponentFixture<FwModalAjusteInventarioPorArchivoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwModalAjusteInventarioPorArchivoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwModalAjusteInventarioPorArchivoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
