import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwModalEstadoComponent } from './fw-modal-estado.component';

describe('FwModalEstadoComponent', () => {
  let component: FwModalEstadoComponent;
  let fixture: ComponentFixture<FwModalEstadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwModalEstadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwModalEstadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
