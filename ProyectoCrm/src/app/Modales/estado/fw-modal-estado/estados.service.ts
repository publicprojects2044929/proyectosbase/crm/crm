import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FwModalEstadoComponent } from '../fw-modal-estado/fw-modal-estado.component'
import { Pais } from 'src/app/Clases/Estructura';

@Injectable({
  providedIn: 'root'
})
export class EstadosService {

  constructor(private modalService: NgbModal) { }

  AbrirModal(tPaisActual: Pais): Promise<any> {

    return new Promise((resolve, reject) => {

      const modalRef = this.modalService.open(FwModalEstadoComponent, { size: 'lg' });
      modalRef.componentInstance.tTituloModal = "Modal de Estados";
      modalRef.componentInstance.tMensajeModal = "";
      modalRef.componentInstance.tPaisActual = tPaisActual;

      modalRef.result.then(result => {

        resolve(result);

      }, reason => {

        reject(reason);

      });

    })

  }
  
}
