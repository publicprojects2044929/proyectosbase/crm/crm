import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/Shared/shared.module'
import { NgbModule, NgbActiveModal, NgbAlertConfig } from '@ng-bootstrap/ng-bootstrap';

import { FwModalEstadoComponent } from './fw-modal-estado/fw-modal-estado.component';

@NgModule({
  declarations: [
    FwModalEstadoComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    SharedModule
  ],
  exports: [
    FwModalEstadoComponent
  ],
  entryComponents: [
    FwModalEstadoComponent
  ],
  providers: [ NgbActiveModal, NgbAlertConfig ]
})
export class EstadoModalModule { }
