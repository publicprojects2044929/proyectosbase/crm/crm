import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/Shared/shared.module'
import { NgbModule, NgbActiveModal, NgbAlertConfig } from '@ng-bootstrap/ng-bootstrap';

import { FwModalFabricantesComponent } from 'src/app/Modales/fabricante/fw-modal-fabricantes/fw-modal-fabricantes.component'

@NgModule({
  declarations: [
    FwModalFabricantesComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    SharedModule
  ],
  exports: [
    FwModalFabricantesComponent
  ],
  entryComponents: [
    FwModalFabricantesComponent
  ],
  providers: [ NgbActiveModal, NgbAlertConfig ]
})
export class FabricanteModalModule { }
