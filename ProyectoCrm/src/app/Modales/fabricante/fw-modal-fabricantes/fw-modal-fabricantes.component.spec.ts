import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwModalFabricantesComponent } from './fw-modal-fabricantes.component';

describe('FwModalFabricantesComponent', () => {
  let component: FwModalFabricantesComponent;
  let fixture: ComponentFixture<FwModalFabricantesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwModalFabricantesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwModalFabricantesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
