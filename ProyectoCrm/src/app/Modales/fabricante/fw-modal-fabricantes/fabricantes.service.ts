import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FwModalFabricantesComponent } from '../fw-modal-fabricantes/fw-modal-fabricantes.component'

@Injectable({
  providedIn: 'root'
})
export class FabricantesService {

  constructor(private modalService: NgbModal) { }

  AbrirModal(): Promise<any> {

    return new Promise((resolve, reject) => {

      const modalRef = this.modalService.open(FwModalFabricantesComponent, { size: 'lg' });
      modalRef.componentInstance.tTituloModal = "Modal de fabricante";
      modalRef.componentInstance.tMensajeModal = "";

      modalRef.result.then(result => {

        resolve(result);

      }, reason => {

        reject(reason);

      });

    })

  }

}
