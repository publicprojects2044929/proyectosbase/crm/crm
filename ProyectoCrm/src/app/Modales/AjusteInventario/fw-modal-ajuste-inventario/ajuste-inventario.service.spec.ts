import { TestBed } from '@angular/core/testing';

import { AjusteInventarioService } from './ajuste-inventario.service';

describe('AjusteInventarioService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AjusteInventarioService = TestBed.get(AjusteInventarioService);
    expect(service).toBeTruthy();
  });
});
