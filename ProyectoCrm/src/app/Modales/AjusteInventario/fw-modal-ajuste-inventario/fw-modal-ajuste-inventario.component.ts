import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Articulo, TipoAjustesInventario, Usuario, Empresas, Almacen } from 'src/app/Clases/Estructura';
import { AjusteInventarioEntrada } from 'src/app/Clases/EstructuraEntrada';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SessionService } from 'src/app/Core/Session/session.service';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { ToastrService } from 'ngx-toastr';
import { ErroresService } from '../../mensajes/fw-modal-msj/errores.service';
import { MensajesService } from '../../mensajes/fw-modal-msj/mensajes.service';
import { ConfirmacionService } from '../../confirmacion/fw-modal-confirmacion/confirmacion.service';
import { environment } from 'src/environments/environment';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-fw-modal-ajuste-inventario',
  templateUrl: './fw-modal-ajuste-inventario.component.html',
  styleUrls: ['./fw-modal-ajuste-inventario.component.scss']
})
export class FwModalAjusteInventarioComponent implements OnInit, OnDestroy {

  @Input() tTituloModal: string = "";
  @Input() tMensajeModal: string = "";
  @Input() tArticuloModal: Articulo = new Articulo();
  @Input() tTiposAjustesInv: TipoAjustesInventario[] = [];
  @Input() tAlmacenes: Almacen[] = [];

  tUsuarioActual: Usuario = new Usuario();
  tEmpresaActual: Empresas = new Empresas();

  tAlmacenActual: Almacen = new Almacen();
  tTipoAjusteActual: TipoAjustesInventario = new TipoAjustesInventario();
  tAjusteInvActual: AjusteInventarioEntrada = new AjusteInventarioEntrada();

  tBloquearTallas: boolean = true;
  tBloquearTallasInt: boolean = true;
  tTallaInt: boolean = false;

  tEmpresa$: Subscription;
  tUsuario$: Subscription;
  tHayConfiguracionWs$: Subscription;

  constructor(public activeModal: NgbActiveModal,
    private tSession: SessionService,
    private Ws: ClsServiciosService,
    private toastr: ToastrService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService,
    private tConfServices: ConfirmacionService) { }

  ngOnInit() {

    this.tTipoAjusteActual = this.tTiposAjustesInv[0] || new TipoAjustesInventario();
    this.tAlmacenActual = this.tAlmacenes[0] || new Almacen();

    this.tEmpresa$ = this.tSession.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    });

    this.tUsuario$ = this.tSession.tUsuarioActual$.subscribe((tUsuario: Usuario) => {

      this.tUsuarioActual = tUsuario;

    });

    this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
      .subscribe((tHayConfig: boolean) => {

        if (tHayConfig === true) {



        }

      });

    this.InicializarAjuste();

    if (this.tArticuloModal.Maneja_tallas == "S") {

      this.tBloquearTallas = false;

      if (this.tArticuloModal.Talla_intermedias == "S") {

        this.tBloquearTallasInt = false;

      }

    }

  }

  ngOnDestroy(): void {

    this.tEmpresa$.unsubscribe();
    this.tUsuario$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();

  }



  //#region "CRUD"

  NuevoAjuste() {

    this.Ws
      .AjusteInventario()
      .Consumir_Crear_Articulo(this.tAjusteInvActual)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.toastr.success("El ajuste se realizo correctamente.", "Ajuste de inventario");
          this.InicializarAjuste();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  ManejaTallas(): string {

    let tManejaTalla: string = "No";

    if (this.tArticuloModal.Maneja_tallas === "S") {

      tManejaTalla = "Sí";

    }

    return tManejaTalla;

  }

  ManejaTallasInt(): string {

    let tManejaTallaInt: string = "No";

    if (this.tArticuloModal.Talla_intermedias === "S") {

      tManejaTallaInt = "Sí";

    }

    return tManejaTallaInt;

  }

  InicializarAjuste(): void {

    this.tAjusteInvActual = new AjusteInventarioEntrada();
    this.tAjusteInvActual.Id = 0;
    this.tAjusteInvActual.Corporacion = this.tEmpresaActual.Corporacion;
    this.tAjusteInvActual.Empresa = this.tEmpresaActual.Empresa;
    this.tAjusteInvActual.Codtipoajusteinv = this.tTipoAjusteActual.Codtipoajusteinv;
    this.tAjusteInvActual.Codalmacen = this.tAlmacenActual.Codalmacen;
    this.tAjusteInvActual.Cantidad = 0;
    this.tAjusteInvActual.Codarticulo = this.tArticuloModal.Codarticulo;
    this.tAjusteInvActual.Talla = "";
    this.tAjusteInvActual.Usuariocreador = this.tUsuarioActual.Codusuario;
    this.tAjusteInvActual.Usuariomodificador = this.tUsuarioActual.Codusuario;

  }

  Agregar(): void {

    if (this.tAjusteInvActual.Cantidad <= 0) {

      this.toastr.clear();
      this.toastr.warning("La cantidad del ajuste tiene que ser un monto positivo y mayor a cero.");
      return;

    }

    if (this.tAjusteInvActual.Talla == "" && this.tArticuloModal.Maneja_tallas == "S") {

      this.toastr.clear();
      this.toastr.warning("El artículo maneja tallas, debe indicar una.");
      return;

    }

    if (this.tTallaInt == true && this.tArticuloModal.Talla_intermedias == "S") {

      this.tAjusteInvActual.Talla = this.tAjusteInvActual.Talla + ".5";

    }

    this.tConfServices
      .AbrirModalConf("¿Desea realizar el ajuste para el artículo?")
      .then(Resultado => {

        switch (Resultado) {

          case "S": {

            this.NuevoAjuste();
            break;

          }

        };

      });

  }

  CambiarTipoAjuste(): void {

    this.tAjusteInvActual.Codtipoajusteinv = this.tTipoAjusteActual.Codtipoajusteinv;

  }

  CambiarAlmacen(): void {

    this.tAjusteInvActual.Codalmacen = this.tAlmacenActual.Codalmacen;

  }



}
