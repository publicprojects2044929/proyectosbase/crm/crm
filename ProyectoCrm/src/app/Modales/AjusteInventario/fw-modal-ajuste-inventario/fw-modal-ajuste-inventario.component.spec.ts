import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwModalAjusteInventarioComponent } from './fw-modal-ajuste-inventario.component';

describe('FwModalAjusteInventarioComponent', () => {
  let component: FwModalAjusteInventarioComponent;
  let fixture: ComponentFixture<FwModalAjusteInventarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwModalAjusteInventarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwModalAjusteInventarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
