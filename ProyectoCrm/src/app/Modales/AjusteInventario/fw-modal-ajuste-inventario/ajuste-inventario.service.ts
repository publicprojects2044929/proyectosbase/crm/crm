import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FwModalAjusteInventarioComponent } from '../fw-modal-ajuste-inventario/fw-modal-ajuste-inventario.component'
import { Articulo, TipoAjustesInventario, Almacen } from 'src/app/Clases/Estructura';

@Injectable({
  providedIn: 'root'
})

export class AjusteInventarioService {

  constructor(private modalService: NgbModal) { }

  AbrirModal(tArticuloModal: Articulo, tTiposAjustesInv: TipoAjustesInventario[], tAlmacenes: Almacen[]): Promise<any> {

    return new Promise((resolve, reject) => {

      const modalRef = this.modalService.open(FwModalAjusteInventarioComponent, { size: 'xl' });
      modalRef.componentInstance.tTituloModal = "Ajuste de inventario";
      modalRef.componentInstance.tMensajeModal = "";
      modalRef.componentInstance.tArticuloModal = tArticuloModal;
      modalRef.componentInstance.tTiposAjustesInv = tTiposAjustesInv;
      modalRef.componentInstance.tAlmacenes= tAlmacenes;

      modalRef.result.then(result => {

        resolve(result);

      }, reason => {

        reject(reason);

      });

    })

  }

}
