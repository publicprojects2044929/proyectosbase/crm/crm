import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/Shared/shared.module'
import { NgbModule, NgbActiveModal, NgbAlertConfig } from '@ng-bootstrap/ng-bootstrap';

import { FwModalAjusteInventarioComponent } from 'src/app/Modales/AjusteInventario/fw-modal-ajuste-inventario/fw-modal-ajuste-inventario.component'

@NgModule({
  declarations: [
    FwModalAjusteInventarioComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    SharedModule,
    ReactiveFormsModule
  ],
  exports: [
    FwModalAjusteInventarioComponent
  ],
  entryComponents: [
    FwModalAjusteInventarioComponent
  ],
  providers: [ NgbActiveModal, NgbAlertConfig ]
})
export class AjusteInventarioModalModule { }
