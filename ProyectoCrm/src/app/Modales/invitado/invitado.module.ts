import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/Shared/shared.module';
import { NgbModule, NgbActiveModal, NgbAlertConfig } from '@ng-bootstrap/ng-bootstrap';

import { FwModalInvitadosComponent } from 'src/app/Modales/invitado/fw-modal-invitados/fw-modal-invitados.component';

@NgModule({
  declarations: [
    FwModalInvitadosComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    SharedModule
  ],
  exports: [
    FwModalInvitadosComponent
  ],
  entryComponents: [
    FwModalInvitadosComponent
  ],
  providers: [ NgbActiveModal, NgbAlertConfig ]
})
export class InvitadoModalModule { }
