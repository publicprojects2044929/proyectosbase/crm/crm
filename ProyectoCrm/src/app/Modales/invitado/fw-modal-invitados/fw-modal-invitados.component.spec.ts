import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwModalInvitadosComponent } from './fw-modal-invitados.component';

describe('FwModalInvitadosComponent', () => {
  let component: FwModalInvitadosComponent;
  let fixture: ComponentFixture<FwModalInvitadosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwModalInvitadosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwModalInvitadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
