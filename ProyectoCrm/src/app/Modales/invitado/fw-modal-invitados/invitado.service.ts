import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FwModalInvitadosComponent } from './fw-modal-invitados.component'

@Injectable({
  providedIn: 'root'
})
export class InvitadosService {

  constructor(private modalService: NgbModal) { }

  AbrirModal(): Promise<any> {

    return new Promise((resolve, reject) => {

      const modalRef = this.modalService.open(FwModalInvitadosComponent, { size: 'lg' });
      modalRef.componentInstance.tTituloModal = "Modal Invitados";
      modalRef.componentInstance.tMensajeModal = "";

      modalRef.result.then(result => {

        resolve(result);

      }, reason => {

        reject(reason);

      });

    })

  }

}
