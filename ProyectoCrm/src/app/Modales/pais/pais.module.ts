import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/Shared/shared.module'
import { NgbModule, NgbActiveModal, NgbAlertConfig } from '@ng-bootstrap/ng-bootstrap';

import { FwModalPaisComponent } from './fw-modal-pais/fw-modal-pais.component';

@NgModule({
  declarations: [
    FwModalPaisComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    SharedModule
  ],
  exports: [
    FwModalPaisComponent
  ],
  entryComponents: [
    FwModalPaisComponent
  ],
  providers: [ NgbActiveModal, NgbAlertConfig ]
})

export class PaisModalModule { }
