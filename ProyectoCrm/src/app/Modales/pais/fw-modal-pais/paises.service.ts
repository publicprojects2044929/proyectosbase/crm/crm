import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FwModalPaisComponent } from '../fw-modal-pais/fw-modal-pais.component'

@Injectable({
  providedIn: 'root'
})
export class PaisesService {

  constructor(private modalService: NgbModal) { }

  AbrirModal(): Promise<any> {

    return new Promise((resolve, reject) => {

      const modalRef = this.modalService.open(FwModalPaisComponent, { size: 'lg' });
      modalRef.componentInstance.tTituloModal = "Modal de Paises";
      modalRef.componentInstance.tMensajeModal = "";

      modalRef.result.then(result => {

        resolve(result);

      }, reason => {

        reject(reason);

      });

    })

  }

}
