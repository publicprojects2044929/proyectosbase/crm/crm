import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwModalPaisComponent } from './fw-modal-pais.component';

describe('FwModalPaisComponent', () => {
  let component: FwModalPaisComponent;
  let fixture: ComponentFixture<FwModalPaisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwModalPaisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwModalPaisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
