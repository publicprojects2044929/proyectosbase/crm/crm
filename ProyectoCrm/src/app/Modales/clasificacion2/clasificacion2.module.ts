import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/Shared/shared.module'
import { NgbModule, NgbActiveModal, NgbAlertConfig } from '@ng-bootstrap/ng-bootstrap';

import { FwModalClasificaciones2Component } from 'src/app/Modales/clasificacion2/fw-modal-clasificaciones2/fw-modal-clasificaciones2.component'

@NgModule({
  declarations: [
    FwModalClasificaciones2Component
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    SharedModule
  ],
  exports: [
    FwModalClasificaciones2Component
  ],
  entryComponents: [
    FwModalClasificaciones2Component
  ],
  providers: [ NgbActiveModal, NgbAlertConfig ]
})
export class Clasificacion2ModalModule { }
