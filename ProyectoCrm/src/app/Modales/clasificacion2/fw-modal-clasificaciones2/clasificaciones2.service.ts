import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FwModalClasificaciones2Component } from '../fw-modal-clasificaciones2/fw-modal-clasificaciones2.component'
import { Clasificacion1 } from 'src/app/Clases/Estructura';

@Injectable({
  providedIn: 'root'
})

export class Clasificaciones2Service {

  constructor(private modalService: NgbModal) { }

  AbrirModal(tClasificacion1Actual: Clasificacion1): Promise<any> {

    return new Promise((resolve, reject) => {

      const modalRef = this.modalService.open(FwModalClasificaciones2Component, { size: 'lg' });
      modalRef.componentInstance.tTituloModal = "Modal de grupo";
      modalRef.componentInstance.tMensajeModal = "";
      modalRef.componentInstance.tClasificacion1 = tClasificacion1Actual;

      modalRef.result.then(result => {

        resolve(result);

      }, reason => {

        reject(reason);

      });

    })

  }

}
