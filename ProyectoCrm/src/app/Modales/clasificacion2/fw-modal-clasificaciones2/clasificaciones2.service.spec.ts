import { TestBed } from '@angular/core/testing';

import { Clasificaciones2Service } from './clasificaciones2.service';

describe('Clasificaciones2Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Clasificaciones2Service = TestBed.get(Clasificaciones2Service);
    expect(service).toBeTruthy();
  });
});
