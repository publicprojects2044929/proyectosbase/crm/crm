import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwModalClasificaciones2Component } from './fw-modal-clasificaciones2.component';

describe('FwModalClasificaciones2Component', () => {
  let component: FwModalClasificaciones2Component;
  let fixture: ComponentFixture<FwModalClasificaciones2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwModalClasificaciones2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwModalClasificaciones2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
