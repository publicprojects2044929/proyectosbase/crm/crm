import { TestBed } from '@angular/core/testing';

import { DetallesPedidoService } from './detalles-pedido.service';

describe('DetallesPedidoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DetallesPedidoService = TestBed.get(DetallesPedidoService);
    expect(service).toBeTruthy();
  });
});
