import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwModalDetallesPedidoComponent } from './fw-modal-detalles-pedido.component';

describe('FwModalDetallesPedidoComponent', () => {
  let component: FwModalDetallesPedidoComponent;
  let fixture: ComponentFixture<FwModalDetallesPedidoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwModalDetallesPedidoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwModalDetallesPedidoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
