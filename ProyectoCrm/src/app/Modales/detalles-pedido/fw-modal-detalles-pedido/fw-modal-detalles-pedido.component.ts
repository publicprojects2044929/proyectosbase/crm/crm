import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Pedido, Empresas, Usuario } from 'src/app/Clases/Estructura';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';

import { MensajesService } from '../../mensajes/fw-modal-msj/mensajes.service';
import { ErroresService } from '../../mensajes/fw-modal-msj/errores.service';
import { environment } from 'src/environments/environment';
import { Subscription } from 'rxjs';
import { SessionService } from 'src/app/Core/Session/session.service';

@Component({
  selector: 'app-fw-modal-detalles-pedido',
  templateUrl: './fw-modal-detalles-pedido.component.html',
  styleUrls: ['./fw-modal-detalles-pedido.component.scss']
})
export class FwModalDetallesPedidoComponent implements OnInit, OnDestroy {

  @Input() tTituloModal: string = "";
  @Input() tMensajeModal: string = "";
  @Input() tPedidoModal: Pedido = new Pedido();

  tPedidoActual: Pedido = new Pedido();

  tUsuarioActual: Usuario = new Usuario();
  tUsuario$: Subscription;
  tEmpresaActual: Empresas = new Empresas();
  tEmpresa$: Subscription;
  
  tPosicion: number = 0;
  tPagina: number = 1;
  tTamanoPag: number = 10;

  tHayConfiguracionWs$: Subscription;

  constructor(public activeModal: NgbActiveModal,
    public Ws: ClsServiciosService,
    private tSession: SessionService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService) { }

  ngOnInit() {

    this.tEmpresa$ = this.tSession.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    });

    this.tUsuario$ = this.tSession.tUsuarioActual$.subscribe((tUsuario: Usuario) => {

      this.tUsuarioActual = tUsuario;

    });

    this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
      .subscribe((tHayConfig: boolean) => {

        if (tHayConfig === true) {

          this.BuscarPedido();

        }

      });

  }

  ngOnDestroy() {

    this.tHayConfiguracionWs$.unsubscribe();

  }

  BuscarPedido() {

    this.Ws
      .Pedidos()
      .Consumir_Obtener_PedidoFull(this.tPedidoModal.Corporacion,
        this.tPedidoModal.Empresa,
        this.tPedidoModal.Codpedido)
      .subscribe(Respuesta => {


        if (Respuesta.Resultado == "N") {


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tPedidoActual = Respuesta.Datos[0] || new Pedido();

        }


      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

}
