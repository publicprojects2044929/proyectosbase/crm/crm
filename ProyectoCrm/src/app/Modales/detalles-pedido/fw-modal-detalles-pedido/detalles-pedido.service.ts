import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FwModalDetallesPedidoComponent } from './fw-modal-detalles-pedido.component'
import { Pedido } from 'src/app/Clases/Estructura';


@Injectable({
  providedIn: 'root'
})

export class DetallesPedidoService {

  constructor(private modalService: NgbModal) { }

  AbrirModalConf(tPedido: Pedido): Promise<string> {

    return new Promise((resolve, reject) => {

      const modalRef = this.modalService.open(FwModalDetallesPedidoComponent, { size: 'xl' });
      modalRef.componentInstance.tTituloModal = "Detalles";
      modalRef.componentInstance.tMensajeModal = "";
      modalRef.componentInstance.tPedidoModal = tPedido;

      modalRef.result.then(result => {

        resolve(result);

      }, reason => {

        reject(reason);

      });

    })

  }


}
