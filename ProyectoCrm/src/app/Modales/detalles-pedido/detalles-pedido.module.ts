import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/Shared/shared.module'
import { NgbModule, NgbActiveModal, NgbAlertConfig } from '@ng-bootstrap/ng-bootstrap';

import { FwModalDetallesPedidoComponent } from 'src/app/Modales/detalles-pedido/fw-modal-detalles-pedido/fw-modal-detalles-pedido.component'

@NgModule({
  declarations: [
    FwModalDetallesPedidoComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    SharedModule
  ],
  exports: [
    FwModalDetallesPedidoComponent
  ],
  entryComponents: [
    FwModalDetallesPedidoComponent
  ],
  providers: [ NgbActiveModal, NgbAlertConfig ]
})
export class DetallesPedidoModalModule { }
