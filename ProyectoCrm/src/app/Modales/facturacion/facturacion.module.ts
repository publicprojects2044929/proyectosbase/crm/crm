import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/Shared/shared.module'
import { NgbModule, NgbActiveModal, NgbAlertConfig } from '@ng-bootstrap/ng-bootstrap';
import { FwModalFacturacionComponent } from './fw-modal-facturacion/fw-modal-facturacion.component';

@NgModule({
  declarations: [
    FwModalFacturacionComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    SharedModule
  ],
  exports: [
    FwModalFacturacionComponent
  ],
  entryComponents: [
    FwModalFacturacionComponent
  ],
  providers: [ NgbActiveModal, NgbAlertConfig ]
})
export class FacturacionModalModule { }
