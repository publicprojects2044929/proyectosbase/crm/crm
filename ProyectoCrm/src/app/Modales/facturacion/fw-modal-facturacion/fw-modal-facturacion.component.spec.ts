import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwModalFacturacionComponent } from './fw-modal-facturacion.component';

describe('FwModalFacturacionComponent', () => {
  let component: FwModalFacturacionComponent;
  let fixture: ComponentFixture<FwModalFacturacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwModalFacturacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwModalFacturacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
