import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Pedido, Cliente, Usuario, Empresas, FormaDePago, MonedaTasas } from 'src/app/Clases/Estructura';
import { VentaEntrada, VentaFormasPagosEntrada, VentaFormasPagosReferenciaEntrada, VentaFormasPagosTasasEntrada } from 'src/app/Clases/EstructuraEntrada';
import { SessionService } from 'src/app/Core/Session/session.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { ToastrService } from 'ngx-toastr';
import { ErroresService } from '../../mensajes/fw-modal-msj/errores.service';
import { MensajesService } from '../../mensajes/fw-modal-msj/mensajes.service';
import { ConfirmacionService } from '../../confirmacion/fw-modal-confirmacion/confirmacion.service';
import { environment } from 'src/environments/environment';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-fw-modal-facturacion',
  templateUrl: './fw-modal-facturacion.component.html',
  styleUrls: ['./fw-modal-facturacion.component.scss']
})
export class FwModalFacturacionComponent implements OnInit, OnDestroy {

  @Input() tTituloModal: string = "";
  @Input() tMensajeModal: string = "";
  @Input() tPedidoModal: Pedido = new Pedido();
  @Input() tClienteModal: Cliente = new Cliente();

  tPosicionArticulos: number = 0;
  tPaginaArticulos: number = 1;
  tTamanoPagArticulos: number = 2;

  tPosicionFormasPagos: number = 0;
  tPaginaFormasPagos: number = 1;
  tTamanoPagFormasPagos: number = 5;

  tUsuarioActual: Usuario = new Usuario();
  tUsuario$: Subscription;
  tEmpresaActual: Empresas = new Empresas();
  tEmpresa$: Subscription;
  tHayConfiguracionWs$: Subscription;

  tFormasPagos: FormaDePago[] = [];
  tFormaPagoActual: FormaDePago = new FormaDePago();

  tTasas: MonedaTasas[] = [];
  tTasaActual: MonedaTasas = new MonedaTasas();

  tVentaActual: VentaEntrada = new VentaEntrada();
  tVentaFormaPagoActual: VentaFormasPagosEntrada = new VentaFormasPagosEntrada();
  tVentaFormaPagoRefActual: VentaFormasPagosReferenciaEntrada = new VentaFormasPagosReferenciaEntrada();
  tVentaFormaPagoTasaActual: VentaFormasPagosTasasEntrada = new VentaFormasPagosTasasEntrada();

  tFacturaContado: boolean = false;
  tFacturaCredito: boolean = false;

  constructor(public activeModal: NgbActiveModal,
    private tSession: SessionService,
    private Ws: ClsServiciosService,
    private toastr: ToastrService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService,
    private tConfServices: ConfirmacionService) { }

  ngOnInit() {

    this.tEmpresa$ = this.tSession.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    });

    this.tUsuario$ = this.tSession.tUsuarioActual$.subscribe((tUsuario: Usuario) => {

      this.tUsuarioActual = tUsuario;

    });

    this.tVentaActual.Corporacion = this.tEmpresaActual.Corporacion;
    this.tVentaActual.Empresa = this.tEmpresaActual.Empresa;
    this.tVentaActual.Codpedido = this.tPedidoModal.Codpedido;
    this.tVentaActual.Usuariocreador = this.tUsuarioActual.Codusuario;
    this.tVentaActual.Usuariomodificador = this.tUsuarioActual.Codusuario;

    this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
      .subscribe((tHayConfig: boolean) => {

        if (tHayConfig === true) {

          this.ObtenerFormasDePago();
          this.ObtenerCotizaciones();

        }

      });

    if (this.tClienteModal.Facturas_contado === "S") {

      this.FacturarContado();
      return;

    }

    if (this.tClienteModal.Facturas_credito === "S") {

      this.FacturarCredito();
      return;

    }

  }

  ngOnDestroy() {

    this.tUsuario$.unsubscribe();
    this.tEmpresa$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();

  }

  //#region "CRUD"

  NuevaVenta() {

    this.Ws
      .Venta()
      .Consumir_Crear_Venta(this.tVentaActual)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.toastr.success("El pedido se proceso correctamente.", "Venta");
          this.activeModal.close({
            "Resultado": "S"
          })

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  //#region "Busqueda"

  ObtenerFormasDePago(): void {

    this.Ws
      .FormasPago()
      .Consumir_Obtener_FormasPagoCrm(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tFormasPagos = Respuesta.Datos;
          this.tFormaPagoActual = this.tFormasPagos[0];
          this.SeleccionarFormaPago();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      })

  }

  ObtenerCotizaciones(): void {

    this.Ws
      .MonedasTasas()
      .Consumir_Obtener_Moneda_TasasPorDestinoActivo(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        this.tEmpresaActual.Codmoneda)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tTasas = Respuesta.Datos;

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  SeleccionarFormaPago(): void {

    this.tTasaActual = this.tTasas[0] || new MonedaTasas();

    this.tVentaFormaPagoActual = new VentaFormasPagosEntrada();
    this.tVentaFormaPagoActual.Id = 0;
    this.tVentaFormaPagoActual.Corporacion = this.tVentaActual.Corporacion;
    this.tVentaFormaPagoActual.Empresa = this.tVentaActual.Empresa;
    this.tVentaFormaPagoActual.Codventa = this.tVentaActual.Codventa;
    this.tVentaFormaPagoActual.Codformapago = this.tFormaPagoActual.Codformapago;
    this.tVentaFormaPagoActual.Monto = this.ObtenerSaldoPendiente();
    this.tVentaFormaPagoActual.Usuariocreador = this.tVentaActual.Usuariocreador;
    this.tVentaFormaPagoActual.Usuariomodificador = this.tVentaActual.Usuariomodificador;

    this.tVentaFormaPagoActual.FormaPagoReferencia.Corporacion = this.tVentaActual.Corporacion;
    this.tVentaFormaPagoActual.FormaPagoReferencia.Empresa = this.tVentaActual.Empresa;
    this.tVentaFormaPagoActual.FormaPagoReferencia.Codformapago = this.tFormaPagoActual.Codformapago;

    this.tVentaFormaPagoActual.FormaPagoTasa.Corporacion = this.tVentaActual.Corporacion;
    this.tVentaFormaPagoActual.FormaPagoTasa.Empresa = this.tVentaActual.Empresa;
    this.tVentaFormaPagoActual.FormaPagoTasa.Codformapago = this.tFormaPagoActual.Codformapago;
    this.tVentaFormaPagoActual.FormaPagoTasa.Codmonedadestino = this.tEmpresaActual.Codmoneda;

    this.tTasaActual = this.tTasas[0] || new MonedaTasas();

  }

  ObtenerTotalPagado(): number {

    let tPagado: number = 0;

    this.tVentaActual.LFormasPagos.forEach(tFormaPago => {

      tPagado += +tFormaPago.Monto;

    })

    return tPagado;

  }

  ObtenerSaldoPendiente(): number {

    let tSaldoPendiente: number = 0;
    let tPagado: number = this.ObtenerTotalPagado();
    tSaldoPendiente = this.tPedidoModal.Total - tPagado;

    if (tSaldoPendiente < 0) {

      tSaldoPendiente = 0;

    }

    return tSaldoPendiente;

  }

  ObtenerVuelto(): number {

    let tVuelto: number = 0;
    let tPagado: number = this.ObtenerTotalPagado();

    if ((tPagado - this.tPedidoModal.Total) < 0) {

      tVuelto = 0;

    }
    else {

      tVuelto = tPagado - this.tPedidoModal.Total;

    }

    return tVuelto;

  }

  CalcularCambio(): void {

    if (this.tFormaPagoActual.Tasa == "N") {

      return;

    }

    this.tVentaFormaPagoActual.Monto = this.tTasaActual.Tasa * this.tVentaFormaPagoActual.FormaPagoTasa.Monto;

  }

  ObtenerDescripcionFormaPago(tFormaPagoVenta: VentaFormasPagosEntrada): string {

    let tDescripcion = "";
    let tFormaPago: FormaDePago = new FormaDePago();

    tFormaPago = this.tFormasPagos.find(tFormaPagoFor => {

      if (tFormaPagoFor.Corporacion == tFormaPagoVenta.Corporacion &&
        tFormaPagoFor.Empresa == tFormaPagoVenta.Empresa &&
        tFormaPagoFor.Codformapago == tFormaPagoVenta.Codformapago) {

        return tFormaPagoFor;

      }

    });

    tDescripcion = "[ " + tFormaPagoVenta.Codformapago + " ] " + tFormaPago.Descripcion;

    return tDescripcion;

  }

  BloquearControlMonto(): boolean {

    let tBloquear: boolean = false;

    if (this.tFormaPagoActual.Tasa == "S") {

      tBloquear = true;

    }

    return tBloquear;

  }

  AgregarFormaPago(): void {

    if (this.tVentaFormaPagoActual.Monto <= 0) {

      return;

    }

    let tExiste: boolean = false;
    let tSaldoPendiente: number = this.ObtenerSaldoPendiente();

    if (tSaldoPendiente <= 0) {

      this.toastr.info("Ya no queda saldo pendiente por facturar.", "Facturación");
      return;

    }

    for (let tFormaPago of this.tVentaActual.LFormasPagos) {

      if (tFormaPago.Corporacion == this.tFormaPagoActual.Corporacion &&
        tFormaPago.Empresa == this.tFormaPagoActual.Empresa &&
        tFormaPago.Codformapago == this.tFormaPagoActual.Codformapago) {

        tExiste = true;

      }

    }

    for (let tFormaPago of this.tVentaActual.LFormasPagos) {

      if (tFormaPago.Corporacion == this.tFormaPagoActual.Corporacion &&
        tFormaPago.Empresa == this.tFormaPagoActual.Empresa &&
        this.tVentaFormaPagoActual.FormaPagoReferencia.Referencia == tFormaPago.FormaPagoReferencia.Referencia) {

        this.tMsjServices.AbrirModalMsj("Disculpe, la Referencia ya se encuentra registrada");

        return;
      }

    }

    if (tExiste == true && this.tFormaPagoActual.Duplicado == 'N') {

      this.tMsjServices.AbrirModalMsj("Disculpe, esta forma de pago no admite duplicados");

      return;

    }

    if (this.tVentaFormaPagoActual.Monto > tSaldoPendiente && this.tFormaPagoActual.Vuelto == 'N') {

      this.tMsjServices.AbrirModalMsj("El monto para esta forma de pago no puede ser mayor al saldo pendiente de la factura.");

      return;

    }

    if (this.tVentaFormaPagoActual.FormaPagoReferencia.Referencia == "" &&
      this.tFormaPagoActual.Referencia == 'S') {

      this.tMsjServices.AbrirModalMsj("Debe indicar el nro de referencia para el pago.");

      return;

    }

    if (this.tFormaPagoActual.Tasa == "S") {

      this.tVentaFormaPagoActual.FormaPagoTasa.Codtasa = this.tTasaActual.Codtasa;
      this.tVentaFormaPagoActual.FormaPagoTasa.Codmonedaorigen = this.tTasaActual.Codmonedaorigen;

    }

    this.tVentaActual.LFormasPagos.push(this.tVentaFormaPagoActual);
    this.SeleccionarFormaPago();


  }

  Facturar(): void {

    if (this.tClienteModal.Facturas_credito_vencidas >= this.tClienteModal.Limite_facturas_vencidas) {

      this.toastr.clear();
      this.toastr.info("El cliente esta bloqueado para facturar por tener muchas facturas vencidas.");
      return;

    }

    if (this.tFacturaContado === true) {

      if (this.tClienteModal.Facturas_contado === "N") {

        this.toastr.clear();
        this.toastr.info("El cliente no factura a contado");
        return;

      }

      let tSaldoPendiente: number = this.ObtenerSaldoPendiente();

      if (tSaldoPendiente > 0) {

        this.toastr.info("Todavía queda un saldo pendiente para la factura.", "Venta");
        return;

      }

      this.tFormasPagos.forEach(tFormaPagoFor => {

        let tItem: number = 1

        this.tVentaActual.LFormasPagos.find(tVentaFormaPagoFor => {

          if (tVentaFormaPagoFor.Corporacion == tFormaPagoFor.Corporacion &&
            tVentaFormaPagoFor.Empresa == tFormaPagoFor.Empresa &&
            tVentaFormaPagoFor.Codformapago == tFormaPagoFor.Codformapago) {

            tVentaFormaPagoFor.Item = tItem;
            tVentaFormaPagoFor.FormaPagoReferencia.Item = tItem;
            tVentaFormaPagoFor.FormaPagoTasa.Item = tItem;

            tItem++;

          }

        })

      })

    }

    if (this.tFacturaCredito === true) {

      let tCreditoDisponible: number = 0;

      if (this.tClienteModal.Facturas_credito === "N") {

        this.toastr.clear();
        this.toastr.info("El cliente no factura a crédito");
        return;

      }

      tCreditoDisponible = this.tClienteModal.Limite_credito - this.tClienteModal.Credito_usado;

      if (this.tPedidoModal.Total > tCreditoDisponible) {

        this.toastr.clear();
        this.toastr.info("El limite del crédito para el cliente no es suficiente para facturar el pedido.");
        return;

      }

      if (this.tClienteModal.Facturas_credito_activas >= this.tClienteModal.Limite_facturas_credito) {

        this.toastr.clear();
        this.toastr.info("El cliente alcanzo el limite de facturas a creditos activas.");
        return;

      }

    }

    this.tConfServices.AbrirModalConf("¿Desea facturar el pedido?").then(Resultado => {

      switch (Resultado) {

        case "S": {

          this.NuevaVenta();
          break;

        }

      };

    });

  }

  SeleccionarVentaPago(tVentaPago: VentaFormasPagosEntrada): void {

    this.tVentaFormaPagoRefActual = tVentaPago.FormaPagoReferencia;
    this.tVentaFormaPagoTasaActual = tVentaPago.FormaPagoTasa;

  }

  ObtenerFormaPagoTasa(): string {

    let tCodtasaYTasa: string = "";
    let tFormaPago: FormaDePago = this.tFormasPagos.find(tFormaPagoFor => {

      if (tFormaPagoFor.Corporacion == this.tFormaPagoActual.Corporacion &&
        tFormaPagoFor.Empresa == this.tFormaPagoActual.Empresa &&
        tFormaPagoFor.Codformapago == this.tFormaPagoActual.Codformapago) {

        return tFormaPagoFor;

      }

    }) || new FormaDePago();

    if (tFormaPago.Tasa == "S") {

      let tTasa: MonedaTasas = this.tTasas.find(tTasaFor => {

        if (tTasaFor.Corporacion == this.tVentaFormaPagoTasaActual.Corporacion &&
          tTasaFor.Empresa == this.tVentaFormaPagoTasaActual.Empresa &&
          tTasaFor.Codtasa == this.tVentaFormaPagoTasaActual.Codtasa) {

          return tTasaFor;

        }

      }) || new MonedaTasas();

      tCodtasaYTasa = "[ " + this.tVentaFormaPagoTasaActual.Codtasa + " ] " + tTasa.Tasa;

    }

    return tCodtasaYTasa;

  }

  ObtenerFormaPagoOrigen(): string {

    let tFormaPagoOrigen: string = "";
    let tFormaPago: FormaDePago = this.tFormasPagos.find(tFormaPagoFor => {

      if (tFormaPagoFor.Corporacion == this.tFormaPagoActual.Corporacion &&
        tFormaPagoFor.Empresa == this.tFormaPagoActual.Empresa &&
        tFormaPagoFor.Codformapago == this.tFormaPagoActual.Codformapago) {

        return tFormaPagoFor;

      }

    }) || new FormaDePago();

    if (tFormaPago.Tasa == "S") {

      let tTasa: MonedaTasas = this.tTasas.find(tTasaFor => {

        if (tTasaFor.Corporacion == this.tVentaFormaPagoTasaActual.Corporacion &&
          tTasaFor.Empresa == this.tVentaFormaPagoTasaActual.Empresa &&
          tTasaFor.Codtasa == this.tVentaFormaPagoTasaActual.Codtasa) {

          return tTasaFor;

        }

      }) || new MonedaTasas();

      tFormaPagoOrigen = "[ " + this.tVentaFormaPagoTasaActual.Codmonedaorigen + " ] " + tTasa.Descripcion_origen;

    }

    return tFormaPagoOrigen;

  }

  ObtenerFormaPagoDestino(): string {

    let tFormaPagoDestino: string = "";
    let tFormaPago: FormaDePago = this.tFormasPagos.find(tFormaPagoFor => {

      if (tFormaPagoFor.Corporacion == this.tFormaPagoActual.Corporacion &&
        tFormaPagoFor.Empresa == this.tFormaPagoActual.Empresa &&
        tFormaPagoFor.Codformapago == this.tFormaPagoActual.Codformapago) {

        return tFormaPagoFor;

      }

    }) || new FormaDePago();

    if (tFormaPago.Tasa == "S") {

      let tTasa: MonedaTasas = this.tTasas.find(tTasaFor => {

        if (tTasaFor.Corporacion == this.tVentaFormaPagoTasaActual.Corporacion &&
          tTasaFor.Empresa == this.tVentaFormaPagoTasaActual.Empresa &&
          tTasaFor.Codtasa == this.tVentaFormaPagoTasaActual.Codtasa) {

          return tTasaFor;

        }

      }) || new MonedaTasas();

      tFormaPagoDestino = "[ " + this.tVentaFormaPagoTasaActual.Codmonedadestino + " ] " + tTasa.Descripcion_destino;

    }

    return tFormaPagoDestino;

  }

  EliminarFormaPago(tPosicion: number): void {

    this.tVentaActual.LFormasPagos.splice(tPosicion, 1);
    this.tVentaFormaPagoRefActual = new VentaFormasPagosReferenciaEntrada();
    this.tVentaFormaPagoTasaActual = new VentaFormasPagosTasasEntrada();

  }

  FacturarContado(): void {

    if (this.tClienteModal.Facturas_contado === "N") {

      return;

    }

    this.tVentaActual.Tipo = "CONTADO";
    this.tFacturaContado = true;
    this.tFacturaCredito = false;

  }

  FacturarCredito(): void {

    if (this.tClienteModal.Facturas_credito === "N") {

      return;

    }

    this.tVentaActual.Tipo = "CREDITO";
    this.tFacturaContado = false;
    this.tFacturaCredito = true;
    
  }

}
