import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FwModalFacturacionComponent } from '../fw-modal-facturacion/fw-modal-facturacion.component'
import { Pedido, Cliente } from 'src/app/Clases/Estructura';

@Injectable({
  providedIn: 'root'
})
export class FacturacionService {

  constructor(private modalService: NgbModal) { }

  AbrirModal(tClientePedidoActual: Pedido, tClienteActual: Cliente): Promise<any> {

    return new Promise((resolve, reject) => {

      const modalRef = this.modalService.open(FwModalFacturacionComponent, { size: 'xl' });
    modalRef.componentInstance.tTituloModal = "Facturación";
    modalRef.componentInstance.tMensajeModal = "";
    modalRef.componentInstance.tPedidoModal = tClientePedidoActual;
    modalRef.componentInstance.tClienteModal = tClienteActual;

      modalRef.result.then(result => {

        resolve(result);

      }, reason => {

        reject(reason);

      });

    })

  }
  
}
