import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/Shared/shared.module'
import { NgbModule, NgbActiveModal, NgbAlertConfig } from '@ng-bootstrap/ng-bootstrap';

import { FwModalPedidoArticulosComponent } from './fw-modal-pedido-articulos/fw-modal-pedido-articulos.component' 

@NgModule({
  declarations: [
    FwModalPedidoArticulosComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    SharedModule
  ],
  exports: [
    FwModalPedidoArticulosComponent
  ],
  entryComponents: [
    FwModalPedidoArticulosComponent
  ],
  providers: [ NgbActiveModal, NgbAlertConfig ]
})
export class PedidoArticulosModalModule { }
