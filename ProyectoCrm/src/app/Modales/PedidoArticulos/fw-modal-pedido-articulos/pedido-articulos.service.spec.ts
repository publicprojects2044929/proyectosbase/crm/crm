import { TestBed } from '@angular/core/testing';

import { PedidoArticulosService } from './pedido-articulos.service';

describe('PedidoArticulosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PedidoArticulosService = TestBed.get(PedidoArticulosService);
    expect(service).toBeTruthy();
  });
});
