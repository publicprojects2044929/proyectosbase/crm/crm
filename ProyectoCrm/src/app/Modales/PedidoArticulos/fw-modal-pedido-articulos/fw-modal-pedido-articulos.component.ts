import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Pedido, Cliente, Usuario, Empresas, Articulo, Tipo, ArticuloInventario, PedidoArticulo } from 'src/app/Clases/Estructura';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { ToastrService } from 'ngx-toastr';
import { ErroresService } from '../../mensajes/fw-modal-msj/errores.service';
import { MensajesService } from '../../mensajes/fw-modal-msj/mensajes.service';
import { ConfirmacionService } from '../../confirmacion/fw-modal-confirmacion/confirmacion.service';
import { ManejarImagenes } from 'src/app/Manejadores/cls-procedimientos';
import { SessionService } from 'src/app/Core/Session/session.service';
import { environment } from 'src/environments/environment';
import { PedidoEntrada, PedidoArticuloEntrada } from 'src/app/Clases/EstructuraEntrada';
import { Subscription } from 'rxjs';
import { ArticulosService } from '../../articulo/fw-modal-articulos/articulos.service';

@Component({
  selector: 'app-fw-modal-pedido-articulos',
  templateUrl: './fw-modal-pedido-articulos.component.html',
  styleUrls: ['./fw-modal-pedido-articulos.component.scss']
})
export class FwModalPedidoArticulosComponent implements OnInit, OnDestroy {

  @Input() tTituloModal: string = "";
  @Input() tMensajeModal: string = "";
  @Input() tPedidoModal: Pedido = new Pedido();
  @Input() tClienteModal: Cliente = new Cliente();

  tDescripcionPedido: string = "Sin pedido cargado";
  tDescripcionCliente: string = "";

  tUsuarioActual: Usuario = new Usuario();
  tUsuario$: Subscription;
  tEmpresaActual: Empresas = new Empresas();
  tEmpresa$: Subscription;
  tHayConfiguracionWs$: Subscription;

  tManejarImagen: ManejarImagenes = new ManejarImagenes();
  tArticuloActual: Articulo = new Articulo();

  tTiposPedido: Tipo[] = [];
  tTipoPedidoActual: Tipo = new Tipo();
  tArticuloInventarioActual: ArticuloInventario = new ArticuloInventario();
  tPedidoArticuloActual: PedidoArticulo = new PedidoArticulo();
  tNuevo: boolean = true;
  tNuevoArticulo: boolean = true;

  constructor(public activeModal: NgbActiveModal,
    private tSession: SessionService,
    private Ws: ClsServiciosService,
    private toastr: ToastrService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService,
    private tArticuloService: ArticulosService,
    private tConfServices: ConfirmacionService) {
  }

  ngOnInit() {

    this.tEmpresa$ = this.tSession.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    });

    this.tUsuario$ = this.tSession.tUsuarioActual$.subscribe((tUsuario: Usuario) => {

      this.tUsuarioActual = tUsuario;

    });

    this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
      .subscribe((tHayConfig: boolean) => {

        if (tHayConfig === true) {

          this.Ws
            .Tipo()
            .Consumir_Obtener_Tipo(this.tEmpresaActual.Corporacion,
              this.tEmpresaActual.Empresa,
              "TR_PEDIDO")
            .subscribe(Respuesta => {

              if (Respuesta.Resultado == "N") {


              }
              else if (Respuesta.Resultado == "E") {

                this.tMsjServices.AbrirModalMsj(environment.msjError);

              }
              else {

                this.tTiposPedido = Respuesta.Datos;
                this.tTipoPedidoActual = this.tTiposPedido.find(tTipo => {

                  if (tTipo.Codtipo == this.tPedidoModal.Tipo) {

                    return tTipo;

                  }

                }) || this.tTiposPedido[0];

              }

            }, error => {

              this.tErroresServices.MostrarError(error);

            });

        }

      });

    this.tDescripcionCliente = `[${this.tClienteModal.Codcliente}] - ${this.tClienteModal.Descripcion} `;

    if (this.tPedidoModal.Codpedido !== "") {

      this.tNuevo = false;
      this.tDescripcionPedido = `${this.tPedidoModal.Tipo} - ${this.tPedidoModal.Codpedido}`;

    }
    else {
      this.tDescripcionPedido = 'Sin pedido cargado';
    }

  }

  ngOnDestroy() {

    this.tEmpresa$.unsubscribe();
    this.tUsuario$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();

  }

  AbrirModalArticulos(): void {

    this.tArticuloService.AbrirModal().then(Resultado => {

      if (Resultado.Resultado == "S") {

        var tArticuloModal: Articulo = Resultado.Articulo;
        this.tArticuloActual.Codarticulo = tArticuloModal.Codarticulo;
        this.BuscarArticulo(this.tArticuloActual.Codarticulo);

      }

    });
  }

  //#region "CRUD"

  NuevoPedido() {

    let tNuevoPedido: PedidoEntrada = {

      "Id": 0,
      "Corporacion": this.tEmpresaActual.Corporacion,
      "Empresa": this.tEmpresaActual.Empresa,
      "Codpedido": "",
      "Codcliente": this.tClienteModal.Codcliente,
      "Tipo": this.tTipoPedidoActual.Codtipo,
      "Estatus": "ACTIVO",
      "Usuariocreador": this.tUsuarioActual.Codusuario,
      "Usuariomodificador": this.tUsuarioActual.Codusuario,
      "LArticulos": [],
      "LCupones": [],
      "LColecciones": []

    }

    let tArticuloPedido: PedidoArticuloEntrada = {

      "Id": 0,
      "Corporacion": this.tEmpresaActual.Corporacion,
      "Empresa": this.tEmpresaActual.Empresa,
      "Codpedido": "",
      "Codarticulo": this.tPedidoArticuloActual.Codarticulo,
      "Talla": this.tPedidoArticuloActual.Talla,
      "Codalmacen": this.tPedidoArticuloActual.Codalmacen,
      "Cantidad": this.tPedidoArticuloActual.Cantidad,
      "Usuariocreador": this.tUsuarioActual.Codusuario,
      "Usuariomodificador": this.tUsuarioActual.Codusuario

    }

    tNuevoPedido.LArticulos.push(tArticuloPedido);

    this.Ws
      .Pedidos()
      .Consumir_Crear_Pedido(tNuevoPedido)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tNuevo = false;
          this.toastr.success("El pedido se creo correctamente", "Pedido");
          this.tPedidoModal = Respuesta.Datos[0];
          this.Limpiar();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  ModificarArticulo() {

    let tArticuloPedido: PedidoArticuloEntrada = {

      "Id": 0,
      "Corporacion": this.tPedidoModal.Corporacion,
      "Empresa": this.tPedidoModal.Empresa,
      "Codpedido": this.tPedidoModal.Codpedido,
      "Codarticulo": this.tPedidoArticuloActual.Codarticulo,
      "Codalmacen": this.tPedidoArticuloActual.Codalmacen,
      "Talla": this.tPedidoArticuloActual.Talla,
      "Cantidad": this.tPedidoArticuloActual.Cantidad,
      "Usuariocreador": this.tPedidoModal.Usuariocreador,
      "Usuariomodificador": this.tUsuarioActual.Codusuario

    }

    this.Ws
      .PedidosArticulos()
      .Consumir_Modificar_PedidoArticulo(tArticuloPedido)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.toastr.success("El articulo se cargo correctamente.", "Pedido");
          this.tPedidoModal = Respuesta.Datos[0];
          this.Limpiar();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  NuevoArticulo() {

    let tArticuloPedido: PedidoArticuloEntrada = {

      "Id": 0,
      "Corporacion": this.tPedidoModal.Corporacion,
      "Empresa": this.tPedidoModal.Empresa,
      "Codpedido": this.tPedidoModal.Codpedido,
      "Codarticulo": this.tPedidoArticuloActual.Codarticulo,
      "Talla": this.tPedidoArticuloActual.Talla,
      "Codalmacen": this.tPedidoArticuloActual.Codalmacen,
      "Cantidad": this.tPedidoArticuloActual.Cantidad,
      "Usuariocreador": this.tPedidoModal.Usuariocreador,
      "Usuariomodificador": this.tUsuarioActual.Codusuario

    }

    this.Ws
      .PedidosArticulos()
      .Consumir_Crear_PedidoArticulo(tArticuloPedido)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.toastr.success("El articulo se cargo correctamente.", "Pedido");
          this.tPedidoModal = Respuesta.Datos[0];
          this.Limpiar();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  ObtenerImagenPrincipal(): string {

    let tImagenUrl: string = this.tManejarImagen.ObtenerImagen();

    if (this.tArticuloActual.Url != "") {

      tImagenUrl = this.tArticuloActual.Url;

    }

    return tImagenUrl;

  }

  BuscarArticulo(tCodarticulo: string) {

    if (tCodarticulo == "") {

      return;

    }

    this.Ws
      .Articulo()
      .Consumir_Obtener_ArticuloInventario(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        tCodarticulo)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tArticuloActual = Respuesta.Datos[0];
          this.SeleccionarTalla(this.tArticuloActual.LInventario[0]);

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  SeleccionarTalla(tTalla: ArticuloInventario) {

    this.tArticuloInventarioActual = tTalla;
    this.tNuevoArticulo = true;

    this.tPedidoArticuloActual = this.tPedidoModal.LArticulos.find(tArticulo => {

      if (tArticulo.Corporacion == this.tArticuloInventarioActual.Corporacion &&
        tArticulo.Empresa == this.tArticuloInventarioActual.Empresa &&
        tArticulo.Codarticulo == this.tArticuloInventarioActual.Codarticulo &&
        tArticulo.Talla == this.tArticuloInventarioActual.Talla) {

        this.tNuevoArticulo = false;
        return tArticulo;

      }

    }) || new PedidoArticulo;

    if (this.tNuevoArticulo == true) {

      this.tPedidoArticuloActual.Corporacion = this.tPedidoModal.Corporacion;
      this.tPedidoArticuloActual.Empresa = this.tPedidoModal.Empresa;
      this.tPedidoArticuloActual.Codpedido = this.tPedidoModal.Codpedido;
      this.tPedidoArticuloActual.Codarticulo = this.tArticuloActual.Codarticulo;
      this.tPedidoArticuloActual.Talla = this.tArticuloInventarioActual.Talla;
      this.tPedidoArticuloActual.Codalmacen = this.tArticuloInventarioActual.Codalmacen;
      this.tPedidoArticuloActual.Cantidad = 1;

    }

  }

  SumarCantidad() {

    this.tPedidoArticuloActual.Cantidad++;

  }

  RestarCantidad() {

    this.tPedidoArticuloActual.Cantidad--;

    if (this.tPedidoArticuloActual.Cantidad <= 0) {

      this.tPedidoArticuloActual.Cantidad = 1;

    }

  }

  AceptarPedido() {

    if (this.tArticuloActual.Codarticulo === "") {

      this.toastr.warning("Debe indicar un artículo", "Pedidos");
      return;

    }

    if (this.tArticuloInventarioActual.Codarticulo === "") {

      this.toastr.warning("Debe indicar una talla", "Pedidos");
      return;

    }

    if (this.tPedidoArticuloActual.Cantidad <= this.tPedidoArticuloActual.Cantidad_coleccion) {

      this.toastr.warning("La cantidad minima para este artículo es de " + this.tPedidoArticuloActual.Cantidad_coleccion, "Pedidos");
      return;

    }

    if (this.tPedidoModal.Codpedido == "") {

      this.tConfServices
        .AbrirModalConf("Desea generar un nuevo pedido")
        .then(Respuesta => {

          switch (Respuesta) {

            case "S": {

              this.NuevoPedido();
              break;

            }

          };

        });

    }
    else {

      if (this.tNuevoArticulo == false) {

        this.tConfServices
          .AbrirModalConf("Desea modificar el pedido")
          .then(Respuesta => {

            switch (Respuesta) {

              case "S": {

                this.ModificarArticulo();
                break;

              }

            };

          });

      }
      else {

        this.tConfServices
          .AbrirModalConf("Desea agregar el articulo al pedido")
          .then(Respuesta => {

            switch (Respuesta) {

              case "S": {

                this.NuevoArticulo();
                break;

              }

            };

          });

      }

    }

  }

  Limpiar() {

    this.tArticuloActual = new Articulo();
    this.tArticuloInventarioActual = new ArticuloInventario();

  }

  Seleccionar_Tipo(tTipoSel: Tipo){

    if(this.tNuevo === false) { return; }

    this.tTipoPedidoActual = tTipoSel;

  }

}
