import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwModalPedidoArticulosComponent } from './fw-modal-pedido-articulos.component';

describe('FwModalPedidoArticulosComponent', () => {
  let component: FwModalPedidoArticulosComponent;
  let fixture: ComponentFixture<FwModalPedidoArticulosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwModalPedidoArticulosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwModalPedidoArticulosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
