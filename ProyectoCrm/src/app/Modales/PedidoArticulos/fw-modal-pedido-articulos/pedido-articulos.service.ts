import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FwModalPedidoArticulosComponent } from './fw-modal-pedido-articulos.component'
import { Cliente, Pedido } from 'src/app/Clases/Estructura';

@Injectable({
  providedIn: 'root'
})
export class PedidoArticulosService {

  constructor(private modalService: NgbModal) { }

  AbrirModal(tClienteActual: Cliente, tClientePedidoActual : Pedido): Promise<any> {

    return new Promise((resolve, reject) => {

      const modalRef = this.modalService.open(FwModalPedidoArticulosComponent, { size: 'xl' });
      modalRef.componentInstance.tTituloModal = "Pedidos";
      modalRef.componentInstance.tMensajeModal = "";
      modalRef.componentInstance.tPedidoModal = tClientePedidoActual;
      modalRef.componentInstance.tClienteModal = tClienteActual;

      modalRef.result.then(result => {

        resolve(result);

      }, reason => {

        reject(reason);

      });

    })

  }
  
}
