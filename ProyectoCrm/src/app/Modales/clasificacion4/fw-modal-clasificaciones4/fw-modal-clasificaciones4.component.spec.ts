import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwModalClasificaciones4Component } from './fw-modal-clasificaciones4.component';

describe('FwModalClasificacion4Component', () => {
  let component: FwModalClasificaciones4Component;
  let fixture: ComponentFixture<FwModalClasificaciones4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwModalClasificaciones4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwModalClasificaciones4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
