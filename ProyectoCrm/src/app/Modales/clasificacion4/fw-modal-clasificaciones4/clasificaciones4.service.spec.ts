import { TestBed } from '@angular/core/testing';

import { Clasificaciones4Service } from './clasificaciones4.service';

describe('Clasificaciones4Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Clasificaciones4Service = TestBed.get(Clasificaciones4Service);
    expect(service).toBeTruthy();
  });
});
