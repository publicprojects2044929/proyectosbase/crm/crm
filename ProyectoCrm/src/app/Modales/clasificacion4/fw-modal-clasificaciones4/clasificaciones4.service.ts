import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FwModalClasificaciones4Component } from '../fw-modal-clasificaciones4/fw-modal-clasificaciones4.component'
import { Clasificacion3 } from 'src/app/Clases/Estructura';

@Injectable({
  providedIn: 'root'
})

export class Clasificaciones4Service {

  constructor(private modalService: NgbModal) { }

  AbrirModal(tClasificacion3Actual: Clasificacion3): Promise<any> {

    return new Promise((resolve, reject) => {

      const modalRef = this.modalService.open(FwModalClasificaciones4Component, { size: 'lg' });
      modalRef.componentInstance.tTituloModal = "Modal de subcategoria";
      modalRef.componentInstance.tMensajeModal = "";
      modalRef.componentInstance.tClasificacion3 = tClasificacion3Actual;

      modalRef.result.then(result => {

        resolve(result);

      }, reason => {

        reject(reason);

      });

    })

  }

}
