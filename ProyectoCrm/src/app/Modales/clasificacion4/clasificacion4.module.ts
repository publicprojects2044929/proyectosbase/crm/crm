import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/Shared/shared.module'
import { NgbModule, NgbActiveModal, NgbAlertConfig } from '@ng-bootstrap/ng-bootstrap';

import { FwModalClasificaciones4Component } from 'src/app/Modales/clasificacion4/fw-modal-clasificaciones4/fw-modal-clasificaciones4.component'

@NgModule({
  declarations: [
    FwModalClasificaciones4Component
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    SharedModule
  ],
  exports: [
    FwModalClasificaciones4Component
  ],
  entryComponents: [
    FwModalClasificaciones4Component
  ],
  providers: [ NgbActiveModal, NgbAlertConfig ]
})

export class Clasificacion4ModalModule { }
