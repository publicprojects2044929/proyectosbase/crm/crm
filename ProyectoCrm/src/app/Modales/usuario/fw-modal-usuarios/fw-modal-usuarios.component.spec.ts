import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwModalUsuariosComponent } from './fw-modal-usuarios.component';

describe('FwModalUsuariosComponent', () => {
  let component: FwModalUsuariosComponent;
  let fixture: ComponentFixture<FwModalUsuariosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwModalUsuariosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwModalUsuariosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
