import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FwModalUsuariosComponent } from '../fw-modal-usuarios/fw-modal-usuarios.component'

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

  constructor(private modalService: NgbModal) { }

  AbrirModal(): Promise<any> {

    return new Promise((resolve, reject) => {

      const modalRef = this.modalService.open(FwModalUsuariosComponent, { size: 'lg' });
      modalRef.componentInstance.tTituloModal = "Modal Asesores";
      modalRef.componentInstance.tMensajeModal = "";

      modalRef.result.then(result => {

        resolve(result);

      }, reason => {

        reject(reason);

      });

    })

  }

}
