import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/Shared/shared.module';
import { NgbModule, NgbActiveModal, NgbAlertConfig } from '@ng-bootstrap/ng-bootstrap';

import { FwModalUsuariosComponent } from 'src/app/Modales/usuario/fw-modal-usuarios/fw-modal-usuarios.component';

@NgModule({
  declarations: [
    FwModalUsuariosComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    SharedModule
  ],
  exports: [
    FwModalUsuariosComponent
  ],
  entryComponents: [
    FwModalUsuariosComponent
  ],
  providers: [ NgbActiveModal, NgbAlertConfig ]
})
export class UsuarioModalModule { }
