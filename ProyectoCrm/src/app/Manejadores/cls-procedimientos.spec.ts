import { ManejarImagenes, ManejarFechas, ManejarArchivo, ManejarPdf, Validaciones } from './cls-procedimientos';
import { formatDate } from '@angular/common';

fdescribe('clsProcedimientos', () => {

  describe('ManejarImagenes', () => {

    let tClass: ManejarImagenes;

    beforeEach(() => {

      tClass = new ManejarImagenes();

    });

    it('should create an instance for ManejarImagenes', () => {

      expect(tClass).toBeTruthy();

    });

  });

  describe('ManejarFechas', () => {

    let tClass: ManejarFechas;

    beforeEach(() => {

      tClass = new ManejarFechas();

    });

    it('should create an instance for ManejarFechas', () => {

      expect(tClass).toBeTruthy();

    });

    it('Probamos la función formatear fecha', () => {

      const myDate = new Date();
      const locale = 'en-US';
      const tFecha = formatDate(myDate, 'yyyy/MM/dd', locale);
      expect(tClass.Formatear_Fecha(new Date(), "yyyy/MM/dd")).toBe(tFecha);

    });

    it('Probamos la función Seleccionar_Fecha', () => {

      const myDate = new Date();
      const locale = 'en-US';
      const tFecha = +formatDate(myDate, 'yyyyMMdd', locale);

      expect(tClass.Seleccionar_Fecha(new Date())).toBe(tFecha);

    });

  });

  describe('ManejarArchivo', () => {

    let tClass: ManejarArchivo;

    beforeEach(() => {

      tClass = new ManejarArchivo();

    });

    it('should create an instance for ManejarArchivo', () => {

      expect(tClass).toBeTruthy();

    });

  });

  describe('ManejarPdf', () => {

    let tClass: ManejarPdf;

    beforeEach(() => {

      tClass = new ManejarPdf();

    });

    it('should create an instance for ManejarPdf', () => {

      expect(tClass).toBeTruthy();

    });

  });

  describe('Validaciones', () => {

    let tClass: Validaciones;

    beforeEach(() => {

      tClass = new Validaciones();

    });

    it('should create an instance for Validaciones', () => {

      expect(tClass).toBeTruthy();

    });

  });
  
});


