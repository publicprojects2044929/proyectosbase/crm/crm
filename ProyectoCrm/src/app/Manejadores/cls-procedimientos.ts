import { formatDate } from '@angular/common';
import * as XLSX from 'xlsx';
import { Configuracion } from '../Clases/EstructuraConfiguracion'
import Datos from 'src/Configuracion/config.json';
import { Observable } from 'rxjs';
import { Reportes } from '../Clases/Estructura';
import { map } from 'rxjs/operators';

export class ManejarImagenes {

  public tImagen: string = "";
  public tImagenes: string[] = [];

  public Cargar_Archivo(tArchivo: any) {

    var file = tArchivo.dataTransfer ? tArchivo.dataTransfer.files[0] : tArchivo.target.files[0];
    var fr = new FileReader();
    fr.onload = this.Procesar_Archivo.bind(this);
    fr.readAsDataURL(file);

  }

  public Cargar_Archivos(tArchivo: any): string[] {

    var files = tArchivo.dataTransfer ? tArchivo.dataTransfer.files : tArchivo.target.files;

    for (let file of files) {

      var fr = new FileReader();
      fr.onload = this.Procesar_Archivos.bind(this);
      fr.readAsDataURL(file);

    }

    return this.tImagenes;

  }

  private Procesar_Archivo(e) {

    let reader = e.target;
    this.tImagen = reader.result;

  }

  private Procesar_Archivos(e) {

    let reader = e.target;
    this.tImagenes.push(reader.result);

  }

  public ObtenerImagen(): string {

    // return this.tImagen;

    if (this.tImagen == "") {

      return "assets/Imagen.svg";

    }
    else {

      return this.tImagen;

    }

  }

  public ObtenerImagenes(tImgenArr: string): string {

    // return this.tImagen;

    if (tImgenArr == "") {

      return "assets/image.png";

    }
    else {

      return tImgenArr;

    }

  }

  public obtenerImagen64(): string {

    var tImagenSplit: string[] = this.tImagen.split(",");
    var tImagen64: string = "";

    if (tImagenSplit.length > 1) {

      tImagen64 = tImagenSplit[1];

    }

    return tImagen64;

  }

  public obtenerImagenes64(tImgenArr: string): string {

    var tImagenSplit: string[] = tImgenArr.split(",");
    var tImagen64: string = "";

    if (tImagenSplit.length > 1) {

      tImagen64 = tImagenSplit[1];

    }

    return tImagen64;

  }

  public ObtenerImagen64$(): Observable<any> {

    const tImagenObs = new Observable(Obs => {

      setTimeout(() => {

        Obs.next(this.tImagen);

      }, 5000);

    });

    return tImagenObs;

  }

}

export class ManejarFechas {

  Seleccionar_Fecha(tFecha: any): number {

    const format = 'yyyyMMdd';
    const myDate = tFecha;
    const locale = 'en-US';
    return +formatDate(myDate, format, locale);

  };

  Formatear_Fecha(tFecha: any, tFormato: string): string {

    const myDate = tFecha;
    const locale = 'en-US';
    return formatDate(myDate, tFormato, locale);

  }

}

export class ManejarArchivo {

  public tExcelDatos$: Observable<any> = new Observable<any>();

  public Cargar_Archivo(tArchivo: any) {

    var file = tArchivo.dataTransfer ? tArchivo.dataTransfer.files[0] : tArchivo.target.files[0];
    var fr = new FileReader();
    fr.onload = this.Procesar_Archivo.bind(this);
    fr.readAsBinaryString(file);

  }

  private Procesar_Archivo(e) {

    let workBook = null;
    let jsonData = null;
    let reader = e.target;

    const data = reader.result;
    workBook = XLSX.read(data, { type: 'binary' });

    jsonData = workBook.SheetNames.reduce((initial, name) => {
      const sheet = workBook.Sheets[name];
      initial[name] = XLSX.utils.sheet_to_json(sheet);
      return initial;
    }, {});

    this.tExcelDatos$ = jsonData; //'JSON.stringify(jsonData);

  }

  public ObtenerDatos(): Observable<any> {

    const tArchivoObs = new Observable(Obs => {

      setTimeout(() => {

        Obs.next(this.tExcelDatos$);

      }, 5000);

    });

    return tArchivoObs.pipe(

      map((tDato) => {

        let tPropiedad: string = Object.keys(tDato)[0] || "";
        tDato[tPropiedad].forEach(tElemento => {

          Object.keys(tElemento).forEach((tLlave, tPos) => {

            var tPrimerCaracter: string = tLlave.charAt(0).toUpperCase() || "";
            var tOtrosCaracter: string = tLlave.slice(1).toLowerCase() || "";
            let tLlaveTransf: string = tPrimerCaracter + tOtrosCaracter;

            tElemento[tLlaveTransf] = tElemento[tLlave];
            delete tElemento[tLlave];

          });

        });

        return tDato;

      })

    ); //this.tExcelDatos$.pipe();

  }

  public GenerarExcel(tDatos: any, tNombreExcel: string, tNombrePagina: string) {

    const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(tDatos);

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, tNombrePagina);

    /* save to file */
    XLSX.writeFile(wb, tNombreExcel);

  }

}

export class ManejarPdf {

  tConfiguracion: Configuracion = Datos;

  public AbrirPdf(Reporte: Reportes) {

    switch (Reporte.Formato) {
      case 'string64':

        window.open('data:application/pdf;base64,' + Reporte.Reporte, "_blank");
        break;

      default:

        window.open(this.tConfiguracion.UrlPdf + Reporte.Reporte, "_blank");
        break;

    }

    // window.open(this.tConfiguracion.UrlPdf + NombrePdf, "_blank");

  }

}

export class ManejarColores {

  hexToRGB(hex, alpha): string {

    let r = parseInt(hex.slice(1, 3), 16),
      g = parseInt(hex.slice(3, 5), 16),
      b = parseInt(hex.slice(5, 7), 16);

    if (alpha) {
      return "rgba(" + r + ", " + g + ", " + b + ", " + alpha + ")";
    } else {
      return "rgb(" + r + ", " + g + ", " + b + ")";
    }

  }

}

export class Validaciones {

  public nombrePattern: any = /^[a-z0-9A-ZñÑ\,\áàéèíìóòúùý\s()\-\&]*$/;
  public emailPattern: any = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  public ValidarEmail(tEmail: string): boolean {

    let isValid: boolean = false;
    isValid = this.emailPattern.test(tEmail);
    return isValid

  }
}
