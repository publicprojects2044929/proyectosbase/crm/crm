import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwFabricantePage } from './fw-fabricante.page';

describe('FwFabricantePage', () => {
  let component: FwFabricantePage;
  let fixture: ComponentFixture<FwFabricantePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwFabricantePage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwFabricantePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
