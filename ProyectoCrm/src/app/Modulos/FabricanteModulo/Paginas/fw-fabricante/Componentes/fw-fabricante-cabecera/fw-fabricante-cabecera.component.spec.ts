import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwFabricanteCabeceraComponent } from './fw-fabricante-cabecera.component';

describe('FwFabricanteCabeceraComponent', () => {
  let component: FwFabricanteCabeceraComponent;
  let fixture: ComponentFixture<FwFabricanteCabeceraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwFabricanteCabeceraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwFabricanteCabeceraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
