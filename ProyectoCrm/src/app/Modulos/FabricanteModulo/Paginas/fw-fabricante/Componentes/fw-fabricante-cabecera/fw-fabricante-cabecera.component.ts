import { Component, OnInit, Input } from '@angular/core';
import { Fabricante } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-fabricante-cabecera',
  templateUrl: './fw-fabricante-cabecera.component.html',
  styleUrls: ['./fw-fabricante-cabecera.component.scss']
})
export class FwFabricanteCabeceraComponent implements OnInit {

  @Input('Fabricante') tFabricante: Fabricante = new Fabricante();

  constructor() { }

  ngOnInit(): void {
  }

}
