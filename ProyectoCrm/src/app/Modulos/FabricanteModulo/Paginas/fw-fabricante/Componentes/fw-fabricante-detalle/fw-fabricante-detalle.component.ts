import { Component, OnInit, Input, ViewChild, OnChanges, SimpleChanges } from '@angular/core';
import { Fabricante, Configuracion } from 'src/app/Clases/Estructura';
import { ManejarImagenes } from 'src/app/Manejadores/cls-procedimientos';

@Component({
  selector: 'app-fw-fabricante-detalle',
  templateUrl: './fw-fabricante-detalle.component.html',
  styleUrls: ['./fw-fabricante-detalle.component.scss']
})
export class FwFabricanteDetalleComponent implements OnInit, OnChanges {

  @ViewChild('flArchivo', { static: true }) tControlArchivo: any;

  @Input('Fabricante') tFabricante: Fabricante = new Fabricante();
  @Input('Configuracion') tConfiguracion: Configuracion = new Configuracion();
  @Input('ManejarImagenes') tManejarImagen: ManejarImagenes = new ManejarImagenes();

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {

    if ('tFabricante' in changes) {
      this.tControlArchivo.nativeElement.value = "";
    }

  }

  BloquearImagen(): boolean {

    if (this.tConfiguracion.Valor === "S") {

      return false;

    }
    else {

      return true;

    }

  }

}
