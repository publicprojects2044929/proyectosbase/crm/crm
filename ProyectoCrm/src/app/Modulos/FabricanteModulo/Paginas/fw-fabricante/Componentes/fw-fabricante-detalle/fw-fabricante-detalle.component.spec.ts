import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwFabricanteDetalleComponent } from './fw-fabricante-detalle.component';

describe('FwFabricanteDetalleComponent', () => {
  let component: FwFabricanteDetalleComponent;
  let fixture: ComponentFixture<FwFabricanteDetalleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwFabricanteDetalleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwFabricanteDetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
