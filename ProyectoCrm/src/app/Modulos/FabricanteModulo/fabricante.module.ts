import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from 'src/app/Shared/shared.module';

import { FabricanteRoutingModule } from './fabricante-routing.module';
import { FwFabricantePage } from './Paginas/fw-fabricante/fw-fabricante.page';
import { FwFabricanteCabeceraComponent } from './Paginas/fw-fabricante/Componentes/fw-fabricante-cabecera/fw-fabricante-cabecera.component';
import { FwFabricanteDetalleComponent } from './Paginas/fw-fabricante/Componentes/fw-fabricante-detalle/fw-fabricante-detalle.component';

@NgModule({
  declarations: [FwFabricantePage, FwFabricanteCabeceraComponent, FwFabricanteDetalleComponent],
  imports: [
    CommonModule,
    FabricanteRoutingModule,
    SharedModule,
    FormsModule,
    NgbModule
  ]
})
export class FabricanteModule { }
