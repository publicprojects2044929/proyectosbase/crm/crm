import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from 'src/app/Shared/shared.module'

import { EstadoRoutingModule } from './estado-routing.module';
import { FwEstadoPage } from './Paginas/fw-estado/fw-estado.page';
import { FwEstadoCabeceraComponent } from './Paginas/fw-estado/Componentes/fw-estado-cabecera/fw-estado-cabecera.component';
import { FwEstadoDetalleComponent } from './Paginas/fw-estado/Componentes/fw-estado-detalle/fw-estado-detalle.component';

@NgModule({
  declarations: [FwEstadoPage, FwEstadoCabeceraComponent, FwEstadoDetalleComponent],
  imports: [
    CommonModule,
    EstadoRoutingModule,
    SharedModule,
    FormsModule,
    NgbModule
  ]
})
export class EstadoModule { }
