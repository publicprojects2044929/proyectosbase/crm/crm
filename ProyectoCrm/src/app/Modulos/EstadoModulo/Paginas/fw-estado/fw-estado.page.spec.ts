import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwEstadoPage } from './fw-estado.page';

describe('FwEstadoPage', () => {
  let component: FwEstadoPage;
  let fixture: ComponentFixture<FwEstadoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwEstadoPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwEstadoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
