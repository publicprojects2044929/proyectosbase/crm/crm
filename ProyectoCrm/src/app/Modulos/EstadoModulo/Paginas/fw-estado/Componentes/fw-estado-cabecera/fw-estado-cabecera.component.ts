import { Component, OnInit, Input } from '@angular/core';
import { Estado } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-estado-cabecera',
  templateUrl: './fw-estado-cabecera.component.html',
  styleUrls: ['./fw-estado-cabecera.component.scss']
})
export class FwEstadoCabeceraComponent implements OnInit {

  @Input('Estado') tEstado: Estado = new Estado();

  constructor() { }

  ngOnInit(): void {
  }

}
