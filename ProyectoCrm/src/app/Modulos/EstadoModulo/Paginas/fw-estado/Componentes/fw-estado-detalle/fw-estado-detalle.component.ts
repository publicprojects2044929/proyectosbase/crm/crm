import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Pais, Estado } from 'src/app/Clases/Estructura';
import { PaisesService } from 'src/app/Modales/pais/fw-modal-pais/paises.service';

@Component({
  selector: 'app-fw-estado-detalle',
  templateUrl: './fw-estado-detalle.component.html',
  styleUrls: ['./fw-estado-detalle.component.scss']
})
export class FwEstadoDetalleComponent implements OnInit, OnChanges {

  @Input('Estado') tEstado: Estado = new Estado();
  @Input('Paises') tPaises: Pais[] = [];
  @Input('Nuevo') tNuevo: boolean = false;

  tPaisActual: Pais = new Pais();

  constructor(private tPaisService: PaisesService) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {

    if('tPaises' in changes){

      this.tPaisActual = this.tPaises[0] || new Pais();
      this.CambiarPais();

    }

    if ('tEstado' in changes) {

      this.tPaisActual = this.tPaises.find(tPais => {
        return tPais.Corporacion === this.tEstado.Corporacion &&
          tPais.Empresa === this.tEstado.Empresa &&
          tPais.Codpais === this.tEstado.Codpais
      }) || this.tPaises[0] || new Pais();

      if (this.tEstado.Codpais === '') {
        this.CambiarPais();
      }

    }

  }

  //#region "Funciones Modal"

  AbrirModalPais(): void {

    this.tPaisService
      .AbrirModal()
      .then(result => {

        if (result.Resultado == "S") {

          var tPaisModal: Pais = result.Pais;
          this.tPaisActual = this.tPaises.find(tPais => {
            return tPais.Corporacion === tPaisModal.Corporacion &&
              tPais.Empresa === tPaisModal.Empresa &&
              tPais.Codpais === tPaisModal.Codpais
          }) || this.tPaises[0] || new Pais();
          this.CambiarPais();

        }

      }, reason => {



      });

  }

  //#endregion

  CambiarPais() {
    this.tEstado.Codpais = this.tPaisActual.Codpais;
  }

}
