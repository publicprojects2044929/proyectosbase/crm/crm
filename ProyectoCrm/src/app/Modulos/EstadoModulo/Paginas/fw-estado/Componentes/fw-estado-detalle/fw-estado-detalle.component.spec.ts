import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwEstadoDetalleComponent } from './fw-estado-detalle.component';

describe('FwEstadoDetalleComponent', () => {
  let component: FwEstadoDetalleComponent;
  let fixture: ComponentFixture<FwEstadoDetalleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwEstadoDetalleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwEstadoDetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
