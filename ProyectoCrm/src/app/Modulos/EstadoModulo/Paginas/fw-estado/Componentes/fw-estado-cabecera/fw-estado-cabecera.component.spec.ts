import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwEstadoCabeceraComponent } from './fw-estado-cabecera.component';

describe('FwEstadoCabeceraComponent', () => {
  let component: FwEstadoCabeceraComponent;
  let fixture: ComponentFixture<FwEstadoCabeceraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwEstadoCabeceraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwEstadoCabeceraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
