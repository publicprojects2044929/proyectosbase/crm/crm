import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormularioConfiguracion } from 'src/app/Clases/EstructuraConfiguracion';
import { Usuario, Empresas, Pais, Estado, Formulario, Filtros, Configuracion } from 'src/app/Clases/Estructura';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/Core/Session/session.service';
import { ToastrService } from 'ngx-toastr';
import { MensajesService } from 'src/app/Modales/mensajes/fw-modal-msj/mensajes.service';
import { ErroresService } from 'src/app/Modales/mensajes/fw-modal-msj/errores.service';
import { ConfirmacionService } from 'src/app/Modales/confirmacion/fw-modal-confirmacion/confirmacion.service';
import { environment } from 'src/environments/environment';
import { EstadoEntrada } from 'src/app/Clases/EstructuraEntrada';
import { Subscription } from 'rxjs';
import { ConfiguracionService } from 'src/app/Core/Configuracion/configuracion.service';

@Component({
  selector: 'app-fw-estado',
  templateUrl: './fw-estado.page.html',
  styleUrls: ['./fw-estado.page.scss']
})
export class FwEstadoPage implements OnInit, OnDestroy {

  tCodFormularioActual: string = "fEstado";
  ConfiguracionFormulario: FormularioConfiguracion = new FormularioConfiguracion();
  tPermisoAgregar: boolean = false;
  tPermisoEditar: boolean = false;
  tPermisoEliminar: boolean = false;
  tHabilitarGuardar: boolean = false;

  tUsuarioActual: Usuario = new Usuario();
  tEmpresaActual: Empresas = new Empresas();
  tEmpresa$: Subscription;
  tUsuario$: Subscription;

  tPaises: Pais[] = [];

  tTitulo: string = "Gestión de Estados";
  tNuevo: boolean = true;
  tPosicion: number = 0;
  tPagina: number = 1;
  tTamanoPag: number = 5;

  tEstado: Estado[] = [];
  tEstadoActual: Estado = new Estado();

  tFormularios: Formulario[] = [];

  tFiltros: Filtros[] = [];
  tFiltroActual: Filtros = new Filtros();
  tFiltroBusqueda: string = "";

  tConfiguracion: Configuracion = new Configuracion();
  tConfiguracion$: Subscription;
  tHayConfiguracionWs$: Subscription;
  tStrConfiguracion: string = 'Estado/Estado.Config.json';

  constructor(public Ws: ClsServiciosService,
    private router: Router,
    private tSesion: SessionService,
    private tConfiguracionService: ConfiguracionService,
    private toastr: ToastrService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService,
    private tConfServices: ConfirmacionService) {

    this.tFiltroBusqueda = "";
    this.tPosicion = 0;

    this.tFiltros = [

      { Codfiltro: "Codestado", Filtro: "Código" },
      { Codfiltro: "Descripcion", Filtro: "Descripción" },

    ]

  }

  ngOnInit() {

    this.tConfiguracion$ = this.tConfiguracionService
      .Consumir_Obtener_Configuracion(this.tStrConfiguracion)
      .subscribe((tConfiguracion: FormularioConfiguracion) => {

        this.ConfiguracionFormulario = tConfiguracion;
        this.tTitulo = this.ConfiguracionFormulario.Titulo;
        this.tErroresServices.tTituloMsj = this.ConfiguracionFormulario.ModalMensaje.Titulo;
        this.tMsjServices.tTituloMsj = this.ConfiguracionFormulario.ModalMensaje.Titulo;
        this.tConfServices.tTituloConf = this.ConfiguracionFormulario.ModalConfirmacion.Titulo;

      });

    this.tEmpresa$ = this.tSesion.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    })

    this.tUsuario$ = this.tSesion.tUsuarioActual$.subscribe((tUsuario: Usuario) => {

      this.tUsuarioActual = tUsuario;

    })

    var tTienePermiso: boolean = this.tSesion.ObtenerPermisoAcceso(this.tCodFormularioActual);
    this.tPermisoAgregar = this.tSesion.ObtenerPermisoAgregar();
    this.tPermisoEditar = this.tSesion.ObtenerPermisoEditar();
    this.tPermisoEliminar = this.tSesion.ObtenerPermisoEliminar();

    if (tTienePermiso == true) {

      this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
        .subscribe((tHayConfig: boolean) => {

          if (tHayConfig === true) {

            this.Buscar_Estados();
            this.BuscarPais();
            this.Nuevo();

          }

        });

    }
    else {

      this.tMsjServices.AbrirModalMsj(environment.msjSinAcceso);
      this.router.navigate(['/Home']);

    }

  }

  ngOnDestroy() {

    this.tEmpresa$.unsubscribe();
    this.tUsuario$.unsubscribe();
    this.tConfiguracion$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();

  }

  //#region "Funciones de busqueda"

  BuscarPais(): void {

    this.Ws
      .Paises()
      .Consumir_Obtener_Paises(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tPaises = Respuesta.Datos;

        }


      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Buscar_Estados(): void {

    this.Ws
      .Estados()
      .Consumir_Obtener_Estados(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa)
      .subscribe(Respuesta => {

        this.tEstado = [];


        if (Respuesta.Resultado == "N") {

          // this.tMsjServicesAbrirModalMsj(Respuesta.Mensaje);
          //

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.tEstado = Respuesta.Datos;
          this.tFiltroActual = this.tFiltros[0];

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  //#region "CRUD"

  Nuevo_Estado(): void {

    var tNuevoEstado: EstadoEntrada;
    tNuevoEstado = {

      "Id": 0,
      "Corporacion": this.tEmpresaActual.Corporacion,
      "Empresa": this.tEmpresaActual.Empresa,
      "Codpais": this.tEstadoActual.Codpais,
      "Codestado": "",
      "Descripcion": this.tEstadoActual.Descripcion,
      "Abreviacion": this.tEstadoActual.Abreviacion,
      "Usuariocreador": this.tUsuarioActual.Codusuario,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }


    this.Ws
      .Estados()
      .Consumir_Crear_Estado(tNuevoEstado)
      .subscribe(Respuesta => {


        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj("Hay un problema de conexión con el servidor. Por favor contacte con sistemas.");


        }
        else {

          this.toastr.success('El estado se ha creado de manera exitosa!')
          this.tFiltroActual = this.tFiltros[0];
          this.Buscar_Estados();
          this.Nuevo();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Modificar_Estado(): void {

    var tModificarEstado: EstadoEntrada;
    tModificarEstado = {
      "Id": 0,
      "Corporacion": this.tEmpresaActual.Corporacion,
      "Empresa": this.tEmpresaActual.Empresa,
      "Codpais": this.tEstadoActual.Codpais,
      "Codestado": this.tEstadoActual.Codestado,
      "Descripcion": this.tEstadoActual.Descripcion,
      "Abreviacion": this.tEstadoActual.Abreviacion,
      "Usuariocreador": this.tUsuarioActual.Codusuario,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }



    this.Ws
      .Estados()
      .Consumir_Modificar_Estado(tModificarEstado)
      .subscribe(Respuesta => {


        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj("Hay un problema de conexión con el servidor. Por favor contacte con sistemas.");

        }
        else {

          this.toastr.success('El estado se ha modificado de manera exitosa!')
          this.tFiltroActual = this.tFiltros[0];
          this.Buscar_Estados();
          this.Nuevo();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Eliminar_Estado(): void {

    var tEliminarEstado: EstadoEntrada;
    tEliminarEstado = {
      "Id": 0,
      "Corporacion": this.tEmpresaActual.Corporacion,
      "Empresa": this.tEmpresaActual.Empresa,
      "Codpais": this.tEstadoActual.Codpais,
      "Codestado": this.tEstadoActual.Codestado,
      "Descripcion": this.tEstadoActual.Descripcion,
      "Abreviacion": this.tEstadoActual.Abreviacion,
      "Usuariocreador": this.tUsuarioActual.Codusuario,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }

    this.Ws
      .Estados()
      .Consumir_Eliminar_Estado(tEliminarEstado)
      .subscribe(Respuesta => {


        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj("Hay un problema de conexión con el servidor. Por favor contacte con sistemas.");


        }
        else {

          this.toastr.success('El estado se ha eliminado de manera exitosa!')
          this.tFiltroActual = this.tFiltros[0];
          this.Buscar_Estados();
          this.Nuevo();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  Nuevo(): void {

    this.tHabilitarGuardar = this.tPermisoAgregar;
    this.tNuevo = true;
    this.tEstadoActual = new Estado();
    this.toastr.clear();
    this.toastr.info('Por favor llene los datos necesarios para registrar un nuevo Estado!');

  }

  Seleccionar_Estado(tEstadoActual_Sel: Estado, tPos: number): void {

    this.tHabilitarGuardar = this.tPermisoEditar;
    this.tNuevo = false;
    this.tPosicion = tPos;
    this.tEstadoActual = new Estado();
    Object.assign(this.tEstadoActual, tEstadoActual_Sel);
    this.tEstadoActual.Usuariomodificador = this.tUsuarioActual.Usuariomodificador;

  }

  Seleccionar_Estado_Eliminar(tEstado_Sel: Estado): void {

    // this.tControlArchio.nativeElement.value = "";
    this.tEstadoActual = new Estado();
    Object.assign(this.tEstadoActual, tEstado_Sel);
    this.tEstadoActual.Usuariomodificador = this.tUsuarioActual.Usuariomodificador;

    this.tConfServices
      .AbrirModalConf(this.ConfiguracionFormulario.ModalConfirmacion.Eliminacion)
      .then(Resultado => {

        switch (Resultado) {
          case "S": {

            this.Eliminar_Estado();
            break;

          }
        }

      });

  }

  Cancelar(): void {

    this.Nuevo();

  }

  ValidarFormulario(): boolean {

    let valido: boolean = false;

    if (this.tEstadoActual.Descripcion == "") {

      this.toastr.warning('Debe ingresar una descripción!', 'Disculpe!');
      return valido;

    }

    return true;

  }

  Agregar(): void {


    if (this.ValidarFormulario() == false) {
      return;
    }

    switch (this.tNuevo) {
      case true: {

        this.tConfServices
          .AbrirModalConf(this.ConfiguracionFormulario.ModalConfirmacion.Agregar)
          .then(Resultado => {

            switch (Resultado) {
              case "S": {

                this.Nuevo_Estado();
                break;

              }
            }

          });
        break;

      }
      default: {

        this.tConfServices
          .AbrirModalConf(this.ConfiguracionFormulario.ModalConfirmacion.Edicion)
          .then(Resultado => {

            switch (Resultado) {
              case "S": {

                this.Modificar_Estado();
                break;

              }
            }

          });
        break;

      }
    }

  }

}
