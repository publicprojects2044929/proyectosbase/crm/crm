import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FwPlantillaComponent  } from 'src/app/Shared/Formularios/fw-plantilla/fw-plantilla.component';
import { FwCambiarEmpresaPage } from './Paginas/fw-cambiar-empresa/fw-cambiar-empresa.page';

const routes: Routes = [
  {
    path: '',
    component: FwPlantillaComponent,
    children:[
      {
        path: '',
        component: FwCambiarEmpresaPage,
        data:{
          Codformulario: 'fCambiarEmpresa'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CambiarEmpresaModuloRoutingModule { }
