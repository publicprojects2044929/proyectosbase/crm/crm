import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwCambiarEmpresaPage } from './fw-cambiar-empresa.page';

describe('FwCambiarEmpresaPage', () => {
  let component: FwCambiarEmpresaPage;
  let fixture: ComponentFixture<FwCambiarEmpresaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwCambiarEmpresaPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwCambiarEmpresaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
