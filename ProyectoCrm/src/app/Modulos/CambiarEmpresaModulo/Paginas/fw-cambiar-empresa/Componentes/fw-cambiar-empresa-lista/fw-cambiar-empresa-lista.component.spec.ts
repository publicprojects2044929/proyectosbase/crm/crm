import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwCambiarEmpresaListaComponent } from './fw-cambiar-empresa-lista.component';

describe('FwCambiarEmpresaListaComponent', () => {
  let component: FwCambiarEmpresaListaComponent;
  let fixture: ComponentFixture<FwCambiarEmpresaListaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwCambiarEmpresaListaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwCambiarEmpresaListaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
