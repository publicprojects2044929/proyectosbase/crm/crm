import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Empresas_Usuario, Empresas } from 'src/app/Clases/Estructura';
import { SessionService } from 'src/app/Core/Session/session.service';

@Component({
  selector: 'app-fw-cambiar-empresa-lista',
  templateUrl: './fw-cambiar-empresa-lista.component.html',
  styleUrls: ['./fw-cambiar-empresa-lista.component.scss']
})
export class FwCambiarEmpresaListaComponent implements OnInit {

  @Input('EmpresaActual') tEmpresaActual: Empresas = new Empresas();
  @Input('Empresa') tEmpresa: Empresas_Usuario = new Empresas_Usuario();

  @Output('CambiarEmpresa') tCambiarEmpresa: EventEmitter<Empresas_Usuario> = new EventEmitter();

  constructor(private tSession: SessionService) { }

  ngOnInit(): void {
  }

  ValidarLicencia(): boolean{

    let tResultadoLic: any = this.tSession.VerificarLicencia(this.tEmpresa);

    return tResultadoLic.Resultado;

  }

  CambiarEmpresa(){

    this.tCambiarEmpresa.emit(this.tEmpresa);

  }

}
