import { Component, OnInit, OnDestroy } from "@angular/core";
import { Usuario, Empresas, Empresas_Usuario } from "src/app/Clases/Estructura";
import { Subscription } from "rxjs";
import { SessionService } from "src/app/Core/Session/session.service";
import { ClsServiciosService } from "src/app/Core/Servicios/cls-servicios.service";
import { ConfiguracionService } from "src/app/Core/Configuracion/configuracion.service";
import { ErroresService } from "src/app/Modales/mensajes/fw-modal-msj/errores.service";
import { MensajesService } from "src/app/Modales/mensajes/fw-modal-msj/mensajes.service";
import { FormularioConfiguracion } from "src/app/Clases/EstructuraConfiguracion";
import { environment } from "src/environments/environment";

@Component({
  selector: "app-fw-cambiar-empresa",
  templateUrl: "./fw-cambiar-empresa.page.html",
  styleUrls: ["./fw-cambiar-empresa.page.scss"],
})
export class FwCambiarEmpresaPage implements OnInit, OnDestroy {

  tUsuarioActual: Usuario = new Usuario();
  tEmpresaActual: Empresas = new Empresas();
  tEmpresa$: Subscription;
  tUsuario$: Subscription;

  tPosicion: number = 0;
  tPagina: number = 1;
  tTamanoPag: number = 10;

  tTitulo: string = "";
  tEmpresasUsuario: Empresas_Usuario[] = [];

  tFormularioConfig: FormularioConfiguracion = new FormularioConfiguracion();

  tConfiguracion$: Subscription;
  tHayConfiguracionWs$: Subscription;
  tStrConfiguracion: string = "CambiarEmpresa/CambiarEmpresa.Config.json";

  constructor(
    public Ws: ClsServiciosService,
    private tSession: SessionService,
    private tConfiguracionService: ConfiguracionService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService
  ) { }

  ngOnInit(): void {

    this.tConfiguracion$ = this.tConfiguracionService
      .Consumir_Obtener_Configuracion(this.tStrConfiguracion)
      .subscribe((tConfiguracion: FormularioConfiguracion) => {
        this.tFormularioConfig = tConfiguracion;
        this.tTitulo = this.tFormularioConfig.Titulo;
        this.tErroresServices.tTituloMsj = this.tFormularioConfig.ModalMensaje.Titulo;
        this.tMsjServices.tTituloMsj = this.tFormularioConfig.ModalMensaje.Titulo;
      });

    this.tEmpresa$ = this.tSession.tEmpresaActual$.subscribe(
      (tEmpresa: Empresas) => {
        this.tEmpresaActual = tEmpresa;
      }
    );

    this.tUsuario$ = this.tSession.tUsuarioActual$.subscribe(
      (tUsuario: Usuario) => {
        this.tUsuarioActual = tUsuario;
      }
    );

    this.tHayConfiguracionWs$ = this.Ws.tHayConfig$.subscribe(
      (tHayConfig: boolean) => {
        if (tHayConfig === true) {
          this.Ws.Usuario()
            .Consumir_Obtener_UsuarioEmpresa(
              this.tEmpresaActual.Corporacion,
              this.tUsuarioActual.Codusuario
            )
            .subscribe(
              (Respuesta) => {

                if (Respuesta.Resultado == "N") {

                  this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

                } else if (Respuesta.Resultado == "E") {

                  this.tMsjServices.AbrirModalMsj(environment.msjError);

                } else {

                  this.tEmpresasUsuario = Respuesta.Datos;

                }

              },
              (error) => {
                this.tErroresServices.MostrarError(error);
              }
            );
        }
      }
    );
  }

  ngOnDestroy(): void {
    this.tEmpresa$.unsubscribe();
    this.tUsuario$.unsubscribe();
  }

  Cambiar_Empresa(tEmpresaSel: Empresas_Usuario): void {

    let tResultadoLic: any = this.tSession.VerificarLicencia(tEmpresaSel);

    if(tResultadoLic.Resultado === false){

      this.tMsjServices.AbrirModalMsj(tResultadoLic.Mensaje);
      return

    }

    this.Ws.Usuario()
      .Consumir_Cambiar_Empresa(tEmpresaSel.Corporacion,
        tEmpresaSel.Empresa,
        this.tUsuarioActual.Codusuario)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        } else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        } else {

          let tUsuarioDatos: Usuario = Respuesta.Datos[0];
          this.tSession.Login(tUsuarioDatos, tEmpresaSel);

        }

      }, error => {
        this.tErroresServices.MostrarError(error);

      });

  }

}
