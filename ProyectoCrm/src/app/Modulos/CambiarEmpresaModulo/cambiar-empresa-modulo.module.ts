import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from 'src/app/Shared/shared.module';

import { CambiarEmpresaModuloRoutingModule } from './cambiar-empresa-modulo-routing.module';
import { FwCambiarEmpresaPage } from './Paginas/fw-cambiar-empresa/fw-cambiar-empresa.page';
import { FwCambiarEmpresaListaComponent } from './Paginas/fw-cambiar-empresa/Componentes/fw-cambiar-empresa-lista/fw-cambiar-empresa-lista.component';

@NgModule({
  declarations: [FwCambiarEmpresaPage, FwCambiarEmpresaListaComponent],
  imports: [
    CommonModule,
    CambiarEmpresaModuloRoutingModule,
    SharedModule,
    FormsModule,
    NgbModule
  ]
})
export class CambiarEmpresaModuloModule { }
