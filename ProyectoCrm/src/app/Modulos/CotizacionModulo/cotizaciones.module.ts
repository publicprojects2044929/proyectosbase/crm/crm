import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from 'src/app/Shared/shared.module';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { BsDatepickerModule } from 'ngx-bootstrap';

import { CotizacionesRoutingModule } from './cotizaciones-routing.module';
import { FwCotizacionesPage } from './Paginas/fw-cotizaciones/fw-cotizaciones.page';
import { FwCotizacionesMonedaComponent } from './Paginas/fw-cotizaciones/Componentes/fw-cotizaciones-moneda/fw-cotizaciones-moneda.component';
import { FwCotizacionesOtrasMonedasComponent } from './Paginas/fw-cotizaciones/Componentes/fw-cotizaciones-otras-monedas/fw-cotizaciones-otras-monedas.component';
import { FwCotizacionesHistorialComponent } from './Paginas/fw-cotizaciones/Componentes/fw-cotizaciones-historial/fw-cotizaciones-historial.component';
import { FwCotizacionesGraficoComponent } from './Paginas/fw-cotizaciones/Componentes/fw-cotizaciones-grafico/fw-cotizaciones-grafico.component';


@NgModule({
  declarations: [FwCotizacionesPage, FwCotizacionesMonedaComponent, FwCotizacionesOtrasMonedasComponent, FwCotizacionesHistorialComponent, FwCotizacionesGraficoComponent],
  imports: [
    CommonModule,
    CotizacionesRoutingModule,
    SharedModule,
    FormsModule,
    NgbModule,
    NgMultiSelectDropDownModule.forRoot(),
    BsDatepickerModule.forRoot()
  ]
})
export class CotizacionesModule { }
