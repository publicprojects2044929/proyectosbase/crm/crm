import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwCotizacionesPage } from './fw-cotizaciones.page';

describe('FwCotizacionesPage', () => {
  let component: FwCotizacionesPage;
  let fixture: ComponentFixture<FwCotizacionesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwCotizacionesPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwCotizacionesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
