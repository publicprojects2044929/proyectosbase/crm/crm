import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormularioConfiguracion } from 'src/app/Clases/EstructuraConfiguracion';
import { ManejarFechas } from 'src/app/Manejadores/cls-procedimientos';
import { Usuario, Empresas, Filtros, Moneda, MonedaTasas } from 'src/app/Clases/Estructura';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/Core/Session/session.service';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { ToastrService } from 'ngx-toastr';
import { ErroresService } from 'src/app/Modales/mensajes/fw-modal-msj/errores.service';
import { MensajesService } from 'src/app/Modales/mensajes/fw-modal-msj/mensajes.service';
import { ConfirmacionService } from 'src/app/Modales/confirmacion/fw-modal-confirmacion/confirmacion.service';
import { environment } from 'src/environments/environment';
import { MonedaTasaEntrada } from 'src/app/Clases/EstructuraEntrada';
import { MonedaService } from 'src/app/Modales/moneda/fw-modal-monedas/moneda.service';
import { AreaService } from 'src/app/Core/Graficos/Area/area.service';
import { Subscription } from 'rxjs';
import { ConfiguracionService } from 'src/app/Core/Configuracion/configuracion.service';
import { LineaService } from 'src/app/Core/Graficos/Linea/linea.service';
import { Linea, DataSetLinea } from 'src/app/Clases/EstructuraGrafico';

@Component({
  selector: 'app-fw-cotizaciones',
  templateUrl: './fw-cotizaciones.page.html',
  styleUrls: ['./fw-cotizaciones.page.scss']
})
export class FwCotizacionesPage implements OnInit, OnDestroy {

  tCodFormularioActual: string = "fCotizacion";
  tConfiguracionFormulario: FormularioConfiguracion = new FormularioConfiguracion();
  tPermisoAgregar: boolean = false;
  tPermisoEditar: boolean = false;
  tPermisoEliminar: boolean = false;
  tHabilitarGuardar: boolean = false;

  tUsuarioActual: Usuario = new Usuario();
  tEmpresaActual: Empresas = new Empresas();
  tEmpresa$: Subscription;
  tUsuario$: Subscription;

  tTitulo: string = "Cotizaciones";
  tNuevo: boolean = true;

  tMonedas: Moneda[] = [];
  tMonedaActual: Moneda = new Moneda();
  tOtraMonedaActual: Moneda = new Moneda();
  tOtrasMonedasActual: Moneda[] = [];
  tTasasActual: MonedaTasas[] = [];

  tConfiguracion$: Subscription;
  tHayConfiguracionWs$: Subscription;
  tStrConfiguracion: string = 'Cotizaciones/Cotizaciones.Config.json';

  constructor(public router: Router,
    private tSesion: SessionService,
    private tConfiguracionService: ConfiguracionService,
    public Ws: ClsServiciosService,
    private toastr: ToastrService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService,
    private tConfServices: ConfirmacionService,
    private tMonedaService: MonedaService) {

  }

  ngOnInit() {

    this.tConfiguracion$ = this.tConfiguracionService
      .Consumir_Obtener_Configuracion(this.tStrConfiguracion)
      .subscribe((tConfiguracion: FormularioConfiguracion) => {

        this.tConfiguracionFormulario = tConfiguracion;
        this.tTitulo = this.tConfiguracionFormulario.Titulo;
        this.tErroresServices.tTituloMsj = this.tConfiguracionFormulario.ModalMensaje.Titulo;
        this.tMsjServices.tTituloMsj = this.tConfiguracionFormulario.ModalMensaje.Titulo;
        this.tConfServices.tTituloConf = this.tConfiguracionFormulario.ModalConfirmacion.Titulo;

      });

    this.tEmpresa$ = this.tSesion.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    })

    this.tUsuario$ = this.tSesion.tUsuarioActual$.subscribe((tUsuario: Usuario) => {

      this.tUsuarioActual = tUsuario;

    })

    var tTienePermiso: boolean = this.tSesion.ObtenerPermisoAcceso(this.tCodFormularioActual);
    this.tPermisoAgregar = this.tSesion.ObtenerPermisoAgregar();
    this.tPermisoEditar = this.tSesion.ObtenerPermisoEditar();
    this.tPermisoEliminar = this.tSesion.ObtenerPermisoEliminar();

    if (tTienePermiso == true) {

      this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
        .subscribe((tHayConfig: boolean) => {

          if (tHayConfig === true) {

            this.Buscar_Monedas();
            this.Nuevo();

          }

        });

    }
    else {

      this.tMsjServices.AbrirModalMsj(environment.msjSinAcceso);
      this.router.navigate(['/Home']);

    }

  }

  ngOnDestroy() {

    this.tEmpresa$.unsubscribe();
    this.tUsuario$.unsubscribe();
    this.tConfiguracion$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();

  }

  //#region "Modal"

  AbrirModalMonedas(): void {

    this.tMonedaService.AbrirModal().then(Resultado => {

      if (Resultado.Resultado == "S") {

        var tMonedaModal: Moneda = Resultado.Moneda;
        this.Buscar_Moneda(tMonedaModal.Codmoneda);

      }

    });

  }


  //#endregion

  //#region "Funciones de busqueda"

  Buscar_Monedas(): void {

    this.Ws
      .Monedas()
      .Consumir_Obtener_MonedaFull(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa)
      .subscribe(Respuesta => {

        this.tMonedas = [];

        if (Respuesta.Resultado == "N") {

          // this.tMensajeMsj = Respuesta.Mensaje;

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tMonedas = Respuesta.Datos;

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Buscar_Moneda(tCodmoneda: string): void {

    this.Ws
      .Monedas()
      .Consumir_Obtener_MonedaFullPorCodigo(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        tCodmoneda)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          // this.tMensajeMsj = Respuesta.Mensaje;

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.Seleccionar_Moneda(Respuesta.Datos[0]);

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  //#region "CRUD"

  Nueva_Cotizacion(): void {

  }

  Modificar_Cotizacion(): void {

  }

  Eliminar_Cotizacion(): void {

  }

  //#endregion

  Nuevo(): void {

    this.tHabilitarGuardar = this.tPermisoAgregar;
    this.tNuevo = true;
    this.tMonedaActual = new Moneda();
    this.tTasasActual = [];
    this.tOtrasMonedasActual = [];

    this.toastr.info('Por favor llene los datos necesarios para registrar una nueva moneda!');


  }

  Cancelar(): void {

    this.Nuevo();

  }

  Seleccionar_Moneda(tMoneda: Moneda) {

    this.tMonedaActual = tMoneda;
    this.tOtrasMonedasActual = [];

    this.tMonedas.forEach(tMoneda => {

      if (tMoneda.Corporacion === this.tMonedaActual.Corporacion &&
        tMoneda.Empresa === this.tMonedaActual.Empresa &&
        tMoneda.Codmoneda !== this.tMonedaActual.Codmoneda) {

        // this.tOtrasMonedasActual.push(tMoneda);
        this.tOtrasMonedasActual = [
          ...this.tOtrasMonedasActual,
          tMoneda
        ]

      }

    })

  }

  Seleccionar_Cotizaciones(tMonedaSel: Moneda) {

    this.tOtraMonedaActual = new Moneda();

    this.tMonedas.forEach(tMoneda => {

      if (tMoneda.Codmoneda == tMonedaSel.Codmoneda) {

        this.tOtraMonedaActual = tMoneda;

      }

    });

    this.Buscar_Tasas();

  }

  Buscar_Tasas() {

    this.tTasasActual = [];

    this.tMonedaActual.LTasas.forEach(tTasa => {

      if (tTasa.Corporacion === this.tOtraMonedaActual.Corporacion &&
        tTasa.Empresa === this.tOtraMonedaActual.Empresa &&
        tTasa.Codmonedadestino === this.tOtraMonedaActual.Codmoneda) {

          this.tTasasActual = [
          ...this.tTasasActual,
          tTasa
        ]

      }

    });

  }

  Agregar_Cotizacion(tTasa: any) {

    let tNuevaCotizacion: MonedaTasaEntrada = {

      "Id": 0,
      "Corporacion": this.tEmpresaActual.Corporacion,
      "Empresa": this.tEmpresaActual.Empresa,
      "Codmonedaorigen": this.tMonedaActual.Codmoneda,
      "Codmonedadestino": this.tOtraMonedaActual.Codmoneda,
      "Tasa": tTasa,
      "Usuariocreador": this.tUsuarioActual.Codusuario,
      "Usuariomodificador": this.tUsuarioActual.Codusuario

    }

    this.Ws
      .MonedasTasas()
      .Consumir_Crear_MonedaTasa(tNuevaCotizacion)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tMonedaActual.LTasas = Respuesta.Datos;

          this.Buscar_Tasas();


        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

}
