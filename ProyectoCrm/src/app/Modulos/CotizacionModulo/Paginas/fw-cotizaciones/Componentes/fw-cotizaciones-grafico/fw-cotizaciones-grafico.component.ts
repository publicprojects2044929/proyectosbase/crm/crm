import { Component, OnInit, AfterViewInit, OnChanges, Input, SimpleChanges } from '@angular/core';
import { Linea, DataSetLinea } from 'src/app/Clases/EstructuraGrafico';
import { LineaService } from 'src/app/Core/Graficos/Linea/linea.service';
import { ManejarFechas } from 'src/app/Manejadores/cls-procedimientos';
import { MonedaTasas, Moneda } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-cotizaciones-grafico',
  templateUrl: './fw-cotizaciones-grafico.component.html',
  styleUrls: ['./fw-cotizaciones-grafico.component.scss']
})
export class FwCotizacionesGraficoComponent implements OnInit, OnChanges, AfterViewInit {

  @Input('Moneda') tMoneda: Moneda = new Moneda();
  @Input('OtraMoneda') tOtraMoneda: Moneda = new Moneda();
  @Input('Tasas') tTasas: MonedaTasas[] = [];

  tFechas: Date[];

  tElemento: any;
  tLineaDatos: Linea = new Linea();

  tManejarFechas: ManejarFechas = new ManejarFechas();

  constructor(private tLineaGraficoService: LineaService) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {

    if ('tTasas' in changes) {

      let tMaxFecha: Date = new Date();
      let tMinFecha: Date = new Date();
      let tMinFechaL: number = this.tManejarFechas.Seleccionar_Fecha(new Date());

      this.tTasas.forEach(tTasa => {

        if (tMinFechaL >= tTasa.Fecha_vigencial) {

          tMinFechaL = tTasa.Fecha_vigencial;
          tMinFecha = new Date(tTasa.Fecha_vigencia);

        }

      })

      this.tFechas = [
        tMinFecha,
        tMaxFecha
      ];

      if(this.tElemento){
        // this.tLineaGraficoService.ArmarGrafico(this.tElemento, this.tLineaDatos);
        this.Filtrar_Tasas();
      }


    }

  }

  ngAfterViewInit() {

    this.tElemento = document.getElementById("canvas");

  }

  Seleccionar_Fecha(tFechaSel: any) {

    this.Filtrar_Tasas();

  }

  Filtrar_Tasas() {

    this.tTasas.sort((a, b) => {

      return new Date(a.Fecha_vigencia).getTime() - new Date(b.Fecha_vigencia).getTime();

    })

    let tFechaIni: number = this.tManejarFechas.Seleccionar_Fecha((this.tFechas[0]));
    let tFechaFin: number = this.tManejarFechas.Seleccionar_Fecha((this.tFechas[1]));

    this.tLineaDatos = new Linea();
    let tDato: DataSetLinea = new DataSetLinea();
    tDato.Etiqueta = `[${this.tMoneda.Simbolo}] ${this.tMoneda.Descripcion} => [${this.tOtraMoneda.Simbolo}] ${this.tOtraMoneda.Descripcion}`;
    tDato.Color = "green";

    this.tTasas.forEach((tTasa, tPos) => {

      let tFechaTasa: number = tTasa.Fecha_vigencial;

      if (tFechaTasa >= tFechaIni && tFechaTasa <= tFechaFin) {

        let tFecha: string = this.tManejarFechas.Formatear_Fecha(tTasa.Fecha_vigencia, 'dd/MM/yyyy HH:mm:ss');
        tDato.Datos.push(tTasa.Tasa.toString());
        this.tLineaDatos.Etiquetas.push(tFecha);

      }

    });

    if (tDato.Datos.length <= 0) {

      let tFecha: string = this.tManejarFechas.Formatear_Fecha(new Date(), 'dd/MM/yyyy HH:mm:ss');
      tDato.Datos.push("1");
      this.tLineaDatos.Etiquetas.push(tFecha);

    }

    this.tLineaDatos.DataSet.push(tDato);
    this.tLineaGraficoService.ArmarGrafico(this.tElemento, this.tLineaDatos);

  }

}
