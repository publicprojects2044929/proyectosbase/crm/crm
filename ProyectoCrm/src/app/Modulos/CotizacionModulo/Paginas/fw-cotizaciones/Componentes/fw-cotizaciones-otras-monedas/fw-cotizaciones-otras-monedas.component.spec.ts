import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwCotizacionesOtrasMonedasComponent } from './fw-cotizaciones-otras-monedas.component';

describe('FwCotizacionesOtrasMonedasComponent', () => {
  let component: FwCotizacionesOtrasMonedasComponent;
  let fixture: ComponentFixture<FwCotizacionesOtrasMonedasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwCotizacionesOtrasMonedasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwCotizacionesOtrasMonedasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
