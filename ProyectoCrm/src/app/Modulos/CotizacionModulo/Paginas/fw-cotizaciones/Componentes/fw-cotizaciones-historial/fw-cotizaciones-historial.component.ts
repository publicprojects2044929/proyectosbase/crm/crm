import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MonedaTasas } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-cotizaciones-historial',
  templateUrl: './fw-cotizaciones-historial.component.html',
  styleUrls: ['./fw-cotizaciones-historial.component.scss']
})
export class FwCotizacionesHistorialComponent implements OnInit {

  @Input('Tasas') tTasas: MonedaTasas[] = [];
  @Input('PermisoAgregar') tPermisoAgregar: boolean = false;

  @Output('AgregarCotizacion') tAgregar: EventEmitter<any> = new EventEmitter();

  tPosicionTasa: number = 0;
  tPaginaTasa: number = 1;
  tTamanoTasa: number = 5;


  constructor() { }

  ngOnInit(): void {
  }

  Agregar(tTasa: any){
    this.tAgregar.emit(tTasa);
  }

}
