import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwCotizacionesMonedaComponent } from './fw-cotizaciones-moneda.component';

describe('FwCotizacionesMonedaComponent', () => {
  let component: FwCotizacionesMonedaComponent;
  let fixture: ComponentFixture<FwCotizacionesMonedaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwCotizacionesMonedaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwCotizacionesMonedaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
