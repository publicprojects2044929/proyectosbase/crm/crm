import { Component, OnInit, OnChanges, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { Moneda } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-cotizaciones-moneda',
  templateUrl: './fw-cotizaciones-moneda.component.html',
  styleUrls: ['./fw-cotizaciones-moneda.component.scss']
})
export class FwCotizacionesMonedaComponent implements OnInit, OnChanges {

  @Input('Moneda') tMoneda: Moneda = new Moneda();

  @Output('AbrirModal') tAbrir: EventEmitter<string> = new EventEmitter();

  tTituloDetalles: string = "";

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges){

    if('tMoneda' in changes){

      this.tTituloDetalles = "";

      if(this.tMoneda.Codmoneda !== ''){

        this.tTituloDetalles = `[ ${ this.tMoneda.Codmoneda } ] - ${ this.tMoneda.Descripcion }`;

      }

    }

  }

  Abrir(){
    this.tAbrir.emit('');
  }

}
