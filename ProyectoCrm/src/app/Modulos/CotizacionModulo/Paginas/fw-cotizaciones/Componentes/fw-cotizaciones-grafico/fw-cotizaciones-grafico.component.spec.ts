import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwCotizacionesGraficoComponent } from './fw-cotizaciones-grafico.component';

describe('FwCotizacionesGraficoComponent', () => {
  let component: FwCotizacionesGraficoComponent;
  let fixture: ComponentFixture<FwCotizacionesGraficoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwCotizacionesGraficoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwCotizacionesGraficoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
