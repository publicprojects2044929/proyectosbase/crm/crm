import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwCotizacionesHistorialComponent } from './fw-cotizaciones-historial.component';

describe('FwCotizacionesHistorialComponent', () => {
  let component: FwCotizacionesHistorialComponent;
  let fixture: ComponentFixture<FwCotizacionesHistorialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwCotizacionesHistorialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwCotizacionesHistorialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
