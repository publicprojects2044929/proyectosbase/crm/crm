import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Moneda } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-cotizaciones-otras-monedas',
  templateUrl: './fw-cotizaciones-otras-monedas.component.html',
  styleUrls: ['./fw-cotizaciones-otras-monedas.component.scss']
})
export class FwCotizacionesOtrasMonedasComponent implements OnInit {

  @Input('OtrasMonedas') tOtrasMonedas: Moneda[] = [];

  @Output('Seleccionar') tSeleccionar: EventEmitter<Moneda> = new EventEmitter();

  tPosicion: number = 0;
  tPagina: number = 1;
  tTamanoPag: number = 5;

  constructor() { }

  ngOnInit(): void {
  }

  Seleccionar(tMonedaSel: Moneda) {

    this.tSeleccionar.emit(tMonedaSel)

  }

}
