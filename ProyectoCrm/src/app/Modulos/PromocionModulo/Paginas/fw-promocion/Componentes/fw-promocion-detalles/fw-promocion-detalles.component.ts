import { Component, OnInit, OnChanges, Input, SimpleChanges } from '@angular/core';
import { Promocion, Estatus, Configuracion } from 'src/app/Clases/Estructura';
import { ManejarFechas, ManejarImagenes } from 'src/app/Manejadores/cls-procedimientos';

@Component({
  selector: 'app-fw-promocion-detalles',
  templateUrl: './fw-promocion-detalles.component.html',
  styleUrls: ['./fw-promocion-detalles.component.scss']
})
export class FwPromocionDetallesComponent implements OnInit, OnChanges {

  @Input('Promocion') tPromocion: Promocion = new Promocion();
  @Input('Configuracion') tConfiguracion: Configuracion = new Configuracion();
  @Input('Estatus') tEstatus: Estatus[] = [];
  @Input('ManejarFechas') tManejarFecha: ManejarFechas = new ManejarFechas();
  @Input('ManejarImagen') tManejarImagen: ManejarImagenes = new ManejarImagenes();

  tEstatusActual: Estatus = new Estatus();

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {

    if ('tEstatus' in changes) {

      this.tEstatusActual = this.tEstatus[0] || new Estatus();
      this.CambiarEstatus();

    }

    if ('tPromocion' in changes) {

      this.tEstatusActual = this.tEstatus.find(tEst => {
        return tEst.Corporacion === this.tPromocion.Corporacion &&
          tEst.Empresa === this.tPromocion.Empresa &&
          tEst.Codestatus === this.tPromocion.Estatus
      }) || this.tEstatus[0] || new Estatus();

      if(this.tPromocion.Estatus === ''){
        this.CambiarEstatus();
      }

    }

  }

  Seleccionar_Fecha(tFecha: any): void {
    this.tPromocion.Fecha_vigencial = this.tManejarFecha.Seleccionar_Fecha(tFecha);
  }

  CambiarEstatus(){
    this.tPromocion.Estatus = this.tEstatusActual.Codestatus;
  }


}
