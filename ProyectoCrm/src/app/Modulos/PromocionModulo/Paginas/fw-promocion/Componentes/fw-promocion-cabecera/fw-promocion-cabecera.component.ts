import { Component, OnInit, Input } from '@angular/core';
import { Promocion } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-promocion-cabecera',
  templateUrl: './fw-promocion-cabecera.component.html',
  styleUrls: ['./fw-promocion-cabecera.component.scss']
})
export class FwPromocionCabeceraComponent implements OnInit {

  @Input('Promocion') tPromocion: Promocion = new Promocion();

  constructor() { }

  ngOnInit(): void {
  }

}
