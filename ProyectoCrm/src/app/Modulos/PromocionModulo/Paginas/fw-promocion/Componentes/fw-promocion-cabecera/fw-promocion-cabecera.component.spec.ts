import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwPromocionCabeceraComponent } from './fw-promocion-cabecera.component';

describe('FwPromocionCabeceraComponent', () => {
  let component: FwPromocionCabeceraComponent;
  let fixture: ComponentFixture<FwPromocionCabeceraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwPromocionCabeceraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwPromocionCabeceraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
