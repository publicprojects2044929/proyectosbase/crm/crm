import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwPromocionDetallesComponent } from './fw-promocion-detalles.component';

describe('FwPromocionDetallesComponent', () => {
  let component: FwPromocionDetallesComponent;
  let fixture: ComponentFixture<FwPromocionDetallesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwPromocionDetallesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwPromocionDetallesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
