import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwPromocionPage } from './fw-promocion.page';

describe('FwPromocionPage', () => {
  let component: FwPromocionPage;
  let fixture: ComponentFixture<FwPromocionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwPromocionPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwPromocionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
