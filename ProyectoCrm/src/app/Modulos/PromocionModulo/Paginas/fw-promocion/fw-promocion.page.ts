import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/Core/Session/session.service';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { Filtros, Empresas, Usuario, Formulario, Promocion, Estatus, Configuracion } from 'src/app/Clases/Estructura';
import { PromocionEntrada } from 'src/app/Clases/EstructuraEntrada';
import { ManejarFechas, ManejarImagenes } from 'src/app/Manejadores/cls-procedimientos';
import { FormularioConfiguracion } from 'src/app/Clases/EstructuraConfiguracion';
import { ToastrService } from 'ngx-toastr';
import { ErroresService } from 'src/app/Modales/mensajes/fw-modal-msj/errores.service';
import { MensajesService } from 'src/app/Modales/mensajes/fw-modal-msj/mensajes.service';
import { ConfirmacionService } from 'src/app/Modales/confirmacion/fw-modal-confirmacion/confirmacion.service';
import { environment } from 'src/environments/environment';
import { isNullOrUndefined } from 'util';
import { Subscription } from 'rxjs';
import { ConfiguracionService } from 'src/app/Core/Configuracion/configuracion.service';

@Component({
  selector: 'app-fw-promocion',
  templateUrl: './fw-promocion.page.html',
  styleUrls: ['./fw-promocion.page.scss']
})
export class FwPromocionPage implements OnInit, OnDestroy {

  tCodFormularioActual: string = "fPromocion";

  tConfiguracionFormulario: FormularioConfiguracion = new FormularioConfiguracion();
  tPermisoAgregar: boolean = false;
  tPermisoEditar: boolean = false;
  tPermisoEliminar: boolean = false;
  tHabilitarGuardar: boolean = false;

  tUsuarioActual: Usuario = new Usuario();
  tEmpresaActual: Empresas = new Empresas();
  tEmpresa$: Subscription;
  tUsuario$: Subscription;

  tEstatus: Estatus[] = [];

  tTitulo: string = "Gestión de promociones";
  tNuevo: boolean = true;
  tPosicion: number = 0;
  tPagina: number = 1;
  tTamanoPag: number = 5;

  tPromocion: Promocion[] = [];
  tPromocionActual: Promocion = new Promocion();

  tFormularios: Formulario[] = [];

  tFiltros: Filtros[] = [];
  tFiltroActual: Filtros = new Filtros();
  tFiltroBusqueda: string = "";

  tConfiguracion: Configuracion = new Configuracion();
  tManejarImagen: ManejarImagenes = new ManejarImagenes();
  tManejarFecha: ManejarFechas = new ManejarFechas();

  tConfiguracion$: Subscription;
  tHayConfiguracionWs$: Subscription;
  tStrConfiguracion: string = 'Promocion/Promocion.Config.json';

  constructor(public router: Router,
    public Ws: ClsServiciosService,
    private tConfiguracionService: ConfiguracionService,
    private tSesion: SessionService,
    private toastr: ToastrService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService,
    private tConfServices: ConfirmacionService) {

    this.tFiltroBusqueda = "";
    this.tPosicion = 0;

    this.tFiltros = [

      { Codfiltro: "Codpromocion", Filtro: "Código" },
      { Codfiltro: "Titulo", Filtro: "Título" },

    ]

    this.tEstatus = [

      {
        "Id": 0,
        "Corporacion": "",
        "Empresa": "",
        "Codestatus": "",
        "Descripcion": "",
        "Secuencia": 0,
        "Tabla": "",
        "Hexadecimal": "",
        "Fecha_creacion": new Date(),
        "Fecha_creacionl": 0,
        "Fecha_modificacion": new Date(),
        "Fecha_modificacionl": 0,
        "Usuariocreador": "",
        "Usuariomodificador": ""
      }

    ];

  }

  ngOnInit() {

    this.tConfiguracion$ = this.tConfiguracionService
      .Consumir_Obtener_Configuracion(this.tStrConfiguracion)
      .subscribe((tConfiguracion: FormularioConfiguracion) => {

        this.tConfiguracionFormulario = tConfiguracion;
        this.tTitulo = this.tConfiguracionFormulario.Titulo;
        this.tErroresServices.tTituloMsj = this.tConfiguracionFormulario.ModalMensaje.Titulo;
        this.tMsjServices.tTituloMsj = this.tConfiguracionFormulario.ModalMensaje.Titulo;
        this.tConfServices.tTituloConf = this.tConfiguracionFormulario.ModalConfirmacion.Titulo;

      });


    this.tEmpresa$ = this.tSesion.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    })

    this.tUsuario$ = this.tSesion.tUsuarioActual$.subscribe((tUsuario: Usuario) => {

      this.tUsuarioActual = tUsuario;

    })

    var tTienePermiso: boolean = this.tSesion.ObtenerPermisoAcceso(this.tCodFormularioActual);
    this.tPermisoAgregar = this.tSesion.ObtenerPermisoAgregar();
    this.tPermisoEditar = this.tSesion.ObtenerPermisoEditar();
    this.tPermisoEliminar = this.tSesion.ObtenerPermisoEliminar();

    if (tTienePermiso == true) {

      this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
        .subscribe((tHayConfig: boolean) => {

          if (tHayConfig === true) {

            this.BuscarConfiguracion();
            this.Buscar_Promociones();
            this.BuscarEstatus();
            this.Nuevo();

          }

        });

    }
    else {

      this.tMsjServices.AbrirModalMsj(environment.msjSinAcceso);
      this.router.navigate(['/Home']);

    }

  }

  ngOnDestroy() {

    this.tEmpresa$.unsubscribe();
    this.tUsuario$.unsubscribe();
    this.tConfiguracion$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();

  }

  //#region "Funciones de busqueda"

  BuscarEstatus(): void {

    this.Ws
      .Estatus()
      .Consumir_Obtener_Estatus(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        'MR_PROMOCION')
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          // this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);
          //

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tEstatus = Respuesta.Datos;

        }


      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Buscar_Promociones(): void {


    this.Ws
      .Promociones()
      .Consumir_Obtener_Promocion(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa)
      .subscribe(Respuesta => {

        this.tPromocion = [];

        if (Respuesta.Resultado == "N") {

          // this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);
          //

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.tPromocion = Respuesta.Datos;
          this.tFiltroActual = this.tFiltros[0];

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  BuscarConfiguracion(): void {


    this.Ws
      .Configuraciones()
      .Consumir_Obtener_Configuracion(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        'MR_PROMOCION',
        'IMAGEN')
      .subscribe(Respuesta => {


        if (Respuesta.Resultado == "N") {

          // this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);
          //

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.tConfiguracion = Respuesta.Datos[0];

        }

      }, error => {


        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  //#region "CRUD"

  Nuevo_Promocion(): void {

    var tNuevoPromocion: PromocionEntrada;
    tNuevoPromocion = {
      "Id": 0,
      "Corporacion": this.tEmpresaActual.Corporacion,
      "Empresa": this.tEmpresaActual.Empresa,
      "Codpromocion": "",
      "Titulo": this.tPromocionActual.Titulo,
      "Subtitulo": this.tPromocionActual.Subtitulo,
      "Url": this.tPromocionActual.Url,
      "Estatus": this.tPromocionActual.Estatus,
      "Fecha_vigencial": this.tPromocionActual.Fecha_vigencial,
      "Imagen64": this.tManejarImagen.obtenerImagen64(),
      "Usuariocreador": this.tUsuarioActual.Codusuario,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }


    this.Ws
      .Promociones()
      .Consumir_Crear_Promocion(tNuevoPromocion)
      .subscribe(Respuesta => {


        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.toastr.success('La promoción se ha creado de manera exitosa!');
          this.tFiltroActual = this.tFiltros[0];
          this.Buscar_Promociones();
          this.Nuevo();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Modificar_Promocion(): void {

    var tModificarPromocion: PromocionEntrada;
    tModificarPromocion = {
      "Id": 0,
      "Corporacion": this.tPromocionActual.Corporacion,
      "Empresa": this.tPromocionActual.Empresa,
      "Codpromocion": this.tPromocionActual.Codpromocion,
      "Titulo": this.tPromocionActual.Titulo,
      "Subtitulo": this.tPromocionActual.Subtitulo,
      "Url": this.tPromocionActual.Url,
      "Estatus": this.tPromocionActual.Estatus,
      "Fecha_vigencial": this.tPromocionActual.Fecha_vigencial,
      "Imagen64": this.tManejarImagen.obtenerImagen64(),
      "Usuariocreador": this.tPromocionActual.Usuariocreador,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }


    this.Ws
      .Promociones()
      .Consumir_Modificar_Promocion(tModificarPromocion)
      .subscribe(Respuesta => {


        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.toastr.success('La promoción se ha modificado de manera exitosa!');
          this.tFiltroActual = this.tFiltros[0];
          this.Buscar_Promociones();
          this.Nuevo();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Eliminar_Promocion(): void {

    var tEliminarPromocion: PromocionEntrada;
    tEliminarPromocion = {
      "Id": 0,
      "Corporacion": this.tPromocionActual.Corporacion,
      "Empresa": this.tPromocionActual.Empresa,
      "Codpromocion": this.tPromocionActual.Codpromocion,
      "Titulo": this.tPromocionActual.Titulo,
      "Subtitulo": this.tPromocionActual.Subtitulo,
      "Url": this.tPromocionActual.Url,
      "Estatus": this.tPromocionActual.Estatus,
      "Fecha_vigencial": this.tPromocionActual.Fecha_vigencial,
      "Imagen64": "",
      "Usuariocreador": this.tPromocionActual.Usuariocreador,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }


    this.Ws
      .Promociones()
      .Consumir_Eliminar_Promocion(tEliminarPromocion)
      .subscribe(Respuesta => {


        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.toastr.success('La promoción se ha eliminado de manera exitosa!');
          this.tFiltroActual = this.tFiltros[0];
          this.Buscar_Promociones();
          this.Nuevo();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  Nuevo(): void {

    this.tHabilitarGuardar = this.tPermisoAgregar;
    this.tNuevo = true;

    this.tPromocionActual = new Promocion();
    this.tManejarImagen.tImagen = "";
    this.toastr.info('Por favor llene los datos necesarios para registrar una nueva promoción!');

  }

  Seleccionar_Promocion(tPromocionActual_Sel: Promocion, tPos: number): void {

    this.tHabilitarGuardar = this.tPermisoEditar;
    this.tNuevo = false;
    this.tPosicion = tPos;
    this.tPromocionActual = new Promocion();
    Object.assign(this.tPromocionActual, tPromocionActual_Sel);
    this.tPromocionActual.Usuariomodificador = this.tUsuarioActual.Usuariomodificador;
    this.tManejarImagen.tImagen = this.tPromocionActual.Url;

  }

  Seleccionar_Promocion_Eliminar(tPromocion_Sel: Promocion): void {

    this.tPromocionActual = new Promocion();
    Object.assign(this.tPromocionActual, tPromocion_Sel);
    this.tPromocionActual.Usuariomodificador = this.tUsuarioActual.Usuariomodificador;

    this.tConfServices
      .AbrirModalConf(this.tConfiguracionFormulario.ModalConfirmacion.Eliminacion)
      .then(Resultado => {

        switch (Resultado) {

          case "S": {

            this.Eliminar_Promocion();
            break;

          }

        }

      });

  }

  Cancelar(): void {

    this.Nuevo();

  }

  ValidarFormulario(): boolean {

    let valido: boolean = false;

    if (this.tPromocionActual.Titulo == "" || isNullOrUndefined(this.tPromocionActual.Titulo)) {
      this.toastr.warning('Debe ingresar un titulo!', 'Disculpe!')
      return valido
    }

    return true;

  }

  Agregar(): void {

    if (this.ValidarFormulario() == false) {
      return
    }

    switch (this.tNuevo) {
      case true: {

        this.tConfServices
          .AbrirModalConf(this.tConfiguracionFormulario.ModalConfirmacion.Agregar)
          .then(Resultado => {

            switch (Resultado) {

              case "S": {

                this.Nuevo_Promocion();
                break;

              }

            }

          });
        break;

      }
      default: {

        this.tConfServices
          .AbrirModalConf(this.tConfiguracionFormulario.ModalConfirmacion.Edicion)
          .then(Resultado => {

            switch (Resultado) {

              case "S": {

                this.Modificar_Promocion();
                break;

              }

            }

          });
        break;

      }
    }

  }

  // BloquearImagen(): boolean {

  //   if (this.tConfiguracion.Valor == "S") {

  //     return false;

  //   }
  //   else {

  //     return true;

  //   }

  // }

}

