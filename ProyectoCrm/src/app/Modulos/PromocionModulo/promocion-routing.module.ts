import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FwPlantillaComponent  } from 'src/app/Shared/Formularios/fw-plantilla/fw-plantilla.component';
import { FwPromocionPage  } from './Paginas/fw-promocion/fw-promocion.page';

const routes: Routes = [
  {
    path: '',
    component: FwPlantillaComponent,
    children:[
      {
        path: '',
        component: FwPromocionPage,
        data:{
          Codformulario: 'fPromocion'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PromocionRoutingModule { }
