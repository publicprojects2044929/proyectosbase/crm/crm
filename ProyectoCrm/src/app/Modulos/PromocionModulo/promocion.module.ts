import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PromocionRoutingModule } from './promocion-routing.module';
import { FwPromocionPage } from './Paginas/fw-promocion/fw-promocion.page';
import { BsDatepickerModule } from 'ngx-bootstrap';

import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from 'src/app/Shared/shared.module';
import { FwPromocionCabeceraComponent } from './Paginas/fw-promocion/Componentes/fw-promocion-cabecera/fw-promocion-cabecera.component';
import { FwPromocionDetallesComponent } from './Paginas/fw-promocion/Componentes/fw-promocion-detalles/fw-promocion-detalles.component'

@NgModule({
  declarations: [FwPromocionPage, FwPromocionCabeceraComponent, FwPromocionDetallesComponent],
  imports: [
    CommonModule,
    PromocionRoutingModule,
    SharedModule,
    FormsModule,
    BsDatepickerModule.forRoot(),
    NgbModule
  ]
})
export class PromocionModule { }
