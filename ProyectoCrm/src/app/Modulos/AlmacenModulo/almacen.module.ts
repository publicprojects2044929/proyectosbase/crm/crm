import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from 'src/app/Shared/shared.module'

import { AlmacenRoutingModule } from './almacen-routing.module';
import { FwAlmacenPage } from './Paginas/fw-almacen/fw-almacen.page';
import { FwAlmacenDetalleCabeceraComponent } from './Paginas/fw-almacen/Componentes/fw-almacen-detalle-cabecera/fw-almacen-detalle-cabecera.component';
import { FwAlmacenDetalleDatosComponent } from './Paginas/fw-almacen/Componentes/fw-almacen-detalle-datos/fw-almacen-detalle-datos.component';


@NgModule({
  declarations: [FwAlmacenPage, FwAlmacenDetalleCabeceraComponent, FwAlmacenDetalleDatosComponent],
  imports: [
    CommonModule,
    AlmacenRoutingModule,
    SharedModule,
    FormsModule,
    NgbModule
  ]
})
export class AlmacenModule { }
