import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwAlmacenPage } from './fw-almacen.page';

describe('FwAlmacenPage', () => {
  let component: FwAlmacenPage;
  let fixture: ComponentFixture<FwAlmacenPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwAlmacenPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwAlmacenPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
