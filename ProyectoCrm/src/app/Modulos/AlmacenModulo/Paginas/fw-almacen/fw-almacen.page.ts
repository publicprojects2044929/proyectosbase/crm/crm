import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormularioConfiguracion } from 'src/app/Clases/EstructuraConfiguracion';
import { Usuario, Empresas, Almacen, Formulario, Filtros, Configuracion } from 'src/app/Clases/Estructura';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/Core/Session/session.service';
import { ToastrService } from 'ngx-toastr';
import { ErroresService } from 'src/app/Modales/mensajes/fw-modal-msj/errores.service';
import { MensajesService } from 'src/app/Modales/mensajes/fw-modal-msj/mensajes.service';
import { ConfirmacionService } from 'src/app/Modales/confirmacion/fw-modal-confirmacion/confirmacion.service';
import { environment } from 'src/environments/environment';
import { AlmacenEntrada } from 'src/app/Clases/EstructuraEntrada';
import { Subscription } from 'rxjs';
import { ConfiguracionService } from 'src/app/Core/Configuracion/configuracion.service';

@Component({
  selector: 'app-fw-almacen',
  templateUrl: './fw-almacen.page.html',
  styleUrls: ['./fw-almacen.page.scss']
})
export class FwAlmacenPage implements OnInit, OnDestroy {

  tCodFormularioActual: string = "fAlmacen";
  ConfiguracionFormulario: FormularioConfiguracion = new FormularioConfiguracion();
  tPermisoAgregar: boolean = false;
  tPermisoEditar: boolean = false;
  tPermisoEliminar: boolean = false;
  tHabilitarGuardar: boolean = false;

  tUsuarioActual: Usuario = new Usuario();
  tEmpresaActual: Empresas = new Empresas();
  tEmpresa$: Subscription;
  tUsuario$: Subscription;

  tTitulo: string = "Gestión de Almacèn";
  tNuevo: boolean = true;
  tPosicion: number = 0;
  tPagina: number = 1;
  tTamanoPag: number = 5;

  tAlmacenes: Almacen[] = [];
  tAlmacenActual: Almacen = new Almacen();

  tFormularios: Formulario[] = [];

  tFiltros: Filtros[] = [];
  tFiltroActual: Filtros = new Filtros();
  tFiltroBusqueda: string = "";

  tConfiguracion: Configuracion = new Configuracion();
  tConfiguracion$: Subscription;
  tHayConfiguracionWs$: Subscription;
  tStrConfiguracion: string = 'Almacen/Almacen.Config.json';

  constructor(public Ws: ClsServiciosService,
    private router: Router,
    private tSesion: SessionService,
    private tConfiguracionService: ConfiguracionService,
    private toastr: ToastrService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService,
    private tConfServices: ConfirmacionService) {

    this.tFiltroBusqueda = "";
    this.tPosicion = 0;

    this.tFiltros = [

      { Codfiltro: "Codalmacen", Filtro: "Código" },
      { Codfiltro: "Descripcion", Filtro: "Descripción" },
    ]

  }

  ngOnInit() {

    this.tConfiguracion$ = this.tConfiguracionService
      .Consumir_Obtener_Configuracion(this.tStrConfiguracion)
      .subscribe((tConfiguracion: FormularioConfiguracion) => {

        this.ConfiguracionFormulario = tConfiguracion;
        this.tTitulo = this.ConfiguracionFormulario.Titulo;
        this.tErroresServices.tTituloMsj = this.ConfiguracionFormulario.ModalMensaje.Titulo;
        this.tMsjServices.tTituloMsj = this.ConfiguracionFormulario.ModalMensaje.Titulo;
        this.tConfServices.tTituloConf = this.ConfiguracionFormulario.ModalConfirmacion.Titulo;

      });

    this.tEmpresa$ = this.tSesion.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    })

    this.tUsuario$ = this.tSesion.tUsuarioActual$.subscribe((tUsuario: Usuario) => {

      this.tUsuarioActual = tUsuario;

    })

    var tTienePermiso: boolean = this.tSesion.ObtenerPermisoAcceso(this.tCodFormularioActual);
    this.tPermisoAgregar = this.tSesion.ObtenerPermisoAgregar();
    this.tPermisoEditar = this.tSesion.ObtenerPermisoEditar();
    this.tPermisoEliminar = this.tSesion.ObtenerPermisoEliminar();

    if (tTienePermiso == true) {

      this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
        .subscribe((tHayConfig: boolean) => {

          if (tHayConfig === true) {

            this.Buscar_Almacenes();
            this.Nuevo();

          }

        });

    }
    else {

      this.tMsjServices.AbrirModalMsj(environment.msjSinAcceso);
      this.router.navigate(['/Home']);

    }

  }

  ngOnDestroy() {

    this.tEmpresa$.unsubscribe();
    this.tUsuario$.unsubscribe();
    this.tConfiguracion$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();

  }

  //#region "Funciones de busqueda"

  Buscar_Almacenes(): void {

    this.Ws
      .Almacen()
      .Consumir_Obtener_Almacen(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa)
      .subscribe(Respuesta => {

        this.tAlmacenes = [];

        if (Respuesta.Resultado == "N") {

          // this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);
          //

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tAlmacenes = Respuesta.Datos;
          this.tFiltroActual = this.tFiltros[0];
          this.Nuevo();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  //#region "CRUD"

  Nuevo_Almacen(): void {

    var tNuevoAlmacen: AlmacenEntrada;
    tNuevoAlmacen = {

      "Id": 0,
      "Corporacion": this.tEmpresaActual.Corporacion,
      "Empresa": this.tEmpresaActual.Empresa,
      "Codalmacen": "",
      "Descripcion": this.tAlmacenActual.Descripcion,
      "Venta": this.tAlmacenActual.Venta,
      "Usuariocreador": this.tUsuarioActual.Codusuario,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }


    this.Ws
      .Almacen()
      .Consumir_Crear_Almacen(tNuevoAlmacen)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.toastr.clear();
          this.toastr.success('El almacén se ha creado de manera exitosa!');
          this.tFiltroActual = this.tFiltros[0];
          this.Nuevo();
          this.Buscar_Almacenes();


        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Modificar_Almacen(): void {

    var tModificarAlmacen: AlmacenEntrada;
    tModificarAlmacen = {
      "Id": 0,
      "Corporacion": this.tEmpresaActual.Corporacion,
      "Empresa": this.tEmpresaActual.Empresa,
      "Codalmacen": this.tAlmacenActual.Codalmacen,
      "Descripcion": this.tAlmacenActual.Descripcion,
      "Venta": this.tAlmacenActual.Venta,
      "Usuariocreador": this.tUsuarioActual.Codusuario,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }

    this.Ws
      .Almacen()
      .Consumir_Modificar_Almacen(tModificarAlmacen)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.toastr.clear();
          this.toastr.success('El almacén se ha modificado de manera exitosa!');
          this.tFiltroActual = this.tFiltros[0];
          this.Nuevo();
          this.Buscar_Almacenes();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Eliminar_Almacen(): void {

    var tEliminarAlmacen: AlmacenEntrada;
    tEliminarAlmacen = {
      "Id": 0,
      "Corporacion": this.tEmpresaActual.Corporacion,
      "Empresa": this.tEmpresaActual.Empresa,
      "Codalmacen": this.tAlmacenActual.Codalmacen,
      "Descripcion": this.tAlmacenActual.Descripcion,
      "Venta": this.tAlmacenActual.Venta,
      "Usuariocreador": this.tUsuarioActual.Codusuario,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }

    this.Ws
      .Almacen()
      .Consumir_Eliminar_Almacen(tEliminarAlmacen)
      .subscribe(Respuesta => {


        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.toastr.clear();
          this.toastr.success('El almacén se ha eliminado de manera exitosa!');
          this.tFiltroActual = this.tFiltros[0];
          this.Nuevo();
          this.Buscar_Almacenes();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  Nuevo(): void {

    this.tHabilitarGuardar = this.tPermisoAgregar;
    this.tNuevo = true;

    this.tAlmacenActual = new Almacen();

    this.toastr.clear();
    this.toastr.info('Por favor llene los datos necesarios para registrar un nuevo Almacén!');

  }

  Seleccionar_Almacen(tAlmacenActual_Sel: Almacen, tPos: number): void {

    this.tHabilitarGuardar = this.tPermisoEditar;
    this.tNuevo = false;
    this.tPosicion = tPos;
    this.tAlmacenActual = new Almacen();
    Object.assign(this.tAlmacenActual, tAlmacenActual_Sel);
    this.tAlmacenActual.Usuariomodificador = this.tUsuarioActual.Usuariomodificador;

  }

  Seleccionar_Almacen_Eliminar(tAlmacen_Sel: Almacen): void {

    this.tAlmacenActual = new Almacen();
    this.tAlmacenActual.Id = tAlmacen_Sel.Id;
    Object.assign(this.tAlmacenActual, tAlmacen_Sel);
    this.tAlmacenActual.Usuariomodificador = this.tUsuarioActual.Usuariomodificador;

    this.tConfServices
      .AbrirModalConf(this.ConfiguracionFormulario.ModalConfirmacion.Eliminacion)
      .then(Resultado => {

        switch (Resultado) {
          case "S": {

            this.Eliminar_Almacen();
            break;

          }
        }

      });

  }

  Cancelar(): void {

    this.Nuevo();

  }

  ValidarFormulario(): boolean {

    let valido: boolean = false;

    if (this.tAlmacenActual.Descripcion === "") {

      this.toastr.warning('Debe ingresar una descripciòn!', 'Disculpe!');
      return valido;

    }

    return true;
  }

  Agregar(): void {

    if (this.ValidarFormulario() == false) {

      return;

    }

    switch (this.tNuevo) {
      case true: {

        this.tConfServices
          .AbrirModalConf(this.ConfiguracionFormulario.ModalConfirmacion.Agregar)
          .then(Resultado => {

            switch (Resultado) {
              case "S": {

                this.Nuevo_Almacen();
                break;

              }
            }
          });

        break;

      }
      default: {

        this.tConfServices
          .AbrirModalConf(this.ConfiguracionFormulario.ModalConfirmacion.Edicion)
          .then(Resultado => {

            switch (Resultado) {
              case "S": {

                this.Modificar_Almacen();
                break;

              }
            }

          });

        break;

      }
    }

  }

}
