import { Component, OnInit, Input } from '@angular/core';
import { Almacen } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-almacen-detalle-cabecera',
  templateUrl: './fw-almacen-detalle-cabecera.component.html',
  styleUrls: ['./fw-almacen-detalle-cabecera.component.scss']
})
export class FwAlmacenDetalleCabeceraComponent implements OnInit {

  @Input('Almacen') tAlmacen: Almacen = new Almacen()

  constructor() { }

  ngOnInit(): void {
  }

}
