import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwAlmacenDetalleDatosComponent } from './fw-almacen-detalle-datos.component';

describe('FwAlmacenDetalleDatosComponent', () => {
  let component: FwAlmacenDetalleDatosComponent;
  let fixture: ComponentFixture<FwAlmacenDetalleDatosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwAlmacenDetalleDatosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwAlmacenDetalleDatosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
