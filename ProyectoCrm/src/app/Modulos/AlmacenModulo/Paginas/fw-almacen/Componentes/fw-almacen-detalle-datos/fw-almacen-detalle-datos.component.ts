import { Component, OnInit, Input } from '@angular/core';
import { Almacen } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-almacen-detalle-datos',
  templateUrl: './fw-almacen-detalle-datos.component.html',
  styleUrls: ['./fw-almacen-detalle-datos.component.scss']
})
export class FwAlmacenDetalleDatosComponent implements OnInit {

  @Input('Almacen') tAlmacen: Almacen = new Almacen();

  constructor() { }

  ngOnInit(): void {
  }

  EvaluarVenta(): number {

    if (this.tAlmacen.Venta == 1) {

      return 1;

    }
    else {

      return 0;

    }

  };

  Check_Venta(tCheckbox: any): void {

    if (tCheckbox == true) {

      this.tAlmacen.Venta = 1;

    }
    else {

      this.tAlmacen.Venta = 0;

    }

  }

}
