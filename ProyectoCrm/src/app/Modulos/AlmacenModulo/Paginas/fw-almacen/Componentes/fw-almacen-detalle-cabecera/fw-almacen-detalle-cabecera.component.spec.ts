import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwAlmacenDetalleCabeceraComponent } from './fw-almacen-detalle-cabecera.component';

describe('FwAlmacenDetalleCabeceraComponent', () => {
  let component: FwAlmacenDetalleCabeceraComponent;
  let fixture: ComponentFixture<FwAlmacenDetalleCabeceraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwAlmacenDetalleCabeceraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwAlmacenDetalleCabeceraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
