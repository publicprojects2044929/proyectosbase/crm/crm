import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FwUsuariosPage } from './Paginas/fw-usuarios/fw-usuarios.page';
import { FwPlantillaComponent  } from 'src/app/Shared/Formularios/fw-plantilla/fw-plantilla.component';


const routes: Routes = [
  {
    path: '',
    component: FwPlantillaComponent,
    children:[
      {
        path: '',
        component: FwUsuariosPage,
        data:{
          Codformulario: 'fUsuarios'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsuariosRoutingModule { }
