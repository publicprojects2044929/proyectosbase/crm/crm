import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwUsuariosPage } from './fw-usuarios.page';

describe('FwUsuariosPage', () => {
  let component: FwUsuariosPage;
  let fixture: ComponentFixture<FwUsuariosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwUsuariosPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwUsuariosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
