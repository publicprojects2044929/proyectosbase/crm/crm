import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwUsuariosDetallesComponent } from './fw-usuarios-detalles.component';

describe('FwUsuariosDetallesComponent', () => {
  let component: FwUsuariosDetallesComponent;
  let fixture: ComponentFixture<FwUsuariosDetallesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwUsuariosDetallesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwUsuariosDetallesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
