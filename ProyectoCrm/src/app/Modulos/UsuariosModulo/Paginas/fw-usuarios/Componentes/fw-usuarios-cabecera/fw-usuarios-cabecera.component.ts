import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { Usuario, Estatus, Tipo } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-usuarios-cabecera',
  templateUrl: './fw-usuarios-cabecera.component.html',
  styleUrls: ['./fw-usuarios-cabecera.component.scss']
})
export class FwUsuariosCabeceraComponent implements OnInit, OnChanges {

  @Input('Usuario') tUsuario: Usuario = new Usuario();
  @Input('Nuevo') tNuevo: boolean = false;
  @Input('Tipos') tTipos: Tipo[] = [];
  @Input('Estatus') tEstatus: Estatus[] = [];

  @Output('Buscar') tBuscar: EventEmitter<string> = new EventEmitter();
  @Output('AbrirModal') tAbrir: EventEmitter<string> = new EventEmitter();

  tEstatusActual: Estatus = new Estatus();
  tTipoActual: Tipo = new Tipo();

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {

    if('tTipos' in changes){

      this.tTipoActual = this.tTipos[0] || new Tipo();
      this.CambiarTipo();

    }

    if('tEstatus' in changes){

      this.tEstatusActual = this.tEstatus[0] || new Estatus();
      this.CambiarEstatus();

    }

    if('tUsuario' in changes){

      this.tTipoActual = this.tTipos.find(tTipo => {
        return tTipo.Corporacion === this.tUsuario.Corporacion &&
              tTipo.Empresa === this.tUsuario.Empresa &&
              tTipo.Codtipo === this.tUsuario.Tipo
      }) || this.tTipos[0] || new Tipo();

      this.tEstatusActual = this.tEstatus.find(tEst => {
        return tEst.Corporacion === this.tUsuario.Corporacion &&
              tEst.Empresa === this.tUsuario.Empresa &&
              tEst.Codestatus === this.tUsuario.Estatus
      }) || this.tEstatus[0] || new Estatus();

      if(this.tUsuario.Codusuario === ''){

        this.CambiarEstatus();
        this.CambiarTipo();

      }

    }

  }

  AbrirModal(){
    this.tAbrir.emit('');
  }

  Buscar(){

    if(this.tUsuario.Codusuario === '') { return; }

    this.tBuscar.emit(this.tUsuario.Codusuario);

  }

  CambiarTipo(){
    this.tUsuario.Tipo = this.tTipoActual.Codtipo;
  }

  CambiarEstatus(){
    this.tUsuario.Estatus = this.tEstatusActual.Codestatus;
  }

}
