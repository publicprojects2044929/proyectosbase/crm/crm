import { Component, OnInit, Input } from '@angular/core';
import { GruposUsuario, Usuario } from 'src/app/Clases/Estructura';
import { PermisosService } from 'src/app/Modales/permisos/fw-modal-permisos/permisos.service';

@Component({
  selector: 'app-fw-usuarios-detalles',
  templateUrl: './fw-usuarios-detalles.component.html',
  styleUrls: ['./fw-usuarios-detalles.component.scss']
})
export class FwUsuariosDetallesComponent implements OnInit {

  @Input('Usuario') tUsuario: Usuario = new Usuario();


  constructor(private tPermisosService: PermisosService) { }

  ngOnInit(): void {
  }

  AbrirModalPermisos(tGrupo: GruposUsuario): void {

    this.tPermisosService
      .AbrirModal(tGrupo)
      .then(Resultado => {

      });

  }

  Grupo_Pertenece(tCheckbox: any, tGrupo_Sel: GruposUsuario): void {

    if (tCheckbox == true) {

      tGrupo_Sel.Pertenece = 1;

    }
    else {

      tGrupo_Sel.Pertenece = 0;

    }

  }

}
