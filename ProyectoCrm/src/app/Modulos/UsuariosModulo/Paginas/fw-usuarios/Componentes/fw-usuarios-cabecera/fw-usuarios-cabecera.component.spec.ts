import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwUsuariosCabeceraComponent } from './fw-usuarios-cabecera.component';

describe('FwUsuariosCabeceraComponent', () => {
  let component: FwUsuariosCabeceraComponent;
  let fixture: ComponentFixture<FwUsuariosCabeceraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwUsuariosCabeceraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwUsuariosCabeceraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
