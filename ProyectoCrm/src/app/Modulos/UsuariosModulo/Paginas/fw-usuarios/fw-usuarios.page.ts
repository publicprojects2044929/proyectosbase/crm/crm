import { Component, OnInit, OnDestroy } from '@angular/core';
import { Usuario, Empresas, Tipo, Estatus, Filtros, GruposUsuario, Grupos } from 'src/app/Clases/Estructura';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/Core/Session/session.service';
import { ToastrService } from 'ngx-toastr';
import { ErroresService } from 'src/app/Modales/mensajes/fw-modal-msj/errores.service';
import { MensajesService } from 'src/app/Modales/mensajes/fw-modal-msj/mensajes.service';
import { ConfirmacionService } from 'src/app/Modales/confirmacion/fw-modal-confirmacion/confirmacion.service';
import { environment } from 'src/environments/environment';
import { UsuarioEntrada, UsuarioEmpresasEntrada, UsuarioGruposEntrada } from 'src/app/Clases/EstructuraEntrada';
import { Subscription } from 'rxjs';
import { FormularioConfiguracion } from 'src/app/Clases/EstructuraConfiguracion';
import { ConfiguracionService } from 'src/app/Core/Configuracion/configuracion.service';
import { UsuariosService } from 'src/app/Modales/usuario/fw-modal-usuarios/usuarios.service';
import { Validaciones } from 'src/app/Manejadores/cls-procedimientos';

@Component({
  selector: 'app-fw-usuarios',
  templateUrl: './fw-usuarios.page.html',
  styleUrls: ['./fw-usuarios.page.scss']
})
export class FwUsuariosPage implements OnInit, OnDestroy {

  tConfiguracionFormulario: FormularioConfiguracion = new FormularioConfiguracion();
  tCodFormularioActual: string = "fUsuarios";
  tPermisoAgregar: boolean = false;
  tPermisoEditar: boolean = false;
  tPermisoEliminar: boolean = false;
  tHabilitarGuardar: boolean = false;
  tHabilitarEliminar: boolean = false;

  tUsuarioActual: Usuario = new Usuario();
  tEmpresaActual: Empresas = new Empresas();
  tEmpresa$: Subscription;
  tUsuario$: Subscription;

  tTitulo: string = "Gestión de Usuarios";
  tNuevo: boolean = true;
  tPosicion: number = 0;
  tPagina: number = 1;
  tTamanoPag: number = 5;

  tUsuario: Usuario = new Usuario();

  tTipos: Tipo[] = [];

  tEstatus: Estatus[] = [];

  tFiltros: Filtros[] = [];
  tFiltroActual: Filtros = new Filtros();
  tFiltroBusqueda: string = "";
 
  tGruposNuevo: GruposUsuario[] = [];
  tGrupos: GruposUsuario[] = [];

  tConfiguracion$: Subscription;
  tHayConfiguracionWs$: Subscription;
  tStrConfiguracion: string = 'Usuario/Usuario.Config.json';

  tValidaciones: Validaciones = new Validaciones();

  constructor(
    public Ws: ClsServiciosService,
    private tConfiguracionService: ConfiguracionService,
    private router: Router,
    private tSesion: SessionService,
    private toastr: ToastrService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService,
    private tConfServices: ConfirmacionService,
    private tUsuarioService: UsuariosService,
  ) {

    this.tFiltroBusqueda = "";

    this.tEstatus = [

      {
        "Id": 0,
        "Corporacion": "",
        "Empresa": "",
        "Codestatus": "",
        "Descripcion": "",
        "Secuencia": 0,
        "Tabla": "",
        "Hexadecimal": "",
        "Fecha_creacion": new Date(),
        "Fecha_creacionl": 0,
        "Fecha_modificacion": new Date(),
        "Fecha_modificacionl": 0,
        "Usuariocreador": "",
        "Usuariomodificador": ""
      }

    ];

    this.tFiltros = [

      { Codfiltro: "Codusuario", Filtro: "Código" },
      { Codfiltro: "Descripcion", Filtro: "Descripcion" },

    ]

    this.tTipos = [

      {
        "Id": 0,
        "Corporacion": "",
        "Empresa": "",
        "Codtipo": "",
        "Descripcion": "",
        "Tabla": "",
        "Columna": "",
        "Abreviacion": "",
        "Hexadecimal": "",
        "Fecha_creacion": new Date(),
        "Fecha_creacionl": 0,
        "Fecha_modificacion": new Date(),
        "Fecha_modificacionl": 0,
        "Usuariocreador": "",
        "Usuariomodificador": ""
      }

    ];

  }

  ngOnInit() {

    this.tConfiguracion$ = this.tConfiguracionService
      .Consumir_Obtener_Configuracion(this.tStrConfiguracion)
      .subscribe((tConfiguracion: FormularioConfiguracion) => {

        this.tConfiguracionFormulario = tConfiguracion;
        this.tTitulo = this.tConfiguracionFormulario.Titulo;
        this.tErroresServices.tTituloMsj = this.tConfiguracionFormulario.ModalMensaje.Titulo;
        this.tMsjServices.tTituloMsj = this.tConfiguracionFormulario.ModalMensaje.Titulo;
        this.tConfServices.tTituloConf = this.tConfiguracionFormulario.ModalConfirmacion.Titulo;

      });

    this.tEmpresa$ = this.tSesion.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    })

    this.tUsuario$ = this.tSesion.tUsuarioActual$.subscribe((tUsuario: Usuario) => {

      this.tUsuarioActual = tUsuario;

    })

    var tTienePermiso: boolean = this.tSesion.ObtenerPermisoAcceso(this.tCodFormularioActual);
    this.tPermisoAgregar = this.tSesion.ObtenerPermisoAgregar();
    this.tPermisoEditar = this.tSesion.ObtenerPermisoEditar();
    this.tPermisoEliminar = this.tSesion.ObtenerPermisoEliminar();
    this.tHabilitarEliminar = this.tPermisoEliminar;

    if (tTienePermiso === true) {

      this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
        .subscribe((tHayConfig: boolean) => {

          if (tHayConfig === true) {

            this.BuscarEstatus();
            this.BuscarTipo();
            this.Buscar_Grupos();
            this.Nuevo();

          }

        });

    }
    else {

      this.tMsjServices.AbrirModalMsj(environment.msjSinAcceso);
      this.router.navigate(['/Home']);

    }

  }

  ngOnDestroy() {

    this.tEmpresa$.unsubscribe();
    this.tUsuario$.unsubscribe();
    this.tConfiguracion$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();

  }

  //#region "Funciones de busqueda"

  BuscarEstatus(): void {

    this.Ws
      .Estatus()
      .Consumir_Obtener_Estatus(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        'MR_USUARIO')
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          // this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tEstatus = Respuesta.Datos;

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  BuscarTipo(): void {

    this.Ws
      .Tipo()
      .Consumir_Obtener_Tipo(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        'MR_USUARIO')
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          // this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tTipos = Respuesta.Datos;

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  BuscarUsuario(tCodusuario: string): void {

    this.Ws
      .Usuario()
      .Consumir_Obtener_UsuariNativosOcultoGrupo(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        tCodusuario,
        'N')
      .subscribe(Respuesta => {

        this.tUsuario = new Usuario();

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tNuevo = false;
          this.tHabilitarGuardar = this.tPermisoEditar;
          this.tUsuario = Respuesta.Datos[0] || new Usuario();
          this.tFiltroActual = this.tFiltros[0];

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Buscar_Grupos(): void {

    this.Ws
      .Grupo()
      .Consumir_Obtener_GrupoOculto(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        "N")
      .subscribe(Respuesta => {

        this.tGrupos = [];

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tGrupos = Respuesta.Datos;
          this.tUsuario.LGrupos = this.tGrupos || [];

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  //#region "Modal"

  AbrirModalUsuario(): void {


    this.tUsuarioService.AbrirModal().then(result => {

      if (result.Resultado == "S") {

        var tUsuarioModal: Usuario = result.Usuario;
        this.BuscarUsuario(tUsuarioModal.Codusuario);

      }

    }, reason => {



    });

  }

  //#endregion

  Funciones(tFuncion: String): void {

    switch (tFuncion) {
      case "NuevoUsuario": {

        this.Nuevo_Usuario();
        break;

      }
      case "ModificarUsuario": {

        this.Modificar_Usuario();
        break;

      }
      case "EliminarUsuario": {

        this.Eliminar_Usuario();
        break;

      }
      default: {

        break;

      }
    }

  };

  Eliminar() {

    this.tConfServices
      .AbrirModalConf("¿Confirma eliminar el usuario?")
      .then(Resultado => {

        switch (Resultado) {

          case "S": {

            this.Eliminar_Usuario();
            break;

          }

        };

      });

  }

  Nuevo(): void {

    this.tHabilitarGuardar = this.tPermisoAgregar;
    this.tNuevo = true;
    this.tUsuario = new Usuario();
    this.tUsuario.LGrupos = this.tGrupos || [];

    this.toastr.clear();
    this.toastr.info('Por favor llene los datos necesarios para registrar un nuevo Usuario!');

  }

  Cancelar(): void {

    this.Nuevo();

  }

  ValidarFormulario(): boolean {

    let valido: boolean = false;

    if (this.tUsuario.Codusuario == "") {
      this.toastr.warning('Debe ingresar un Usuario!', 'Disculpe!')
      return valido
    }

    if (this.tUsuario.Descripcion == "") {
      this.toastr.warning('Debe ingresar una Descripciòn!', 'Disculpe!')
      return valido
    }

    if (this.tUsuario.Cedula == "") {
      this.toastr.warning('Debe ingresar un Nùmero de identificaciòn !', 'Disculpe!')
      return valido
    }

    if (this.tUsuario.Email == "") {
      this.toastr.warning('Debe ingresar un Email!', 'Disculpe!')
      return valido
    }

    if (this.tValidaciones.ValidarEmail(this.tUsuario.Email) == false) {
      this.toastr.error('Debe ingresar un Email vàlido', 'Disculpe!')
      return valido
    }

    return true;

  }

  Agregar(): void {


    if (this.ValidarFormulario() == false) {
      return
    }

    switch (this.tNuevo) {
      case true: {

        this.tConfServices
          .AbrirModalConf("Confirma crear el nuevo usuario con los datos suministrados.")
          .then(Resultado => {

            switch (Resultado) {

              case "S": {

                this.Nuevo_Usuario();
                break;

              }

            };

          });

        break;

      }
      default: {

        this.tConfServices
          .AbrirModalConf("Confirma modificar el usuario con los datos suministrados.")
          .then(Resultado => {

            switch (Resultado) {

              case "S": {

                this.Modificar_Usuario();
                break;

              }

            };

          });

        break;

      }
    }

  }

  Nuevo_Usuario(): void {

    var tNuevoUsuario: UsuarioEntrada;
    tNuevoUsuario = {
      "Id": 0,
      "Corporacion": this.tEmpresaActual.Corporacion,
      "Empresa": this.tEmpresaActual.Empresa,
      "Codusuario": this.tUsuario.Codusuario,
      "Descripcion": this.tUsuario.Descripcion,
      "Clave": "",
      "Cedula": this.tUsuario.Cedula,
      "Email": this.tUsuario.Email,
      "Estatus": this.tUsuario.Estatus,
      "Imagen64": "",
      "Url": this.tUsuario.Url,
      "Conectado": 0,
      "Token": "",
      "Dispositivo": "",
      "Tipo": this.tUsuario.Tipo,
      "Cambiar_clave": "N",
      "Oculto": "N",
      "Usuariocreador": this.tUsuarioActual.Codusuario,
      "Usuariomodificador": this.tUsuarioActual.Codusuario,
      "LUsuario_Empresas": []
    }

    var tCrearEmpresaUsuario: UsuarioEmpresasEntrada;
    tCrearEmpresaUsuario = {
      "Id": 0,
      "Corporacion": this.tEmpresaActual.Corporacion,
      "Empresa": this.tEmpresaActual.Empresa,
      "Codusuario": this.tUsuario.Codusuario,
      "Nativo": "S",
      "Usuariocreador": this.tUsuarioActual.Codusuario,
      "Usuariomodificador": this.tUsuarioActual.Codusuario,
      "LUsuario_Grupos": []
    }

    for (let tGrupo of this.tUsuario.LGrupos) {

      if (tGrupo.Pertenece == 1) {

        var tCrearGrupoUsuario: UsuarioGruposEntrada;
        tCrearGrupoUsuario = {
          "Id": 0,
          "Corporacion": this.tEmpresaActual.Corporacion,
          "Empresa": this.tEmpresaActual.Empresa,
          "Codusuario": this.tUsuario.Codusuario,
          "Codgrupo": tGrupo.Codgrupo,
          "Usuariocreador": this.tUsuarioActual.Codusuario,
          "Usuariomodificador": this.tUsuarioActual.Codusuario
        }

        tCrearEmpresaUsuario.LUsuario_Grupos.push(tCrearGrupoUsuario);

      }


    }

    tNuevoUsuario.LUsuario_Empresas.push(tCrearEmpresaUsuario);

    this.Ws.Usuario().Consumir_Crear_Usuario(tNuevoUsuario).subscribe(Respuesta => {

      if (Respuesta.Resultado == "N") {

        this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

      }
      else if (Respuesta.Resultado == "E") {

        this.tMsjServices.AbrirModalMsj(environment.msjError);

      }
      else {

        this.toastr.clear();
        this.toastr.success('El usuario se ha creado de manera exitosa!');
        this.tFiltroActual = this.tFiltros[0];
        this.Nuevo();

      }

    }, error => {

      this.tErroresServices.MostrarError(error);

    });

  }

  Modificar_Usuario(): void {

    var tModificarUsuario: UsuarioEntrada;
    tModificarUsuario = {
      "Id": 0,
      "Corporacion": this.tUsuario.Corporacion,
      "Empresa": this.tUsuario.Empresa,
      "Codusuario": this.tUsuario.Codusuario,
      "Descripcion": this.tUsuario.Descripcion,
      "Clave": this.tUsuario.Clave,
      "Cedula": this.tUsuario.Cedula,
      "Email": this.tUsuario.Email,
      "Estatus": this.tUsuario.Estatus,
      "Imagen64": "",
      "Url": this.tUsuario.Url,
      "Conectado": this.tUsuario.Conectado,
      "Token": this.tUsuario.Token,
      "Dispositivo": this.tUsuario.Dispositivo,
      "Tipo": this.tUsuario.Tipo,
      "Cambiar_clave": "N",
      "Oculto": this.tUsuario.Oculto,
      "Usuariocreador": this.tUsuarioActual.Codusuario,
      "Usuariomodificador": this.tUsuarioActual.Codusuario,
      "LUsuario_Empresas": []
    }

    var tModificarEmpresaUsuario: UsuarioEmpresasEntrada;
    tModificarEmpresaUsuario = {
      "Id": 0,
      "Corporacion": this.tUsuario.Corporacion,
      "Empresa": this.tUsuario.Empresa,
      "Codusuario": this.tUsuario.Codusuario,
      "Nativo": "S",
      "Usuariocreador": this.tUsuarioActual.Codusuario,
      "Usuariomodificador": this.tUsuarioActual.Codusuario,
      "LUsuario_Grupos": []
    }

    for (let tGrupo of this.tUsuario.LGrupos) {

      if (tGrupo.Pertenece == 1) {

        var tModificarGrupoUsuario: UsuarioGruposEntrada;
        tModificarGrupoUsuario = {
          "Id": 0,
          "Corporacion": this.tUsuario.Corporacion,
          "Empresa": this.tUsuario.Empresa,
          "Codusuario": this.tUsuario.Codusuario,
          "Codgrupo": tGrupo.Codgrupo,
          "Usuariocreador": this.tUsuarioActual.Codusuario,
          "Usuariomodificador": this.tUsuarioActual.Codusuario
        }

        tModificarEmpresaUsuario.LUsuario_Grupos.push(tModificarGrupoUsuario);

      }


    }

    tModificarUsuario.LUsuario_Empresas.push(tModificarEmpresaUsuario);

    this.Ws.Usuario().Consumir_Modificar_Usuario(tModificarUsuario).subscribe(Respuesta => {

      if (Respuesta.Resultado == "N") {

        this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

      }
      else if (Respuesta.Resultado == "E") {

        this.tMsjServices.AbrirModalMsj(environment.msjError);


      }
      else {

        this.toastr.clear();
        this.toastr.success('El usuario se ha modificado de manera exitosa!');
        this.tFiltroActual = this.tFiltros[0];
        this.Nuevo();

      }

    }, error => {

      this.tErroresServices.MostrarError(error);

    });

  }

  Eliminar_Usuario() {

    var tEliminarUsuario: UsuarioEntrada;
    tEliminarUsuario = {
      "Id": 0,
      "Corporacion": this.tUsuario.Corporacion,
      "Empresa": this.tUsuario.Empresa,
      "Codusuario": this.tUsuario.Codusuario,
      "Descripcion": this.tUsuario.Descripcion,
      "Clave": this.tUsuario.Clave,
      "Cedula": this.tUsuario.Cedula,
      "Email": this.tUsuario.Email,
      "Estatus": this.tUsuario.Estatus,
      "Conectado": this.tUsuario.Conectado,
      "Imagen64": "",
      "Url": this.tUsuario.Url,
      "Token": this.tUsuario.Token,
      "Dispositivo": this.tUsuario.Dispositivo,
      "Tipo": this.tUsuario.Tipo,
      "Cambiar_clave": "N",
      "Oculto": this.tUsuario.Oculto,
      "Usuariocreador": this.tUsuarioActual.Codusuario,
      "Usuariomodificador": this.tUsuarioActual.Codusuario,
      "LUsuario_Empresas": []
    }

    this.Ws.Usuario().Consumir_Eliminar_Usuario(tEliminarUsuario).subscribe(Respuesta => {

      if (Respuesta.Resultado == "N") {

        this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

      }
      else if (Respuesta.Resultado == "E") {

        this.tMsjServices.AbrirModalMsj(environment.msjError);

      }
      else {

        this.toastr.clear();
        this.toastr.success('El usuario se ha eliminado de manera exitosa!');
        this.tFiltroActual = this.tFiltros[0];
        this.Nuevo();

      }

    }, error => {

      this.tErroresServices.MostrarError(error);

    });

  }

}
