import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { UsuariosRoutingModule } from './usuarios-routing.module';
import { FwUsuariosPage } from './Paginas/fw-usuarios/fw-usuarios.page';

import { SharedModule } from 'src/app/Shared/shared.module';
import { FwUsuariosCabeceraComponent } from './Paginas/fw-usuarios/Componentes/fw-usuarios-cabecera/fw-usuarios-cabecera.component';
import { FwUsuariosDetallesComponent } from './Paginas/fw-usuarios/Componentes/fw-usuarios-detalles/fw-usuarios-detalles.component'

@NgModule({
  declarations: [FwUsuariosPage, FwUsuariosCabeceraComponent, FwUsuariosDetallesComponent],
  imports: [
    CommonModule,
    UsuariosRoutingModule,
    SharedModule,
    FormsModule,
    NgbModule
  ]
})
export class UsuariosModule { }
