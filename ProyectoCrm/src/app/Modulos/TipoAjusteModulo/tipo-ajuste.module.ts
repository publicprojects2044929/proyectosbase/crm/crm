import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from 'src/app/Shared/shared.module';

import { TipoAjusteRoutingModule } from './tipo-ajuste-routing.module';
import { FwTiposAjustesPage } from './Paginas/fw-tipos-ajustes/fw-tipos-ajustes.page';
import { FwTipoAjustesCabeceraComponent } from './Paginas/fw-tipos-ajustes/Componentes/fw-tipo-ajustes-cabecera/fw-tipo-ajustes-cabecera.component';
import { FwTipoAjustesDetallesComponent } from './Paginas/fw-tipos-ajustes/Componentes/fw-tipo-ajustes-detalles/fw-tipo-ajustes-detalles.component';

@NgModule({
  declarations: [FwTiposAjustesPage, FwTipoAjustesCabeceraComponent, FwTipoAjustesDetallesComponent],
  imports: [
    CommonModule,
    TipoAjusteRoutingModule,
    SharedModule,
    FormsModule,
    NgbModule
  ]
})
export class TipoAjusteModule { }
