import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormularioConfiguracion } from 'src/app/Clases/EstructuraConfiguracion';
import { Usuario, Empresas, TiposOpcion, TipoAjustesInventario, Formulario, Filtros } from 'src/app/Clases/Estructura';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/Core/Session/session.service';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { ToastrService } from 'ngx-toastr';
import { ErroresService } from 'src/app/Modales/mensajes/fw-modal-msj/errores.service';
import { MensajesService } from 'src/app/Modales/mensajes/fw-modal-msj/mensajes.service';
import { ConfirmacionService } from 'src/app/Modales/confirmacion/fw-modal-confirmacion/confirmacion.service';
import { TipoAjustesInventarioEntrada } from 'src/app/Clases/EstructuraEntrada';
import { environment } from 'src/environments/environment';
import { Subscription } from 'rxjs';
import { ConfiguracionService } from 'src/app/Core/Configuracion/configuracion.service';

@Component({
  selector: 'app-fw-tipos-ajustes',
  templateUrl: './fw-tipos-ajustes.page.html',
  styleUrls: ['./fw-tipos-ajustes.page.scss']
})

export class FwTiposAjustesPage implements OnInit, OnDestroy {

  tCodFormularioActual: string = "fTipoAjustes";
  tConfiguracionFormulario: FormularioConfiguracion = new FormularioConfiguracion();
  tPermisoAgregar: boolean = false;
  tPermisoEditar: boolean = false;
  tPermisoEliminar: boolean = false;
  tHabilitarGuardar: boolean = false;

  tUsuarioActual: Usuario = new Usuario();
  tEmpresaActual: Empresas = new Empresas();
  tEmpresa$: Subscription;
  tUsuario$: Subscription;

  tTitulo: string = "Tipos de ajustes";
  tNuevo: boolean = true;
  tPosicion: number = 0;
  tPagina: number = 1;
  tTamanoPag: number = 5;

  tTiposAjustes: TipoAjustesInventario[] = [];
  tTipoAjusteActual: TipoAjustesInventario = new TipoAjustesInventario();

  tFormularios: Formulario[] = [];

  tFiltros: Filtros[] = [];
  tFiltroActual: Filtros = new Filtros();
  tFiltroBusqueda: string = "";

  tConfiguracion$: Subscription;
  tHayConfiguracionWs$: Subscription;
  tStrConfiguracion: string = 'TipoAjuste/TiposAjusteInventario.Config.json';

  constructor(public router: Router,
    private tSesion: SessionService,
    public Ws: ClsServiciosService,
    private tConfiguracionService: ConfiguracionService,
    private toastr: ToastrService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService,
    private tConfServices: ConfirmacionService) {

    this.tFiltroBusqueda = "";
    this.tPosicion = 0;

    this.tFiltros = [

      { Codfiltro: "Codtipoajusteinv", Filtro: "Código" },
      { Codfiltro: "Descripcion", Filtro: "Descripción" },

    ]

  }

  ngOnInit() {

    this.tConfiguracion$ = this.tConfiguracionService
      .Consumir_Obtener_Configuracion(this.tStrConfiguracion)
      .subscribe((tConfiguracion: FormularioConfiguracion) => {

        this.tConfiguracionFormulario = tConfiguracion;
        this.tTitulo = this.tConfiguracionFormulario.Titulo;
        this.tErroresServices.tTituloMsj = this.tConfiguracionFormulario.ModalMensaje.Titulo;
        this.tMsjServices.tTituloMsj = this.tConfiguracionFormulario.ModalMensaje.Titulo;
        this.tConfServices.tTituloConf = this.tConfiguracionFormulario.ModalConfirmacion.Titulo;

      });

    this.tEmpresa$ = this.tSesion.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    })

    this.tUsuario$ = this.tSesion.tUsuarioActual$.subscribe((tUsuario: Usuario) => {

      this.tUsuarioActual = tUsuario;

    })

    var tTienePermiso: boolean = this.tSesion.ObtenerPermisoAcceso(this.tCodFormularioActual);
    this.tPermisoAgregar = this.tSesion.ObtenerPermisoAgregar();
    this.tPermisoEditar = this.tSesion.ObtenerPermisoEditar();
    this.tPermisoEliminar = this.tSesion.ObtenerPermisoEliminar();

    if (tTienePermiso == true) {

      this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
        .subscribe((tHayConfig: boolean) => {

          if (tHayConfig === true) {

            this.Buscar_TipoAjustes();
            this.Nuevo();

          }

        });

    }
    else {

      this.tMsjServices.AbrirModalMsj(environment.msjSinAcceso);
      this.router.navigate(['/Home']);

    }

  }

  ngOnDestroy() {

    this.tEmpresa$.unsubscribe();
    this.tUsuario$.unsubscribe();
    this.tConfiguracion$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();

  }

  //#region "Funciones de busqueda"

  Buscar_TipoAjustes(): void {

    this.Ws.TipoAjuste()
      .Consumir_Obtener_TipoAjuste(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa)
      .subscribe(Respuesta => {

        this.tTiposAjustes = [];

        if (Respuesta.Resultado == "N") {

          // this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);
        }
        else {

          this.tTiposAjustes = Respuesta.Datos;
          this.tFiltroActual = this.tFiltros[0];

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }


  //#endregion

  //#region "CRUD"

  Nuevo_TipoAjustes(): void {

    var tTipoAjustes: TipoAjustesInventarioEntrada;
    tTipoAjustes = {
      "Id": 0,
      "Corporacion": this.tEmpresaActual.Corporacion,
      "Empresa": this.tEmpresaActual.Empresa,
      "Codtipoajusteinv": "",
      "Descripcion": this.tTipoAjusteActual.Descripcion,
      "Operacion": this.tTipoAjusteActual.Operacion,
      "Usuariocreador": this.tUsuarioActual.Codusuario,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }

    this.Ws.TipoAjuste()
      .Consumir_Crear_TipoAjuste(tTipoAjustes)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.toastr.success('El tipo de ajuste se ha creado de manera exitosa!');
          this.tFiltroActual = this.tFiltros[0];
          this.Buscar_TipoAjustes();
          this.Nuevo();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Modificar_TipoAjustes(): void {

    var tModificarTipoAjustesInventario: TipoAjustesInventarioEntrada;
    tModificarTipoAjustesInventario = {
      "Id": 0,
      "Corporacion": this.tEmpresaActual.Corporacion,
      "Empresa": this.tEmpresaActual.Empresa,
      "Codtipoajusteinv": this.tTipoAjusteActual.Codtipoajusteinv,
      "Descripcion": this.tTipoAjusteActual.Descripcion,
      "Operacion": this.tTipoAjusteActual.Operacion,
      "Usuariocreador": this.tUsuarioActual.Codusuario,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }

    this.Ws.TipoAjuste()
      .Consumir_Modificar_TipoAjuste(tModificarTipoAjustesInventario)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.toastr.success('El tipo de ajuste se ha modificado de manera exitosa!');
          this.tFiltroActual = this.tFiltros[0];
          this.Buscar_TipoAjustes();
          this.Nuevo();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Eliminar_TipoAjustes(): void {

    var tEliminarTipoAjustes: TipoAjustesInventarioEntrada;
    tEliminarTipoAjustes = {
      "Id": 0,
      "Corporacion": this.tEmpresaActual.Corporacion,
      "Empresa": this.tEmpresaActual.Empresa,
      "Codtipoajusteinv": this.tTipoAjusteActual.Codtipoajusteinv,
      "Descripcion": this.tTipoAjusteActual.Descripcion,
      "Operacion": this.tTipoAjusteActual.Operacion,
      "Usuariocreador": this.tUsuarioActual.Codusuario,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }

    this.Ws.TipoAjuste()
      .Consumir_Eliminar_TipoAjuste(tEliminarTipoAjustes)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.toastr.success('El tipo de ajuste se ha eliminado de manera exitosa!');
          this.tFiltroActual = this.tFiltros[0];
          this.Buscar_TipoAjustes();
          this.Nuevo();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  Nuevo(): void {

    this.tHabilitarGuardar = this.tPermisoAgregar;
    this.tNuevo = true;
    this.tTipoAjusteActual = new TipoAjustesInventario();

    this.toastr.info('Por favor llene los datos necesarios para registrar una nueva Tipo Ajuste!');


  }

  Seleccionar_TipoAjustes(tTipoAjuste_Sel: TipoAjustesInventario, tPos: number): void {

    this.tHabilitarGuardar = this.tPermisoEditar;
    this.tNuevo = false;
    this.tPosicion = tPos;
    this.tTipoAjusteActual = new TipoAjustesInventario();
    Object.assign(this.tTipoAjusteActual, tTipoAjuste_Sel);
    this.tTipoAjusteActual.Usuariomodificador = this.tUsuarioActual.Usuariomodificador;

  }

  Seleccionar_TipoAjustes_Eliminar(tTipoAjuste_Sel: TipoAjustesInventario): void {

    this.tTipoAjusteActual = new TipoAjustesInventario();
    Object.assign(this.tTipoAjusteActual, tTipoAjuste_Sel);
    this.tTipoAjusteActual.Usuariomodificador = this.tUsuarioActual.Usuariomodificador;

    this.tConfServices
      .AbrirModalConf(this.tConfiguracionFormulario.ModalConfirmacion.Eliminacion)
      .then(Resultado => {

        switch (Resultado) {

          case "S": {

            this.Eliminar_TipoAjustes();
            break;

          }

        };

      });

  }

  Cancelar(): void {

    this.Nuevo();

  }
  ValidarFormulario(): boolean {

    let valido: boolean = false;

    if (this.tTipoAjusteActual.Descripcion == "") {

      this.toastr.warning('Debe ingresar una descripciòn!', 'Disculpe!');
      return valido;

    }

    return true;
  }


  Agregar(): void {

    if (this.ValidarFormulario() == false) {
      return
    }

    switch (this.tNuevo) {
      case true: {

        this.tConfServices
          .AbrirModalConf(this.tConfiguracionFormulario.ModalConfirmacion.Agregar)
          .then(Resultado => {

            switch (Resultado) {

              case "S": {

                this.Nuevo_TipoAjustes();
                break;

              }

            };

          });

        break;

      }
      default: {

        this.tConfServices
          .AbrirModalConf(this.tConfiguracionFormulario.ModalConfirmacion.Edicion)
          .then(Resultado => {

            switch (Resultado) {

              case "S": {

                this.Modificar_TipoAjustes();
                break;

              }

            };

          });

        break;

      }
    }

  }

}

