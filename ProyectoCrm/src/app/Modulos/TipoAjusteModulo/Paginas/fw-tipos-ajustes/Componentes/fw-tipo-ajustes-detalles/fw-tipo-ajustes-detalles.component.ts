import { Component, OnInit, OnChanges, Input, SimpleChanges } from '@angular/core';
import { TipoAjustesInventario, TiposOpcion } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-tipo-ajustes-detalles',
  templateUrl: './fw-tipo-ajustes-detalles.component.html',
  styleUrls: ['./fw-tipo-ajustes-detalles.component.scss']
})
export class FwTipoAjustesDetallesComponent implements OnInit, OnChanges {

  @Input('TipoAjuste') tTipoAjuste: TipoAjustesInventario = new TipoAjustesInventario();

  tTipoOperacionActual: TiposOpcion = new TiposOpcion();
  tTiposOperaciones: TiposOpcion[] = [];

  constructor() {

    this.tTiposOperaciones = [

      { Codigo: "+", Descripcion: "Suma" },
      { Codigo: "-", Descripcion: "Resta" },

    ]

  }

  ngOnInit(): void {

    this.tTipoOperacionActual = this.tTiposOperaciones[0] || new TiposOpcion();
    this.CambiarTipo();

  }

  ngOnChanges(changes: SimpleChanges) {

    if ('tTipoAjuste' in changes) {

      this.tTipoOperacionActual = this.tTiposOperaciones.find(tTipo => {
        return tTipo.Codigo == this.tTipoAjuste.Operacion;
      }) || this.tTiposOperaciones[0] || new TiposOpcion();
      this.CambiarTipo();

    }

  }

  CambiarTipo() {
    this.tTipoAjuste.Operacion = this.tTipoOperacionActual.Codigo;
  }

}
