import { Component, OnInit, Input } from '@angular/core';
import { TipoAjustesInventario } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-tipo-ajustes-cabecera',
  templateUrl: './fw-tipo-ajustes-cabecera.component.html',
  styleUrls: ['./fw-tipo-ajustes-cabecera.component.scss']
})
export class FwTipoAjustesCabeceraComponent implements OnInit {

  @Input('TipoAjuste') tTipoAjuste: TipoAjustesInventario = new TipoAjustesInventario();

  constructor() { }

  ngOnInit(): void {
  }

}
