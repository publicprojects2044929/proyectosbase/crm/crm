import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwTipoAjustesDetallesComponent } from './fw-tipo-ajustes-detalles.component';

describe('FwTipoAjustesDetallesComponent', () => {
  let component: FwTipoAjustesDetallesComponent;
  let fixture: ComponentFixture<FwTipoAjustesDetallesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwTipoAjustesDetallesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwTipoAjustesDetallesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
