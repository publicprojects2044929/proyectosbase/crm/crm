import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwTiposAjustesPage } from './fw-tipos-ajustes.page';

describe('FwTiposAjustesPage', () => {
  let component: FwTiposAjustesPage;
  let fixture: ComponentFixture<FwTiposAjustesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwTiposAjustesPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwTiposAjustesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
