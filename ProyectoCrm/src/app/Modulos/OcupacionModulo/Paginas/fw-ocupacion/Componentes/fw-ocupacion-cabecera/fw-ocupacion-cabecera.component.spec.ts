import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwOcupacionCabeceraComponent } from './fw-ocupacion-cabecera.component';

describe('FwOcupacionCabeceraComponent', () => {
  let component: FwOcupacionCabeceraComponent;
  let fixture: ComponentFixture<FwOcupacionCabeceraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwOcupacionCabeceraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwOcupacionCabeceraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
