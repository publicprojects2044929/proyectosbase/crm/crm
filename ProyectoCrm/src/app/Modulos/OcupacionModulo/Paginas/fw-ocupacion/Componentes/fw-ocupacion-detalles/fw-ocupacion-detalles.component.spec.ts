import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwOcupacionDetallesComponent } from './fw-ocupacion-detalles.component';

describe('FwOcupacionDetallesComponent', () => {
  let component: FwOcupacionDetallesComponent;
  let fixture: ComponentFixture<FwOcupacionDetallesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwOcupacionDetallesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwOcupacionDetallesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
