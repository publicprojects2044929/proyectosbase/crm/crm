import { Component, OnInit, Input } from '@angular/core';
import { Ocupacion } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-ocupacion-cabecera',
  templateUrl: './fw-ocupacion-cabecera.component.html',
  styleUrls: ['./fw-ocupacion-cabecera.component.scss']
})
export class FwOcupacionCabeceraComponent implements OnInit {

  @Input('Ocupacion') tOcupacion: Ocupacion = new Ocupacion();

  constructor() { }

  ngOnInit(): void {
  }

}
