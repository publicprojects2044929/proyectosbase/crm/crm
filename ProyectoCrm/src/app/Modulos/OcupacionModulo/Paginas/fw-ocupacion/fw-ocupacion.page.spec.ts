import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwOcupacionPage } from './fw-ocupacion.page';

describe('FwOcupacionPage', () => {
  let component: FwOcupacionPage;
  let fixture: ComponentFixture<FwOcupacionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwOcupacionPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwOcupacionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
