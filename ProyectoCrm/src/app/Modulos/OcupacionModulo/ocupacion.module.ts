import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from 'src/app/Shared/shared.module'

import { OcupacionRoutingModule } from './ocupacion-routing.module';
import { FwOcupacionPage } from './Paginas/fw-ocupacion/fw-ocupacion.page';
import { FwOcupacionCabeceraComponent } from './Paginas/fw-ocupacion/Componentes/fw-ocupacion-cabecera/fw-ocupacion-cabecera.component';
import { FwOcupacionDetallesComponent } from './Paginas/fw-ocupacion/Componentes/fw-ocupacion-detalles/fw-ocupacion-detalles.component';


@NgModule({
  declarations: [FwOcupacionPage, FwOcupacionCabeceraComponent, FwOcupacionDetallesComponent],
  imports: [
    CommonModule,
    OcupacionRoutingModule,
    SharedModule,
    FormsModule,
    NgbModule
  ]
})
export class OcupacionModule { }
