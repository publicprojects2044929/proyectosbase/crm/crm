import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from 'src/app/Shared/shared.module';
import { ColorSliderModule } from 'ngx-color/slider';

import { ConfiguracionModuloRoutingModule } from './configuracion-modulo-routing.module';
import { FwConfiguracionPage } from './Paginas/fw-configuracion/fw-configuracion.page';
import { FwEstatusPage } from './Paginas/fw-estatus/fw-estatus.page';
import { FwTiposPage } from './Paginas/fw-tipos/fw-tipos.page';
import { FwConfiguracionCabeceraComponent } from './Paginas/fw-configuracion/Componentes/fw-configuracion-cabecera/fw-configuracion-cabecera.component';
import { FwConfiguracionDetalleComponent } from './Paginas/fw-configuracion/Componentes/fw-configuracion-detalle/fw-configuracion-detalle.component';
import { FwEstatusCabeceraComponent } from './Paginas/fw-estatus/Componentes/fw-estatus-cabecera/fw-estatus-cabecera.component';
import { FwEstatusDetalleComponent } from './Paginas/fw-estatus/Componentes/fw-estatus-detalle/fw-estatus-detalle.component';
import { FwTiposCabeceraComponent } from './Paginas/fw-tipos/Componentes/fw-tipos-cabecera/fw-tipos-cabecera.component';
import { FwTiposDetalleComponent } from './Paginas/fw-tipos/Componentes/fw-tipos-detalle/fw-tipos-detalle.component';


@NgModule({
  declarations: [FwConfiguracionPage, FwEstatusPage, FwTiposPage, FwConfiguracionCabeceraComponent, FwConfiguracionDetalleComponent, FwEstatusCabeceraComponent, FwEstatusDetalleComponent, FwTiposCabeceraComponent, FwTiposDetalleComponent],
  imports: [
    CommonModule,
    ConfiguracionModuloRoutingModule,
    SharedModule,
    FormsModule,
    NgbModule,
    ColorSliderModule
  ]
})
export class ConfiguracionModuloModule { }
