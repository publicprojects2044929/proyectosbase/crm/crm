import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FwPlantillaComponent  } from 'src/app/Shared/Formularios/fw-plantilla/fw-plantilla.component';
import { FwConfiguracionPage } from './Paginas/fw-configuracion/fw-configuracion.page';
import { FwEstatusPage } from './Paginas/fw-estatus/fw-estatus.page';
import { FwTiposPage } from './Paginas/fw-tipos/fw-tipos.page';

const routes: Routes = [
  {
    path: '',
    component: FwPlantillaComponent,
    children:[
      {
        path: 'Configuracion',
        component: FwConfiguracionPage,
        data:{
          Codformulario: 'fConfiguracion'
        }
      },
      {
        path: 'Tipos',
        component: FwTiposPage,
        data:{
          Codformulario: 'fTipo'
        }
      },
      {
        path: 'Estatus',
        component: FwEstatusPage,
        data:{
          Codformulario: 'fEstatus'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfiguracionModuloRoutingModule { }
