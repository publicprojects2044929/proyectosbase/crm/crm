import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwEstatusDetalleComponent } from './fw-estatus-detalle.component';

describe('FwEstatusDetalleComponent', () => {
  let component: FwEstatusDetalleComponent;
  let fixture: ComponentFixture<FwEstatusDetalleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwEstatusDetalleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwEstatusDetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
