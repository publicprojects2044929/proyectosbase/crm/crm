import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Estatus } from 'src/app/Clases/Estructura';
import { ColorEvent } from 'ngx-color';

@Component({
  selector: 'app-fw-estatus-detalle',
  templateUrl: './fw-estatus-detalle.component.html',
  styleUrls: ['./fw-estatus-detalle.component.scss']
})
export class FwEstatusDetalleComponent implements OnInit {

  @Input('Estatus') tEstatus: Estatus = new Estatus();

  @Output('ModificarEstatus') tModificar: EventEmitter<Estatus> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  CambiarColor($event: ColorEvent){

    this.tEstatus.Hexadecimal = $event.color.hex;

  }

  ValidarEstatus(): void {

    // this.Modificar_Estatus(tEstatusSel);
    this.tModificar.emit(this.tEstatus);

  }

}
