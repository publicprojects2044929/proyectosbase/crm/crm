import { Component, OnInit, Input } from '@angular/core';
import { Tabla } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-estatus-cabecera',
  templateUrl: './fw-estatus-cabecera.component.html',
  styleUrls: ['./fw-estatus-cabecera.component.scss']
})
export class FwEstatusCabeceraComponent implements OnInit {

  @Input('Tabla') tTabla: Tabla = new Tabla();

  constructor() { }

  ngOnInit(): void {
  }

}
