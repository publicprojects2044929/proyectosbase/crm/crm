import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwEstatusCabeceraComponent } from './fw-estatus-cabecera.component';

describe('FwEstatusCabeceraComponent', () => {
  let component: FwEstatusCabeceraComponent;
  let fixture: ComponentFixture<FwEstatusCabeceraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwEstatusCabeceraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwEstatusCabeceraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
