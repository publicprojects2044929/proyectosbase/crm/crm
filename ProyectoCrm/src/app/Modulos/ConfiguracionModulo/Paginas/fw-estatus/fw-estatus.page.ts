import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormularioConfiguracion } from 'src/app/Clases/EstructuraConfiguracion';
import { ManejarImagenes } from 'src/app/Manejadores/cls-procedimientos';
import { Filtros, Usuario, Empresas, Tabla, Estatus } from 'src/app/Clases/Estructura';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { SessionService } from 'src/app/Core/Session/session.service';
import { ConfiguracionService } from 'src/app/Core/Configuracion/configuracion.service';
import { ToastrService } from 'ngx-toastr';
import { ErroresService } from 'src/app/Modales/mensajes/fw-modal-msj/errores.service';
import { MensajesService } from 'src/app/Modales/mensajes/fw-modal-msj/mensajes.service';
import { ConfirmacionService } from 'src/app/Modales/confirmacion/fw-modal-confirmacion/confirmacion.service';
import { environment } from 'src/environments/environment';
import { EstatusEntrada } from 'src/app/Clases/EstructuraEntrada';
import { ColorEvent } from 'ngx-color';

@Component({
  selector: 'app-fw-estatus',
  templateUrl: './fw-estatus.page.html',
  styleUrls: ['./fw-estatus.page.scss']
})
export class FwEstatusPage implements OnInit, OnDestroy {

  tConfiguracionFormulario: FormularioConfiguracion = new FormularioConfiguracion();
  tManejarImagen: ManejarImagenes = new ManejarImagenes();
  tCodFormularioActual: string = "fEstatus";
  tPermisoAgregar: boolean = false;
  tPermisoEditar: boolean = false;
  tPermisoEliminar: boolean = false;
  tHabilitarGuardar: boolean = false;
  tHabilitarEliminar: boolean = false;

  tFiltros: Filtros[] = [];
  tFiltroActual: Filtros = new Filtros();
  tFiltroBusqueda: string = "";

  tTitulo: string = "";

  tPosicion: number = 0;
  tPagina: number = 1;
  tTamanoPag: number = 5;

  tPosicionEst: number = 0;
  tPaginaEst: number = 1;
  tTamanoPagEst: number = 5;

  tNuevo: boolean = true;
  tEditar: boolean = false
  tBloquearBuscar: boolean = false;
  tUsuarioActual: Usuario = new Usuario();
  tEmpresaActual: Empresas = new Empresas();
  tEmpresa$: Subscription;
  tUsuario$: Subscription;

  tConfiguracion$: Subscription;
  tHayConfiguracionWs$: Subscription;
  tStrConfiguracion: string = 'Estatus/Estatus.Config.json';

  tTablas: Tabla[] = [];
  tTablaActual: Tabla = new Tabla();

  constructor(public router: Router,
    public Ws: ClsServiciosService,
    private tSesion: SessionService,
    private tConfiguracionService: ConfiguracionService,
    private toastr: ToastrService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService,
    private tConfServices: ConfirmacionService) {

    this.tFiltroBusqueda = "";
    this.tPosicion = 0;

    this.tFiltros = [

      { Codfiltro: "Descripcion", Filtro: "Descripción" },

    ]

  }

  ngOnInit() {

    this.tConfiguracion$ = this.tConfiguracionService
      .Consumir_Obtener_Configuracion(this.tStrConfiguracion)
      .subscribe((tConfiguracion: FormularioConfiguracion) => {

        this.tConfiguracionFormulario = tConfiguracion;
        this.tTitulo = this.tConfiguracionFormulario.Titulo;
        this.tConfServices.tTituloConf = this.tConfiguracionFormulario.ModalConfirmacion.Titulo;
        this.tErroresServices.tTituloMsj = this.tConfiguracionFormulario.ModalMensaje.Titulo;
        this.tMsjServices.tTituloMsj = this.tConfiguracionFormulario.ModalMensaje.Titulo;

      });

    this.tEmpresa$ = this.tSesion.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    })

    this.tUsuario$ = this.tSesion.tUsuarioActual$.subscribe((tUsuario: Usuario) => {

      this.tUsuarioActual = tUsuario;

    })

    var tTienePermiso: boolean = this.tSesion.ObtenerPermisoAcceso(this.tCodFormularioActual);
    this.tPermisoAgregar = this.tSesion.ObtenerPermisoAgregar();
    this.tPermisoEditar = this.tSesion.ObtenerPermisoEditar();
    this.tPermisoEliminar = this.tSesion.ObtenerPermisoEliminar();

    if (tTienePermiso == true) {

      this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
        .subscribe((tHayConfig: boolean) => {

          if (tHayConfig === true) {

            this.BuscarTabla();
            this.Nuevo();

          }

        });

    }
    else {

      this.tMsjServices.AbrirModalMsj(environment.msjSinAcceso);
      this.router.navigate(['/Home']);

    }


  }

  ngOnDestroy() {

    this.tEmpresa$.unsubscribe();
    this.tUsuario$.unsubscribe();
    this.tConfiguracion$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();

  }

  //#region "Funciones de busqueda"

  BuscarTabla(): void {

    this.Ws
      .Tabla()
      .Consumir_Obtener_TablaEstatus(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          // this.tMensajeMsj = Respuesta.Mensaje;
          // this.AbrirModalMsj();

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          let tTablaTem: Tabla[] = Respuesta.Datos;
          this.tTablas = tTablaTem.filter(tTabla => {

            return tTabla.LEstatus.length > 0;

          }) || [];

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  //#Region "CRUD"

  Modificar_Estatus(tEstatusSel: Estatus): void {

    let tModificar_Estatus: EstatusEntrada;

    tModificar_Estatus = {

      "Id": tEstatusSel.Id,
      "Corporacion": tEstatusSel.Corporacion,
      "Empresa": tEstatusSel.Empresa,
      "Codestatus": tEstatusSel.Codestatus,
      "Tabla": tEstatusSel.Tabla,
      "Descripcion": tEstatusSel.Descripcion,
      "Secuencia": tEstatusSel.Secuencia,
      "Hexadecimal": tEstatusSel.Hexadecimal,
      "Usuariocreador": tEstatusSel.Usuariocreador,
      "Usuariomodificador": this.tUsuarioActual.Codusuario

    }

    this.Ws
      .Estatus()
      .Consumir_Modificar_Estatus(tModificar_Estatus)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tErroresServices.MostrarError(environment.msjError);

        }
        else {

          this.toastr.clear();
          this.toastr.success('El estatus se modifico correctamente');
          this.BuscarTabla();
          this.Nuevo();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#Endregion

  Nuevo(): void {

    this.tFiltroBusqueda = "";
    this.tFiltroActual = this.tFiltros[0] || new Filtros();
    this.tHabilitarGuardar = this.tPermisoAgregar;
    this.tNuevo = true;
    this.tTablaActual = new Tabla();

  }

  Seleccionar_Tabla(tTablaSel: Tabla) {

    this.tHabilitarGuardar = this.tPermisoEditar;
    this.tNuevo = false;
    this.tTablaActual = new Tabla();
    this.tTablaActual = tTablaSel;

  }

  Cancelar(): void {

    this.Nuevo();

  }

  Agregar(): void {

    this.Nuevo();

  }

}
