import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwEstatusComponent } from './fw-estatus.page';

describe('FwEstatusPage', () => {
  let component: FwEstatusPage;
  let fixture: ComponentFixture<FwEstatusPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwEstatusPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwEstatusPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
