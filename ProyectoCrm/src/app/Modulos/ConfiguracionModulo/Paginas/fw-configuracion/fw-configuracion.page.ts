import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormularioConfiguracion, Configuracion } from 'src/app/Clases/EstructuraConfiguracion';
import { ManejarImagenes } from 'src/app/Manejadores/cls-procedimientos';
import { Usuario, Empresas, Tabla, Configuracion as mrConfiguracion, Filtros } from 'src/app/Clases/Estructura';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { SessionService } from 'src/app/Core/Session/session.service';
import { ConfiguracionService } from 'src/app/Core/Configuracion/configuracion.service';
import { ToastrService } from 'ngx-toastr';
import { ErroresService } from 'src/app/Modales/mensajes/fw-modal-msj/errores.service';
import { MensajesService } from 'src/app/Modales/mensajes/fw-modal-msj/mensajes.service';
import { ConfirmacionService } from 'src/app/Modales/confirmacion/fw-modal-confirmacion/confirmacion.service';
import { environment } from 'src/environments/environment';
import { ConfiguracionEntrada } from 'src/app/Clases/EstructuraEntrada';

@Component({
  selector: 'app-fw-configuracion',
  templateUrl: './fw-configuracion.page.html',
  styleUrls: ['./fw-configuracion.page.scss']
})
export class FwConfiguracionPage implements OnInit, OnDestroy {

  tConfiguracionFormulario: FormularioConfiguracion = new FormularioConfiguracion();
  tManejarImagen: ManejarImagenes = new ManejarImagenes();
  tCodFormularioActual: string = "fConfiguracion";
  tPermisoAgregar: boolean = false;
  tPermisoEditar: boolean = false;
  tPermisoEliminar: boolean = false;
  tHabilitarGuardar: boolean = false;
  tHabilitarEliminar: boolean = false;

  tFiltros: Filtros[] = [];
  tFiltroActual: Filtros = new Filtros();
  tFiltroBusqueda: string = "";

  tTitulo: string = "";

  tPosicion: number = 0;
  tPagina: number = 1;
  tTamanoPag: number = 5;

  tPosicionConfig: number = 0;
  tPaginaConfig: number = 1;
  tTamanoPagConfig: number = 5;

  tNuevo: boolean = true;
  tEditar: boolean = false
  tBloquearBuscar: boolean = false;
  tUsuarioActual: Usuario = new Usuario();
  tEmpresaActual: Empresas = new Empresas();
  tEmpresa$: Subscription;
  tUsuario$: Subscription;

  tConfiguracion: Configuracion = new Configuracion();

  tConfiguracion$: Subscription;
  tHayConfiguracionWs$: Subscription;
  tStrConfiguracion: string = 'Configuracion/Configuracion.Config.json';

  tTablas: Tabla[] = [];
  tTablaActual: Tabla = new Tabla();

  constructor(public router: Router,
    public Ws: ClsServiciosService,
    private tSesion: SessionService,
    private tConfiguracionService: ConfiguracionService,
    private toastr: ToastrService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService,
    private tConfServices: ConfirmacionService) {

    this.tFiltroBusqueda = "";
    this.tPosicion = 0;

    this.tFiltros = [

      { Codfiltro: "Descripcion", Filtro: "Descripción" },

    ]

  }

  ngOnInit() {

    this.tConfiguracion$ = this.tConfiguracionService
      .Consumir_Obtener_Configuracion(this.tStrConfiguracion)
      .subscribe((tConfiguracion: FormularioConfiguracion) => {

        this.tConfiguracionFormulario = tConfiguracion;
        this.tTitulo = this.tConfiguracionFormulario.Titulo;
        this.tConfServices.tTituloConf = this.tConfiguracionFormulario.ModalConfirmacion.Titulo;
        this.tErroresServices.tTituloMsj = this.tConfiguracionFormulario.ModalMensaje.Titulo;
        this.tMsjServices.tTituloMsj = this.tConfiguracionFormulario.ModalMensaje.Titulo;

      });

    this.tEmpresa$ = this.tSesion.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    })

    this.tUsuario$ = this.tSesion.tUsuarioActual$.subscribe((tUsuario: Usuario) => {

      this.tUsuarioActual = tUsuario;

    })

    var tTienePermiso: boolean = this.tSesion.ObtenerPermisoAcceso(this.tCodFormularioActual);
    this.tPermisoAgregar = this.tSesion.ObtenerPermisoAgregar();
    this.tPermisoEditar = this.tSesion.ObtenerPermisoEditar();
    this.tPermisoEliminar = this.tSesion.ObtenerPermisoEliminar();

    if (tTienePermiso == true) {

      this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
        .subscribe((tHayConfig: boolean) => {

          if (tHayConfig === true) {

            this.BuscarTabla();
            this.Nuevo();

          }

        });

    }
    else {

      this.tMsjServices.AbrirModalMsj(environment.msjSinAcceso);
      this.router.navigate(['/Home']);

    }

  }

  ngOnDestroy() {

    this.tEmpresa$.unsubscribe();
    this.tUsuario$.unsubscribe();
    this.tConfiguracion$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();

  }

  //#region "Funciones de busqueda"

  BuscarTabla(): void {

    this.Ws
      .Tabla()
      .Consumir_Obtener_Tabla(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          // this.tMensajeMsj = Respuesta.Mensaje;
          // this.AbrirModalMsj();

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          let tTablasTemp: Tabla[] = Respuesta.Datos;

          this.tTablas = tTablasTemp.filter(tTabla => {

            let tConfigVisible: number = tTabla.LConfiguracion.filter(tConfig => {
              return tConfig.Oculto === 'N'
            }).length || 0;

            if (tConfigVisible > 0) {

              return tTabla;

            }

          }) || [];

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  //#Region "CRUD"

  Modificar_Configuracion(tConfiguracionSel: mrConfiguracion): void {

    let tModificar_Configuracion: ConfiguracionEntrada;

    tModificar_Configuracion = {

      "Id": tConfiguracionSel.Id,
      "Corporacion": tConfiguracionSel.Corporacion,
      "Empresa": tConfiguracionSel.Empresa,
      "Tabla": tConfiguracionSel.Tabla,
      "Columna": tConfiguracionSel.Columna,
      "Descripcion": tConfiguracionSel.Descripcion,
      "Tipo": tConfiguracionSel.Tipo,
      "Valor": tConfiguracionSel.Valor,
      "Oculto": tConfiguracionSel.Oculto,
      "Editable": tConfiguracionSel.Editable,
      "Expresion_regular": tConfiguracionSel.Expresion_regular,
      "Usuariocreador": tConfiguracionSel.Usuariocreador,
      "Usuariomodificador": this.tUsuarioActual.Codusuario

    }

    this.Ws
      .Configuraciones()
      .Consumir_Modificar_Configuracion(tModificar_Configuracion)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tErroresServices.MostrarError(environment.msjError);

        }
        else {

          this.toastr.clear();
          this.toastr.success('La configuración se edito correctamente');
          this.BuscarTabla();
          this.Nuevo();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#Endregion

  Nuevo(): void {

    this.tFiltroBusqueda = "";
    this.tFiltroActual = this.tFiltros[0] || new Filtros();
    this.tHabilitarGuardar = this.tPermisoAgregar;
    this.tNuevo = true;
    this.tTablaActual = new Tabla();

  }

  Seleccionar_Tabla(tTablaSel: Tabla) {

    this.tHabilitarGuardar = this.tPermisoEditar;
    this.tNuevo = false;
    this.tTablaActual = new Tabla();
    this.tTablaActual = tTablaSel;
    this.tTablaActual.LConfiguracion = this.tTablaActual.LConfiguracion.filter(tConfiguracion => {

      return tConfiguracion.Oculto === 'N';

    });

  }

  Cancelar(): void {

    this.Nuevo();

  }

  Agregar(): void {

    this.Nuevo();

  }

}
