import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwConfiguracionDetalleComponent } from './fw-configuracion-detalle.component';

describe('FwConfiguracionDetalleComponent', () => {
  let component: FwConfiguracionDetalleComponent;
  let fixture: ComponentFixture<FwConfiguracionDetalleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwConfiguracionDetalleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwConfiguracionDetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
