import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Configuracion } from 'src/app/Clases/Estructura';
import { isNumber } from 'util';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-fw-configuracion-detalle',
  templateUrl: './fw-configuracion-detalle.component.html',
  styleUrls: ['./fw-configuracion-detalle.component.scss']
})
export class FwConfiguracionDetalleComponent implements OnInit {

  @Input('Configuracion') tConfiguracion: Configuracion = new Configuracion();

  @Output('ModificarConfiguracion') tModificar: EventEmitter<Configuracion> = new EventEmitter();

  constructor(private toastr: ToastrService) { }

  ngOnInit(): void {
  }

  Editable(tConfiguracion: Configuracion): boolean {

    if (tConfiguracion.Editable === 'S') {

      return false;

    }
    else {

      return true;

    }

  }

  ValidarConfiguracion(tConfiguracionSel: Configuracion): void {

    if (tConfiguracionSel.Valor === '') {

      return;

    }

    switch (tConfiguracionSel.Tipo) {
      case "INT":

        if (isNumber(tConfiguracionSel.Valor) === false) {

          this.toastr.clear();
          this.toastr.warning('El valor tiene que ser un número válido');
          return;

        }

        break;

      default:

        break;

    }

    // this.Modificar_Configuracion(tConfiguracionSel);
    this.tModificar.emit(tConfiguracionSel);

  }

}
