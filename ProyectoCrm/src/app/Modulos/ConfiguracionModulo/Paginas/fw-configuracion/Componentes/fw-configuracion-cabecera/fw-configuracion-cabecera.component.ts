import { Component, OnInit, Input } from '@angular/core';
import { Tabla } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-configuracion-cabecera',
  templateUrl: './fw-configuracion-cabecera.component.html',
  styleUrls: ['./fw-configuracion-cabecera.component.scss']
})
export class FwConfiguracionCabeceraComponent implements OnInit {

  @Input('Tabla') tTabla: Tabla = new Tabla();

  constructor() { }

  ngOnInit(): void {
  }

}
