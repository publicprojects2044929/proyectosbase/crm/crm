import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwConfiguracionCabeceraComponent } from './fw-configuracion-cabecera.component';

describe('FwConfiguracionCabeceraComponent', () => {
  let component: FwConfiguracionCabeceraComponent;
  let fixture: ComponentFixture<FwConfiguracionCabeceraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwConfiguracionCabeceraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwConfiguracionCabeceraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
