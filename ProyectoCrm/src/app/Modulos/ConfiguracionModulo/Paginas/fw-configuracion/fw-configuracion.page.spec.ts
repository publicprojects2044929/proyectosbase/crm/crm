import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwConfiguracionPage } from './fw-configuracion.page';

describe('FwConfiguracionPage', () => {
  let component: FwConfiguracionPage;
  let fixture: ComponentFixture<FwConfiguracionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwConfiguracionPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwConfiguracionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
