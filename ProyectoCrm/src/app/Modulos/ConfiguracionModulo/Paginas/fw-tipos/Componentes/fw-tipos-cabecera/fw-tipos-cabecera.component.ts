import { Component, OnInit, Input } from '@angular/core';
import { Tabla } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-tipos-cabecera',
  templateUrl: './fw-tipos-cabecera.component.html',
  styleUrls: ['./fw-tipos-cabecera.component.scss']
})
export class FwTiposCabeceraComponent implements OnInit {

  @Input('Tabla') tTabla: Tabla = new Tabla();

  constructor() { }

  ngOnInit(): void {
  }

}
