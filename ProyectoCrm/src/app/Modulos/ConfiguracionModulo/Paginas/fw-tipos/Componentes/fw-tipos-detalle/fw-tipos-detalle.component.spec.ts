import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwTiposDetalleComponent } from './fw-tipos-detalle.component';

describe('FwTiposDetalleComponent', () => {
  let component: FwTiposDetalleComponent;
  let fixture: ComponentFixture<FwTiposDetalleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwTiposDetalleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwTiposDetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
