import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwTiposCabeceraComponent } from './fw-tipos-cabecera.component';

describe('FwTiposCabeceraComponent', () => {
  let component: FwTiposCabeceraComponent;
  let fixture: ComponentFixture<FwTiposCabeceraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwTiposCabeceraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwTiposCabeceraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
