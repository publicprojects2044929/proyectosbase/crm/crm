import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Tipo } from 'src/app/Clases/Estructura';
import { ColorEvent } from 'ngx-color';

@Component({
  selector: 'app-fw-tipos-detalle',
  templateUrl: './fw-tipos-detalle.component.html',
  styleUrls: ['./fw-tipos-detalle.component.scss']
})
export class FwTiposDetalleComponent implements OnInit {

  @Input('Tipo') tTipo: Tipo = new Tipo();

  @Output('ModificarTipo') tModificar: EventEmitter<Tipo> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  CambiarColor($event: ColorEvent){

    this.tTipo.Hexadecimal = $event.color.hex;

  }

  ValidarTipos(): void {

    // this.Modificar_Tipos(tTipoSel);

    this.tModificar.emit(this.tTipo);

  }

}
