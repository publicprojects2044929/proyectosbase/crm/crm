import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwTiposPage } from './fw-tipos.page';

describe('FwTiposPage', () => {
  let component: FwTiposPage;
  let fixture: ComponentFixture<FwTiposPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwTiposPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwTiposPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
