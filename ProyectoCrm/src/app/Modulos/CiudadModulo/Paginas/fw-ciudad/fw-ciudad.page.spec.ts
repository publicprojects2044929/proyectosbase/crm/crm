import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwCiudadPage } from './fw-ciudad.page';

describe('FwCiudadPage', () => {
  let component: FwCiudadPage;
  let fixture: ComponentFixture<FwCiudadPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwCiudadPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwCiudadPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
