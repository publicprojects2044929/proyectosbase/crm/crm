import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwCiudadCabeceraComponent } from './fw-ciudad-cabecera.component';

describe('FwCiudadCabeceraComponent', () => {
  let component: FwCiudadCabeceraComponent;
  let fixture: ComponentFixture<FwCiudadCabeceraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwCiudadCabeceraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwCiudadCabeceraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
