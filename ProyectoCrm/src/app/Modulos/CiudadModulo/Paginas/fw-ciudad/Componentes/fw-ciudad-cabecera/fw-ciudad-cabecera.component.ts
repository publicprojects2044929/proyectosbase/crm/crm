import { Component, OnInit, Input } from '@angular/core';
import { Ciudad } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-ciudad-cabecera',
  templateUrl: './fw-ciudad-cabecera.component.html',
  styleUrls: ['./fw-ciudad-cabecera.component.scss']
})
export class FwCiudadCabeceraComponent implements OnInit {

  @Input('Ciudad') tCiudad: Ciudad = new Ciudad();

  constructor() { }

  ngOnInit(): void {
  }

}
