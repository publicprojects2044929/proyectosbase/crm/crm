import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Ciudad, Pais, Estado, Municipio } from 'src/app/Clases/Estructura';
import { PaisesService } from 'src/app/Modales/pais/fw-modal-pais/paises.service';
import { EstadosService } from 'src/app/Modales/estado/fw-modal-estado/estados.service';
import { MunicipiosService } from 'src/app/Modales/municipio/fw-modal-municipio/municipios.service';

@Component({
  selector: 'app-fw-ciudad-detalles',
  templateUrl: './fw-ciudad-detalles.component.html',
  styleUrls: ['./fw-ciudad-detalles.component.scss']
})
export class FwCiudadDetallesComponent implements OnInit, OnChanges {

  @Input('Ciudad') tCiudad: Ciudad = new Ciudad();
  @Input('Paises') tPaises: Pais[] = [];
  @Input('Nuevo') tNuevo: boolean = false;

  tPaisActual: Pais = new Pais();

  tEstados: Estado[] = [];
  tEstadoActual: Estado = new Estado();

  tMunicipios: Municipio[] = [];
  tMunicipioActual: Municipio = new Municipio();

  constructor(private tPaisService: PaisesService,
    private tEstadoService: EstadosService,
    private tMunicipioService: MunicipiosService) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {

    if ('tCiudad' in changes) {

      this.tPaisActual = this.tPaises.find(tPais => {
        return tPais.Corporacion === this.tCiudad.Corporacion &&
          tPais.Empresa === this.tCiudad.Empresa &&
          tPais.Codpais === this.tCiudad.Codpais
      }) || this.tPaises[0] || new Pais();

      this.tEstados = this.tPaisActual.LEstados || [];

      this.tEstadoActual = this.tEstados.find(tEstado => {
        return tEstado.Corporacion === this.tCiudad.Corporacion &&
          tEstado.Empresa === this.tCiudad.Empresa &&
          tEstado.Codpais === this.tCiudad.Codpais &&
          tEstado.Codestado === this.tCiudad.Codestado
      }) || this.tEstados[0] || new Estado();

      this.tMunicipios = this.tEstadoActual.LMunicipio || [];

      this.tMunicipioActual = this.tMunicipios.find(tMunicipio => {
        return tMunicipio.Corporacion === this.tCiudad.Corporacion &&
          tMunicipio.Empresa === this.tCiudad.Empresa &&
          tMunicipio.Codpais === this.tCiudad.Codpais &&
          tMunicipio.Codestado === this.tCiudad.Codestado &&
          tMunicipio.Codmunicipio === this.tCiudad.Codmunicipio
      }) || this.tMunicipios[0] || new Municipio();

      if(this.tCiudad.Codciudad === ''){

        this.Seleccionar_Estado();
        this.Seleccionar_Municipio();
        this.Seleccionar_Pais();

      }

    }

    if('tPaises' in changes){

      this.tPaisActual = this.tPaises[0] || new Pais();
      this.Seleccionar_Pais();
      this.Seleccionar_Estado();
      this.Seleccionar_Municipio();

    }

  }

  AbrirModalPais(): void {

    this.tPaisService
      .AbrirModal()
      .then(result => {

        if (result.Resultado == "S") {

          var tPaisModal: Pais = result.Pais;
          this.tPaisActual = this.tPaises.find(tPais => {

            return tPais.Corporacion === tPaisModal.Corporacion &&
              tPais.Empresa === tPaisModal.Empresa &&
              tPais.Codpais === tPaisModal.Codpais

          }) || this.tPaises[0] || new Pais();
          this.Seleccionar_Pais();

        }

      });

  }

  AbrirModalEstado(): void {

    this.tEstadoService
      .AbrirModal(this.tPaisActual)
      .then(result => {

        if (result.Resultado == "S") {

          var tEstadoModal: Estado = result.Estado;
          this.tEstadoActual = this.tEstados.find(tEstado => {

            return tEstado.Corporacion === tEstadoModal.Corporacion &&
              tEstado.Empresa === tEstadoModal.Empresa &&
              tEstado.Codpais === tEstadoModal.Codpais &&
              tEstado.Codestado === tEstadoModal.Codestado

          }) || this.tEstados[0] || new Estado();
          this.Seleccionar_Estado();

        }

      }, reason => {



      });

  }

  AbrirModalMunicipio(): void {

    this.tMunicipioService
      .AbrirModal(this.tEstadoActual)
      .then(result => {

        if (result.Resultado == "S") {

          var tMunicipioModal: Municipio = result.Municipio;
          this.tMunicipioActual = this.tMunicipios.find(tMunicipio => {

            return tMunicipio.Corporacion == tMunicipioModal.Corporacion &&
              tMunicipio.Empresa == tMunicipioModal.Empresa &&
              tMunicipio.Codpais == tMunicipioModal.Codpais &&
              tMunicipio.Codestado == tMunicipioModal.Codestado &&
              tMunicipio.Codmunicipio == tMunicipioModal.Codmunicipio

          }) || this.tMunicipios[0] || new Municipio();

          this.Seleccionar_Municipio();

        }

      }, reason => {



      });

  }

  //#endregion

  Seleccionar_Pais() {

    this.tEstados = this.tPaisActual.LEstados;
    this.tEstadoActual = new Estado();
    this.tMunicipioActual = new Municipio();

    this.tEstadoActual = this.tEstados[0] || new Estado();
    this.tMunicipios = this.tEstadoActual.LMunicipio || [];
    this.tMunicipioActual = this.tMunicipios[0] || new Municipio();

    this.Seleccionar_Estado();

  };

  Seleccionar_Estado() {

    this.tMunicipios = this.tEstadoActual.LMunicipio;
    this.tMunicipioActual = new Municipio();
    this.tMunicipioActual = this.tMunicipios[0] || new Municipio();

    this.Seleccionar_Municipio();

  };

  Seleccionar_Municipio() {

    this.tCiudad.Codpais = this.tMunicipioActual.Codpais;
    this.tCiudad.Codestado = this.tMunicipioActual.Codestado;
    this.tCiudad.Codmunicipio = this.tMunicipioActual.Codmunicipio;

  }

}
