import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwCiudadDetallesComponent } from './fw-ciudad-detalles.component';

describe('FwCiudadDetallesComponent', () => {
  let component: FwCiudadDetallesComponent;
  let fixture: ComponentFixture<FwCiudadDetallesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwCiudadDetallesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwCiudadDetallesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
