import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormularioConfiguracion, Configuracion } from 'src/app/Clases/EstructuraConfiguracion';
import { Usuario, Empresas, Ciudad, Pais, Formulario, Filtros } from 'src/app/Clases/Estructura';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/Core/Session/session.service';
import { ToastrService } from 'ngx-toastr';
import { ErroresService } from 'src/app/Modales/mensajes/fw-modal-msj/errores.service';
import { MensajesService } from 'src/app/Modales/mensajes/fw-modal-msj/mensajes.service';
import { ConfirmacionService } from 'src/app/Modales/confirmacion/fw-modal-confirmacion/confirmacion.service';
import { environment } from 'src/environments/environment';
import { CiudadEntrada } from 'src/app/Clases/EstructuraEntrada';
import { Subscription } from 'rxjs';
import { ConfiguracionService } from 'src/app/Core/Configuracion/configuracion.service';

@Component({
  selector: 'app-fw-ciudad',
  templateUrl: './fw-ciudad.page.html',
  styleUrls: ['./fw-ciudad.page.scss']
})
export class FwCiudadPage implements OnInit, OnDestroy {

  tCodFormularioActual: string = "fCiudad";
  ConfiguracionFormulario: FormularioConfiguracion = new FormularioConfiguracion();
  tPermisoAgregar: boolean = false;
  tPermisoEditar: boolean = false;
  tPermisoEliminar: boolean = false;
  tHabilitarGuardar: boolean = false;

  tUsuarioActual: Usuario = new Usuario();
  tEmpresaActual: Empresas = new Empresas();
  tEmpresa$: Subscription;
  tUsuario$: Subscription;

  tCiudades: Ciudad[] = [];
  tCiudadActual: Ciudad = new Ciudad();

  tPaises: Pais[] = [];

  tTitulo: string = "Gestión de Ciudades";
  tNuevo: boolean = true;
  tPosicion: number = 0;
  tPagina: number = 1;
  tTamanoPag: number = 5;

  tFormularios: Formulario[] = [];

  tFiltros: Filtros[] = [];
  tFiltroActual: Filtros = new Filtros();
  tFiltroBusqueda: string = "";

  tConfiguracion: Configuracion = new Configuracion();
  tConfiguracion$: Subscription;
  tHayConfiguracionWs$: Subscription;
  tStrConfiguracion: string = 'Ciudad/Ciudad.Config.json';

  constructor(public Ws: ClsServiciosService,
    private router: Router,
    private tSesion: SessionService,
    private tConfiguracionService: ConfiguracionService,
    private toastr: ToastrService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService,
    private tConfServices: ConfirmacionService) {

    this.tFiltroBusqueda = "";
    this.tPosicion = 0;

    this.tFiltros = [

      { Codfiltro: "Codciudad", Filtro: "Código" },
      { Codfiltro: "Descripcion", Filtro: "Descripción" },

    ]

  }

  ngOnInit() {

    this.tConfiguracion$ = this.tConfiguracionService
      .Consumir_Obtener_Configuracion(this.tStrConfiguracion)
      .subscribe((tConfiguracion: FormularioConfiguracion) => {

        this.ConfiguracionFormulario = tConfiguracion;
        this.tTitulo = this.ConfiguracionFormulario.Titulo;
        this.tConfServices.tTituloConf = this.ConfiguracionFormulario.ModalConfirmacion.Titulo;
        this.tErroresServices.tTituloMsj = this.ConfiguracionFormulario.ModalMensaje.Titulo;
        this.tMsjServices.tTituloMsj = this.ConfiguracionFormulario.ModalMensaje.Titulo;

      });

    this.tEmpresa$ = this.tSesion.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    })

    this.tUsuario$ = this.tSesion.tUsuarioActual$.subscribe((tUsuario: Usuario) => {

      this.tUsuarioActual = tUsuario;

    })

    var tTienePermiso: boolean = this.tSesion.ObtenerPermisoAcceso(this.tCodFormularioActual);
    this.tPermisoAgregar = this.tSesion.ObtenerPermisoAgregar();
    this.tPermisoEditar = this.tSesion.ObtenerPermisoEditar();
    this.tPermisoEliminar = this.tSesion.ObtenerPermisoEliminar();

    if (tTienePermiso == true) {

      this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
        .subscribe((tHayConfig: boolean) => {

          if (tHayConfig === true) {

            this.BuscarPaisEstadoMunicipio();
            this.Buscar_Ciudades();
            this.Nuevo();

          }

        });

    }
    else {

      this.tMsjServices.AbrirModalMsj(environment.msjSinAcceso);
      this.router.navigate(['/Home']);

    }

  }

  ngOnDestroy() {

    this.tEmpresa$.unsubscribe();
    this.tUsuario$.unsubscribe();
    this.tConfiguracion$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();

  }

  //#region "Funciones de busqueda"

  BuscarPaisEstadoMunicipio(): void {

    this.Ws
      .Paises()
      .Consumir_Obtener_PaisEstadoMunicipio(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa)
      .subscribe(Respuesta => {

        this.tPaises = [];

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {


          this.tPaises = Respuesta.Datos;
          // this.tPaisActual = this.tPaises[0] || new Pais;
          // this.tEstados = this.tPaisActual.LEstados || [];
          // this.tEstadoActual = this.tEstados[0] || new Estado;
          // this.tMunicipios = this.tEstadoActual.LMunicipio || [];
          // this.tMunicipioActual = this.tMunicipios[0] || new Municipio;

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Buscar_Ciudades(): void {

    this.Ws
      .Ciudades()
      .Consumir_Obtener_Ciudades(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          // this.tMensajeMsj = Respuesta.Mensaje;
          // this.AbrirModalMsj();

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tCiudades = Respuesta.Datos;
          this.tFiltroActual = this.tFiltros[0];

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  //#region "CRUD"

  Nuevo_Ciudad(): void {

    var tNuevoCiudad: CiudadEntrada;
    tNuevoCiudad = {

      "Id": 0,
      "Corporacion": this.tEmpresaActual.Corporacion,
      "Empresa": this.tEmpresaActual.Empresa,
      "Codpais": this.tCiudadActual.Codpais,
      "Codestado": this.tCiudadActual.Codestado,
      "Codmunicipio": this.tCiudadActual.Codmunicipio,
      "Codciudad": "",
      "Descripcion": this.tCiudadActual.Descripcion,
      "Abreviacion": this.tCiudadActual.Abreviacion,
      "Codigopostal": this.tCiudadActual.Codigopostal,
      "Usuariocreador": this.tUsuarioActual.Codusuario,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }

    this.Ws
      .Ciudades()
      .Consumir_Crear_Ciudad(tNuevoCiudad)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tFiltroActual = this.tFiltros[0];
          this.Buscar_Ciudades();
          this.Nuevo();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Modificar_Ciudad(): void {

    var tModificarCiudad: CiudadEntrada;
    tModificarCiudad = {
      "Id": 0,
      "Corporacion": this.tEmpresaActual.Corporacion,
      "Empresa": this.tEmpresaActual.Empresa,
      "Codpais": this.tCiudadActual.Codpais,
      "Codestado": this.tCiudadActual.Codestado,
      "Codmunicipio": this.tCiudadActual.Codmunicipio,
      "Codciudad": this.tCiudadActual.Codciudad,
      "Descripcion": this.tCiudadActual.Descripcion,
      "Abreviacion": this.tCiudadActual.Abreviacion,
      "Codigopostal": this.tCiudadActual.Codigopostal,
      "Usuariocreador": this.tUsuarioActual.Codusuario,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }

    this.Ws
      .Ciudades()
      .Consumir_Modificar_Ciudad(tModificarCiudad)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {


          this.tFiltroActual = this.tFiltros[0];
          this.Buscar_Ciudades();
          this.Nuevo();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Eliminar_Ciudad(): void {

    var tEliminarCiudad: CiudadEntrada;
    tEliminarCiudad = {
      "Id": 0,
      "Corporacion": this.tEmpresaActual.Corporacion,
      "Empresa": this.tEmpresaActual.Empresa,
      "Codpais": this.tCiudadActual.Codpais,
      "Codestado": this.tCiudadActual.Codestado,
      "Codmunicipio": this.tCiudadActual.Codmunicipio,
      "Codciudad": this.tCiudadActual.Codciudad,
      "Descripcion": this.tCiudadActual.Descripcion,
      "Abreviacion": this.tCiudadActual.Abreviacion,
      "Codigopostal": this.tCiudadActual.Codigopostal,
      "Usuariocreador": this.tUsuarioActual.Codusuario,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }

    this.Ws
      .Ciudades()
      .Consumir_Eliminar_Ciudad(tEliminarCiudad)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tFiltroActual = this.tFiltros[0];
          this.Buscar_Ciudades();
          this.Nuevo();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  Nuevo(): void {

    this.tHabilitarGuardar = this.tPermisoAgregar;
    this.tNuevo = true;

    this.tCiudadActual = new Ciudad();

    this.toastr.clear();
    this.toastr.info('Por favor llene los datos necesarios para registrar una nueva ciudad.');

  }

  Seleccionar_Ciudad(tCiudadActual_Sel: Ciudad, tPos: number): void {

    this.tHabilitarGuardar = this.tPermisoEditar;
    this.tNuevo = false;
    this.tPosicion = tPos;
    this.tCiudadActual = new Ciudad();
    Object.assign(this.tCiudadActual, tCiudadActual_Sel);
    // this.tCiudadActual.Id = tCiudadActual_Sel.Id;
    // this.tCiudadActual.Corporacion = tCiudadActual_Sel.Corporacion;
    // this.tCiudadActual.Empresa = tCiudadActual_Sel.Empresa;
    // this.tCiudadActual.Codciudad = tCiudadActual_Sel.Codciudad;
    // this.tCiudadActual.Codmunicipio = tCiudadActual_Sel.Codmunicipio;
    // this.tCiudadActual.Codestado = tCiudadActual_Sel.Codestado;
    // this.tCiudadActual.Codpais = tCiudadActual_Sel.Codpais;
    // this.tCiudadActual.Descripcion = tCiudadActual_Sel.Descripcion;
    // this.tCiudadActual.Abreviacion = tCiudadActual_Sel.Abreviacion;
    // this.tCiudadActual.Codigopostal = tCiudadActual_Sel.Codigopostal;
    // this.tCiudadActual.Fecha_creacion = tCiudadActual_Sel.Fecha_creacion;
    // this.tCiudadActual.Fecha_creacionl = tCiudadActual_Sel.Fecha_creacionl;
    // this.tCiudadActual.Fecha_modificacion = tCiudadActual_Sel.Fecha_modificacion;
    // this.tCiudadActual.Fecha_modificacionl = tCiudadActual_Sel.Fecha_modificacionl;
    // this.tCiudadActual.Usuariocreador = tCiudadActual_Sel.Usuariocreador;
    this.tCiudadActual.Usuariomodificador = this.tUsuarioActual.Usuariomodificador;



  }

  Seleccionar_Ciudad_Eliminar(tCiudad_Sel: Ciudad): void {

    this.tCiudadActual = new Ciudad();
    Object.assign(this.tCiudadActual, tCiudad_Sel);
    // this.tCiudadActual.Id = tCiudad_Sel.Id;
    // this.tCiudadActual.Corporacion = tCiudad_Sel.Corporacion;
    // this.tCiudadActual.Empresa = tCiudad_Sel.Empresa;
    // this.tCiudadActual.Codciudad = tCiudad_Sel.Codciudad;
    // this.tCiudadActual.Codmunicipio = tCiudad_Sel.Codmunicipio;
    // this.tCiudadActual.Codestado = tCiudad_Sel.Codestado;
    // this.tCiudadActual.Codpais = tCiudad_Sel.Codpais;
    // this.tCiudadActual.Descripcion = tCiudad_Sel.Descripcion;
    // this.tCiudadActual.Abreviacion = tCiudad_Sel.Abreviacion;
    // this.tCiudadActual.Codigopostal = tCiudad_Sel.Codigopostal;
    // this.tCiudadActual.Fecha_creacion = tCiudad_Sel.Fecha_creacion;
    // this.tCiudadActual.Fecha_creacionl = tCiudad_Sel.Fecha_creacionl;
    // this.tCiudadActual.Fecha_modificacion = tCiudad_Sel.Fecha_modificacion;
    // this.tCiudadActual.Fecha_modificacionl = tCiudad_Sel.Fecha_modificacionl;
    // this.tCiudadActual.Usuariocreador = tCiudad_Sel.Usuariocreador;
    this.tCiudadActual.Usuariomodificador = this.tUsuarioActual.Usuariomodificador;

    this.tConfServices
      .AbrirModalConf(this.ConfiguracionFormulario.ModalConfirmacion.Eliminacion)
      .then(Resultado => {

        switch (Resultado) {
          case "S": {

            this.Eliminar_Ciudad();
            break;

          }
        }

      });

  }

  Cancelar(): void {

    this.Nuevo();

  }

  ValidarFormulario(): boolean {

    let valido: boolean = false;

    if (this.tCiudadActual.Descripcion == "") {

      this.toastr.clear();
      this.toastr.warning('Debe ingresar una descripciòn!', 'Disculpe!');
      return valido;

    }
    return true
  }

  Agregar(): void {

    if (this.ValidarFormulario() == false) {

      return;

    }

    switch (this.tNuevo) {
      case true: {

        this.tConfServices
          .AbrirModalConf(this.ConfiguracionFormulario.ModalConfirmacion.Agregar)
          .then(Resultado => {

            switch (Resultado) {
              case "S": {

                this.Nuevo_Ciudad();
                break;

              }
            }

          });

        break;

      }
      default: {

        this.tConfServices
          .AbrirModalConf(this.ConfiguracionFormulario.ModalConfirmacion.Edicion)
          .then(Resultado => {

            switch (Resultado) {
              case "S": {

                this.Modificar_Ciudad();
                break;

              }
            }

          });

        break;

      }
    }

  }

}
