import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from 'src/app/Shared/shared.module';

import { CiudadRoutingModule } from './ciudad-routing.module';
import { FwCiudadPage } from './Paginas/fw-ciudad/fw-ciudad.page';
import { FwCiudadCabeceraComponent } from './Paginas/fw-ciudad/Componentes/fw-ciudad-cabecera/fw-ciudad-cabecera.component';
import { FwCiudadDetallesComponent } from './Paginas/fw-ciudad/Componentes/fw-ciudad-detalles/fw-ciudad-detalles.component';


@NgModule({
  declarations: [FwCiudadPage, FwCiudadCabeceraComponent, FwCiudadDetallesComponent],
  imports: [
    CommonModule,
    CiudadRoutingModule,
    SharedModule,
    FormsModule,
    NgbModule
  ]
})
export class CiudadModule { }
