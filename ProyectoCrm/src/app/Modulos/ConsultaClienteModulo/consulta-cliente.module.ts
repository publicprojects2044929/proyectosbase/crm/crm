import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConsultaClienteRoutingModule } from './consulta-cliente-routing.module';
import { FwConsultaClientePage } from './Paginas/fw-consulta-cliente/fw-consulta-cliente.page';
import { FwConsultaClienteListadoComponent } from './Paginas/fw-consulta-cliente/Componentes/fw-consulta-cliente-listado/fw-consulta-cliente-listado.component';
import { FwConsultaClienteDetalleComponent } from './Paginas/fw-consulta-cliente/Componentes/fw-consulta-cliente-detalle/fw-consulta-cliente-detalle.component';

import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from 'src/app/Shared/shared.module'

@NgModule({
  declarations: [FwConsultaClientePage, FwConsultaClienteListadoComponent, FwConsultaClienteDetalleComponent],
  imports: [
    CommonModule,
    ConsultaClienteRoutingModule,
    SharedModule,
    FormsModule,
    NgbModule
  ]
})
export class ConsultaClienteModule { }
