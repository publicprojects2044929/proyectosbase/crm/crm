import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/Core/Session/session.service';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { Filtros, Empresas, Usuario, Cliente, Configuracion } from 'src/app/Clases/Estructura';
import { FormularioConfiguracion } from 'src/app/Clases/EstructuraConfiguracion';
import { ErroresService } from 'src/app/Modales/mensajes/fw-modal-msj/errores.service';
import { MensajesService } from 'src/app/Modales/mensajes/fw-modal-msj/mensajes.service';
import { environment } from 'src/environments/environment';
import { Subscription } from 'rxjs';
import { ConfiguracionService } from 'src/app/Core/Configuracion/configuracion.service';

@Component({
  selector: 'app-fw-consulta-cliente',
  templateUrl: './fw-consulta-cliente.page.html',
  styleUrls: ['./fw-consulta-cliente.page.scss']
})
export class FwConsultaClientePage implements OnInit, OnDestroy {

  @ViewChild('flArchivo', { static: true }) tControlArchivo: any;

  ConfiguracionFormulario: FormularioConfiguracion = new FormularioConfiguracion();
  tCodFormularioActual: string = "fConsultaCliente";

  tUsuarioActual: Usuario = new Usuario;
  tUsuario$: Subscription;
  tEmpresaActual: Empresas = new Empresas;
  tEmpresa$: Subscription;

  tTitulo: string = "";
  tPosicion: number = 0;
  tPagina: number = 1;
  tTamanoPag: number = 10;

  tClientes: Cliente[] = [];
  tClienteActual: Cliente = new Cliente();

  tConfiguracion: Configuracion = new Configuracion();

  tConfiguracion$: Subscription;
  tHayConfiguracionWs$: Subscription;
  tStrConfiguracion: string = 'Cliente/Cliente.Config.json';

  tFiltroActual: Filtros[] = [];
  tFiltroBusqueda: string = "";

  constructor(public router: Router,
    public Ws: ClsServiciosService,
    private tSesion: SessionService,
    private tConfiguracionService: ConfiguracionService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService) {

    this.tPosicion = 0;

    this.tFiltroActual = [

      { Codfiltro: "Codcliente", Filtro: "Cliente" },
      { Codfiltro: "Descripcion", Filtro: "Nombre" }

    ]

  }

  ngOnInit() {

    this.tConfiguracion$ = this.tConfiguracionService
      .Consumir_Obtener_Configuracion(this.tStrConfiguracion)
      .subscribe((tConfiguracion: FormularioConfiguracion) => {

        this.ConfiguracionFormulario = tConfiguracion;
        this.tTitulo = this.ConfiguracionFormulario.Titulo;
        this.tErroresServices.tTituloMsj = this.ConfiguracionFormulario.ModalMensaje.Titulo;
        this.tMsjServices.tTituloMsj = this.ConfiguracionFormulario.ModalMensaje.Titulo;

      });

    this.tEmpresa$ = this.tSesion.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    });

    this.tUsuario$ = this.tSesion.tUsuarioActual$.subscribe((tUsuario: Usuario) => {

      this.tUsuarioActual = tUsuario;

    });

    var tTienePermiso: boolean = this.tSesion.ObtenerPermisoAcceso(this.tCodFormularioActual);

    if (tTienePermiso == true) {

      this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
        .subscribe((tHayConfig: boolean) => {

          if (tHayConfig === true) {

            this.BuscarClientes();

          }

        });

    }
    else {

      this.tMsjServices.AbrirModalMsj(environment.msjSinAcceso);
      this.router.navigate(['/Home']);

    }

  }

  ngOnDestroy() {

    this.tUsuario$.unsubscribe();
    this.tEmpresa$.unsubscribe();
    this.tConfiguracion$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();

  }


  //#region "Funciones de busqueda"

  BuscarClientes(): void {

    this.Ws.Cliente()
      .Consumir_Obtener_Clientes(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.tClientes = Respuesta.Datos;

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  BuscarCliente(tCodcliente: string): void {

    this.Ws.Cliente()
      .Consumir_Obtener_ClientesDetallado(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        tCodcliente)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.tClienteActual = Respuesta.Datos[0];

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  SeleccionarCliente(tClienteSel: Cliente) {

    this.BuscarCliente(tClienteSel.Codcliente);

  }

}

