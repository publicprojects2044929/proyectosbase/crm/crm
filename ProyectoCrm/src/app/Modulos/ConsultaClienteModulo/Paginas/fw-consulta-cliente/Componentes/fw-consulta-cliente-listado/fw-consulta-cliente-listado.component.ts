import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Cliente } from 'src/app/Clases/Estructura';
import { ManejarImagenes } from 'src/app/Manejadores/cls-procedimientos';

@Component({
  selector: 'app-fw-consulta-cliente-listado',
  templateUrl: './fw-consulta-cliente-listado.component.html',
  styleUrls: ['./fw-consulta-cliente-listado.component.scss']
})
export class FwConsultaClienteListadoComponent implements OnInit {

  @Input('Cliente') tCliente: Cliente = new Cliente();
  @Input('Actual') tActual: boolean = false;
  @Output('seleccionarCliente') tClienteSel: EventEmitter<Cliente> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {

  }

  SeleccionarCliente(){
    this.tClienteSel.emit(this.tCliente);
  }

}
