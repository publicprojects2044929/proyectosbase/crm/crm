import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwConsultaClienteListadoComponent } from './fw-consulta-cliente-listado.component';

describe('FwConsultaClienteListadoComponent', () => {
  let component: FwConsultaClienteListadoComponent;
  let fixture: ComponentFixture<FwConsultaClienteListadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwConsultaClienteListadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwConsultaClienteListadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
