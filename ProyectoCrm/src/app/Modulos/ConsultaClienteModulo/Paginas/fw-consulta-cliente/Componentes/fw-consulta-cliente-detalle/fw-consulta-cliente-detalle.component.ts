import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { Cliente } from 'src/app/Clases/Estructura';
import { Marcador } from 'src/app/Clases/EstructuraMapa';

@Component({
  selector: 'app-fw-consulta-cliente-detalle',
  templateUrl: './fw-consulta-cliente-detalle.component.html',
  styleUrls: ['./fw-consulta-cliente-detalle.component.scss']
})
export class FwConsultaClienteDetalleComponent implements OnInit, OnChanges {

  @Input('Cliente') tCliente: Cliente = new Cliente();
  tLMarcadores: Marcador[] = [];

  tPosicionTelefonos: number = 0;
  tPaginaTelefonos: number = 1;
  tTamanoPagTelefonos: number = 10;

  tPosicionDoc: number = 0;
  tPaginaDoc: number = 1;
  tTamanoPagDoc: number = 10;

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(){

    this.tLMarcadores = [];

    let tMarcador: Marcador = new Marcador();
    tMarcador.Latitud = this.tCliente.Latitud;
    tMarcador.Longitud = this.tCliente.Longitud;
    this.tLMarcadores = [...this.tLMarcadores, tMarcador];

    tMarcador = new Marcador();
    tMarcador.Latitud = this.tCliente.Latitud_envio;
    tMarcador.Longitud = this.tCliente.Longitud_envio;
    this.tLMarcadores = [...this.tLMarcadores, tMarcador];

    tMarcador = new Marcador();
    tMarcador.Latitud = this.tCliente.Latitud_facturacion;
    tMarcador.Longitud = this.tCliente.Longitud_facturacion;
    this.tLMarcadores = [...this.tLMarcadores, tMarcador];

  }

}
