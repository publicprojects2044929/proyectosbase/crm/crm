import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwConsultaClienteDetalleComponent } from './fw-consulta-cliente-detalle.component';

describe('FwConsultaClienteDetalleComponent', () => {
  let component: FwConsultaClienteDetalleComponent;
  let fixture: ComponentFixture<FwConsultaClienteDetalleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwConsultaClienteDetalleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwConsultaClienteDetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
