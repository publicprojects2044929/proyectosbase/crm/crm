import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwConsultaClientePage } from './fw-consulta-cliente.component';

describe('FwConsultaClientePage', () => {
  let component: FwConsultaClientePage;
  let fixture: ComponentFixture<FwConsultaClientePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwConsultaClientePage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwConsultaClientePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
