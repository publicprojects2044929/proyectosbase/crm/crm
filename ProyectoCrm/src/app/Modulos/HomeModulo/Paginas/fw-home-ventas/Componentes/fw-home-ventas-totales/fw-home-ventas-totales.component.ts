import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Empresas } from 'src/app/Clases/Estructura';
import { Barra } from 'src/app/Clases/EstructuraGrafico';
import { BarrasService } from 'src/app/Core/Graficos/Barras/barras.service';

@Component({
  selector: 'app-fw-home-ventas-totales',
  templateUrl: './fw-home-ventas-totales.component.html',
  styleUrls: ['./fw-home-ventas-totales.component.scss']
})
export class FwHomeVentasTotalesComponent implements OnInit, OnChanges {

  @Input('Total') tTotalVenta: number = 0;
  @Input('Cantidad') tCantidadVenta: number = 0;
  @Input('Empresa') tEmpresaActual: Empresas = new Empresas();
  @Input('DatosBarra') tDatosBarra: Barra = new Barra();

  tElementoTipo: any;

  constructor(private tBarrasService: BarrasService) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges){

    if ('tDatosBarra' in changes) {

      this.MostrarGraficoTipo();

    }

  }

  MostrarGraficoTipo(): void {

    this.tElementoTipo = document.getElementById("canvasTipoBarra");
    this.tBarrasService.ArmarGrafico(this.tElementoTipo, this.tDatosBarra);

  }

}
