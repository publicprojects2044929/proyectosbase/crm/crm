import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Venta, Empresas } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-home-ventas-lista',
  templateUrl: './fw-home-ventas-lista.component.html',
  styleUrls: ['./fw-home-ventas-lista.component.scss']
})
export class FwHomeVentasListaComponent implements OnInit {

  @Input('Venta') tVenta: Venta = new Venta();
  @Input('Empresa') tEmpresaActual: Empresas = new Empresas();
  @Output('DescargarReporte') tDescargar: EventEmitter<Venta> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  Descargar_Reportes_Venta(tVenta: Venta){

    this.tDescargar.emit(tVenta);

  }

}
