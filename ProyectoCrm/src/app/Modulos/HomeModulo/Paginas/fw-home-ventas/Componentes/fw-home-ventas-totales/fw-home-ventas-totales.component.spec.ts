import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwHomeVentasTotalesComponent } from './fw-home-ventas-totales.component';

describe('FwHomeVentasTotalesComponent', () => {
  let component: FwHomeVentasTotalesComponent;
  let fixture: ComponentFixture<FwHomeVentasTotalesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwHomeVentasTotalesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwHomeVentasTotalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
