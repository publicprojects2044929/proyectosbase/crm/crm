import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwHomeVentasListaComponent } from './fw-home-ventas-lista.component';

describe('FwHomeVentasListaComponent', () => {
  let component: FwHomeVentasListaComponent;
  let fixture: ComponentFixture<FwHomeVentasListaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwHomeVentasListaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwHomeVentasListaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
