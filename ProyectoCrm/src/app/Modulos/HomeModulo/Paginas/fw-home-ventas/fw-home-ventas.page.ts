import { Component, OnInit, OnDestroy } from '@angular/core';
import { Usuario, Empresas, Venta, Respuesta, Reportes, Tipo } from 'src/app/Clases/Estructura';
import { Subscription } from 'rxjs';
import { ManejarPdf } from 'src/app/Manejadores/cls-procedimientos';
import { SessionService } from 'src/app/Core/Session/session.service';
import { SocketService } from 'src/app/Core/Socket/socket.service';
import { ErroresService } from 'src/app/Modales/mensajes/fw-modal-msj/errores.service';
import { MensajesService } from 'src/app/Modales/mensajes/fw-modal-msj/mensajes.service';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';
import { Barra, DataSetBarra } from 'src/app/Clases/EstructuraGrafico';

@Component({
  selector: 'app-fw-home-ventas',
  templateUrl: './fw-home-ventas.page.html',
  styleUrls: ['./fw-home-ventas.page.scss']
})
export class FwHomeVentasPage implements OnInit, OnDestroy {

  tManejarPdf: ManejarPdf = new ManejarPdf();

  tUsuarioActual: Usuario = new Usuario();
  tEmpresaActual: Empresas = new Empresas();
  tEmpresa$: Subscription;
  tUsuario$: Subscription;

  tSocketVenta$: Subscription;
  tHayConfiguracionSocket$: Subscription;
  tHayConfiguracionWs$: Subscription;

  tVentas: Venta[] = [];
  tTotalVenta: number = 0;
  tCantidadVenta: number = 0;

  tPosicionVentas: number = 0;
  tPaginaVentas: number = 1;
  tTamanoPagVentas: number = 5;

  tTipos: Tipo[] = [];
  tDatosBarrasTipo: Barra = new Barra();

  constructor(private tSocket: SocketService,
    private tSesion: SessionService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService,
    public Ws: ClsServiciosService,
    private toastr: ToastrService) { }

  ngOnInit(): void {

    this.tEmpresa$ = this.tSesion.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    });

    this.tUsuario$ = this.tSesion.tUsuarioActual$.subscribe((tUsuario: Usuario) => {

      this.tUsuarioActual = tUsuario;

    });

    this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
      .subscribe((tHayConfig: boolean) => {

        if (tHayConfig === true) {

          this.Buscar_Tipo();

        }

      });

    this.tHayConfiguracionSocket$ = this.tSocket.tHayConfig$
      .subscribe((tHayConfig: boolean) => {

        if (tHayConfig === true) {

          this.tSocket.emit("Ventas:VentasDelDia");

          this.tSocketVenta$ = this.tSocket
            .listen("Ventas:VentasDelDia")
            .subscribe((tRespuesta: Respuesta) => {

              this.tVentas = tRespuesta.Datos || [];

              this.tTotalVenta = 0;
              this.tCantidadVenta = 0;

              this.tVentas.forEach(tVenta => {

                this.tTotalVenta += tVenta.Total;
                this.tCantidadVenta++;

              });

              this.ArmarGraficoTipo();

            });

        }

      });

  }

  ngOnDestroy() {

    this.tEmpresa$.unsubscribe();
    this.tUsuario$.unsubscribe();
    this.tSocketVenta$.unsubscribe();
    this.tHayConfiguracionSocket$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();
    this.tSocket.disconnect();

  }

  //#region "Funciones de busqueda"

  Buscar_Tipo(): void {

    this.Ws
      .Tipo()
      .Consumir_Obtener_Tipo(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        "TR_VENTA")
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tTipos = Respuesta.Datos || [];
          this.ArmarGraficoTipo();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  Descargar_Reportes_Venta(tVentaSel: Venta): void {

    this.Ws
      .Venta()
      .Consumir_Obtener_VentaRpt(tVentaSel.Corporacion,
        tVentaSel.Empresa,
        tVentaSel.Codventa)
      .subscribe(Respuesta => {


        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          var Reporte: Reportes = Respuesta.Datos[0];
          this.tManejarPdf.AbrirPdf(Reporte);

        }

      }, error => {


        this.tErroresServices.MostrarError(error);

      });

  }

  ArmarGraficoTipo(): void {

    this.tDatosBarrasTipo = new Barra();

    this.tTipos.forEach(tTipo => {

      let tDataSetTipo: DataSetBarra = new DataSetBarra();

      let tContadorTipo: number = 0;
      let tAcumuladorTipo: number = 0;

      tDataSetTipo.Color = tTipo.Hexadecimal; //'#' + (Math.random() * 0xFFFFFF << 0).toString(16);
      tDataSetTipo.Etiqueta = tTipo.Codtipo;

      this.tVentas.forEach(tVenta => {

        if (tVenta.Tipo === tTipo.Codtipo) {

          tContadorTipo++;
          tAcumuladorTipo += tVenta.Total;

        }

      });

      tDataSetTipo.Datos = tAcumuladorTipo.toString();

      this.tDatosBarrasTipo.Etiquetas.push(tTipo.Codtipo);
      this.tDatosBarrasTipo.DataSet.push(tDataSetTipo);

    });


  }

}
