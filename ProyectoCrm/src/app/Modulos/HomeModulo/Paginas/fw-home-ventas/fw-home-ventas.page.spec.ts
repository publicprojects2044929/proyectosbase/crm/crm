import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwHomeVentasPage } from './fw-home-ventas.page';

describe('FwHomeVentasPage', () => {
  let component: FwHomeVentasPage;
  let fixture: ComponentFixture<FwHomeVentasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwHomeVentasPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwHomeVentasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
