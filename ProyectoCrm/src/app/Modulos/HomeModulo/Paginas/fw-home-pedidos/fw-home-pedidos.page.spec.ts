import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwHomePedidosPage } from './fw-home-pedidos.page';

describe('FwHomePedidosPage', () => {
  let component: FwHomePedidosPage;
  let fixture: ComponentFixture<FwHomePedidosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwHomePedidosPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwHomePedidosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
