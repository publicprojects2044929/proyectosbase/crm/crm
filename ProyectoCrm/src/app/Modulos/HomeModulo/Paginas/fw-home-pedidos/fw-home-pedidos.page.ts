import { Component, OnInit, OnDestroy } from '@angular/core';
import { Usuario, Empresas, Pedido, Respuesta, Reportes, Tipo } from 'src/app/Clases/Estructura';
import { Subscription } from 'rxjs';
import { ManejarPdf } from 'src/app/Manejadores/cls-procedimientos';
import { SessionService } from 'src/app/Core/Session/session.service';
import { SocketService } from 'src/app/Core/Socket/socket.service';
import { ErroresService } from 'src/app/Modales/mensajes/fw-modal-msj/errores.service';
import { MensajesService } from 'src/app/Modales/mensajes/fw-modal-msj/mensajes.service';
import { ConfirmacionService } from 'src/app/Modales/confirmacion/fw-modal-confirmacion/confirmacion.service';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';
import { PedidoEntrada } from 'src/app/Clases/EstructuraEntrada';
import { Barra, DataSetBarra } from 'src/app/Clases/EstructuraGrafico';

@Component({
  selector: 'app-fw-home-pedidos',
  templateUrl: './fw-home-pedidos.page.html',
  styleUrls: ['./fw-home-pedidos.page.scss']
})
export class FwHomePedidosPage implements OnInit, OnDestroy {

  tManejarPdf: ManejarPdf = new ManejarPdf();

  tUsuarioActual: Usuario = new Usuario();
  tEmpresaActual: Empresas = new Empresas();
  tEmpresa$: Subscription;
  tUsuario$: Subscription;

  tSocketPedido$: Subscription;
  tHayConfiguracionSocket$: Subscription;
  tHayConfiguracionWs$: Subscription;

  tTipos: Tipo[] = [];

  tPedidos: Pedido[] = [];

  tPosicionOrdenes: number = 0;
  tPaginaOrdenes: number = 1;
  tTamanoPagOrdenes: number = 5;

  tCantidadPedidos: number = 0;
  tTotalPedidos: number = 0;
  tDatosBarrasTipo: Barra = new Barra();

  constructor(private tSocket: SocketService,
    private tSesion: SessionService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService,
    private tConfServices: ConfirmacionService,
    public Ws: ClsServiciosService,
    private toastr: ToastrService) { }

  ngOnInit(): void {

    this.tEmpresa$ = this.tSesion.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    });

    this.tUsuario$ = this.tSesion.tUsuarioActual$.subscribe((tUsuario: Usuario) => {

      this.tUsuarioActual = tUsuario;

    });

    this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
      .subscribe((tHayConfig: boolean) => {

        if (tHayConfig === true) {

          this.Buscar_Tipo();

        }

      });

    this.tHayConfiguracionSocket$ = this.tSocket.tHayConfig$
      .subscribe((tHayConfig: boolean) => {

        if (tHayConfig === true) {

          this.tSocket.emit("Pedidos:PedidosActivos");

          this.tSocketPedido$ = this.tSocket
            .listen("Pedidos:PedidosActivos")
            .subscribe((tRespuesta: Respuesta) => {

              this.tPedidos = tRespuesta.Datos || [];
              this.tCantidadPedidos = this.tPedidos.length;
              this.tTotalPedidos = 0;
              this.tPedidos.forEach(tPedido => { this.tTotalPedidos = this.tTotalPedidos + tPedido.Total });
              this.ArmarGraficoTipo();

            });

        }

      });

  }

  ngOnDestroy() {

    this.tEmpresa$.unsubscribe();
    this.tUsuario$.unsubscribe();
    this.tSocketPedido$.unsubscribe();
    this.tHayConfiguracionSocket$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();
    this.tSocket.disconnect();

  }

  //#region "Funciones de busqueda"

  Buscar_Tipo(): void {

    this.Ws
      .Tipo()
      .Consumir_Obtener_Tipo(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        "TR_PEDIDO")
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tTipos = Respuesta.Datos || [];
          this.ArmarGraficoTipo();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  //#region  "CRUD"

  Aprobar_Pedido(tPedido: Pedido) {

    let tPedidoAprobar: PedidoEntrada = {

      "Id": 0,
      "Corporacion": tPedido.Corporacion,
      "Empresa": tPedido.Empresa,
      "Codpedido": tPedido.Codpedido,
      "Codcliente": tPedido.Codcliente,
      "Tipo": tPedido.Tipo,
      "Estatus": tPedido.Estatus,
      "Usuariocreador": tPedido.Usuariocreador,
      "Usuariomodificador": this.tUsuarioActual.Codusuario,
      "LArticulos": [],
      "LCupones": [],
      "LColecciones": []

    }

    this.Ws
      .Pedidos()
      .Consumir_Aprobar_Pedido(tPedidoAprobar)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.toastr.success("El pedido se aprobo correctamente.", "Pedido");

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Cerrar_Pedido(tPedido: Pedido) {

    let tPedidoAprobar: PedidoEntrada = {

      "Id": 0,
      "Corporacion": tPedido.Corporacion,
      "Empresa": tPedido.Empresa,
      "Codpedido": tPedido.Codpedido,
      "Codcliente": tPedido.Codcliente,
      "Tipo": tPedido.Tipo,
      "Estatus": tPedido.Estatus,
      "Usuariocreador": tPedido.Usuariocreador,
      "Usuariomodificador": this.tUsuarioActual.Codusuario,
      "LArticulos": [],
      "LCupones": [],
      "LColecciones": []

    }

    this.Ws
      .Pedidos()
      .Consumir_Cerrar_Pedido(tPedidoAprobar)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.toastr.success("El pedido se cerro correctamente.", "Pedido");

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  ArmarGraficoTipo(): void {

    this.tDatosBarrasTipo = new Barra();

    this.tTipos.forEach(tTipo => {

      let tDataSetTipo: DataSetBarra = new DataSetBarra();

      let tContadorTipo: number = 0;
      let tAcumuladorTipo: number = 0;

      tDataSetTipo.Color = tTipo.Hexadecimal; //'#' + (Math.random() * 0xFFFFFF << 0).toString(16);
      tDataSetTipo.Etiqueta = tTipo.Codtipo;

      this.tPedidos.forEach(tPedido => {

        if (tPedido.Tipo === tTipo.Codtipo) {

          tContadorTipo++;
          tAcumuladorTipo += tPedido.Total;

        }

      });

      tDataSetTipo.Datos = tAcumuladorTipo.toString();

      this.tDatosBarrasTipo.Etiquetas.push(tTipo.Codtipo);
      this.tDatosBarrasTipo.DataSet.push(tDataSetTipo);

    });


  }

  Aprobar(tPedidoSel: Pedido): void {

    // let tMsjConf: string = "Confimar aprobar la [ " + tPedidoSel.Tipo + " ] Nro. " + tPedidoSel.Codpedido;

    let tMsjConf: string = `Confirma aprobar la ${ tPedidoSel.Tipo } Nro. ${ tPedidoSel.Codpedido }`;

    this.tConfServices
      .AbrirModalConf(tMsjConf)
      .then(Resultado => {

        switch (Resultado) {
          case "S": {

            this.Aprobar_Pedido(tPedidoSel);
            break;

          }
        }

      });

  }

  Cerrar(tPedidoSel: Pedido): void {

    // let tMsjConf: string = "Confimar cerrar la [ " + tPedidoSel.Tipo + " ] Nro. " + tPedidoSel.Codpedido;

    let tMsjConf: string = `Confimar cerrar la ${ tPedidoSel.Tipo } Nro. ${ tPedidoSel.Codpedido }`;

    this.tConfServices
      .AbrirModalConf(tMsjConf)
      .then(Resultado => {

        switch (Resultado) {
          case "S": {

            this.Cerrar_Pedido(tPedidoSel);
            break;

          }
        }

      });

  }

  Descargar_Reportes_Pedido(tPedidoSel: Pedido): void {

    this.Ws
      .Pedidos()
      .Consumir_Obtener_PedidoRpt(tPedidoSel.Corporacion,
        tPedidoSel.Empresa,
        tPedidoSel.Codpedido)
      .subscribe(Respuesta => {


        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          var Reporte: Reportes = Respuesta.Datos[0];
          this.tManejarPdf.AbrirPdf(Reporte);

        }

      }, error => {


        this.tErroresServices.MostrarError(error);

      });

  }

}
