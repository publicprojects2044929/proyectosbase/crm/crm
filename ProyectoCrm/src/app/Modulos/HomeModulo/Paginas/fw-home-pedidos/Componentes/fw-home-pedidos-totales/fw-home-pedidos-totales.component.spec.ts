import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwHomePedidosTotalesComponent } from './fw-home-pedidos-totales.component';

describe('FwHomePedidosTotalesComponent', () => {
  let component: FwHomePedidosTotalesComponent;
  let fixture: ComponentFixture<FwHomePedidosTotalesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwHomePedidosTotalesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwHomePedidosTotalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
