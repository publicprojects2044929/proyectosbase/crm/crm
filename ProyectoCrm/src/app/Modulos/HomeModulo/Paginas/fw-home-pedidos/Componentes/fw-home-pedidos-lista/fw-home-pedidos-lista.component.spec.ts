import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwHomePedidosListaComponent } from './fw-home-pedidos-lista.component';

describe('FwHomePedidosListaComponent', () => {
  let component: FwHomePedidosListaComponent;
  let fixture: ComponentFixture<FwHomePedidosListaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwHomePedidosListaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwHomePedidosListaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
