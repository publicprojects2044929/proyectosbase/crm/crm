import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Empresas } from 'src/app/Clases/Estructura';
import { BarrasService } from 'src/app/Core/Graficos/Barras/barras.service';
import { Barra } from 'src/app/Clases/EstructuraGrafico';

@Component({
  selector: 'app-fw-home-pedidos-totales',
  templateUrl: './fw-home-pedidos-totales.component.html',
  styleUrls: ['./fw-home-pedidos-totales.component.scss']
})
export class FwHomePedidosTotalesComponent implements OnInit, OnChanges {

  @Input('Cantidad') tCantidad: number = 0;
  @Input('Total') tTotal: number = 0;
  @Input('DatosBarra') tDatosBarra: Barra = new Barra();
  @Input('Empresa') tEmpresaActual: Empresas = new Empresas();

  tElementoTipo: any;

  constructor(private tBarrasService: BarrasService) { }

  ngOnInit(): void {

  }

  ngOnChanges(changes: SimpleChanges) {

    if ('tDatosBarra' in changes) {

      this.MostrarGraficoTipo();

    }

  }

  MostrarGraficoTipo(): void {

    this.tElementoTipo = document.getElementById("canvasTipoBarra");
    this.tBarrasService.ArmarGrafico(this.tElementoTipo, this.tDatosBarra);

  }

}
