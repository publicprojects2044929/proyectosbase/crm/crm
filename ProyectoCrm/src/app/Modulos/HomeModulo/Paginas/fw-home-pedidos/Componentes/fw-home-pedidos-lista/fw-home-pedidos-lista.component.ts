import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Pedido, Empresas } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-home-pedidos-lista',
  templateUrl: './fw-home-pedidos-lista.component.html',
  styleUrls: ['./fw-home-pedidos-lista.component.scss']
})
export class FwHomePedidosListaComponent implements OnInit {

  @Input('Pedido') tPedido: Pedido = new Pedido();
  @Input('Empresa') tEmpresaActual: Empresas = new Empresas();
  @Output('Aprobar') tAprobar: EventEmitter<Pedido> = new EventEmitter();
  @Output('Cerrar') tCerrar: EventEmitter<Pedido> = new EventEmitter();
  @Output('Descargar') tDescargar: EventEmitter<Pedido> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  Aprobar(): void {

    this.tAprobar.emit(this.tPedido);

  }

  Cerrar(): void {

    this.tCerrar.emit(this.tPedido);

  }

  Descargar_Reportes_Pedido(): void {

    this.tDescargar.emit(this.tPedido);

  }

}
