import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwHomeTicketsPage } from './fw-home-tickets.page';

describe('FwHomeTicketsPage', () => {
  let component: FwHomeTicketsPage;
  let fixture: ComponentFixture<FwHomeTicketsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwHomeTicketsPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwHomeTicketsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
