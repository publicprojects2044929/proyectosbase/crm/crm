import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Usuario, Empresas, Respuesta, Ticket, Tipo, Estatus } from 'src/app/Clases/Estructura';
import { Subscription } from 'rxjs';
import { SocketService } from 'src/app/Core/Socket/socket.service';
import { SessionService } from 'src/app/Core/Session/session.service';
import { ErroresService } from 'src/app/Modales/mensajes/fw-modal-msj/errores.service';
import { MensajesService } from 'src/app/Modales/mensajes/fw-modal-msj/mensajes.service';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { TicketEntrada } from 'src/app/Clases/EstructuraEntrada';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-fw-home-tickets',
  templateUrl: './fw-home-tickets.page.html',
  styleUrls: ['./fw-home-tickets.page.scss']
})
export class FwHomeTicketsPage implements OnInit, OnDestroy {

  @ViewChild('NuevoTicket', { static: true }) tNuevoTicket: any;

  tUsuarioActual: Usuario = new Usuario();
  tEmpresaActual: Empresas = new Empresas();
  tEmpresa$: Subscription;
  tUsuario$: Subscription;

  tSocketTickets$: Subscription;
  tSocketTotales$: Subscription;
  tHayConfiguracionSocket$: Subscription;
  tHayConfiguracionWs$: Subscription;

  tTipos: Tipo[] = [];
  tEstatus: Estatus[] = [];
  LTickets: Ticket[] = [];

  constructor(private tSocket: SocketService,
    private tSesion: SessionService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService,
    public Ws: ClsServiciosService) { }

  ngOnInit(): void {

    this.tEmpresa$ = this.tSesion.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    });

    this.tUsuario$ = this.tSesion.tUsuarioActual$.subscribe((tUsuario: Usuario) => {

      this.tUsuarioActual = tUsuario;

    });

    this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
      .subscribe((tHayConfig: boolean) => {

        if (tHayConfig === true) {

          this.Buscar_Tipo();
          this.Buscar_Estatus();

        }

      });

    this.tHayConfiguracionSocket$ = this.tSocket.tHayConfig$
      .subscribe((tHayConfig: boolean) => {

        if (tHayConfig === true) {

          this.tSocket.emit("Ticket:TicketsActivos");

          this.tSocketTickets$ = this.tSocket
            .listen("Ticket:TicketsActivos")
            .subscribe((tRespuesta: Respuesta) => this.TicketsListen(tRespuesta));

        }

      });

  }

  ngOnDestroy(): void {

    this.tEmpresa$.unsubscribe();
    this.tUsuario$.unsubscribe();
    this.tSocketTickets$.unsubscribe();
    this.tHayConfiguracionSocket$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();

    if (this.tSocketTotales$) {
      this.tSocketTotales$.unsubscribe();
    }

    this.tSocket.disconnect();

  }

  //#region "Funciones de busqueda"

  Buscar_Tipo(): void {

    this.Ws
      .Tipo()
      .Consumir_Obtener_Tipo(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        "MR_TICKET")
      .subscribe(Respuesta => {

        if (Respuesta.Resultado === "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado === "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tTipos = Respuesta.Datos || [];

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Buscar_Estatus(): void {

    this.Ws
      .Estatus()
      .Consumir_Obtener_Estatus(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        "MR_TICKET")
      .subscribe(Respuesta => {

        if (Respuesta.Resultado === "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado === "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tEstatus = Respuesta.Datos || [];

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  CrearTicket(tTicket: TicketEntrada) {

    this.Ws
      .Ticket()
      .Consumir_Crear_Ticket(tTicket)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tNuevoTicket.Cancelar();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  TicketsListen(tRespuesta: Respuesta) {

    this.LTickets = tRespuesta.Datos || [];
    let tTickets: string[] = this.LTickets.map(tTicket => tTicket.Codticket);

    if (this.tSocketTotales$) {
      this.tSocketTotales$.unsubscribe();
    }

    this.tSocket.emit("Mensajes:Totales", tTickets);

    this.tSocketTotales$ = this.tSocket
      .listen("Mensajes:Totales")
      .subscribe((LMensajes: any) => {
        this.LTickets.map((tTicket) => {
          tTicket.TotalMensajes = LMensajes.find(
            (tTictketTotal) =>
              tTictketTotal.Corporacion === tTicket.Corporacion &&
              tTictketTotal.Empresa === tTicket.Empresa &&
              tTictketTotal.Codticket === tTicket.Codticket
          )?.TotalMensajes || 0;
        });
      })
  }
}
