import { Component, OnInit, Input } from '@angular/core';
import { Ticket, Tipo, Estatus } from 'src/app/Clases/Estructura';
import { TicketMensajesService } from 'src/app/Modales/ticket-mensajes/fw-ticket-mensajes/ticket-mensajes.service';

@Component({
  selector: 'app-fw-home-tickets-listado',
  templateUrl: './fw-home-tickets-listado.component.html',
  styleUrls: ['./fw-home-tickets-listado.component.scss']
})
export class FwHomeTicketsListadoComponent implements OnInit {

  @Input("Tickets") LTickets: Ticket[] = [];
  @Input("Tipos") tTipos: Tipo[] = [];
  @Input("Estatus") tEstatus: Estatus[] = [];

  tEstatusSel: string[] = [];
  tTiposSel: string[] = [];

  tPosicionTicket: number = 0;
  tPaginaTicket: number = 1;
  tTamanoPagTicket: number = 5;

  constructor(private tTicketMsjService: TicketMensajesService) { }

  ngOnInit(): void { }

  Seleccionar_Estatus(tEstSel: string) {

    let tPos: number = this.tEstatusSel.findIndex(tEst => tEst === tEstSel);

    if (tPos > -1) {

      this.tEstatusSel = this.tEstatusSel.filter(tEst => tEst !== tEstSel)

    }
    else {

      this.tEstatusSel = [
        ...this.tEstatusSel,
        tEstSel
      ];

    }

  }

  Seleccionar_Tipo(tTipoSel: string) {

    let tPos: number = this.tTiposSel.findIndex(tTipo => tTipo === tTipoSel);

    if (tPos > -1) {

      this.tTiposSel = this.tTiposSel.filter(tTipo => tTipo !== tTipoSel);

    }
    else {

      this.tTiposSel = [
        ...this.tTiposSel,
        tTipoSel
      ];

    }

  }

  Abrir_Ticket(tTicketSel: Ticket) {

    this.tTicketMsjService
      .AbrirModal(tTicketSel)
      .then(result => {

        if (result.Resultado == "S") {

        }

      }, reason => {

      });

  }

}
