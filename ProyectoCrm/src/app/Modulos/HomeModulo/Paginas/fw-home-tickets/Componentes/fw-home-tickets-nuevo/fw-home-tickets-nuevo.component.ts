import { Component, OnInit, OnChanges, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { ClientesService } from 'src/app/Modales/cliente/fw-modal-clientes/clientes.service';
import { Cliente, Empresas, Usuario, Tipo } from 'src/app/Clases/Estructura';
import { TicketEntrada, TicketUsuarioEntradas } from 'src/app/Clases/EstructuraEntrada';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-fw-home-tickets-nuevo',
  templateUrl: './fw-home-tickets-nuevo.component.html',
  styleUrls: ['./fw-home-tickets-nuevo.component.scss']
})
export class FwHomeTicketsNuevoComponent implements OnInit, OnChanges {

  @Input("Usuario") tUsuario: Usuario = new Usuario();
  @Input("Empresa") tEmpresa: Empresas = new Empresas();
  @Input("Tipos") tTipos: Tipo[] = [];
  @Output("Crear") tCrear: EventEmitter<TicketEntrada> = new EventEmitter();

  tClienteActual: Cliente = new Cliente();
  tTitulo: string = '';
  tTipoActual: string = '';

  tNuevoTicket: TicketEntrada = new TicketEntrada();

  constructor(private tClienteService: ClientesService,
              private toastr: ToastrService) { }

  ngOnInit(): void {

    this.Cancelar();

  }

  ngOnChanges(changes: SimpleChanges): void{

    if('tTipos' in changes){
      this.Cancelar();
    }

  }

  AbrirModalClientes(): void {

    this.tClienteService
      .AbrirModal()
      .then(result => {

        if (result.Resultado === "S") {

          this.tClienteActual = result.Cliente;

        }

      }, reason => {



      });

  }

  Cancelar(){

    this.tClienteActual = new Cliente();
    this.tTitulo = "";
    this.tTipoActual = this.tTipos[0]?.Codtipo || '';

  }

  Crear(){

    if(this.tTitulo === ''){

      this.toastr.clear();
      this.toastr.warning('Debe indicar un titulo para el ticket.');
      return;

    }

    if(this.tClienteActual.Codcliente === ''){

      this.toastr.clear();
      this.toastr.warning('Debe seleccionar un cliente para asociar al ticket.');
      return;

    }

    let tNuevoTicket: TicketEntrada = new TicketEntrada();
    let tNuevoTicketUsuario: TicketUsuarioEntradas = new TicketUsuarioEntradas();

    tNuevoTicket.Corporacion = this.tEmpresa.Corporacion;
    tNuevoTicket.Empresa = this.tEmpresa.Empresa;
    tNuevoTicket.Codcliente = this.tClienteActual.Codcliente;
    tNuevoTicket.Titulo = this.tTitulo;
    tNuevoTicket.Tipo = this.tTipoActual;
    tNuevoTicket.Usuariocreador = this.tUsuario.Codusuario;
    tNuevoTicket.Usuariomodificador = this.tUsuario.Codusuario;

    tNuevoTicketUsuario.Corporacion = this.tEmpresa.Corporacion;
    tNuevoTicketUsuario.Empresa = this.tEmpresa.Empresa;
    tNuevoTicketUsuario.Codusuario = this.tUsuario.Codusuario;

    tNuevoTicket.LUsuarios.push(tNuevoTicketUsuario);

    this.tCrear.emit(tNuevoTicket);

  }

}
