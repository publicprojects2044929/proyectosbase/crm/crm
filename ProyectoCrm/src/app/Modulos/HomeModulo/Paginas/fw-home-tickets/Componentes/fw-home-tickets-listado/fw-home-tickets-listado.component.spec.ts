import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwHomeTicketsListadoComponent } from './fw-home-tickets-listado.component';

describe('FwHomeTicketsListadoComponent', () => {
  let component: FwHomeTicketsListadoComponent;
  let fixture: ComponentFixture<FwHomeTicketsListadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwHomeTicketsListadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwHomeTicketsListadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
