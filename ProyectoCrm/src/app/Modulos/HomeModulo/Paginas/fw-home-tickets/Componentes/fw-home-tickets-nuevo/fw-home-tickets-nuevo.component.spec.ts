import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwHomeTicketsNuevoComponent } from './fw-home-tickets-nuevo.component';

describe('FwHomeTicketsNuevoComponent', () => {
  let component: FwHomeTicketsNuevoComponent;
  let fixture: ComponentFixture<FwHomeTicketsNuevoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwHomeTicketsNuevoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwHomeTicketsNuevoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
