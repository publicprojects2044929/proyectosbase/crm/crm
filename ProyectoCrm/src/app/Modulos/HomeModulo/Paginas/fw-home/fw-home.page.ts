import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-fw-home',
  templateUrl: './fw-home.page.html',
  styleUrls: ['./fw-home.page.scss']
})
export class FwHomePage implements OnInit, OnDestroy {

  tFecha: Date = new Date();
  tTimer: any;

  constructor(
    public router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {

    this.tTimer = setInterval(() => {
      this.tFecha = new Date();
    }, 1);

  }

  ngOnDestroy() {

    clearInterval(this.tTimer);

  }

}
