import { Pipe, PipeTransform } from '@angular/core';
import { Ticket } from 'src/app/Clases/Estructura';

@Pipe({
  name: 'filtrarTickets'
})
export class FiltrarTicketsPipe implements PipeTransform {

  transform(tTickets: Ticket[], tEstatus: string[], tTipos: string[]): Ticket[] {

    let tTicketsFiltrados: Ticket[] = [];

    tTicketsFiltrados = tTickets.filter(tTicket => {

      if(tEstatus.length > 0 || tTipos.length > 0){
        return tEstatus.includes(tTicket.Estatus) || tTipos.includes(tTicket.Tipo);
      }
      else{
        return tTicket;
      }

    });

    return tTicketsFiltrados;

  }

}
