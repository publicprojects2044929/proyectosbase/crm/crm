import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/Shared/shared.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { HomeRoutingModule } from './home-routing.module';
import { FwHomePage } from './Paginas/fw-home/fw-home.page';
import { FwHomeVentasPage } from './Paginas/fw-home-ventas/fw-home-ventas.page';
import { FwHomePedidosPage } from './Paginas/fw-home-pedidos/fw-home-pedidos.page';
import { FwHomeTicketsPage } from './Paginas/fw-home-tickets/fw-home-tickets.page';
import { FwHomePedidosTotalesComponent } from './Paginas/fw-home-pedidos/Componentes/fw-home-pedidos-totales/fw-home-pedidos-totales.component';
import { FwHomePedidosListaComponent } from './Paginas/fw-home-pedidos/Componentes/fw-home-pedidos-lista/fw-home-pedidos-lista.component';
import { FwHomeVentasTotalesComponent } from './Paginas/fw-home-ventas/Componentes/fw-home-ventas-totales/fw-home-ventas-totales.component';
import { FwHomeVentasListaComponent } from './Paginas/fw-home-ventas/Componentes/fw-home-ventas-lista/fw-home-ventas-lista.component';
import { FwHomeTicketsNuevoComponent } from './Paginas/fw-home-tickets/Componentes/fw-home-tickets-nuevo/fw-home-tickets-nuevo.component';
import { FwHomeTicketsListadoComponent } from './Paginas/fw-home-tickets/Componentes/fw-home-tickets-listado/fw-home-tickets-listado.component';
import { FiltrarTicketsPipe } from './Pipes/filtrar-tickets.pipe';

@NgModule({
  declarations: [FwHomePage,
                  FwHomeVentasPage,
                  FwHomePedidosPage,
                  FwHomeTicketsPage,
                  FwHomePedidosTotalesComponent,
                  FwHomePedidosListaComponent,
                  FwHomeVentasTotalesComponent,
                  FwHomeVentasListaComponent,
                  FwHomeTicketsNuevoComponent,
                  FwHomeTicketsListadoComponent,
                  FiltrarTicketsPipe],
  imports: [
    CommonModule,
    FormsModule,
    HomeRoutingModule,
    SharedModule,
    NgbModule
  ]
})
export class HomeModule { }
