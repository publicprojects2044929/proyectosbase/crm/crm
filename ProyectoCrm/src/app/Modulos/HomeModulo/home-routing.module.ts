import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FwHomePage } from './Paginas/fw-home/fw-home.page';
import { FwHomeVentasPage } from './Paginas/fw-home-ventas/fw-home-ventas.page';
import { FwHomePedidosPage } from './Paginas/fw-home-pedidos/fw-home-pedidos.page';
import { FwHomeTicketsPage } from './Paginas/fw-home-tickets/fw-home-tickets.page';
import { FwPlantillaComponent } from 'src/app/Shared/Formularios/fw-plantilla/fw-plantilla.component';

const routes: Routes = [
  {
    path: '',
    component: FwPlantillaComponent,
    children: [
      {
        path: '',
        component: FwHomePage,
        data: {
          Codformulario: 'fHome'
        },
        children:[
          {
            path: '',
            redirectTo: '/Home/Tickets',
            pathMatch: 'full'
          },
          {
            path: 'Tickets',
            component: FwHomeTicketsPage
          },
          {
            path: 'Ventas',
            component: FwHomeVentasPage
          },
          {
            path: 'Pedidos',
            component: FwHomePedidosPage
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
