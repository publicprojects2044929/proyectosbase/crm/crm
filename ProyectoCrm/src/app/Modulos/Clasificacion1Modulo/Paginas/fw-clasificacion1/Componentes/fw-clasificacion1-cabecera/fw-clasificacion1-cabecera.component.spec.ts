import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwClasificacion1CabeceraComponent } from './fw-clasificacion1-cabecera.component';

describe('FwClasificacion1CabeceraComponent', () => {
  let component: FwClasificacion1CabeceraComponent;
  let fixture: ComponentFixture<FwClasificacion1CabeceraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwClasificacion1CabeceraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwClasificacion1CabeceraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
