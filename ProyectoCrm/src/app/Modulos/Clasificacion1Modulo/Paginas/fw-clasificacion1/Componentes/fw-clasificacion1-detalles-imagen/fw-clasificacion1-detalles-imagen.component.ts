import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Clasificacion1, Configuracion } from 'src/app/Clases/Estructura';
import { ManejarImagenes } from 'src/app/Manejadores/cls-procedimientos';

@Component({
  selector: 'app-fw-clasificacion1-detalles-imagen',
  templateUrl: './fw-clasificacion1-detalles-imagen.component.html',
  styleUrls: ['./fw-clasificacion1-detalles-imagen.component.scss']
})
export class FwClasificacion1DetallesImagenComponent implements OnInit {

  @ViewChild('flArchivo', { static: true }) tControlArchio: any;

  @Input('Clasificacion1') tClasificacion1: Clasificacion1 = new Clasificacion1();
  @Input('Configuracion') tConfiguracion: Configuracion = new Configuracion();
  @Input('ManejarImagen') tManejarImagen: ManejarImagenes = new ManejarImagenes();

  constructor() { }

  ngOnInit(): void {
  }

  Limpiar() {
    this.tControlArchio.nativeElement.value = "";
  }

  BloquearImagen(): boolean {

    if (this.tConfiguracion.Valor === "S") {

      return false;

    }
    else {

      return true;

    }

  }

}
