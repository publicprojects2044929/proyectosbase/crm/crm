import { Component, OnInit, Input } from '@angular/core';
import { Clasificacion1 } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-clasificacion1-cabecera',
  templateUrl: './fw-clasificacion1-cabecera.component.html',
  styleUrls: ['./fw-clasificacion1-cabecera.component.scss']
})
export class FwClasificacion1CabeceraComponent implements OnInit {

  @Input('Clasificacion1') tClasificacion1: Clasificacion1 = new Clasificacion1();

  constructor() { }

  ngOnInit(): void {
  }

}
