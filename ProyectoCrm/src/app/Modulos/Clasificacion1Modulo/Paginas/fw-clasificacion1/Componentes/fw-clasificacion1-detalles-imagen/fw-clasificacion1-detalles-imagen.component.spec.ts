import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwClasificacion1DetallesImagenComponent } from './fw-clasificacion1-detalles-imagen.component';

describe('FwClasificacion1DetallesImagenComponent', () => {
  let component: FwClasificacion1DetallesImagenComponent;
  let fixture: ComponentFixture<FwClasificacion1DetallesImagenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwClasificacion1DetallesImagenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwClasificacion1DetallesImagenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
