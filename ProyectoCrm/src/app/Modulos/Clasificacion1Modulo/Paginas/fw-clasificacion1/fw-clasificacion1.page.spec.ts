import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwClasificacion1Page } from './fw-clasificacion1.page';

describe('FwClasificacion1Page', () => {
  let component: FwClasificacion1Page;
  let fixture: ComponentFixture<FwClasificacion1Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwClasificacion1Page ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwClasificacion1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
