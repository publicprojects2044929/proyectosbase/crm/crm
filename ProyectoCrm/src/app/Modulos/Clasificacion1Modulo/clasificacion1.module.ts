import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from 'src/app/Shared/shared.module';

import { Clasificacion1RoutingModule } from './clasificacion1-routing.module';
import { FwClasificacion1Page } from './Paginas/fw-clasificacion1/fw-clasificacion1.page';
import { FwClasificacion1CabeceraComponent } from './Paginas/fw-clasificacion1/Componentes/fw-clasificacion1-cabecera/fw-clasificacion1-cabecera.component';
import { FwClasificacion1DetallesImagenComponent } from './Paginas/fw-clasificacion1/Componentes/fw-clasificacion1-detalles-imagen/fw-clasificacion1-detalles-imagen.component';

@NgModule({
  declarations: [FwClasificacion1Page, FwClasificacion1CabeceraComponent, FwClasificacion1DetallesImagenComponent],
  imports: [
    CommonModule,
    Clasificacion1RoutingModule,
    SharedModule,
    FormsModule,
    NgbModule
  ]
})
export class Clasificacion1Module { }
