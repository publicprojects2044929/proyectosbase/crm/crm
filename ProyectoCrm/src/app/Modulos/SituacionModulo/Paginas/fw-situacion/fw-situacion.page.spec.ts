import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwSituacionPage } from './fw-situacion.page';

describe('FwSituacionPage', () => {
  let component: FwSituacionPage;
  let fixture: ComponentFixture<FwSituacionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwSituacionPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwSituacionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
