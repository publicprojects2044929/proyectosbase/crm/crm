import { Component, OnInit, Input } from '@angular/core';
import { Situacion } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-situacion-cabecera',
  templateUrl: './fw-situacion-cabecera.component.html',
  styleUrls: ['./fw-situacion-cabecera.component.scss']
})
export class FwSituacionCabeceraComponent implements OnInit {

  @Input('Situacion') tSituacion: Situacion = new Situacion();

  constructor() { }

  ngOnInit(): void {
  }

}
