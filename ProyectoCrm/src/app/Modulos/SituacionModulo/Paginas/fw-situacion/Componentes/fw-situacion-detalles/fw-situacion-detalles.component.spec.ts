import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwSituacionDetallesComponent } from './fw-situacion-detalles.component';

describe('FwSituacionDetallesComponent', () => {
  let component: FwSituacionDetallesComponent;
  let fixture: ComponentFixture<FwSituacionDetallesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwSituacionDetallesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwSituacionDetallesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
