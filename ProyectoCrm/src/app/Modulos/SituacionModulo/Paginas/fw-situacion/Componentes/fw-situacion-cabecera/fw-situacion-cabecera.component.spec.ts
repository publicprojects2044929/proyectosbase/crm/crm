import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwSituacionCabeceraComponent } from './fw-situacion-cabecera.component';

describe('FwSituacionCabeceraComponent', () => {
  let component: FwSituacionCabeceraComponent;
  let fixture: ComponentFixture<FwSituacionCabeceraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwSituacionCabeceraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwSituacionCabeceraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
