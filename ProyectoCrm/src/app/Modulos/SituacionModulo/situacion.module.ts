import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SituacionRoutingModule } from './situacion-routing.module';
import { FwSituacionPage } from './Paginas/fw-situacion/fw-situacion.page';
import { SharedModule } from 'src/app/Shared/shared.module';
import { FwSituacionCabeceraComponent } from './Paginas/fw-situacion/Componentes/fw-situacion-cabecera/fw-situacion-cabecera.component';
import { FwSituacionDetallesComponent } from './Paginas/fw-situacion/Componentes/fw-situacion-detalles/fw-situacion-detalles.component'

@NgModule({
  declarations: [FwSituacionPage, FwSituacionCabeceraComponent, FwSituacionDetallesComponent],
  imports: [
    CommonModule,
    SituacionRoutingModule,
    SharedModule,
    FormsModule,
    NgbModule
  ]
})
export class SituacionModule { }
