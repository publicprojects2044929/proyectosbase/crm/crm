import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from 'src/app/Shared/shared.module';
import { ColorChromeModule } from 'ngx-color/chrome';

import { ColorRoutingModule } from './color-routing.module';
import { FwColorPage } from './Paginas/fw-color/fw-color.page';
import { FwColorCabeceraComponent } from './Paginas/fw-color/Componentes/fw-color-cabecera/fw-color-cabecera.component';
import { FwColorPaletaComponent } from './Paginas/fw-color/Componentes/fw-color-paleta/fw-color-paleta.component';
import { FwColorImagenComponent } from './Paginas/fw-color/Componentes/fw-color-imagen/fw-color-imagen.component';
import { FwColorHexadecimalesComponent } from './Paginas/fw-color/Componentes/fw-color-hexadecimales/fw-color-hexadecimales.component';

@NgModule({
  declarations: [FwColorPage, FwColorCabeceraComponent, FwColorPaletaComponent, FwColorImagenComponent, FwColorHexadecimalesComponent],
  imports: [
    CommonModule,
    ColorRoutingModule,
    SharedModule,
    FormsModule,
    NgbModule,
    ColorChromeModule
  ]
})
export class ColorModule { }
