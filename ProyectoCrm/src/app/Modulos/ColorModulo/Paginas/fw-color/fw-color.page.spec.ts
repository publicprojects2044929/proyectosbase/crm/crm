import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwColorPage } from './fw-color.page';

describe('FwColorPage', () => {
  let component: FwColorPage;
  let fixture: ComponentFixture<FwColorPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwColorPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwColorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
