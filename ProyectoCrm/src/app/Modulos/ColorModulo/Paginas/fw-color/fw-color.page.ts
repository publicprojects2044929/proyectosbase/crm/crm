import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { FormularioConfiguracion } from 'src/app/Clases/EstructuraConfiguracion';
import { Usuario, Empresas, Filtros, Color, ColorHexadecimales } from 'src/app/Clases/Estructura';
import { SessionService } from 'src/app/Core/Session/session.service';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { ToastrService } from 'ngx-toastr';
import { ErroresService } from 'src/app/Modales/mensajes/fw-modal-msj/errores.service';
import { MensajesService } from 'src/app/Modales/mensajes/fw-modal-msj/mensajes.service';
import { ConfirmacionService } from 'src/app/Modales/confirmacion/fw-modal-confirmacion/confirmacion.service';
import { ColorEntrada, ColorHexadecimalesEntrada } from 'src/app/Clases/EstructuraEntrada';
import { environment } from 'src/environments/environment';
import { Subscription } from 'rxjs';
import { ConfiguracionService } from 'src/app/Core/Configuracion/configuracion.service';

@Component({
  selector: 'app-fw-color',
  templateUrl: './fw-color.page.html',
  styleUrls: ['./fw-color.page.scss']
})
export class FwColorPage implements OnInit, OnDestroy {

  tCargaPorImg: boolean = true;

  tCodFormularioActual: string = "fColor";
  tConfiguracionFormulario: FormularioConfiguracion = new FormularioConfiguracion();
  tPermisoAgregar: boolean = false;
  tPermisoEditar: boolean = false;
  tPermisoEliminar: boolean = false;
  tHabilitarGuardar: boolean = false;

  tUsuarioActual: Usuario = new Usuario();
  tEmpresaActual: Empresas = new Empresas();
  tEmpresa$: Subscription;
  tUsuario$: Subscription;

  tTitulo: string = "Gestión de Colores";
  tNuevo: boolean = true;
  tPosicion: number = 0;
  tPagina: number = 1;
  tTamanoPag: number = 5;

  tColores: Color[] = [];
  tColorActual: Color = new Color();

  tFiltros: Filtros[] = [];
  tFiltroActual: Filtros = new Filtros();
  tFiltroBusqueda: string = "";

  tConfiguracion$: Subscription;
  tHayConfiguracionWs$: Subscription;
  tStrConfiguracion: string = 'Color/Color.Config.json';

  constructor(public router: Router,
    private tSesion: SessionService,
    private tConfiguracionService: ConfiguracionService,
    public Ws: ClsServiciosService,
    private toastr: ToastrService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService,
    private tConfServices: ConfirmacionService) {

    this.tFiltroBusqueda = "";
    this.tPosicion = 0;

    this.tFiltros = [

      { Codfiltro: "Codcolor", Filtro: "Código" },
      { Codfiltro: "Descripcion", Filtro: "Descripción" },

    ]

  }

  ngOnInit() {

    this.tConfiguracion$ = this.tConfiguracionService
      .Consumir_Obtener_Configuracion(this.tStrConfiguracion)
      .subscribe((tConfiguracion: FormularioConfiguracion) => {

        this.tConfiguracionFormulario = tConfiguracion;
        this.tTitulo = this.tConfiguracionFormulario.Titulo;
        this.tErroresServices.tTituloMsj = this.tConfiguracionFormulario.ModalMensaje.Titulo;
        this.tMsjServices.tTituloMsj = this.tConfiguracionFormulario.ModalMensaje.Titulo;
        this.tConfServices.tTituloConf = this.tConfiguracionFormulario.ModalConfirmacion.Titulo;

      });

    this.tEmpresa$ = this.tSesion.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    })

    this.tUsuario$ = this.tSesion.tUsuarioActual$.subscribe((tUsuario: Usuario) => {

      this.tUsuarioActual = tUsuario;

    })

    var tTienePermiso: boolean = this.tSesion.ObtenerPermisoAcceso(this.tCodFormularioActual);
    this.tPermisoAgregar = this.tSesion.ObtenerPermisoAgregar();
    this.tPermisoEditar = this.tSesion.ObtenerPermisoEditar();
    this.tPermisoEliminar = this.tSesion.ObtenerPermisoEliminar();

    if (tTienePermiso == true) {

      this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
        .subscribe((tHayConfig: boolean) => {

          if (tHayConfig === true) {

            this.Buscar_Colores();
            this.Nuevo();

          }

        });

    }
    else {

      this.tMsjServices.AbrirModalMsj(environment.msjSinAcceso);
      this.router.navigate(['/Home']);

    }

  }

  ngOnDestroy() {

    this.tEmpresa$.unsubscribe();
    this.tUsuario$.unsubscribe();
    this.tConfiguracion$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();

  }

  //#region "Funciones de busqueda"

  Buscar_Colores(): void {

    this.Ws
      .Color()
      .Consumir_Obtener_ColoresFull(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa)
      .subscribe(Respuesta => {

        this.tColores = [];

        if (Respuesta.Resultado == "N") {

          // this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tColores = Respuesta.Datos;
          this.tFiltroActual = this.tFiltros[0];

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  //#region "CRUD"

  Nuevo_Color(): void {

    var tNuevoColor: ColorEntrada;
    tNuevoColor = {
      "Id": 0,
      "Corporacion": this.tEmpresaActual.Corporacion,
      "Empresa": this.tEmpresaActual.Empresa,
      "Codcolor": "",
      "Descripcion": this.tColorActual.Descripcion,
      "Hexadecimal": this.tColorActual.Hexadecimal,
      "Usuariocreador": this.tUsuarioActual.Codusuario,
      "Usuariomodificador": this.tUsuarioActual.Codusuario,
      "LHexadecimales": []
    }

    this.tColorActual.LHexadecimales.forEach(tHexadecimal => {

      var tNuevoHexa: ColorHexadecimalesEntrada;
      tNuevoHexa = {

        "Id": 0,
        "Corporacion": tHexadecimal.Corporacion,
        "Empresa": tHexadecimal.Empresa,
        "Codcolor": tHexadecimal.Codcolor,
        "Principal": tHexadecimal.Principal,
        "Hexadecimal": tHexadecimal.Hexadecimal,
        "Nuevo": tHexadecimal.Nuevo,
        "Usuariocreador": tHexadecimal.Usuariocreador,
        "Usuariomodificador": tHexadecimal.Usuariomodificador,

      }

      tNuevoColor.LHexadecimales.push(tNuevoHexa);

    });


    this.Ws.Color()
      .Consumir_Crear_Color(tNuevoColor)
      .subscribe(Respuesta => {


        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.toastr.success('El color se ha creado de manera exitosa!');
          this.tFiltroActual = this.tFiltros[0];
          this.Buscar_Colores();
          this.Nuevo();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Modificar_Color(): void {

    var tModificarColor: ColorEntrada;
    tModificarColor = {
      "Id": 0,
      "Corporacion": this.tColorActual.Corporacion,
      "Empresa": this.tColorActual.Empresa,
      "Codcolor": this.tColorActual.Codcolor,
      "Descripcion": this.tColorActual.Descripcion,
      "Hexadecimal": this.tColorActual.Hexadecimal,
      "Usuariocreador": this.tColorActual.Usuariocreador,
      "Usuariomodificador": this.tUsuarioActual.Codusuario,
      "LHexadecimales": []
    }

    this.tColorActual.LHexadecimales.forEach(tHexadecimal => {

      var tModificarHexa: ColorHexadecimalesEntrada;
      tModificarHexa = {

        "Id": 0,
        "Corporacion": tHexadecimal.Corporacion,
        "Empresa": tHexadecimal.Empresa,
        "Codcolor": tHexadecimal.Codcolor,
        "Principal": tHexadecimal.Principal,
        "Hexadecimal": tHexadecimal.Hexadecimal,
        "Nuevo": tHexadecimal.Nuevo,
        "Usuariocreador": tHexadecimal.Usuariocreador,
        "Usuariomodificador": tHexadecimal.Usuariomodificador,

      }

      tModificarColor.LHexadecimales.push(tModificarHexa);

    });

    this.Ws.Color()
      .Consumir_Modificar_Color(tModificarColor)
      .subscribe(Respuesta => {


        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.toastr.success('El color se ha modificado de manera exitosa!');
          this.tFiltroActual = this.tFiltros[0];
          this.Buscar_Colores();
          this.Nuevo();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Eliminar_Color(): void {

    var tEliminarColor: ColorEntrada;
    tEliminarColor = {
      "Id": 0,
      "Corporacion": this.tColorActual.Corporacion,
      "Empresa": this.tColorActual.Empresa,
      "Codcolor": this.tColorActual.Codcolor,
      "Descripcion": this.tColorActual.Descripcion,
      "Hexadecimal": this.tColorActual.Hexadecimal,
      "Usuariocreador": this.tUsuarioActual.Codusuario,
      "Usuariomodificador": this.tUsuarioActual.Codusuario,
      "LHexadecimales": []
    }

    this.Ws.Color()
      .Consumir_Eliminar_Color(tEliminarColor)
      .subscribe(Respuesta => {


        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.toastr.success('El color se ha eliminado de manera exitosa!');
          this.tFiltroActual = this.tFiltros[0];
          this.Buscar_Colores();
          this.Nuevo();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Eliminar_Hexadecimal(tHexadecimal: ColorHexadecimales, tPosicion: number): void {

    var tEliminarColorHexa: ColorHexadecimalesEntrada;
    tEliminarColorHexa = {
      "Id": 0,
      "Corporacion": tHexadecimal.Corporacion,
      "Empresa": tHexadecimal.Empresa,
      "Codcolor": tHexadecimal.Codcolor,
      "Hexadecimal": tHexadecimal.Hexadecimal,
      "Principal": tHexadecimal.Principal,
      "Nuevo": tHexadecimal.Nuevo,
      "Usuariocreador": tHexadecimal.Usuariocreador,
      "Usuariomodificador": tHexadecimal.Usuariomodificador,
    }

    this.Ws
      .ColorHexadecimal()
      .Consumir_Eliminar_ColorHexadecimal(tEliminarColorHexa)
      .subscribe(Respuesta => {


        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.toastr.success('El hexadecimal se ha eliminado de manera exitosa!');
          this.tColorActual.LHexadecimales.splice(tPosicion, 1);
          this.Buscar_Colores();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  Nuevo(): void {

    this.tHabilitarGuardar = this.tPermisoAgregar;
    this.tNuevo = true;
    this.tColorActual = new Color();

    this.toastr.info('Por favor llene los datos necesarios para registrar un nuevo Color!');

  }

  Seleccionar_Color(tColor_Sel: Color, tPos: number): void {

    this.tHabilitarGuardar = this.tPermisoEditar;
    this.tNuevo = false;
    this.tPosicion = tPos;
    this.tColorActual = new Color();
    Object.assign(this.tColorActual, tColor_Sel);
    this.tColorActual.Usuariomodificador = this.tUsuarioActual.Usuariomodificador;

  }

  Seleccionar_Color_Eliminar(tColor_Sel: Color): void {

    this.tColorActual = new Color();
    Object.assign(this.tColorActual, tColor_Sel);
    this.tColorActual.Usuariomodificador = this.tUsuarioActual.Usuariomodificador;

    this.tConfServices
      .AbrirModalConf(this.tConfiguracionFormulario.ModalConfirmacion.Eliminacion)
      .then(Resultado => {

        switch (Resultado) {
          case "S": {

            this.Eliminar_Color();
            break;

          }
        }

      });

  }

  Cancelar(): void {

    this.Nuevo();

  }

  ValidarFormulario(): boolean {

    let valido: boolean = false;

    if (this.tColorActual.Descripcion == "") {

      this.toastr.warning('Debe ingresar una Descripción!', 'Disculpe!');
      return valido;

    }

    return true;

  }

  Agregar(): void {

    if (this.ValidarFormulario() == false) {
      return
    }

    switch (this.tNuevo) {
      case true: {

        this.tConfServices
          .AbrirModalConf(this.tConfiguracionFormulario.ModalConfirmacion.Agregar)
          .then(Resultado => {

            switch (Resultado) {
              case "S": {

                this.Nuevo_Color();
                break;

              }
            }

          });

        break;

      }
      default: {

        this.tConfServices
          .AbrirModalConf(this.tConfiguracionFormulario.ModalConfirmacion.Edicion)
          .then(Resultado => {

            switch (Resultado) {
              case "S": {

                this.Modificar_Color();
                break;

              }
            }

          });
        break;

      }
    }

  }

  PorPaleta() {
    event.preventDefault();
    this.tCargaPorImg = false;
  }

  PorImagen() {
    event.preventDefault();
    this.tCargaPorImg = true;
  }

  EliminarHexadecimal({ tHexadecimal, tPosicion }) {

    this.tConfServices
      .AbrirModalConf('¿Desea eliminar el hexadecimal?')
      .then(Resultado => {

        switch (Resultado) {
          case "S": {

            this.Eliminar_Hexadecimal(tHexadecimal, tPosicion);
            break;

          }
        }

      });

  }

  AgregarHexadecimal(tHexadecimal: string): void {

    let tExisteHexa: ColorHexadecimales = this.tColorActual.LHexadecimales.find(tHexa => tHexa.Hexadecimal === tHexadecimal) || undefined;

    if (tExisteHexa === undefined) {

      let tNuevoHexa = new ColorHexadecimales();
      tNuevoHexa.Corporacion = this.tEmpresaActual.Corporacion;
      tNuevoHexa.Empresa = this.tEmpresaActual.Empresa;
      tNuevoHexa.Codcolor = this.tColorActual.Codcolor;
      tNuevoHexa.Hexadecimal = tHexadecimal;
      tNuevoHexa.Usuariocreador = this.tUsuarioActual.Codusuario;
      tNuevoHexa.Usuariomodificador = this.tUsuarioActual.Codusuario;
      // this.tColorActual.LHexadecimales.push(tNuevoHexa);
      this.tColorActual.LHexadecimales = [
        ...this.tColorActual.LHexadecimales,
        tNuevoHexa
      ]

    }

  }

}
