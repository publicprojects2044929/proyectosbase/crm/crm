import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwColorCabeceraComponent } from './fw-color-cabecera.component';

describe('FwColorCabeceraComponent', () => {
  let component: FwColorCabeceraComponent;
  let fixture: ComponentFixture<FwColorCabeceraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwColorCabeceraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwColorCabeceraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
