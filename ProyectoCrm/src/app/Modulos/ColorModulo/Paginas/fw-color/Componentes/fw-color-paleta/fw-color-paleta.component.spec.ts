import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwColorPaletaComponent } from './fw-color-paleta.component';

describe('FwColorPaletaComponent', () => {
  let component: FwColorPaletaComponent;
  let fixture: ComponentFixture<FwColorPaletaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwColorPaletaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwColorPaletaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
