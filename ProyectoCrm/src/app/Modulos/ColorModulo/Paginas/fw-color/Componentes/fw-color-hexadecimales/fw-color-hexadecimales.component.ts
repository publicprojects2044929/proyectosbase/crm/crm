import { Component, OnInit, OnChanges, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { Color, ColorHexadecimales } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-color-hexadecimales',
  templateUrl: './fw-color-hexadecimales.component.html',
  styleUrls: ['./fw-color-hexadecimales.component.scss']
})
export class FwColorHexadecimalesComponent implements OnInit, OnChanges {

  @Input('Hexadecimales') tHexadecimales: ColorHexadecimales[] = [];

  @Output('EliminarColor') tEliminar: EventEmitter<{tHexadecimal: ColorHexadecimales, tPosicion: number}> = new EventEmitter();

  tColores1x3: any[] = [];

  tPosicionHexadecimales: number = 0;
  tPaginaHexadecimales: number = 1;
  tTamanoPagHexadecimales: number = 2;

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges){

    if('tHexadecimales' in changes){

      this.AgruparColores();

    }

  }

  EliminarHexadecimal(tHexadecimal: ColorHexadecimales) {

    let tPosicion: number = this.tHexadecimales.findIndex((tHexa, tPosicion) => {
        return tHexa.Hexadecimal === tHexadecimal.Hexadecimal
      }) || 0;

    if (tHexadecimal.Nuevo === "S") {

      this.tHexadecimales.splice(tPosicion, 1);
      this.AgruparColores();

    }
    else {

      this.tEliminar.emit({tHexadecimal, tPosicion})

    }



  }

  AsignarPrincipal(tHexadecimal: ColorHexadecimales) {

    tHexadecimal.Principal = 1;

    this.tColores1x3.map((tColoresX3: ColorHexadecimales[]) => {

      tColoresX3.map(tColorHex => {

        if (tColorHex.Hexadecimal !== tHexadecimal.Hexadecimal) {

          tColorHex.Principal = 0;

        }

      });

    });

    this.tHexadecimales.map(tColorHex => {

      if (tColorHex.Hexadecimal !== tHexadecimal.Hexadecimal) {

        tColorHex.Principal = 0;

      }

    });

    event.preventDefault();

  }

  AgruparColores() {

    this.tColores1x3 = [];
    let tColoresX3: ColorHexadecimales[] = [];
    let tContador: number = 0;

    this.tHexadecimales.forEach((tColor, tPosicion) => {

      tContador++;
      tColoresX3.push(tColor);

      if (tContador >= 3 || tPosicion == this.tHexadecimales.length - 1) {

        tContador = 0;
        this.tColores1x3.push(tColoresX3);
        tColoresX3 = [];

      }

    })

  }

}
