import { Component, OnInit, Input } from '@angular/core';
import { Color } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-color-cabecera',
  templateUrl: './fw-color-cabecera.component.html',
  styleUrls: ['./fw-color-cabecera.component.scss']
})
export class FwColorCabeceraComponent implements OnInit {

  @Input('Color') tColor: Color = new Color();

  constructor() { }

  ngOnInit(): void {
  }

}
