import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwColorImagenComponent } from './fw-color-imagen.component';

describe('FwColorImagenComponent', () => {
  let component: FwColorImagenComponent;
  let fixture: ComponentFixture<FwColorImagenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwColorImagenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwColorImagenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
