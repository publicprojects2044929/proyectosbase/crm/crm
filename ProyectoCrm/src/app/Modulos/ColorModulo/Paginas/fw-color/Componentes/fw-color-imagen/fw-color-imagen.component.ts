import { Component, OnInit, ElementRef, Output, EventEmitter, OnDestroy } from '@angular/core';
import { ManejarImagenes } from 'src/app/Manejadores/cls-procedimientos';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-fw-color-imagen',
  templateUrl: './fw-color-imagen.component.html',
  styleUrls: ['./fw-color-imagen.component.scss']
})
export class FwColorImagenComponent implements OnInit, OnDestroy {

  @Output('AgregarColor') tAgregar: EventEmitter<string> = new EventEmitter();

  tCanvasImg: HTMLCanvasElement;
  tFlArchivo: ElementRef;

  tContexto: CanvasRenderingContext2D;
  tAncho: number = 0;
  tAlto: number = 0;

  tManejarImagen: ManejarImagenes = new ManejarImagenes();

  tImagen$: Subscription;

  constructor() { }

  ngOnInit(): void {
  }

  ngOnDestroy(){

    if(this.tImagen$){
      this.tImagen$.unsubscribe();
    }

  }

  CargarArchivo(Imagenes: any): void {

    this.tManejarImagen.Cargar_Archivo(Imagenes);

    if (this.tCanvasImg === undefined) {

      this.tCanvasImg = document.getElementById('cvImagen') as HTMLCanvasElement;
      this.tContexto = this.tCanvasImg.getContext('2d');
      this.tAncho = this.tCanvasImg.width;
      this.tAlto = this.tCanvasImg.height;

    }

    this.tImagen$ = this.tManejarImagen
                        .ObtenerImagen64$()
                        .subscribe(tImagen => {

                          const image = new Image();
                          image.setAttribute('crossOrigin', 'anonymus');
                          image.setAttribute('src', tImagen);

                          image.addEventListener('load', () => {

                            this.tContexto.clearRect(0, 0, this.tAncho, this.tAlto);
                            this.tContexto.drawImage(image, 0, 0, this.tAncho, this.tAlto);
                            this.ObtenerPaletaColores();

                          });

                        });

  }

  ObtenerPaletaColores(): void {

    let tImagenData = this.tContexto.getImageData(0, 0, this.tAncho, this.tAlto).data;
    let tCalidad: number = 1000;

    for (let jv = 0; jv < (this.tAncho * this.tAlto); jv = jv + tCalidad) {

      let tOffset = jv + 4;
      let tAlpha = tImagenData[tOffset + 3];

      if (tAlpha > 0) {

        let tRed = tImagenData[tOffset];
        let tGreen = tImagenData[tOffset + 1];
        let tBlue = tImagenData[tOffset + 2];
        let tRgb: string = `rgba(${tRed}, ${tGreen}, ${tBlue}, ${tAlpha})`;
        let tHexadecimal = this.rgb2hex(tRgb);
        // this.AgregarHexadecimal(tHexadecimal);
        this.tAgregar.emit(tHexadecimal);

      }

    }

    this.tContexto.clearRect(0, 0, this.tAncho, this.tAlto);

  }

  rgb2hex(rgb): string {

    rgb = rgb.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i);
    return (rgb && rgb.length === 4) ? "#" +
      ("0" + parseInt(rgb[1], 10).toString(16)).slice(-2) +
      ("0" + parseInt(rgb[2], 10).toString(16)).slice(-2) +
      ("0" + parseInt(rgb[3], 10).toString(16)).slice(-2) : '';

  }

}
