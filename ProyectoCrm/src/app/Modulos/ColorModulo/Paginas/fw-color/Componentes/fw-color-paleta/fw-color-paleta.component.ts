import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ColorEvent } from 'ngx-color';

@Component({
  selector: 'app-fw-color-paleta',
  templateUrl: './fw-color-paleta.component.html',
  styleUrls: ['./fw-color-paleta.component.scss']
})
export class FwColorPaletaComponent implements OnInit {

  @Output('AgregarColor') tAgregar: EventEmitter<string> = new EventEmitter();

  tHexadecimalActual: string = "";

  constructor() { }

  ngOnInit(): void {
  }

  Cambiar_Color($event: ColorEvent): void {

    this.tHexadecimalActual = $event.color.hex;

  }

  Agregar(){
    this.tAgregar.emit(this.tHexadecimalActual);
  }

}
