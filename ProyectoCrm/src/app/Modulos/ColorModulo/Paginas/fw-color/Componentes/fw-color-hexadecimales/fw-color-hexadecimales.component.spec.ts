import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwColorHexadecimalesComponent } from './fw-color-hexadecimales.component';

describe('FwColorHexadecimalesComponent', () => {
  let component: FwColorHexadecimalesComponent;
  let fixture: ComponentFixture<FwColorHexadecimalesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwColorHexadecimalesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwColorHexadecimalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
