import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from 'src/app/Shared/shared.module';

import { CondicionRoutingModule } from './condicion-routing.module';
import { FwCondicionPage } from './Paginas/fw-condicion/fw-condicion.page';
import { FwCondicionCabeceraComponent } from './Paginas/fw-condicion/Componentes/fw-condicion-cabecera/fw-condicion-cabecera.component';
import { FwCondicionDetallesComponent } from './Paginas/fw-condicion/Componentes/fw-condicion-detalles/fw-condicion-detalles.component';

@NgModule({
  declarations: [FwCondicionPage, FwCondicionCabeceraComponent, FwCondicionDetallesComponent],
  imports: [
    CommonModule,
    CondicionRoutingModule,
    SharedModule,
    FormsModule,
    NgbModule
  ]
})
export class CondicionModule { }
