import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormularioConfiguracion } from 'src/app/Clases/EstructuraConfiguracion';
import { Usuario, Empresas, Condicion, Formulario, Filtros, Configuracion } from 'src/app/Clases/Estructura';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/Core/Session/session.service';
import { ToastrService } from 'ngx-toastr';
import { ErroresService } from 'src/app/Modales/mensajes/fw-modal-msj/errores.service';
import { MensajesService } from 'src/app/Modales/mensajes/fw-modal-msj/mensajes.service';
import { ConfirmacionService } from 'src/app/Modales/confirmacion/fw-modal-confirmacion/confirmacion.service';
import { environment } from 'src/environments/environment';
import { CondicionEntrada } from 'src/app/Clases/EstructuraEntrada';
import { Subscription } from 'rxjs';
import { ConfiguracionService } from 'src/app/Core/Configuracion/configuracion.service';

@Component({
  selector: 'app-fw-condicion',
  templateUrl: './fw-condicion.page.html',
  styleUrls: ['./fw-condicion.page.scss']
})
export class FwCondicionPage implements OnInit, OnDestroy {

  tCodFormularioActual: string = "fCondicion";
  tConfiguracionFormulario: FormularioConfiguracion = new FormularioConfiguracion();
  tPermisoAgregar: boolean = false;
  tPermisoEditar: boolean = false;
  tPermisoEliminar: boolean = false;
  tHabilitarGuardar: boolean = false;

  tUsuarioActual: Usuario = new Usuario();
  tEmpresaActual: Empresas = new Empresas();
  tEmpresa$: Subscription;
  tUsuario$: Subscription;

  tTitulo: string = "Gestión de condición";
  tNuevo: boolean = true;
  tPosicion: number = 0;
  tPagina: number = 1;
  tTamanoPag: number = 5;

  tCondiciones: Condicion[] = [];
  tCondicionActual: Condicion = new Condicion();

  tFormularios: Formulario[] = [];

  tFiltros: Filtros[] = [];
  tFiltroActual: Filtros = new Filtros();
  tFiltroBusqueda: string = "";

  tConfiguracion: Configuracion = new Configuracion();

  tConfiguracion$: Subscription;
  tHayConfiguracionWs$: Subscription;
  tStrConfiguracion: string = 'Condicion/Condicion.Config.json';

  constructor(public Ws: ClsServiciosService,
    private router: Router,
    private tSesion: SessionService,
    private tConfiguracionService: ConfiguracionService,
    private toastr: ToastrService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService,
    private tConfServices: ConfirmacionService) {

    this.tFiltroBusqueda = "";
    this.tPosicion = 0;

    this.tFiltros = [

      { Codfiltro: "Codcondicion", Filtro: "Código" },
      { Codfiltro: "Descripcion", Filtro: "Descripción" },

    ]

  }

  ngOnInit() {

    this.tConfiguracion$ = this.tConfiguracionService
      .Consumir_Obtener_Configuracion(this.tStrConfiguracion)
      .subscribe((tConfiguracion: FormularioConfiguracion) => {

        this.tConfiguracionFormulario = tConfiguracion;
        this.tTitulo = this.tConfiguracionFormulario.Titulo;
        this.tErroresServices.tTituloMsj = this.tConfiguracionFormulario.ModalMensaje.Titulo;
        this.tMsjServices.tTituloMsj = this.tConfiguracionFormulario.ModalMensaje.Titulo;
        this.tConfServices.tTituloConf = this.tConfiguracionFormulario.ModalConfirmacion.Titulo;

      });

    this.tEmpresa$ = this.tSesion.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    })

    this.tUsuario$ = this.tSesion.tUsuarioActual$.subscribe((tUsuario: Usuario) => {

      this.tUsuarioActual = tUsuario;

    })

    var tTienePermiso: boolean = this.tSesion.ObtenerPermisoAcceso(this.tCodFormularioActual);
    this.tPermisoAgregar = this.tSesion.ObtenerPermisoAgregar();
    this.tPermisoEditar = this.tSesion.ObtenerPermisoEditar();
    this.tPermisoEliminar = this.tSesion.ObtenerPermisoEliminar();

    if (tTienePermiso == true) {

      this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
        .subscribe((tHayConfig: boolean) => {

          if (tHayConfig === true) {

            this.Buscar_Condiciones();
            this.Nuevo();

          }

        });

    }
    else {

      this.tMsjServices.AbrirModalMsj(environment.msjSinAcceso);
      this.router.navigate(['/Home']);

    }


  }

  ngOnDestroy() {

    this.tEmpresa$.unsubscribe();
    this.tUsuario$.unsubscribe();
    this.tConfiguracion$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();

  }

  //#region "Funciones de busqueda"

  Buscar_Condiciones(): void {

    this.Ws
      .Condicion()
      .Consumir_Obtener_Condicion(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa)
      .subscribe(Respuesta => {

        this.tCondiciones = [];

        if (Respuesta.Resultado == "N") {

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tCondiciones = Respuesta.Datos;
          this.tFiltroActual = this.tFiltros[0];

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  //#region "CRUD"

  Nueva_Condicion(): void {

    var tNuevaCondicion: CondicionEntrada;
    tNuevaCondicion = {

      "Id": 0,
      "Corporacion": this.tEmpresaActual.Corporacion,
      "Empresa": this.tEmpresaActual.Empresa,
      "Codcondicion": "",
      "Descripcion": this.tCondicionActual.Descripcion,
      "Facturas_contado": this.tCondicionActual.Facturas_contado,
      "Facturas_credito": this.tCondicionActual.Facturas_credito,
      "Limite_credito": this.tCondicionActual.Limite_credito,
      "Limite_facturas_credito": this.tCondicionActual.Limite_facturas_credito,
      "Limite_facturas_vencidas": this.tCondicionActual.Limite_facturas_vencidas,
      "Dias_credito": this.tCondicionActual.Dias_credito,
      "Usuariocreador": this.tUsuarioActual.Codusuario,
      "Usuariomodificador": this.tUsuarioActual.Codusuario

    }

    this.Ws
      .Condicion()
      .Consumir_Crear_Condicion(tNuevaCondicion)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.toastr.success('La condiciòn se ha creado de manera exitosa!')
          this.tFiltroActual = this.tFiltros[0];
          this.Nuevo();
          this.Buscar_Condiciones();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Modificar_Condicion(): void {

    var tModificarCondicion: CondicionEntrada;
    tModificarCondicion = {
      "Id": 0,
      "Corporacion": this.tEmpresaActual.Corporacion,
      "Empresa": this.tEmpresaActual.Empresa,
      "Codcondicion": this.tCondicionActual.Codcondicion,
      "Descripcion": this.tCondicionActual.Descripcion,
      "Facturas_contado": this.tCondicionActual.Facturas_contado,
      "Facturas_credito": this.tCondicionActual.Facturas_credito,
      "Limite_credito": this.tCondicionActual.Limite_credito,
      "Limite_facturas_credito": this.tCondicionActual.Limite_facturas_credito,
      "Limite_facturas_vencidas": this.tCondicionActual.Limite_facturas_vencidas,
      "Dias_credito": this.tCondicionActual.Dias_credito,
      "Usuariocreador": this.tUsuarioActual.Codusuario,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }

    this.Ws
      .Condicion()
      .Consumir_Modificar_Condicion(tModificarCondicion)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.toastr.success('La condiciòn se ha modificado de manera exitosa!')
          this.tFiltroActual = this.tFiltros[0];
          this.Nuevo();
          this.Buscar_Condiciones();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Eliminar_Condicion(): void {

    var tEliminarCondicion: CondicionEntrada;
    tEliminarCondicion = {
      "Id": 0,
      "Corporacion": this.tEmpresaActual.Corporacion,
      "Empresa": this.tEmpresaActual.Empresa,
      "Codcondicion": this.tCondicionActual.Codcondicion,
      "Descripcion": this.tCondicionActual.Descripcion,
      "Facturas_contado": this.tCondicionActual.Facturas_contado,
      "Facturas_credito": this.tCondicionActual.Facturas_credito,
      "Limite_credito": this.tCondicionActual.Limite_credito,
      "Limite_facturas_credito": this.tCondicionActual.Limite_facturas_credito,
      "Limite_facturas_vencidas": this.tCondicionActual.Limite_facturas_vencidas,
      "Dias_credito": this.tCondicionActual.Dias_credito,
      "Usuariocreador": this.tUsuarioActual.Codusuario,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }

    this.Ws
      .Condicion()
      .Consumir_Eliminar_Condicion(tEliminarCondicion)
      .subscribe(Respuesta => {


        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {
          this.toastr.success('La condiciòn se ha eliminado de manera exitosa!')
          this.tFiltroActual = this.tFiltros[0];
          this.Nuevo();
          this.Buscar_Condiciones();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  Nuevo(): void {

    this.tHabilitarGuardar = this.tPermisoAgregar;
    this.tNuevo = true;

    this.tCondicionActual = new Condicion();

    this.toastr.info('Por favor llene los datos necesarios para registrar una nueva Condiciòn!');

  }

  Seleccionar_Condicion(tCondicion_Sel: Condicion, tPos: number): void {

    // this.tControlArchio.nativeElement.value = "";
    this.tHabilitarGuardar = this.tPermisoEditar;
    this.tNuevo = false;
    this.tPosicion = tPos;
    this.tCondicionActual = new Condicion();
    Object.assign(this.tCondicionActual, tCondicion_Sel);
    this.tCondicionActual.Usuariomodificador = this.tUsuarioActual.Usuariomodificador;

  }

  Seleccionar_Condicion_Eliminar(tCondicion_Sel: Condicion): void {

    // this.tControlArchio.nativeElement.value = "";
    this.tCondicionActual = new Condicion();
    Object.assign(this.tCondicionActual, tCondicion_Sel);
    this.tCondicionActual.Usuariomodificador = this.tUsuarioActual.Usuariomodificador;

    this.tConfServices
      .AbrirModalConf(this.tConfiguracionFormulario.ModalConfirmacion.Eliminacion)
      .then(Resultado => {

        switch (Resultado) {
          case "S": {

            this.Eliminar_Condicion();
            break;

          }
        }

      });

  }

  Cancelar(): void {

    this.Nuevo();

  }

  ValidarFormulario(): boolean {

    let valido: boolean = false;

    if (this.tCondicionActual.Descripcion == "") {

      this.toastr.warning('Debe ingresar una descripciòn!', 'Disculpe!');
      return valido;

    }

    if (this.tCondicionActual.Facturas_credito === "S") {

      if (this.tCondicionActual.Limite_credito <= 0) {

        this.toastr.warning('Debe indicar un limite de crédito debe ser un monto mayor a 0', 'Disculpe!');
        return valido;

      }

      if (this.tCondicionActual.Limite_facturas_credito <= 0) {

        this.toastr.warning('El limite de facturas a credito debe ser una cantidad mayor a 0', 'Disculpe!');
        return valido;

      }

      if (this.tCondicionActual.Limite_facturas_vencidas <= 0) {

        this.toastr.warning('El limite de facturas vencidas debe ser una cantidad mayor a 0', 'Disculpe!');
        return valido;

      }

    }

    return true;
  }

  Agregar(): void {

    if (this.ValidarFormulario() == false) {

      return;

    }

    switch (this.tNuevo) {
      case true: {

        this.tConfServices
          .AbrirModalConf(this.tConfiguracionFormulario.ModalConfirmacion.Agregar)
          .then(Resultado => {

            switch (Resultado) {
              case "S": {

                this.Nueva_Condicion();
                break;

              }
            }

          });
        break;

      }
      default: {

        this.tConfServices
          .AbrirModalConf(this.tConfiguracionFormulario.ModalConfirmacion.Edicion)
          .then(Resultado => {

            switch (Resultado) {
              case "S": {

                this.Modificar_Condicion();
                break;

              }
            }

          });
        break;

      }
    }

  }

}
