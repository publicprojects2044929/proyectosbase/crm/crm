import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwCondicionPage } from './fw-condicion.page';

describe('FwCondicionPage', () => {
  let component: FwCondicionPage;
  let fixture: ComponentFixture<FwCondicionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwCondicionPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwCondicionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
