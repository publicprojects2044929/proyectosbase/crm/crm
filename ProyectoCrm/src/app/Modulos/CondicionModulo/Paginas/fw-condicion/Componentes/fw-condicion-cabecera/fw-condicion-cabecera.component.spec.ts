import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwCondicionCabeceraComponent } from './fw-condicion-cabecera.component';

describe('FwCondicionCabeceraComponent', () => {
  let component: FwCondicionCabeceraComponent;
  let fixture: ComponentFixture<FwCondicionCabeceraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwCondicionCabeceraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwCondicionCabeceraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
