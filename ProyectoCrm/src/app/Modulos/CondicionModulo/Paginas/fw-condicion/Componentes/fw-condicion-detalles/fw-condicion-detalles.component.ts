import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Condicion } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-condicion-detalles',
  templateUrl: './fw-condicion-detalles.component.html',
  styleUrls: ['./fw-condicion-detalles.component.scss']
})
export class FwCondicionDetallesComponent implements OnInit, OnChanges {

  @Input('Condicion') tCondicion: Condicion = new Condicion();

  tBloquearLimiteCred: boolean = false;
  tBloquearLimiteFactCred: boolean = false;
  tBloquearLimiteFactVenc: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges){

    if('tCondicion' in changes){

      this.SeleccionarCredito();

    }

  }

  SeleccionarCredito() {

    this.tBloquearLimiteCred = false;
    this.tBloquearLimiteFactCred = false;
    this.tBloquearLimiteFactVenc = false;

    if (this.tCondicion.Facturas_credito === "N") {

      this.tBloquearLimiteCred = true;
      this.tBloquearLimiteFactCred = true;
      this.tBloquearLimiteFactVenc = true;

    }

  }

}
