import { Component, OnInit, Input } from '@angular/core';
import { Condicion } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-condicion-cabecera',
  templateUrl: './fw-condicion-cabecera.component.html',
  styleUrls: ['./fw-condicion-cabecera.component.scss']
})
export class FwCondicionCabeceraComponent implements OnInit {

  @Input('Condicion') tCondicion: Condicion = new Condicion();

  constructor() { }

  ngOnInit(): void {
  }

}
