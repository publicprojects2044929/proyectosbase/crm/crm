import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwCondicionDetallesComponent } from './fw-condicion-detalles.component';

describe('FwCondicionDetallesComponent', () => {
  let component: FwCondicionDetallesComponent;
  let fixture: ComponentFixture<FwCondicionDetallesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwCondicionDetallesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwCondicionDetallesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
