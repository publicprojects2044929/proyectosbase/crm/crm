import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from 'src/app/Shared/shared.module';

import { PaisRoutingModule } from './pais-routing.module';
import { FwPaisPage } from './Paginas/fw-pais/fw-pais.page';
import { FwPaisCabeceraComponent } from './Paginas/fw-pais/Componentes/fw-pais-cabecera/fw-pais-cabecera.component';
import { FwPaisDetallesComponent } from './Paginas/fw-pais/Componentes/fw-pais-detalles/fw-pais-detalles.component';


@NgModule({
  declarations: [FwPaisPage, FwPaisCabeceraComponent, FwPaisDetallesComponent],
  imports: [
    CommonModule,
    PaisRoutingModule,
    SharedModule,
    FormsModule,
    NgbModule
  ]
})
export class PaisModule { }
