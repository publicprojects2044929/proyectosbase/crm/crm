import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwPaisPage } from './fw-pais.page';

describe('FwPaisPage', () => {
  let component: FwPaisPage;
  let fixture: ComponentFixture<FwPaisPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwPaisPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwPaisPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
