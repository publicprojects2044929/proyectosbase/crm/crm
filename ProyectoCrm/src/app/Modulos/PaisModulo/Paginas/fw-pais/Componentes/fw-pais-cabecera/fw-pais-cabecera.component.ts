import { Component, OnInit, Input } from '@angular/core';
import { Pais } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-pais-cabecera',
  templateUrl: './fw-pais-cabecera.component.html',
  styleUrls: ['./fw-pais-cabecera.component.scss']
})
export class FwPaisCabeceraComponent implements OnInit {

  @Input('Pais') tPais: Pais = new Pais();

  constructor() { }

  ngOnInit(): void {
  }

}
