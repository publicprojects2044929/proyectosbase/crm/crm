import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwPaisCabeceraComponent } from './fw-pais-cabecera.component';

describe('FwPaisCabeceraComponent', () => {
  let component: FwPaisCabeceraComponent;
  let fixture: ComponentFixture<FwPaisCabeceraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwPaisCabeceraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwPaisCabeceraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
