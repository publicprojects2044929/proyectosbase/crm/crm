import { Component, OnInit, Input } from '@angular/core';
import { Pais } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-pais-detalles',
  templateUrl: './fw-pais-detalles.component.html',
  styleUrls: ['./fw-pais-detalles.component.scss']
})
export class FwPaisDetallesComponent implements OnInit {

  @Input('Pais') tPais: Pais = new Pais();

  constructor() { }

  ngOnInit(): void {
  }

}
