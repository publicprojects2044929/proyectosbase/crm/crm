import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwPaisDetallesComponent } from './fw-pais-detalles.component';

describe('FwPaisDetallesComponent', () => {
  let component: FwPaisDetallesComponent;
  let fixture: ComponentFixture<FwPaisDetallesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwPaisDetallesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwPaisDetallesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
