import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwMonedasPage } from './fw-monedas.page';

describe('FwMonedasPage', () => {
  let component: FwMonedasPage;
  let fixture: ComponentFixture<FwMonedasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwMonedasPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwMonedasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
