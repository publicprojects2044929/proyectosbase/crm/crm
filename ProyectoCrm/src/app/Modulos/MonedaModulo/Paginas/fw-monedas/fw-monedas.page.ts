import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormularioConfiguracion } from 'src/app/Clases/EstructuraConfiguracion';
import { Usuario, Empresas, Moneda, Formulario, Filtros, MonedaDenominacion } from 'src/app/Clases/Estructura';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/Core/Session/session.service';
import { ToastrService } from 'ngx-toastr';
import { ErroresService } from 'src/app/Modales/mensajes/fw-modal-msj/errores.service';
import { MensajesService } from 'src/app/Modales/mensajes/fw-modal-msj/mensajes.service';
import { ConfirmacionService } from 'src/app/Modales/confirmacion/fw-modal-confirmacion/confirmacion.service';
import { environment } from 'src/environments/environment';
import { MonedaEntrada, MonedaDenominacionEntrada } from 'src/app/Clases/EstructuraEntrada';
import { Subscription } from 'rxjs';
import { ConfiguracionService } from 'src/app/Core/Configuracion/configuracion.service';

@Component({
  selector: 'app-fw-monedas',
  templateUrl: './fw-monedas.page.html',
  styleUrls: ['./fw-monedas.page.scss']
})
export class FwMonedasPage implements OnInit, OnDestroy {

  tConfiguracionFormulario: FormularioConfiguracion = new FormularioConfiguracion();
  tCodFormularioActual: string = "fMoneda";
  tPermisoAgregar: boolean = false;
  tPermisoEditar: boolean = false;
  tPermisoEliminar: boolean = false;
  tHabilitarGuardar: boolean = false;

  tUsuarioActual: Usuario = new Usuario();
  tEmpresaActual: Empresas = new Empresas();
  tEmpresa$: Subscription;
  tUsuario$: Subscription;

  tTitulo: string = "";
  tNuevo: boolean = true;
  tEditar: boolean = false
  tBloquearBuscar: boolean = false;
  tPosicion: number = 0;
  tPagina: number = 1;
  tTamanoPag: number = 5;

  tMonedas: Moneda[] = [];
  tMonedaActual: Moneda = new Moneda();

  tFormularios: Formulario[] = [];

  tFiltros: Filtros[] = [];
  tFiltroActual: Filtros = new Filtros();
  tFiltroBusqueda: string = "";

  tConfiguracion$: Subscription;
  tHayConfiguracionWs$: Subscription;
  tStrConfiguracion: string = 'Moneda/Moneda.Config.json';

  constructor(public Ws: ClsServiciosService,
    private router: Router,
    private tSesion: SessionService,
    private tConfiguracionService: ConfiguracionService,
    private toastr: ToastrService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService,
    private tConfServices: ConfirmacionService) {

    this.tFiltroBusqueda = "";
    this.tPosicion = 0;

    this.tFiltros = [

      { Codfiltro: "Codmoneda", Filtro: "Código" },
      { Codfiltro: "Descripcion", Filtro: "Descripción" },

    ]

  }

  ngOnInit() {

    this.tConfiguracion$ = this.tConfiguracionService
      .Consumir_Obtener_Configuracion(this.tStrConfiguracion)
      .subscribe((tConfiguracion: FormularioConfiguracion) => {

        this.tConfiguracionFormulario = tConfiguracion;
        this.tTitulo = this.tConfiguracionFormulario.Titulo;
        this.tErroresServices.tTituloMsj = this.tConfiguracionFormulario.ModalMensaje.Titulo;
        this.tMsjServices.tTituloMsj = this.tConfiguracionFormulario.ModalMensaje.Titulo;
        this.tConfServices.tTituloConf = this.tConfiguracionFormulario.ModalConfirmacion.Titulo;


      });

    this.tEmpresa$ = this.tSesion.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    })

    this.tUsuario$ = this.tSesion.tUsuarioActual$.subscribe((tUsuario: Usuario) => {

      this.tUsuarioActual = tUsuario;

    })

    var tTienePermiso: boolean = this.tSesion.ObtenerPermisoAcceso(this.tCodFormularioActual);
    this.tPermisoAgregar = this.tSesion.ObtenerPermisoAgregar();
    this.tPermisoEditar = this.tSesion.ObtenerPermisoEditar();
    this.tPermisoEliminar = this.tSesion.ObtenerPermisoEliminar();

    if (tTienePermiso == true) {

      this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
        .subscribe((tHayConfig: boolean) => {

          if (tHayConfig === true) {

            this.Buscar_Monedas();
            this.Nuevo();

          }

        });

    }
    else {

      this.tMsjServices.AbrirModalMsj(environment.msjSinAcceso);
      this.router.navigate(['/Home']);

    }

  }

  ngOnDestroy() {

    this.tEmpresa$.unsubscribe();
    this.tUsuario$.unsubscribe();
    this.tConfiguracion$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();

  }

  //#region "Funciones de busqueda"

  Buscar_Monedas(): void {

    this.Ws
      .Monedas()
      .Consumir_Obtener_MonedaFull(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa)
      .subscribe(Respuesta => {

        this.tMonedas = [];

        if (Respuesta.Resultado == "N") {

          // this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);
          //

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tMonedas = Respuesta.Datos;
          this.tFiltroActual = this.tFiltros[0];

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  //#region "CRUD"

  Nueva_Moneda(): void {

    var tNuevaMoneda: MonedaEntrada;
    tNuevaMoneda = {
      "Id": 0,
      "Corporacion": this.tEmpresaActual.Corporacion,
      "Empresa": this.tEmpresaActual.Empresa,
      "Codmoneda": "",
      "Descripcion": this.tMonedaActual.Descripcion,
      "Simbolo": this.tMonedaActual.Simbolo,
      "Codigoiso": this.tMonedaActual.Codigoiso,
      "Principal": this.tMonedaActual.Principal,
      "LDenominaciones": [],
      "Usuariocreador": this.tUsuarioActual.Codusuario,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }

    for (let tDenominacion of this.tMonedaActual.LDenominaciones) {

      var tNuevaDenominacion: MonedaDenominacionEntrada = new MonedaDenominacionEntrada();
      tNuevaDenominacion = {

        "Id": 0,
        "Corporacion": this.tEmpresaActual.Corporacion,
        "Empresa": this.tEmpresaActual.Empresa,
        "Codmoneda": "",
        "Coddenominacion": "",
        "Descripcion": tDenominacion.Descripcion,
        "Factor": tDenominacion.Factor,
        "Nuevo": "S",
        "Usuariocreador": this.tUsuarioActual.Codusuario,
        "Usuariomodificador": this.tUsuarioActual.Codusuario
      }

      tNuevaMoneda.LDenominaciones.push(tNuevaDenominacion);

    }

    this.Ws
      .Monedas()
      .Consumir_Crear_Moneda(tNuevaMoneda)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.toastr.clear();
          this.toastr.success('La moneda se ha creado de manera exitosa!');
          this.tFiltroActual = this.tFiltros[0];
          this.Nuevo();
          this.Buscar_Monedas();

        }

      }, error => {


        this.tErroresServices.MostrarError(error);

      });

  }

  Modificar_Moneda(): void {

    var tModificarMoneda: MonedaEntrada;
    tModificarMoneda = {
      "Id": 0,
      "Corporacion": this.tMonedaActual.Corporacion,
      "Empresa": this.tMonedaActual.Empresa,
      "Codmoneda": this.tMonedaActual.Codmoneda,
      "Descripcion": this.tMonedaActual.Descripcion,
      "Simbolo": this.tMonedaActual.Simbolo,
      "Codigoiso": this.tMonedaActual.Codigoiso,
      "Principal": this.tMonedaActual.Principal,
      "LDenominaciones": [],
      "Usuariocreador": this.tMonedaActual.Usuariocreador,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }

    for (let tDenominacion of this.tMonedaActual.LDenominaciones) {

      var tModificarDenominacion: MonedaDenominacionEntrada = new MonedaDenominacionEntrada();
      tModificarDenominacion = {

        "Id": 0,
        "Corporacion": this.tEmpresaActual.Corporacion,
        "Empresa": this.tEmpresaActual.Empresa,
        "Codmoneda": this.tMonedaActual.Codmoneda,
        "Coddenominacion": tDenominacion.Coddenominacion,
        "Descripcion": tDenominacion.Descripcion,
        "Factor": tDenominacion.Factor,
        "Nuevo": tDenominacion.Nuevo,
        "Usuariocreador": tDenominacion.Usuariocreador,
        "Usuariomodificador": this.tUsuarioActual.Codusuario

      }

      tModificarMoneda.LDenominaciones.push(tModificarDenominacion);

    }

    this.Ws
      .Monedas()
      .Consumir_Modificar_Moneda(tModificarMoneda)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.toastr.clear();
          this.toastr.success('La moneda se ha modificado de manera exitosa!');
          this.tFiltroActual = this.tFiltros[0];
          this.Buscar_Monedas();
          this.Nuevo();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Eliminar_Moneda(): void {

    var tEliminarMoneda: MonedaEntrada;
    tEliminarMoneda = {
      "Id": 0,
      "Corporacion": this.tMonedaActual.Corporacion,
      "Empresa": this.tMonedaActual.Empresa,
      "Codmoneda": this.tMonedaActual.Codmoneda,
      "Descripcion": this.tMonedaActual.Descripcion,
      "Simbolo": this.tMonedaActual.Simbolo,
      "Codigoiso": this.tMonedaActual.Codigoiso,
      "Principal": this.tMonedaActual.Principal,
      "LDenominaciones": [],
      "Usuariocreador": this.tMonedaActual.Usuariocreador,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }

    this.Ws
      .Monedas()
      .Consumir_Eliminar_Moneda(tEliminarMoneda)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.toastr.clear();
          this.toastr.success('La moneda se ha eliminado de manera exitosa!');
          this.tFiltroActual = this.tFiltros[0];
          this.Nuevo();
          this.Buscar_Monedas();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  Nuevo(): void {

    this.tHabilitarGuardar = this.tPermisoAgregar;
    this.tNuevo = true;
    this.tMonedaActual = new Moneda();

    this.toastr.info('Por favor llene los datos necesarios para registrar una nueva Moneda!');

  }

  Seleccionar_Moneda(tMoneda_Sel: Moneda, tPos: number): void {

    this.tHabilitarGuardar = this.tPermisoEditar;
    this.tNuevo = false;
    this.tPosicion = tPos;
    this.tMonedaActual = new Moneda();
    Object.assign(this.tMonedaActual, tMoneda_Sel);
    this.tMonedaActual.Usuariomodificador = this.tUsuarioActual.Usuariomodificador;

  }

  Seleccionar_Clasificacion_Eliminar(tMoneda_Sel: Moneda): void {

    this.tMonedaActual = new Moneda();
    Object.assign(this.tMonedaActual, tMoneda_Sel);
    this.tMonedaActual.Usuariomodificador = this.tUsuarioActual.Usuariomodificador;

    this.tConfServices
      .AbrirModalConf(this.tConfiguracionFormulario.ModalConfirmacion.Eliminacion)
      .then(Resultado => {

        switch (Resultado) {
          case "S": {

            this.Eliminar_Moneda();
            break;

          }
        }

      });

  }

  Cancelar(): void {

    this.Nuevo();

  }

  ValidarFormulario(): boolean {

    let valido: boolean = false;

    if (this.tMonedaActual.Descripcion == "") {

      this.toastr.warning('Debe ingresar una descripciòn!', 'Disculpe!');
      return valido;

    }

    if (this.tMonedaActual.Simbolo == "") {

      this.toastr.warning('Debe ingresar un simbolo!', 'Disculpe!');
      return valido;

    }

    if (this.tMonedaActual.Codigoiso == "") {

      this.toastr.warning('Debe ingresar un còdigo ISO!', 'Disculpe!');
      return valido;

    }

    return true;

  }


  Agregar(): void {

    if (this.ValidarFormulario() == false) {

      return;

    }

    switch (this.tNuevo) {
      case true: {

        this.tConfServices
          .AbrirModalConf(this.tConfiguracionFormulario.ModalConfirmacion.Agregar)
          .then(Resultado => {

            switch (Resultado) {
              case "S": {

                this.Nueva_Moneda();
                break;

              }
            }

          });
        break;

      }
      default: {

        this.tConfServices
          .AbrirModalConf(this.tConfiguracionFormulario.ModalConfirmacion.Edicion)
          .then(Resultado => {

            switch (Resultado) {
              case "S": {

                this.Modificar_Moneda();
                break;

              }
            }

          });
        break;

      }
    }

  }

  Eliminar_Denominacion(tDenominacionSel: MonedaDenominacion) {

    var tEliminarDenominacion: MonedaDenominacionEntrada = new MonedaDenominacionEntrada();
    tEliminarDenominacion = {
      "Id": 0,
      "Corporacion": tDenominacionSel.Corporacion,
      "Empresa": tDenominacionSel.Empresa,
      "Codmoneda": tDenominacionSel.Codmoneda,
      "Coddenominacion": tDenominacionSel.Coddenominacion,
      "Descripcion": tDenominacionSel.Descripcion,
      "Factor": tDenominacionSel.Factor,
      "Nuevo": tDenominacionSel.Nuevo,
      "Usuariocreador": tDenominacionSel.Usuariocreador,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }


    this.Ws
      .MonedasDeno()
      .Consumir_Eliminar_MonedaDenominacion(tEliminarDenominacion)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          let tPos: number = this.tMonedaActual.LDenominaciones.indexOf(tDenominacionSel) || -1;
          if(tPos > 0){
            this.tMonedaActual.LDenominaciones.splice(tPos, 1);
          }

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });


  }

}
