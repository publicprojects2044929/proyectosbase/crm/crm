import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwMonedasCabeceraComponent } from './fw-monedas-cabecera.component';

describe('FwMonedasCabeceraComponent', () => {
  let component: FwMonedasCabeceraComponent;
  let fixture: ComponentFixture<FwMonedasCabeceraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwMonedasCabeceraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwMonedasCabeceraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
