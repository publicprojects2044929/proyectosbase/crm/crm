import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MonedaDenominacion } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-monedas-detalles',
  templateUrl: './fw-monedas-detalles.component.html',
  styleUrls: ['./fw-monedas-detalles.component.scss']
})
export class FwMonedasDetallesComponent implements OnInit {

  @Input('Denominaciones') tDenominaciones: MonedaDenominacion[] = [];
  @Input('PermisoEliminar') tPermisoEliminar: boolean = false;

  @Output('Eliminar') tEliminar: EventEmitter<MonedaDenominacion> = new EventEmitter();

  tPosicionDeno: number = 0;
  tPaginaDeno: number = 1;
  tTamanoPagDeno: number = 2;

  constructor() { }

  ngOnInit(): void {
  }

  Agregar_Denominacion() {

    let tNuevaDenominacion: MonedaDenominacion = new MonedaDenominacion();
    this.tDenominaciones.push(tNuevaDenominacion);
    // this.tDenominaciones = [
    //   ...this.tDenominaciones,
    //   tNuevaDenominacion
    // ];

  }

  Eliminar_Denominacion({ DenominacionSel, PosicionSel }) {

    if(DenominacionSel.Nuevo === 'S'){

      this.tDenominaciones.splice(PosicionSel, 1);

    }
    else{

      this.tEliminar.emit(DenominacionSel);

    }

  }

}
