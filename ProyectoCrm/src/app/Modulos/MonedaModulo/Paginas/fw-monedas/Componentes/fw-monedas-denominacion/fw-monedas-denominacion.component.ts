import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MonedaDenominacion } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-monedas-denominacion',
  templateUrl: './fw-monedas-denominacion.component.html',
  styleUrls: ['./fw-monedas-denominacion.component.scss']
})
export class FwMonedasDenominacionComponent implements OnInit {

  @Input('Denominacion') tDenominacion: MonedaDenominacion = new MonedaDenominacion();
  @Input('Posicion') tPosicion: number = 0;
  @Input('PermisoEliminar') tPermisoEliminar: boolean = false;

  @Output('Eliminar') tEliminar: EventEmitter<{DenominacionSel: MonedaDenominacion, PosicionSel: number}> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  Eliminar(){

    let DenominacionSel: MonedaDenominacion = this.tDenominacion;
    let PosicionSel: number = 0;

    this.tEliminar.emit({DenominacionSel, PosicionSel});

  }

}
