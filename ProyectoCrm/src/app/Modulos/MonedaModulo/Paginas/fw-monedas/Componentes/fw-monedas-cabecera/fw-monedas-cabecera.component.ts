import { Component, OnInit, Input } from '@angular/core';
import { Moneda } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-monedas-cabecera',
  templateUrl: './fw-monedas-cabecera.component.html',
  styleUrls: ['./fw-monedas-cabecera.component.scss']
})
export class FwMonedasCabeceraComponent implements OnInit {

  @Input('Moneda') tMoneda: Moneda = new Moneda();

  constructor() { }

  ngOnInit(): void {
  }

  Moneda_Principal(tCheckbox: any): void {

    if (tCheckbox === true) {

      this.tMoneda.Principal = 1;

    }
    else {

      this.tMoneda.Principal = 0;

    }

  }

}
