import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwMonedasDetallesComponent } from './fw-monedas-detalles.component';

describe('FwMonedasDetallesComponent', () => {
  let component: FwMonedasDetallesComponent;
  let fixture: ComponentFixture<FwMonedasDetallesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwMonedasDetallesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwMonedasDetallesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
