import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwMonedasDenominacionComponent } from './fw-monedas-denominacion.component';

describe('FwMonedasDenominacionComponent', () => {
  let component: FwMonedasDenominacionComponent;
  let fixture: ComponentFixture<FwMonedasDenominacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwMonedasDenominacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwMonedasDenominacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
