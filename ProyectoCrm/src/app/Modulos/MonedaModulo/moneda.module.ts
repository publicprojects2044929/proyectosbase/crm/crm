import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from 'src/app/Shared/shared.module'

import { MonedaRoutingModule } from './moneda-routing.module';
import { FwMonedasPage } from './Paginas/fw-monedas/fw-monedas.page';
import { FwMonedasCabeceraComponent } from './Paginas/fw-monedas/Componentes/fw-monedas-cabecera/fw-monedas-cabecera.component';
import { FwMonedasDetallesComponent } from './Paginas/fw-monedas/Componentes/fw-monedas-detalles/fw-monedas-detalles.component';
import { FwMonedasDenominacionComponent } from './Paginas/fw-monedas/Componentes/fw-monedas-denominacion/fw-monedas-denominacion.component';


@NgModule({
  declarations: [FwMonedasPage, FwMonedasCabeceraComponent, FwMonedasDetallesComponent, FwMonedasDenominacionComponent],
  imports: [
    CommonModule,
    MonedaRoutingModule,
    SharedModule,
    FormsModule,
    NgbModule
  ]
})
export class MonedaModule { }
