import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from 'src/app/Shared/shared.module';

import { ArticuloRoutingModule } from './articulo-routing.module';
import { FwArticuloPage } from './Paginas/fw-articulo/fw-articulo.page';
import { FwArticuloCabeceraComponent } from './Paginas/fw-articulo/Componentes/fw-articulo-cabecera/fw-articulo-cabecera.component';
import { FwArticuloDetallesInformacionComponent } from './Paginas/fw-articulo/Componentes/fw-articulo-detalles-informacion/fw-articulo-detalles-informacion.component';
import { FwArticuloDetallesClasificacionComponent } from './Paginas/fw-articulo/Componentes/fw-articulo-detalles-clasificacion/fw-articulo-detalles-clasificacion.component';
import { FwArticuloDetallesImagenesComponent } from './Paginas/fw-articulo/Componentes/fw-articulo-detalles-imagenes/fw-articulo-detalles-imagenes.component';

@NgModule({
  declarations: [FwArticuloPage, FwArticuloCabeceraComponent, FwArticuloDetallesInformacionComponent, FwArticuloDetallesClasificacionComponent, FwArticuloDetallesImagenesComponent],
  imports: [
    CommonModule,
    ArticuloRoutingModule,
    SharedModule,
    FormsModule,
    NgbModule
  ]
})
export class ArticuloModule { }
