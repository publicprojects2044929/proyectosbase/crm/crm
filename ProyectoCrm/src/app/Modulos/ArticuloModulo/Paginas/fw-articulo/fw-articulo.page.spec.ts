import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwArticuloPage } from './fw-articulo.page';

describe('FwArticuloPage', () => {
  let component: FwArticuloPage;
  let fixture: ComponentFixture<FwArticuloPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwArticuloPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwArticuloPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
