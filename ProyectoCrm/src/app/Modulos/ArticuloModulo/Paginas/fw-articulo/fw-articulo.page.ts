import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormularioConfiguracion } from 'src/app/Clases/EstructuraConfiguracion';
import { ManejarImagenes } from 'src/app/Manejadores/cls-procedimientos';
import { Usuario, Empresas, Articulo, Tipo, Color, UnidadMedida, Clasificacion1,  Fabricante, Configuracion, ArticuloImagenes } from 'src/app/Clases/Estructura';
import { Router } from '@angular/router';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { SessionService } from 'src/app/Core/Session/session.service';
import { ToastrService } from 'ngx-toastr';
import { ErroresService } from 'src/app/Modales/mensajes/fw-modal-msj/errores.service';
import { MensajesService } from 'src/app/Modales/mensajes/fw-modal-msj/mensajes.service';
import { ConfirmacionService } from 'src/app/Modales/confirmacion/fw-modal-confirmacion/confirmacion.service';
import { ArticulosService } from 'src/app/Modales/articulo/fw-modal-articulos/articulos.service';
import { environment } from 'src/environments/environment';
import { ArticuloEntrada, ArticuloImagenEntrada } from 'src/app/Clases/EstructuraEntrada';
import { Subscription } from 'rxjs';
import { ConfiguracionService } from 'src/app/Core/Configuracion/configuracion.service';

@Component({
  selector: 'app-fw-articulo',
  templateUrl: './fw-articulo.page.html',
  styleUrls: ['./fw-articulo.page.scss']
})
export class FwArticuloPage implements OnInit, OnDestroy {

  @ViewChild('DetallesInfo', { static: true }) tDetallesInfo: any;
  @ViewChild('DetallesClas', { static: true }) tDetallesClas: any;

  tManejarImagen: ManejarImagenes = new ManejarImagenes();
  tConfiguracionFormulario: FormularioConfiguracion = new FormularioConfiguracion();
  tCodFormularioActual: string = "fArticulos";
  tPermisoAgregar: boolean = false;
  tPermisoEditar: boolean = false;
  tPermisoEliminar: boolean = false;
  tHabilitarGuardar: boolean = false;
  tHabilitarEliminar: boolean = false;
  tVprecio1: boolean = false;
  tVprecio2: boolean = false;
  tVprecio3: boolean = false;
  tVprecioLista: boolean = false;

  tUsuarioActual: Usuario = new Usuario();
  tEmpresaActual: Empresas = new Empresas();
  tEmpresa$: Subscription;
  tUsuario$: Subscription;

  tTitulo: string = "";
  tNuevo: boolean = true;
  tEditar: boolean = false
  tBloquearBuscar: boolean = false;
  tPosicion: number = 0;
  tPagina: number = 1;
  tTamanoPag: number = 5;

  tArticuloActual: Articulo = new Articulo();

  tTipos: Tipo[] = [];
  tTiposImagenes: Tipo[] = [];

  tColores: Color[] = [];

  tUnidades: UnidadMedida[] = [];
  tClasificaciones1: Clasificacion1[] = [];

  tFabricantes: Fabricante[] = [];

  tConfiguracion: Configuracion = new Configuracion();

  tConfiguracion$: Subscription;
  tHayConfiguracionWs$: Subscription;
  tStrConfiguracion: string = 'Articulo/Articulo.Config.json';

  constructor(
    public router: Router,
    public Ws: ClsServiciosService,
    private tSesion: SessionService,
    private tConfiguracionService: ConfiguracionService,
    private toastr: ToastrService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService,
    private tConfServices: ConfirmacionService,
    private tArticuloService: ArticulosService
  ) {

    this.tPosicion = 0;

    this.tTipos = [

      {
        "Id": 0,
        "Corporacion": "",
        "Empresa": "",
        "Codtipo": "",
        "Descripcion": "",
        "Tabla": "",
        "Abreviacion": "",
        "Columna": "",
        "Hexadecimal": "",
        "Fecha_creacion": new Date(),
        "Fecha_creacionl": 0,
        "Fecha_modificacion": new Date(),
        "Fecha_modificacionl": 0,
        "Usuariocreador": "",
        "Usuariomodificador": ""
      }

    ];

  }

  ngOnInit() {

    this.tConfiguracion$ = this.tConfiguracionService
      .Consumir_Obtener_Configuracion(this.tStrConfiguracion)
      .subscribe((tConfiguracion: FormularioConfiguracion) => {

        this.tConfiguracionFormulario = tConfiguracion;
        this.tTitulo = this.tConfiguracionFormulario.Titulo;
        this.tConfServices.tTituloConf = this.tConfiguracionFormulario.ModalConfirmacion.Titulo;
        this.tErroresServices.tTituloMsj = this.tConfiguracionFormulario.ModalMensaje.Titulo;
        this.tMsjServices.tTituloMsj = this.tConfiguracionFormulario.ModalMensaje.Titulo;

      });

    this.tEmpresa$ = this.tSesion.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    })

    this.tUsuario$ = this.tSesion.tUsuarioActual$.subscribe((tUsuario: Usuario) => {

      this.tUsuarioActual = tUsuario;

    })

    var tTienePermiso: boolean = this.tSesion.ObtenerPermisoAcceso(this.tCodFormularioActual);
    this.tPermisoAgregar = this.tSesion.ObtenerPermisoAgregar();
    this.tPermisoEditar = this.tSesion.ObtenerPermisoEditar();
    this.tPermisoEliminar = this.tSesion.ObtenerPermisoEliminar();

    if (tTienePermiso == true) {

      this.tVprecio1 = this.tSesion.ObtenerTareaAdicional('vPrecio1');
      this.tVprecio2 = this.tSesion.ObtenerTareaAdicional('vPrecio2');
      this.tVprecio3 = this.tSesion.ObtenerTareaAdicional('vPrecio3');
      this.tVprecioLista = this.tSesion.ObtenerTareaAdicional('vPrecioLista');

      this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
        .subscribe((tHayConfig: boolean) => {

          if (tHayConfig === true) {

            this.BuscarConfiguracion();
            this.BuscarTipo();
            this.BuscarTipoImagenes();
            this.BuscarColor();
            this.BuscarUnidades();
            this.BuscarClasificaciones();
            this.BuscarFabricantes();
            this.Nuevo();

          }

        });

    }
    else {

      this.tMsjServices.AbrirModalMsj(environment.msjSinAcceso);
      this.router.navigate(['/Home']);

    }

  }

  ngOnDestroy() {

    this.tEmpresa$.unsubscribe();
    this.tUsuario$.unsubscribe();
    this.tConfiguracion$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();

  }

  AbrirModalArticulos(): void {

    this.tArticuloService.AbrirModal().then(Resultado => {

      if (Resultado.Resultado == "S") {

        var tArticuloModal: Articulo = Resultado.Articulo;
        this.BuscarArticulo(tArticuloModal.Codarticulo);

      }

    });

  }

  //#region "Funciones de busqueda"

  BuscarConfiguracion(): void {

    this.Ws
      .Configuraciones()
      .Consumir_Obtener_Configuracion(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        'MR_ARTICULO',
        'IMAGEN')
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tConfiguracion = Respuesta.Datos[0];

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  BuscarTipo(): void {

    this.Ws
      .Tipo()
      .Consumir_Obtener_Tipo(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        'MR_ARTICULO')
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tTipos = Respuesta.Datos;

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  BuscarTipoImagenes(): void {

    this.Ws
      .Tipo()
      .Consumir_Obtener_Tipo(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        'MR_ARTICULO_IMAGENES')
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tTiposImagenes = Respuesta.Datos;

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  BuscarColor(): void {

    this.Ws.Color()
      .Consumir_Obtener_Colores(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tColores = Respuesta.Datos;

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  BuscarUnidades(): void {

    this.Ws.Unidad()
      .Consumir_Obtener_UnidadesMedida(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa)
      .subscribe(Respuesta => {


        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tUnidades = Respuesta.Datos;

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  BuscarClasificaciones(): void {

    this.Ws.Clasificacion1()
      .Consumir_Obtener_Clasificacion1Full4(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tClasificaciones1 = Respuesta.Datos;

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  BuscarFabricantes(): void {

    this.Ws.Fabricante()
      .Consumir_Obtener_FabricanteFull(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa)
      .subscribe(Respuesta => {

        this.tFabricantes = [];

        if (Respuesta.Resultado == "N") {

          // this.tMensajeMsj = Respuesta.Mensaje;
          // this.AbrirModalMsj();

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tFabricantes = Respuesta.Datos;

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  BuscarArticulo(tCodarticulo: string): void {

    this.Ws.Articulo()
      .Consumir_Obtener_ArticuloDetallado(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        tCodarticulo)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          var tArticulo: Articulo = Respuesta.Datos[0];
          this.tEditar = true;
          this.tNuevo = false;
          this.tHabilitarGuardar = this.tPermisoEditar;
          this.tHabilitarEliminar = this.tPermisoEliminar;
          this.tBloquearBuscar = true;
          this.tArticuloActual = tArticulo;

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  //#region "CRUD"

  Nuevo_Articulo(): void {

    var tNuevoArticulo: ArticuloEntrada;
    tNuevoArticulo = {
      "Id": 0,
      "Corporacion": this.tEmpresaActual.Corporacion,
      "Empresa": this.tEmpresaActual.Empresa,
      "Codarticulo": "",
      "Sku": this.tArticuloActual.Sku,
      "Codalterno": this.tArticuloActual.Codalterno,
      "Referencia": this.tArticuloActual.Referencia,
      "Descripcion": this.tArticuloActual.Descripcion,
      "Descripcion2": this.tArticuloActual.Descripcion2,
      "Tipo": this.tArticuloActual.Tipo,
      "Codfabricante": this.tArticuloActual.Codfabricante,
      "Codmarca": this.tArticuloActual.Codmarca,
      "Codmodelo": this.tArticuloActual.Codmodelo,
      "Codclasificacion1": this.tArticuloActual.Codclasificacion1,
      "Codclasificacion2": this.tArticuloActual.Codclasificacion2,
      "Codclasificacion3": this.tArticuloActual.Codclasificacion3,
      "Codclasificacion4": this.tArticuloActual.Codclasificacion4,
      "Codcolor": this.tArticuloActual.Codcolor,
      "Codunidadmedida": this.tArticuloActual.Codunidadmedida,
      "Url": this.tArticuloActual.Url,
      "Iva": this.tArticuloActual.Iva,
      "Costo": this.tArticuloActual.Costo,
      "Precio1": this.tArticuloActual.Precio1,
      "Precio2": this.tArticuloActual.Precio2,
      "Precio3": this.tArticuloActual.Precio3,
      "Preciolista": this.tArticuloActual.Preciolista,
      "Peso": this.tArticuloActual.Peso,
      "Alto": this.tArticuloActual.Alto,
      "Ancho": this.tArticuloActual.Ancho,
      "Largo": this.tArticuloActual.Largo,
      "Volumen": this.tArticuloActual.Volumen,
      "Maneja_lotes": this.tArticuloActual.Maneja_lotes,
      "Maneja_seriales": this.tArticuloActual.Maneja_seriales,
      "Maneja_tallas": this.tArticuloActual.Maneja_tallas,
      "Talla_inicial": this.tArticuloActual.Talla_inicial,
      "Talla_final": this.tArticuloActual.Talla_final,
      "Talla_intermedias": this.tArticuloActual.Talla_intermedias,
      "Talla_sugerida": this.tArticuloActual.Talla_sugerida,
      "Maneja_vencimiento": this.tArticuloActual.Maneja_vencimiento,
      "Dias_vencimiento_d": this.tArticuloActual.Dias_vencimiento_d,
      "Dias_vencimiento_r": this.tArticuloActual.Dias_vencimiento_r,
      "LImagenes": [],
      "Usuariocreador": this.tUsuarioActual.Codusuario,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }

    this.tArticuloActual.LImagenes.forEach(tImagen => {

      if (tImagen.Nuevo == "S") {

        let tNuevoArticuloImagenEntrada: ArticuloImagenEntrada = {

          "Id": 0,
          "Corporacion": this.tEmpresaActual.Corporacion,
          "Empresa": this.tEmpresaActual.Empresa,
          "Codarticulo": this.tArticuloActual.Codarticulo,
          "Principal": tImagen.Principal,
          "Nuevo": tImagen.Nuevo,
          "Url": tImagen.Url,
          "Imagen64": this.tManejarImagen.obtenerImagenes64(tImagen.Url),
          "Tipo": tImagen.Tipo,
          "Orden": tImagen.Orden,
          "Usuariocreador": tImagen.Usuariocreador,
          "Usuariomodificador": tImagen.Usuariomodificador

        }

        tNuevoArticulo.LImagenes.push(tNuevoArticuloImagenEntrada);

      }

    });

    this.Ws
      .Articulo()
      .Consumir_Crear_Articulo(tNuevoArticulo)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tErroresServices.MostrarError(environment.msjError);

        }
        else {

          this.toastr.success('El artículo se ha creado de manera exitosa!');
          this.Nuevo();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Modificar_Articulo(): void {


    var tModificarArticulo: ArticuloEntrada;
    tModificarArticulo = {
      "Id": 0,
      "Corporacion": this.tEmpresaActual.Corporacion,
      "Empresa": this.tEmpresaActual.Empresa,
      "Codarticulo": this.tArticuloActual.Codarticulo,
      "Referencia": this.tArticuloActual.Referencia,
      "Sku": this.tArticuloActual.Sku,
      "Codalterno": this.tArticuloActual.Codalterno,
      "Descripcion": this.tArticuloActual.Descripcion,
      "Descripcion2": this.tArticuloActual.Descripcion2,
      "Tipo": this.tArticuloActual.Tipo,
      "Codfabricante": this.tArticuloActual.Codfabricante,
      "Codmarca": this.tArticuloActual.Codmarca,
      "Codmodelo": this.tArticuloActual.Codmodelo,
      "Codclasificacion1": this.tArticuloActual.Codclasificacion1,
      "Codclasificacion2": this.tArticuloActual.Codclasificacion2,
      "Codclasificacion3": this.tArticuloActual.Codclasificacion3,
      "Codclasificacion4": this.tArticuloActual.Codclasificacion4,
      "Codcolor": this.tArticuloActual.Codcolor,
      "Codunidadmedida": this.tArticuloActual.Codunidadmedida,
      "LImagenes": [],
      "Url": this.tArticuloActual.Url,
      "Iva": this.tArticuloActual.Iva,
      "Costo": this.tArticuloActual.Costo,
      "Precio1": this.tArticuloActual.Precio1,
      "Precio2": this.tArticuloActual.Precio2,
      "Precio3": this.tArticuloActual.Precio3,
      "Preciolista": this.tArticuloActual.Preciolista,
      "Peso": this.tArticuloActual.Peso,
      "Alto": this.tArticuloActual.Alto,
      "Ancho": this.tArticuloActual.Ancho,
      "Largo": this.tArticuloActual.Largo,
      "Volumen": this.tArticuloActual.Volumen,
      "Maneja_lotes": this.tArticuloActual.Maneja_lotes,
      "Maneja_seriales": this.tArticuloActual.Maneja_seriales,
      "Maneja_tallas": this.tArticuloActual.Maneja_tallas,
      "Talla_inicial": this.tArticuloActual.Talla_inicial,
      "Talla_final": this.tArticuloActual.Talla_final,
      "Talla_intermedias": this.tArticuloActual.Talla_intermedias,
      "Talla_sugerida": this.tArticuloActual.Talla_sugerida,
      "Maneja_vencimiento": this.tArticuloActual.Maneja_vencimiento,
      "Dias_vencimiento_d": this.tArticuloActual.Dias_vencimiento_d,
      "Dias_vencimiento_r": this.tArticuloActual.Dias_vencimiento_r,
      "Usuariocreador": this.tArticuloActual.Usuariocreador,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }

    this.tArticuloActual.LImagenes.forEach(tImagen => {

      let tModificaArticuloImagenEntrada: ArticuloImagenEntrada = {

        "Id": 0,
        "Corporacion": this.tEmpresaActual.Corporacion,
        "Empresa": this.tEmpresaActual.Empresa,
        "Codarticulo": this.tArticuloActual.Codarticulo,
        "Principal": tImagen.Principal,
        "Nuevo": tImagen.Nuevo,
        "Url": "",
        "Imagen64": "",
        "Tipo": tImagen.Tipo,
        "Orden": tImagen.Orden,
        "Usuariocreador": tImagen.Usuariocreador,
        "Usuariomodificador": tImagen.Usuariomodificador

      }

      if (tImagen.Nuevo == "S") {


        tModificaArticuloImagenEntrada.Imagen64 = this.tManejarImagen.obtenerImagenes64(tImagen.Url);


      }
      else {

        tModificaArticuloImagenEntrada.Url = tImagen.Url;

      }

      tModificarArticulo.LImagenes.push(tModificaArticuloImagenEntrada);

    });

    this.Ws
      .Articulo()
      .Consumir_Modificar_Articulo(tModificarArticulo)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tErroresServices.MostrarError(environment.msjError);

        }
        else {

          this.toastr.success('El artículo se ha modificado de manera exitosa!');
          this.Nuevo();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Eliminar_Articulo(): void {

    var tEliminarArticulo: ArticuloEntrada;
    tEliminarArticulo = {
      "Id": 0,
      "Corporacion": this.tEmpresaActual.Corporacion,
      "Empresa": this.tEmpresaActual.Empresa,
      "Codarticulo": this.tArticuloActual.Codarticulo,
      "Referencia": this.tArticuloActual.Referencia,
      "Sku": this.tArticuloActual.Sku,
      "Codalterno": this.tArticuloActual.Codalterno,
      "Descripcion": this.tArticuloActual.Descripcion,
      "Descripcion2": this.tArticuloActual.Descripcion2,
      "Tipo": this.tArticuloActual.Tipo,
      "Codfabricante": this.tArticuloActual.Codfabricante,
      "Codmarca": this.tArticuloActual.Codmarca,
      "Codmodelo": this.tArticuloActual.Codmodelo,
      "Codclasificacion1": this.tArticuloActual.Codclasificacion1,
      "Codclasificacion2": this.tArticuloActual.Codclasificacion2,
      "Codclasificacion3": this.tArticuloActual.Codclasificacion3,
      "Codclasificacion4": this.tArticuloActual.Codclasificacion4,
      "Codcolor": this.tArticuloActual.Codcolor,
      "Codunidadmedida": this.tArticuloActual.Codunidadmedida,
      "LImagenes": [],
      "Url": this.tArticuloActual.Url,
      "Iva": this.tArticuloActual.Iva,
      "Costo": this.tArticuloActual.Costo,
      "Precio1": this.tArticuloActual.Precio1,
      "Precio2": this.tArticuloActual.Precio2,
      "Precio3": this.tArticuloActual.Precio3,
      "Preciolista": this.tArticuloActual.Preciolista,
      "Peso": this.tArticuloActual.Peso,
      "Alto": this.tArticuloActual.Alto,
      "Ancho": this.tArticuloActual.Ancho,
      "Largo": this.tArticuloActual.Largo,
      "Volumen": this.tArticuloActual.Volumen,
      "Maneja_lotes": this.tArticuloActual.Maneja_lotes,
      "Maneja_seriales": this.tArticuloActual.Maneja_seriales,
      "Maneja_tallas": this.tArticuloActual.Maneja_tallas,
      "Talla_inicial": this.tArticuloActual.Talla_inicial,
      "Talla_final": this.tArticuloActual.Talla_final,
      "Talla_intermedias": this.tArticuloActual.Talla_intermedias,
      "Talla_sugerida": this.tArticuloActual.Talla_sugerida,
      "Maneja_vencimiento": this.tArticuloActual.Maneja_vencimiento,
      "Dias_vencimiento_d": this.tArticuloActual.Dias_vencimiento_d,
      "Dias_vencimiento_r": this.tArticuloActual.Dias_vencimiento_r,
      "Usuariocreador": this.tArticuloActual.Usuariocreador,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }

    this.Ws
      .Articulo()
      .Consumir_Eliminar_Articulo(tEliminarArticulo)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tErroresServices.MostrarError(environment.msjError);

        }
        else {

          this.toastr.success('El artículo se ha eliminado de manera exitosa!');
          this.Nuevo();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  Nuevo(): void {

    this.tHabilitarGuardar = this.tPermisoAgregar;
    this.tHabilitarEliminar = false;
    this.tBloquearBuscar = false;
    this.tNuevo = true;
    this.tEditar = false;
    this.tManejarImagen.tImagenes = [];
    // this.tArticulosImg1x3 = [];
    this.tArticuloActual = new Articulo();
    this.tManejarImagen.tImagen = "";
    this.tDetallesInfo.Limpiar();
    this.tDetallesClas.Limpiar();

    this.toastr.info('Por favor llene los datos necesarios para registrar un nuevo articulo!');

  }

  Cancelar(): void {

    this.tBloquearBuscar = false;
    this.Nuevo();

  }

  ValidarFormulario(): boolean {

    let valido: boolean = false;

    if (this.tArticuloActual.Descripcion == "") {

      this.toastr.warning('Debe ingresar una Descripcion corta!', 'Disculpe!');
      return valido;

    }

    if (this.tArticuloActual.Descripcion2 == "") {

      this.toastr.warning('Debe ingresar una Descripcion larga!', 'Disculpe!');
      return valido;

    }

    if (this.tArticuloActual.Sku == "") {

      this.toastr.warning('Debe ingresar un Sku!', 'Disculpe!');
      return valido;

    }

    if (this.tArticuloActual.Costo <= 0) {

      this.toastr.error('Debe ingresar un Costo mayor que 0!', 'Disculpe!');
      return valido;

    }

    if (this.tArticuloActual.Precio1 <= 0) {

      this.toastr.error('Debe ingresar un Precio1 mayor que 0!', 'Disculpe!');
      return valido;

    }


    if (this.tArticuloActual.Preciolista <= 0) {

      this.toastr.error('Debe ingresar un PrecioLista mayor que 0!', 'Disculpe!');
      return valido;

    }

    return true;

  }

  Agregar(): void {

    if (this.ValidarFormulario() == false) {

      return;

    }

    switch (true) {
      case this.tNuevo: {

        this.tConfServices
          .AbrirModalConf(this.tConfiguracionFormulario.ModalConfirmacion.Agregar)
          .then(Resultado => {

            switch (Resultado) {
              case "S": {

                this.Nuevo_Articulo();
                break;

              }
            }

          });

        break;

      }
      case this.tEditar: {

        this.tConfServices
          .AbrirModalConf(this.tConfiguracionFormulario.ModalConfirmacion.Edicion)
          .then(Resultado => {

            switch (Resultado) {
              case "S": {

                this.Modificar_Articulo();
                break;

              }
            }

          });


        break;

      }
    }

  }

  Eliminar(): void {

    this.tConfServices
      .AbrirModalConf(this.tConfiguracionFormulario.ModalConfirmacion.Eliminacion)
      .then(Resultado => {

        switch (Resultado) {
          case "S": {

            this.Eliminar_Articulo();
            break;

          }
        }

      });

  }

  CargarImagenes({ ImagenesStr, Tipo }) {

    ImagenesStr.forEach(tImagenStr => {

      let tNuevaImagen: ArticuloImagenes = {

        "Id": 0,
        "Corporacion": this.tEmpresaActual.Corporacion,
        "Empresa": this.tEmpresaActual.Empresa,
        "Codarticulo": this.tArticuloActual.Codarticulo,
        "Principal": 0,
        "Url": tImagenStr,
        "Nuevo": "S",
        "Tipo": Tipo.Codtipo,
        "Orden": this.tArticuloActual.LImagenes.length + 1,
        "Usuariocreador": this.tUsuarioActual.Codusuario,
        "Usuariomodificador": this.tUsuarioActual.Codusuario,
        "Fecha_creacion": new Date(),
        "Fecha_creacionl": 0,
        "Fecha_modificacion": new Date(),
        "Fecha_modificacionl": 0

      }

      this.tArticuloActual.LImagenes.push(tNuevaImagen);

    });

  }

  EliminarImagenes({ArticuloImagenes, Posicion}) {

    let tArticuloImagen: ArticuloImagenEntrada = {

      "Id": 0,
      "Corporacion": ArticuloImagenes.Corporacion,
      "Empresa": ArticuloImagenes.Empresa,
      "Codarticulo": ArticuloImagenes.Codarticulo,
      "Url": ArticuloImagenes.Url,
      "Imagen64": "",
      "Nuevo": ArticuloImagenes.Nuevo,
      "Principal": ArticuloImagenes.Principal,
      "Tipo": ArticuloImagenes.Tipo,
      "Orden": ArticuloImagenes.Orden,
      "Usuariocreador": ArticuloImagenes.Usuariocreador,
      "Usuariomodificador": ArticuloImagenes.Usuariomodificador

    }

    this.Ws
      .ArticuloImagenes()
      .Consumir_Eliminar_ArticuloImagen(tArticuloImagen)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tArticuloActual.LImagenes.splice(Posicion, 1);

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });
  }

}
