import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwArticuloCabeceraComponent } from './fw-articulo-cabecera.component';

describe('FwArticuloCabeceraComponent', () => {
  let component: FwArticuloCabeceraComponent;
  let fixture: ComponentFixture<FwArticuloCabeceraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwArticuloCabeceraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwArticuloCabeceraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
