import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Articulo, Empresas, Tipo, UnidadMedida, Color } from 'src/app/Clases/Estructura';
import { UnidadesMedidaService } from 'src/app/Modales/unidad-medida/fw-modal-unidades-medida/unidades-medida.service';
import { ColoresService } from 'src/app/Modales/color/fw-modal-colores/colores.service';

@Component({
  selector: 'app-fw-articulo-detalles-informacion',
  templateUrl: './fw-articulo-detalles-informacion.component.html',
  styleUrls: ['./fw-articulo-detalles-informacion.component.scss']
})
export class FwArticuloDetallesInformacionComponent implements OnInit, OnChanges {

  @Input('Articulo') tArticulo: Articulo = new Articulo();
  @Input('Empresa') tEmpresa: Empresas = new Empresas();
  @Input('Vprecio1') tVprecio1: boolean = false;
  @Input('Vprecio2') tVprecio2: boolean = false;
  @Input('Vprecio3') tVprecio3: boolean = false;
  @Input('VprecioLista') tVprecioLista: boolean = false;
  @Input('Tipos') tTipos: Tipo[] = [];
  @Input('Unidades') tUnidades: UnidadMedida[] = [];
  @Input('Colores') tColores: Color[] = [];

  tTipoActual: Tipo = new Tipo();
  tUnidadActual: UnidadMedida = new UnidadMedida();
  tColorActual: Color = new Color();

  constructor(private tUnidadMedidaService: UnidadesMedidaService,
    private tColoresService: ColoresService) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {

    if ('tArticulo' in changes) {

      this.tTipoActual = this.tTipos.find(tTip => {

        return tTip.Corporacion === this.tArticulo.Corporacion &&
          tTip.Empresa === this.tArticulo.Empresa &&
          tTip.Abreviacion === this.tArticulo.Tipo

      }) || this.tTipos[0] || new Tipo();

      this.tUnidadActual = this.tUnidades.find(tUnidad => {

        return tUnidad.Corporacion === this.tArticulo.Corporacion &&
          tUnidad.Empresa === this.tArticulo.Empresa &&
          tUnidad.Codunidadmedida === this.tArticulo.Codunidadmedida

      }) || this.tUnidades[0] || new UnidadMedida();

      this.tColorActual = this.tColores.find(tColor => {

        return tColor.Corporacion === this.tArticulo.Corporacion &&
          tColor.Empresa === this.tArticulo.Empresa &&
          tColor.Codcolor === this.tArticulo.Codcolor

      }) || this.tColores[0] || new Color();

      if(this.tArticulo.Codarticulo === ''){

        this.CambiarColor();
        this.CambiarTallas();
        this.CambiarTipo();
        this.CambiarUnidadMedida();
        this.CambiarVencimiento();

      }

    }

    if ('tUnidades' in changes) {

      this.tUnidadActual = this.tUnidades[0];

    }

    if ('tColores' in changes) {

      this.tColorActual = this.tColores[0];

    }

  }

  CambiarTipo() {

    this.tArticulo.Tipo = this.tTipoActual.Abreviacion;

  }

  CambiarUnidadMedida() {

    this.tArticulo.Codunidadmedida = this.tUnidadActual.Codunidadmedida;

  }

  CambiarColor() {

    this.tArticulo.Codcolor = this.tColorActual.Codcolor;

  }

  Limpiar() {

    this.tTipoActual = this.tTipos[0] || new Tipo();
    this.tColorActual = this.tColores[0] || new Color();
    this.tUnidadActual = this.tUnidades[0] || new UnidadMedida();

  }

  AbrirModalUnidadesMedida(): void {

    this.tUnidadMedidaService.AbrirModal().then(result => {

      if (result.Resultado == "S") {

        var tUnidadModal: UnidadMedida = result.UnidadMedida;
        this.tUnidadActual = this.tUnidades.find(tUnidad => {
          return tUnidad.Corporacion == tUnidadModal.Corporacion &&
            tUnidad.Empresa == tUnidadModal.Empresa &&
            tUnidad.Codunidadmedida == tUnidadModal.Codunidadmedida
        }) || this.tUnidades[0] || new UnidadMedida();


      }

    }, reason => {



    });

  }

  AbrirModalColores(): void {

    this.tColoresService.AbrirModal().then(result => {

      if (result.Resultado == "S") {

        var tColorModal: Color = result.Color;
        this.tColorActual = this.tColores.find(tColor => {
          return tColor.Corporacion == tColorModal.Corporacion &&
            tColor.Empresa == tColorModal.Empresa &&
            tColor.Codcolor == tColorModal.Codcolor
        }) || this.tColores[0] || new Color();

        // for (let tColor of this.tColores) {

        //   if () {

        //     this.tColorActual = tColor;
        //     break;

        //   }

        // }

      }

    });

  }

  BloquearTallas(): boolean {

    if (this.tArticulo.Maneja_tallas === "S") {

      return false;

    }
    else {

      return true;

    }

  }

  CambiarTallas() {

    if (this.tArticulo.Maneja_tallas === "N") {

      this.tArticulo.Talla_inicial = "";
      this.tArticulo.Talla_final = "";
      this.tArticulo.Talla_sugerida = "";
      this.tArticulo.Talla_intermedias = "N";

    }

  }

  CambiarVencimiento() {

    if (this.tArticulo.Maneja_vencimiento === "N") {

      this.tArticulo.Dias_vencimiento_d = 0;
      this.tArticulo.Dias_vencimiento_r = 0;

    }

  }

  BloquearVencimiento(): boolean {

    if (this.tArticulo.Maneja_vencimiento === "S") {

      return false;

    }
    else {

      return true;

    }

  }

}
