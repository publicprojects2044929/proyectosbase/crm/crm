import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwArticuloDetallesClasificacionComponent } from './fw-articulo-detalles-clasificacion.component';

describe('FwArticuloDetallesClasificacionComponent', () => {
  let component: FwArticuloDetallesClasificacionComponent;
  let fixture: ComponentFixture<FwArticuloDetallesClasificacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwArticuloDetallesClasificacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwArticuloDetallesClasificacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
