import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Articulo } from 'src/app/Clases/Estructura';
import { ManejarImagenes } from 'src/app/Manejadores/cls-procedimientos';

@Component({
  selector: 'app-fw-articulo-cabecera',
  templateUrl: './fw-articulo-cabecera.component.html',
  styleUrls: ['./fw-articulo-cabecera.component.scss']
})
export class FwArticuloCabeceraComponent implements OnInit {

  @Input('Articulo') tArticulo: Articulo = new Articulo();
  @Input('BloquearBuscar') tBloquearBuscar: boolean = false;
  @Input('Editar') tEditar: boolean = false;

  @Output('Buscar') tBuscar: EventEmitter<string> = new EventEmitter();
  @Output('AbrirModal') tAbrirModal: EventEmitter<string> = new EventEmitter();

  tManejarImagen: ManejarImagenes = new ManejarImagenes();

  constructor() { }

  ngOnInit(): void {
  }

  Buscar() {

    if (this.tArticulo.Codarticulo === '') { return; }

    this.tBuscar.emit(this.tArticulo.Codarticulo);

  }

  AbrirModal(){
    this.tAbrirModal.emit('');
  }

  ObtenerImagenPrincipal(): string {

    let tImagenUrl: string = this.tManejarImagen.ObtenerImagen();

    this.tArticulo.LImagenes.forEach(tImagen => {

      if (tImagen.Principal === 1) {

        tImagenUrl = tImagen.Url;

      }

    })

    return tImagenUrl;

  }



}
