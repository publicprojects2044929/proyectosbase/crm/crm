import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwArticuloDetallesInformacionComponent } from './fw-articulo-detalles-informacion.component';

describe('FwArticuloDetallesInformacionComponent', () => {
  let component: FwArticuloDetallesInformacionComponent;
  let fixture: ComponentFixture<FwArticuloDetallesInformacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwArticuloDetallesInformacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwArticuloDetallesInformacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
