import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Clasificacion1, Clasificacion2, Clasificacion3, Clasificacion4, Fabricante, Marca, Modelo, Articulo } from 'src/app/Clases/Estructura';
import { Clasificaciones1Service } from 'src/app/Modales/clasificacion1/fw-modal-clasificaciones1/clasificaciones1.service';
import { Clasificaciones2Service } from 'src/app/Modales/clasificacion2/fw-modal-clasificaciones2/clasificaciones2.service';
import { Clasificaciones3Service } from 'src/app/Modales/clasificacion3/fw-modal-clasificaciones3/clasificaciones3.service';
import { Clasificaciones4Service } from 'src/app/Modales/clasificacion4/fw-modal-clasificaciones4/clasificaciones4.service';
import { FabricantesService } from 'src/app/Modales/fabricante/fw-modal-fabricantes/fabricantes.service';
import { MarcasService } from 'src/app/Modales/marca/fw-modal-marcas/marcas.service';
import { ModelosService } from 'src/app/Modales/modelo/fw-modal-modelos/modelos.service';

@Component({
  selector: 'app-fw-articulo-detalles-clasificacion',
  templateUrl: './fw-articulo-detalles-clasificacion.component.html',
  styleUrls: ['./fw-articulo-detalles-clasificacion.component.scss']
})
export class FwArticuloDetallesClasificacionComponent implements OnInit, OnChanges {

  @Input('Articulo') tArticulo: Articulo = new Articulo();
  @Input('Clasificaciones1') tClasificaciones1: Clasificacion1[] = [];
  @Input('Fabricantes') tFabricantes: Fabricante[] = [];

  tClasificaciones2: Clasificacion2[] = [];
  tClasificaciones3: Clasificacion3[] = [];
  tClasificaciones4: Clasificacion4[] = [];

  tMarcas: Marca[] = [];
  tModelos: Modelo[] = [];

  tClasificacion1Actual: Clasificacion1 = new Clasificacion1();
  tClasificacion2Actual: Clasificacion2 = new Clasificacion2();
  tClasificacion3Actual: Clasificacion3 = new Clasificacion3();
  tClasificacion4Actual: Clasificacion4 = new Clasificacion4();

  tFabricanteActual: Fabricante = new Fabricante();
  tMarcaActual: Marca = new Marca();
  tModeloActual: Modelo = new Modelo();

  constructor(private tClasificacion1Service: Clasificaciones1Service,
    private tClasificacion2Service: Clasificaciones2Service,
    private tClasificacion3Service: Clasificaciones3Service,
    private tClasificacion4Service: Clasificaciones4Service,
    private tFabricantesService: FabricantesService,
    private tMarcaService: MarcasService,
    private tModeloService: ModelosService) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {

    if ('tArticulo' in changes) {

      this.tClasificacion1Actual = this.tClasificaciones1.find(tClasificacion1 => {
        return tClasificacion1.Corporacion === this.tArticulo.Corporacion &&
          tClasificacion1.Empresa === this.tArticulo.Empresa &&
          tClasificacion1.Codclasificacion1 === this.tArticulo.Codclasificacion1
      }) || this.tClasificaciones1[0] || new Clasificacion1();

      this.tClasificaciones2 = this.tClasificacion1Actual.LClasificacion2 || [];

      this.tClasificacion2Actual = this.tClasificaciones2.find(tClasificacion2 => {
        return tClasificacion2.Corporacion === this.tArticulo.Corporacion &&
          tClasificacion2.Empresa === this.tArticulo.Empresa &&
          tClasificacion2.Codclasificacion1 === this.tArticulo.Codclasificacion1 &&
          tClasificacion2.Codclasificacion2 === this.tArticulo.Codclasificacion2
      }) || this.tClasificaciones2[0] || new Clasificacion2();

      this.tClasificaciones3 = this.tClasificacion2Actual.LClasificacion3 || [];

      this.tClasificacion3Actual = this.tClasificaciones3.find(tClasificacion3 => {
        return tClasificacion3.Corporacion === this.tArticulo.Corporacion &&
          tClasificacion3.Empresa === this.tArticulo.Empresa &&
          tClasificacion3.Codclasificacion1 === this.tArticulo.Codclasificacion1 &&
          tClasificacion3.Codclasificacion2 === this.tArticulo.Codclasificacion2 &&
          tClasificacion3.Codclasificacion3 === this.tArticulo.Codclasificacion3
      }) || this.tClasificaciones3[0] || new Clasificacion3();

      this.tClasificaciones4 = this.tClasificacion3Actual.LClasificacion4 || [];

      this.tClasificacion4Actual = this.tClasificaciones4.find(tClasificacion4 => {
        return tClasificacion4.Corporacion === this.tArticulo.Corporacion &&
          tClasificacion4.Empresa === this.tArticulo.Empresa &&
          tClasificacion4.Codclasificacion1 === this.tArticulo.Codclasificacion1 &&
          tClasificacion4.Codclasificacion2 === this.tArticulo.Codclasificacion2 &&
          tClasificacion4.Codclasificacion3 === this.tArticulo.Codclasificacion3 &&
          tClasificacion4.Codclasificacion4 === this.tArticulo.Codclasificacion2
      }) || this.tClasificaciones4[0] || new Clasificacion4();

      this.tFabricanteActual = this.tFabricantes.find(tFabricante => {
        return tFabricante.Corporacion === this.tArticulo.Corporacion &&
          tFabricante.Empresa === this.tArticulo.Empresa &&
          tFabricante.Codfabricante === this.tArticulo.Codfabricante
      }) || this.tFabricantes[0] || new Fabricante();

      this.tMarcas = this.tFabricanteActual.LMarcas || [];

      this.tMarcaActual = this.tMarcas.find(tMarca => {
        return tMarca.Corporacion === this.tArticulo.Corporacion &&
          tMarca.Empresa === this.tArticulo.Empresa &&
          tMarca.Codfabricante === this.tArticulo.Codfabricante &&
          tMarca.Codmarca === this.tArticulo.Codmarca
      }) || this.tMarcas[0] || new Marca();

      this.tModelos = this.tMarcaActual.LModelos || [];

      this.tModeloActual = this.tModelos.find(tModelo => {
        return tModelo.Corporacion === this.tArticulo.Corporacion &&
          tModelo.Empresa === this.tArticulo.Empresa &&
          tModelo.Codfabricante === this.tArticulo.Codfabricante &&
          tModelo.Codmarca === this.tArticulo.Codmarca &&
          tModelo.Codmodelo === this.tArticulo.Codmodelo
      }) || this.tModelos[0] || new Modelo();

    }

    if ('tClasificaciones1' in changes) {

      this.LimpiarClasificaciones();
      this.Seleccionar_Clasificacion4();

    }

    if ('tFabricantes' in changes) {

      this.LimpiarFabricantes();
      this.Seleccionar_Modelo();

    }

  }

  AbrirModalClasificacion1(): void {

    this.tClasificacion1Service.AbrirModal().then(result => {

      if (result.Resultado == "S") {

        var tClasificacion1Modal: Clasificacion1 = result.Clasificacion1;
        this.tClasificacion1Actual = this.tClasificaciones1.find(tClasificacion1 => {
          return tClasificacion1.Corporacion === tClasificacion1Modal.Corporacion &&
            tClasificacion1.Empresa === tClasificacion1Modal.Empresa &&
            tClasificacion1.Codclasificacion1 === tClasificacion1Modal.Codclasificacion1
        }) || new Clasificacion1();

        this.Seleccionar_Clasificacion1();

      }

    });

  }

  AbrirModalClasificacion2(): void {

    this.tClasificacion2Service
      .AbrirModal(this.tClasificacion1Actual)
      .then(result => {

        if (result.Resultado == "S") {

          var tClasificacion2Modal: Clasificacion2 = result.Clasificacion2;
          this.tClasificacion2Actual = this.tClasificaciones2.find(tClasificacion2 => {
            return tClasificacion2.Corporacion === tClasificacion2Modal.Corporacion &&
              tClasificacion2.Empresa === tClasificacion2Modal.Empresa &&
              tClasificacion2.Codclasificacion1 === tClasificacion2Modal.Codclasificacion1 &&
              tClasificacion2.Codclasificacion2 === tClasificacion2Modal.Codclasificacion2
          }) || new Clasificacion2();

          this.Seleccionar_Clasificacion2();

        }

      });

  }

  AbrirModalClasificacion3(): void {

    this.tClasificacion3Service
      .AbrirModal(this.tClasificacion2Actual)
      .then(result => {

        if (result.Resultado == "S") {

          var tClasificacion3Modal: Clasificacion3 = result.Clasificacion3;
          this.tClasificacion3Actual = this.tClasificaciones3.find(tClasificacion3 => {
            return tClasificacion3.Corporacion === tClasificacion3Modal.Corporacion &&
              tClasificacion3.Empresa === tClasificacion3Modal.Empresa &&
              tClasificacion3.Codclasificacion1 === tClasificacion3Modal.Codclasificacion1 &&
              tClasificacion3.Codclasificacion2 === tClasificacion3Modal.Codclasificacion2 &&
              tClasificacion3.Codclasificacion3 === tClasificacion3Modal.Codclasificacion3
          }) || new Clasificacion3();

          this.Seleccionar_Clasificacion3();

        }

      }, reason => {



      });

  }

  AbrirModalClasificacion4(): void {

    this.tClasificacion4Service
      .AbrirModal(this.tClasificacion3Actual)
      .then(result => {

        if (result.Resultado == "S") {

          var tClasificacion4Modal: Clasificacion4 = result.Clasificacion4;
          this.tClasificacion4Actual = this.tClasificaciones4.find(tClasificacion4 => {
            return tClasificacion4.Corporacion === tClasificacion4Modal.Corporacion &&
              tClasificacion4.Empresa === tClasificacion4Modal.Empresa &&
              tClasificacion4.Codclasificacion1 === tClasificacion4Modal.Codclasificacion1 &&
              tClasificacion4.Codclasificacion2 === tClasificacion4Modal.Codclasificacion2 &&
              tClasificacion4.Codclasificacion3 === tClasificacion4Modal.Codclasificacion3 &&
              tClasificacion4.Codclasificacion4 === tClasificacion4Modal.Codclasificacion4
          }) || new Clasificacion4();
          this.Seleccionar_Clasificacion4();

        }

      }, reason => {



      });

  }

  AbrirModalFabricante(): void {

    this.tFabricantesService
      .AbrirModal()
      .then(result => {

        if (result.Resultado == "S") {

          var tFabricanteModal: Fabricante = result.Fabricante;
          this.tFabricanteActual = this.tFabricantes.find(tFabricante => {
            return tFabricante.Corporacion === tFabricanteModal.Corporacion &&
              tFabricante.Empresa === tFabricanteModal.Empresa &&
              tFabricante.Codfabricante === tFabricanteModal.Codfabricante
          }) || new Fabricante();
          this.Seleccionar_Fabricante();

        }

      });

  }

  AbrirModalMarca(): void {

    this.tMarcaService
      .AbrirModal(this.tFabricanteActual)
      .then(result => {

        if (result.Resultado == "S") {

          var tMarcaModal: Marca = result.Marca;
          this.tMarcaActual = this.tMarcas.find(tMarca => {
            return tMarca.Corporacion === tMarcaModal.Corporacion &&
              tMarca.Empresa === tMarcaModal.Empresa &&
              tMarca.Codfabricante === tMarcaModal.Codfabricante &&
              tMarca.Codmarca === tMarcaModal.Codmarca
          }) || new Marca();
          this.Seleccionar_Marca();

        }

      }, reason => {



      });

  }

  AbrirModalModelo(): void {

    this.tModeloService
      .AbrirModal(this.tMarcaActual)
      .then(result => {

        if (result.Resultado == "S") {

          var tModeloModal: Modelo = result.Modelo;
          this.tModeloActual = this.tModelos.find(tModelo => {
            return tModelo.Corporacion === tModeloModal.Corporacion &&
              tModelo.Empresa === tModeloModal.Empresa &&
              tModelo.Codfabricante === tModeloModal.Codfabricante &&
              tModelo.Codmarca === tModeloModal.Codmarca &&
              tModelo.Codmodelo === tModeloModal.Codmodelo
          }) || new Modelo();
          this.Seleccionar_Modelo();

        }

      }, reason => {



      });

  }

  Seleccionar_Clasificacion1() {

    this.tClasificaciones2 = this.tClasificacion1Actual.LClasificacion2;
    this.tClasificaciones3 = [];
    this.tClasificaciones4 = [];
    this.tClasificacion2Actual = new Clasificacion2();
    this.tClasificacion3Actual = new Clasificacion3();
    this.tClasificacion4Actual = new Clasificacion4();

    this.tClasificacion2Actual = this.tClasificaciones2[0] || new Clasificacion2();
    this.tClasificaciones3 = this.tClasificacion2Actual.LClasificacion3 || [];

    this.tClasificacion3Actual = this.tClasificaciones3[0] || new Clasificacion3();
    this.tClasificaciones4 = this.tClasificacion3Actual.LClasificacion4 || [];

    this.tClasificacion4Actual = this.tClasificaciones4[0] || new Clasificacion4();
    this.Seleccionar_Clasificacion4();

  };

  Seleccionar_Clasificacion2() {

    this.tClasificaciones3 = this.tClasificacion2Actual.LClasificacion3;
    this.tClasificaciones4 = [];
    this.tClasificacion3Actual = new Clasificacion3();
    this.tClasificacion4Actual = new Clasificacion4();

    this.tClasificacion3Actual = this.tClasificaciones3[0] || new Clasificacion3();
    this.tClasificaciones4 = this.tClasificacion3Actual.LClasificacion4 || [];

    this.tClasificacion4Actual = this.tClasificaciones4[0] || new Clasificacion4();
    this.Seleccionar_Clasificacion4();

  };

  Seleccionar_Clasificacion3() {

    this.tClasificaciones4 = this.tClasificacion3Actual.LClasificacion4 || [];
    this.tClasificacion4Actual = this.tClasificaciones4[0] || new Clasificacion4();
    this.Seleccionar_Clasificacion4();

  };

  Seleccionar_Clasificacion4() {

    this.tArticulo.Codclasificacion1 = this.tClasificacion4Actual.Codclasificacion1;
    this.tArticulo.Codclasificacion2 = this.tClasificacion4Actual.Codclasificacion2;
    this.tArticulo.Codclasificacion3 = this.tClasificacion4Actual.Codclasificacion3;
    this.tArticulo.Codclasificacion4 = this.tClasificacion4Actual.Codclasificacion4;

  };

  Seleccionar_Fabricante() {

    this.tMarcas = this.tFabricanteActual.LMarcas;
    this.tModelos = [];
    this.tMarcaActual = new Marca();
    this.tModeloActual = new Modelo();

    this.tMarcaActual = this.tMarcas[0] || new Marca();
    this.tModelos = this.tMarcaActual.LModelos || [];

    this.tModeloActual = this.tModelos[0] || new Modelo();
    this.Seleccionar_Modelo();

  }

  Seleccionar_Marca() {

    this.tModelos = this.tMarcaActual.LModelos || [];
    this.tModeloActual = this.tModelos[0] || new Modelo();
    this.Seleccionar_Modelo();

  }

  Seleccionar_Modelo() {

    this.tArticulo.Codfabricante = this.tModeloActual.Codfabricante;
    this.tArticulo.Codmarca = this.tModeloActual.Codmarca;
    this.tArticulo.Codmodelo = this.tModeloActual.Codmodelo;

  }

  LimpiarFabricantes() {

    this.tFabricanteActual = this.tFabricantes[0] || new Fabricante();

    this.tMarcas = this.tFabricanteActual.LMarcas || [];
    this.tMarcaActual = this.tMarcas[0] || new Marca();

    this.tModelos = this.tMarcaActual.LModelos || [];
    this.tModeloActual = this.tModelos[0] || new Modelo();

  }

  LimpiarClasificaciones() {

    this.tClasificacion1Actual = this.tClasificaciones1[0] || new Clasificacion1();

    this.tClasificaciones2 = this.tClasificacion1Actual.LClasificacion2 || [];
    this.tClasificacion2Actual = this.tClasificaciones2[0] || new Clasificacion2;

    this.tClasificaciones3 = this.tClasificacion2Actual.LClasificacion3 || [];
    this.tClasificacion3Actual = this.tClasificaciones3[0] || new Clasificacion3();

    this.tClasificaciones4 = this.tClasificacion3Actual.LClasificacion4 || [];
    this.tClasificacion4Actual = this.tClasificaciones4[0] || new Clasificacion4();

  }

  Limpiar() {

    this.LimpiarFabricantes();
    this.LimpiarClasificaciones();
    this.Seleccionar_Clasificacion4();
    this.Seleccionar_Modelo();

  }

}
