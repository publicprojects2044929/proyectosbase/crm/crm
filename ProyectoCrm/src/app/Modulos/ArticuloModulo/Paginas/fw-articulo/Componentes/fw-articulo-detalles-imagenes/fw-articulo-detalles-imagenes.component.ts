import { Component, OnInit, Input, Output, OnChanges, SimpleChanges, EventEmitter } from '@angular/core';
import { Tipo, ArticuloImagenes, Configuracion, Articulo, Empresas } from 'src/app/Clases/Estructura';
import { ManejarImagenes } from 'src/app/Manejadores/cls-procedimientos';

@Component({
  selector: 'app-fw-articulo-detalles-imagenes',
  templateUrl: './fw-articulo-detalles-imagenes.component.html',
  styleUrls: ['./fw-articulo-detalles-imagenes.component.scss']
})
export class FwArticuloDetallesImagenesComponent implements OnInit, OnChanges {

  @Input('Imagenes') tImagenes: ArticuloImagenes[] = [];
  @Input('Tipos') tTipos: Tipo[] = [];
  @Input('Configuracion') tConfiguracion: Configuracion = new Configuracion();
  @Input('ManejarImagen') tManejarImagen: ManejarImagenes = new ManejarImagenes();

  @Output('CargarImagen') tCargarImagen: EventEmitter<{ImagenesStr: string[], Tipo: Tipo}> = new EventEmitter();
  @Output('EliminarImagen') tEliminarImagen: EventEmitter<{ArticuloImagenes: ArticuloImagenes, Posicion: number}> = new EventEmitter();


  tPosicionImagen: number = 0;
  tPaginaImagen: number = 1;
  tTamanoPagImagen: number = 5;

  tTiposImagenesActual: Tipo = new Tipo();

  tArticuloImgActual: ArticuloImagenes = new ArticuloImagenes();
  tArticuloImgFiltrado: ArticuloImagenes[] = [];
  tPosicionSprite: number = 0;

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {

    // this.tManejarImagen.tImagen = this.tArticuloActual.Url;

    if('tTipos' in changes){

      this.tTiposImagenesActual = this.tTipos[0] || new Tipo();

    }

  }


  BloquearImagen(): boolean {

    if (this.tConfiguracion.Valor == "S") {

      return false;

    }
    else {

      return true;

    }

  }

  CargarImagenes(Imagenes: void): void {

    let tImagenesStr: string[] = this.tManejarImagen.Cargar_Archivos(Imagenes);

    setTimeout(() => {

      this.tCargarImagen.emit({
        ImagenesStr: tImagenesStr,
        Tipo: this.tTiposImagenesActual
      });

      this.tManejarImagen.tImagenes = [];
      this.FiltrarImagenes();

    }, 500);

  }

  ImagenPrincipal(tImagenSel: ArticuloImagenes): void {

    this.tImagenes.map(tImagen => {

      if (tImagenSel.Corporacion === tImagen.Corporacion &&
        tImagenSel.Empresa === tImagen.Empresa &&
        tImagenSel.Codarticulo === tImagen.Codarticulo &&
        tImagenSel.Url === tImagen.Url) {

        tImagen.Principal = 1;

      }
      else {

        tImagen.Principal = 0;

      }

    })

  }

  EliminarImagen(tImagenSel: ArticuloImagenes, tPos: number) {

    this.tImagenes.map((tImagen: ArticuloImagenes) => {

      if (tImagen.Orden >= tImagenSel.Orden && tImagen.Tipo === this.tTiposImagenesActual.Codtipo) {

        tImagen.Orden--;

      }

    });

    if (tImagenSel.Nuevo === "S") {

      this.tImagenes.forEach((tImagen: ArticuloImagenes, tPosFor: number) => {

        if (tImagen.Corporacion === tImagenSel.Corporacion &&
          tImagen.Empresa === tImagenSel.Empresa &&
          tImagen.Codarticulo === tImagenSel.Codarticulo &&
          tImagen.Url === tImagenSel.Url) {

          this.tImagenes.splice(tPosFor, 1);

        }

      });

    }
    else {

      this.tEliminarImagen.emit({
        ArticuloImagenes: tImagenSel,
        Posicion: tPos
      })

    }

    this.FiltrarImagenes();

  }

  ObtenerUrl(tUrlFor: string): string {

    let tUrl: string = "";

    tUrl = this.tManejarImagen.ObtenerImagenes(tUrlFor);

    return tUrl;

  }

  SeleccionarImagen(tArticuloImgagen: ArticuloImagenes) {

    this.tArticuloImgActual = tArticuloImgagen;

  }

  FiltrarImagenes() {

    this.tPosicionSprite = 0;

    this.tArticuloImgFiltrado = this.tImagenes.filter(tImagen => tImagen.Tipo === this.tTiposImagenesActual.Codtipo) || [];

    this.tArticuloImgFiltrado.sort(function (a, b) {
      if (a.Orden > b.Orden) {
        return 1;
      }
      if (a.Orden < b.Orden) {
        return -1;
      }
      // a must be equal to b
      return 0;
    });

    if (this.tTiposImagenesActual.Codtipo === 'SPRITE') {

      this.MostrarImagenes();

    }

  }

  MostrarImagenes() {

    this.tArticuloImgActual = this.tArticuloImgFiltrado[this.tPosicionSprite];

  }

  SubirOrden(tArticuloImagenSel: ArticuloImagenes) {

    if (tArticuloImagenSel.Orden === 0) {

      return;

    }

    this.tImagenes.map(tArticuloImagen => {

      if (tArticuloImagen.Corporacion === tArticuloImagenSel.Corporacion &&
        tArticuloImagen.Empresa === tArticuloImagenSel.Empresa &&
        tArticuloImagen.Codarticulo === tArticuloImagenSel.Codarticulo &&
        tArticuloImagen.Tipo === tArticuloImagenSel.Tipo &&
        tArticuloImagen.Url === tArticuloImagenSel.Url) {

        tArticuloImagen.Orden++;

      }
      else if (tArticuloImagen.Corporacion === tArticuloImagenSel.Corporacion &&
        tArticuloImagen.Empresa === tArticuloImagenSel.Empresa &&
        tArticuloImagen.Codarticulo === tArticuloImagenSel.Codarticulo &&
        tArticuloImagen.Tipo === tArticuloImagenSel.Tipo &&
        tArticuloImagen.Url !== tArticuloImagenSel.Url &&
        tArticuloImagen.Orden === tArticuloImagenSel.Orden) {

        tArticuloImagen.Orden--;

      }

    });

  }

  BajarOrden(tArticuloImagenSel: ArticuloImagenes) {

    if (tArticuloImagenSel.Orden === (this.tArticuloImgFiltrado.length + 1)) {

      return;

    }

    this.tImagenes.map(tArticuloImagen => {

      if (tArticuloImagen.Corporacion === tArticuloImagenSel.Corporacion &&
        tArticuloImagen.Empresa === tArticuloImagenSel.Empresa &&
        tArticuloImagen.Codarticulo === tArticuloImagenSel.Codarticulo &&
        tArticuloImagen.Tipo === tArticuloImagenSel.Tipo &&
        tArticuloImagen.Url === tArticuloImagenSel.Url) {

        tArticuloImagen.Orden--;

      }
      else if (tArticuloImagen.Corporacion === tArticuloImagenSel.Corporacion &&
        tArticuloImagen.Empresa === tArticuloImagenSel.Empresa &&
        tArticuloImagen.Codarticulo === tArticuloImagenSel.Codarticulo &&
        tArticuloImagen.Tipo === tArticuloImagenSel.Tipo &&
        tArticuloImagen.Url !== tArticuloImagenSel.Url &&
        tArticuloImagen.Orden === tArticuloImagenSel.Orden) {

        tArticuloImagen.Orden++;

      }

    });

  }

}
