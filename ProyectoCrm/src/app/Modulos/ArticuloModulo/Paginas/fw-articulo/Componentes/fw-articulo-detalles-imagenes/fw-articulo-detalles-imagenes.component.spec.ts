import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwArticuloDetallesImagenesComponent } from './fw-articulo-detalles-imagenes.component';

describe('FwArticuloDetallesImagenesComponent', () => {
  let component: FwArticuloDetallesImagenesComponent;
  let fixture: ComponentFixture<FwArticuloDetallesImagenesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwArticuloDetallesImagenesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwArticuloDetallesImagenesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
