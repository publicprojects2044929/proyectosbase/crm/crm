import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwPlaneadorVentasPage } from './fw-planeador-ventas.page';

describe('FwPlaneadorVentasPage', () => {
  let component: FwPlaneadorVentasPage;
  let fixture: ComponentFixture<FwPlaneadorVentasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwPlaneadorVentasPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwPlaneadorVentasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
