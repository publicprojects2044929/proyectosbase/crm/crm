import { Component, OnInit, OnDestroy } from '@angular/core';
import { SessionService } from 'src/app/Core/Session/session.service';
import * as CryptoJS from 'crypto-js';
import { Usuario, Empresas, Filtros, Pedido, Venta, Estatus, Cliente, Tipo, Reportes } from 'src/app/Clases/Estructura';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { FormularioConfiguracion } from 'src/app/Clases/EstructuraConfiguracion';
import { ErroresService } from 'src/app/Modales/mensajes/fw-modal-msj/errores.service';
import { MensajesService } from 'src/app/Modales/mensajes/fw-modal-msj/mensajes.service';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { ManejarFechas, ManejarPdf, ManejarArchivo } from 'src/app/Manejadores/cls-procedimientos';
import { DetallesVentaService, } from 'src/app/Modales/detalles-venta/fw-modal-detalles-venta/detalles-venta.service';
import { PieService } from 'src/app/Core/Graficos/Pie/pie.service';
import { BarrasService } from 'src/app/Core/Graficos/Barras/barras.service';
import { Subscription } from 'rxjs';
import { ConfiguracionService } from 'src/app/Core/Configuracion/configuracion.service';
import { Barra, DataSetBarra, Pie, DataSetPie } from 'src/app/Clases/EstructuraGrafico';

@Component({
  selector: 'app-fw-planeador-ventas',
  templateUrl: './fw-planeador-ventas.page.html',
  styleUrls: ['./fw-planeador-ventas.page.scss']
})
export class FwPlaneadorVentasPage implements OnInit, OnDestroy {

  tTitulo: string = "Reporte de ventas";
  ConfiguracionFormulario: FormularioConfiguracion = new FormularioConfiguracion();
  tCodFormularioActual: string = "fPlaneadorVentas";
  tManejarPdf: ManejarPdf = new ManejarPdf();
  tManejarArchivos: ManejarArchivo = new ManejarArchivo();

  tUsuarioActual: Usuario = new Usuario();
  tEmpresaActual: Empresas = new Empresas();
  tEmpresa$: Subscription;

  tPosicion: number = 0;
  tPagina: number = 1;
  tTamanoPag: number = 5;

  tFiltros: Filtros[] = [];
  tFiltroActual: Filtros = new Filtros();
  tFiltroBusqueda: string = "";

  tVentas: Venta[] = [];

  tEstatus: Estatus[] = [];
  tEstatusActuales: Estatus[] = [];

  tSituacion: string[] = ['OPERATIVO', 'VENCIDO'];
  tSituacionesActuales: string[] = [];

  tTipos: Tipo[] = [];
  tTiposActuales: Tipo[] = [];

  tClienteActual: Cliente = new Cliente();
  tClientePedidoActual: Pedido = new Pedido();

  tFechasCreacion: Date[] = [];
  tFiltrarFechaC: boolean = true;
  tFechasVencimiento: Date[] = [];
  tFiltrarFechaVenc: boolean = false;
  tManejarFecha: ManejarFechas = new ManejarFechas();

  tConfiguracionEstatus: {};
  tConfiguracionSituacion: {};
  tConfiguracionTipo: {};

  tElementoTipo: any;
  tTipoPie: boolean = true;
  tDatosBarraTipo: Barra = new Barra();
  tDatosPieTipo: Pie = new Pie();

  tElementoSituacion: any;
  tSituacionPie: boolean = true;
  tDatosBarraSituacion: Barra = new Barra();
  tDatosPieSituacion: Pie = new Pie();

  tElementoEstatus: any;
  tEstatusPie: boolean = true;
  tDatosBarraEstatus: Barra = new Barra();
  tDatosPieEstatus: Pie = new Pie();

  tConfiguracion$: Subscription;
  tHayConfiguracionWs$: Subscription;
  tStrConfiguracion: string = 'PlaneadorVentas/PlaneadorVenta.Config.json';

  constructor(public router: Router,
    public Ws: ClsServiciosService,
    private tConfiguracionService: ConfiguracionService,
    private tSesion: SessionService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService,
    private tDetallesVentaService: DetallesVentaService,
    private tPieService: PieService,
    private tBarrasService: BarrasService) {

    this.tPosicion = 0;
    this.tBarrasService.Etiqueta = "Total";

    this.tFiltros = [

      { Codfiltro: "Codventa", Filtro: "Venta" },
      { Codfiltro: "Codpedido", Filtro: "Pedido" },
      { Codfiltro: "Descripcion", Filtro: "Nombre Cliente" },
      { Codfiltro: "Codcliente", Filtro: "Cliente" }

    ]

    this.tConfiguracionEstatus = {
      singleSelection: false,
      idField: 'Codestatus',
      textField: 'Codestatus',
      selectAllText: 'Seleccionar todos',
      unSelectAllText: 'Deseleccionar todos',
      itemsShowLimit: 3,
      allowSearchFilter: true,
      searchPlaceholderText: "Buscar",
      noDataAvailablePlaceholderText: "Sin estatus registrados",
      closeDropDownOnSelection: true
    };

    this.tConfiguracionSituacion = {
      singleSelection: false,
      idField: 'Descripcion',
      textField: 'Descripcion',
      selectAllText: 'Seleccionar todos',
      unSelectAllText: 'Deseleccionar todos',
      itemsShowLimit: 3,
      allowSearchFilter: true,
      searchPlaceholderText: "Buscar",
      noDataAvailablePlaceholderText: "Sin situaciones registradas",
      closeDropDownOnSelection: true
    };

    this.tConfiguracionTipo = {
      singleSelection: false,
      idField: 'Codtipo',
      textField: 'Codtipo',
      selectAllText: 'Seleccionar todos',
      unSelectAllText: 'Deseleccionar todos',
      itemsShowLimit: 3,
      allowSearchFilter: true,
      searchPlaceholderText: "Buscar",
      noDataAvailablePlaceholderText: "Sin tipos registrados",
      closeDropDownOnSelection: true
    };

  }

  ngOnInit() {

    this.tFechasCreacion = [new Date(), new Date()];
    this.tFechasVencimiento = [new Date(), new Date()];

    this.tConfiguracion$ = this.tConfiguracionService
      .Consumir_Obtener_Configuracion(this.tStrConfiguracion)
      .subscribe((tConfiguracion: FormularioConfiguracion) => {

        this.ConfiguracionFormulario = tConfiguracion;
        this.tTitulo = this.ConfiguracionFormulario.Titulo;

      });

    this.tEmpresa$ = this.tSesion.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;
      this.tBarrasService.Simbolo = this.tEmpresaActual.Simbolo;

    })

    var tTienePermiso: boolean = this.tSesion.ObtenerPermisoAcceso(this.tCodFormularioActual); //false;

    if (tTienePermiso == true) {

      this.tFiltroActual = this.tFiltros[0];

      this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
        .subscribe((tHayConfig: boolean) => {

          if (tHayConfig === true) {

            this.Buscar_Estatus();
            this.Buscar_Tipos();
            this.Buscar_Ventas();

          }

        });

    }
    else {

      this.tMsjServices.AbrirModalMsj(environment.msjSinAcceso);
      this.router.navigate(['/Home']);

    }

  }

  ngOnDestroy() {

    this.tEmpresa$.unsubscribe();
    this.tConfiguracion$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();

  }

  //#region "Funciones Modal"

  AbrirModalDetalles(tVentaModal: Venta): void {

    this.tDetallesVentaService
      .AbrirModalConf(tVentaModal)
      .then(result => {



      }, reason => {



      });


  }

  //#endregion

  //#region "Funciones de busqueda"

  Buscar_Estatus(): void {

    this.Ws
      .Estatus()
      .Consumir_Obtener_Estatus(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        "TR_VENTA")
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tEstatus = Respuesta.Datos;

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Buscar_Tipos(): void {

    this.tTipos = [];

    this.Ws
      .Tipo()
      .Consumir_Obtener_TipoColumna(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        "TR_VENTA",
        "TIPO")
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tTipos = Respuesta.Datos;

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Buscar_Ventas(): void {

    this.tVentas = [];
    var tEstatusFiltros: string = '';
    var tSituacionFiltros: string = '';
    var tTipoFiltros: string = '';
    var tFechacreacionI: number = 0;
    var tFechacreacionF: number = 0;
    var tFechaVencI: number = 0;
    var tFechaVencF: number = 0;

    for (let tEstatus of this.tEstatusActuales) {

      if (tEstatusFiltros === "") {

        tEstatusFiltros = "'" + tEstatus.Codestatus + "'";

      }
      else {

        tEstatusFiltros += ", '" + tEstatus.Codestatus + "'";

      }

    }

    for (let tSituacion of this.tSituacionesActuales) {

      if (tSituacionFiltros === "") {

        tSituacionFiltros = "'" + tSituacion + "'";

      }
      else {

        tSituacionFiltros += ", '" + tSituacion + "'";

      }

    }

    for (let tTipo of this.tTiposActuales) {

      if (tTipoFiltros === "") {

        tTipoFiltros = "'" + tTipo.Codtipo + "'";

      }
      else {

        tTipoFiltros += ", '" + tTipo.Codtipo + "'";

      }

    }

    if (tSituacionFiltros !== "") {

      tSituacionFiltros = "(" + tSituacionFiltros + ")";

    }

    if (tEstatusFiltros !== "") {

      tEstatusFiltros = "(" + tEstatusFiltros + ")"

    }

    if (tTipoFiltros !== "") {

      tTipoFiltros = "(" + tTipoFiltros + ")"

    }

    if (this.tFiltrarFechaC === true) {

      tFechacreacionI = this.tManejarFecha.Seleccionar_Fecha(this.tFechasCreacion[0]);
      tFechacreacionF = this.tManejarFecha.Seleccionar_Fecha(this.tFechasCreacion[1]);

    }

    if (this.tFiltrarFechaVenc === true) {

      tFechaVencI = this.tManejarFecha.Seleccionar_Fecha(this.tFechasVencimiento[0]);
      tFechaVencF = this.tManejarFecha.Seleccionar_Fecha(this.tFechasVencimiento[1]);

    }

    this.Ws
      .Venta()
      .Consumir_Obtener_VentaPlaneador(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        tEstatusFiltros,
        tSituacionFiltros,
        tTipoFiltros,
        tFechacreacionI,
        tFechacreacionF,
        tFechaVencI,
        tFechaVencF)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tVentas = Respuesta.Datos;

        }

        this.ArmarGraficoSituacion();
        this.ArmarGraficoTipo();
        this.ArmarGraficoEstatus();

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  Descargar_Reportes(tVentaSel: Venta): void {

    this.Ws
      .Venta()
      .Consumir_Obtener_VentaRpt(tVentaSel.Corporacion,
        tVentaSel.Empresa,
        tVentaSel.Codventa)
      .subscribe(Respuesta => {


        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          var Reporte: Reportes = Respuesta.Datos[0];
          this.tManejarPdf.AbrirPdf(Reporte);

        }

      }, error => {


        this.tErroresServices.MostrarError(error);

      });

  }

  Descargar_Excel() {

    let tVentasTemp: Venta[] = this.tVentas;

    tVentasTemp.map(tVenta => {

      delete tVenta.Id;
      delete tVenta.Corporacion;
      delete tVenta.Empresa;
      delete tVenta.Usuariocreador;
      delete tVenta.Usuariomodificador;
      delete tVenta.Fecha_creacion;
      delete tVenta.Fecha_creacionl;
      delete tVenta.Fecha_modificacion;
      delete tVenta.Fecha_modificacionl;
      delete tVenta.LFormasPagos;

    });

    this.tManejarArchivos.GenerarExcel(tVentasTemp, 'Ventas.xlsx', 'Ventas');
    event.preventDefault();

  }

  Limpiar(): void {

    this.tFechasCreacion = [new Date(), new Date()];
    this.tFechasVencimiento = [new Date(), new Date()];
    this.tFiltrarFechaC = true;
    this.tFiltrarFechaVenc = false;
    this.tEstatusActuales = [];
    this.tSituacionesActuales = [];
    this.tTiposActuales = [];
    this.Buscar_Ventas();

  }

  SeleccionarTipo(tTipoSel: any): void {

    for (let tTipo of this.tTipos) {

      if (tTipo.Codtipo === tTipoSel.Codtipo) {

        this.tTiposActuales.push(tTipo);

      }

    }

  }

  DeseleccionarTipo(tTipoSel: any): void {

    var jv: number = 0;

    for (let tTipo of this.tTipos) {

      if (tTipo.Codtipo === tTipoSel.Codtipo) {

        this.tTiposActuales.splice(jv, 1);

      }

      jv = +jv + +1;

    }

  }

  SeleccionarEstatus(tEstatusSel: any): void {

    for (let tEsta of this.tEstatus) {

      if (tEsta.Codestatus === tEstatusSel.Codestatus) {

        this.tEstatusActuales.push(tEsta);

      }

    }

  }

  DeseleccionarEstatus(tEstatusSel: any): void {

    var jv: number = 0;

    for (let tEstatus of this.tEstatusActuales) {

      if (tEstatus.Codestatus == tEstatusSel.Codestatus) {

        this.tEstatusActuales.splice(jv, 1);

      }

      jv = +jv + +1;

    }

  }

  SeleccionarSituacion(tSituacionSel: string): void {

    for (let tSituacio of this.tSituacion) {

      if (tSituacio == tSituacionSel) {

        this.tSituacionesActuales.push(tSituacio);

      }

    }

  }

  DeseleccionarSituacion(tSituacionSel: string): void {

    var jv: number = 0;

    for (let tSituacion of this.tSituacionesActuales) {

      if (tSituacion == tSituacionSel) {

        this.tSituacionesActuales.splice(jv, 1);

      }

      jv = +jv + +1;

    }

  }

  Seleccionar_Fecha(tFecha: any): void {

    // this.tFechacreacionI = this.tManejarFecha.Seleccionar_Fecha(tFecha);

  }

  TotalVentas(): number {

    let tTotal: number = 0;

    this.tVentas.forEach(tVenta => {

      tTotal += tVenta.Total;

    });

    return tTotal;

  }

  ObtenerSaldoPendiente(tVenta: Venta): number {

    let tSaldo: number = 0;

    tSaldo += tVenta.Total - tVenta.Pagado;

    if (tSaldo <= 0) {

      tSaldo = 0;

    }

    return tSaldo;

  }

  TotalPagado(): number {

    let tTotal: number = 0;

    this.tVentas.forEach(tVenta => {

      tTotal += tVenta.Pagado - tVenta.Vuelto;

    });

    return tTotal;

  }

  AbonarFactura(tVenta_Sel: Venta) {

    // let tVentaEnc: string = CryptoJS.AES.encrypt(JSON.stringify(tVenta_Sel), environment.ClaveSecreta).toString();
    let tVentaEnc: string = CryptoJS.AES.encrypt(tVenta_Sel.Codventa, environment.ClaveSecreta).toString();
    this.router.navigate(['/ReporteVentas/Abono', tVentaEnc]);


  }

  Cancelar(): void {

    this.tClienteActual = new Cliente;
    this.tClientePedidoActual = new Pedido;

  }

  ArmarGraficoSituacion(): void {

    this.tDatosBarraSituacion = new Barra();
    this.tDatosPieSituacion = new Pie();

    this.tSituacion.forEach(tSituacion => {

      let tDataSetSit: DataSetBarra = new DataSetBarra();
      let tDataSetSitPie: DataSetPie = new DataSetPie();
      let tContadorSit: number = 0;
      let tAcumuladorSit: number = 0;
      // let tColor: string = '#' + (Math.random() * 0xFFFFFF << 0).toString(16);
      tDataSetSit.Color = '#' + (Math.random() * 0xFFFFFF << 0).toString(16);
      tDataSetSit.Etiqueta = tSituacion;

      tDataSetSitPie.Color = tDataSetSit.Color;
      tDataSetSitPie.Etiqueta = tDataSetSit.Etiqueta;

      this.tVentas.forEach(tVenta => {

        if (tVenta.Situacion === tSituacion) {

          tContadorSit++;
          tAcumuladorSit += tVenta.Total;

        }

      });

      tDataSetSit.Datos = tAcumuladorSit.toString();
      tDataSetSitPie.Datos.push(tContadorSit.toString());

      this.tDatosBarraSituacion.Etiquetas.push(tSituacion);
      this.tDatosBarraSituacion.DataSet.push(tDataSetSit);

      this.tDatosPieSituacion.Etiquetas.push(tSituacion);
      this.tDatosPieSituacion.DataSet.push(tDataSetSitPie);

    });

    this.MostrarGraficoSituacion();

  }

  MostrarGraficoSituacion(): void {

    if (this.tSituacionPie === true) {

      this.tElementoSituacion = document.getElementById("canvasPieSituacion");
      this.tPieService.ArmarGrafico(this.tElementoSituacion, this.tDatosPieSituacion);

    }
    else {

      this.tElementoSituacion = document.getElementById("canvasBarraSituacion");
      this.tBarrasService.ArmarGrafico(this.tElementoSituacion, this.tDatosBarraSituacion);

    }

  }

  ArmarGraficoTipo(): void {

    this.tDatosBarraTipo = new Barra();
    this.tDatosPieTipo = new Pie();

    this.tTipos.forEach(tTipo => {

      let tDataSetTipo = new DataSetBarra();
      let tDataSetTipoPie = new DataSetPie();

      let tContadorTipo: number = 0;
      let tAcumuladorTipo: number = 0;

      tDataSetTipo.Color = tTipo.Hexadecimal; //'#' + (Math.random() * 0xFFFFFF << 0).toString(16);
      tDataSetTipo.Etiqueta = tTipo.Codtipo;

      tDataSetTipoPie.Color = tTipo.Hexadecimal; //'#' + (Math.random() * 0xFFFFFF << 0).toString(16);
      tDataSetTipoPie.Etiqueta = tTipo.Codtipo;

      this.tVentas.forEach(tVenta => {

        if (tVenta.Tipo === tTipo.Codtipo) {

          tContadorTipo++;
          tAcumuladorTipo += tVenta.Total;

        }

      });

      tDataSetTipo.Datos = tAcumuladorTipo.toString();
      tDataSetTipoPie.Datos.push(tContadorTipo.toString());

      this.tDatosBarraTipo.Etiquetas.push(tTipo.Codtipo);
      this.tDatosBarraTipo.DataSet.push(tDataSetTipo);

      this.tDatosPieTipo.Etiquetas.push(tTipo.Codtipo);
      this.tDatosPieTipo.DataSet.push(tDataSetTipoPie);

    });

    this.MostrarGraficoTipo();

  }

  MostrarGraficoTipo(): void {

    if (this.tTipoPie === true) {

      this.tElementoTipo = document.getElementById("canvasTipoPie");
      this.tPieService.ArmarGrafico(this.tElementoTipo, this.tDatosPieTipo);

    }
    else {

      this.tElementoTipo = document.getElementById("canvasTipoBarra");
      this.tBarrasService.ArmarGrafico(this.tElementoTipo, this.tDatosBarraTipo);

    }

  }

  ArmarGraficoEstatus(): void {

    this.tDatosBarraEstatus = new Barra();
    this.tDatosPieEstatus = new Pie();

    this.tEstatus.forEach(tEstatus => {

      let tDataSetEstatus: DataSetBarra = new DataSetBarra();
      let tDataSetEstatusPie: DataSetPie = new DataSetPie();

      let tContadorEstatus: number = 0;
      let tAcumuladorEstatus: number = 0;

      tDataSetEstatus.Color = tEstatus.Hexadecimal; //'#' + (Math.random() * 0xFFFFFF << 0).toString(16);
      tDataSetEstatus.Etiqueta = tEstatus.Codestatus;

      tDataSetEstatusPie.Color = tEstatus.Hexadecimal; //'#' + (Math.random() * 0xFFFFFF << 0).toString(16);
      tDataSetEstatusPie.Etiqueta = tEstatus.Codestatus;

      this.tVentas.forEach(tVenta => {

        if (tVenta.Estatus === tEstatus.Codestatus) {

          tContadorEstatus++;
          tAcumuladorEstatus += tVenta.Total;

        }

      });

      tDataSetEstatus.Datos = tAcumuladorEstatus.toString();
      tDataSetEstatusPie.Datos.push(tContadorEstatus.toString());

      this.tDatosBarraEstatus.Etiquetas.push(tEstatus.Codestatus);
      this.tDatosBarraEstatus.DataSet.push(tDataSetEstatus);

      this.tDatosPieEstatus.Etiquetas.push(tEstatus.Codestatus);
      this.tDatosPieEstatus.DataSet.push(tDataSetEstatusPie);

    });

    this.MostrarGraficoEstatus();

  }

  MostrarGraficoEstatus(): void {

    if (this.tEstatusPie === true) {

      this.tElementoEstatus = document.getElementById("canvasPieEstatus");
      this.tPieService.ArmarGrafico(this.tElementoEstatus, this.tDatosPieEstatus);

    }
    else {

      this.tElementoEstatus = document.getElementById("canvasBarraEstatus");
      this.tBarrasService.ArmarGrafico(this.tElementoEstatus, this.tDatosBarraEstatus);

    }

  }

}
