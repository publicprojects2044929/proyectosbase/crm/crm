import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { environment } from 'src/environments/environment';
import * as CryptoJS from 'crypto-js';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { Venta, Cliente, Usuario, Empresas, FormaDePago, MonedaTasas } from 'src/app/Clases/Estructura';
import { SessionService } from 'src/app/Core/Session/session.service';
import { ErroresService } from 'src/app/Modales/mensajes/fw-modal-msj/errores.service';
import { MensajesService } from 'src/app/Modales/mensajes/fw-modal-msj/mensajes.service';
import { Subscription } from 'rxjs';
import { VentaFormasPagosEntrada } from 'src/app/Clases/EstructuraEntrada';
import { ConfiguracionService } from 'src/app/Core/Configuracion/configuracion.service';
import { FormularioConfiguracion } from 'src/app/Clases/EstructuraConfiguracion';

@Component({
  selector: 'app-fw-abono',
  templateUrl: './fw-abono.page.html',
  styleUrls: ['./fw-abono.page.scss']
})
export class FwAbonoPage implements OnInit, OnDestroy {

  tTitulo: string = "Abono de facturas";
  tVentaUrl: Venta = new Venta();
  tVentaActual: Venta = new Venta();
  tClienteActual: Cliente = new Cliente();

  tUsuarioActual: Usuario = new Usuario();
  tEmpresaActual: Empresas = new Empresas();
  tEmpresa$: Subscription;
  tUsuario$: Subscription;

  tVentaStr: string = "";

  tFormasPagos: FormaDePago[] = [];

  tTasas: MonedaTasas[] = [];

  tConfiguracion$: Subscription;
  tHayConfiguracionWs$: Subscription;
  tStrConfiguracion: string = 'Abono/Abono.Config.json';

  constructor(private route: ActivatedRoute,
    private Ws: ClsServiciosService,
    private tConfiguracionService: ConfiguracionService,
    private tSesion: SessionService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService) { }

  ngOnInit() {

    this.tConfiguracion$ = this.tConfiguracionService
      .Consumir_Obtener_Configuracion(this.tStrConfiguracion)
      .subscribe((tConfiguracion: FormularioConfiguracion) => {

      });

    this.tEmpresa$ = this.tSesion.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    });

    this.tUsuario$ = this.tSesion.tUsuarioActual$.subscribe((tUsuario: Usuario) => {

      this.tUsuarioActual = tUsuario;

    })

    this.route.params.subscribe((Parametros: Params) => {

      this.tVentaStr = CryptoJS.AES.decrypt(Parametros.Venta, environment.ClaveSecreta).toString(CryptoJS.enc.Utf8);

      this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
        .subscribe((tHayConfig: boolean) => {

          if (tHayConfig === true) {

            this.ObtenerFormasDePago();
            this.ObtenerCotizaciones();
            this.ObtenerVenta(this.tVentaStr);

          }

        });


    });

  }

  ngOnDestroy() {

    this.tEmpresa$.unsubscribe();
    this.tUsuario$.unsubscribe();
    this.tConfiguracion$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();

  }

  //#region "Busqueda"

  ObtenerFormasDePago(): void {

    this.Ws
      .FormasPago()
      .Consumir_Obtener_FormasPagoCrm(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tFormasPagos = Respuesta.Datos;

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      })

  }

  ObtenerCotizaciones(): void {

    this.Ws
      .MonedasTasas()
      .Consumir_Obtener_Moneda_TasasPorDestinoActivo(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        this.tEmpresaActual.Codmoneda)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tTasas = Respuesta.Datos;

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  ObtenerVenta(tCodventa: string): void {

    this.Ws
      .Venta()
      .Consumir_Obtener_VentaFull(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        tCodventa)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tVentaActual = Respuesta.Datos[0];
          this.ObtenerCliente();
          // this.SeleccionarFormaPago();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      })

  }

  ObtenerCliente(): void {

    this.Ws
      .Cliente()
      .Consumir_Obtener_Clientes2(this.tVentaActual.Corporacion,
        this.tVentaActual.Empresa,
        this.tVentaActual.Codcliente)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tClienteActual = Respuesta.Datos[0];

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      })

  }

  //#endregion

  //#region "CRUD"

  NuevoPago(tFormaPago: VentaFormasPagosEntrada) {

    this.Ws
      .VentaFormasPagos()
      .Consumir_Crear_VentaFormasPagos(tFormaPago)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.ObtenerVenta(this.tVentaStr);

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

}
