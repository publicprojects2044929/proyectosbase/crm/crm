import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwAbonoPage } from './fw-abono.page';

describe('FwAbonoPage', () => {
  let component: FwAbonoPage;
  let fixture: ComponentFixture<FwAbonoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwAbonoPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwAbonoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
