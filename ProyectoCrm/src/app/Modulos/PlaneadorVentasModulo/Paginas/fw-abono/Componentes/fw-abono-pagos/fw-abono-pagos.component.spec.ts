import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwAbonoPagosComponent } from './fw-abono-pagos.component';

describe('FwAbonoPagosComponent', () => {
  let component: FwAbonoPagosComponent;
  let fixture: ComponentFixture<FwAbonoPagosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwAbonoPagosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwAbonoPagosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
