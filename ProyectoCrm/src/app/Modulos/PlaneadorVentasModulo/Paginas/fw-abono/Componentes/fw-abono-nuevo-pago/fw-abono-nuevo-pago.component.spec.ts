import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwAbonoNuevoPagoComponent } from './fw-abono-nuevo-pago.component';

describe('FwAbonoNuevoPagoComponent', () => {
  let component: FwAbonoNuevoPagoComponent;
  let fixture: ComponentFixture<FwAbonoNuevoPagoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwAbonoNuevoPagoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwAbonoNuevoPagoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
