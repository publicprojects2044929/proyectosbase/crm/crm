import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwAbonoCabeceraComponent } from './fw-abono-cabecera.component';

describe('FwAbonoCabeceraComponent', () => {
  let component: FwAbonoCabeceraComponent;
  let fixture: ComponentFixture<FwAbonoCabeceraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwAbonoCabeceraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwAbonoCabeceraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
