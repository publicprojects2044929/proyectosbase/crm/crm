import { Component, OnInit, Input } from '@angular/core';
import { Cliente, Empresas, Venta } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-abono-cabecera',
  templateUrl: './fw-abono-cabecera.component.html',
  styleUrls: ['./fw-abono-cabecera.component.scss']
})
export class FwAbonoCabeceraComponent implements OnInit {

  @Input('Cliente') tCliente: Cliente = new Cliente();
  @Input('Empresa') tEmpresa: Empresas = new Empresas();
  @Input('Venta') tVenta: Venta = new Venta();

  constructor() { }

  ngOnInit(): void {
  }

}
