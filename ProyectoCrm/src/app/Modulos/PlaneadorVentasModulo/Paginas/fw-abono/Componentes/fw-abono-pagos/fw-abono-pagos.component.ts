import { Component, OnInit, Input } from '@angular/core';
import { Venta, Empresas, FormaDePago, VentaFormasPagos } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-abono-pagos',
  templateUrl: './fw-abono-pagos.component.html',
  styleUrls: ['./fw-abono-pagos.component.scss']
})
export class FwAbonoPagosComponent implements OnInit {

  @Input('Venta') tVenta: Venta = new Venta();
  @Input('Empresa') tEmpresa: Empresas = new Empresas();
  @Input('FormasPago') tFormasPagos: FormaDePago[] = [];

  tPosicionFormasPagos: number = 0;
  tPaginaFormasPagos: number = 1;
  tTamanoPagFormasPagos: number = 5;

  constructor() { }

  ngOnInit(): void {
  }

  ObtenerDescripcionFormaPago(tFormaPagoVenta: VentaFormasPagos): string {

    let tDescripcion = "";
    let tFormaPago: FormaDePago = new FormaDePago();

    tFormaPago = this.tFormasPagos.find(tFormaPagoFor => {

      if (tFormaPagoFor.Corporacion == tFormaPagoVenta.Corporacion &&
        tFormaPagoFor.Empresa == tFormaPagoVenta.Empresa &&
        tFormaPagoFor.Codformapago == tFormaPagoVenta.Codformapago) {

        return tFormaPagoFor;

      }

    }) || new FormaDePago();

    tDescripcion = "[ " + tFormaPagoVenta.Codformapago + " ] " + tFormaPago.Descripcion;

    return tDescripcion;

  }

}
