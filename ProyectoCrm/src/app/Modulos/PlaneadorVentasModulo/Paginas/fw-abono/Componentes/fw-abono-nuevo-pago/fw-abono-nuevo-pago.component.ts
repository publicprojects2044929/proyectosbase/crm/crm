import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { Venta, Empresas, FormaDePago, MonedaTasas } from 'src/app/Clases/Estructura';
import { VentaFormasPagosEntrada, VentaFormasPagosReferenciaEntrada, VentaFormasPagosTasasEntrada } from 'src/app/Clases/EstructuraEntrada';
import { Location } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { MensajesService } from 'src/app/Modales/mensajes/fw-modal-msj/mensajes.service';
import { ConfirmacionService } from 'src/app/Modales/confirmacion/fw-modal-confirmacion/confirmacion.service';
import { SaldoPendientePipe } from '../../../../Pipes/saldo-pendiente.pipe';

@Component({
  selector: 'app-fw-abono-nuevo-pago',
  templateUrl: './fw-abono-nuevo-pago.component.html',
  styleUrls: ['./fw-abono-nuevo-pago.component.scss']
})
export class FwAbonoNuevoPagoComponent implements OnInit, OnChanges {

  @Input('Venta') tVenta: Venta = new Venta();
  @Input('FormasPago') tFormasPago: FormaDePago = new FormaDePago();
  @Input('Tasas') tTasas: MonedaTasas = new MonedaTasas();
  @Input('Empresa') tEmpresa: Empresas = new Empresas();
  @Output('Abono') tAbono: EventEmitter<VentaFormasPagosEntrada> = new EventEmitter();

  tSaldoPendientePipe: SaldoPendientePipe = new SaldoPendientePipe();

  tFormaPagoActual: FormaDePago = new FormaDePago();
  tTasaActual: MonedaTasas = new MonedaTasas();

  tVentaFormaPagoActual: VentaFormasPagosEntrada = new VentaFormasPagosEntrada();
  tVentaFormaPagoRefActual: VentaFormasPagosReferenciaEntrada = new VentaFormasPagosReferenciaEntrada();
  tVentaFormaPagoTasaActual: VentaFormasPagosTasasEntrada = new VentaFormasPagosTasasEntrada();

  constructor(private tlocation: Location,
              private toastr: ToastrService,
              private tMsjServices: MensajesService,
              private tConfServices: ConfirmacionService) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void{

    if('tFormasPago' in changes){

      this.tFormaPagoActual = this.tFormasPago[0] || new FormaDePago();

    }

  }

  SeleccionarFormaPago(): void {

    this.tTasaActual = this.tTasas[0] || new MonedaTasas();

    this.tVentaFormaPagoActual = new VentaFormasPagosEntrada();
    this.tVentaFormaPagoActual.Id = 0;
    this.tVentaFormaPagoActual.Corporacion = this.tVenta.Corporacion;
    this.tVentaFormaPagoActual.Empresa = this.tVenta.Empresa;
    this.tVentaFormaPagoActual.Codventa = this.tVenta.Codventa;
    this.tVentaFormaPagoActual.Codformapago = this.tFormaPagoActual.Codformapago;
    this.tVentaFormaPagoActual.Monto = this.tSaldoPendientePipe.transform(this.tVenta);
    this.tVentaFormaPagoActual.Usuariocreador = this.tVenta.Usuariocreador;
    this.tVentaFormaPagoActual.Usuariomodificador = this.tVenta.Usuariomodificador;

    this.tVentaFormaPagoActual.FormaPagoReferencia.Corporacion = this.tVenta.Corporacion;
    this.tVentaFormaPagoActual.FormaPagoReferencia.Empresa = this.tVenta.Empresa;
    this.tVentaFormaPagoActual.FormaPagoReferencia.Codformapago = this.tFormaPagoActual.Codformapago;
    this.tVentaFormaPagoActual.FormaPagoReferencia.Codventa = this.tVenta.Codventa;

    this.tVentaFormaPagoActual.FormaPagoTasa.Corporacion = this.tVenta.Corporacion;
    this.tVentaFormaPagoActual.FormaPagoTasa.Empresa = this.tVenta.Empresa;
    this.tVentaFormaPagoActual.FormaPagoTasa.Codformapago = this.tFormaPagoActual.Codformapago;
    this.tVentaFormaPagoActual.FormaPagoTasa.Codmonedadestino = this.tEmpresa.Codmoneda;
    this.tVentaFormaPagoActual.FormaPagoTasa.Codventa = this.tVenta.Codventa;

    this.tTasaActual = this.tTasas[0] || new MonedaTasas();

  }

  BloquearControlMonto(): boolean {

    let tBloquear: boolean = false;

    if (this.tFormaPagoActual.Tasa == "S") {

      tBloquear = true;

    }

    return tBloquear;

  }

  CalcularCambio(): void {

    if (this.tFormaPagoActual.Tasa == "N") {

      return;

    }

    this.tVentaFormaPagoActual.Monto = this.tTasaActual.Tasa * this.tVentaFormaPagoActual.FormaPagoTasa.Monto;

  }

  AgregarFormaPago(): void {

    if (this.tVentaFormaPagoActual.Monto <= 0) {

      return;

    }

    let tSaldoPendiente: number = this.tSaldoPendientePipe.transform(this.tVenta);

    if (tSaldoPendiente <= 0) {

      this.toastr.info("Ya no queda saldo pendiente por facturar.", "Facturación");
      return;

    }

    for (let tFormaPago of this.tVenta.LFormasPagos) {

      if (tFormaPago.Corporacion == this.tFormaPagoActual.Corporacion &&
        tFormaPago.Empresa == this.tFormaPagoActual.Empresa &&
        this.tVentaFormaPagoActual.FormaPagoReferencia.Referencia == tFormaPago.FormaPagoReferencia.Referencia &&
        tFormaPago.FormaPagoReferencia.Referencia !== "") {

        this.tMsjServices.AbrirModalMsj("Disculpe, la Referencia ya se encuentra registrada");

        return;
      }

    }

    if (this.tVentaFormaPagoActual.Monto > tSaldoPendiente && this.tFormaPagoActual.Vuelto == 'N') {

      this.tMsjServices.AbrirModalMsj("El monto para esta forma de pago no puede ser mayor al saldo pendiente de la factura.");

      return;

    }

    if (this.tVentaFormaPagoActual.FormaPagoReferencia.Referencia == "" &&
      this.tFormaPagoActual.Referencia == 'S') {

      this.tMsjServices.AbrirModalMsj("Debe indicar el nro de referencia para el pago.");

      return;

    }

    if (this.tFormaPagoActual.Tasa == "S") {

      this.tVentaFormaPagoActual.FormaPagoTasa.Codtasa = this.tTasaActual.Codtasa;
      this.tVentaFormaPagoActual.FormaPagoTasa.Codmonedaorigen = this.tTasaActual.Codmonedaorigen;

    }

    let tItem: number = 1

    this.tVenta.LFormasPagos.find(tVentaFormaPagoFor => {

      if (tVentaFormaPagoFor.Corporacion == this.tVentaFormaPagoActual.Corporacion &&
        tVentaFormaPagoFor.Empresa == this.tVentaFormaPagoActual.Empresa &&
        tVentaFormaPagoFor.Codformapago == this.tVentaFormaPagoActual.Codformapago) {

        // tVentaFormaPagoFor.Item = tItem;
        //   tVentaFormaPagoFor.FormaPagoReferencia.Item = tItem;
        //   tVentaFormaPagoFor.FormaPagoTasa.Item = tItem;
        tItem++;

      }

    })

    this.tVentaFormaPagoActual.Item = tItem;
    this.tVentaFormaPagoActual.FormaPagoReferencia.Item = tItem;
    this.tVentaFormaPagoActual.FormaPagoTasa.Item = tItem;

    this.tConfServices.AbrirModalConf("¿Desea abonar a la factura?").then(Resultado => {

      switch (Resultado) {

        case "S": {

          // this.NuevoPago();
          this.tAbono.emit(this.tVentaFormaPagoActual);
          break;

        }

      };

    });

  }

  Volver() {

    this.tlocation.back();

  }

}
