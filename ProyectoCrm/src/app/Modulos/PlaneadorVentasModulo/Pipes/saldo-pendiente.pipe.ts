import { Pipe, PipeTransform } from '@angular/core';
import { TotalPagadoPipe } from './total-pagado.pipe'
import { Venta } from 'src/app/Clases/Estructura';

@Pipe({
  name: 'saldoPendiente'
})
export class SaldoPendientePipe implements PipeTransform {

  tTotalPagadoPipe: TotalPagadoPipe = new TotalPagadoPipe();

  transform(tVenta: Venta): number {
    let tSaldoPendiente: number = 0;
    let tPagado: number = this.tTotalPagadoPipe.transform(tVenta);
    tSaldoPendiente = (tVenta.Total || 0) - tPagado;

    if (tSaldoPendiente < 0) {

      tSaldoPendiente = 0;

    }

    return tSaldoPendiente;
  }

}
