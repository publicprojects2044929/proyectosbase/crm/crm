import { Pipe, PipeTransform } from '@angular/core';
import { TotalPagadoPipe } from './total-pagado.pipe';
import { Venta, VentaFormasPagos } from 'src/app/Clases/Estructura';
import { VentaFormasPagosEntrada } from 'src/app/Clases/EstructuraEntrada';

@Pipe({
  name: 'vuelto'
})
export class VueltoPipe implements PipeTransform {

  tTotalPagoPipe: TotalPagadoPipe = new TotalPagadoPipe();

  transform(tVenta: Venta, tVentaFormaPagoActual: VentaFormasPagosEntrada): number {
    let tVuelto: number = 0;
    let tPagado: number = this.tTotalPagoPipe.transform(tVenta);
    let tPendiente: number = (tVenta.Total - tPagado) < 0 ? 0 : (tVenta.Total - tPagado);
    let tMonto: number = tVentaFormaPagoActual.Monto;

    if (tPendiente <= 0) { return tVuelto; }

    if ((tMonto - tPendiente) < 0) {

      tVuelto = 0;

    }
    else {

      tVuelto = tMonto - tPendiente;

    }

    return tVuelto;
  }

}
