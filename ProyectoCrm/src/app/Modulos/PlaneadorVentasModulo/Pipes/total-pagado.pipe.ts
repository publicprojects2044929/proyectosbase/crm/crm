import { Pipe, PipeTransform } from '@angular/core';
import { Venta } from 'src/app/Clases/Estructura';

@Pipe({
  name: 'totalPagado'
})
export class TotalPagadoPipe implements PipeTransform {

  transform(tVenta: Venta): number {
    let tPagado: number = 0;

    tPagado = tVenta.Pagado || 0;

    return tPagado;
  }

}
