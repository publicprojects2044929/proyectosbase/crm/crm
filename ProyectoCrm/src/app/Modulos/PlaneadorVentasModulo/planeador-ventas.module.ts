import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlaneadorVentasRoutingModule } from './planeador-ventas-routing.module';
import { FwPlaneadorVentasPage } from './Paginas/fw-planeador-ventas/fw-planeador-ventas.page';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { NgSelectModule } from '@ng-select/ng-select';

import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { SharedModule } from 'src/app/Shared/shared.module';
import { FwAbonoPage } from './Paginas/fw-abono/fw-abono.page';
import { FwAbonoCabeceraComponent } from './Paginas/fw-abono/Componentes/fw-abono-cabecera/fw-abono-cabecera.component';
import { FwAbonoPagosComponent } from './Paginas/fw-abono/Componentes/fw-abono-pagos/fw-abono-pagos.component';
import { FwAbonoNuevoPagoComponent } from './Paginas/fw-abono/Componentes/fw-abono-nuevo-pago/fw-abono-nuevo-pago.component';
import { TotalPagadoPipe } from './Pipes/total-pagado.pipe';
import { SaldoPendientePipe } from './Pipes/saldo-pendiente.pipe';
import { VueltoPipe } from './Pipes/vuelto.pipe';

@NgModule({
  declarations: [FwPlaneadorVentasPage, 
                FwAbonoPage, 
                FwAbonoCabeceraComponent, 
                FwAbonoPagosComponent, 
                FwAbonoNuevoPagoComponent, 
                TotalPagadoPipe, 
                SaldoPendientePipe, 
                VueltoPipe],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    BsDatepickerModule.forRoot(),
    NgMultiSelectDropDownModule.forRoot(),
    NgSelectModule,
    NgbModule,
    PlaneadorVentasRoutingModule
  ]
})
export class PlaneadorVentasModule { }
