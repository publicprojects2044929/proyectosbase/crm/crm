import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FwPlantillaComponent  } from 'src/app/Shared/Formularios/fw-plantilla/fw-plantilla.component'
import { FwPlaneadorVentasPage  } from './Paginas/fw-planeador-ventas/fw-planeador-ventas.page';
import { FwAbonoPage } from './Paginas/fw-abono/fw-abono.page';

const routes: Routes = [
  {
    path: '',
    component: FwPlantillaComponent,
    children:[
      {
        path: '',
        component: FwPlaneadorVentasPage,
        data:{
          Codformulario: 'fPlaneadorVentas'
        }
      },
      {
        path: 'Abono/:Venta',
        component: FwAbonoPage,
        data:{
          Codformulario: 'fPlaneadorVentas'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlaneadorVentasRoutingModule { }
