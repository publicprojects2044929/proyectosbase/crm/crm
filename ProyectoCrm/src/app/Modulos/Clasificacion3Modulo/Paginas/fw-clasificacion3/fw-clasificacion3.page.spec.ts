import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwClasificacion3Page } from './fw-clasificacion3.page';

describe('FwClasificacion3Page', () => {
  let component: FwClasificacion3Page;
  let fixture: ComponentFixture<FwClasificacion3Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwClasificacion3Page ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwClasificacion3Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
