import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwClasificacion3DetallesComponent } from './fw-clasificacion3-detalles.component';

describe('FwClasificacion3DetallesComponent', () => {
  let component: FwClasificacion3DetallesComponent;
  let fixture: ComponentFixture<FwClasificacion3DetallesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwClasificacion3DetallesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwClasificacion3DetallesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
