import { Component, OnInit, ViewChild, OnChanges, Input, SimpleChanges } from '@angular/core';
import { Clasificacion1, Clasificacion2, Clasificacion3, Configuracion } from 'src/app/Clases/Estructura';
import { Clasificaciones2Service } from 'src/app/Modales/clasificacion2/fw-modal-clasificaciones2/clasificaciones2.service';
import { Clasificaciones1Service } from 'src/app/Modales/clasificacion1/fw-modal-clasificaciones1/clasificaciones1.service';
import { ManejarImagenes } from 'src/app/Manejadores/cls-procedimientos';

@Component({
  selector: 'app-fw-clasificacion3-detalles',
  templateUrl: './fw-clasificacion3-detalles.component.html',
  styleUrls: ['./fw-clasificacion3-detalles.component.scss']
})
export class FwClasificacion3DetallesComponent implements OnInit, OnChanges {

  @ViewChild('flArchivo', { static: true }) tControlArchivo: any;
  @Input('Clasificacion3') tClasificacion3: Clasificacion3 = new Clasificacion3();
  @Input('Clasificaciones1') tClasificaciones1: Clasificacion1[] = [];
  @Input('Configuracion') tConfiguracion: Configuracion = new Configuracion();
  @Input('Nuevo') tNuevo: boolean = false;
  @Input('ManejarImagen') tManejarImagen: ManejarImagenes = new ManejarImagenes();

  tClasificacion1Actual: Clasificacion1 = new Clasificacion1();

  tClasificaciones2: Clasificacion2[] = [];
  tClasificacion2Actual: Clasificacion2 = new Clasificacion2();

  constructor(private tClasificacion1Service: Clasificaciones1Service,
    private tClasificacion2Service: Clasificaciones2Service) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {

    if ('tClasificaciones1' in changes) {

      this.tClasificacion1Actual = this.tClasificaciones1[0] || new Clasificacion1();
      this.tClasificaciones2 = this.tClasificacion1Actual.LClasificacion2 || [];
      this.tClasificacion2Actual = this.tClasificaciones2[0] || new Clasificacion2();

      if (this.tClasificacion3.Codclasificacion3 === '') {
        this.Seleccionar_Clasificacion2();
      }

    }

    if ('tClasificacion3' in changes) {

      this.tClasificacion1Actual = this.tClasificaciones1.find(tClasificacion1 => {
        return tClasificacion1.Corporacion === this.tClasificacion3.Corporacion &&
          tClasificacion1.Empresa === this.tClasificacion3.Empresa &&
          tClasificacion1.Codclasificacion1 === this.tClasificacion3.Codclasificacion1
      }) || this.tClasificaciones1[0] || new Clasificacion1();

      this.tClasificaciones2 = this.tClasificacion1Actual.LClasificacion2 || [];

      this.tClasificacion2Actual = this.tClasificaciones2.find(tClasificacion2 => {
        return tClasificacion2.Corporacion === this.tClasificacion3.Corporacion &&
          tClasificacion2.Empresa === this.tClasificacion3.Empresa &&
          tClasificacion2.Codclasificacion1 === this.tClasificacion3.Codclasificacion1 &&
          tClasificacion2.Codclasificacion2 === this.tClasificacion3.Codclasificacion2
      }) || this.tClasificaciones2[0] || new Clasificacion2();

    }

  }

  //#region "Funciones Modal"

  AbrirModalClasificacion1(): void {

    this.tClasificacion1Service
      .AbrirModal()
      .then(result => {

        if (result.Resultado == "S") {

          var tClasificacion1Modal: Clasificacion1 = result.Clasificacion1;
          this.tClasificacion1Actual = this.tClasificaciones1.find(tClasificacon1 => {

            return tClasificacon1.Corporacion === tClasificacion1Modal.Corporacion &&
              tClasificacon1.Empresa === tClasificacion1Modal.Empresa &&
              tClasificacon1.Codclasificacion1 === tClasificacion1Modal.Codclasificacion1

          }) || this.tClasificaciones1[0] || new Clasificacion1();

          this.Seleccionar_Clasificacion1();

        }

      }, reason => {



      });

  }

  AbrirModalClasificacion2(): void {

    this.tClasificacion2Service
      .AbrirModal(this.tClasificacion1Actual)
      .then(result => {

        if (result.Resultado == "S") {

          var tClasificacion2Modal: Clasificacion2 = result.Clasificacion2;
          this.tClasificacion2Actual = this.tClasificaciones2.find(tClasificacion2 => {
            return tClasificacion2.Corporacion == tClasificacion2Modal.Corporacion &&
              tClasificacion2.Empresa == tClasificacion2Modal.Empresa &&
              tClasificacion2.Codclasificacion1 == tClasificacion2Modal.Codclasificacion1 &&
              tClasificacion2.Codclasificacion2 == tClasificacion2Modal.Codclasificacion2
          }) || this.tClasificaciones2[0] || new Clasificacion2();
          this.Seleccionar_Clasificacion2();

        }

      }, reason => {



      });

  }

  //#endregion

  Seleccionar_Clasificacion1() {

    this.tClasificaciones2 = this.tClasificacion1Actual.LClasificacion2;
    this.tClasificacion2Actual = this.tClasificaciones2[0] || new Clasificacion2;
    this.Seleccionar_Clasificacion2();

  };

  Seleccionar_Clasificacion2() {

    this.tClasificacion3.Codclasificacion1 = this.tClasificacion2Actual.Codclasificacion1;
    this.tClasificacion3.Codclasificacion2 = this.tClasificacion2Actual.Codclasificacion2;

  }

  ObtenerImagen(): string {

    if (this.tClasificacion3.Url == "") {

      return "assets/image.png";

    }
    else {

      return this.tClasificacion3.Url;

    }

  }

  BloquearImagen(): boolean {

    if (this.tConfiguracion.Valor == "S") {

      return false;

    }
    else {

      return true;

    }

  }

  Limpiar() {

    this.tClasificacion1Actual = this.tClasificaciones1[0] || new Clasificacion1();
    this.tClasificaciones2 = this.tClasificacion1Actual.LClasificacion2 || [];
    this.tClasificacion2Actual = this.tClasificaciones2[0] || new Clasificacion2();

  }

}
