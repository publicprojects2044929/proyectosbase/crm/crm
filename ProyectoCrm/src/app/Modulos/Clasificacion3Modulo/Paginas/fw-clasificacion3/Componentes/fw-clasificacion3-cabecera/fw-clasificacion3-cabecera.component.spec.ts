import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwClasificacion3CabeceraComponent } from './fw-clasificacion3-cabecera.component';

describe('FwClasificacion3CabeceraComponent', () => {
  let component: FwClasificacion3CabeceraComponent;
  let fixture: ComponentFixture<FwClasificacion3CabeceraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwClasificacion3CabeceraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwClasificacion3CabeceraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
