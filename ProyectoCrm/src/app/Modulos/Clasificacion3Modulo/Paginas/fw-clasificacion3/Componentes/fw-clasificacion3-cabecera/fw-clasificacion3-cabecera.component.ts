import { Component, OnInit, Input } from '@angular/core';
import { Clasificacion3 } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-clasificacion3-cabecera',
  templateUrl: './fw-clasificacion3-cabecera.component.html',
  styleUrls: ['./fw-clasificacion3-cabecera.component.scss']
})
export class FwClasificacion3CabeceraComponent implements OnInit {

  @Input('Clasificacion3') tClasificacion3: Clasificacion3 = new Clasificacion3();

  constructor() { }

  ngOnInit(): void {
  }

}
