import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from 'src/app/Shared/shared.module';

import { Clasificacion3RoutingModule } from './clasificacion3-routing.module';
import { FwClasificacion3Page } from './Paginas/fw-clasificacion3/fw-clasificacion3.page';
import { FwClasificacion3CabeceraComponent } from './Paginas/fw-clasificacion3/Componentes/fw-clasificacion3-cabecera/fw-clasificacion3-cabecera.component';
import { FwClasificacion3DetallesComponent } from './Paginas/fw-clasificacion3/Componentes/fw-clasificacion3-detalles/fw-clasificacion3-detalles.component'

@NgModule({
  declarations: [FwClasificacion3Page, FwClasificacion3CabeceraComponent, FwClasificacion3DetallesComponent],
  imports: [
    CommonModule,
    Clasificacion3RoutingModule,
    SharedModule,
    FormsModule,
    NgbModule
  ]
})
export class Clasificacion3Module { }
