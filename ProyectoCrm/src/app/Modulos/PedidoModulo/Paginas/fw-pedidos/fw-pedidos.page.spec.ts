import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwPedidosPage } from './fw-pedidos.page';

describe('FwPedidosPage', () => {
  let component: FwPedidosPage;
  let fixture: ComponentFixture<FwPedidosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwPedidosPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwPedidosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
