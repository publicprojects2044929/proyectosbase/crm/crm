import { Component, OnInit, OnDestroy } from '@angular/core';
import { SocketService } from 'src/app/Core/Socket/socket.service';
import { FormularioConfiguracion } from 'src/app/Clases/EstructuraConfiguracion';
import { Usuario, Empresas, Pedido, Cliente, Respuesta, PedidoArticulo } from 'src/app/Clases/Estructura';
import { Router } from '@angular/router';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { ToastrService } from 'ngx-toastr';
import { ErroresService } from 'src/app/Modales/mensajes/fw-modal-msj/errores.service';
import { MensajesService } from 'src/app/Modales/mensajes/fw-modal-msj/mensajes.service';
import { ConfirmacionService } from 'src/app/Modales/confirmacion/fw-modal-confirmacion/confirmacion.service';
import { environment } from 'src/environments/environment';
import { SessionService } from 'src/app/Core/Session/session.service';
import { PedidoEntrada, PedidoArticuloEntrada } from 'src/app/Clases/EstructuraEntrada';
import { FacturacionService } from 'src/app/Modales/facturacion/fw-modal-facturacion/facturacion.service';
import { Subscription } from 'rxjs'
import { ConfiguracionService } from 'src/app/Core/Configuracion/configuracion.service';

@Component({
  selector: 'app-fw-pedidos',
  templateUrl: './fw-pedidos.page.html',
  styleUrls: ['./fw-pedidos.page.scss']
})
export class FwPedidosPage implements OnInit, OnDestroy {

  tConfiguracionFormulario: FormularioConfiguracion = new FormularioConfiguracion();
  tCodFormularioActual: string = "fPedido";
  tPermisoAgregar: boolean = false;
  tPermisoEditar: boolean = false;
  tPermisoEliminar: boolean = false;
  tHabilitarGuardar: boolean = false;

  tUsuarioActual: Usuario = new Usuario();
  tEmpresaActual: Empresas = new Empresas();
  tEmpresa$: Subscription;
  tUsuario$: Subscription;

  tTitulo: string = "";

  tPedidos: Pedido[] = [];
  tOrdenes: Pedido[] = [];
  tPreordenes: Pedido[] = [];
  tMisOrdenes: Pedido[] = [];
  tPedidoActual: Pedido = new Pedido();

  tClienteActual: Cliente = new Cliente();
  tClientePedidoActual: Pedido = new Pedido();

  tSocket$: Subscription;
  tConfiguracion$: Subscription;
  tHayConfiguracionWs$: Subscription;
  tHayConfiguracionSocket$: Subscription;
  tStrConfiguracion: string = 'Pedido/Pedido.Config.json';

  tMostrarOrdenes: boolean = true;
  tMostrarPreOrdenes: boolean = false;
  tMostrarMisOrdenes: boolean = false;

  constructor(private tSocket: SocketService,
    private tSesion: SessionService,
    private tConfiguracionService: ConfiguracionService,
    public router: Router,
    public Ws: ClsServiciosService,
    private toastr: ToastrService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService,
    private tConfServices: ConfirmacionService,
    private tFacturacionService: FacturacionService) {

  }

  ngOnInit() {

    this.tConfiguracion$ = this.tConfiguracionService
      .Consumir_Obtener_Configuracion(this.tStrConfiguracion)
      .subscribe((tConfiguracion: FormularioConfiguracion) => {

        this.tConfiguracionFormulario = tConfiguracion;
        this.tTitulo = this.tConfiguracionFormulario.Titulo;

      });

    this.tEmpresa$ = this.tSesion.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    })

    this.tUsuario$ = this.tSesion.tUsuarioActual$.subscribe((tUsuario: Usuario) => {

      this.tUsuarioActual = tUsuario;

    })

    var tTienePermiso: boolean = this.tSesion.ObtenerPermisoAcceso(this.tCodFormularioActual);
    this.tPermisoAgregar = this.tSesion.ObtenerPermisoAgregar();
    this.tPermisoEditar = this.tSesion.ObtenerPermisoEditar();
    this.tPermisoEliminar = this.tSesion.ObtenerPermisoEliminar();

    if (tTienePermiso == true) {

      this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
        .subscribe((tHayConfig: boolean) => {

          if (tHayConfig === true) {



          }

        });

      this.tHayConfiguracionSocket$ = this.tSocket.tHayConfig$
        .subscribe((tHayConfig: boolean) => {

          if (tHayConfig === true) {

            this.tSocket.emit("Pedidos:PedidosActivos");

            this.tSocket$ = this.tSocket
              .listen("Pedidos:PedidosActivos")
              .subscribe((tRespuesta: Respuesta) => {

                this.tPedidos = tRespuesta.Datos || [];
                this.ObtenerOrdenes();
                this.ObtenerPreOrdenes();
                this.ObtenerMisOrdenes();

              });

          }

        });

    }
    else {

      this.tMsjServices.AbrirModalMsj(environment.msjSinAcceso);
      this.router.navigate(['/Home']);

    }

  }

  ngOnDestroy(): void {

    this.tEmpresa$.unsubscribe();
    this.tUsuario$.unsubscribe();
    this.tSocket$.unsubscribe();
    this.tConfiguracion$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();
    this.tSocket.disconnect();

  }

  //#region "Funciones de busqueda"

  Buscar_Pedido(tCodpedido: string): void {

    this.tClientePedidoActual = new Pedido();
    console.log(tCodpedido);

    this.Ws
      .Pedidos()
      .Consumir_Obtener_PedidoFull(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        tCodpedido)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          // this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);
          //

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tClientePedidoActual = Respuesta.Datos[0];
          this.Buscar_Cliente(this.tClientePedidoActual.Codcliente);

        }

      }, error => {


        this.tErroresServices.MostrarError(error);

      });


  }

  Buscar_Cliente(tCodcliente: string): void {

    this.tClienteActual = new Cliente();

    this.Ws
      .Cliente()
      .Consumir_Obtener_Clientes2(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        tCodcliente)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          // this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);
          //

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tClienteActual = Respuesta.Datos[0] || new Cliente();

        }

      }, error => {


        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  //#region "Funciones Modal"

  ModalClientes(tClienteSel: Cliente): void {

    var tTienePedido: boolean = false;

    this.Cancelar();

    this.tOrdenes.forEach((tOrden) => {

      if (tOrden.Corporacion === tClienteSel.Corporacion &&
        tOrden.Empresa === tClienteSel.Empresa &&
        tOrden.Codcliente === tClienteSel.Codcliente) {

        tTienePedido = true;
        this.tPedidoActual = tOrden;
        this.Buscar_Pedido(this.tPedidoActual.Codpedido);
        return;

      }

    });

    this.tPreordenes.forEach((tPreOrden) => {

      if (tPreOrden.Corporacion === tClienteSel.Corporacion &&
        tPreOrden.Empresa === tClienteSel.Empresa &&
        tPreOrden.Codcliente === tClienteSel.Codcliente) {

        tTienePedido = true;
        this.tPedidoActual = tPreOrden;
        this.Buscar_Pedido(this.tPedidoActual.Codpedido);
        return;

      }

    });

    if (tTienePedido === false) {

      this.Buscar_Cliente(tClienteSel.Codcliente);

    }

  }

  AbrirModalFacturacion(): void {

    if (this.tClientePedidoActual.Codpedido == "") {

      this.toastr.info("No hay ningún pedido escogido", "Pedido");
      return;

    }

    this.tFacturacionService
      .AbrirModal(this.tClientePedidoActual, this.tClienteActual)
      .then(result => {

        if (result.Resultado == "S") {

          this.Cancelar();


        }

      }, reason => {



      });

  }

  //#endregion

  //#region "CRUD"

  Anular_Pedido(): void {

    let tPedidoAnular: PedidoEntrada = {

      "Id": 0,
      "Corporacion": this.tClientePedidoActual.Corporacion,
      "Empresa": this.tClientePedidoActual.Empresa,
      "Codpedido": this.tClientePedidoActual.Codpedido,
      "Codcliente": this.tClientePedidoActual.Codcliente,
      "Tipo": this.tClientePedidoActual.Tipo,
      "Estatus": this.tClientePedidoActual.Estatus,
      "Usuariocreador": this.tClientePedidoActual.Usuariocreador,
      "Usuariomodificador": this.tUsuarioActual.Codusuario,
      "LArticulos": [],
      "LCupones": [],
      "LColecciones": []

    }

    this.Ws
      .Pedidos()
      .Consumir_Anular_Pedido(tPedidoAnular)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.toastr.success("El pedido se anulo correctamente.", "Pedido");
          this.Cancelar();

        }


      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  EliminarArticulo(tPedidoArticulo: PedidoArticulo): void {


    let tArticuloEliminar: PedidoArticuloEntrada = {

      "Id": 0,
      "Corporacion": tPedidoArticulo.Corporacion,
      "Empresa": tPedidoArticulo.Empresa,
      "Codpedido": tPedidoArticulo.Codpedido,
      "Codarticulo": tPedidoArticulo.Codarticulo,
      "Talla": tPedidoArticulo.Talla,
      "Codalmacen": tPedidoArticulo.Codalmacen,
      "Cantidad": tPedidoArticulo.Cantidad,
      "Usuariocreador": tPedidoArticulo.Usuariocreador,
      "Usuariomodificador": this.tUsuarioActual.Codusuario

    }

    this.Ws
      .PedidosArticulos()
      .Consumir_Eliminar_PedidoArticulo(tArticuloEliminar)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.toastr.success("Articulo eliminado.", "Pedido");
          this.tPedidoActual.Codpedido = tPedidoArticulo.Codpedido;
          this.Buscar_Pedido(this.tPedidoActual.Codpedido);

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  ModificarArticulo({tPedidoArticulo, tCantidad}) {

    let tModificarArticulo: PedidoArticuloEntrada = {

      "Id": 0,
      "Corporacion": tPedidoArticulo.Corporacion,
      "Empresa": tPedidoArticulo.Empresa,
      "Codpedido": tPedidoArticulo.Codpedido,
      "Codarticulo": tPedidoArticulo.Codarticulo,
      "Talla": tPedidoArticulo.Talla,
      "Codalmacen": tPedidoArticulo.Codalmacen,
      "Cantidad": tCantidad,
      "Usuariocreador": tPedidoArticulo.Usuariocreador,
      "Usuariomodificador": this.tUsuarioActual.Codusuario

    }

    this.Ws
      .PedidosArticulos()
      .Consumir_Modificar_PedidoArticulo(tModificarArticulo)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          //this.toastr.success("El articulo se cargo correctamente.", "Pedido");
          // this.tClientePedidoActual = Respuesta.Datos[0];
          this.tPedidoActual.Codpedido = tPedidoArticulo.Codpedido;
          this.Buscar_Pedido(this.tPedidoActual.Codpedido);

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Aprobar_Pedido() {

    let tPedidoAprobar: PedidoEntrada = {

      "Id": 0,
      "Corporacion": this.tClientePedidoActual.Corporacion,
      "Empresa": this.tClientePedidoActual.Empresa,
      "Codpedido": this.tClientePedidoActual.Codpedido,
      "Codcliente": this.tClientePedidoActual.Codcliente,
      "Tipo": this.tClientePedidoActual.Tipo,
      "Estatus": this.tClientePedidoActual.Estatus,
      "Usuariocreador": this.tClientePedidoActual.Usuariocreador,
      "Usuariomodificador": this.tUsuarioActual.Codusuario,
      "LArticulos": [],
      "LCupones": [],
      "LColecciones": []

    }

    this.Ws
      .Pedidos()
      .Consumir_Aprobar_Pedido(tPedidoAprobar)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.toastr.success("El pedido se aprobo correctamente.", "Pedido");
          this.Cancelar();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Concluir_Pedido() {

    let tPedidoConcluir: PedidoEntrada = {

      "Id": 0,
      "Corporacion": this.tClientePedidoActual.Corporacion,
      "Empresa": this.tClientePedidoActual.Empresa,
      "Codpedido": this.tClientePedidoActual.Codpedido,
      "Codcliente": this.tClientePedidoActual.Codcliente,
      "Tipo": this.tClientePedidoActual.Tipo,
      "Estatus": this.tClientePedidoActual.Estatus,
      "Usuariocreador": this.tClientePedidoActual.Usuariocreador,
      "Usuariomodificador": this.tUsuarioActual.Codusuario,
      "LArticulos": [],
      "LCupones": [],
      "LColecciones": []

    }

    this.Ws
      .Pedidos()
      .Consumir_Concluir_Pedido(tPedidoConcluir)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.toastr.success("El pedido se concluyo correctamente", "Pedido");
          this.Cancelar();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Cerrar_Pedido() {

    let tPedidoAprobar: PedidoEntrada = {

      "Id": 0,
      "Corporacion": this.tClientePedidoActual.Corporacion,
      "Empresa": this.tClientePedidoActual.Empresa,
      "Codpedido": this.tClientePedidoActual.Codpedido,
      "Codcliente": this.tClientePedidoActual.Codcliente,
      "Tipo": this.tClientePedidoActual.Tipo,
      "Estatus": this.tClientePedidoActual.Estatus,
      "Usuariocreador": this.tClientePedidoActual.Usuariocreador,
      "Usuariomodificador": this.tUsuarioActual.Codusuario,
      "LArticulos": [],
      "LCupones": [],
      "LColecciones": []

    }

    this.Ws
      .Pedidos()
      .Consumir_Cerrar_Pedido(tPedidoAprobar)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.toastr.success("El pedido se cerro correctamente.", "Pedido");
          this.Cancelar();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  ObtenerOrdenes(): void {

    this.tOrdenes = [];

    this.tOrdenes = this.tPedidos.filter(tPedido => {

      if (tPedido.Tipo === "ORDEN" && tPedido.Codasesor !== this.tUsuarioActual.Codusuario) {

        return tPedido;

      }

    })


  }

  ObtenerPreOrdenes(): void {

    this.tPreordenes = [];

    this.tPreordenes = this.tPedidos.filter(tPedido => {

      if (tPedido.Tipo === "PREORDEN" && tPedido.Codasesor !== this.tUsuarioActual.Codusuario) {
        return tPedido;

      }

    })

  }

  ObtenerMisOrdenes(): void {

    this.tMisOrdenes = [];

    this.tMisOrdenes = this.tPedidos.filter(tPedido => {

      if (tPedido.Codasesor === this.tUsuarioActual.Codusuario) {

        return tPedido;

      }

    })

  }

  Seleccionar(tPedido: Pedido): void {

    this.tPedidoActual = new Pedido();
    this.tPedidoActual = tPedido;
    this.Buscar_Pedido(this.tPedidoActual.Codpedido);

  }

  Cancelar(): void {

    this.tClienteActual = new Cliente();
    this.tClientePedidoActual = new Pedido();
    this.tPedidoActual = new Pedido();

  }

  Anular(): void {

    if (this.tClientePedidoActual.Codpedido === "") {

      this.toastr.info("No hay ningún pedido escogido", "Pedido");
      return;

    }

    // let tMsjConf = "Confimar anular la [ " + this.tClientePedidoActual.Tipo + " ] Nro. " + this.tClientePedidoActual.Codpedido;
    let tMsjConf = `Confimar anular la [${ this.tClientePedidoActual.Tipo }] Nro. ${ this.tClientePedidoActual.Codpedido }`;

    this.tConfServices
      .AbrirModalConf(tMsjConf)
      .then(Resultado => {

        switch (Resultado) {
          case "S": {

            this.Anular_Pedido();
            break;

          }
        }

      });

  }

  Aprobar(): void {

    if (this.tClientePedidoActual.Codpedido == "") {

      this.toastr.info("No hay ningún pedido escogido", "Pedido");
      return;

    }

    // let tMsjConf: string = "Confimar aprobar la [ " + this.tClientePedidoActual.Tipo + " ] Nro. " + this.tClientePedidoActual.Codpedido;

    let tMsjConf: string = `Confimar aprobar la [ ${this.tClientePedidoActual.Tipo} ] Nro. ${this.tClientePedidoActual.Codpedido} `;

    this.tConfServices
      .AbrirModalConf(tMsjConf)
      .then(Resultado => {

        switch (Resultado) {
          case "S": {

            this.Aprobar_Pedido();
            break;

          }
        }

      });

  }

  Concluir(): void {

    if (this.tClientePedidoActual.Codpedido === "") {

      this.toastr.info("No hay ningún pedido escogido", "Pedido");
      return;

    }

    // let tMsjConf: string = "Confimar concluir la [ " + this.tClientePedidoActual.Tipo + " ] Nro. " + this.tClientePedidoActual.Codpedido;
    let tMsjConf: string = `Confimar concluir la [${this.tClientePedidoActual .Tipo} Nro. ${this.tClientePedidoActual.Codpedido} `;

    this.tConfServices
      .AbrirModalConf(tMsjConf)
      .then(Resultado => {

        switch (Resultado) {
          case "S": {

            this.Concluir_Pedido();
            break;

          }
        }

      });


  }

  Cerrar(): void {

    if (this.tClientePedidoActual.Codpedido === "") {

      this.toastr.info("No hay ningún pedido escogido", "Pedido");
      return;

    }

    // let tMsjConf: string = "Confimar cerrar la [ " + this.tClientePedidoActual.Tipo + " ] Nro. " + this.tClientePedidoActual.Codpedido;
    let tMsjConf: string = `Confirma cerrar la [${this.tClientePedidoActual.Tipo}] Nro. ${this.tClientePedidoActual.Codpedido}`;

    this.tConfServices
      .AbrirModalConf(tMsjConf)
      .then(Resultado => {

        switch (Resultado) {
          case "S": {

            this.Cerrar_Pedido();
            break;

          }
        }

      });

  }

  MostrarOrdenes() {

    this.tMostrarOrdenes = true;
    this.tMostrarPreOrdenes = false;
    this.tMostrarMisOrdenes = false;

  }

  MostrarPreOrdenes() {

    this.tMostrarOrdenes = false;
    this.tMostrarPreOrdenes = true;
    this.tMostrarMisOrdenes = false;

  }

  MostrarMisOrdenes() {

    this.tMostrarOrdenes = false;
    this.tMostrarPreOrdenes = false;
    this.tMostrarMisOrdenes = true;

  }

}
