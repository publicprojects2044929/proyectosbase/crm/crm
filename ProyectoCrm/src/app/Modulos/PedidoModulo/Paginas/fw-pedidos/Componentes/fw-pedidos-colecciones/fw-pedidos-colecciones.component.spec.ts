import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwPedidosColeccionesComponent } from './fw-pedidos-colecciones.component';

describe('FwPedidosColeccionesComponent', () => {
  let component: FwPedidosColeccionesComponent;
  let fixture: ComponentFixture<FwPedidosColeccionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwPedidosColeccionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwPedidosColeccionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
