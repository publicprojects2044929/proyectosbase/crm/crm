import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Pedido, Empresas, Cliente, PedidoArticulo } from 'src/app/Clases/Estructura';
import { PedidoArticulosService } from 'src/app/Modales/PedidoArticulos/fw-modal-pedido-articulos/pedido-articulos.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-fw-pedidos-articulos',
  templateUrl: './fw-pedidos-articulos.component.html',
  styleUrls: ['./fw-pedidos-articulos.component.scss']
})
export class FwPedidosArticulosComponent implements OnInit {

  @Input('Cliente') tCliente: Cliente = new Cliente();
  @Input('Pedido') tPedido: Pedido = new Pedido();
  @Input('Empresa') tEmpresa: Empresas = new Empresas();

  @Output('BuscarPedido') tBuscar: EventEmitter<string> = new EventEmitter();
  @Output('ModificarArticulo') tModificarArticulo: EventEmitter<{tPedidoArticulo: PedidoArticulo, tCantidad: number}> = new EventEmitter();
  @Output('EliminarArticulo') tEliminarArticulo: EventEmitter<PedidoArticulo> = new EventEmitter();

  tPosicionArticulos: number = 0;
  tPaginaArticulos: number = 1;
  tTamanoPagArticulos: number = 5;

  constructor(private tPedidoArticulosService: PedidoArticulosService,
              private toastr: ToastrService) { }

  ngOnInit(): void {
  }

  AbrirModalPedidoArticulo(): void {

    if (this.tCliente.Codcliente === '') {

      return;

    }

    this.tPedidoArticulosService
      .AbrirModal(this.tCliente, this.tPedido)
      .then(result => {

        if (result.Resultado == "S") {

          let tPedidoModal: Pedido = result.Pedido;

          if (tPedidoModal.Codpedido !== "") {

            this.tPedido = tPedidoModal;
            this.tBuscar.emit(this.tPedido.Codpedido);

          }

        }

      }, reason => {



      });

  }

  SumarCantidad(tPedidoArticulo: PedidoArticulo) {

    let tCantidad: number = tPedidoArticulo.Cantidad + 1;
    this.tModificarArticulo.emit({tPedidoArticulo, tCantidad});
    // this.ModificarArticulo(tPedidoArticulo, tCantidad);

  }

  RestarCantidad(tPedidoArticulo: PedidoArticulo) {

    let tCantidad: number = tPedidoArticulo.Cantidad - 1;

    if (tCantidad <= 0) {

      return;

    }

    if (tPedidoArticulo.Cantidad <= tPedidoArticulo.Cantidad_coleccion) {

      this.toastr.clear();
      this.toastr.warning("El artículo tiene la cantidad miníma para las colecciones del pedido.");
      return;

    }

    this.tModificarArticulo.emit({tPedidoArticulo, tCantidad});
    // this.ModificarArticulo(tPedidoArticulo, tCantidad);

  }

  Eliminar(tPedidoArticulo: PedidoArticulo){

    this.tEliminarArticulo.emit(tPedidoArticulo);

  }

}
