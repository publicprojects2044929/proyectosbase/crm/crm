import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Pedido, Empresas, PedidoColecciones, Cliente } from 'src/app/Clases/Estructura';
import { ModalDetallesService } from 'src/app/Modales/DetallesColeccion/fw-modal-detalles-coleccion/modal-detalles.service';
import { PedidoColeccionesService } from 'src/app/Modales/PedidoColecciones/fw-modal-pedido-colecciones/pedido-colecciones.service';

@Component({
  selector: 'app-fw-pedidos-colecciones',
  templateUrl: './fw-pedidos-colecciones.component.html',
  styleUrls: ['./fw-pedidos-colecciones.component.scss']
})
export class FwPedidosColeccionesComponent implements OnInit {

  @Input('Pedido') tPedido: Pedido = new Pedido();
  @Input('Empresa') tEmpresa: Empresas = new Empresas();
  @Input('Cliente') tCliente: Cliente = new Cliente();
  @Output('BuscarPedido') tBuscar: EventEmitter<string> = new EventEmitter();

  tPosicionColecciones: number = 0;
  tPaginaColecciones: number = 1;
  tTamanoPagColecciones: number = 5;

  constructor(private tPedidoColeccionesService: PedidoColeccionesService,
    private tDetallesColeccionService: ModalDetallesService) { }

  ngOnInit(): void {
  }

  AbrirModalDetellaColeccion(tColeccionSel: PedidoColecciones): void {

    this.tDetallesColeccionService
      .AbrirModal(tColeccionSel)
      .then(result => {

        if (result.Resultado === "S") {

          // this.Buscar_Pedido();
          // this.tBuscar.emit('');

        }

      }, reason => {



      });

  }

  AbrirModalPedidoColeccion(): void {

    if (this.tCliente.Codcliente === "") {

      return;

    }

    this.tPedidoColeccionesService
      .AbrirModal(this.tCliente, this.tPedido)
      .then(result => {

        if (result.Resultado === "S") {

          let tPedidoModal: Pedido = result.Pedido;

          if (tPedidoModal.Codpedido !== "") {

            this.tPedido = result.Pedido;
            this.tBuscar.emit(this.tPedido.Codpedido);
            // this.Buscar_Pedido();

          }

        }

      }, reason => {



      });

  }

}
