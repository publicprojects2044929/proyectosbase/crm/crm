import { Component, OnInit, OnChanges, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { Cliente, Pedido, Empresas } from 'src/app/Clases/Estructura';
import { ClientesService } from 'src/app/Modales/cliente/fw-modal-clientes/clientes.service';
import { FichaClienteService } from 'src/app/Modales/ficha-cliente/fw-modal-ficha-cliente/ficha-cliente.service';

@Component({
  selector: 'app-fw-pedidos-datos',
  templateUrl: './fw-pedidos-datos.component.html',
  styleUrls: ['./fw-pedidos-datos.component.scss']
})
export class FwPedidosDatosComponent implements OnInit, OnChanges {

  @Input('Cliente') tCliente: Cliente = new Cliente();
  @Input('Pedido') tPedido: Pedido = new Pedido();
  @Input('Empresa') tEmpresa: Empresas = new Empresas();

  @Output('Seleccionar') tSeleccionar: EventEmitter<Cliente> = new EventEmitter();


  tDescripcionPedido: string = "Sin pedido cargado";
  tDescripcionCliente: string = "";

    constructor(private tClienteService: ClientesService,
                private tFichaClienteService: FichaClienteService) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void{

    // this.tDescripcionCliente = "[" + this.tCliente.Codcliente + "] - " + this.tCliente.Descripcion;

    if('tPedido' in changes){

      if(this.tPedido.Codpedido === ''){

        this.tDescripcionPedido = 'Sin pedido cargado';

      }
      else{

        this.tDescripcionPedido = `${this.tPedido.Tipo} - ${this.tPedido.Codpedido}`;

      }

    }

    if('tCliente' in changes){

      if(this.tCliente.Codcliente === ''){

        this.tDescripcionCliente = '';

      }
      else{

        this.tDescripcionCliente = `[${this.tCliente.Codcliente}] - ${this.tCliente.Descripcion} `;

      }

    }

  }

  AbrirModalClientes(): void {

    this.tClienteService
      .AbrirModal()
      .then(result => {

        if (result.Resultado === "S") {

          let tClienteModal: Cliente = result.Cliente;
          this.tSeleccionar.emit(tClienteModal);

        }

      }, reason => {



      });

  }

  AbrirModalFichaCliente(): void {

    if (this.tCliente.Codcliente === "") {

      return;

    }

    this.tFichaClienteService
      .AbrirModal(this.tCliente)
      .then(result => {



      }, reason => {



      });

  }

}
