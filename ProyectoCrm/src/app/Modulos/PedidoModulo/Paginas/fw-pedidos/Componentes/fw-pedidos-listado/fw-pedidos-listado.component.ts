import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Filtros, Pedido } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-pedidos-listado',
  templateUrl: './fw-pedidos-listado.component.html',
  styleUrls: ['./fw-pedidos-listado.component.scss']
})
export class FwPedidosListadoComponent implements OnInit {

  @Input('Pedidos') tOrdenes: Pedido[] = [];
  @Output('Seleccionar') tSeleccionarPedido: EventEmitter<Pedido> = new EventEmitter();

  tPosicion: number = 0;
  tPagina: number = 1;
  tTamanoPag: number = 10;

  tFiltros: Filtros[] = [];
  tFiltroBusqueda: string = "";

  constructor() {

    this.tFiltros = [

      { Codfiltro: "Codpedido", Filtro: "Número" },
      { Codfiltro: "Codcliente", Filtro: "Nro. Cliente" },
      { Codfiltro: "Descripcion_cliente", Filtro: "Nombre" }

    ]

   }

  ngOnInit(): void {

  }

  Seleccionar(tPedidoSel: Pedido){

    this.tSeleccionarPedido.emit(tPedidoSel);

  }

}
