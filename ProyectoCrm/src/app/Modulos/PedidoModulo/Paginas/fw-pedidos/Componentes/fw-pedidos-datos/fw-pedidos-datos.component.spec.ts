import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwPedidosDatosComponent } from './fw-pedidos-datos.component';

describe('FwPedidosDatosComponent', () => {
  let component: FwPedidosDatosComponent;
  let fixture: ComponentFixture<FwPedidosDatosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwPedidosDatosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwPedidosDatosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
