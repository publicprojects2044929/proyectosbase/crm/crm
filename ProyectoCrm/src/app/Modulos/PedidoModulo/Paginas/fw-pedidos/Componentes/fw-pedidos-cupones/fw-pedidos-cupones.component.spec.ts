import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwPedidosCuponesComponent } from './fw-pedidos-cupones.component';

describe('FwPedidosCuponesComponent', () => {
  let component: FwPedidosCuponesComponent;
  let fixture: ComponentFixture<FwPedidosCuponesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwPedidosCuponesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwPedidosCuponesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
