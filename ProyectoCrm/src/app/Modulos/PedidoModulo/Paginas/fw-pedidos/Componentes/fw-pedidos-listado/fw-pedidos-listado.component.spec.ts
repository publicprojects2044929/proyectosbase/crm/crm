import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwPedidosListadoComponent } from './fw-pedidos-listado.component';

describe('FwPedidosListadoComponent', () => {
  let component: FwPedidosListadoComponent;
  let fixture: ComponentFixture<FwPedidosListadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwPedidosListadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwPedidosListadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
