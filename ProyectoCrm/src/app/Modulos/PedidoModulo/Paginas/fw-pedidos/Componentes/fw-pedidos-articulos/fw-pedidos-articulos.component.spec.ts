import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwPedidosArticulosComponent } from './fw-pedidos-articulos.component';

describe('FwPedidosArticulosComponent', () => {
  let component: FwPedidosArticulosComponent;
  let fixture: ComponentFixture<FwPedidosArticulosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwPedidosArticulosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwPedidosArticulosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
