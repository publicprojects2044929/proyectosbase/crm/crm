import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from 'src/app/Shared/shared.module';

import { PedidoRoutingModule } from './pedido-routing.module';
import { FwPedidosPage } from './Paginas/fw-pedidos/fw-pedidos.page';
import { FwPedidosListadoComponent } from './Paginas/fw-pedidos/Componentes/fw-pedidos-listado/fw-pedidos-listado.component';
import { FwPedidosDatosComponent } from './Paginas/fw-pedidos/Componentes/fw-pedidos-datos/fw-pedidos-datos.component';
import { FwPedidosArticulosComponent } from './Paginas/fw-pedidos/Componentes/fw-pedidos-articulos/fw-pedidos-articulos.component';
import { FwPedidosColeccionesComponent } from './Paginas/fw-pedidos/Componentes/fw-pedidos-colecciones/fw-pedidos-colecciones.component';
import { FwPedidosCuponesComponent } from './Paginas/fw-pedidos/Componentes/fw-pedidos-cupones/fw-pedidos-cupones.component';

@NgModule({
  declarations: [FwPedidosPage, FwPedidosListadoComponent, FwPedidosDatosComponent, FwPedidosArticulosComponent, FwPedidosColeccionesComponent, FwPedidosCuponesComponent],
  imports: [
    CommonModule,
    PedidoRoutingModule,
    SharedModule,
    FormsModule,
    NgbModule
  ]
})
export class PedidoModule { }
