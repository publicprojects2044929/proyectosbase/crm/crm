import { Component, OnInit, Input } from '@angular/core';
import { Municipio } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-municipio-cabecera',
  templateUrl: './fw-municipio-cabecera.component.html',
  styleUrls: ['./fw-municipio-cabecera.component.scss']
})
export class FwMunicipioCabeceraComponent implements OnInit {

  @Input('Municipio') tMunicipio: Municipio = new Municipio();

  constructor() { }

  ngOnInit(): void {
  }

}
