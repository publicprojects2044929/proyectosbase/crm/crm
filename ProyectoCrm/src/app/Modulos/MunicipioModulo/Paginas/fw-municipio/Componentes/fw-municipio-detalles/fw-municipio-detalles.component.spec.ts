import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwMunicipioDetallesComponent } from './fw-municipio-detalles.component';

describe('FwMunicipioDetallesComponent', () => {
  let component: FwMunicipioDetallesComponent;
  let fixture: ComponentFixture<FwMunicipioDetallesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwMunicipioDetallesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwMunicipioDetallesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
