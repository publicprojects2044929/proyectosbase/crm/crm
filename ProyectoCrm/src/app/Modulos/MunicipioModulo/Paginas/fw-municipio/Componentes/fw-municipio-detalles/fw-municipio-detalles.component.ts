import { Component, OnInit, OnChanges, SimpleChanges, Input } from '@angular/core';
import { Pais, Municipio, Estado } from 'src/app/Clases/Estructura';
import { PaisesService } from 'src/app/Modales/pais/fw-modal-pais/paises.service';
import { EstadosService } from 'src/app/Modales/estado/fw-modal-estado/estados.service';

@Component({
  selector: 'app-fw-municipio-detalles',
  templateUrl: './fw-municipio-detalles.component.html',
  styleUrls: ['./fw-municipio-detalles.component.scss']
})
export class FwMunicipioDetallesComponent implements OnInit, OnChanges {

  @Input('Municipio') tMunicipio: Municipio = new Municipio();
  @Input('Paises') tPaises: Pais[] = [];
  @Input('Nuevo') tNuevo: boolean = false;

  tPaisActual: Pais = new Pais();

  tEstados: Estado[] = [];
  tEstadoActual: Estado = new Estado();

  constructor(private tPaisService: PaisesService,
    private tEstadoService: EstadosService) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {

    if('tPaises' in changes){

      this.tPaisActual  = this.tPaises[0] || new Pais();
      this.Seleccionar_Pais()

    }

    if('tMunicipio' in changes){

      this.tPaisActual = this.tPaises.find(tPais => {
        return tPais.Corporacion === this.tMunicipio.Corporacion &&
          tPais.Empresa === this.tMunicipio.Empresa &&
          tPais.Codpais === this.tMunicipio.Codpais
      }) || this.tPaises[0] || new Pais();

      this.tEstados = this.tPaisActual.LEstados || [];

      this.tEstadoActual = this.tEstados.find(tEstado => {
        return tEstado.Corporacion === this.tMunicipio.Corporacion &&
          tEstado.Empresa === this.tMunicipio.Empresa &&
          tEstado.Codpais === this.tMunicipio.Codpais &&
          tEstado.Codestado === this.tMunicipio.Codestado
      }) || this.tEstados[0] || new Estado();

      if(this.tMunicipio.Codmunicipio === ''){

        this.Seleccionar_Estado();

      }

    }


  }

  //#region "Funciones Modal"

  AbrirModalPais(): void {

    this.tPaisService
      .AbrirModal()
      .then(result => {

        if (result.Resultado == "S") {

          var tPaisModal: Pais = result.Pais;
          this.tPaisActual = this.tPaises.find(tPais => {
            return tPais.Corporacion === tPaisModal.Corporacion &&
              tPais.Empresa === tPaisModal.Empresa &&
              tPais.Codpais === tPaisModal.Codpais
          }) || this.tPaises[0] || new Pais();
          this.Seleccionar_Pais();

        }

      }, reason => {



      });

  }

  AbrirModalEstado(): void {

    this.tEstadoService
      .AbrirModal(this.tPaisActual)
      .then(result => {

        if (result.Resultado == "S") {

          var tEstadoModal: Estado = result.Estado;
          this.tEstadoActual = this.tEstados.find(tEstado => {
            return tEstado.Corporacion === tEstadoModal.Corporacion &&
              tEstado.Empresa === tEstadoModal.Empresa &&
              tEstado.Codpais === tEstadoModal.Codpais &&
              tEstado.Codestado === tEstadoModal.Codestado
          }) || this.tEstados[0] || new Estado();
          this.Seleccionar_Estado();

        }

      }, reason => {

      });

  }

  //#endregion

  Seleccionar_Pais() {

    this.tEstados = this.tPaisActual.LEstados || [];
    this.tEstadoActual = new Estado();
    this.tEstadoActual = this.tEstados[0] || new Estado();
    this.Seleccionar_Estado();

  }

  Seleccionar_Estado(){

    this.tMunicipio.Codpais = this.tEstadoActual.Codpais;
    this.tMunicipio.Codestado = this.tEstadoActual.Codestado;

  }

}
