import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwMunicipioCabeceraComponent } from './fw-municipio-cabecera.component';

describe('FwMunicipioCabeceraComponent', () => {
  let component: FwMunicipioCabeceraComponent;
  let fixture: ComponentFixture<FwMunicipioCabeceraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwMunicipioCabeceraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwMunicipioCabeceraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
