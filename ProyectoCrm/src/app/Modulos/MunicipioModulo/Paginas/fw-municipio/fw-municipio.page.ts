import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormularioConfiguracion, Configuracion } from 'src/app/Clases/EstructuraConfiguracion';
import { Usuario, Empresas, Pais, Estado, Municipio, Formulario, Filtros } from 'src/app/Clases/Estructura';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/Core/Session/session.service';
import { ToastrService } from 'ngx-toastr';
import { ErroresService } from 'src/app/Modales/mensajes/fw-modal-msj/errores.service';
import { MensajesService } from 'src/app/Modales/mensajes/fw-modal-msj/mensajes.service';
import { ConfirmacionService } from 'src/app/Modales/confirmacion/fw-modal-confirmacion/confirmacion.service';
import { environment } from 'src/environments/environment';
import { MunicipioEntrada } from 'src/app/Clases/EstructuraEntrada';
import { Subscription } from 'rxjs';
import { ConfiguracionService } from 'src/app/Core/Configuracion/configuracion.service';

@Component({
  selector: 'app-fw-municipio',
  templateUrl: './fw-municipio.page.html',
  styleUrls: ['./fw-municipio.page.scss']
})
export class FwMunicipioPage implements OnInit, OnDestroy {

  tCodFormularioActual: string = "fMunicipio";
  ConfiguracionFormulario: FormularioConfiguracion = new FormularioConfiguracion();
  tPermisoAgregar: boolean = false;
  tPermisoEditar: boolean = false;
  tPermisoEliminar: boolean = false;
  tHabilitarGuardar: boolean = false;

  tUsuarioActual: Usuario = new Usuario();
  tEmpresaActual: Empresas = new Empresas();
  tEmpresa$: Subscription;
  tUsuario$: Subscription;

  tPaises: Pais[] = [];

  tTitulo: string = "Gestión de Municipios";
  tNuevo: boolean = true;
  tPosicion: number = 0;
  tPagina: number = 1;
  tTamanoPag: number = 5;

  tMunicipio: Municipio[] = [];
  tMunicipioActual: Municipio = new Municipio();

  tFormularios: Formulario[] = [];

  tFiltros: Filtros[] = [];
  tFiltroActual: Filtros = new Filtros();
  tFiltroBusqueda: string = "";

  tConfiguracion: Configuracion = new Configuracion();
  tConfiguracion$: Subscription;
  tHayConfiguracionWs$: Subscription;
  tStrConfiguracion: string = 'Municipio/Municipio.Config.json';

  constructor(public Ws: ClsServiciosService,
    private router: Router,
    private tConfiguracionService: ConfiguracionService,
    private tSesion: SessionService,
    private toastr: ToastrService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService,
    private tConfServices: ConfirmacionService) {

    this.tFiltroBusqueda = "";
    this.tPosicion = 0;

    this.tFiltros = [

      { Codfiltro: "Codmunicipio", Filtro: "Código" },
      { Codfiltro: "Descripcion", Filtro: "Descripción" },

    ]

  }

  ngOnInit() {

    this.tConfiguracion$ = this.tConfiguracionService
      .Consumir_Obtener_Configuracion(this.tStrConfiguracion)
      .subscribe((tConfiguracion: FormularioConfiguracion) => {

        this.ConfiguracionFormulario = tConfiguracion;
        this.tTitulo = this.ConfiguracionFormulario.Titulo;
        this.tErroresServices.tTituloMsj = this.ConfiguracionFormulario.ModalMensaje.Titulo;
        this.tMsjServices.tTituloMsj = this.ConfiguracionFormulario.ModalMensaje.Titulo;
        this.tConfServices.tTituloConf = this.ConfiguracionFormulario.ModalConfirmacion.Titulo;

      });

    this.tEmpresa$ = this.tSesion.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    })

    this.tUsuario$ = this.tSesion.tUsuarioActual$.subscribe((tUsuario: Usuario) => {

      this.tUsuarioActual = tUsuario;

    })

    var tTienePermiso: boolean = this.tSesion.ObtenerPermisoAcceso(this.tCodFormularioActual);
    this.tPermisoAgregar = this.tSesion.ObtenerPermisoAgregar();
    this.tPermisoEditar = this.tSesion.ObtenerPermisoEditar();
    this.tPermisoEliminar = this.tSesion.ObtenerPermisoEliminar();

    if (tTienePermiso == true) {

      this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
        .subscribe((tHayConfig: boolean) => {

          if (tHayConfig === true) {

            this.Buscar_Municipios();
            this.Buscar_PaisEstado();
            this.Nuevo();

          }

        });

    }
    else {

      this.tMsjServices.AbrirModalMsj(environment.msjSinAcceso);
      this.router.navigate(['/Home']);

    }

  }

  ngOnDestroy() {

    this.tEmpresa$.unsubscribe();
    this.tUsuario$.unsubscribe();
    this.tConfiguracion$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();

  }

  //#region "Funciones de busqueda"

  Buscar_PaisEstado(): void {

    this.Ws
      .Paises()
      .Consumir_Obtener_PaisEstado(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa)
      .subscribe(Respuesta => {

        this.tPaises = [];

        if (Respuesta.Resultado == "N") {

          // this.tMsjService.AbrirModalMsj(Respuesta.Mensaje);
          //

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tPaises = Respuesta.Datos || [];

        }

      }, error => {


        this.tErroresServices.MostrarError(error);

      });

  }

  Buscar_Municipios(): void {

    this.Ws
      .Municipios()
      .Consumir_Obtener_Municipios(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          // this.tMsjService.AbrirModalMsj(Respuesta.Mensaje);
          //

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tMunicipio = Respuesta.Datos || [];
          this.tFiltroActual = this.tFiltros[0];

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  //#region "CRUD"

  Nuevo_Municipio(): void {

    var tNuevoMunicipio: MunicipioEntrada;
    tNuevoMunicipio = {

      "Id": 0,
      "Corporacion": this.tEmpresaActual.Corporacion,
      "Empresa": this.tEmpresaActual.Empresa,
      "Codpais": this.tMunicipioActual.Codpais,
      "Codestado": this.tMunicipioActual.Codestado,
      "Codmunicipio": "",
      "Descripcion": this.tMunicipioActual.Descripcion,
      "Abreviacion": this.tMunicipioActual.Abreviacion,
      "Usuariocreador": this.tUsuarioActual.Codusuario,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }

    this.Ws
      .Municipios()
      .Consumir_Crear_Municipio(tNuevoMunicipio)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.toastr.clear();
          this.toastr.success('El municipio se ha creado de manera exitosa!');
          this.tFiltroActual = this.tFiltros[0];
          this.Buscar_Municipios();
          this.Nuevo();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Modificar_Municipio(): void {

    var tModificarMunicipio: MunicipioEntrada;
    tModificarMunicipio = {
      "Id": 0,
      "Corporacion": this.tEmpresaActual.Corporacion,
      "Empresa": this.tEmpresaActual.Empresa,
      "Codpais": this.tMunicipioActual.Codpais,
      "Codestado": this.tMunicipioActual.Codestado,
      "Codmunicipio": this.tMunicipioActual.Codmunicipio,
      "Descripcion": this.tMunicipioActual.Descripcion,
      "Abreviacion": this.tMunicipioActual.Abreviacion,
      "Usuariocreador": this.tUsuarioActual.Codusuario,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }

    this.Ws
      .Municipios()
      .Consumir_Modificar_Municipio(tModificarMunicipio)
      .subscribe(Respuesta => {


        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.toastr.clear();
          this.toastr.success('El municipio se ha modificado de manera exitosa!');
          this.tFiltroActual = this.tFiltros[0];
          this.Buscar_Municipios();
          this.Nuevo();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Eliminar_Municipio(): void {

    var tEliminarMunicipio: MunicipioEntrada;
    tEliminarMunicipio = {
      "Id": 0,
      "Corporacion": this.tEmpresaActual.Corporacion,
      "Empresa": this.tEmpresaActual.Empresa,
      "Codpais": this.tMunicipioActual.Codpais,
      "Codestado": this.tMunicipioActual.Codestado,
      "Codmunicipio": this.tMunicipioActual.Codmunicipio,
      "Descripcion": this.tMunicipioActual.Descripcion,
      "Abreviacion": this.tMunicipioActual.Abreviacion,
      "Usuariocreador": this.tUsuarioActual.Codusuario,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }

    this.Ws
      .Municipios()
      .Consumir_Eliminar_Municipio(tEliminarMunicipio)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.toastr.clear();
          this.toastr.success('El municipio se ha eliminado de manera exitosa!');
          this.tFiltroActual = this.tFiltros[0];
          this.Buscar_Municipios();
          this.Nuevo();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  Nuevo(): void {

    this.tHabilitarGuardar = this.tPermisoAgregar;
    this.tNuevo = true;
    this.tMunicipioActual = new Municipio();

    this.toastr.clear();
    this.toastr.info('Por favor llene los datos necesarios para registrar un nuevo Municipio!');

  }

  Seleccionar_Municipio(tMunicipioActual_Sel: Municipio, tPos: number): void {

    // this.tControlArchio.nativeElement.value = "";
    this.tHabilitarGuardar = this.tPermisoEditar;
    this.tNuevo = false;
    this.tPosicion = tPos;
    this.tMunicipioActual = new Municipio();
    Object.assign(this.tMunicipioActual, tMunicipioActual_Sel);
    this.tMunicipioActual.Usuariomodificador = this.tUsuarioActual.Usuariomodificador;

  }

  Seleccionar_Municipio_Eliminar(tMunicipio_Sel: Municipio): void {

    // this.tControlArchio.nativeElement.value = "";
    this.tMunicipioActual = new Municipio();
    Object.assign(this.tMunicipioActual, tMunicipio_Sel);
    this.tMunicipioActual.Usuariomodificador = this.tUsuarioActual.Usuariomodificador;

    this.tConfServices
      .AbrirModalConf(this.ConfiguracionFormulario.ModalConfirmacion.Eliminacion)
      .then(Resultado => {

        switch (Resultado) {
          case "S": {

            this.Eliminar_Municipio();
            break;

          }
        }

      });

  }

  Cancelar(): void {

    this.Nuevo();

  }

  ValidarFormulario(): boolean {

    let valido: boolean = false;

    if (this.tMunicipioActual.Descripcion == "") {

      this.toastr.warning('Debe ingresar una descripciòn!', 'Disculpe!');
      return valido;

    }

    return true;
  }

  Agregar(): void {

    if (this.ValidarFormulario() == false) {

      return;

    }

    switch (this.tNuevo) {
      case true: {

        this.tConfServices
          .AbrirModalConf(this.ConfiguracionFormulario.ModalConfirmacion.Agregar)
          .then(Resultado => {

            switch (Resultado) {
              case "S": {

                this.Nuevo_Municipio();
                break;

              }
            }

          });
        break;

      }
      default: {

        this.tConfServices
          .AbrirModalConf(this.ConfiguracionFormulario.ModalConfirmacion.Edicion)
          .then(Resultado => {

            switch (Resultado) {
              case "S": {

                this.Modificar_Municipio();
                break;

              }
            }

          });
        break;

      }
    }

  }

}
