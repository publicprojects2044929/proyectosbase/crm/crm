import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwMunicipioPage } from './fw-municipio.page';

describe('FwMunicipioPage', () => {
  let component: FwMunicipioPage;
  let fixture: ComponentFixture<FwMunicipioPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwMunicipioPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwMunicipioPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
