import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from 'src/app/Shared/shared.module'

import { MunicipioRoutingModule } from './municipio-routing.module';
import { FwMunicipioPage } from './Paginas/fw-municipio/fw-municipio.page';
import { FwMunicipioCabeceraComponent } from './Paginas/fw-municipio/Componentes/fw-municipio-cabecera/fw-municipio-cabecera.component';
import { FwMunicipioDetallesComponent } from './Paginas/fw-municipio/Componentes/fw-municipio-detalles/fw-municipio-detalles.component';


@NgModule({
  declarations: [FwMunicipioPage, FwMunicipioCabeceraComponent, FwMunicipioDetallesComponent],
  imports: [
    CommonModule,
    MunicipioRoutingModule,
    SharedModule,
    FormsModule,
    NgbModule
  ]
})
export class MunicipioModule { }
