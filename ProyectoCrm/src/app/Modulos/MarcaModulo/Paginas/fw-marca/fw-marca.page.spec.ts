import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwMarcaPage } from './fw-marca.page';

describe('FwMarcaPage', () => {
  let component: FwMarcaPage;
  let fixture: ComponentFixture<FwMarcaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwMarcaPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwMarcaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
