import { Component, OnInit, Input } from '@angular/core';
import { Marca } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-marca-cabecera',
  templateUrl: './fw-marca-cabecera.component.html',
  styleUrls: ['./fw-marca-cabecera.component.scss']
})
export class FwMarcaCabeceraComponent implements OnInit {

  @Input('Marca') tMarca: Marca = new Marca();

  constructor() { }

  ngOnInit(): void {
  }

}
