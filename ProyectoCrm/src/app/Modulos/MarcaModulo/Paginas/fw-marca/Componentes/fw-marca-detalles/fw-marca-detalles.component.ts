import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Fabricante, Marca } from 'src/app/Clases/Estructura';
import { FabricantesService } from 'src/app/Modales/fabricante/fw-modal-fabricantes/fabricantes.service';

@Component({
  selector: 'app-fw-marca-detalles',
  templateUrl: './fw-marca-detalles.component.html',
  styleUrls: ['./fw-marca-detalles.component.scss']
})
export class FwMarcaDetallesComponent implements OnInit, OnChanges {

  @Input('Marca') tMarca: Marca = new Marca();
  @Input('Fabricantes') tFabricantes: Fabricante[] = [];
  @Input('Nuevo') tNuevo: boolean = false;

  tFabricanteActual: Fabricante = new Fabricante();

  constructor(private tFabricanteService: FabricantesService) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {

    if ('tFabricantes' in changes) {

      this.tFabricanteActual = this.tFabricantes[0] || new Fabricante();
      this.SeleccionarFabricante();

    }

    if ('tMarca' in changes) {

      this.tFabricanteActual = this.tFabricantes.find(tFabricante => {
        return tFabricante.Corporacion === this.tMarca.Corporacion &&
          tFabricante.Empresa === this.tMarca.Empresa &&
          tFabricante.Codfabricante === this.tMarca.Codfabricante
      }) || this.tFabricantes[0] || new Fabricante();

      if(this.tMarca.Codmarca === ''){
        this.SeleccionarFabricante();
      }

    }

  }

  //#region "Funciones Modal"

  AbrirModalFabricante(): void {

    this.tFabricanteService
      .AbrirModal()
      .then(result => {

        if (result.Resultado == "S") {

          var tFabricanteModal: Fabricante = result.Fabricante;
          this.tFabricanteActual = this.tFabricantes.find(tFabricante => {
            return tFabricante.Corporacion === tFabricanteModal.Corporacion &&
              tFabricante.Empresa === tFabricanteModal.Empresa &&
              tFabricante.Codfabricante === tFabricanteModal.Codfabricante
          }) || this.tFabricantes[0] || new Fabricante();
          this.SeleccionarFabricante();


        }

      }, reason => {



      });

  }

  //#endregion

  SeleccionarFabricante() {
    this.tMarca.Codfabricante = this.tFabricanteActual.Codfabricante;
  }

}
