import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwMarcaDetallesComponent } from './fw-marca-detalles.component';

describe('FwMarcaDetallesComponent', () => {
  let component: FwMarcaDetallesComponent;
  let fixture: ComponentFixture<FwMarcaDetallesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwMarcaDetallesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwMarcaDetallesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
