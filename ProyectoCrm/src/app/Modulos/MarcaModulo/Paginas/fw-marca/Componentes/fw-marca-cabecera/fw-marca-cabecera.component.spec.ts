import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwMarcaCabeceraComponent } from './fw-marca-cabecera.component';

describe('FwMarcaCabeceraComponent', () => {
  let component: FwMarcaCabeceraComponent;
  let fixture: ComponentFixture<FwMarcaCabeceraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwMarcaCabeceraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwMarcaCabeceraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
