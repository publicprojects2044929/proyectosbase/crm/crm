import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from 'src/app/Shared/shared.module'

import { MarcasRoutingModule } from './marcas-routing.module';
import { FwMarcaPage } from './Paginas/fw-marca/fw-marca.page';
import { FwMarcaCabeceraComponent } from './Paginas/fw-marca/Componentes/fw-marca-cabecera/fw-marca-cabecera.component';
import { FwMarcaDetallesComponent } from './Paginas/fw-marca/Componentes/fw-marca-detalles/fw-marca-detalles.component';

@NgModule({
  declarations: [FwMarcaPage, FwMarcaCabeceraComponent, FwMarcaDetallesComponent],
  imports: [
    CommonModule,
    MarcasRoutingModule,
    SharedModule,
    FormsModule,
    NgbModule
  ]
})
export class MarcasModule { }
