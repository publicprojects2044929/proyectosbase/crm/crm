import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from 'src/app/Shared/shared.module';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

import { ColeccionesModuloRoutingModule } from './colecciones-modulo-routing.module';
import { FwColeccionesPage } from './Paginas/fw-colecciones/fw-colecciones.page';
import { FiltrarInventarioPipe } from './Pipe/filtrar-inventario.pipe';
import { FwColeccionesCabeceraComponent } from './Paginas/fw-colecciones/Componentes/fw-colecciones-cabecera/fw-colecciones-cabecera.component';
import { FwColeccionesArticuloComponent } from './Paginas/fw-colecciones/Componentes/fw-colecciones-articulo/fw-colecciones-articulo.component';
import { FwColeccionesArticulosComponent } from './Paginas/fw-colecciones/Componentes/fw-colecciones-articulos/fw-colecciones-articulos.component';
import { FwColeccionesTotalComponent } from './Paginas/fw-colecciones/Componentes/fw-colecciones-total/fw-colecciones-total.component';

@NgModule({
  declarations: [
    FwColeccionesPage,
    FiltrarInventarioPipe,
    FwColeccionesCabeceraComponent,
    FwColeccionesArticuloComponent,
    FwColeccionesArticulosComponent,
    FwColeccionesTotalComponent
  ],
  imports: [
    CommonModule,
    ColeccionesModuloRoutingModule,
    SharedModule,
    FormsModule,
    NgbModule,
    BsDatepickerModule.forRoot()
  ],
  exports: [
    FiltrarInventarioPipe,
    BsDatepickerModule
  ]
})
export class ColeccionesModuloModule { }
