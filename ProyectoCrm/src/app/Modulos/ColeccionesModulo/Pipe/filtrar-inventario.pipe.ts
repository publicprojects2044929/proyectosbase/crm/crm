import { Pipe, PipeTransform } from '@angular/core';
import { ArticuloInventario, ColeccionArticulos, Coleccion } from 'src/app/Clases/Estructura';

@Pipe({
  name: 'filtrarInventario'
})
export class FiltrarInventarioPipe implements PipeTransform {

  transform(tArticuloInvActual: ArticuloInventario[], tArticuloInv: ArticuloInventario[], tArticuloColecciones: ColeccionArticulos[]): any {

    tArticuloInvActual = [];

    if (!tArticuloInv) {

      return;

    }

    if (!tArticuloColecciones) {

      return;

    }

    tArticuloInvActual = tArticuloInv.filter(tArticulo => {

      let tExiste: number = -1;

      tArticuloColecciones.findIndex((tArticuloColeccion, tPos) => {

        if (tArticulo.Corporacion === tArticuloColeccion.Corporacion &&
          tArticulo.Empresa === tArticuloColeccion.Empresa &&
          tArticulo.Codarticulo === tArticuloColeccion.Codarticulo &&
          tArticulo.Talla === tArticuloColeccion.Talla) {

          tExiste = tPos;
          return;

        }

      });

      if (tExiste < 0) {

        return tArticulo;

      }

    }) || [];

    return tArticuloInvActual;

  }

}
