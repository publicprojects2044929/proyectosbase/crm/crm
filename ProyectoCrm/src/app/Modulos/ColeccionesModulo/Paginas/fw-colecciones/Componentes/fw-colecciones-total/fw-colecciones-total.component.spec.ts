import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwColeccionesTotalComponent } from './fw-colecciones-total.component';

describe('FwColeccionesTotalComponent', () => {
  let component: FwColeccionesTotalComponent;
  let fixture: ComponentFixture<FwColeccionesTotalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwColeccionesTotalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwColeccionesTotalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
