import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { Coleccion, Estatus, Empresas, Tipo } from 'src/app/Clases/Estructura';
import { ManejarFechas } from 'src/app/Manejadores/cls-procedimientos';

@Component({
  selector: 'app-fw-colecciones-cabecera',
  templateUrl: './fw-colecciones-cabecera.component.html',
  styleUrls: ['./fw-colecciones-cabecera.component.scss']
})
export class FwColeccionesCabeceraComponent implements OnInit, OnChanges {

  @Input('Coleccion') tColeccion: Coleccion = new Coleccion();
  @Input('BloquearBuscar') tBloquearBuscar: boolean = false;
  @Input('Editar') tEditar: boolean = false;
  @Input('Estatus') tEstatus: Estatus[] = [];
  @Input('Empresa') tEmpresa: Empresas = new Empresas();
  @Input('TiposDesc') tTiposDescuentos: Tipo[] = [];

  @Output('AbrirModal') tAbrirModal: EventEmitter<string> = new EventEmitter();
  @Output('Buscar') tBuscar: EventEmitter<string> = new EventEmitter();

  tManejarFechas: ManejarFechas = new ManejarFechas();

  tEstatusActual: Estatus = new Estatus();
  tTiposDescuentoActual: Tipo = new Tipo();
  tFechas: Date[];

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {

    if ('tEstatus' in changes) {

      this.tEstatusActual = this.tEstatus[0] || new Estatus();
      this.CambiarEstatus();

    }

    if ('tTiposDescuentos' in changes) {

      this.tTiposDescuentoActual = this.tTiposDescuentos[0] || new Tipo();
      this.CambiarTipoDescuento();

    }

    if ('tColeccion' in changes) {

      this.tEstatusActual = this.tEstatus.find(tEst => {
        return tEst.Corporacion === this.tColeccion.Corporacion &&
          tEst.Empresa === this.tColeccion.Empresa &&
          tEst.Codestatus === this.tColeccion.Estatus
      }) || this.tEstatus[0] || new Estatus();

      this.tTiposDescuentoActual = this.tTiposDescuentos.find(tTipoDesc => {
        return tTipoDesc.Corporacion === this.tColeccion.Corporacion &&
          tTipoDesc.Empresa === this.tColeccion.Empresa &&
          tTipoDesc.Codtipo === this.tColeccion.Tipodescuento
      }) || this.tTiposDescuentos[0] || new Tipo();


      if (this.tColeccion.Codcoleccion === '') {

        this.CambiarEstatus();
        this.CambiarTipoDescuento();

        this.tFechas = [new Date(), new Date()];

      } else {

        let tFechaI: Date;
        let tFechaF: Date;

        tFechaI = new Date(this.tManejarFechas.Formatear_Fecha(this.tColeccion.Fecha_inicio, "yyyy-MM-dd"));
        tFechaF = new Date(this.tManejarFechas.Formatear_Fecha(this.tColeccion.Fecha_fin, "yyyy-MM-dd"));
        this.tFechas = [tFechaI, tFechaF];

      }


    }

  }

  AbrirModal() {
    this.tAbrirModal.emit('');
  }

  Buscar() {

    if (this.tColeccion.Codcoleccion === '') { return; }

    this.tBuscar.emit(this.tColeccion.Codcoleccion);

  }

  CambiarEstatus() {
    this.tColeccion.Estatus = this.tEstatusActual.Codestatus;
  }

  CambiarTipoDescuento() {
    this.tColeccion.Tipodescuento = this.tTiposDescuentoActual.Codtipo;
  }

  cambiarFechas() {

    let tFechaI: number = this.tManejarFechas.Seleccionar_Fecha(this.tFechas[0]);
    let tFechaF: number = this.tManejarFechas.Seleccionar_Fecha(this.tFechas[1]);

    this.tColeccion.Fecha_iniciol = tFechaI;
    this.tColeccion.Fecha_finl = tFechaF;

  }

}
