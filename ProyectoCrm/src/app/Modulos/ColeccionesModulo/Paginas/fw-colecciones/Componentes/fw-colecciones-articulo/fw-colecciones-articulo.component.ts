import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Articulo, ArticuloInventario, ColeccionArticulos, Coleccion, Empresas, Usuario } from 'src/app/Clases/Estructura';
import { ToastrService } from 'ngx-toastr';
import { ColeccionArticulosEntrada } from 'src/app/Clases/EstructuraEntrada';

@Component({
  selector: 'app-fw-colecciones-articulo',
  templateUrl: './fw-colecciones-articulo.component.html',
  styleUrls: ['./fw-colecciones-articulo.component.scss']
})
export class FwColeccionesArticuloComponent implements OnInit {

  @Input('Articulo') tArticulo: Articulo = new Articulo();
  @Input('Inventario') tInventario: ArticuloInventario[] = [];
  @Input('Coleccion') tColeccion: Coleccion = new Coleccion();
  @Input('Empresa') tEmpresa: Empresas = new Empresas();
  @Input('Usuario') tUsuario: Usuario = new Usuario();
  @Input('Nuevo') tNuevo: boolean = false;
  @Input('InventarioActual') tArticuloInvActual: ArticuloInventario[] = [];

  @Output('AbrirModal') tAbrirModal: EventEmitter<string> = new EventEmitter();
  @Output('AgregarArticulo') tAgregar: EventEmitter<ColeccionArticulosEntrada> = new EventEmitter();

  tPosicionInv: number = 0;
  tPaginaInv: number = 1;
  tTamanoPagInv: number = 5;


  constructor(private toastr: ToastrService) { }

  ngOnInit(): void {
  }

  AbrirModal() {
    this.tAbrirModal.emit('');
  }

  ObtenerTalla(tTallaFor: ArticuloInventario): string {

    let tTalla: string = "NA";

    if (tTallaFor.Talla != "") {

      tTalla = tTallaFor.Talla;

    }

    return tTalla;

  }

  AgregarArticuloColeccion(txtCantidad: any, tArtInv: ArticuloInventario) {

    if (txtCantidad.value <= 0) {

      return;

    }

    if (txtCantidad.value % this.tArticulo.Factor_venta !== 0) {

      this.toastr.clear();
      // this.toastr.info("La cantidad tiene que ser un multiplo de " + this.tArticulo.Factor_venta);
      this.toastr.info(`La cantidad tiene que ser un multiplo de ${this.tArticulo.Factor_venta}`);

      return;

    }

    if (this.tNuevo === true) {

      let tArtCol: ColeccionArticulos = {

        "Id": 0,
        "Corporacion": this.tEmpresa.Corporacion,
        "Empresa": this.tEmpresa.Empresa,
        "Codcoleccion": this.tColeccion.Codcoleccion,
        "Codarticulo": tArtInv.Codarticulo,
        "Talla": tArtInv.Talla,
        "Cantidad": txtCantidad.value,
        "Descripcion": tArtInv.Descripcion,
        "Codcolor": this.tArticulo.Codcolor,
        "Desc_color": this.tArticulo.Desc_color,
        "Existencia": tArtInv.Existencia,
        "Reservado": tArtInv.Reservado,
        "Disponible": tArtInv.Disponible,
        "Url": this.tArticulo.Url,
        "Fecha_creacion": new Date(),
        "Fecha_modificacion": new Date(),
        "Fecha_creacionl": 0,
        "Fecha_modificacionl": 0,
        "Usuariocreador": this.tUsuario.Codusuario,
        "Usuariomodificador": this.tUsuario.Codusuario

      }

      this.tColeccion.LArticulos.push(tArtCol);
      this.tInventario = [];

    }
    else {

      let tArticuloCol: ColeccionArticulosEntrada = {

        "Id": 0,
        "Corporacion": this.tColeccion.Corporacion,
        "Empresa": this.tColeccion.Empresa,
        "Codcoleccion": this.tColeccion.Codcoleccion,
        "Codarticulo": tArtInv.Codarticulo,
        "Talla": tArtInv.Talla,
        "Cantidad": txtCantidad.value,
        "Usuariocreador": this.tColeccion.Usuariocreador,
        "Usuariomodificador": this.tUsuario.Codusuario

      };

      this.tAgregar.emit(tArticuloCol)

    }

  }

}
