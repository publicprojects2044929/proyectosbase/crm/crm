import { Component, OnInit, Input } from '@angular/core';
import { Coleccion } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-colecciones-total',
  templateUrl: './fw-colecciones-total.component.html',
  styleUrls: ['./fw-colecciones-total.component.scss']
})
export class FwColeccionesTotalComponent implements OnInit {

  @Input('Coleccion') tColeccion: Coleccion = new Coleccion();

  constructor() { }

  ngOnInit(): void {
  }

  Total_Articulos() {

    let tTotalArticulos: number = 0;

    for (let tArticulos of this.tColeccion.LArticulos) {

      if (tArticulos.Cantidad > 0) {

        tTotalArticulos += tArticulos.Cantidad;

      }
    }
     return tTotalArticulos;
  }

}
