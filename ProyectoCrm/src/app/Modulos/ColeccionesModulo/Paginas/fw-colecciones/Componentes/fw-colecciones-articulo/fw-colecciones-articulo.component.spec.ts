import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwColeccionesArticuloComponent } from './fw-colecciones-articulo.component';

describe('FwColeccionesArticuloComponent', () => {
  let component: FwColeccionesArticuloComponent;
  let fixture: ComponentFixture<FwColeccionesArticuloComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwColeccionesArticuloComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwColeccionesArticuloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
