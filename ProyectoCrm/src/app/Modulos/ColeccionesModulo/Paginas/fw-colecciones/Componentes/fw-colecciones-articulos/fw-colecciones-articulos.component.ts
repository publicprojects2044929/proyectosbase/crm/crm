import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Coleccion, ColeccionArticulos, Usuario, ArticuloInventario } from 'src/app/Clases/Estructura';
import { ColeccionArticulosEntrada } from 'src/app/Clases/EstructuraEntrada';

@Component({
  selector: 'app-fw-colecciones-articulos',
  templateUrl: './fw-colecciones-articulos.component.html',
  styleUrls: ['./fw-colecciones-articulos.component.scss']
})
export class FwColeccionesArticulosComponent implements OnInit {

  @Input('Coleccion') tColeccion: Coleccion = new Coleccion();
  @Input('Nuevo') tNuevo: boolean = false;
  @Input('Usuario') tUsuario: Usuario = new Usuario();
  @Input('InventarioActual') tArticuloInvActual: ArticuloInventario[] = [];

  @Output('EliminarArticulo') tEliminar: EventEmitter<ColeccionArticulosEntrada> = new EventEmitter();

  tPosicionArticulos: number = 0;
  tPaginaArticulos: number = 1;
  tTamanoPagArticulos: number = 5;

  constructor() { }

  ngOnInit(): void {
  }

  EliminarArticuloColeccion(tArtCol: ColeccionArticulos) {

    if (this.tNuevo === true) {

      this.tColeccion.LArticulos.find((tArticuloCol, tPosicion) => {

        if (tArtCol.Corporacion === tArticuloCol.Corporacion &&
          tArtCol.Empresa === tArticuloCol.Empresa &&
          tArtCol.Codcoleccion === tArticuloCol.Codcoleccion &&
          tArtCol.Codarticulo === tArticuloCol.Codarticulo &&
          tArtCol.Talla === tArticuloCol.Talla) {

          this.tColeccion.LArticulos.splice(tPosicion, 1);
          return

        }

      });

      this.tArticuloInvActual = [];

    }
    else {

      let tArticuloCol: ColeccionArticulosEntrada = {

        "Id": 0,
        "Corporacion": this.tColeccion.Corporacion,
        "Empresa": this.tColeccion.Empresa,
        "Codcoleccion": this.tColeccion.Codcoleccion,
        "Codarticulo": tArtCol.Codarticulo,
        "Talla": tArtCol.Talla,
        "Cantidad": tArtCol.Cantidad,
        "Usuariocreador": this.tColeccion.Usuariocreador,
        "Usuariomodificador": this.tUsuario.Codusuario

      };

      // this.Eliminar_Articulo_Coleccion(tArticuloCol);
      this.tEliminar.emit(tArticuloCol);

    }

  }

}
