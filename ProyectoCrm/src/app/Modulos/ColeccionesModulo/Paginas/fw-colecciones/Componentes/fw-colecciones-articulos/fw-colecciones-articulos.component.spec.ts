import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwColeccionesArticulosComponent } from './fw-colecciones-articulos.component';

describe('FwColeccionesArticulosComponent', () => {
  let component: FwColeccionesArticulosComponent;
  let fixture: ComponentFixture<FwColeccionesArticulosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwColeccionesArticulosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwColeccionesArticulosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
