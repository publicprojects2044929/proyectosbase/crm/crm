import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwColeccionesCabeceraComponent } from './fw-colecciones-cabecera.component';

describe('FwColeccionesCabeceraComponent', () => {
  let component: FwColeccionesCabeceraComponent;
  let fixture: ComponentFixture<FwColeccionesCabeceraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwColeccionesCabeceraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwColeccionesCabeceraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
