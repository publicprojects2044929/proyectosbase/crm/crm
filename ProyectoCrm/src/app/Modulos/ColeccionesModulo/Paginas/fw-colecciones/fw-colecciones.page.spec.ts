import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwColeccionesPage } from './fw-colecciones.page';

describe('FwColeccionesPage', () => {
  let component: FwColeccionesPage;
  let fixture: ComponentFixture<FwColeccionesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwColeccionesPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwColeccionesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
