import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormularioConfiguracion } from 'src/app/Clases/EstructuraConfiguracion';
import { Usuario, Empresas, Coleccion, Estatus, Articulo, ArticuloInventario, Tipo } from 'src/app/Clases/Estructura';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { SessionService } from 'src/app/Core/Session/session.service';
import { ToastrService } from 'ngx-toastr';
import { ErroresService } from 'src/app/Modales/mensajes/fw-modal-msj/errores.service';
import { MensajesService } from 'src/app/Modales/mensajes/fw-modal-msj/mensajes.service';
import { ConfirmacionService } from 'src/app/Modales/confirmacion/fw-modal-confirmacion/confirmacion.service';
import { ArticulosService } from 'src/app/Modales/articulo/fw-modal-articulos/articulos.service';
import { environment } from 'src/environments/environment';
import { ColeccionesService } from 'src/app/Modales/colecciones/fw-modal-colecciones/colecciones.service';
import { ColeccionEntrada, ColeccionArticulosEntrada } from 'src/app/Clases/EstructuraEntrada';
import { ConfiguracionService } from 'src/app/Core/Configuracion/configuracion.service';

@Component({
  selector: 'app-fw-colecciones',
  templateUrl: './fw-colecciones.page.html',
  styleUrls: ['./fw-colecciones.page.scss']
})
export class FwColeccionesPage implements OnInit, OnDestroy {

  tConfiguracionFormulario: FormularioConfiguracion = new FormularioConfiguracion();
  tCodFormularioActual: string = "fColecciones";
  tPermisoAgregar: boolean = false;
  tPermisoEditar: boolean = false;
  tPermisoEliminar: boolean = false;
  tHabilitarGuardar: boolean = false;
  tHabilitarEliminar: boolean = false;

  tUsuarioActual: Usuario = new Usuario();
  tEmpresaActual: Empresas = new Empresas();
  tEmpresa$: Subscription;
  tUsuario$: Subscription;

  tTitulo: string = "";
  tNuevo: boolean = true;
  tEditar: boolean = false
  tBloquearBuscar: boolean = false;

  tArticuloActual: Articulo = new Articulo();
  tArticuloInv: ArticuloInventario[] = [];

  tEstatus: Estatus[] = [];

  tColeccionActual: Coleccion = new Coleccion();

  tConfiguracion$: Subscription;
  tHayConfiguracionWs$: Subscription;
  tStrConfiguracion: string = 'Coleccion/Coleccion.Config.json';

  tTiposDescuento: Tipo[] = [];
  tArticuloInvActual: ArticuloInventario[] = [];

  constructor(public router: Router,
    public Ws: ClsServiciosService,
    private tSesion: SessionService,
    private tConfiguracionService: ConfiguracionService,
    private toastr: ToastrService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService,
    private tConfServices: ConfirmacionService,
    private tArticuloService: ArticulosService,
    private tColeccionesService: ColeccionesService) {

  }

  ngOnInit() {

    this.tConfiguracion$ = this.tConfiguracionService
      .Consumir_Obtener_Configuracion(this.tStrConfiguracion)
      .subscribe((tConfiguracion: FormularioConfiguracion) => {

        this.tConfiguracionFormulario = tConfiguracion;
        this.tTitulo = this.tConfiguracionFormulario.Titulo;
        this.tConfServices.tTituloConf = this.tConfiguracionFormulario.ModalConfirmacion.Titulo;
        this.tErroresServices.tTituloMsj = this.tConfiguracionFormulario.ModalMensaje.Titulo;
        this.tMsjServices.tTituloMsj = this.tConfiguracionFormulario.ModalMensaje.Titulo;

      });

    this.tEmpresa$ = this.tSesion.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    })

    this.tUsuario$ = this.tSesion.tUsuarioActual$.subscribe((tUsuario: Usuario) => {

      this.tUsuarioActual = tUsuario;

    })

    var tTienePermiso: boolean = this.tSesion.ObtenerPermisoAcceso(this.tCodFormularioActual);
    this.tPermisoAgregar = this.tSesion.ObtenerPermisoAgregar();
    this.tPermisoEditar = this.tSesion.ObtenerPermisoEditar();
    this.tPermisoEliminar = this.tSesion.ObtenerPermisoEliminar();

    if (tTienePermiso == true) {

      this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
        .subscribe((tHayConfig: boolean) => {

          if (tHayConfig === true) {

            this.BuscarEstatus();
            this.BuscarTipo();
            this.Nuevo();

          }

        });

    }
    else {

      this.tMsjServices.AbrirModalMsj(environment.msjSinAcceso);
      this.router.navigate(['/Home']);

    }

  }

  ngOnDestroy() {

    this.tEmpresa$.unsubscribe();
    this.tUsuario$.unsubscribe();
    this.tConfiguracion$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();

  }

  //#region "Funciones de busqueda"

  BuscarEstatus(): void {

    this.Ws
      .Estatus()
      .Consumir_Obtener_Estatus(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        'MR_COLECCION')
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tEstatus = Respuesta.Datos;

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  BuscarTipo(): void {

    this.Ws
      .Tipo()
      .Consumir_Obtener_TipoColumna(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        'MR_COLECCION',
        'TIPODESCUENTO')
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tTiposDescuento = Respuesta.Datos;

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  BuscarColeccion(tCodcoleccion: string): void {

    this.Ws.Coleccion()
      .Consumir_ObtenerColeccionesFull(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        tCodcoleccion)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          var tColeccion: Coleccion = Respuesta.Datos[0] || new Coleccion();
          this.tEditar = true;
          this.tNuevo = false;
          this.tHabilitarGuardar = this.tPermisoEditar;
          this.tHabilitarEliminar = this.tPermisoEliminar;
          this.tBloquearBuscar = true;
          this.tColeccionActual = tColeccion;

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  BuscarArticulo(tCodarticulo: string): void {

    this.Ws
      .Articulo()
      .Consumir_Obtener_ArticuloInventario(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        tCodarticulo)
      .subscribe(Respuesta => {

        this.tArticuloActual = new Articulo();

        if (Respuesta.Resultado == "N") {

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tArticuloActual = Respuesta.Datos[0] || new Articulo();
          this.Buscar_ArticuloInventario();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Buscar_ArticuloInventario(): void {

    this.Ws
      .ArticuloInventario()
      .Consumir_Obtener_ArticulosInventario2(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        this.tArticuloActual.Codarticulo)
      .subscribe(Respuesta => {

        this.tArticuloInv = [];

        if (Respuesta.Resultado == "N") {

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tArticuloInv = Respuesta.Datos;

        }


      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  //#region "Funciones Modal"

  AbrirModalArticulos(): void {

    this.tArticuloService.AbrirModal().then(Resultado => {

      if (Resultado.Resultado == "S") {

        var tArticuloModal: Articulo = Resultado.Articulo;
        this.BuscarArticulo(tArticuloModal.Codarticulo);

      }

    });

  }

  AbrirModalColecciones(): void {

    this.tColeccionesService.AbrirModal().then(Resultado => {

      if (Resultado.Resultado == "S") {

        var tColeccionModal: Coleccion = Resultado.Coleccion;
        this.BuscarColeccion(tColeccionModal.Codcoleccion);

      }

    });

  }

  //#endregion

  //#region "CRUD"

  Nueva_Coleccion(): void {

    var tNuevaColeccion: ColeccionEntrada = {

      "Id": 0,
      "Corporacion": this.tEmpresaActual.Corporacion,
      "Empresa": this.tEmpresaActual.Empresa,
      "Codcoleccion": this.tColeccionActual.Codcoleccion,
      "Descripcion": this.tColeccionActual.Descripcion,
      "Estatus": this.tColeccionActual.Estatus,
      "Descuento": this.tColeccionActual.Descuento,
      "Tipodescuento": this.tColeccionActual.Tipodescuento,
      "Fecha_iniciol": this.tColeccionActual.Fecha_iniciol,
      "Fecha_finl": this.tColeccionActual.Fecha_finl,
      "Usuariocreador": this.tUsuarioActual.Codusuario,
      "Usuariomodificador": this.tUsuarioActual.Codusuario,
      "LArticulos": []

    }

    this.tColeccionActual.LArticulos.forEach(tArticulo => {

      let tArticuloCol: ColeccionArticulosEntrada = {

        "Id": 0,
        "Corporacion": this.tEmpresaActual.Corporacion,
        "Empresa": this.tEmpresaActual.Empresa,
        "Codcoleccion": tArticulo.Codcoleccion,
        "Codarticulo": tArticulo.Codarticulo,
        "Talla": tArticulo.Talla,
        "Cantidad": tArticulo.Cantidad,
        "Usuariocreador": this.tUsuarioActual.Codusuario,
        "Usuariomodificador": this.tUsuarioActual.Codusuario

      };

      tNuevaColeccion.LArticulos.push(tArticuloCol);

    });

    this.Ws
      .Coleccion()
      .Consumir_Crear_Coleccion(tNuevaColeccion)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tErroresServices.MostrarError(environment.msjError);

        }
        else {

          this.toastr.success('La colección se creo correctamente!');
          this.Nuevo();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Modificar_Coleccion(): void {

    var tModificarColeccion: ColeccionEntrada = {

      "Id": 0,
      "Corporacion": this.tColeccionActual.Corporacion,
      "Empresa": this.tColeccionActual.Empresa,
      "Codcoleccion": this.tColeccionActual.Codcoleccion,
      "Descripcion": this.tColeccionActual.Descripcion,
      "Estatus": this.tColeccionActual.Estatus,
      "Descuento": this.tColeccionActual.Descuento,
      "Tipodescuento": this.tColeccionActual.Tipodescuento,
      "Fecha_iniciol": this.tColeccionActual.Fecha_iniciol,
      "Fecha_finl": this.tColeccionActual.Fecha_finl,
      "Usuariocreador": this.tColeccionActual.Usuariocreador,
      "Usuariomodificador": this.tUsuarioActual.Codusuario,
      "LArticulos": []

    }

    this.tColeccionActual.LArticulos.forEach(tArticulo => {

      let tArticuloCol: ColeccionArticulosEntrada = {

        "Id": 0,
        "Corporacion": this.tColeccionActual.Corporacion,
        "Empresa": this.tColeccionActual.Empresa,
        "Codcoleccion": tArticulo.Codcoleccion,
        "Codarticulo": tArticulo.Codarticulo,
        "Talla": tArticulo.Talla,
        "Cantidad": tArticulo.Cantidad,
        "Usuariocreador": this.tColeccionActual.Usuariocreador,
        "Usuariomodificador": this.tUsuarioActual.Codusuario

      };

      tModificarColeccion.LArticulos.push(tArticuloCol);

    });

    this.Ws
      .Coleccion()
      .Consumir_Modificar_Coleccion(tModificarColeccion)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tErroresServices.MostrarError(environment.msjError);

        }
        else {

          this.toastr.success('La colección se modifico correctamente!');
          this.Nuevo();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Eliminar_Coleccion(): void {

    var tEliminarColeccion: ColeccionEntrada = {

      "Id": 0,
      "Corporacion": this.tColeccionActual.Corporacion,
      "Empresa": this.tColeccionActual.Empresa,
      "Codcoleccion": this.tColeccionActual.Codcoleccion,
      "Descripcion": this.tColeccionActual.Descripcion,
      "Estatus": this.tColeccionActual.Estatus,
      "Descuento": this.tColeccionActual.Descuento,
      "Tipodescuento": this.tColeccionActual.Tipodescuento,
      "Fecha_iniciol": this.tColeccionActual.Fecha_iniciol,
      "Fecha_finl": this.tColeccionActual.Fecha_finl,
      "Usuariocreador": this.tColeccionActual.Usuariocreador,
      "Usuariomodificador": this.tUsuarioActual.Codusuario,
      "LArticulos": []

    }

    this.Ws
      .Coleccion()
      .Consumir_Eliminar_Coleccion(tEliminarColeccion)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tErroresServices.MostrarError(environment.msjError);

        }
        else {

          this.toastr.success('La colección se elimino correctamente!');
          this.Nuevo();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Agregar_Articulo_Coleccion(tArticuloCol: ColeccionArticulosEntrada) {

    this.Ws
      .ColeccionArticulos()
      .Consumir_Crear_ColeccionArticulo(tArticuloCol)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tErroresServices.MostrarError(environment.msjError);

        }
        else {

          this.BuscarColeccion(this.tColeccionActual.Codcoleccion);

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Eliminar_Articulo_Coleccion(tArticuloCol: ColeccionArticulosEntrada) {

    this.Ws
      .ColeccionArticulos()
      .Consumir_Eliminar_ColeccionArticulo(tArticuloCol)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tErroresServices.MostrarError(environment.msjError);

        }
        else {

          this.BuscarColeccion(this.tColeccionActual.Codcoleccion);

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  Cancelar(): void {

    this.Nuevo();

  }

  Nuevo(): void {

    this.tHabilitarGuardar = this.tPermisoAgregar;
    this.tHabilitarEliminar = false;
    this.tBloquearBuscar = false;
    this.tNuevo = true;
    this.tEditar = false;
    this.tColeccionActual = new Coleccion();
    this.tArticuloActual = new Articulo();
    this.tArticuloInv = [];

    this.toastr.info('Por favor llene los datos necesarios para registrar una nueva colección!');

  }

  ValidarFormulario(): boolean {

    let valido: boolean = false;

    if (this.tColeccionActual.Descripcion === "") {

      this.toastr.warning('Debe ingresar una Descripción!', 'Disculpe!');
      return valido;

    }

    if (this.tColeccionActual.Descuento < 0) {

      this.toastr.warning('Debe ingresar un monto de descuento válido!', 'Disculpe!');
      return valido;

    }

    return true;

  }

  Agregar(): void {

    if (this.ValidarFormulario() == false) {

      return;

    }

    switch (true) {
      case this.tNuevo: {

        this.tConfServices
          .AbrirModalConf(this.tConfiguracionFormulario.ModalConfirmacion.Agregar)
          .then(Resultado => {

            switch (Resultado) {
              case "S": {

                this.Nueva_Coleccion();
                break;

              }
            }

          });

        break;

      }
      case this.tEditar: {

        this.tConfServices
          .AbrirModalConf(this.tConfiguracionFormulario.ModalConfirmacion.Edicion)
          .then(Resultado => {

            switch (Resultado) {
              case "S": {

                this.Modificar_Coleccion();
                break;

              }
            }

          });


        break;

      }
    }

  }

  Eliminar(): void {

    this.tConfServices
      .AbrirModalConf(this.tConfiguracionFormulario.ModalConfirmacion.Eliminacion)
      .then(Resultado => {

        switch (Resultado) {
          case "S": {

            this.Eliminar_Coleccion();
            break;

          }
        }

      });

  }

}
