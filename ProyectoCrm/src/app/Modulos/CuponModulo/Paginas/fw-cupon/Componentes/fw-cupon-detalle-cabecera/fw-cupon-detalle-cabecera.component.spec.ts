import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwCuponDetalleCabeceraComponent } from './fw-cupon-detalle-cabecera.component';

describe('FwCuponDetalleCabeceraComponent', () => {
  let component: FwCuponDetalleCabeceraComponent;
  let fixture: ComponentFixture<FwCuponDetalleCabeceraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwCuponDetalleCabeceraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwCuponDetalleCabeceraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
