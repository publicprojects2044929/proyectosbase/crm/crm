import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Cupon } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-cupon-detalle-cliente',
  templateUrl: './fw-cupon-detalle-cliente.component.html',
  styleUrls: ['./fw-cupon-detalle-cliente.component.scss']
})
export class FwCuponDetalleClienteComponent implements OnInit {

  @Input('Cupon') tCupon: Cupon = new Cupon();
  @Input('Nuevo') tNuevo: boolean = false;
  @Input('BloquearBuscar') tBloquearBuscar: boolean = false;

  @Output('BuscarCliente') tBuscar: EventEmitter<string> = new EventEmitter();
  @Output('AbrirModalClientes') tAbrir: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  BuscarCliente(tCodcliente: string){

    if(tCodcliente === '') { return; }

    this.tBuscar.emit(tCodcliente);

  }

  AbrirModalCliente(){

    this.tAbrir.emit('');

  }

}
