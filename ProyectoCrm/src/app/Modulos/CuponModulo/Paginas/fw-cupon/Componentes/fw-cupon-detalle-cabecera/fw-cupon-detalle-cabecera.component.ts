import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { Cupon } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-cupon-detalle-cabecera',
  templateUrl: './fw-cupon-detalle-cabecera.component.html',
  styleUrls: ['./fw-cupon-detalle-cabecera.component.scss']
})
export class FwCuponDetalleCabeceraComponent implements OnInit, OnChanges {

  @Input('Cupon') tCupon: Cupon = new Cupon();
  tQrString: string = "";

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(){

    this.tQrString = `${ this.tCupon.Corporacion }|${ this.tCupon.Empresa }|${this.tCupon.Codcupon}|${this.tCupon.Codcliente}`;

  }

}
