import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwCuponDetalleDatosComponent } from './fw-cupon-detalle-datos.component';

describe('FwCuponDetalleDatosComponent', () => {
  let component: FwCuponDetalleDatosComponent;
  let fixture: ComponentFixture<FwCuponDetalleDatosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwCuponDetalleDatosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwCuponDetalleDatosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
