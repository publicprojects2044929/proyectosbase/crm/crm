import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Cupon, Estatus, Tipo, Empresas } from 'src/app/Clases/Estructura';
import { ManejarFechas } from 'src/app/Manejadores/cls-procedimientos';

@Component({
  selector: 'app-fw-cupon-detalle-datos',
  templateUrl: './fw-cupon-detalle-datos.component.html',
  styleUrls: ['./fw-cupon-detalle-datos.component.scss']
})
export class FwCuponDetalleDatosComponent implements OnInit, OnChanges {

  @Input('Cupon') tCupon: Cupon = new Cupon();
  @Input('Estatus') tEstatus: Estatus[] = [];
  @Input('Tipos') tTipos: Tipo[] = [];
  @Input('Nuevo') tNuevo: boolean = false;
  @Input('Empresa') tEmpresaActual: Empresas = new Empresas();

  tManejarFecha: ManejarFechas = new ManejarFechas();

  tEstatusActual: Estatus = new Estatus();
  tTipoActual: Tipo = new Tipo();

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {

    if ('tCupon' in changes) {

      this.tEstatusActual = this.tEstatus.find(tEst => {

        return tEst.Corporacion === this.tCupon.Corporacion &&
          tEst.Empresa === this.tCupon.Empresa &&
          tEst.Codestatus === this.tCupon.Estatus

      }) || this.tEstatus[0] || new Estatus();

      this.tTipoActual = this.tTipos.find(tTip => {

        return tTip.Corporacion === this.tCupon.Corporacion &&
          tTip.Empresa === this.tCupon.Empresa &&
          tTip.Abreviacion === this.tCupon.Tipo

      }) || this.tTipos[0] || new Tipo();

    }

  }

  CambiarEstatus() {
    this.tCupon.Estatus = this.tEstatusActual.Codestatus;
  }

  CambiarTipo() {
    this.tCupon.Tipo = this.tTipoActual.Abreviacion;
  }

  Seleccionar_Fecha(tFecha: any): void {

    this.tCupon.Fecha_vencimientol = this.tManejarFecha.Seleccionar_Fecha(tFecha);

  }

  Limpiar() {

    this.tEstatusActual = this.tEstatus[0] || new Estatus();
    this.tTipoActual = this.tTipos[0] || new Tipo();
    this.tCupon.Fecha_vencimiento = new Date();

  }

}
