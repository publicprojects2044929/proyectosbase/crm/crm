import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwCuponDetalleClienteComponent } from './fw-cupon-detalle-cliente.component';

describe('FwCuponDetalleClienteComponent', () => {
  let component: FwCuponDetalleClienteComponent;
  let fixture: ComponentFixture<FwCuponDetalleClienteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwCuponDetalleClienteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwCuponDetalleClienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
