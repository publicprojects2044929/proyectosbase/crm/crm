import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/Core/Session/session.service';
import { Filtros, Empresas, Usuario, Formulario, Cupon, Estatus, Tipo, Cliente } from 'src/app/Clases/Estructura';
import { CuponEntrada } from 'src/app/Clases/EstructuraEntrada';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { FormularioConfiguracion } from 'src/app/Clases/EstructuraConfiguracion';
import { ToastrService } from 'ngx-toastr';
import { ErroresService } from 'src/app/Modales/mensajes/fw-modal-msj/errores.service';
import { MensajesService } from 'src/app/Modales/mensajes/fw-modal-msj/mensajes.service';
import { ConfirmacionService } from 'src/app/Modales/confirmacion/fw-modal-confirmacion/confirmacion.service';
import { ClientesService } from 'src/app/Modales/cliente/fw-modal-clientes/clientes.service'
import { environment } from 'src/environments/environment';
import { Subscription } from 'rxjs';
import { ConfiguracionService } from 'src/app/Core/Configuracion/configuracion.service';


@Component({
  selector: 'app-fw-cupon',
  templateUrl: './fw-cupon.page.html',
  styleUrls: ['./fw-cupon.page.scss']
})

export class FwCuponPage implements OnInit, OnDestroy {

  @ViewChild('DetalleDatos', { static: true }) tDetalleDatos : any;

  tCodFormularioActual: string = "fCupon";
  tConfiguracionFormulario: FormularioConfiguracion = new FormularioConfiguracion;
  tPermisoAgregar: boolean = false;
  tPermisoEditar: boolean = false;
  tPermisoEliminar: boolean = false;
  tHabilitarGuardar: boolean = false;

  tUsuarioActual: Usuario = new Usuario;
  tEmpresaActual: Empresas = new Empresas;
  tEmpresa$: Subscription;
  tUsuario$: Subscription;

  tTitulo: string = "Gestión de cupones de descuento";
  tNuevo: boolean = true;
  tBloquearBuscar: boolean = false;
  // tPosicion: number = 0;
  tPagina: number = 1;
  tTamanoPag: number = 5;

  tCupones: Cupon[] = [];
  tCuponActual: Cupon = new Cupon;

  tFormularios: Formulario[] = [];

  tEstatus: Estatus[] = [];

  tTipos: Tipo[] = [];

  tFiltros: Filtros[] = [];
  tFiltroActual: Filtros = new Filtros;
  tFiltroBusqueda: string = "";

  tConfiguracion$: Subscription;
  tHayConfiguracionWs$: Subscription;
  tStrConfiguracion: string = 'Cupon/Cupon.Config.json';

  constructor(public router: Router,
    public Ws: ClsServiciosService,
    private tConfiguracionService: ConfiguracionService,
    private tSesion: SessionService,
    private toastr: ToastrService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService,
    private tConfServices: ConfirmacionService,
    private tClienteService: ClientesService) {

    this.tFiltroBusqueda = "";
    // this.tPosicion = 0;

    this.tFiltros = [

      { Codfiltro: "Codcupon", Filtro: "Código" },
      { Codfiltro: "Descripcion", Filtro: "Descripción" },

    ]

    this.tEstatus = [

      {
        "Id": 0,
        "Corporacion": "",
        "Empresa": "",
        "Codestatus": "",
        "Descripcion": "",
        "Secuencia": 0,
        "Tabla": "",
        "Hexadecimal": "",
        "Fecha_creacion": new Date(),
        "Fecha_creacionl": 0,
        "Fecha_modificacion": new Date(),
        "Fecha_modificacionl": 0,
        "Usuariocreador": "",
        "Usuariomodificador": ""
      }

    ];

    this.tTipos = [

      {
        "Id": 0,
        "Corporacion": "",
        "Empresa": "",
        "Codtipo": "",
        "Descripcion": "",
        "Tabla": "",
        "Columna": "",
        "Abreviacion": "",
        "Hexadecimal": "",
        "Fecha_creacion": new Date(),
        "Fecha_creacionl": 0,
        "Fecha_modificacion": new Date(),
        "Fecha_modificacionl": 0,
        "Usuariocreador": "",
        "Usuariomodificador": ""
      }

    ];

  }

  ngOnInit() {

    this.tConfiguracion$ = this.tConfiguracionService
      .Consumir_Obtener_Configuracion(this.tStrConfiguracion)
      .subscribe((tConfiguracion: FormularioConfiguracion) => {

        this.tConfiguracionFormulario = tConfiguracion;
        this.tTitulo = this.tConfiguracionFormulario.Titulo;
        this.tErroresServices.tTituloMsj = this.tConfiguracionFormulario.ModalMensaje.Titulo;
        this.tMsjServices.tTituloMsj = this.tConfiguracionFormulario.ModalMensaje.Titulo;
        this.tConfServices.tTituloConf = this.tConfiguracionFormulario.ModalConfirmacion.Titulo;

      });

    this.tEmpresa$ = this.tSesion.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    })

    this.tUsuario$ = this.tSesion.tUsuarioActual$.subscribe((tUsuario: Usuario) => {

      this.tUsuarioActual = tUsuario;

    })

    var tTienePermiso: boolean = this.tSesion.ObtenerPermisoAcceso(this.tCodFormularioActual);
    this.tPermisoAgregar = this.tSesion.ObtenerPermisoAgregar();
    this.tPermisoEditar = this.tSesion.ObtenerPermisoEditar();
    this.tPermisoEliminar = this.tSesion.ObtenerPermisoEliminar();

    if (tTienePermiso == true) {

      this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
        .subscribe((tHayConfig: boolean) => {

          if (tHayConfig === true) {

            this.BuscarTipo();
            this.BuscarEstatus();
            this.Buscar_Cupones();
            this.Nuevo();

          }

        });

    }
    else {

      this.tMsjServices.AbrirModalMsj(environment.msjSinAcceso);
      this.router.navigate(['/Home']);

    }

  };

  ngOnDestroy() {

    this.tEmpresa$.unsubscribe();
    this.tUsuario$.unsubscribe();
    this.tConfiguracion$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();

  };

  //#region "Funciones Modal"

  AbrirModalClientes(): void {

    this.tCuponActual.Codcliente = "";
    this.tCuponActual.Descripcion_cliente = "";
    this.tCuponActual.Telefono = "";
    this.tCuponActual.Email = "";

    this.tClienteService
      .AbrirModal()
      .then(result => {

        if (result.Resultado == "S") {

          var tClienteModal: Cliente = result.Cliente;
          this.tCuponActual.Codcliente = tClienteModal.Codcliente;
          this.tCuponActual.Descripcion_cliente = tClienteModal.Descripcion;
          this.tCuponActual.Telefono = tClienteModal.Telefono;
          this.tCuponActual.Email = tClienteModal.Email;

        }

      }, reason => {



      });

  };

  //#endregion


  //#region "Funciones de busqueda"

  Buscar_Cupones(): void {

    this.Ws.Cupon()
      .Consumir_Obtener_Cupones(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa)
      .subscribe(Respuesta => {

        this.tCupones = [];

        if (Respuesta.Resultado == "N") {

          // this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);
          //

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tCupones = Respuesta.Datos;
          this.tFiltroActual = this.tFiltros[0];

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  };

  BuscarEstatus(): void {

    this.Ws
      .Estatus()
      .Consumir_Obtener_Estatus(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        'MR_CUPON')
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          // this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);
          //

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.tEstatus = Respuesta.Datos;

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  };

  BuscarTipo(): void {

    this.Ws
      .Tipo()
      .Consumir_Obtener_Tipo(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        'MR_CUPON')
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          // this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);
          //

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.tTipos = Respuesta.Datos;

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  };

  BuscarCliente(txtCodcliente: string): void {

    this.tBloquearBuscar = false;
    this.tCuponActual.Descripcion_cliente = "";
    this.tCuponActual.Telefono = "";
    this.tCuponActual.Email = "";

    if (txtCodcliente == "") {
      return;
    }

    this.Ws.Cliente()
      .Consumir_Obtener_Clientes2(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        txtCodcliente)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.tBloquearBuscar = true;
          this.tCuponActual.Descripcion_cliente = Respuesta.Datos[0].Descripcion;
          this.tCuponActual.Telefono = Respuesta.Datos[0].Telefono;
          this.tCuponActual.Email = Respuesta.Datos[0].Email;

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  };

  //#endregion

  //#region "CRUD"

  Nuevo_Cupon(): void {

    var tNuevoCupon: CuponEntrada;
    tNuevoCupon = {
      "Id": 0,
      "Corporacion": this.tEmpresaActual.Corporacion,
      "Empresa": this.tEmpresaActual.Empresa,
      "Codcupon": "",
      "Codcliente": this.tCuponActual.Codcliente,
      "Descripcion": this.tCuponActual.Descripcion,
      "Tipo": this.tCuponActual.Tipo,
      "Monto": this.tCuponActual.Monto,
      "Fecha_vencimientol": this.tCuponActual.Fecha_vencimientol,
      "Estatus": this.tCuponActual.Estatus,
      "Usuariocreador": this.tUsuarioActual.Codusuario,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }

    this.Ws.Cupon()
      .Consumir_Crear_Cupon(tNuevoCupon)
      .subscribe(Respuesta => {


        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.toastr.success('El cupon se ha creado de manera exitosa!')
          this.tFiltroActual = this.tFiltros[0];
          this.Buscar_Cupones();
          this.Nuevo();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  };

  Modificar_Cupon(): void {

    var tModificarCupon: CuponEntrada;
    tModificarCupon = {
      "Id": 0,
      "Corporacion": this.tCuponActual.Corporacion,
      "Empresa": this.tCuponActual.Empresa,
      "Codcupon": this.tCuponActual.Codcupon,
      "Codcliente": this.tCuponActual.Codcliente,
      "Descripcion": this.tCuponActual.Descripcion,
      "Tipo": this.tCuponActual.Tipo,
      "Monto": this.tCuponActual.Monto,
      "Estatus": this.tCuponActual.Estatus,
      "Fecha_vencimientol": this.tCuponActual.Fecha_vencimientol,
      "Usuariocreador": this.tCuponActual.Usuariocreador,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }

    this.Ws.Cupon()
      .Consumir_Modificar_Cupon(tModificarCupon)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.toastr.success('El cupon se ha modificado de manera exitosa!')
          this.tFiltroActual = this.tFiltros[0];
          this.Buscar_Cupones();
          this.Nuevo();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  };

  Eliminar_Cupon(): void {

    var tEliminarCipon: CuponEntrada;
    tEliminarCipon = {
      "Id": 0,
      "Corporacion": this.tCuponActual.Corporacion,
      "Empresa": this.tCuponActual.Empresa,
      "Codcupon": this.tCuponActual.Codcupon,
      "Codcliente": this.tCuponActual.Codcliente,
      "Descripcion": this.tCuponActual.Descripcion,
      "Tipo": this.tCuponActual.Tipo,
      "Monto": this.tCuponActual.Monto,
      "Estatus": this.tCuponActual.Estatus,
      "Fecha_vencimientol": this.tCuponActual.Fecha_vencimientol,
      "Usuariocreador": this.tCuponActual.Usuariocreador,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }

    this.Ws.Cupon()
      .Consumir_Eliminar_Cupon(tEliminarCipon)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.toastr.success('El cupon se ha eliminado de manera exitosa!')
          this.tFiltroActual = this.tFiltros[0];
          this.Buscar_Cupones();
          this.Nuevo();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  };

  //#endregion

  Nuevo(): void {

    this.tHabilitarGuardar = this.tPermisoAgregar;
    this.tNuevo = true;
    this.tCuponActual = new Cupon();
    this.tDetalleDatos.Limpiar();
    this.toastr.info('Por favor llene los datos necesarios para registrar un nuevo Cupon!');

  };

  Seleccionar_Cupon(tCupon_Sel: Cupon, tPos: number): void {


    this.tHabilitarGuardar = this.tPermisoEditar;
    this.tNuevo = false;
    // this.tPosicion = tPos;
    this.tCuponActual = new Cupon();
    Object.assign(this.tCuponActual, tCupon_Sel);
    this.tCuponActual.Usuariomodificador = this.tUsuarioActual.Usuariomodificador;

  };

  Seleccionar_Cupon_Eliminar(tCupon_Sel: Cupon): void {

    this.tCuponActual = new Cupon();
    Object.assign(this.tCuponActual, tCupon_Sel);
    this.tCuponActual.Usuariomodificador = this.tUsuarioActual.Usuariomodificador;

    this.tConfServices
      .AbrirModalConf(this.tConfiguracionFormulario.ModalConfirmacion.Eliminacion)
      .then(Resultado => {

        switch (Resultado) {
          case "S": {

            this.Eliminar_Cupon();
            break;

          }
        }

      });

  };

  Cancelar(): void {

    this.tBloquearBuscar = false;
    this.Nuevo();

  };

  ValidarFormulario(): boolean {

    let valido: boolean = false;

    if (this.tCuponActual.Descripcion == "") {
      this.toastr.warning('Debe ingresar una descripciòn!', 'Disculpe!')
      return valido
    }

    if (this.tCuponActual.Monto <= 0) {
      this.toastr.error('Debe ingresar un monto mayor que 0!', 'Disculpe!')
      return valido
    }

    return true;
  };

  Agregar(): void {

    if (this.ValidarFormulario() == false) {
      return
    }

    switch (this.tNuevo) {
      case true: {

        this.tConfServices
          .AbrirModalConf(this.tConfiguracionFormulario.ModalConfirmacion.Agregar)
          .then(Resultado => {

            switch (Resultado) {
              case "S": {

                this.Nuevo_Cupon();
                break;

              }
            }

          });
        break;

      }
      default: {

        this.tConfServices
          .AbrirModalConf(this.tConfiguracionFormulario.ModalConfirmacion.Edicion)
          .then(Resultado => {

            switch (Resultado) {
              case "S": {

                this.Modificar_Cupon();
                break;

              }
            }

          });
        break;

      }
    }

  };

}
