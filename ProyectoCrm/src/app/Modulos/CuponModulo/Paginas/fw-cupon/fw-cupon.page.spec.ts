import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwCuponPage } from './fw-cupon.page';

describe('FwCuponPage', () => {
  let component: FwCuponPage;
  let fixture: ComponentFixture<FwCuponPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwCuponPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwCuponPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
