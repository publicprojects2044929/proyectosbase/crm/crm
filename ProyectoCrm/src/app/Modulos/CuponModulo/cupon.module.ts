import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QRCodeModule } from 'angularx-qrcode';

import { CuponRoutingModule } from './cupon-routing.module';
import { FwCuponPage } from './Paginas/fw-cupon/fw-cupon.page';

import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { SharedModule } from 'src/app/Shared/shared.module';
import { FwCuponDetalleCabeceraComponent } from './Paginas/fw-cupon/Componentes/fw-cupon-detalle-cabecera/fw-cupon-detalle-cabecera.component';
import { FwCuponDetalleDatosComponent } from './Paginas/fw-cupon/Componentes/fw-cupon-detalle-datos/fw-cupon-detalle-datos.component';
import { FwCuponDetalleClienteComponent } from './Paginas/fw-cupon/Componentes/fw-cupon-detalle-cliente/fw-cupon-detalle-cliente.component';

@NgModule({
  declarations: [FwCuponPage, FwCuponDetalleCabeceraComponent, FwCuponDetalleDatosComponent, FwCuponDetalleClienteComponent],
  imports: [
    CommonModule,
    CuponRoutingModule,
    SharedModule,
    FormsModule,
    BsDatepickerModule.forRoot(),
    NgbModule,
    QRCodeModule
  ]
})
export class CuponModule { }
