import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from 'src/app/Shared/shared.module';

import { Clasificacion4RoutingModule } from './clasificacion4-routing.module';
import { FwClasificacion4Page } from './Paginas/fw-clasificacion4/fw-clasificacion4.page';
import { FwClasificacion4CabeceraComponent } from './Paginas/fw-clasificacion4/Componentes/fw-clasificacion4-cabecera/fw-clasificacion4-cabecera.component';
import { FwClasificacion4DetallesComponent } from './Paginas/fw-clasificacion4/Componentes/fw-clasificacion4-detalles/fw-clasificacion4-detalles.component'

@NgModule({
  declarations: [FwClasificacion4Page, FwClasificacion4CabeceraComponent, FwClasificacion4DetallesComponent],
  imports: [
    CommonModule,
    Clasificacion4RoutingModule,
    SharedModule,
    FormsModule,
    NgbModule
  ]
})
export class Clasificacion4Module { }
