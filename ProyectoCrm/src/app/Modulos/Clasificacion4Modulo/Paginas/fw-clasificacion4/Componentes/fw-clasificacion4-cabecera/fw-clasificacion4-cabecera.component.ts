import { Component, OnInit, Input } from '@angular/core';
import { Clasificacion4 } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-clasificacion4-cabecera',
  templateUrl: './fw-clasificacion4-cabecera.component.html',
  styleUrls: ['./fw-clasificacion4-cabecera.component.scss']
})
export class FwClasificacion4CabeceraComponent implements OnInit {

  @Input('Clasificacion4') tClasificacion4: Clasificacion4 = new Clasificacion4();

  constructor() { }

  ngOnInit(): void {
  }

}
