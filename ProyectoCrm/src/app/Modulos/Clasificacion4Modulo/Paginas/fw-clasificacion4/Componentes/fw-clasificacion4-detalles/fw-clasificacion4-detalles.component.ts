import { Component, OnInit, OnChanges, Input, ViewChild, SimpleChanges } from '@angular/core';
import { Clasificacion4, Clasificacion1, Clasificacion2, Clasificacion3, Configuracion } from 'src/app/Clases/Estructura';
import { Clasificaciones1Service } from 'src/app/Modales/clasificacion1/fw-modal-clasificaciones1/clasificaciones1.service';
import { Clasificaciones2Service } from 'src/app/Modales/clasificacion2/fw-modal-clasificaciones2/clasificaciones2.service';
import { Clasificaciones3Service } from 'src/app/Modales/clasificacion3/fw-modal-clasificaciones3/clasificaciones3.service';
import { ManejarImagenes } from 'src/app/Manejadores/cls-procedimientos';

@Component({
  selector: 'app-fw-clasificacion4-detalles',
  templateUrl: './fw-clasificacion4-detalles.component.html',
  styleUrls: ['./fw-clasificacion4-detalles.component.scss']
})
export class FwClasificacion4DetallesComponent implements OnInit, OnChanges {

  @ViewChild('flArchivo', { static: true }) tControlArchivo: any;
  @Input('Configuracion') tConfiguracion: Configuracion = new Configuracion();
  @Input('Clasificacion4') tClasificacion4: Clasificacion4 = new Clasificacion4();
  @Input('Clasificaciones1') tClasificaciones1: Clasificacion1[] = [];
  @Input('Nuevo') tNuevo: boolean = false;
  @Input('ManejarImagen') tManejarImagen: ManejarImagenes = new ManejarImagenes();

  tClasificacion1Actual: Clasificacion1 = new Clasificacion1();

  tClasificaciones2: Clasificacion2[] = [];
  tClasificacion2Actual: Clasificacion2 = new Clasificacion2();

  tClasificaciones3: Clasificacion3[] = [];
  tClasificacion3Actual: Clasificacion3 = new Clasificacion3();


  constructor(private tClasificacion1Service: Clasificaciones1Service,
    private tClasificacion2Service: Clasificaciones2Service,
    private tClasificacion3Service: Clasificaciones3Service) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {

    if ('tClasificaciones1' in changes) {

      this.tClasificacion1Actual = this.tClasificaciones1[0] || new Clasificacion1();
      this.Seleccionar_Clasificacion1();

    }

    if ('tClasificacion4' in changes) {

      this.tClasificacion1Actual = this.tClasificaciones1.find(tClasificacion1 => {
        return tClasificacion1.Corporacion === this.tClasificacion4.Corporacion &&
          tClasificacion1.Empresa === this.tClasificacion4.Empresa &&
          tClasificacion1.Codclasificacion1 === this.tClasificacion4.Codclasificacion1
      }) || this.tClasificaciones1[0] || new Clasificacion1();

      this.tClasificaciones2 = this.tClasificacion1Actual.LClasificacion2 || [];

      this.tClasificacion2Actual = this.tClasificaciones2.find(tClasificacion2 => {
        return tClasificacion2.Corporacion === this.tClasificacion4.Corporacion &&
          tClasificacion2.Empresa === this.tClasificacion4.Empresa &&
          tClasificacion2.Codclasificacion1 === this.tClasificacion4.Codclasificacion1 &&
          tClasificacion2.Codclasificacion2 === this.tClasificacion4.Codclasificacion2
      }) || this.tClasificaciones2[0] || new Clasificacion2();

      this.tClasificaciones3 = this.tClasificacion2Actual.LClasificacion3 || [];

      this.tClasificacion3Actual = this.tClasificaciones3.find(tClasificacion3 => {
        return tClasificacion3.Corporacion === this.tClasificacion4.Corporacion &&
          tClasificacion3.Empresa === this.tClasificacion4.Empresa &&
          tClasificacion3.Codclasificacion1 === this.tClasificacion4.Codclasificacion1 &&
          tClasificacion3.Codclasificacion2 === this.tClasificacion4.Codclasificacion2 &&
          tClasificacion3.Codclasificacion3 === this.tClasificacion4.Codclasificacion3
      }) || this.tClasificaciones3[0] || new Clasificacion3();

      if(this.tClasificacion4.Codclasificacion4 === ''){
        this.Seleccionar_Clasificacion3();

      }



    }

  }

  //#region "Funciones Modal"

  AbrirModalClasificacion1(): void {

    this.tClasificacion1Service
      .AbrirModal()
      .then(result => {

        if (result.Resultado == "S") {

          var tClasificacion1Modal: Clasificacion1 = result.Clasificacion1;
          this.tClasificacion1Actual = this.tClasificaciones1.find(tClasificacion1 => {

            return tClasificacion1.Corporacion === tClasificacion1Modal.Corporacion &&
              tClasificacion1.Empresa === tClasificacion1Modal.Empresa &&
              tClasificacion1.Codclasificacion1 === tClasificacion1Modal.Codclasificacion1

          }) || this.tClasificaciones1[0] || new Clasificacion1();
          this.Seleccionar_Clasificacion1();

        }

      }, reason => {



      });

  }

  AbrirModalClasificacion2(): void {

    this.tClasificacion2Service
      .AbrirModal(this.tClasificacion1Actual)
      .then(result => {

        if (result.Resultado == "S") {

          var tClasificacion2Modal: Clasificacion2 = result.Clasificacion2;
          this.tClasificacion2Actual = this.tClasificaciones2.find(tClasificacion2 => {

            return tClasificacion2.Corporacion === tClasificacion2Modal.Corporacion &&
              tClasificacion2.Empresa === tClasificacion2Modal.Empresa &&
              tClasificacion2.Codclasificacion1 === tClasificacion2Modal.Codclasificacion1 &&
              tClasificacion2.Codclasificacion2 === tClasificacion2Modal.Codclasificacion2

          }) || this.tClasificaciones2[0] || new Clasificacion2();
          this.Seleccionar_Clasificacion2();

        }

      }, reason => {



      });

  }

  AbrirModalClasificacion3(): void {

    this.tClasificacion3Service
      .AbrirModal(this.tClasificacion2Actual)
      .then(result => {

        if (result.Resultado == "S") {

          var tClasificacion3Modal: Clasificacion3 = result.Clasificacion3;
          this.tClasificacion3Actual = this.tClasificaciones3.find(tClasificacion3 => {

            return tClasificacion3.Corporacion === tClasificacion3Modal.Corporacion &&
              tClasificacion3.Empresa === tClasificacion3Modal.Empresa &&
              tClasificacion3.Codclasificacion1 === tClasificacion3Modal.Codclasificacion1 &&
              tClasificacion3.Codclasificacion2 === tClasificacion3Modal.Codclasificacion2 &&
              tClasificacion3.Codclasificacion3 === tClasificacion3Modal.Codclasificacion3

          }) || this.tClasificaciones3[0] || new Clasificacion3();
          this.Seleccionar_Clasificacion3();

        }

      }, reason => {



      });

  }

  //#endregion

  Seleccionar_Clasificacion1() {

    this.tClasificaciones2 = this.tClasificacion1Actual.LClasificacion2 || [];
    this.tClasificaciones3 = [];
    this.tClasificacion2Actual = new Clasificacion2();
    this.tClasificacion3Actual = new Clasificacion3();

    this.tClasificacion2Actual = this.tClasificaciones2[0] || new Clasificacion2();
    this.tClasificaciones3 = this.tClasificacion2Actual.LClasificacion3 || [];
    this.tClasificacion3Actual = this.tClasificaciones3[0] || new Clasificacion3();

    this.Seleccionar_Clasificacion2();

  };

  Seleccionar_Clasificacion2() {

    this.tClasificaciones3 = this.tClasificacion2Actual.LClasificacion3 || [];
    this.tClasificacion3Actual = new Clasificacion3();
    this.tClasificacion3Actual = this.tClasificaciones3[0] || new Clasificacion3();
    this.Seleccionar_Clasificacion3();

  };

  Seleccionar_Clasificacion3() {

    this.tClasificacion4.Codclasificacion1 = this.tClasificacion3Actual.Codclasificacion1;
    this.tClasificacion4.Codclasificacion2 = this.tClasificacion3Actual.Codclasificacion2;
    this.tClasificacion4.Codclasificacion3 = this.tClasificacion3Actual.Codclasificacion3;

  };

  ObtenerImagen(): string {

    if (this.tClasificacion4.Url == "") {

      return "assets/image.png";

    }
    else {

      return this.tClasificacion4.Url;

    }

  }

  BloquearImagen(): boolean {

    if (this.tConfiguracion.Valor == "S") {

      return false;

    }
    else {

      return true;

    }

  }

}
