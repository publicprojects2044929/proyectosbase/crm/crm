import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwClasificacion4DetallesComponent } from './fw-clasificacion4-detalles.component';

describe('FwClasificacion4DetallesComponent', () => {
  let component: FwClasificacion4DetallesComponent;
  let fixture: ComponentFixture<FwClasificacion4DetallesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwClasificacion4DetallesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwClasificacion4DetallesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
