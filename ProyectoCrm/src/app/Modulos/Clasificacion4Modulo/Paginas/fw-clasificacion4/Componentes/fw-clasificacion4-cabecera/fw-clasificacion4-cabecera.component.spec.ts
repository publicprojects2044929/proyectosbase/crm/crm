import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwClasificacion4CabeceraComponent } from './fw-clasificacion4-cabecera.component';

describe('FwClasificacion4CabeceraComponent', () => {
  let component: FwClasificacion4CabeceraComponent;
  let fixture: ComponentFixture<FwClasificacion4CabeceraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwClasificacion4CabeceraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwClasificacion4CabeceraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
