import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { Usuario, Empresas, Formulario, Filtros, Clasificacion4, Clasificacion3, Clasificacion1, Clasificacion2, Configuracion } from 'src/app/Clases/Estructura';
import { ManejarImagenes } from 'src/app/Manejadores/cls-procedimientos';
import { FormularioConfiguracion } from 'src/app/Clases/EstructuraConfiguracion';
import { SessionService } from 'src/app/Core/Session/session.service';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { ToastrService } from 'ngx-toastr';
import { ErroresService } from 'src/app/Modales/mensajes/fw-modal-msj/errores.service';
import { MensajesService } from 'src/app/Modales/mensajes/fw-modal-msj/mensajes.service';
import { ConfirmacionService } from 'src/app/Modales/confirmacion/fw-modal-confirmacion/confirmacion.service';
import { Clasificaciones1Service } from 'src/app/Modales/clasificacion1/fw-modal-clasificaciones1/clasificaciones1.service';
import { Clasificaciones2Service } from 'src/app/Modales/clasificacion2/fw-modal-clasificaciones2/clasificaciones2.service';
import { Clasificaciones3Service } from 'src/app/Modales/clasificacion3/fw-modal-clasificaciones3/clasificaciones3.service';
import { Clasificacion4Entrada } from 'src/app/Clases/EstructuraEntrada';
import { Subscription } from 'rxjs';
import { ConfiguracionService } from 'src/app/Core/Configuracion/configuracion.service';

@Component({
  selector: 'app-fw-clasificacion4',
  templateUrl: './fw-clasificacion4.page.html',
  styleUrls: ['./fw-clasificacion4.page.scss']
})

export class FwClasificacion4Page implements OnInit, OnDestroy {

  tCodFormularioActual: string = "fClasificacion4";
  tPermisoAgregar: boolean = false;
  tPermisoEditar: boolean = false;
  tPermisoEliminar: boolean = false;
  tHabilitarGuardar: boolean = false;

  tUsuarioActual: Usuario = new Usuario();
  tEmpresaActual: Empresas = new Empresas();
  tEmpresa$: Subscription;
  tUsuario$: Subscription;

  tTitulo: string = "";
  tNuevo: boolean = true;
  tPosicion: number = 0;
  tPagina: number = 1;
  tTamanoPag: number = 5;

  tFormularios: Formulario[] = [];

  tFiltros: Filtros[] = [];
  tFiltroActual: Filtros = new Filtros();
  tFiltroBusqueda: string = "";

  tClasificaciones4: Clasificacion4[] = [];
  tClasificacionActual: Clasificacion4 = new Clasificacion4();

  tClasificaciones1: Clasificacion1[] = [];

  tConfiguracion: Configuracion = new Configuracion();
  tManejarImagen: ManejarImagenes = new ManejarImagenes();
  tConfiguracionFormulario: FormularioConfiguracion = new FormularioConfiguracion();

  tConfiguracion$: Subscription;
  tHayConfiguracionWs$: Subscription;
  tStrConfiguracion: string = 'Clasificacion4/Clasificacion4.Config.json';

  constructor(public router: Router,
    private tSesion: SessionService,
    private tConfiguracionService: ConfiguracionService,
    public Ws: ClsServiciosService,
    private toastr: ToastrService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService,
    private tConfServices: ConfirmacionService) {

    this.tFiltroBusqueda = "";
    this.tPosicion = 0;

    this.tFiltros = [

      { Codfiltro: "Codclasificacion1", Filtro: "Clase" },
      { Codfiltro: "Codclasificacion2", Filtro: "Grupo" },
      { Codfiltro: "Codclasificacion3", Filtro: "Categoria" },
      { Codfiltro: "Codclasificacion4", Filtro: "Subcategoria" },
      { Codfiltro: "Descripcion", Filtro: "Descripción" },

    ]

  }

  ngOnInit() {

    this.tConfiguracion$ = this.tConfiguracionService
      .Consumir_Obtener_Configuracion(this.tStrConfiguracion)
      .subscribe((tConfiguracion: FormularioConfiguracion) => {

        this.tConfiguracionFormulario = tConfiguracion;
        this.tTitulo = this.tConfiguracionFormulario.Titulo;
        this.tErroresServices.tTituloMsj = this.tConfiguracionFormulario.ModalMensaje.Titulo; //"Practica - Clasificación2 de artículos";
        this.tMsjServices.tTituloMsj = this.tConfiguracionFormulario.ModalMensaje.Titulo; //"Practica - Clasificación2 de artículos";
        this.tConfServices.tTituloConf = this.tConfiguracionFormulario.ModalConfirmacion.Titulo; //"Practica - Clasificación2 de artículos";

      });

    this.tEmpresa$ = this.tSesion.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    })

    this.tUsuario$ = this.tSesion.tUsuarioActual$.subscribe((tUsuario: Usuario) => {

      this.tUsuarioActual = tUsuario;

    })

    var tTienePermiso: boolean = this.tSesion.ObtenerPermisoAcceso(this.tCodFormularioActual);
    this.tPermisoAgregar = this.tSesion.ObtenerPermisoAgregar();
    this.tPermisoEditar = this.tSesion.ObtenerPermisoEditar();
    this.tPermisoEliminar = this.tSesion.ObtenerPermisoEliminar();

    if (tTienePermiso == true) {

      this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
        .subscribe((tHayConfig: boolean) => {

          if (tHayConfig === true) {

            this.BuscarConfiguracion();
            this.Buscar_Clasificaciones1();
            this.Buscar_Clasificaciones4();
            this.Nuevo();

          }

        });

    }
    else {

      this.tMsjServices.AbrirModalMsj(environment.msjSinAcceso);
      this.router.navigate(['/Home']);

    }

  }

  ngOnDestroy() {

    this.tEmpresa$.unsubscribe();
    this.tUsuario$.unsubscribe();
    this.tConfiguracion$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();

  }

  //#region "Funciones de busqueda"

  Buscar_Clasificaciones4(): void {


    this.Ws.Clasificacion4()
      .Consumir_Obtener_Clasificacion4(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa)
      .subscribe(Respuesta => {

        this.tClasificaciones4 = [];

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tClasificaciones4 = Respuesta.Datos;
          this.tFiltroActual = this.tFiltros[0];

        }

      }, error => {

        this.tErroresServices.MostrarError(error);


      });

  }

  Buscar_Clasificaciones1(): void {


    this.Ws.Clasificacion1()
      .Consumir_Obtener_Clasificacion1Nivel3(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa)
      .subscribe(Respuesta => {

        this.tClasificaciones1 = [];

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {


          this.tClasificaciones1 = Respuesta.Datos;

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  BuscarConfiguracion(): void {

    this.Ws
      .Configuraciones()
      .Consumir_Obtener_Configuracion(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        'MR_CLASIFICACION4',
        'IMAGEN')
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          // this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.tConfiguracion = Respuesta.Datos[0];

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  //#region "CRUD"

  Nueva_Clasificacion4(): void {

    var tNuevaClasificacion4: Clasificacion4Entrada;
    tNuevaClasificacion4 = {
      "Id": 0,
      "Corporacion": this.tEmpresaActual.Corporacion,
      "Empresa": this.tEmpresaActual.Empresa,
      "Codclasificacion1": this.tClasificacionActual.Codclasificacion1,
      "Codclasificacion2": this.tClasificacionActual.Codclasificacion2,
      "Codclasificacion3": this.tClasificacionActual.Codclasificacion3,
      "Codclasificacion4": "",
      "Descripcion": this.tClasificacionActual.Descripcion,
      "Imagen64": this.tManejarImagen.obtenerImagen64(),
      "Url": this.tClasificacionActual.Url,
      "Usuariocreador": this.tUsuarioActual.Codusuario,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }

    this.Ws.Clasificacion4()
      .Consumir_Crear_Clasificacion4(tNuevaClasificacion4)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.toastr.success('La Subcategoria se ha creado de manera exitosa!');
          this.tFiltroActual = this.tFiltros[0];
          this.Buscar_Clasificaciones4();
          this.Nuevo();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Modificar_Clasificacion4(): void {

    var tModificarClasificacion4: Clasificacion4Entrada;
    tModificarClasificacion4 = {
      "Id": 0,
      "Corporacion": this.tClasificacionActual.Corporacion,
      "Empresa": this.tClasificacionActual.Empresa,
      "Codclasificacion1": this.tClasificacionActual.Codclasificacion1,
      "Codclasificacion2": this.tClasificacionActual.Codclasificacion2,
      "Codclasificacion3": this.tClasificacionActual.Codclasificacion3,
      "Codclasificacion4": this.tClasificacionActual.Codclasificacion4,
      "Descripcion": this.tClasificacionActual.Descripcion,
      "Imagen64": this.tManejarImagen.obtenerImagen64(),
      "Url": this.tClasificacionActual.Url,
      "Usuariocreador": this.tClasificacionActual.Usuariocreador,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }

    this.Ws.Clasificacion4()
      .Consumir_Modificar_Clasificacion4(tModificarClasificacion4)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.toastr.success('La Subcategoria se ha modificado de manera exitosa!');
          this.tFiltroActual = this.tFiltros[0];
          this.Buscar_Clasificaciones4();
          this.Nuevo();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Eliminar_Clasificacion4(): void {

    var tEliminarClasificacion4: Clasificacion4Entrada;
    tEliminarClasificacion4 = {
      "Id": 0,
      "Corporacion": this.tClasificacionActual.Corporacion,
      "Empresa": this.tClasificacionActual.Empresa,
      "Codclasificacion1": this.tClasificacionActual.Codclasificacion1,
      "Codclasificacion2": this.tClasificacionActual.Codclasificacion2,
      "Codclasificacion3": this.tClasificacionActual.Codclasificacion3,
      "Codclasificacion4": this.tClasificacionActual.Codclasificacion4,
      "Descripcion": this.tClasificacionActual.Descripcion,
      "Imagen64": "",
      "Url": this.tClasificacionActual.Url,
      "Usuariocreador": this.tClasificacionActual.Usuariocreador,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }

    this.Ws.Clasificacion4()
      .Consumir_Eliminar_Clasificacion4(tEliminarClasificacion4)
      .subscribe(Respuesta => {


        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.toastr.success('La Subcategoria se ha eliminado de manera exitosa!');
          this.tFiltroActual = this.tFiltros[0];
          this.Buscar_Clasificaciones4();
          this.Nuevo();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  Nuevo(): void {

    this.tHabilitarGuardar = this.tPermisoAgregar;
    this.tNuevo = true;
    this.tClasificacionActual = new Clasificacion4();
    this.tManejarImagen.tImagen = "";

    this.toastr.info('Por favor llene los datos necesarios para registrar una nueva Subcategoria!');

  }

  Seleccionar_Clasificacion(tClasificacion_Sel: Clasificacion4, tPos: number): void {

    this.tHabilitarGuardar = this.tPermisoEditar;
    this.tNuevo = false;
    this.tPosicion = tPos;
    this.tClasificacionActual = new Clasificacion4();
    Object.assign(this.tClasificacionActual, tClasificacion_Sel);
    this.tClasificacionActual.Usuariomodificador = this.tUsuarioActual.Usuariomodificador;
    this.tManejarImagen.tImagen = this.tClasificacionActual.Url;

  }

  Seleccionar_Clasificacion_Eliminar(tClasificacion_Sel: Clasificacion4): void {

    this.tClasificacionActual = new Clasificacion4();
    Object.assign(this.tClasificacionActual, tClasificacion_Sel);
    this.tClasificacionActual.Usuariomodificador = this.tUsuarioActual.Usuariomodificador;

    this.tConfServices
      .AbrirModalConf(this.tConfiguracionFormulario.ModalConfirmacion.Eliminacion)
      .then(Resultado => {

        switch (Resultado) {
          case "S": {

            this.Eliminar_Clasificacion4();
            break;

          }
        }

      });

  }

  Cancelar(): void {

    this.Nuevo();

  }

  ValidarFormulario(): boolean {

    let valido: boolean = false;

    if (this.tClasificacionActual.Descripcion == "") {

      this.toastr.warning('Debe ingresar una Descripción!', 'Disculpe!');
      return valido;

    }

    return true;

  }

  Agregar(): void {

    if (this.ValidarFormulario() == false) {
      return
    }

    switch (this.tNuevo) {
      case true: {

        this.tConfServices
          .AbrirModalConf(this.tConfiguracionFormulario.ModalConfirmacion.Agregar)
          .then(Resultado => {

            switch (Resultado) {
              case "S": {

                this.Nueva_Clasificacion4();
                break;

              }
            }

          });
        break;

      }
      default: {

        this.tConfServices
          .AbrirModalConf(this.tConfiguracionFormulario.ModalConfirmacion.Edicion)
          .then(Resultado => {

            switch (Resultado) {
              case "S": {

                this.Modificar_Clasificacion4();
                break;

              }
            }

          });
        break;

      }
    }

  }

}
