import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwClasificacion4Page } from './fw-clasificacion4.page';

describe('FwClasificacion4Page', () => {
  let component: FwClasificacion4Page;
  let fixture: ComponentFixture<FwClasificacion4Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwClasificacion4Page ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwClasificacion4Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
