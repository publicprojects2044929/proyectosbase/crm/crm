import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { LoginRoutingModule } from './login-routing.module';

//Componentes
import { FwLoginPage } from './Paginas/fw-login/fw-login.page';
import { FwLoginIngresoComponent } from './Paginas/fw-login/Componentes/fw-login-ingreso/fw-login-ingreso.component';
import { FwLoginRecuperarComponent } from './Paginas/fw-login/Componentes/fw-login-recuperar/fw-login-recuperar.component';
import { FwLoginCambiarComponent } from './Paginas/fw-login/Componentes/fw-login-cambiar/fw-login-cambiar.component';

@NgModule({
  declarations: [
    FwLoginPage,
    FwLoginIngresoComponent,
    FwLoginRecuperarComponent,
    FwLoginCambiarComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    LoginRoutingModule
  ],
  providers: []
})
export class LoginModule { }
