import { Component, OnInit, OnDestroy } from '@angular/core';
import { Empresas, Usuario, Licencia } from 'src/app/Clases/Estructura'
import { FormularioConfiguracion } from 'src/app/Clases/EstructuraConfiguracion'
import { UsuarioEntrada } from 'src/app/Clases/EstructuraEntrada'
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { Router } from '@angular/router';
import { ErroresService } from 'src/app/Modales/mensajes/fw-modal-msj/errores.service';
import { MensajesService } from 'src/app/Modales/mensajes/fw-modal-msj/mensajes.service';
import { environment } from 'src/environments/environment'
import { SessionService } from 'src/app/Core/Session/session.service'
import { Subscription } from 'rxjs';
import { ConfiguracionService } from 'src/app/Core/Configuracion/configuracion.service';

@Component({
  selector: 'app-fw-login',
  templateUrl: './fw-login.page.html',
  styleUrls: ['./fw-login.page.scss']
})
export class FwLoginPage implements OnInit, OnDestroy {

  tRecuperar: boolean = false;
  tCambiar: boolean = false;
  tLogin: boolean = false;

  tEmpresas: Empresas[] = [];
  tEmpresa_Sel: Empresas = new Empresas();
  tUsuario_Login: Usuario = new Usuario();
  tFormularioConfig: FormularioConfiguracion = new FormularioConfiguracion();

  tConfiguracion$: Subscription;
  tHayConfiguracionWs$: Subscription;
  tStrConfiguracion: string = 'Login/Login.Config.json';

  constructor(public router: Router,
    public Ws: ClsServiciosService,
    private tSession: SessionService,
    private tConfiguracionService: ConfiguracionService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService) {

    // this.tFormularioConfig = Datos;
    // this.tErroresServices.tTituloMsj = this.tFormularioConfig.ModalMensaje.Titulo;
    // this.tMsjServices.tTituloMsj = this.tFormularioConfig.ModalMensaje.Titulo;

  }

  ngOnInit() {

    this.tConfiguracion$ = this.tConfiguracionService
      .Consumir_Obtener_Configuracion(this.tStrConfiguracion)
      .subscribe((tConfiguracion: FormularioConfiguracion) => {

        this.tFormularioConfig = tConfiguracion;
        this.tErroresServices.tTituloMsj = this.tFormularioConfig.ModalMensaje.Titulo;
        this.tMsjServices.tTituloMsj = this.tFormularioConfig.ModalMensaje.Titulo;

      });

    this.tRecuperar = false;
    this.tCambiar = false;
    this.tLogin = false;

    this.tEmpresa_Sel = new Empresas();
    this.tUsuario_Login = new Usuario();

    window.sessionStorage.clear();

    this.tHayConfiguracionWs$ = this.Ws.tHayConfig$.subscribe((tHayConfig: boolean) => {

      if (tHayConfig === true) {

        this.Ws
          .CorporacionEmpresa()
          .Consumir_Obtener_Empresa()
          .subscribe(Respuesta => {

            if (Respuesta.Resultado == "N") {

              this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

            }
            else if (Respuesta.Resultado == "E") {

              this.tMsjServices.AbrirModalMsj(environment.msjError);

            }
            else {

              this.tEmpresas = Respuesta.Datos;

            }

          }, error => {

            this.tErroresServices.MostrarError(error);

          });

      }

    });

  }

  ngOnDestroy() {

    this.tConfiguracion$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();

  }

  RecuperarClave(): boolean {

    this.tRecuperar = true;
    this.tLogin = true;
    return false;

  }

  CancelarRecuperarClave(): boolean {

    this.tRecuperar = false;
    this.tLogin = false;
    return false;

  }

  IniciarSesion({tUsuario, tClave}): boolean {

    var tEmpresaDominio: Empresas;
    var tExiste: boolean = false;
    var tValorDominio: string = "";

    var tUsuarioSplit: string[] = tUsuario.split("@");

    var tUsuarioSinDominio: string = tUsuarioSplit[0];
    var tDominio: string = tUsuarioSplit[1];

    if (tUsuario != "") {

      for (let tEmpresa of this.tEmpresas) {

        tValorDominio = tEmpresa["Dominio"];

        if (tValorDominio == tDominio) {

          tExiste = true;
          tEmpresaDominio = tEmpresa;
          let tValidacionLic: any = this.tSession.VerificarLicencia(tEmpresaDominio);

          if(tValidacionLic.Resultado === false){

            this.tMsjServices.AbrirModalMsj(tValidacionLic.Mensaje);
            return false;

          }

        }

      }

    }

    if (tExiste == false) {

      this.tMsjServices.AbrirModalMsj("El dominio indicado no se encuentra registrado.");
      return false;

    }

    if (tUsuario === '') {

      this.tMsjServices.AbrirModalMsj("Por favor debe ingresar un usuario.");
      return false;

    }

    if (tClave === '') {

      this.tMsjServices.AbrirModalMsj("Por favor debe ingresar la clave.");
      return false;

    }

    this.Ws
      .Usuario()
      .Consumir_Usuario_Login(tEmpresaDominio.Corporacion, tEmpresaDominio.Empresa,
        tUsuarioSinDominio, tClave)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado === "E") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado === "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else {

          this.tUsuario_Login = Respuesta.Datos[0];
          this.tEmpresa_Sel = tEmpresaDominio;

          if (this.tUsuario_Login.Cambiar_clave == 'S') {

            this.tCambiar = true;
            this.tLogin = true;

          }
          else {


            this.IrHome();

          }

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

    return false;

  }

  CambiarClave({tClaveNueva, tClaveConf}): boolean {

    if (tClaveNueva === "") {

      this.tMsjServices.AbrirModalMsj("Por favor debe ingresar la nueva clave.");
      return false;

    }

    if (tClaveConf === "") {

      this.tMsjServices.AbrirModalMsj("Por favor debe confirmar la nueva clave.");
      return false;

    }

    if (tClaveConf !== tClaveNueva) {

      this.tMsjServices.AbrirModalMsj("Las claves no coinciden.");
      return false;

    }

    var tUsuario: UsuarioEntrada = {
      "Id": 0,
      "Corporacion": this.tEmpresa_Sel.Corporacion,
      "Empresa": this.tEmpresa_Sel.Empresa,
      "Codusuario": this.tUsuario_Login.Codusuario,
      "Descripcion": this.tUsuario_Login.Descripcion,
      "Clave": tClaveNueva,
      "Cedula": this.tUsuario_Login.Cedula,
      "Email": this.tUsuario_Login.Email,
      "Estatus": this.tUsuario_Login.Estatus,
      "Imagen64": "",
      "Url": this.tUsuario_Login.Url,
      "Conectado": 0,
      "Token": "",
      "Dispositivo": "",
      "Tipo": this.tUsuario_Login.Tipo,
      "Cambiar_clave": "",
      "Oculto": this.tUsuario_Login.Oculto,
      "Usuariocreador": "",
      "Usuariomodificador": "",
      "LUsuario_Empresas": []
    };

    this.Ws.Usuario().Consumir_Cambiar_Clave(tUsuario).subscribe(Respuesta => {

      if (Respuesta.Resultado == "E") {

        this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

      }
      else if (Respuesta.Resultado == "N") {

        this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

      }
      else {

        this.IrHome();

      }

    }, error => {

      this.tErroresServices.MostrarError(error);

    });

    return false;

  }

  RestablecerClave(tCorreo: string) {

    if (tCorreo == "") {

      this.tMsjServices.AbrirModalMsj("Por favor debe ingresar un correo.");
      return;

    }

    var tUsuario: UsuarioEntrada = {
      "Id": 0,
      "Corporacion": "",
      "Empresa": "",
      "Codusuario": "",
      "Descripcion": "",
      "Clave": "",
      "Cedula": "",
      "Email": tCorreo,
      "Estatus": "",
      "Imagen64": "",
      "Url": "",
      "Conectado": 0,
      "Token": "",
      "Dispositivo": "",
      "Tipo": "",
      "Cambiar_clave": "",
      "Oculto": "N",
      "Usuariocreador": "",
      "Usuariomodificador": "",
      "LUsuario_Empresas": []
    };

    this.Ws.Usuario().Consumir_Restablecer_Clave(tUsuario).subscribe(Respuesta => {

      if (Respuesta.Resultado == "E") {

        this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

      }
      else if (Respuesta.Resultado == "N") {

        this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

      }
      else {

        this.tMsjServices.AbrirModalMsj('Se envío un correo con su nueva clave.');
        this.CancelarRecuperarClave();

      }

    }, error => {

      this.tErroresServices.MostrarError(error);

    });

  }

  IrHome() {

    this.tSession.Login(this.tUsuario_Login, this.tEmpresa_Sel);
    this.router.navigate(['/Home']);

  }

}
