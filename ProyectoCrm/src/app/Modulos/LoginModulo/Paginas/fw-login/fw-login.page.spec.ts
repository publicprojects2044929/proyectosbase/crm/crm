import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwLoginPage } from './fw-login.page';

describe('FwLoginPage', () => {
  let component: FwLoginPage;
  let fixture: ComponentFixture<FwLoginPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwLoginPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwLoginPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
