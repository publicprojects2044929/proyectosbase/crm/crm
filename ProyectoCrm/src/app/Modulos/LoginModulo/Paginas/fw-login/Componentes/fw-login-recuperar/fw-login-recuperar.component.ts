import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-fw-login-recuperar',
  templateUrl: './fw-login-recuperar.component.html',
  styleUrls: ['./fw-login-recuperar.component.scss']
})
export class FwLoginRecuperarComponent implements OnInit {

  tCorreoRecuperacion: string = "";

  @Output('Recuperar') tRecuperar: EventEmitter<string> = new EventEmitter();
  @Output('Cancelar') tCancelar: EventEmitter<string> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  Recuperar(){
    this.tRecuperar.emit(this.tCorreoRecuperacion);
    this.tCorreoRecuperacion = '';
  }

  Cancelar(){
    this.tCorreoRecuperacion = '';
    this.tCancelar.emit('');
  }

}
