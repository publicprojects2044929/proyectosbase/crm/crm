import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwLoginIngresoComponent } from './fw-login-ingreso.component';

describe('FwLoginIngresoComponent', () => {
  let component: FwLoginIngresoComponent;
  let fixture: ComponentFixture<FwLoginIngresoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwLoginIngresoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwLoginIngresoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
