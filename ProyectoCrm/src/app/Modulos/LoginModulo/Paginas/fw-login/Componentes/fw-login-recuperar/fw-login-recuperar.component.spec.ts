import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwLoginRecuperarComponent } from './fw-login-recuperar.component';

describe('FwLoginRecuperarComponent', () => {
  let component: FwLoginRecuperarComponent;
  let fixture: ComponentFixture<FwLoginRecuperarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwLoginRecuperarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwLoginRecuperarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
