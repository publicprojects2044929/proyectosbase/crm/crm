import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-fw-login-cambiar',
  templateUrl: './fw-login-cambiar.component.html',
  styleUrls: ['./fw-login-cambiar.component.scss']
})
export class FwLoginCambiarComponent implements OnInit {

  @Output('Cambiar') tCambiar: EventEmitter<{tClaveNueva: string, tClaveConf: string}> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  Cambiar(txtNuevaClave: any, txtCofirmarClave: any){

    let tClaveNueva: string = txtNuevaClave.value;
    let tClaveConf: string = txtCofirmarClave.value;

    this.tCambiar.emit({tClaveNueva, tClaveConf});
  }

}
