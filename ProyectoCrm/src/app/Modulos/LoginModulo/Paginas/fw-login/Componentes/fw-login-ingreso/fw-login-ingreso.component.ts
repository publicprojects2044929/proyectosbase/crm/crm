import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-fw-login-ingreso',
  templateUrl: './fw-login-ingreso.component.html',
  styleUrls: ['./fw-login-ingreso.component.scss']
})
export class FwLoginIngresoComponent implements OnInit {

  @Output('Ingresar') tIngresar: EventEmitter<{tUsuario: string, tClave: string}> = new EventEmitter();
  @Output('Recuperar') tRecuperar: EventEmitter<string> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  Ingresar(txtUsuario: any, txtClave: any){

    let tUsuario: string = txtUsuario.value;
    let tClave: string = txtClave.value;

    this.tIngresar.emit({tUsuario, tClave});

  }

  Recuperar(){
    this.tRecuperar.emit('');
  }

}
