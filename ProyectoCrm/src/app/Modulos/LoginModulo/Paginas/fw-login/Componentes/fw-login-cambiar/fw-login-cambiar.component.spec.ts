import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwLoginCambiarComponent } from './fw-login-cambiar.component';

describe('FwLoginCambiarComponent', () => {
  let component: FwLoginCambiarComponent;
  let fixture: ComponentFixture<FwLoginCambiarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwLoginCambiarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwLoginCambiarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
