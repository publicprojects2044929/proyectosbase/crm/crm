import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//Formularios
import { FwLoginPage } from './Paginas/fw-login/fw-login.page'

const routes: Routes = [
  {
    path: '',
    component: FwLoginPage,
    data:{
      Codformulario: 'fLogin'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule { }
