import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from 'src/app/Shared/shared.module';

import { FormasDePagoRoutingModule } from './formas-de-pago-routing.module';
import { FwFormasDePagoPage } from './Paginas/fw-formas-de-pago/fw-formas-de-pago.page';
import { FwFormasDePagoCabeceraComponent } from './Paginas/fw-formas-de-pago/Componentes/fw-formas-de-pago-cabecera/fw-formas-de-pago-cabecera.component';
import { FwFormasDePagoInformacionComponent } from './Paginas/fw-formas-de-pago/Componentes/fw-formas-de-pago-informacion/fw-formas-de-pago-informacion.component';
import { FwFormasDePagoSistemasComponent } from './Paginas/fw-formas-de-pago/Componentes/fw-formas-de-pago-sistemas/fw-formas-de-pago-sistemas.component';
import { FwFormasDePagoTarjetaComponent } from './Paginas/fw-formas-de-pago/Componentes/fw-formas-de-pago-tarjeta/fw-formas-de-pago-tarjeta.component';

@NgModule({
  declarations: [FwFormasDePagoPage, FwFormasDePagoCabeceraComponent, FwFormasDePagoInformacionComponent, FwFormasDePagoSistemasComponent, FwFormasDePagoTarjetaComponent],
  imports: [
    CommonModule,
    FormasDePagoRoutingModule,
    SharedModule,
    FormsModule,
    NgbModule
  ]
})
export class FormasDePagoModule { }
