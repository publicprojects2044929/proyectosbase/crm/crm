import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwFormasDePagoSistemasComponent } from './fw-formas-de-pago-sistemas.component';

describe('FwFormasDePagoSistemasComponent', () => {
  let component: FwFormasDePagoSistemasComponent;
  let fixture: ComponentFixture<FwFormasDePagoSistemasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwFormasDePagoSistemasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwFormasDePagoSistemasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
