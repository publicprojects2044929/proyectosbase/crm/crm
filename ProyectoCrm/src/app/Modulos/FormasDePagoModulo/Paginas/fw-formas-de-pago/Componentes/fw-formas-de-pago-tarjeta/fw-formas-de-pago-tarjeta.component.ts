import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-fw-formas-de-pago-tarjeta',
  templateUrl: './fw-formas-de-pago-tarjeta.component.html',
  styleUrls: ['./fw-formas-de-pago-tarjeta.component.scss']
})
export class FwFormasDePagoTarjetaComponent implements OnInit {

  @Input('Titulo') tTitulo: string = "";
  @Input('Descripcion') tDescripcion: string = "";
  @Input('Icono') tIcono: string = "";
  @Input('Check') tCheck: number = 0;
  @Input('Nombre') tNombre: string = "";

  @Output('Cambiar') tCambiar: EventEmitter<string> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  CambiarCheck(){

    let tEstadoStr: string = !!this.tCheck === true ? 'S' : 'N';

    this.tCambiar.emit(tEstadoStr);

  }

}
