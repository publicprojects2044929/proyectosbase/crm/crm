import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwFormasDePagoCabeceraComponent } from './fw-formas-de-pago-cabecera.component';

describe('FwFormasDePagoCabeceraComponent', () => {
  let component: FwFormasDePagoCabeceraComponent;
  let fixture: ComponentFixture<FwFormasDePagoCabeceraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwFormasDePagoCabeceraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwFormasDePagoCabeceraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
