import { Component, OnInit, Input } from '@angular/core';
import { FormaDePago } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-formas-de-pago-informacion',
  templateUrl: './fw-formas-de-pago-informacion.component.html',
  styleUrls: ['./fw-formas-de-pago-informacion.component.scss']
})
export class FwFormasDePagoInformacionComponent implements OnInit {

  @Input('FormaDePado') tFormaDePago: FormaDePago = new FormaDePago();

  constructor() { }

  ngOnInit(): void {
  }

  Check_Aprobacion(tValor: string): void {

    this.tFormaDePago.Aprobacion = tValor;

  }

  Check_Referencia(tValor: string): void {

    this.tFormaDePago.Referencia = tValor;

  }

  Check_Tasa(tValor: string): void {

    this.tFormaDePago.Tasa = tValor;

  }

  Check_Duplicado(tValor: string): void {

    this.tFormaDePago.Duplicado = tValor;

  }

  Check_Vuelto(tValor: string): void {

    this.tFormaDePago.Vuelto = tValor;

  }

}
