import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwFormasDePagoTarjetaComponent } from './fw-formas-de-pago-tarjeta.component';

describe('FwFormasDePagoTarjetaComponent', () => {
  let component: FwFormasDePagoTarjetaComponent;
  let fixture: ComponentFixture<FwFormasDePagoTarjetaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwFormasDePagoTarjetaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwFormasDePagoTarjetaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
