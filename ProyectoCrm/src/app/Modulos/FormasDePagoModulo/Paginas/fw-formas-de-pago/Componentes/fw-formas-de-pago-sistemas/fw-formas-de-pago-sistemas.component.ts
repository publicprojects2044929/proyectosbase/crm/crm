import { Component, OnInit, Input } from '@angular/core';
import { FormaDePago } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-formas-de-pago-sistemas',
  templateUrl: './fw-formas-de-pago-sistemas.component.html',
  styleUrls: ['./fw-formas-de-pago-sistemas.component.scss']
})
export class FwFormasDePagoSistemasComponent implements OnInit {

  @Input('FormaDePado') tFormaDePago: FormaDePago = new FormaDePago();

  constructor() { }

  ngOnInit(): void {
  }

  Check_Crm(tValor: string): void {

    this.tFormaDePago.Crm = tValor;

  }

  Check_Web(tValor: string): void {

    this.tFormaDePago.Web = tValor;

  }

  Check_App(tValor: string): void {

    this.tFormaDePago.App = tValor;

  }

}
