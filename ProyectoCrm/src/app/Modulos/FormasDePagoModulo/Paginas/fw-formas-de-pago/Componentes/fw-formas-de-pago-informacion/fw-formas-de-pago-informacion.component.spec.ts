import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwFormasDePagoInformacionComponent } from './fw-formas-de-pago-informacion.component';

describe('FwFormasDePagoInformacionComponent', () => {
  let component: FwFormasDePagoInformacionComponent;
  let fixture: ComponentFixture<FwFormasDePagoInformacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwFormasDePagoInformacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwFormasDePagoInformacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
