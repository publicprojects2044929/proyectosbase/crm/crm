import { Component, OnInit, Input } from '@angular/core';
import { FormaDePago } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-formas-de-pago-cabecera',
  templateUrl: './fw-formas-de-pago-cabecera.component.html',
  styleUrls: ['./fw-formas-de-pago-cabecera.component.scss']
})
export class FwFormasDePagoCabeceraComponent implements OnInit {

  @Input('FormaDePago') tFormaDePago: FormaDePago = new FormaDePago();

  constructor() { }

  ngOnInit(): void {
  }

}
