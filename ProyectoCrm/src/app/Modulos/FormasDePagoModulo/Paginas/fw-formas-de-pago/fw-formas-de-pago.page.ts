import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormularioConfiguracion } from 'src/app/Clases/EstructuraConfiguracion';
import { Usuario, Empresas, FormaDePago, Formulario, Filtros } from 'src/app/Clases/Estructura';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/Core/Session/session.service';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { ToastrService } from 'ngx-toastr';
import { ErroresService } from 'src/app/Modales/mensajes/fw-modal-msj/errores.service';
import { MensajesService } from 'src/app/Modales/mensajes/fw-modal-msj/mensajes.service';
import { ConfirmacionService } from 'src/app/Modales/confirmacion/fw-modal-confirmacion/confirmacion.service';
import { Subscription } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Forma_PagosEntrada } from 'src/app/Clases/EstructuraEntrada';
import { ConfiguracionService } from 'src/app/Core/Configuracion/configuracion.service';

@Component({
  selector: 'app-fw-formas-de-pago',
  templateUrl: './fw-formas-de-pago.page.html',
  styleUrls: ['./fw-formas-de-pago.page.scss']
})
export class FwFormasDePagoPage implements OnInit, OnDestroy {

  tCodFormularioActual: string = "fFormasDePago";

  tConfiguracionFormulario: FormularioConfiguracion = new FormularioConfiguracion();
  tPermisoAgregar: boolean = false;
  tPermisoEditar: boolean = false;
  tPermisoEliminar: boolean = false;
  tHabilitarGuardar: boolean = false;

  tUsuarioActual: Usuario = new Usuario();
  tEmpresaActual: Empresas = new Empresas();
  tEmpresa$: Subscription;

  tTitulo: string = "Formas de pago";
  tNuevo: boolean = true;
  tPosicion: number = 0;
  tPagina: number = 1;
  tTamanoPag: number = 5;

  tFormapagos: FormaDePago[] = [];
  tFormaPagoActual: FormaDePago = new FormaDePago();

  tFormularios: Formulario[] = [];

  tFiltros: Filtros[] = [];
  tFiltroActual: Filtros = new Filtros();
  tFiltroBusqueda: string = "";

  tConfiguracion$: Subscription;
  tHayConfiguracionWs$: Subscription;
  tStrConfiguracion: string = 'FormasDePago/FormasDePago.Config.json';

  constructor(public router: Router,
    private tSesion: SessionService,
    private tConfiguracionService: ConfiguracionService,
    public Ws: ClsServiciosService,
    private toastr: ToastrService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService,
    private tConfServices: ConfirmacionService) {

    this.tFiltroBusqueda = "";
    this.tPosicion = 0;

    this.tFiltros = [

      { Codfiltro: "Codformapago", Filtro: "Código" },
      { Codfiltro: "Descripcion", Filtro: "Descripción" },

    ]

  }

  ngOnInit() {

    this.tConfiguracion$ = this.tConfiguracionService
      .Consumir_Obtener_Configuracion(this.tStrConfiguracion)
      .subscribe((tConfiguracion: FormularioConfiguracion) => {

        this.tConfiguracionFormulario = tConfiguracion;
        this.tTitulo = this.tConfiguracionFormulario.Titulo;
        this.tErroresServices.tTituloMsj = this.tConfiguracionFormulario.ModalMensaje.Titulo;
        this.tMsjServices.tTituloMsj = this.tConfiguracionFormulario.ModalMensaje.Titulo;
        this.tConfServices.tTituloConf = this.tConfiguracionFormulario.ModalConfirmacion.Titulo;

      });

    this.tEmpresa$ = this.tSesion.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    })

    var tTienePermiso: boolean = this.tSesion.ObtenerPermisoAcceso(this.tCodFormularioActual);
    this.tPermisoAgregar = this.tSesion.ObtenerPermisoAgregar();
    this.tPermisoEditar = this.tSesion.ObtenerPermisoEditar();
    this.tPermisoEliminar = this.tSesion.ObtenerPermisoEliminar();

    if (tTienePermiso == true) {

      this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
        .subscribe((tHayConfig: boolean) => {

          if (tHayConfig === true) {

            this.Buscar_FormasPago();
            this.Nuevo();

          }

        });

    }
    else {

      this.tMsjServices.AbrirModalMsj(environment.msjSinAcceso);
      this.router.navigate(['/Home']);

    }

  }

  ngOnDestroy() {

    this.tEmpresa$.unsubscribe();
    this.tConfiguracion$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();

  }

  //#region "Funciones de busqueda"

  Buscar_FormasPago(): void {

    this.Ws.FormasPago()
      .Consumir_Obtener_FormasPago(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa)
      .subscribe(Respuesta => {

        this.tFormapagos = [];

        if (Respuesta.Resultado == "N") {


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tFormapagos = Respuesta.Datos;
          this.tFiltroActual = this.tFiltros[0];

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  //#region "CRUD"

  Nuevo_FormasPago(): void {

    var tFormaPago: Forma_PagosEntrada;
    tFormaPago = {
      "Id": 0,
      "Corporacion": this.tEmpresaActual.Corporacion,
      "Empresa": this.tEmpresaActual.Empresa,
      "Codformapago": "",
      "Descripcion": this.tFormaPagoActual.Descripcion,
      "Crm": this.tFormaPagoActual.Crm != "" ? this.tFormaPagoActual.Crm : "N",
      "Web": this.tFormaPagoActual.Web != "" ? this.tFormaPagoActual.Web : "N",
      "App": this.tFormaPagoActual.App != "" ? this.tFormaPagoActual.App : "N",
      "Duplicado": this.tFormaPagoActual.Duplicado != "" ? this.tFormaPagoActual.Duplicado : "N",
      "Vuelto": this.tFormaPagoActual.Vuelto != "" ? this.tFormaPagoActual.Vuelto : "N",
      "Referencia": this.tFormaPagoActual.Referencia != "" ? this.tFormaPagoActual.Referencia : "N",
      "Tasa": this.tFormaPagoActual.Tasa != "" ? this.tFormaPagoActual.Tasa : "N",
      "Aprobacion": this.tFormaPagoActual.Aprobacion != "" ? this.tFormaPagoActual.Aprobacion : "N",
      "Usuariocreador": this.tUsuarioActual.Codusuario,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }

    this.Ws.FormasPago()
      .Consumir_Crear_FormasPago(tFormaPago)
      .subscribe(Respuesta => {
        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.toastr.success('La forma de pago se ha creado de manera exitosa!');
          this.tFiltroActual = this.tFiltros[0];
          this.Nuevo();
          this.Buscar_FormasPago();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Modificar_FormasPago(): void {

    var tModificarFormaDePago: Forma_PagosEntrada;
    tModificarFormaDePago = {
      "Id": 0,
      "Corporacion": this.tEmpresaActual.Corporacion,
      "Empresa": this.tEmpresaActual.Empresa,
      "Codformapago": this.tFormaPagoActual.Codformapago,
      "Descripcion": this.tFormaPagoActual.Descripcion,
      "Crm": this.tFormaPagoActual.Crm != "" ? this.tFormaPagoActual.Crm : "N",
      "Web": this.tFormaPagoActual.Web != "" ? this.tFormaPagoActual.Web : "N",
      "App": this.tFormaPagoActual.App != "" ? this.tFormaPagoActual.App : "N",
      "Duplicado": this.tFormaPagoActual.Duplicado != "" ? this.tFormaPagoActual.Duplicado : "N",
      "Vuelto": this.tFormaPagoActual.Vuelto != "" ? this.tFormaPagoActual.Vuelto : "N",
      "Referencia": this.tFormaPagoActual.Referencia != "" ? this.tFormaPagoActual.Referencia : "N",
      "Tasa": this.tFormaPagoActual.Tasa != "" ? this.tFormaPagoActual.Tasa : "N",
      "Aprobacion": this.tFormaPagoActual.Aprobacion != "" ? this.tFormaPagoActual.Aprobacion : "N",
      "Usuariocreador": this.tUsuarioActual.Codusuario,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }

    this.Ws.FormasPago()

      .Consumir_Modificar_FormasPago(tModificarFormaDePago)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.toastr.success('La forma de pago se ha modificado de manera exitosa!');
          this.tFiltroActual = this.tFiltros[0];
          this.Nuevo();
          this.Buscar_FormasPago();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Eliminar_FormasPago(): void {

    var tEliminarFormaDePago: Forma_PagosEntrada;
    tEliminarFormaDePago = {
      "Id": 0,
      "Corporacion": this.tEmpresaActual.Corporacion,
      "Empresa": this.tEmpresaActual.Empresa,
      "Codformapago": this.tFormaPagoActual.Codformapago,
      "Descripcion": this.tFormaPagoActual.Descripcion,
      "Crm": this.tFormaPagoActual.Crm != "" ? this.tFormaPagoActual.Crm : "N",
      "Web": this.tFormaPagoActual.Web != "" ? this.tFormaPagoActual.Web : "N",
      "App": this.tFormaPagoActual.App != "" ? this.tFormaPagoActual.App : "N",
      "Duplicado": this.tFormaPagoActual.Duplicado != "" ? this.tFormaPagoActual.Duplicado : "N",
      "Vuelto": this.tFormaPagoActual.Vuelto != "" ? this.tFormaPagoActual.Vuelto : "N",
      "Referencia": this.tFormaPagoActual.Referencia != "" ? this.tFormaPagoActual.Referencia : "N",
      "Tasa": this.tFormaPagoActual.Tasa != "" ? this.tFormaPagoActual.Tasa : "N",
      "Aprobacion": this.tFormaPagoActual.Aprobacion != "" ? this.tFormaPagoActual.Aprobacion : "N",
      "Usuariocreador": this.tUsuarioActual.Codusuario,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }

    this.Ws.FormasPago()

      .Consumir_Eliminar_FormasPago(tEliminarFormaDePago)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.toastr.success('La forma de pago se ha eliminado de manera exitosa!');
          this.tFiltroActual = this.tFiltros[0];
          this.Nuevo();
          this.Buscar_FormasPago();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  Nuevo(): void {

    this.tHabilitarGuardar = this.tPermisoAgregar;
    this.tNuevo = true;
    this.tFormaPagoActual = new FormaDePago();
    this.toastr.info('Por favor llene los datos necesarios para registrar una nueva Forma de pago!');

  }

  Seleccionar_Formapago(tFormaPago_Sel: FormaDePago, tPos: number): void {

    this.tHabilitarGuardar = this.tPermisoEditar;
    this.tNuevo = false;
    this.tPosicion = tPos;
    this.tFormaPagoActual = new FormaDePago();
    Object.assign(this.tFormaPagoActual, tFormaPago_Sel);
    this.tFormaPagoActual.Usuariomodificador = this.tUsuarioActual.Usuariomodificador;

  }

  Seleccionar_Formapago_Eliminar(tFormaPago_Sel: FormaDePago): void {

    this.tFormaPagoActual = new FormaDePago();
    Object.assign(this.tFormaPagoActual, tFormaPago_Sel);
    this.tFormaPagoActual.Usuariomodificador = this.tUsuarioActual.Usuariomodificador;

    this.tConfServices
      .AbrirModalConf(this.tConfiguracionFormulario.ModalConfirmacion.Eliminacion)
      .then(Resultado => {

        switch (Resultado) {
          case "S": {

            this.Eliminar_FormasPago();
            break;

          }
        }

      });

  }

  Cancelar(): void {

    this.Nuevo();

  }

  ValidarFormulario(): boolean {

    let valido: boolean = false;

    if (this.tFormaPagoActual.Descripcion == "") {

      this.toastr.warning('Debe ingresar una descripciòn!', 'Disculpe!');
      return valido;

    }

    return true;
  }

  Agregar(): void {

    if (this.ValidarFormulario() == false) {
      return
    }

    switch (this.tNuevo) {
      case true: {

        this.tConfServices
          .AbrirModalConf(this.tConfiguracionFormulario.ModalConfirmacion.Agregar)
          .then(Resultado => {

            switch (Resultado) {
              case "S": {

                this.Nuevo_FormasPago();
                break;

              }
            }

          });
        break;

      }
      default: {

        this.tConfServices
          .AbrirModalConf(this.tConfiguracionFormulario.ModalConfirmacion.Edicion)
          .then(Resultado => {

            switch (Resultado) {
              case "S": {

                this.Modificar_FormasPago();
                break;

              }
            }

          });
        break;

      }
    }
  }

}
