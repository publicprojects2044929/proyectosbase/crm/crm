import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwFormasDePagoPage } from './fw-formas-de-pago.page';

describe('FwFormasDePagoPage', () => {
  let component: FwFormasDePagoPage;
  let fixture: ComponentFixture<FwFormasDePagoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwFormasDePagoPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwFormasDePagoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
