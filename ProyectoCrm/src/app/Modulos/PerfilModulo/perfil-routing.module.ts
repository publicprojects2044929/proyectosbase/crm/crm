import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FwPerfilPage } from './Paginas/fw-perfil/fw-perfil.page'
import { FwPlantillaComponent  } from 'src/app/Shared/Formularios/fw-plantilla/fw-plantilla.component'


const routes: Routes = [
  {
    path: '',
    component: FwPlantillaComponent,
    children:[
      {
        path: '',
        component: FwPerfilPage,
        data:{
          Codformulario: 'fPerfil'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PerfilRoutingModule { }
