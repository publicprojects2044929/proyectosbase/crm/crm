import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { PerfilRoutingModule } from './perfil-routing.module';
import { FwPerfilPage } from './Paginas/fw-perfil/fw-perfil.page';

import { SharedModule } from 'src/app/Shared/shared.module';

import { FwPerfilInformacionComponent } from './Paginas/fw-perfil/Componentes/fw-perfil-informacion/fw-perfil-informacion.component';
import { FwPerfilConfiguracionComponent } from './Paginas/fw-perfil/Componentes/fw-perfil-configuracion/fw-perfil-configuracion.component';
import { FwPerfilPermisosComponent } from './Paginas/fw-perfil/Componentes/fw-perfil-permisos/fw-perfil-permisos.component';
import { FwPerfilCabeceraComponent } from './Paginas/fw-perfil/Componentes/fw-perfil-cabecera/fw-perfil-cabecera.component';

@NgModule({
  declarations: [FwPerfilPage, FwPerfilInformacionComponent,FwPerfilPermisosComponent,FwPerfilCabeceraComponent,FwPerfilConfiguracionComponent],
  imports: [
    CommonModule,
    PerfilRoutingModule,
    SharedModule,
    FormsModule,
    NgbModule
  ]
})
export class PerfilModule { }
