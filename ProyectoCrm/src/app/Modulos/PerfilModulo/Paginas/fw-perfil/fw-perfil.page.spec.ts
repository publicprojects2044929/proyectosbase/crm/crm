import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwPerfilPage } from './fw-perfil.page';

describe('FwPerfilComponent', () => {
  let component: FwPerfilPage;
  let fixture: ComponentFixture<FwPerfilPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwPerfilPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwPerfilPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
