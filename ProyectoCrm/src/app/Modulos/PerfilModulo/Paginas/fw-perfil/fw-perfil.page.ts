import { Component, OnInit, OnDestroy } from '@angular/core';
import { Usuario, Empresas, Tipo, Estatus, Filtros, GruposUsuario, Grupos, Configuracion } from 'src/app/Clases/Estructura';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/Core/Session/session.service';
import { ToastrService } from 'ngx-toastr';
import { ErroresService } from 'src/app/Modales/mensajes/fw-modal-msj/errores.service';
import { MensajesService } from 'src/app/Modales/mensajes/fw-modal-msj/mensajes.service';
import { ConfirmacionService } from 'src/app/Modales/confirmacion/fw-modal-confirmacion/confirmacion.service';
import { environment } from 'src/environments/environment';
import { UsuarioEntrada, UsuarioEmpresasEntrada, UsuarioGruposEntrada } from 'src/app/Clases/EstructuraEntrada';
import { PermisosService } from 'src/app/Modales/permisos/fw-modal-permisos/permisos.service'
import { Subscription } from 'rxjs';
import { FormularioConfiguracion } from 'src/app/Clases/EstructuraConfiguracion';
import { ConfiguracionService } from 'src/app/Core/Configuracion/configuracion.service';
import { ManejarImagenes, Validaciones } from 'src/app/Manejadores/cls-procedimientos';


@Component({
  selector: 'app-fw-perfil',
  templateUrl: './fw-perfil.page.html',
  styleUrls: ['./fw-perfil.page.scss']
})
export class FwPerfilPage implements OnInit, OnDestroy {

  tConfiguracionFormulario: FormularioConfiguracion = new FormularioConfiguracion();
  tCodFormularioActual: string = "fUsuarios";
  tPermisoAgregar: boolean = false;
  tPermisoEditar: boolean = false;
  tPermisoEliminar: boolean = false;
  tHabilitarGuardar: boolean = false;
  tBloquearBuscar: boolean = false;

  tUsuarioActual: Usuario = new Usuario();
  tEmpresaActual: Empresas = new Empresas();
  tEmpresa$: Subscription;
  tUsuario$: Subscription;

  tConfiguracion: Configuracion = new Configuracion();
  tManejarImagen: ManejarImagenes = new ManejarImagenes();
  tValidaciones: Validaciones = new Validaciones();

  tTitulo: string = "";
  tNuevo: boolean = true;
  tPosicion: number = 0;
  tPagina: number = 1;
  tTamanoPag: number = 5;

  tUsuarios: Usuario[] = [];
  tUsuario: Usuario = new Usuario();

  tUsuariosGrupo: Grupos[] = [];
  tUsuarioGrupo: Grupos = new Grupos();

  tTipoActual: Tipo = new Tipo();
  tTipos: Tipo[] = [];

  tEstatusActual: Estatus = new Estatus();
  tEstatus: Estatus[] = [];

  tFiltros: Filtros[] = [];
  tFiltroActual: Filtros = new Filtros();
  tFiltroBusqueda: string = "";
 
  tGruposNuevo: GruposUsuario[] = [];
  tGrupos: GruposUsuario[] = [];
  tCodGrupoUsuario:string ="";

  tConfiguracion$: Subscription;
  tHayConfiguracionWs$: Subscription;
  tStrConfiguracion: string = 'Perfil/Perfil.Config.json';
 
  tConfirmarClave: string ="";
  tNuevaClave:string ="";
 
  constructor(
    public Ws: ClsServiciosService,
    private tConfiguracionService: ConfiguracionService,
    private router: Router,
    private tSesion: SessionService,
    private toastr: ToastrService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService,
    private tConfServices: ConfirmacionService,
    private tPermisosService: PermisosService,
    
  ) {

    this.tFiltroBusqueda = "";

    this.tEstatus = [

      {
        "Id": 0,
        "Corporacion": "",
        "Empresa": "",
        "Codestatus": "",
        "Descripcion": "",
        "Secuencia": 0,
        "Tabla": "",
        "Hexadecimal": "",
        "Fecha_creacion": new Date(),
        "Fecha_creacionl": 0,
        "Fecha_modificacion": new Date(),
        "Fecha_modificacionl": 0,
        "Usuariocreador": "",
        "Usuariomodificador": ""
      }

    ];

    this.tFiltros = [

      { Codfiltro: "Codusuario", Filtro: "Código" },
      { Codfiltro: "Descripcion", Filtro: "Descripcion" },

    ]

    this.tTipos = [

      {
        "Id": 0,
        "Corporacion": "",
        "Empresa": "",
        "Codtipo": "",
        "Descripcion": "",
        "Tabla": "",
        "Columna": "",
        "Abreviacion": "",
        "Hexadecimal": "",
        "Fecha_creacion": new Date(),
        "Fecha_creacionl": 0,
        "Fecha_modificacion": new Date(),
        "Fecha_modificacionl": 0,
        "Usuariocreador": "",
        "Usuariomodificador": ""
      }

    ];

  }

  ngOnInit() {

    this.tConfiguracion$ = this.tConfiguracionService
      .Consumir_Obtener_Configuracion(this.tStrConfiguracion)
      .subscribe((tConfiguracion: FormularioConfiguracion) => {

        this.tConfiguracionFormulario = tConfiguracion;
        this.tTitulo = this.tConfiguracionFormulario.Titulo;
        this.tErroresServices.tTituloMsj = this.tConfiguracionFormulario.ModalMensaje.Titulo;
        this.tMsjServices.tTituloMsj = this.tConfiguracionFormulario.ModalMensaje.Titulo;
        this.tConfServices.tTituloConf = this.tConfiguracionFormulario.ModalConfirmacion.Titulo;
      });

    this.tEmpresa$ = this.tSesion.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    })

    this.tUsuario$ = this.tSesion.tUsuarioActual$.subscribe((tUsuario: Usuario) => {

      this.tUsuarioActual = tUsuario;

    });


    var tTienePermiso: boolean = this.tSesion.ObtenerPermisoAcceso(this.tCodFormularioActual);
    this.tPermisoAgregar = this.tSesion.ObtenerPermisoAgregar();
    this.tPermisoEditar = this.tSesion.ObtenerPermisoEditar();
    this.tPermisoEliminar = this.tSesion.ObtenerPermisoEliminar();

    if (tTienePermiso === true) {

      this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
        .subscribe((tHayConfig: boolean) => {

          if (tHayConfig === true) {
            this.Nuevo();
            this.BuscarConfiguracion();
            this.BuscarUsuario();
          }

        });

    }
    else {

      this.tMsjServices.AbrirModalMsj(environment.msjSinAcceso);
      this.router.navigate(['/Home']);

    }

  }

  ngOnDestroy() {

    this.tUsuario$.unsubscribe();
    this.tEmpresa$.unsubscribe();
    this.tConfiguracion$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();

  }

  //#region "Funciones de busqueda"

  BuscarUsuario(): void {
  //this.tUsuario = this.tUsuarioActual;
   this.tManejarImagen.obtenerImagen64();

   this.Ws.Usuario()
   .Consumir_Obtener_UsuarioActual(this.tEmpresaActual.Corporacion,
                                  this.tEmpresaActual.Empresa,
                                  this.tUsuarioActual.Codusuario)
   .subscribe(Respuesta => {

    

     if (Respuesta.Resultado == "N") {

       // this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);
       // 

     }
     else if (Respuesta.Resultado == "E") {

       this.tMsjServices.AbrirModalMsj(environment.msjError);


     }
     else {
     
      this.tUsuario = Respuesta.Datos[0];
     }

   }, error => {

     this.tErroresServices.MostrarError(error);

   });

  }


  BuscarConfiguracion(): void {

    this.Ws
      .Configuraciones()
      .Consumir_Obtener_Configuracion(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        'MR_USUARIO',
        'IMAGEN')
      .subscribe(Respuesta => {

        this.tConfiguracion = new Configuracion();

        if (Respuesta.Resultado == "N") {

          // this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tConfiguracion = Respuesta.Datos[0];

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  //#region "Modal"


  //#endregion

  Nuevo(): void {

    this.tHabilitarGuardar = this.tPermisoAgregar;
    this.tUsuario = new Usuario();
    this.tManejarImagen.tImagen="";
    this.ObtenerImagenPerfil();
    this.toastr.clear();
  }

  Cancelar(): void {

    this.Nuevo();
    this.BuscarUsuario();

  }

  ValidarFormulario(): boolean {

    let valido: boolean = false;

    if (this.tUsuario.Codusuario == "") {
      this.toastr.warning('Debe ingresar un Usuario!', 'Disculpe!')
      return valido
    }

    if (this.tUsuario.Descripcion == "") {
      this.toastr.warning('Debe ingresar una Descripciòn!', 'Disculpe!')
      return valido
    }

    if (this.tUsuario.Cedula == "") {
      this.toastr.warning('Debe ingresar un Nùmero de identificaciòn !', 'Disculpe!')
      return valido
    }

    if (this.tValidaciones.ValidarEmail(this.tUsuario.Email) == false) {
      this.toastr.error('Debe ingresar un Email vàlido', 'Disculpe!')
      return valido
    }
     if (this.tNuevaClave != this.tConfirmarClave){
      this.toastr.error('Las contraseñas ingresadas no coinciden', 'Disculpe!')
      return valido
     }
     
    return true;

  }


  Agregar(): void {

    if (this.ValidarFormulario() == false) {
      return
    }

    this.tConfServices
      .AbrirModalConf(this.tConfiguracionFormulario.ModalConfirmacion.Edicion)
      .then(Resultado => {

        switch (Resultado) {
          case "S": {

            this.Modificar_Usuario();
            break;

          }
        }

      });
  }

  Modificar_Usuario(): void {

    if (this.tNuevaClave != "" && (this.tUsuario.Clave != this.tNuevaClave)){
      this.tUsuario.Clave =this.tNuevaClave;
     }

    var tModificarUsuario: UsuarioEntrada;
    tModificarUsuario = {
      "Id": 0,
      "Corporacion": this.tUsuario.Corporacion,
      "Empresa": this.tUsuario.Empresa,
      "Codusuario": this.tUsuario.Codusuario,
      "Descripcion": this.tUsuario.Descripcion,
      "Clave": this.tUsuario.Clave,
      "Cedula": this.tUsuario.Cedula,
      "Email": this.tUsuario.Email,
      "Estatus": this.tUsuario.Estatus,
      "Imagen64": this.tManejarImagen.obtenerImagen64(),
      "Url": this.tUsuario.Url,
      "Conectado": this.tUsuario.Conectado,
      "Token": this.tUsuario.Token,
      "Dispositivo": this.tUsuario.Dispositivo,
      "Tipo": this.tUsuario.Tipo,
      "Cambiar_clave": "N",
      "Oculto": this.tUsuario.Oculto,
      "Usuariocreador": this.tUsuarioActual.Codusuario,
      "Usuariomodificador": this.tUsuarioActual.Codusuario,
      "LUsuario_Empresas": []
    }

    this.Ws.Usuario().Consumir_Modificar_Usuario(tModificarUsuario).subscribe(Respuesta => {

      if (Respuesta.Resultado == "N") {

        this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

      }
      else if (Respuesta.Resultado == "E") {

        this.tMsjServices.AbrirModalMsj(environment.msjError);


      }
      else {

        this.toastr.clear();
        this.toastr.success('El usuario se ha modificado de manera exitosa!');
        this.tFiltroActual = this.tFiltros[0];
        this.BuscarUsuario();


      }

    }, error => {

      this.tErroresServices.MostrarError(error);

    });

  }

  BloquearImagen(): boolean {

    if (this.tConfiguracion.Valor == "S") {

      return false;

    }
    else {
      
    return true;

    }

  }

  ObtenerImagenPerfil(): string {

    let tImagenUrl: string = this.tManejarImagen.ObtenerImagen();

     if (this.tUsuario.Url!=""){

        tImagenUrl = this.tUsuario.Url;
      }

      return tImagenUrl;
     
    }

    
  ObtenerConfirmacionClave(tClave :string){

    this.tConfirmarClave = tClave;
  }

  ObtenerNuevaClave(tClave :string){

    this.tNuevaClave = tClave;
  }
  

}
