import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwPerfilPermisosComponent } from './fw-perfil-permisos.component';

describe('FwPerfilPermisosComponent', () => {
  let component: FwPerfilPermisosComponent;
  let fixture: ComponentFixture<FwPerfilPermisosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwPerfilPermisosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwPerfilPermisosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
