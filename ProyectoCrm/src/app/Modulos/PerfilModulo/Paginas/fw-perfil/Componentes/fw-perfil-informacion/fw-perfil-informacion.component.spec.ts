import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwPerfilInformacionComponent } from './fw-perfil-informacion.component';

describe('FwPerfilInformacionComponent', () => {
  let component: FwPerfilInformacionComponent;
  let fixture: ComponentFixture<FwPerfilInformacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwPerfilInformacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwPerfilInformacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
