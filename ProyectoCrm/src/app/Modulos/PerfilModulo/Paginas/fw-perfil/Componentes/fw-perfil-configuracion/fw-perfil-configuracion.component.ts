import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { empty } from 'apollo-link';
import { Usuario } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-perfil-configuracion',
  templateUrl: './fw-perfil-configuracion.component.html',
  styleUrls: ['./fw-perfil-configuracion.component.scss']
})
export class FwPerfilConfiguracionComponent implements OnInit {

  @Input('Usuario') tUsuario: Usuario = new Usuario();
  @Output('ConfirmarClave') tConfirmarClave: EventEmitter<any> = new EventEmitter();
  @Output('NuevaClave') tNuevaClave: EventEmitter<any> = new EventEmitter();
  
  constructor() { }

  ngOnInit(): void {
    this.tConfirmarClave.emit('');
    this.tNuevaClave.emit('');
    var initCampos = document.getElementById("MostrarCampos"); 
    initCampos.style.display = 'none';
  }

  ConsultarMostrarCampos()
  {
    if (document.getElementById){ //se obtiene el id
      var tMostrarCampos = document.getElementById("MostrarCampos");
      var tNuevaClaveId = (<HTMLInputElement>document.getElementById("NuevaClave"));
      var tConfirmarClaveId = (<HTMLInputElement>document.getElementById("ConfirmarClave"))
      tNuevaClaveId.value = ""; 
      tConfirmarClaveId.value = ""; 
      tMostrarCampos.style.display = (tMostrarCampos.style.display == 'none') ? 'block' : 'none'; 
      }
  }
  MostrarContrasena(id : string){
    if(document.getElementById)
    {
      var tMostrarClave = document.getElementById(id)
      if(tMostrarClave.getAttribute('Type') == "password")
      {
        tMostrarClave.setAttribute("Type","text");
      }else
      {
        tMostrarClave.setAttribute("Type","password");
      }
    }

  }

  ObtenerConfirmacionClave(tClave :any){
    this.tConfirmarClave.emit(tClave.value);
  }

  ObtenerNuevaClave(tClave :any){

    this.tNuevaClave.emit(tClave.value);
  }

}
