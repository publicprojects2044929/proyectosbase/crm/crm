import { Component, OnInit,Input } from '@angular/core';
import { Usuario } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-perfil-cabecera',
  templateUrl: './fw-perfil-cabecera.component.html',
  styleUrls: ['./fw-perfil-cabecera.component.scss']
})
export class FwPerfilCabeceraComponent implements OnInit {

  @Input('Usuario') tUsuario: Usuario = new Usuario();
  constructor() { }

  ngOnInit(): void {
  }

}
