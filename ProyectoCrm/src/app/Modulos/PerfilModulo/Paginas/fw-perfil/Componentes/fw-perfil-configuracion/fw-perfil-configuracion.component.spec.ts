import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwPerfilConfiguracionComponent } from './fw-perfil-configuracion.component';

describe('FwPerfilConfiguracionComponent', () => {
  let component: FwPerfilConfiguracionComponent;
  let fixture: ComponentFixture<FwPerfilConfiguracionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwPerfilConfiguracionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwPerfilConfiguracionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
