import { NumberFormatStyle } from '@angular/common';
import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { format } from 'path';
import { Filtros, Formulario, FormularioPadre, GrupoFormularios, GrupoFormulariosTareas, Grupos, Usuario } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-perfil-permisos',
  templateUrl: './fw-perfil-permisos.component.html',
  styleUrls: ['./fw-perfil-permisos.component.scss']
})
export class FwPerfilPermisosComponent implements OnInit {

  @Input('UsuarioModulos') tUsuarioModulos: FormularioPadre[] = [];

  tFiltros: Filtros[] = [];
  tFiltroActual: Filtros = new Filtros();
  tFiltroBusqueda: string = "";

  tFormularios: Formulario[] = []; 
  tFormularioActual: GrupoFormularios = new GrupoFormularios();

  tPaginaFormulario: number = 1;
  tTamanoPagFormulario: number = 10;

  tPaginaFormularioTareas: number = 1;
  tTamanoPagFormularioTareas: number = 10;

  constructor() {

    this.tFiltroBusqueda = "";

    this.tFiltros = [

      { Codfiltro: "Codgrupo", Filtro: "Código" },
      { Codfiltro: "Descripcion", Filtro: "Descripción" },

    ]
  }

  ngOnInit(): void {
    this.CargarFormularios();
  }

  CargarFormularios(){
    this.tUsuarioModulos.forEach((tFormPadre:FormularioPadre) => {
      tFormPadre.LFormularios.forEach((tForm: Formulario) => {
        this.tFormularios.push(tForm);
      });
    });
  }

  ngOnChanges(changes: SimpleChanges){

    if('tUsuarioModulos' in changes){
      this.tFiltroActual = this.tFiltros[0] || new Filtros();
      this.tFormularioActual = new GrupoFormularios();
    }

  }


  SeleccionarFormulario(tFormularioSel: GrupoFormularios) {

    this.tFormularioActual = tFormularioSel;

  }
  
  CheckTarea(tTareaSel: GrupoFormulariosTareas) {

    if (tTareaSel.Asignado === 1) {

      tTareaSel.Asignado = 0;

    }
    else {

      tTareaSel.Asignado = 1;

    }

    this.tFormularioActual.LTareas.map((tTarea: GrupoFormulariosTareas) => {

      if (tTarea.Corporacion === tTareaSel.Corporacion &&
        tTarea.Empresa === tTareaSel.Empresa &&
        tTarea.Codformulario === tTareaSel.Codformulario &&
        tTarea.Codtarea === tTareaSel.Codtarea) {

        tTarea.Asignado = tTareaSel.Asignado;

      }

    });

    this.tFormularios.map(tFormulario => {

      if (tFormulario.Corporacion === this.tFormularioActual.Corporacion &&
        tFormulario.Empresa === this.tFormularioActual.Empresa &&
        tFormulario.Codformulario === this.tFormularioActual.Codformulario) {

        tFormulario.LTareas = this.tFormularioActual.LTareas;

      }

    });

  }

}
