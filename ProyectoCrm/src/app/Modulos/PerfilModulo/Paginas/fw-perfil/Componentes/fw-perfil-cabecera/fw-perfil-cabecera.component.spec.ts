import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwPerfilCabeceraComponent } from './fw-perfil-cabecera.component';

describe('FwPerfilCabeceraComponent', () => {
  let component: FwPerfilCabeceraComponent;
  let fixture: ComponentFixture<FwPerfilCabeceraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwPerfilCabeceraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwPerfilCabeceraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
