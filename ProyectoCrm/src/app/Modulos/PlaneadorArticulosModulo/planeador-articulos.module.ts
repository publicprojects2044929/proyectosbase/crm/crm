import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlaneadorArticulosRoutingModule } from './planeador-articulos-routing.module';
import { FwPlaneadorArticulosComponent } from './Formularios/fw-planeador-articulos/fw-planeador-articulos.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { NgSelectModule } from '@ng-select/ng-select';

import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { SharedModule } from 'src/app/Shared/shared.module'

@NgModule({
  declarations: [FwPlaneadorArticulosComponent],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    BsDatepickerModule.forRoot(),
    NgMultiSelectDropDownModule.forRoot(),
    NgSelectModule,
    NgbModule,
    PlaneadorArticulosRoutingModule
  ]
})
export class PlaneadorArticulosModule { }
