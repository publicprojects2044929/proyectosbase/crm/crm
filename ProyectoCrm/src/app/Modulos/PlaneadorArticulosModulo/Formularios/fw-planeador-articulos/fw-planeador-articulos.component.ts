import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/Core/Session/session.service';

import {
  Empresas, Usuario, Formulario, Articulo, Filtros,
  Clasificacion1, Clasificacion2, Clasificacion3,
  Fabricante, Marca, Modelo, Reportes, Clasificacion4
} from 'src/app/Clases/Estructura';

import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { ManejarPdf, ManejarArchivo } from 'src/app/Manejadores/cls-procedimientos';
import { FormularioConfiguracion } from 'src/app/Clases/EstructuraConfiguracion';
import { ErroresService } from 'src/app/Modales/mensajes/fw-modal-msj/errores.service';
import { MensajesService } from 'src/app/Modales/mensajes/fw-modal-msj/mensajes.service';
import { environment } from 'src/environments/environment';
import { CargarArticulosService } from 'src/app/Modales/cargar-articulos/fw-modal-cargar-articulos/cargar-articulos.service'
import { Subscription } from 'rxjs';
import { PieService } from 'src/app/Core/Graficos/Pie/pie.service';
import { BarrasService } from 'src/app/Core/Graficos/Barras/barras.service';
import { ConfiguracionService } from 'src/app/Core/Configuracion/configuracion.service';

@Component({
  selector: 'app-fw-planeador-articulos',
  templateUrl: './fw-planeador-articulos.component.html',
  styleUrls: ['./fw-planeador-articulos.component.scss']
})
export class FwPlaneadorArticulosComponent implements OnInit, OnDestroy {

  ConfiguracionFormulario: FormularioConfiguracion = new FormularioConfiguracion;
  tCodFormularioActual: string = "fPlaneadorArticulo";
  tManejarPdf: ManejarPdf = new ManejarPdf();
  tManejarArchivos: ManejarArchivo = new ManejarArchivo();

  tUsuarioActual: Usuario = new Usuario();
  tEmpresaActual: Empresas = new Empresas();
  tEmpresa$: Subscription;

  tTitulo: string = "";
  tPosicion: number = 0;
  tPagina: number = 1;
  tTamanoPag: number = 5;

  tFiltros: Filtros[] = [];
  tFiltroActual: Filtros = new Filtros;
  tFiltroBusqueda: string = "";

  tArticulos: Articulo[] = [];

  tClasificaciones4: Clasificacion4[] = [];
  tClasificaciones4Actuales: Clasificacion4[] = [];

  tClasificaciones1: Clasificacion1[] = [];
  tClasificacion1Actual: Clasificacion1 = new Clasificacion1;

  tClasificaciones2: Clasificacion2[] = [];
  tClasificacion2Actual: Clasificacion2 = new Clasificacion2;

  tClasificaciones3: Clasificacion3[] = [];
  tClasificacion3Actual: Clasificacion3 = new Clasificacion3;

  tFabricantes: Fabricante[] = [];
  tFabricanteActual: Fabricante = new Fabricante;

  tMarcas: Marca[] = [];
  tMarcaActual: Marca = new Marca;

  tModelos: Modelo[] = [];
  tModeloActuales: Modelo[] = [];

  tFormularios: Formulario[] = [];
  tConfiguracionClasificacion1: {};
  tConfiguracionClasificacion2: {};
  tConfiguracionClasificacion3: {};
  tConfiguracionClasificacion4: {};
  tConfiguracionFabricante: {};
  tConfiguracionMarca: {};
  tConfiguracionModelo: {};

  tConfiguracion$: Subscription;
  tHayConfiguracionWs$: Subscription;
  tStrConfiguracion: string = 'PlaneadorArticulos/PlaneadorArticulo.Config.json';

  constructor(public router: Router,
    public Ws: ClsServiciosService,
    private tConfiguracionService: ConfiguracionService,
    private tSesion: SessionService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService,
    private tCargarArticuloService: CargarArticulosService) {

    this.tPosicion = 0;

    this.tFiltros = [

      { Codfiltro: "Codarticulo", Filtro: "Artículo" },
      { Codfiltro: "Descripcion", Filtro: "Nombre" },

    ]

    this.tConfiguracionClasificacion1 = {
      singleSelection: true,
      idField: 'Codclasificacion1',
      textField: 'Descripcion',
      selectAllText: 'Seleccionar todos',
      unSelectAllText: 'Deseleccionar todos',
      itemsShowLimit: 3,
      allowSearchFilter: true,
      searchPlaceholderText: "Buscar",
      noDataAvailablePlaceholderText: "Sin clasificación registradas",
      closeDropDownOnSelection: true
    };

    this.tConfiguracionClasificacion2 = {
      singleSelection: true,
      idField: 'Codclasificacion2',
      textField: 'Descripcion',
      selectAllText: 'Seleccionar todos',
      unSelectAllText: 'Deseleccionar todos',
      itemsShowLimit: 3,
      allowSearchFilter: true,
      searchPlaceholderText: "Buscar",
      noDataAvailablePlaceholderText: "Sin clasificación registradas",
      closeDropDownOnSelection: true
    };

    this.tConfiguracionClasificacion3 = {
      singleSelection: true,
      idField: 'Codclasificacion3',
      textField: 'Descripcion',
      selectAllText: 'Seleccionar todos',
      unSelectAllText: 'Deseleccionar todos',
      itemsShowLimit: 3,
      allowSearchFilter: true,
      searchPlaceholderText: "Buscar",
      noDataAvailablePlaceholderText: "Sin clasificación registradas",
      closeDropDownOnSelection: true
    };

    this.tConfiguracionClasificacion4 = {
      singleSelection: false,
      enableCheckAll: false,
      idField: 'Codclasificacion4',
      textField: 'Descripcion',
      selectAllText: 'Seleccionar todos',
      unSelectAllText: 'Deseleccionar todos',
      itemsShowLimit: 3,
      allowSearchFilter: true,
      searchPlaceholderText: "Buscar",
      noDataAvailablePlaceholderText: "Sin clasificación registradas"
    };

    this.tConfiguracionFabricante = {
      singleSelection: true,
      idField: 'Codfabricante',
      textField: 'Descripcion',
      selectAllText: 'Seleccionar todos',
      unSelectAllText: 'Deseleccionar todos',
      itemsShowLimit: 3,
      allowSearchFilter: true,
      searchPlaceholderText: "Buscar",
      noDataAvailablePlaceholderText: "Sin fabricante registrados",
      closeDropDownOnSelection: true
    };

    this.tConfiguracionMarca = {
      singleSelection: true,
      idField: 'Codmarca',
      textField: 'Descripcion',
      selectAllText: 'Seleccionar todos',
      unSelectAllText: 'Deseleccionar todos',
      itemsShowLimit: 3,
      allowSearchFilter: true,
      searchPlaceholderText: "Buscar",
      noDataAvailablePlaceholderText: "Sin marca registradas",
      closeDropDownOnSelection: true
    };

    this.tConfiguracionModelo = {
      singleSelection: false,
      enableCheckAll: false,
      idField: 'Codmodelo',
      textField: 'Descripcion',
      selectAllText: 'Seleccionar todos',
      unSelectAllText: 'Deseleccionar todos',
      itemsShowLimit: 3,
      allowSearchFilter: true,
      searchPlaceholderText: "Buscar",
      noDataAvailablePlaceholderText: "Sin modelos registrados",
      closeDropDownOnSelection: true
    };

  }

  ngOnInit() {

    this.tConfiguracion$ = this.tConfiguracionService
      .Consumir_Obtener_Configuracion(this.tStrConfiguracion)
      .subscribe((tConfiguracion: FormularioConfiguracion) => {

        this.ConfiguracionFormulario = tConfiguracion;
        this.tTitulo = this.ConfiguracionFormulario.Titulo;

      });

    this.tEmpresa$ = this.tSesion.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    })

    var tTienePermiso: boolean = this.tSesion.ObtenerPermisoAcceso(this.tCodFormularioActual); //false;

    if (tTienePermiso == true) {

      this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
        .subscribe((tHayConfig: boolean) => {

          if (tHayConfig === true) {

            this.Buscar_Clasificaciones1();
            this.Buscar_Fabricantes();
            this.BuscarArticulos();

          }

        });

    }
    else {

      this.tMsjServices.AbrirModalMsj(environment.msjSinAcceso);
      this.router.navigate(['/Home']);

    }

  }

  ngOnDestroy() {

    this.tEmpresa$.unsubscribe();
    this.tConfiguracion$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();

  }

  //#region "Funciones Modal"

  AbrirModalArticulos(): void {

    this.tCargarArticuloService
      .AbrirModal()
      .then(result => {

        if (result == "S") {

          this.BuscarArticulos();


        }

      }, reason => {



      });

  }


  //#endregion

  //#region "Funciones de busqueda"

  BuscarArticulos(): void {


    this.tArticulos = [];

    var tCodclasificacion1: string = '';
    var tCodclasificacion2: string = '';
    var tCodclasificacion3: string = '';
    var tCodclasificacion4: string = '';
    var tCodfabricante: string = '';
    var tCodmarca: string = '';
    var tCodmodelo: string = '';

    tCodclasificacion1 = this.tClasificacion1Actual.Codclasificacion1;
    tCodclasificacion2 = this.tClasificacion2Actual.Codclasificacion2;
    tCodclasificacion3 = this.tClasificacion3Actual.Codclasificacion3;
    tCodfabricante = this.tFabricanteActual.Codfabricante;
    tCodmarca = this.tMarcaActual.Codmarca;

    for (let tClasificacion4 of this.tClasificaciones4Actuales) {

      if (tCodclasificacion4 == "") {

        tCodclasificacion4 = "'" + tClasificacion4.Codclasificacion4 + "'";

      }
      else {

        tCodclasificacion4 += ", '" + tClasificacion4.Codclasificacion4 + "'";

      }

    }

    if (tCodclasificacion4 != "") {

      tCodclasificacion4 = "(" + tCodclasificacion4 + ")";

    }

    for (let tModelo of this.tModeloActuales) {

      if (tCodmodelo == "") {

        tCodmodelo = "'" + tModelo.Codmodelo + "'";

      }
      else {

        tCodmodelo += ", '" + tModelo.Codmodelo + "'";

      }

    }

    if (tCodmodelo != "") {

      tCodmodelo = "(" + tCodmodelo + ")";

    }

    this.Ws
      .Articulo()
      .Consumir_Obtener_ArticulosPlaneador(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        tCodclasificacion1,
        tCodclasificacion2,
        tCodclasificacion3,
        tCodclasificacion4,
        tCodfabricante,
        tCodmarca,
        tCodmodelo)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tArticulos = Respuesta.Datos;
          this.tFiltroActual = this.tFiltros[0];

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Buscar_Clasificaciones1(): void {


    this.Ws.Clasificacion1()
      .Consumir_Obtener_Clasificacion1Full4(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa)
      .subscribe(Respuesta => {


        this.tClasificaciones1 = [];
        this.tClasificaciones2 = [];

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {


          this.tClasificaciones1 = Respuesta.Datos;
          this.tClasificacion1Actual = new Clasificacion1;
          this.tClasificacion2Actual = new Clasificacion2;
          this.tClasificacion3Actual = new Clasificacion3;

        }

      }, error => {


        this.tErroresServices.MostrarError(error);

      });

  }

  Buscar_Fabricantes(): void {

    this.Ws.Fabricante()
      .Consumir_Obtener_FabricanteFull(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa)
      .subscribe(Respuesta => {

        this.tFabricantes = [];
        this.tMarcas = [];
        this.tModelos = [];


        if (Respuesta.Resultado == "N") {


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.tFabricantes = Respuesta.Datos;

        }

      }, error => {


        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  Descargar_Reportes(tArticuloSel: Articulo): void {

    this.Ws
      .Articulo()
      .Consumir_Obtener_ArticuloRpt(tArticuloSel.Corporacion,
        tArticuloSel.Empresa,
        tArticuloSel.Codarticulo)
      .subscribe(Respuesta => {


        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          var Reporte: Reportes = Respuesta.Datos[0];
          this.tManejarPdf.AbrirPdf(Reporte);

        }

      }, error => {


        this.tErroresServices.MostrarError(error);

      });

  }

  Descargar_Excel() {

    let tArticulosTemp: Articulo[] = this.tArticulos;

    tArticulosTemp.map(tArticulo => {

      delete tArticulo.Id;
      delete tArticulo.Corporacion;
      delete tArticulo.Empresa;
      delete tArticulo.Url;
      delete tArticulo.Usuariocreador;
      delete tArticulo.Usuariomodificador;
      delete tArticulo.Fecha_creacion;
      delete tArticulo.Fecha_creacionl;
      delete tArticulo.Fecha_modificacion;
      delete tArticulo.Fecha_modificacionl;
      delete tArticulo.LImagenes;
      delete tArticulo.LInventario;

    });

    this.tManejarArchivos.GenerarExcel(tArticulosTemp, 'Articulos.xlsx', 'Articulos');
    event.preventDefault();

  }

  SeleccionarClasificacion1(tClasificacion1Sel: any): void {

    this.tClasificacion1Actual = new Clasificacion1;
    this.tClasificacion2Actual = new Clasificacion2;
    this.tClasificacion3Actual = new Clasificacion3;
    this.tClasificaciones4Actuales = [];
    this.tClasificaciones2 = [];
    this.tClasificaciones3 = [];
    this.tClasificaciones4 = [];

    for (let tClasificacion of this.tClasificaciones1) {

      if (tClasificacion.Codclasificacion1 == tClasificacion1Sel.Codclasificacion1) {

        this.tClasificacion1Actual = tClasificacion;
        this.tClasificaciones2 = this.tClasificacion1Actual.LClasificacion2;
        break;

      }

    }

  }

  DeseleccionarClasificacion1(): void {

    this.tClasificacion1Actual = new Clasificacion1;
    this.tClasificacion2Actual = new Clasificacion2;
    this.tClasificacion3Actual = new Clasificacion3;
    this.tClasificaciones4Actuales = [];
    this.tClasificaciones2 = [];
    this.tClasificaciones3 = [];
    this.tClasificaciones4 = [];

  }

  SeleccionarClasificacion2(tClasificacion2Sel: any): void {

    this.tClasificacion2Actual = new Clasificacion2;
    this.tClasificacion3Actual = new Clasificacion3;
    this.tClasificaciones4Actuales = [];
    this.tClasificaciones3 = [];
    this.tClasificaciones4 = [];

    for (let tClasificacion of this.tClasificaciones2) {

      if (tClasificacion.Codclasificacion2 == tClasificacion2Sel.Codclasificacion2) {

        this.tClasificacion2Actual = tClasificacion;
        this.tClasificaciones3 = this.tClasificacion2Actual.LClasificacion3;
        break;

      }

    }

  }

  DeseleccionarClasificacion2(): void {

    this.tClasificacion2Actual = new Clasificacion2;
    this.tClasificacion3Actual = new Clasificacion3;
    this.tClasificaciones4Actuales = [];
    this.tClasificaciones2 = [];
    this.tClasificaciones3 = [];
    this.tClasificaciones4 = [];

  }

  SeleccionarClasificacion3(tClasificacion3Sel: any): void {

    this.tClasificacion3Actual = new Clasificacion3;
    this.tClasificaciones4Actuales = [];
    this.tClasificaciones4 = [];

    for (let tClasificacion of this.tClasificaciones3) {

      if (tClasificacion.Codclasificacion3 == tClasificacion3Sel.Codclasificacion3) {

        this.tClasificacion3Actual = tClasificacion;
        this.tClasificaciones4 = this.tClasificacion3Actual.LClasificacion4;
        break;

      }

    }

  }

  DeseleccionarClasificacion3(): void {

    this.tClasificacion3Actual = new Clasificacion3;
    this.tClasificaciones4Actuales = [];
    this.tClasificaciones3 = [];
    this.tClasificaciones4 = [];


  }

  SeleccionarClasificacion4(tClasificacion4Sel: any): void {

    for (let tClasificacion of this.tClasificaciones4) {

      if (tClasificacion.Codclasificacion4 == tClasificacion4Sel.Codclasificacion4) {

        this.tClasificaciones4Actuales.push(tClasificacion);

      }

    }

  }

  SeleccionarTodasClasificacion4(tClasificaciones4Sel: any): void {

    for (let tClasificacionSel of tClasificaciones4Sel) {

      this.SeleccionarClasificacion4(tClasificacionSel);

    }

  }

  DeseleccionarClasificacion4(tClasificacion4Sel: any): void {

    var jv: number = 0;

    for (let tClasificacion of this.tClasificaciones4Actuales) {

      if (tClasificacion.Codclasificacion4 == tClasificacion4Sel.Codclasificacion4) {

        this.tClasificaciones4Actuales.splice(jv, 1);

      }

      jv = +jv + +1;

    }

  }

  SeleccionarFabricantes(tFabricanteSel: any): void {

    this.tFabricanteActual = new Fabricante;
    this.tMarcas = [];
    this.tMarcaActual = new Marca;
    this.tModelos = [];
    this.tModeloActuales = [];

    for (let tFabricante of this.tFabricantes) {

      if (tFabricante.Codfabricante == tFabricanteSel.Codfabricante) {

        this.tFabricanteActual = tFabricante;
        this.tMarcas = this.tFabricanteActual.LMarcas;
        break;

      }

    }

  }

  DeseleccionarFabricantes(): void {

    this.tFabricanteActual = new Fabricante;
    this.tMarcas = [];
    this.tMarcaActual = new Marca;
    this.tModelos = [];
    this.tModeloActuales = [];
  }

  SeleccionarMarcas(tMarcaSel: any): void {

    this.tMarcaActual = new Marca;
    this.tModelos = [];
    this.tModeloActuales = [];

    for (let tMarca of this.tMarcas) {

      if (tMarca.Codmarca == tMarcaSel.Codmarca) {

        this.tMarcaActual = tMarca;
        this.tModelos = this.tMarcaActual.LModelos;
        break;

      }

    }

  }

  DeseleccionarMarca(): void {

    this.tMarcaActual = new Marca;
    this.tModelos = [];
    this.tModeloActuales = [];

  }

  SeleccionarModelo(tModeloSel: any): void {

    for (let tModelo of this.tModelos) {

      if (tModelo.Codmodelo == tModeloSel.Codmodelo) {

        this.tModeloActuales.push(tModelo);

      }

    }

  }

  DeseleccionarModelo(tModeloSel: any): void {

    var jv: number = 0;

    for (let tModelo of this.tModeloActuales) {

      if (tModelo.Codmodelo == tModeloSel.Codmodelo) {

        this.tModeloActuales.splice(jv, 1);

      }

      jv = +jv + +1;

    }

  }

  Limpiar(): void {

    this.tFabricanteActual = new Fabricante;
    this.tMarcas = [];
    this.tMarcaActual = new Marca;
    this.tModelos = [];
    this.tModeloActuales = [];

    this.tClasificacion1Actual = new Clasificacion1;
    this.tClasificacion2Actual = new Clasificacion2;
    this.tClasificacion3Actual = new Clasificacion3;
    this.tClasificaciones4Actuales = [];
    this.tClasificaciones2 = [];
    this.tClasificaciones3 = [];
    this.BuscarArticulos();

  }

}

