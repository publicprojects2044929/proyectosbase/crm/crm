import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwPlaneadorArticulosComponent } from './fw-planeador-articulos.component';

describe('FwPlaneadorArticulosComponent', () => {
  let component: FwPlaneadorArticulosComponent;
  let fixture: ComponentFixture<FwPlaneadorArticulosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwPlaneadorArticulosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwPlaneadorArticulosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
