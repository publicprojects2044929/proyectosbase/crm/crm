import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FwPlantillaComponent  } from 'src/app/Shared/Formularios/fw-plantilla/fw-plantilla.component'
import { FwPlaneadorArticulosComponent  } from './Formularios/fw-planeador-articulos/fw-planeador-articulos.component';

const routes: Routes = [
  {
    path: '',
    component: FwPlantillaComponent,
    children:[
      {
        path: '',
        component: FwPlaneadorArticulosComponent,
        data:{
          Codformulario: 'fPlaneadorArticulo'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlaneadorArticulosRoutingModule { }
