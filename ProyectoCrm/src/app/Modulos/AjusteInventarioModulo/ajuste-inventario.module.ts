import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from 'src/app/Shared/shared.module';

import { AjusteInventarioRoutingModule } from './ajuste-inventario-routing.module';
import { FwAjusteDeInventarioPage } from './Paginas/fw-ajuste-de-inventario/fw-ajuste-de-inventario.page';
import { FwAjusteDeInventarioTotalesComponent } from './Paginas/fw-ajuste-de-inventario/Componentes/fw-ajuste-de-inventario-totales/fw-ajuste-de-inventario-totales.component';
import { FwAjusteDeInventarioArticuloComponent } from './Paginas/fw-ajuste-de-inventario/Componentes/fw-ajuste-de-inventario-articulo/fw-ajuste-de-inventario-articulo.component';
import { FwAjusteDeInventarioHistorialComponent } from './Paginas/fw-ajuste-de-inventario/Componentes/fw-ajuste-de-inventario-historial/fw-ajuste-de-inventario-historial.component';


@NgModule({
  declarations: [FwAjusteDeInventarioPage, FwAjusteDeInventarioTotalesComponent, FwAjusteDeInventarioArticuloComponent, FwAjusteDeInventarioHistorialComponent],
  imports: [
    CommonModule,
    AjusteInventarioRoutingModule,
    SharedModule,
    FormsModule,
    NgbModule
  ]
})
export class AjusteInventarioModule { }
