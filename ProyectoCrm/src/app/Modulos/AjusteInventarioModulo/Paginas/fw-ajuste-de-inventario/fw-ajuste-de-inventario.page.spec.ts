import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwAjusteDeInventarioPage } from './fw-ajuste-de-inventario.page'

describe('FwAjusteDeInventarioPage', () => {
  let component: FwAjusteDeInventarioPage;
  let fixture: ComponentFixture<FwAjusteDeInventarioPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwAjusteDeInventarioPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwAjusteDeInventarioPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
