import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormularioConfiguracion } from 'src/app/Clases/EstructuraConfiguracion';
import { Usuario, Empresas, TipoAjustesInventario, Articulo, ArticuloInventario, AjusteInventario, Filtros, Almacen } from 'src/app/Clases/Estructura';
import { Router } from '@angular/router';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { ToastrService } from 'ngx-toastr';
import { ErroresService } from 'src/app/Modales/mensajes/fw-modal-msj/errores.service';
import { MensajesService } from 'src/app/Modales/mensajes/fw-modal-msj/mensajes.service';
import { ConfirmacionService } from 'src/app/Modales/confirmacion/fw-modal-confirmacion/confirmacion.service';
import { ArticulosService } from 'src/app/Modales/articulo/fw-modal-articulos/articulos.service';
import { SessionService } from 'src/app/Core/Session/session.service';
import { environment } from 'src/environments/environment';
import { AjusteInventarioService } from 'src/app/Modales/AjusteInventario/fw-modal-ajuste-inventario/ajuste-inventario.service';
import { AjusteInventarioPorArchivoService } from 'src/app/Modales/AjusteInventarioPorArchivo/fw-modal-ajuste-inventario-por-archivo/ajuste-inventario-por-archivo.service';
import { Subscription } from 'rxjs';
import { ConfiguracionService } from 'src/app/Core/Configuracion/configuracion.service';

@Component({
  selector: 'app-fw-ajuste-de-inventario',
  templateUrl: './fw-ajuste-de-inventario.page.html',
  styleUrls: ['./fw-ajuste-de-inventario.page.scss']
})
export class FwAjusteDeInventarioPage implements OnInit, OnDestroy {

  @ViewChild('Historial', { static: true }) tHistorial : any;

  tConfiguracionFormulario: FormularioConfiguracion = new FormularioConfiguracion();
  tCodFormularioActual: string = "fAjusteDeInventario";
  tPermisoAgregar: boolean = false;
  tPermisoEditar: boolean = false;
  tPermisoEliminar: boolean = false;
  tHabilitarGuardar: boolean = false;

  tUsuarioActual: Usuario = new Usuario();
  tUsuario$: Subscription;
  tEmpresaActual: Empresas = new Empresas();
  tEmpresa$: Subscription;

  tTitulo: string = "";

  tAlmacenes: Almacen[] = [];
  tAlmacenActual: Almacen = new Almacen();

  tTiposAjuste: TipoAjustesInventario[] = [];

  tArticuloActual: Articulo = new Articulo();
  tArticuloInv: ArticuloInventario[] = [];
  tArticuloAjusteInv: AjusteInventario[] = [];

  tConfiguracion$: Subscription;
  tHayConfiguracionWs$: Subscription;
  tStrConfiguracion: string = 'AjustesDeInventario/AjustesDeInventario.Config.json';

  constructor(public router: Router,
    public Ws: ClsServiciosService,
    private tSesion: SessionService,
    private tConfiguracionService: ConfiguracionService,
    private toastr: ToastrService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService,
    private tConfServices: ConfirmacionService,
    private tArticuloServices: ArticulosService,
    private tAjusteInvServices: AjusteInventarioService,
    private tAjusteInvArchServices: AjusteInventarioPorArchivoService) {

  }

  ngOnInit() {

    this.tConfiguracion$ = this.tConfiguracionService
      .Consumir_Obtener_Configuracion(this.tStrConfiguracion)
      .subscribe((tConfiguracion: FormularioConfiguracion) => {

        this.tConfiguracionFormulario = tConfiguracion;
        this.tTitulo = this.tConfiguracionFormulario.Titulo;
        this.tErroresServices.tTituloMsj = this.tConfiguracionFormulario.ModalMensaje.Titulo;
        this.tMsjServices.tTituloMsj = this.tConfiguracionFormulario.ModalMensaje.Titulo;
        this.tConfServices.tTituloConf = this.tConfiguracionFormulario.ModalConfirmacion.Titulo;

      });

    this.tEmpresa$ = this.tSesion.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    })

    this.tUsuario$ = this.tSesion.tUsuarioActual$.subscribe((tUsuario: Usuario) => {

      this.tUsuarioActual = tUsuario;

    })

    var tTienePermiso: boolean = this.tSesion.ObtenerPermisoAcceso(this.tCodFormularioActual);
    this.tPermisoAgregar = this.tSesion.ObtenerPermisoAgregar();
    this.tPermisoEditar = this.tSesion.ObtenerPermisoEditar();
    this.tPermisoEliminar = this.tSesion.ObtenerPermisoEliminar();

    if (tTienePermiso == true) {

      this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
        .subscribe((tHayConfig: boolean) => {

          if (tHayConfig === true) {

            this.Buscar_TiposDeAjustes();
            this.Buscar_Almacenes();
            this.NuevoAjuste();

          }

        });

    }
    else {

      this.tMsjServices.AbrirModalMsj(environment.msjSinAcceso);
      this.router.navigate(['/Home']);

    }


  }

  ngOnDestroy() {

    this.tEmpresa$.unsubscribe();
    this.tUsuario$.unsubscribe();
    this.tConfiguracion$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();

  }

  //#region "Funciones Modal"

  AbrirModalArticulos(): void {

    this.tArticuloServices
      .AbrirModal()
      .then(result => {

        if (result.Resultado == "S") {

          let tArticuloModal: Articulo = result.Articulo || new Articulo();
          this.tArticuloActual = tArticuloModal;
          this.Buscar_ArticuloInventario();
          this.Buscar_ArticuloAjustesInventario();

        }

      }, reason => {



      });

  }

  //#endregion

  //#region "Funciones de busqueda"

  Buscar_Almacenes(): void {

    this.Ws
      .Almacen()
      .Consumir_Obtener_Almacen(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa)
      .subscribe(Respuesta => {

        this.tAlmacenes = [];

        if (Respuesta.Resultado == "N") {

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tAlmacenes = Respuesta.Datos;
          this.tAlmacenActual = this.tAlmacenes[0] || new Almacen();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Buscar_TiposDeAjustes(): void {

    this.Ws
      .TipoAjuste()
      .Consumir_Obtener_TipoAjuste(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa)
      .subscribe(Respuesta => {

        this.tTiposAjuste = [];

        if (Respuesta.Resultado == "N") {

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tTiposAjuste = Respuesta.Datos;

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Buscar_ArticuloInventario(): void {

    this.Ws
      .ArticuloInventario()
      .Consumir_Obtener_ArticulosInventario2(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        this.tArticuloActual.Codarticulo)
      .subscribe(Respuesta => {

        this.tArticuloInv = [];

        if (Respuesta.Resultado == "N") {

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tArticuloInv = Respuesta.Datos;

        }


      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Buscar_ArticuloAjustesInventario(): void {

    this.Ws
      .AjusteInventario()
      .Consumir_Obtener_AjusteInventarioPorArticulo(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        this.tArticuloActual.Codarticulo)
      .subscribe(Respuesta => {

        this.tArticuloAjusteInv = [];

        if (Respuesta.Resultado == "N") {

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tArticuloAjusteInv = Respuesta.Datos;

        }


      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  BuscarArticulo(tCodarticulo: string): void {

    this.Ws
      .Articulo()
      .Consumir_Obtener_ArticuloInventario(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        tCodarticulo)
      .subscribe(Respuesta => {

        this.tArticuloActual = new Articulo();

        if (Respuesta.Resultado == "N") {

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tArticuloActual = Respuesta.Datos[0] || new Articulo();
          this.Buscar_ArticuloInventario();
          this.Buscar_ArticuloAjustesInventario();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  Cancelar() {

    this.tArticuloActual = new Articulo();
    this.tArticuloInv = [];
    this.tArticuloAjusteInv = [];
    this.tHistorial.LimpiarBusqueda();

  }

  NuevoAjuste(): void {

    if (this.tArticuloActual.Codarticulo == "") {

      this.toastr.clear();
      this.toastr.info("Debe seleccionar un articulo para realizar el ajuste.", "Ajuste de inventario");
      return;

    }

    this.tAjusteInvServices
      .AbrirModal(this.tArticuloActual, this.tTiposAjuste, this.tAlmacenes)
      .then(Resultado => {

        this.BuscarArticulo(this.tArticuloActual.Codarticulo);

      });

  }

  NuevoAjustePorArchivo(): void {

    this.tAjusteInvArchServices
      .AbrirModal(this.tTiposAjuste, this.tAlmacenes)
      .then(Resultado => {

      });

  }

}
