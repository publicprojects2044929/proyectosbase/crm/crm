import { Component, OnInit, Input } from '@angular/core';
import { Filtros, AjusteInventario, TipoAjustesInventario } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-ajuste-de-inventario-historial',
  templateUrl: './fw-ajuste-de-inventario-historial.component.html',
  styleUrls: ['./fw-ajuste-de-inventario-historial.component.scss']
})
export class FwAjusteDeInventarioHistorialComponent implements OnInit {

  @Input('ArticuloAjustes') tArticulosAjustes: AjusteInventario[] = [];
  @Input('TiposDeAjustes') tTiposAjuste: TipoAjustesInventario[] = [];

  tFiltros: Filtros[] = [];
  tFiltroActual: Filtros = new Filtros();
  tFiltroBusqueda: string = "";

  tPosicionAjuste: number = 0;
  tPaginaAjuste: number = 1;
  tTamanoPagAjuste: number = 10;

  constructor() {

    this.tFiltroBusqueda = "";

    this.tFiltros = [

      { Codfiltro: "Codajuste", Filtro: "Ajuste" },
      { Codfiltro: "Codtipoajusteinv", Filtro: "Tipo" },
      { Codfiltro: "Talla", Filtro: "Talla" }

    ]

    this.tFiltroActual = this.tFiltros[0] || new Filtros();

   }

  ngOnInit(): void {
  }

  ObtenerTipoAjuste(tAjusteInv: AjusteInventario): string {

    let tTipoAjusteInv: string = "";

    tTipoAjusteInv = "[ " + tAjusteInv.Codtipoajusteinv + " ] - " + tAjusteInv.Descripcion

    return tTipoAjusteInv;

  }

  LimpiarBusqueda(){

    this.tFiltroBusqueda = "";
    this.tFiltroActual = this.tFiltros[0] || new Filtros();

  }

}
