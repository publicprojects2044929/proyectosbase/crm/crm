import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwAjusteDeInventarioHistorialComponent } from './fw-ajuste-de-inventario-historial.component';

describe('FwAjusteDeInventarioHistorialComponent', () => {
  let component: FwAjusteDeInventarioHistorialComponent;
  let fixture: ComponentFixture<FwAjusteDeInventarioHistorialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwAjusteDeInventarioHistorialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwAjusteDeInventarioHistorialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
