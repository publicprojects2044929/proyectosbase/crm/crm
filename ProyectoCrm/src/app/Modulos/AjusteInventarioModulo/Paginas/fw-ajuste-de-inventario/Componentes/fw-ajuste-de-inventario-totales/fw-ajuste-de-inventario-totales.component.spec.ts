import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwAjusteDeInventarioTotalesComponent } from './fw-ajuste-de-inventario-totales.component';

describe('FwAjusteDeInventarioTotalesComponent', () => {
  let component: FwAjusteDeInventarioTotalesComponent;
  let fixture: ComponentFixture<FwAjusteDeInventarioTotalesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwAjusteDeInventarioTotalesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwAjusteDeInventarioTotalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
