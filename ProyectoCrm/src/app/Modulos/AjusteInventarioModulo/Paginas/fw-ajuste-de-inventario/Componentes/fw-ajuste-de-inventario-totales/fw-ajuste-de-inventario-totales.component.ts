import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-fw-ajuste-de-inventario-totales',
  templateUrl: './fw-ajuste-de-inventario-totales.component.html',
  styleUrls: ['./fw-ajuste-de-inventario-totales.component.scss']
})
export class FwAjusteDeInventarioTotalesComponent implements OnInit {

  @Input('ExistenciaGeneral') tExistencia: number = 0;
  @Input('ReservadoGeneral') tReservado: number = 0;
  @Input('DisponibleGeneral') tDisponible: number = 0;

  constructor() { }

  ngOnInit(): void {
  }

}
