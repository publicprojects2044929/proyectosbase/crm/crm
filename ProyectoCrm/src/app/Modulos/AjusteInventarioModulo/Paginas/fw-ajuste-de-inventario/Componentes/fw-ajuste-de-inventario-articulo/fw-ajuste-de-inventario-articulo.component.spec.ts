import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwAjusteDeInventarioArticuloComponent } from './fw-ajuste-de-inventario-articulo.component';

describe('FwAjusteDeInventarioArticuloComponent', () => {
  let component: FwAjusteDeInventarioArticuloComponent;
  let fixture: ComponentFixture<FwAjusteDeInventarioArticuloComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwAjusteDeInventarioArticuloComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwAjusteDeInventarioArticuloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
