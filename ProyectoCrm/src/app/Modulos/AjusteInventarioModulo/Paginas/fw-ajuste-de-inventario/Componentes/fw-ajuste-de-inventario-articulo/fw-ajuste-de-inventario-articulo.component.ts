import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Articulo, ArticuloInventario } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-ajuste-de-inventario-articulo',
  templateUrl: './fw-ajuste-de-inventario-articulo.component.html',
  styleUrls: ['./fw-ajuste-de-inventario-articulo.component.scss']
})
export class FwAjusteDeInventarioArticuloComponent implements OnInit {

  @Input('Articulo') tArticulo: Articulo = new Articulo();
  @Input('Inventario') tInventario: ArticuloInventario[] = [];

  @Output('AbrirModal') tAbrir: EventEmitter<any> = new EventEmitter();
  @Output('NuevoAjuste') tNuevo: EventEmitter<any> = new EventEmitter();
  @Output('NuevoAjusteArchivo') tNuevoArchivo: EventEmitter<any> = new EventEmitter();

  tPosicionInv: number = 0;
  tPaginaInv: number = 1;
  tTamanoPagInv: number = 5;

  constructor() { }

  ngOnInit(): void {
  }

  AbrirModal() {
    this.tAbrir.emit('');
  }

  NuevoAjuste() {
    this.tNuevo.emit('');
  }

  NuevoAjusteArchivo() {
    this.tNuevoArchivo.emit('');
  }

  ManejaTallas(): string {

    let tManejaTalla: string = "No";

    if (this.tArticulo.Maneja_tallas === "S") {

      tManejaTalla = "Sí";

    }

    return tManejaTalla;

  }

  ManejaTallasInt(): string {

    let tManejaTallaInt: string = "No";

    if (this.tArticulo.Talla_intermedias === "S") {

      tManejaTallaInt = "Sí";

    }

    return tManejaTallaInt;

  }

  ObtenerTalla(tTallaFor: ArticuloInventario): string {

    let tTalla: string = "NA";

    if (tTallaFor.Talla != "") {

      tTalla = tTallaFor.Talla;

    }

    return tTalla;

  }

}
