import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FwPlantillaComponent  } from 'src/app/Shared/Formularios/fw-plantilla/fw-plantilla.component'
import { FwAjusteDeInventarioPage } from './Paginas/fw-ajuste-de-inventario/fw-ajuste-de-inventario.page';

const routes: Routes = [
  {
    path: '',
    component: FwPlantillaComponent,
    children:[
      {
        path: '',
        component: FwAjusteDeInventarioPage,
        data: {
          Codformulario: 'fAjusteDeInventario'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AjusteInventarioRoutingModule { }
