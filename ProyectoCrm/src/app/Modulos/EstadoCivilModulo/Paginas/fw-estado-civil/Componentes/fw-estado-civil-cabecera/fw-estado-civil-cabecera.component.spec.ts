import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwEstadoCivilCabeceraComponent } from './fw-estado-civil-cabecera.component';

describe('FwEstadoCivilCabeceraComponent', () => {
  let component: FwEstadoCivilCabeceraComponent;
  let fixture: ComponentFixture<FwEstadoCivilCabeceraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwEstadoCivilCabeceraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwEstadoCivilCabeceraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
