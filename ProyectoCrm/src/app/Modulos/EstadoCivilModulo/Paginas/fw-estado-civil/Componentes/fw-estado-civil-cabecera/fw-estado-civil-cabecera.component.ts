import { Component, OnInit, Input } from '@angular/core';
import { EstadoCivil } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-estado-civil-cabecera',
  templateUrl: './fw-estado-civil-cabecera.component.html',
  styleUrls: ['./fw-estado-civil-cabecera.component.scss']
})
export class FwEstadoCivilCabeceraComponent implements OnInit {

  @Input('EstadoCivil') tEstadoCivil: EstadoCivil = new EstadoCivil();

  constructor() { }

  ngOnInit(): void {
  }

}
