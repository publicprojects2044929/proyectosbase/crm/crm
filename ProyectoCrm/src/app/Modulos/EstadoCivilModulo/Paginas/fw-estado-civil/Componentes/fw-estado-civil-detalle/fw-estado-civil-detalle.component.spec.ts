import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwEstadoCivilDetalleComponent } from './fw-estado-civil-detalle.component';

describe('FwEstadoCivilDetalleComponent', () => {
  let component: FwEstadoCivilDetalleComponent;
  let fixture: ComponentFixture<FwEstadoCivilDetalleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwEstadoCivilDetalleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwEstadoCivilDetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
