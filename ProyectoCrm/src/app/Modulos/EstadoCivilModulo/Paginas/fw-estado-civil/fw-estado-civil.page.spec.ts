import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwEstadoCivilPage } from './fw-estado-civil.page';

describe('FwEstadoCivilPage', () => {
  let component: FwEstadoCivilPage;
  let fixture: ComponentFixture<FwEstadoCivilPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwEstadoCivilPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwEstadoCivilPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
