import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FwPlantillaComponent  } from 'src/app/Shared/Formularios/fw-plantilla/fw-plantilla.component'
import { FwEstadoCivilPage } from './Paginas/fw-estado-civil/fw-estado-civil.page';

const routes: Routes = [
  {
    path: '',
    component: FwPlantillaComponent,
    children:[
      {
        path: '',
        component: FwEstadoCivilPage,
        data:{
          Codformulario: 'fEstadoCivil'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EstadoCivilRoutingModule { }
