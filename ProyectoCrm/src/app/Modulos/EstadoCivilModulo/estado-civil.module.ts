import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from 'src/app/Shared/shared.module';

import { EstadoCivilRoutingModule } from './estado-civil-routing.module';
import { FwEstadoCivilPage } from './Paginas/fw-estado-civil/fw-estado-civil.page';
import { FwEstadoCivilCabeceraComponent } from './Paginas/fw-estado-civil/Componentes/fw-estado-civil-cabecera/fw-estado-civil-cabecera.component';
import { FwEstadoCivilDetalleComponent } from './Paginas/fw-estado-civil/Componentes/fw-estado-civil-detalle/fw-estado-civil-detalle.component';

@NgModule({
  declarations: [FwEstadoCivilPage, FwEstadoCivilCabeceraComponent, FwEstadoCivilDetalleComponent],
  imports: [
    CommonModule,
    EstadoCivilRoutingModule,
    SharedModule,
    FormsModule,
    NgbModule
  ]
})
export class EstadoCivilModule { }
