import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwConsultaArticuloPage } from './fw-consulta-articulo.page';

describe('FwConsultaArticuloPage', () => {
  let component: FwConsultaArticuloPage;
  let fixture: ComponentFixture<FwConsultaArticuloPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwConsultaArticuloPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwConsultaArticuloPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
