import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormularioConfiguracion } from 'src/app/Clases/EstructuraConfiguracion';
import { Usuario, Empresas, Articulo, Configuracion, Filtros } from 'src/app/Clases/Estructura';
import { Router } from '@angular/router';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { SessionService } from 'src/app/Core/Session/session.service';
import { ErroresService } from 'src/app/Modales/mensajes/fw-modal-msj/errores.service';
import { MensajesService } from 'src/app/Modales/mensajes/fw-modal-msj/mensajes.service';
import { ConfirmacionService } from 'src/app/Modales/confirmacion/fw-modal-confirmacion/confirmacion.service';
import { environment } from 'src/environments/environment';
import { Subscription } from 'rxjs';
import { ConfiguracionService } from 'src/app/Core/Configuracion/configuracion.service';

@Component({
  selector: 'app-fw-consulta-articulo',
  templateUrl: './fw-consulta-articulo.page.html',
  styleUrls: ['./fw-consulta-articulo.page.scss']
})
export class FwConsultaArticuloPage implements OnInit, OnDestroy {

  tConfiguracionFormulario: FormularioConfiguracion = new FormularioConfiguracion();
  tCodFormularioActual: string = "fConsultaArticulo";

  tUsuarioActual: Usuario = new Usuario();
  tEmpresaActual: Empresas = new Empresas();
  tEmpresa$: Subscription;
  tUsuario$: Subscription;

  tTitulo: string = "";
  tPosicion: number = 0;
  tPagina: number = 1;
  tTamanoPag: number = 10;

  tArticulos: Articulo[] = [];
  tArticuloActual: Articulo = new Articulo();

  tConfiguracion: Configuracion = new Configuracion();

  tConfiguracion$: Subscription;
  tHayConfiguracionWs$: Subscription;
  tStrConfiguracion: string = 'ConsultaArticulo/ConsultaArticulo.Config.json';

  tFiltroActual: Filtros[] = [];
  tFiltroBusqueda: string = "";

  constructor(
    public router: Router,
    public Ws: ClsServiciosService,
    private tSesion: SessionService,
    private tConfiguracionService: ConfiguracionService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService,
    private tConfServices: ConfirmacionService
  ) {

    this.tPosicion = 0;

    this.tFiltroActual = [

      { Codfiltro: "Codarticulo", Filtro: "Articulo" },
      { Codfiltro: "Descripcion", Filtro: "Descripcion" }

    ]

  }

  ngOnInit() {

    this.tConfiguracion$ = this.tConfiguracionService
      .Consumir_Obtener_Configuracion(this.tStrConfiguracion)
      .subscribe((tConfiguracion: FormularioConfiguracion) => {

        this.tConfiguracionFormulario = tConfiguracion;
        this.tTitulo = this.tConfiguracionFormulario.Titulo;
        this.tConfServices.tTituloConf = this.tConfiguracionFormulario.ModalConfirmacion.Titulo;
        this.tErroresServices.tTituloMsj = this.tConfiguracionFormulario.ModalMensaje.Titulo;
        this.tMsjServices.tTituloMsj = this.tConfiguracionFormulario.ModalMensaje.Titulo;

      });

    this.tEmpresa$ = this.tSesion.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    })

    this.tUsuario$ = this.tSesion.tUsuarioActual$.subscribe((tUsuario: Usuario) => {

      this.tUsuarioActual = tUsuario;

    })

    var tTienePermiso: boolean = this.tSesion.ObtenerPermisoAcceso(this.tCodFormularioActual);

    if (tTienePermiso == true) {

      this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
        .subscribe((tHayConfig: boolean) => {

          if (tHayConfig === true) {

            this.BuscarArticulos();

          }

        });

    }
    else {

      this.tMsjServices.AbrirModalMsj(environment.msjSinAcceso);
      this.router.navigate(['/Home']);

    }

  }

  ngOnDestroy() {

    this.tEmpresa$.unsubscribe();
    this.tUsuario$.unsubscribe();
    this.tConfiguracion$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();

  }

  //#region "Funciones de busqueda"

  BuscarArticulos(): void {

    this.Ws.Articulo()
      .Consumir_Obtener_Articulos(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado === "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado === "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tArticulos = Respuesta.Datos;
          // this.tManejarImagen.tImagen = this.tArticuloActual.Url;

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  BuscarArticulo(tCodarticulo: string): void {

    this.Ws.Articulo()
      .Consumir_Obtener_ArticuloDetallado(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        tCodarticulo)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado === "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado === "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tArticuloActual = Respuesta.Datos[0];
          // this.tManejarImagen.tImagen = this.tArticuloActual.Url;

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  SeleccionarArticulo(tArticuloSel: Articulo) {

    this.BuscarArticulo(tArticuloSel.Codarticulo);

  }

}
