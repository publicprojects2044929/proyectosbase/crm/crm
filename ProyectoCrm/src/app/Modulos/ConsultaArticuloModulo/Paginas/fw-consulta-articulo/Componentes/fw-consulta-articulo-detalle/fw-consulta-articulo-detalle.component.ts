import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { Articulo, Empresas, ArticuloInventario } from 'src/app/Clases/Estructura';
import { ManejarImagenes } from 'src/app/Manejadores/cls-procedimientos';

@Component({
  selector: 'app-fw-consulta-articulo-detalle',
  templateUrl: './fw-consulta-articulo-detalle.component.html',
  styleUrls: ['./fw-consulta-articulo-detalle.component.scss']
})
export class FwConsultaArticuloDetalleComponent implements OnInit, OnChanges {

  @Input('Articulo') tArticulo: Articulo = new Articulo();
  @Input('Empresa') tEmpresa: Empresas = new Empresas();

  tManejarImagen: ManejarImagenes = new ManejarImagenes();

  constructor() { }

  ngOnInit(): void {

  }

  ngOnChanges() {

    this.tManejarImagen.tImagen = this.tArticulo.Url;

  }

  ObtenerImagenPrincipal(): string {

    let tUrlPrincipal: string = "";

    tUrlPrincipal = this.tManejarImagen.ObtenerImagen();

    return tUrlPrincipal;

  }

  ObtenerTalla(tTallaFor: ArticuloInventario): string {

    let tTalla: string = "NA";

    if (tTallaFor.Talla != "") {

      tTalla = tTallaFor.Talla;

    }

    return tTalla;

  }

}
