import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Articulo } from 'src/app/Clases/Estructura';
import { ManejarImagenes } from 'src/app/Manejadores/cls-procedimientos';

@Component({
  selector: 'app-fw-consulta-articulo-listado',
  templateUrl: './fw-consulta-articulo-listado.component.html',
  styleUrls: ['./fw-consulta-articulo-listado.component.scss']
})
export class FwConsultaArticuloListadoComponent implements OnInit {

  @Input('Articulo') tArticulo: Articulo = new Articulo();
  @Input('Actual') tActual: boolean = false;
  @Output('seleccionarArticulo') tArticuloSel: EventEmitter<Articulo> = new EventEmitter();

  tManejarImagen: ManejarImagenes = new ManejarImagenes();

  constructor() { }

  ngOnInit(): void {

    this.tManejarImagen.tImagen = this.tArticulo.Url;

  }

  SeleccionarArticulo(){
    this.tArticuloSel.emit(this.tArticulo);
  }

  ObtenerImagenPrincipal(): string{

    let tUrlPrincipal: string = "";

    tUrlPrincipal = this.tManejarImagen.ObtenerImagen();

    return tUrlPrincipal;

  }

}
