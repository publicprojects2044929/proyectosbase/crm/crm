import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwConsultaArticuloDetalleComponent } from './fw-consulta-articulo-detalle.component';

describe('FwConsultaArticuloDetalleComponent', () => {
  let component: FwConsultaArticuloDetalleComponent;
  let fixture: ComponentFixture<FwConsultaArticuloDetalleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwConsultaArticuloDetalleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwConsultaArticuloDetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
