import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwConsultaArticuloListadoComponent } from './fw-consulta-articulo-listado.component';

describe('FwConsultaArticuloListadoComponent', () => {
  let component: FwConsultaArticuloListadoComponent;
  let fixture: ComponentFixture<FwConsultaArticuloListadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwConsultaArticuloListadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwConsultaArticuloListadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
