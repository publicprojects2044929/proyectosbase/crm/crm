import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from 'src/app/Shared/shared.module';

import { ConsultaArticuloRoutingModule } from './consulta-articulo-routing.module';
import { FwConsultaArticuloPage } from './Paginas/fw-consulta-articulo/fw-consulta-articulo.page';
import { FwConsultaArticuloListadoComponent } from './Paginas/fw-consulta-articulo/Componentes/fw-consulta-articulo-listado/fw-consulta-articulo-listado.component';
import { FwConsultaArticuloDetalleComponent } from './Paginas/fw-consulta-articulo/Componentes/fw-consulta-articulo-detalle/fw-consulta-articulo-detalle.component';

@NgModule({
  declarations: [FwConsultaArticuloPage, FwConsultaArticuloListadoComponent, FwConsultaArticuloDetalleComponent],
  imports: [
    CommonModule,
    ConsultaArticuloRoutingModule,
    SharedModule,
    FormsModule,
    NgbModule
  ]
})
export class ConsultaArticuloModule { }
