import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from 'src/app/Shared/shared.module';

import { Clasificacion2RoutingModule } from './clasificacion2-routing.module';
import { FwClasificacion2Page } from './Paginas/fw-clasificacion2/fw-clasificacion2.page';
import { FwClasificacion2CabeceraComponent } from './Paginas/fw-clasificacion2/Componentes/fw-clasificacion2-cabecera/fw-clasificacion2-cabecera.component';
import { FwClasificacion2DetallesComponent } from './Paginas/fw-clasificacion2/Componentes/fw-clasificacion2-detalles/fw-clasificacion2-detalles.component';

@NgModule({
  declarations: [ FwClasificacion2Page, FwClasificacion2CabeceraComponent, FwClasificacion2DetallesComponent ],
  imports: [
    CommonModule,
    Clasificacion2RoutingModule,
    SharedModule,
    FormsModule,
    NgbModule
  ]
})
export class Clasificacion2Module { }
