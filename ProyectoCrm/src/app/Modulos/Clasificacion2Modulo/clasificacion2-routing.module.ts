import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FwPlantillaComponent  } from 'src/app/Shared/Formularios/fw-plantilla/fw-plantilla.component'
import { FwClasificacion2Page  } from './Paginas/fw-clasificacion2/fw-clasificacion2.page';

const routes: Routes = [
  {
    path: '',
    component: FwPlantillaComponent,
    children:[
      {
        path: '',
        component: FwClasificacion2Page,
        data:{
          Codformulario: 'fClasificacion2'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Clasificacion2RoutingModule { }
