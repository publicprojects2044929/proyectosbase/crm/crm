import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Usuario, Empresas, Formulario, Filtros, Clasificacion2, Clasificacion1, Configuracion } from 'src/app/Clases/Estructura';
import { ManejarImagenes } from 'src/app/Manejadores/cls-procedimientos';
import { FormularioConfiguracion } from 'src/app/Clases/EstructuraConfiguracion';
import { SessionService } from 'src/app/Core/Session/session.service';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { ToastrService } from 'ngx-toastr';
import { ErroresService } from 'src/app/Modales/mensajes/fw-modal-msj/errores.service';
import { MensajesService } from 'src/app/Modales/mensajes/fw-modal-msj/mensajes.service';
import { ConfirmacionService } from 'src/app/Modales/confirmacion/fw-modal-confirmacion/confirmacion.service';
import { Clasificacion2Entrada } from 'src/app/Clases/EstructuraEntrada';
import { environment } from 'src/environments/environment';
import { Subscription } from 'rxjs';
import { ConfiguracionService } from 'src/app/Core/Configuracion/configuracion.service';

@Component({
  selector: 'app-fw-clasificacion2',
  templateUrl: './fw-clasificacion2.page.html',
  styleUrls: ['./fw-clasificacion2.page.scss']
})
export class FwClasificacion2Page implements OnInit, OnDestroy {

  @ViewChild('ClasificacionDetalles', {static: true}) tClasificacionDetalles: any;
  tCodFormularioActual: string = "fClasificacion2";
  tPermisoAgregar: boolean = false;
  tPermisoEditar: boolean = false;
  tPermisoEliminar: boolean = false;
  tHabilitarGuardar: boolean = false;

  tUsuarioActual: Usuario = new Usuario();
  tEmpresaActual: Empresas = new Empresas();
  tEmpresa$: Subscription;
  tUsuario$: Subscription;

  tTitulo: string = "";
  tNuevo: boolean = true;
  tPosicion: number = 0;
  tPagina: number = 1;
  tTamanoPag: number = 5;

  tFormularios: Formulario[] = [];

  tFiltros: Filtros[] = [];
  tFiltroActual: Filtros = new Filtros();
  tFiltroBusqueda: string = "";

  tClasificaciones2: Clasificacion2[] = [];
  tClasificacionActual: Clasificacion2 = new Clasificacion2();

  tClasificaciones1: Clasificacion1[] = [];

  tConfiguracion: Configuracion = new Configuracion();
  tManejarImagen: ManejarImagenes = new ManejarImagenes();
  tConfiguracionFormulario: FormularioConfiguracion = new FormularioConfiguracion();

  tConfiguracion$: Subscription;
  tHayConfiguracionWs$: Subscription;
  tStrConfiguracion: string = 'Clasificacion2/Clasificacion2.Config.json';

  constructor(public router: Router,
    private tSesion: SessionService,
    private tConfiguracionService: ConfiguracionService,
    public Ws: ClsServiciosService,
    private toastr: ToastrService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService,
    private tConfServices: ConfirmacionService) {

    this.tFiltroBusqueda = "";
    this.tPosicion = 0;

    this.tFiltros = [

      { Codfiltro: "Codclasificacion1", Filtro: "Clase" },
      { Codfiltro: "Codclasificacion2", Filtro: "Grupo" },
      { Codfiltro: "Descripcion", Filtro: "Descripción" },

    ]

  }

  ngOnInit() {

    this.tConfiguracion$ = this.tConfiguracionService
      .Consumir_Obtener_Configuracion(this.tStrConfiguracion)
      .subscribe((tConfiguracion: FormularioConfiguracion) => {

        this.tConfiguracionFormulario = tConfiguracion;
        this.tTitulo = this.tConfiguracionFormulario.Titulo;
        this.tErroresServices.tTituloMsj = this.tConfiguracionFormulario.ModalMensaje.Titulo;
        this.tMsjServices.tTituloMsj = this.tConfiguracionFormulario.ModalMensaje.Titulo; //"Practica - Clasificación2 de artículos";
        this.tConfServices.tTituloConf = this.tConfiguracionFormulario.ModalConfirmacion.Titulo;//"Practica - Clasificación2 de artículos";

      });

    this.tEmpresa$ = this.tSesion.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    })

    this.tUsuario$ = this.tSesion.tUsuarioActual$.subscribe((tUsuario: Usuario) => {

      this.tUsuarioActual = tUsuario;

    })

    var tTienePermiso: boolean = this.tSesion.ObtenerPermisoAcceso(this.tCodFormularioActual);
    this.tPermisoAgregar = this.tSesion.ObtenerPermisoAgregar();
    this.tPermisoEditar = this.tSesion.ObtenerPermisoEditar();
    this.tPermisoEliminar = this.tSesion.ObtenerPermisoEliminar();

    if (tTienePermiso == true) {

      this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
        .subscribe((tHayConfig: boolean) => {

          if (tHayConfig === true) {

            this.BuscarConfiguracion();
            this.Buscar_Clasificaciones1();
            this.Buscar_Clasificaciones2();
            this.Nuevo();

          }

        });

    }
    else {

      this.tMsjServices.AbrirModalMsj(environment.msjSinAcceso);
      this.router.navigate(['/Home']);

    }

  }

  ngOnDestroy() {

    this.tEmpresa$.unsubscribe();
    this.tUsuario$.unsubscribe();
    this.tConfiguracion$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();

  }

  //#region "Funciones de busqueda"

  Buscar_Clasificaciones2(): void {

    this.Ws.Clasificacion2()
      .Consumir_Obtener_Clasificacion2(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa)
      .subscribe(Respuesta => {

        this.tClasificaciones2 = [];

        if (Respuesta.Resultado == "N") {

          // this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tClasificaciones2 = Respuesta.Datos;
          this.tFiltroActual = this.tFiltros[0];

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Buscar_Clasificaciones1(): void {
    this.Ws
      .Clasificacion1()
      .Consumir_Obtener_Clasificacion1(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa)
      .subscribe(Respuesta => {

        this.tClasificaciones1 = [];

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tClasificaciones1 = Respuesta.Datos;

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }


  BuscarConfiguracion(): void {

    this.Ws
      .Configuraciones()
      .Consumir_Obtener_Configuracion(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        'MR_CLASIFICACION2',
        'IMAGEN')
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          // this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tConfiguracion = Respuesta.Datos[0];

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  //#region "CRUD"

  Nueva_Clasificacion2(): void {

    var tNuevaClasificacion2: Clasificacion2Entrada;
    tNuevaClasificacion2 = {
      "Id": 0,
      "Corporacion": this.tEmpresaActual.Corporacion,
      "Empresa": this.tEmpresaActual.Empresa,
      "Codclasificacion1": this.tClasificacionActual.Codclasificacion1,
      "Codclasificacion2": "",
      "Descripcion": this.tClasificacionActual.Descripcion,
      "Imagen64": this.tManejarImagen.obtenerImagen64(),
      "Url": this.tClasificacionActual.Url,
      "Usuariocreador": this.tUsuarioActual.Codusuario,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }

    this.Ws.Clasificacion2()
      .Consumir_Crear_Clasificacion2(tNuevaClasificacion2)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.toastr.success('El grupo se ha creado de manera exitosa!');
          this.tFiltroActual = this.tFiltros[0];
          this.Buscar_Clasificaciones2();
          this.Nuevo();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Modificar_Clasificacion2(): void {

    var tModificarClasificacion2: Clasificacion2Entrada;
    tModificarClasificacion2 = {
      "Id": 0,
      "Corporacion": this.tClasificacionActual.Corporacion,
      "Empresa": this.tClasificacionActual.Empresa,
      "Codclasificacion1": this.tClasificacionActual.Codclasificacion1,
      "Codclasificacion2": this.tClasificacionActual.Codclasificacion2,
      "Descripcion": this.tClasificacionActual.Descripcion,
      "Imagen64": this.tManejarImagen.obtenerImagen64(),
      "Url": this.tClasificacionActual.Url,
      "Usuariocreador": this.tClasificacionActual.Usuariocreador,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }

    this.Ws.Clasificacion2()
      .Consumir_Modificar_Clasificacion2(tModificarClasificacion2)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.toastr.success('El grupo se ha modificado de manera exitosa!')
          this.tFiltroActual = this.tFiltros[0];
          this.Buscar_Clasificaciones2();
          this.Nuevo();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Eliminar_Clasificacion2(): void {

    var tEliminarClasificacion2: Clasificacion2Entrada;
    tEliminarClasificacion2 = {
      "Id": 0,
      "Corporacion": this.tClasificacionActual.Corporacion,
      "Empresa": this.tClasificacionActual.Empresa,
      "Codclasificacion1": this.tClasificacionActual.Codclasificacion1,
      "Codclasificacion2": this.tClasificacionActual.Codclasificacion2,
      "Descripcion": this.tClasificacionActual.Descripcion,
      "Imagen64": "",
      "Url": this.tClasificacionActual.Url,
      "Usuariocreador": this.tClasificacionActual.Usuariocreador,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }

    this.Ws.Clasificacion2()
      .Consumir_Eliminar_Clasificacion2(tEliminarClasificacion2)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.toastr.success('El grupo se ha eliminado de manera exitosa!');
          this.tFiltroActual = this.tFiltros[0];
          this.Buscar_Clasificaciones2();
          this.Nuevo();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  Nuevo(): void {

    this.tHabilitarGuardar = this.tPermisoAgregar;
    this.tNuevo = true;
    this.tClasificacionActual = new Clasificacion2();
    this.tManejarImagen.tImagen = "";
    this.tClasificacionDetalles.Limpiar();

    this.toastr.info('Por favor llene los datos necesarios para registrar un nuevo Grupo!');

  }

  Seleccionar_Clasificacion(tClasificacion_Sel: Clasificacion2, tPos: number): void {

    this.tHabilitarGuardar = this.tPermisoEditar;
    this.tNuevo = false;
    this.tPosicion = tPos;
    this.tClasificacionActual = new Clasificacion2();
    Object.assign(this.tClasificacionActual, tClasificacion_Sel);
    // this.tClasificacionActual.Id = tClasificacion_Sel.Id;
    // this.tClasificacionActual.Corporacion = tClasificacion_Sel.Corporacion;
    // this.tClasificacionActual.Empresa = tClasificacion_Sel.Empresa;
    // this.tClasificacionActual.Codclasificacion1 = tClasificacion_Sel.Codclasificacion1;
    // this.tClasificacionActual.Codclasificacion2 = tClasificacion_Sel.Codclasificacion2;
    // this.tClasificacionActual.Url = tClasificacion_Sel.Url;
    // this.tClasificacionActual.Descripcion = tClasificacion_Sel.Descripcion;
    // this.tClasificacionActual.Fecha_creacion = tClasificacion_Sel.Fecha_creacion;
    // this.tClasificacionActual.Fecha_creacionl = tClasificacion_Sel.Fecha_creacionl;
    // this.tClasificacionActual.Fecha_modificacion = tClasificacion_Sel.Fecha_modificacion;
    // this.tClasificacionActual.Fecha_modificacionl = tClasificacion_Sel.Fecha_modificacionl;
    // this.tClasificacionActual.Usuariocreador = tClasificacion_Sel.Usuariocreador;
    this.tClasificacionActual.Usuariomodificador = this.tUsuarioActual.Usuariomodificador;
    this.tManejarImagen.tImagen = this.tClasificacionActual.Url;

  }

  Seleccionar_Clasificacion_Eliminar(tClasificacion_Sel: Clasificacion2): void {

    this.tClasificacionActual = new Clasificacion2();
    Object.assign(this.tClasificacionActual, tClasificacion_Sel);
    // this.tClasificacionActual.Id = tClasificacion_Sel.Id;
    // this.tClasificacionActual.Corporacion = tClasificacion_Sel.Corporacion;
    // this.tClasificacionActual.Empresa = tClasificacion_Sel.Empresa;
    // this.tClasificacionActual.Codclasificacion1 = tClasificacion_Sel.Codclasificacion1;
    // this.tClasificacionActual.Codclasificacion2 = tClasificacion_Sel.Codclasificacion2;
    // this.tClasificacionActual.Url = tClasificacion_Sel.Url;
    // this.tClasificacionActual.Descripcion = tClasificacion_Sel.Descripcion;
    // this.tClasificacionActual.Fecha_creacion = tClasificacion_Sel.Fecha_creacion;
    // this.tClasificacionActual.Fecha_creacionl = tClasificacion_Sel.Fecha_creacionl;
    // this.tClasificacionActual.Fecha_modificacion = tClasificacion_Sel.Fecha_modificacion;
    // this.tClasificacionActual.Fecha_modificacionl = tClasificacion_Sel.Fecha_modificacionl;
    // this.tClasificacionActual.Usuariocreador = tClasificacion_Sel.Usuariocreador;
    this.tClasificacionActual.Usuariomodificador = this.tUsuarioActual.Usuariomodificador;

    this.tConfServices
      .AbrirModalConf(this.tConfiguracionFormulario.ModalConfirmacion.Eliminacion)
      .then(Resultado => {

        switch (Resultado) {
          case "S": {

            this.Eliminar_Clasificacion2();
            break;

          }
        }

      });

  }

  Cancelar(): void {

    this.Nuevo();

  }

  ValidarFormulario(): boolean {

    let valido: boolean = false;

    if (this.tClasificacionActual.Descripcion == "") {

      this.toastr.warning('Debe ingresar una Descripción!', 'Disculpe!');
      return valido;

    }

    return true;

  }

  Agregar(): void {

    if (this.ValidarFormulario() == false) {

      return;

    }

    switch (this.tNuevo) {
      case true: {

        this.tConfServices
          .AbrirModalConf(this.tConfiguracionFormulario.ModalConfirmacion.Agregar)
          .then(Resultado => {

            switch (Resultado) {
              case "S": {

                this.Nueva_Clasificacion2();
                break;

              }
            }

          });

        break;

      }
      default: {

        this.tConfServices
          .AbrirModalConf(this.tConfiguracionFormulario.ModalConfirmacion.Edicion)
          .then(Resultado => {

            switch (Resultado) {
              case "S": {

                this.Modificar_Clasificacion2();
                break;

              }
            }

          });

        break;

      }
    }

  }

}
