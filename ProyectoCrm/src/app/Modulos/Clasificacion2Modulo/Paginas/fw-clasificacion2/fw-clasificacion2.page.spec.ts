import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwClasificacion2Page } from './fw-clasificacion2.page';

describe('FwClasificacion2Page', () => {
  let component: FwClasificacion2Page;
  let fixture: ComponentFixture<FwClasificacion2Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwClasificacion2Page ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwClasificacion2Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
