import { Component, OnInit, Input } from '@angular/core';
import { Clasificacion2 } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-clasificacion2-cabecera',
  templateUrl: './fw-clasificacion2-cabecera.component.html',
  styleUrls: ['./fw-clasificacion2-cabecera.component.scss']
})
export class FwClasificacion2CabeceraComponent implements OnInit {

  @Input('Clasificacion2') tClasificacion2: Clasificacion2 = new Clasificacion2();

  constructor() { }

  ngOnInit(): void {
  }

}
