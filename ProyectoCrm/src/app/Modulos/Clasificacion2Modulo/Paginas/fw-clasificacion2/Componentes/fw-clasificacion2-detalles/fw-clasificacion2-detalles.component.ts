import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Clasificacion2, Clasificacion1, Configuracion } from 'src/app/Clases/Estructura';
import { ManejarImagenes } from 'src/app/Manejadores/cls-procedimientos';
import { Clasificaciones1Service } from 'src/app/Modales/clasificacion1/fw-modal-clasificaciones1/clasificaciones1.service';

@Component({
  selector: 'app-fw-clasificacion2-detalles',
  templateUrl: './fw-clasificacion2-detalles.component.html',
  styleUrls: ['./fw-clasificacion2-detalles.component.scss']
})
export class FwClasificacion2DetallesComponent implements OnInit, OnChanges {

  @Input('Clasificacion2') tClasificacion2: Clasificacion2 = new Clasificacion2();
  @Input('Clasificaciones1') tClasificaciones1: Clasificacion1[] = [];
  @Input('ManejarImagen') tManejarImagen: ManejarImagenes = new ManejarImagenes();
  @Input('Configuracion') tConfiguracion: Configuracion = new Configuracion();
  @Input('Nuevo') tNuevo: boolean = false;
  tClasificacion1Actual: Clasificacion1 = new Clasificacion1();

  constructor(private tClasificacion1Service: Clasificaciones1Service) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {

    if ('tClasificacion2' in changes) {

      this.tClasificacion1Actual = this.tClasificaciones1.find(tClasificacion1 => {
        return tClasificacion1.Corporacion === this.tClasificacion2.Corporacion &&
          tClasificacion1.Empresa === this.tClasificacion2.Empresa &&
          tClasificacion1.Codclasificacion1 === this.tClasificacion2.Codclasificacion1
      }) || this.tClasificaciones1[0] || new Clasificacion1();

      if(this.tClasificacion2.Codclasificacion2 === ''){

        this.Cambiar_Clasificacion1();

      }

    }

    if ('tClasificaciones1' in changes) {

      this.tClasificacion1Actual = this.tClasificaciones1[0] || new Clasificacion1();
      this.Cambiar_Clasificacion1();

    }

  }

  //#region "Funciones Modal"

  AbrirModalClasificacion1(): void {

    this.tClasificacion1Service
      .AbrirModal()
      .then(result => {

        if (result.Resultado == "S") {

          var tClasificacion1Modal: Clasificacion1 = result.Clasificacion1;
          this.tClasificacion1Actual = this.tClasificaciones1.find(tClasificacion1 => {
            return tClasificacion1.Corporacion == tClasificacion1Modal.Corporacion &&
              tClasificacion1.Empresa == tClasificacion1Modal.Empresa &&
              tClasificacion1.Codclasificacion1 == tClasificacion1Modal.Codclasificacion1
          }) || this.tClasificaciones1[0] || new Clasificacion1();
          this.Cambiar_Clasificacion1();

        }

      }, reason => {



      });

  }

  //#endregion

  Limpiar() {
    this.tClasificacion1Actual = this.tClasificaciones1[0] || new Clasificacion1();
  }

  Cambiar_Clasificacion1(): void {

    this.tClasificacion2.Codclasificacion1 = this.tClasificacion1Actual.Codclasificacion1;

  }

  BloquearImagen(): boolean {

    if (this.tConfiguracion.Valor == "S") {

      return false;

    }
    else {

      return true;

    }

  }

}
