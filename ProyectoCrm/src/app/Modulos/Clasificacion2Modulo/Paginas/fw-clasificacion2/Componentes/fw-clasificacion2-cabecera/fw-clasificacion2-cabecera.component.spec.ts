import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwClasificacion2CabeceraComponent } from './fw-clasificacion2-cabecera.component';

describe('FwClasificacion2CabeceraComponent', () => {
  let component: FwClasificacion2CabeceraComponent;
  let fixture: ComponentFixture<FwClasificacion2CabeceraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwClasificacion2CabeceraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwClasificacion2CabeceraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
