import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwClasificacion2DetallesComponent } from './fw-clasificacion2-detalles.component';

describe('FwClasificacion2DetallesComponent', () => {
  let component: FwClasificacion2DetallesComponent;
  let fixture: ComponentFixture<FwClasificacion2DetallesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwClasificacion2DetallesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwClasificacion2DetallesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
