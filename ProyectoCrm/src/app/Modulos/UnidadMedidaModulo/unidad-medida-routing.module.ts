import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FwPlantillaComponent  } from 'src/app/Shared/Formularios/fw-plantilla/fw-plantilla.component';
import { FwUnidadMedidaPage } from './Paginas/fw-unidad-medida/fw-unidad-medida.page';

const routes: Routes = [
  {
    path: '',
    component: FwPlantillaComponent,
    children:[
      {
        path: '',
        component: FwUnidadMedidaPage,
        data:{
          Codformulario: 'fUnidadMedida'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UnidadMedidaRoutingModule { }
