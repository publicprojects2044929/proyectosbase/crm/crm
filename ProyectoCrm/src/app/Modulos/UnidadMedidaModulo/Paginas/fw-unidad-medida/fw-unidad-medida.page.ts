import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { Usuario, Empresas, UnidadMedida, Filtros } from 'src/app/Clases/Estructura';
import { SessionService } from 'src/app/Core/Session/session.service';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { ToastrService } from 'ngx-toastr';
import { ErroresService } from 'src/app/Modales/mensajes/fw-modal-msj/errores.service';
import { MensajesService } from 'src/app/Modales/mensajes/fw-modal-msj/mensajes.service';
import { ConfirmacionService } from 'src/app/Modales/confirmacion/fw-modal-confirmacion/confirmacion.service';
import { UnidadMedidaEntrada } from 'src/app/Clases/EstructuraEntrada';
import { Subscription } from 'rxjs';
import { FormularioConfiguracion } from 'src/app/Clases/EstructuraConfiguracion';
import { ConfiguracionService } from 'src/app/Core/Configuracion/configuracion.service';

@Component({
  selector: 'app-fw-unidad-medida',
  templateUrl: './fw-unidad-medida.page.html',
  styleUrls: ['./fw-unidad-medida.page.scss']
})

export class FwUnidadMedidaPage implements OnInit, OnDestroy {

  tConfiguracionFormulario: FormularioConfiguracion = new FormularioConfiguracion();
  tCodFormularioActual: string = "fUnidadMedida";
  tPermisoAgregar: boolean = false;
  tPermisoEditar: boolean = false;
  tPermisoEliminar: boolean = false;
  tHabilitarGuardar: boolean = false;

  tUsuarioActual: Usuario = new Usuario();
  tEmpresaActual: Empresas = new Empresas();
  tEmpresa$: Subscription;
  tUsuario$: Subscription;

  tTitulo: string = "Gestión de Unidades";
  tNuevo: boolean = true;
  tPosicion: number = 0;
  tPagina: number = 1;
  tTamanoPag: number = 5;

  tUnidades: UnidadMedida[] = [];
  tUnidadActual: UnidadMedida = new UnidadMedida();

  tFiltros: Filtros[] = [];
  tFiltroActual: Filtros = new Filtros();
  tFiltroBusqueda: string = "";

  tConfiguracion$: Subscription;
  tHayConfiguracionWs$: Subscription;
  tStrConfiguracion: string = 'UnidadMedida/UnidadMedida.Config.json';

  constructor(public router: Router,
    private tSesion: SessionService,
    public Ws: ClsServiciosService,
    private tConfiguracionService: ConfiguracionService,
    private toastr: ToastrService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService,
    private tConfServices: ConfirmacionService) {

    this.tFiltroBusqueda = "";
    this.tPosicion = 0;

    this.tFiltros = [

      { Codfiltro: "Codunidad", Filtro: "Código" },
      { Codfiltro: "Descripcion", Filtro: "Descripción" },

    ]

  }

  ngOnInit() {

    this.tConfiguracion$ = this.tConfiguracionService
      .Consumir_Obtener_Configuracion(this.tStrConfiguracion)
      .subscribe((tConfiguracion: FormularioConfiguracion) => {

        this.tConfiguracionFormulario = tConfiguracion;
        this.tTitulo = this.tConfiguracionFormulario.Titulo;
        this.tErroresServices.tTituloMsj = this.tConfiguracionFormulario.ModalMensaje.Titulo;
        this.tMsjServices.tTituloMsj = this.tConfiguracionFormulario.ModalMensaje.Titulo;
        this.tConfServices.tTituloConf = this.tConfiguracionFormulario.ModalConfirmacion.Titulo;

      });

    this.tEmpresa$ = this.tSesion.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    })

    this.tUsuario$ = this.tSesion.tUsuarioActual$.subscribe((tUsuario: Usuario) => {

      this.tUsuarioActual = tUsuario;

    })

    var tTienePermiso: boolean = this.tSesion.ObtenerPermisoAcceso(this.tCodFormularioActual);
    this.tPermisoAgregar = this.tSesion.ObtenerPermisoAgregar();
    this.tPermisoEditar = this.tSesion.ObtenerPermisoEditar();
    this.tPermisoEliminar = this.tSesion.ObtenerPermisoEliminar();

    if (tTienePermiso == true) {

      this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
        .subscribe((tHayConfig: boolean) => {

          if (tHayConfig === true) {

            this.Buscar_Unidades();
            this.Nuevo();

          }

        });

    }
    else {

      this.tMsjServices.AbrirModalMsj(environment.msjSinAcceso);
      this.router.navigate(['/Home']);

    }

  }

  ngOnDestroy() {

    this.tEmpresa$.unsubscribe();
    this.tUsuario$.unsubscribe();
    this.tConfiguracion$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();

  }

  //#region "Funciones de busqueda"

  Buscar_Unidades(): void {

    this.Ws.Unidad()
      .Consumir_Obtener_UnidadesMedida(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa)
      .subscribe(Respuesta => {

        this.tUnidades = [];

        if (Respuesta.Resultado == "N") {

          // this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);
        }
        else {

          this.tUnidades = Respuesta.Datos;
          this.tFiltroActual = this.tFiltros[0];

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  //#region "CRUD"

  Nueva_Unidad(): void {


    var tNuevaUnidad: UnidadMedidaEntrada;
    tNuevaUnidad = {
      "Id": 0,
      "Corporacion": this.tEmpresaActual.Corporacion,
      "Empresa": this.tEmpresaActual.Empresa,
      "Codunidadmedida": "",
      "Descripcion": this.tUnidadActual.Descripcion,
      "Factor_venta": this.tUnidadActual.Factor_venta,
      "Simbolo": this.tUnidadActual.Simbolo,
      "Usuariocreador": this.tUsuarioActual.Codusuario,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }

    this.Ws.Unidad()
      .Consumir_Crear_UnidadMedida(tNuevaUnidad)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.toastr.success('La unidad de medida se ha creado de manera exitosa!');
          this.tFiltroActual = this.tFiltros[0];
          this.Buscar_Unidades();
          this.Nuevo();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Modificar_Unidad(): void {

    var tModificarUnidad: UnidadMedidaEntrada;
    tModificarUnidad = {
      "Id": 0,
      "Corporacion": this.tUnidadActual.Corporacion,
      "Empresa": this.tUnidadActual.Empresa,
      "Codunidadmedida": this.tUnidadActual.Codunidadmedida,
      "Descripcion": this.tUnidadActual.Descripcion,
      "Simbolo": this.tUnidadActual.Simbolo,
      "Factor_venta": this.tUnidadActual.Factor_venta,
      "Usuariocreador": this.tUnidadActual.Usuariocreador,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }

    this.Ws.Unidad()
      .Consumir_Modificar_UnidadMedida(tModificarUnidad)
      .subscribe(Respuesta => {


        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.toastr.success('La unidad de medida se ha modificado de manera exitosa!');
          this.tFiltroActual = this.tFiltros[0];
          this.Buscar_Unidades();
          this.Nuevo();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Eliminar_Unidad(): void {

    var tEliminarUnidad: UnidadMedidaEntrada;
    tEliminarUnidad = {
      "Id": 0,
      "Corporacion": this.tUnidadActual.Corporacion,
      "Empresa": this.tUnidadActual.Empresa,
      "Codunidadmedida": this.tUnidadActual.Codunidadmedida,
      "Descripcion": this.tUnidadActual.Descripcion,
      "Simbolo": this.tUnidadActual.Simbolo,
      "Factor_venta": this.tUnidadActual.Factor_venta,
      "Usuariocreador": this.tUnidadActual.Usuariocreador,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }

    this.Ws.Unidad()
      .Consumir_Eliminar_UnidadMedida(tEliminarUnidad)
      .subscribe(Respuesta => {


        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.toastr.success('La unidad de medida se ha eliminado de manera exitosa!');
          this.tFiltroActual = this.tFiltros[0];
          this.Buscar_Unidades();
          this.Nuevo();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  Nuevo(): void {

    this.tHabilitarGuardar = this.tPermisoAgregar;
    this.tNuevo = true;
    this.tUnidadActual = new UnidadMedida();

    this.toastr.info('Por favor llene los datos necesarios para registrar una nueva Unidad de medida!');

  }

  Seleccionar_Unidad(tUnidad_Sel: UnidadMedida, tPos: number): void {

    this.tHabilitarGuardar = this.tPermisoEditar;
    this.tNuevo = false;
    this.tPosicion = tPos;
    this.tUnidadActual = new UnidadMedida();
    Object.assign(this.tUnidadActual, tUnidad_Sel);
    this.tUnidadActual.Usuariomodificador = this.tUsuarioActual.Usuariomodificador;

  }

  Seleccionar_Unidad_Eliminar(tUnidad_Sel: UnidadMedida): void {

    this.tUnidadActual = new UnidadMedida();
    Object.assign(this.tUnidadActual, tUnidad_Sel);
    this.tUnidadActual.Usuariomodificador = this.tUsuarioActual.Usuariomodificador;

    this.tConfServices
      .AbrirModalConf( this.tConfiguracionFormulario.ModalConfirmacion.Eliminacion)
      .then(Respuesta => {

        switch (Respuesta) {

          case "S": {

            this.Eliminar_Unidad();
            break;

          }

        };

      });

  }

  Cancelar(): void {

    this.Nuevo();

  }

  ValidarFormulario(): boolean {

    let valido: boolean = false;

    if (this.tUnidadActual.Descripcion == "") {

      this.toastr.warning('Debe ingresar una descripciòn!', 'Disculpe!');
      return valido;

    }

    if (this.tUnidadActual.Factor_venta > 99) {

      this.toastr.error('Debe ingresar un Factor venta menor o igual a 99!', 'Disculpe!');
      return valido;

    }

    if (this.tUnidadActual.Factor_venta <= 0) {

      this.toastr.error('Debe ingresar un Factor venta vàlido', 'Disculpe!');
      return valido;

    }

    return true;
  }


  Agregar(): void {

    if (this.ValidarFormulario() == false) {
      return
    }

    switch (this.tNuevo) {
      case true: {

        this.tConfServices
          .AbrirModalConf( this.tConfiguracionFormulario.ModalConfirmacion.Agregar)
          .then(Respuesta => {

            switch (Respuesta) {

              case "S": {

                this.Nueva_Unidad();
                break;

              }

            };

          });

        break;

      }
      default: {

        this.tConfServices
          .AbrirModalConf(this.tConfiguracionFormulario.ModalConfirmacion.Edicion)
          .then(Respuesta => {

            switch (Respuesta) {

              case "S": {

                this.Modificar_Unidad();
                break;

              }

            };

          });

        break;

      }
    }

  }

}
