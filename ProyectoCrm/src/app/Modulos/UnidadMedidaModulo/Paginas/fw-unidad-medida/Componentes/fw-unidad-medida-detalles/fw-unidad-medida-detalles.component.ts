import { Component, OnInit, Input } from '@angular/core';
import { UnidadMedida } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-unidad-medida-detalles',
  templateUrl: './fw-unidad-medida-detalles.component.html',
  styleUrls: ['./fw-unidad-medida-detalles.component.scss']
})
export class FwUnidadMedidaDetallesComponent implements OnInit {

  @Input('UnidadMedida') tUnidad: UnidadMedida = new UnidadMedida();

  constructor() { }

  ngOnInit(): void {
  }

}
