import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwUnidadMedidaCabeceraComponent } from './fw-unidad-medida-cabecera.component';

describe('FwUnidadMedidaCabeceraComponent', () => {
  let component: FwUnidadMedidaCabeceraComponent;
  let fixture: ComponentFixture<FwUnidadMedidaCabeceraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwUnidadMedidaCabeceraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwUnidadMedidaCabeceraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
