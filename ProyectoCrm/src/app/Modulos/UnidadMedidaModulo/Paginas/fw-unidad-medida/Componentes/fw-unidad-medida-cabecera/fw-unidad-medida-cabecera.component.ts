import { Component, OnInit, Input } from '@angular/core';
import { UnidadMedida } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-unidad-medida-cabecera',
  templateUrl: './fw-unidad-medida-cabecera.component.html',
  styleUrls: ['./fw-unidad-medida-cabecera.component.scss']
})
export class FwUnidadMedidaCabeceraComponent implements OnInit {

  @Input('UnidadMedida') tUnidad: UnidadMedida = new UnidadMedida();

  constructor() { }

  ngOnInit(): void {
  }

}
