import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwUnidadMedidaDetallesComponent } from './fw-unidad-medida-detalles.component';

describe('FwUnidadMedidaDetallesComponent', () => {
  let component: FwUnidadMedidaDetallesComponent;
  let fixture: ComponentFixture<FwUnidadMedidaDetallesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwUnidadMedidaDetallesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwUnidadMedidaDetallesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
