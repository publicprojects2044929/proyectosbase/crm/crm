import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwUnidadMedidaPage } from './fw-unidad-medida.page';

describe('FwUnidadMedidaPage', () => {
  let component: FwUnidadMedidaPage;
  let fixture: ComponentFixture<FwUnidadMedidaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwUnidadMedidaPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwUnidadMedidaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
