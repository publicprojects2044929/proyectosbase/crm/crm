import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from 'src/app/Shared/shared.module';

import { UnidadMedidaRoutingModule } from './unidad-medida-routing.module';
import { FwUnidadMedidaPage } from './Paginas/fw-unidad-medida/fw-unidad-medida.page';
import { FwUnidadMedidaCabeceraComponent } from './Paginas/fw-unidad-medida/Componentes/fw-unidad-medida-cabecera/fw-unidad-medida-cabecera.component';
import { FwUnidadMedidaDetallesComponent } from './Paginas/fw-unidad-medida/Componentes/fw-unidad-medida-detalles/fw-unidad-medida-detalles.component';


@NgModule({
  declarations: [FwUnidadMedidaPage, FwUnidadMedidaCabeceraComponent, FwUnidadMedidaDetallesComponent],
  imports: [
    CommonModule,
    UnidadMedidaRoutingModule,
    SharedModule,
    FormsModule,
    NgbModule
  ]
})
export class UnidadMedidaModule { }
