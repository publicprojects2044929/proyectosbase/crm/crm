import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClienteRoutingModule } from './cliente-routing.module';
import { FwClientePage } from './Paginas/fw-cliente/fw-cliente.page';

// import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
// import { NgSelectModule } from '@ng-select/ng-select';
// import { GoogleChartsModule } from 'angular-google-charts';
// import { AgmCoreModule } from '@agm/core'

import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { SharedModule } from 'src/app/Shared/shared.module'
// import { environment } from 'src/environments/environment';
import { FwClienteCabeceraComponent } from './Paginas/fw-cliente/Componentes/fw-cliente-cabecera/fw-cliente-cabecera.component';
import { FwClienteInformacionComponent } from './Paginas/fw-cliente/Componentes/fw-cliente-informacion/fw-cliente-informacion.component';
import { FwClienteDocumentosComponent } from './Paginas/fw-cliente/Componentes/fw-cliente-documentos/fw-cliente-documentos.component';
import { FwClienteContactoComponent } from './Paginas/fw-cliente/Componentes/fw-cliente-contacto/fw-cliente-contacto.component';
import { FwClienteNacimientoComponent } from './Paginas/fw-cliente/Componentes/fw-cliente-nacimiento/fw-cliente-nacimiento.component';
import { FwClienteFacturacionComponent } from './Paginas/fw-cliente/Componentes/fw-cliente-facturacion/fw-cliente-facturacion.component';
import { FwClienteEnvioComponent } from './Paginas/fw-cliente/Componentes/fw-cliente-envio/fw-cliente-envio.component';

@NgModule({
  declarations: [FwClientePage, FwClienteCabeceraComponent, FwClienteInformacionComponent, FwClienteDocumentosComponent, FwClienteContactoComponent, FwClienteNacimientoComponent, FwClienteFacturacionComponent, FwClienteEnvioComponent],
  imports: [
    CommonModule,
    ClienteRoutingModule,
    SharedModule,
    FormsModule,
    BsDatepickerModule.forRoot(),
    NgbModule
  ]
})
export class ClienteModule { }
