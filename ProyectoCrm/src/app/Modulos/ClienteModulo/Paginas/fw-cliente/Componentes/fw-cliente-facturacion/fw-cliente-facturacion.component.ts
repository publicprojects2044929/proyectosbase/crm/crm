import { Component, OnInit, OnChanges, Input, SimpleChanges } from '@angular/core';
import { Cliente, Pais, Estado, Municipio, Ciudad } from 'src/app/Clases/Estructura';
import { PaisesService } from 'src/app/Modales/pais/fw-modal-pais/paises.service';
import { EstadosService } from 'src/app/Modales/estado/fw-modal-estado/estados.service';
import { MunicipiosService } from 'src/app/Modales/municipio/fw-modal-municipio/municipios.service';
import { CiudadesService } from 'src/app/Modales/ciudad/fw-modal-ciudades/ciudades.service';

@Component({
  selector: 'app-fw-cliente-facturacion',
  templateUrl: './fw-cliente-facturacion.component.html',
  styleUrls: ['./fw-cliente-facturacion.component.scss']
})
export class FwClienteFacturacionComponent implements OnInit, OnChanges {

  @Input('Cliente') tCliente: Cliente = new Cliente();
  @Input('Paises') tPaises: Pais[] = [];

  tEstados: Estado[] = [];
  tMunicipios: Municipio[] = [];
  tCiudades: Ciudad[] = [];

  tPaisfacturacionActual: Pais = new Pais();
  tEstadofacturacionActual: Estado = new Estado();
  tMunicipiofacturacionActual: Municipio = new Municipio();
  tCiudadfacturacionActual: Ciudad = new Ciudad();

  constructor(private tPaisService: PaisesService,
    private tEstadoService: EstadosService,
    private tMunicipioService: MunicipiosService,
    private tCiudadService: CiudadesService) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {

    if ('tPaises' in changes) {

      this.tPaisfacturacionActual = this.tPaises[0] || new Pais();
      this.Seleccionar_Pais_Facturacion();

    }

    if ('tCliente' in changes) {

      this.tPaisfacturacionActual = this.tPaises.find(tPais => {
        return tPais.Corporacion === this.tCliente.Corporacion &&
          tPais.Empresa === this.tCliente.Empresa &&
          tPais.Codpais === this.tCliente.Codpais_facturacion
      }) || this.tPaises[0] || new Pais();

      this.tEstados = this.tPaisfacturacionActual.LEstados || [];

      this.tEstadofacturacionActual = this.tEstados.find(tEstado => {
        return tEstado.Corporacion === this.tCliente.Corporacion &&
          tEstado.Empresa === this.tCliente.Empresa &&
          tEstado.Codpais === this.tCliente.Codpais_facturacion &&
          tEstado.Codestado === this.tCliente.Codestado_facturacion
      }) || this.tEstados[0] || new Estado();

      this.tMunicipios = this.tEstadofacturacionActual.LMunicipio || [];

      this.tMunicipiofacturacionActual = this.tMunicipios.find(tMunicipio => {
        return tMunicipio.Corporacion === this.tCliente.Corporacion &&
          tMunicipio.Empresa === this.tCliente.Empresa &&
          tMunicipio.Codpais === this.tCliente.Codpais_facturacion &&
          tMunicipio.Codestado === this.tCliente.Codestado_facturacion &&
          tMunicipio.Codmunicipio === this.tCliente.Codmunicipio_facturacion
      }) || this.tMunicipios[0] || new Municipio();

      this.tCiudades = this.tMunicipiofacturacionActual.LCiudad || [];

      this.tCiudadfacturacionActual = this.tCiudades.find(tCiudad => {
        return tCiudad.Corporacion === this.tCliente.Corporacion &&
          tCiudad.Empresa === this.tCliente.Empresa &&
          tCiudad.Codpais === this.tCliente.Codpais_facturacion &&
          tCiudad.Codestado === this.tCliente.Codestado_facturacion &&
          tCiudad.Codmunicipio === this.tCliente.Codmunicipio_facturacion &&
          tCiudad.Codciudad === this.tCliente.Codciudad_facturacion
      }) || this.tCiudades[0] || new Ciudad();

      if (this.tCliente.Codcliente === '') {

        this.Seleccionar_Pais_Facturacion();

      }


    }

  }

  AbrirModalPais(): void {

    this.tPaisService
      .AbrirModal()
      .then(result => {

        if (result.Resultado == "S") {

          var tPaisModal: Pais = result.Pais;
          this.tPaisfacturacionActual = this.tPaises.find(tPais => {
            return tPais.Corporacion === tPaisModal.Corporacion &&
              tPais.Empresa === tPaisModal.Empresa &&
              tPais.Codpais === tPaisModal.Codpais
          }) || this.tPaises[0] || new Pais();

          this.Seleccionar_Pais_Facturacion();

        }

      }, reason => {



      });

  }

  AbrirModalEstado(): void {

    this.tEstadoService
      .AbrirModal(this.tPaisfacturacionActual)
      .then(result => {

        if (result.Resultado == "S") {

          var tEstadoModal: Estado = result.Estado;
          this.tEstadofacturacionActual = this.tEstados.find(tEstado => {
            return tEstado.Corporacion === tEstadoModal.Corporacion &&
              tEstado.Empresa === tEstadoModal.Empresa &&
              tEstado.Codpais === tEstadoModal.Codpais &&
              tEstado.Codestado === tEstadoModal.Codestado
          }) || this.tEstados[0] || new Estado();

          this.Seleccionar_Estado_Facturacion();

        }

      }, reason => {



      });

  }

  AbrirModalMunicipio(): void {

    this.tMunicipioService
      .AbrirModal(this.tEstadofacturacionActual)
      .then(result => {

        if (result.Resultado == "S") {

          var tMunicipioModal: Municipio = result.Municipio;
          this.tMunicipiofacturacionActual = this.tMunicipios.find(tMunicipio => {
            return tMunicipio.Corporacion === tMunicipioModal.Corporacion &&
              tMunicipio.Empresa === tMunicipioModal.Empresa &&
              tMunicipio.Codpais === tMunicipioModal.Codpais &&
              tMunicipio.Codestado === tMunicipioModal.Codestado &&
              tMunicipio.Codmunicipio === tMunicipioModal.Codmunicipio
          }) || this.tMunicipios[0] || new Municipio();

          this.Seleccionar_Municipio_Facturacion();

        }

      }, reason => {



      });

  }

  AbrirModalCiudad(): void {

    this.tCiudadService
      .AbrirModal(this.tMunicipiofacturacionActual)
      .then(result => {

        if (result.Resultado == "S") {

          var tCiudadModal: Ciudad = result.Ciudad;
          this.tCiudadfacturacionActual = this.tCiudades.find(tCiudad => {
            return tCiudad.Corporacion === tCiudadModal.Corporacion &&
              tCiudad.Empresa === tCiudadModal.Empresa &&
              tCiudad.Codpais === tCiudadModal.Codpais &&
              tCiudad.Codestado === tCiudadModal.Codestado &&
              tCiudad.Codmunicipio === tCiudadModal.Codmunicipio
          }) || this.tCiudades[0] || new Ciudad();

          this.Seleccionar_Ciudad_Facturacion();

        }

      }, reason => {



      });

  }

  Seleccionar_Pais_Facturacion() {

    this.tEstados = this.tPaisfacturacionActual.LEstados || [];
    this.tEstadofacturacionActual = new Estado();
    this.tMunicipiofacturacionActual = new Municipio();
    this.tCiudadfacturacionActual = new Ciudad();

    this.tEstadofacturacionActual = this.tEstados[0] || new Estado();

    this.Seleccionar_Estado_Facturacion();

  }

  Seleccionar_Estado_Facturacion() {

    this.tMunicipios = this.tEstadofacturacionActual.LMunicipio || [];
    this.tMunicipiofacturacionActual = new Municipio();
    this.tCiudades = this.tMunicipiofacturacionActual.LCiudad || [];
    this.tCiudadfacturacionActual = new Ciudad();
    this.tMunicipiofacturacionActual = this.tMunicipios[0] || new Municipio();
    this.tCiudades = this.tMunicipiofacturacionActual.LCiudad || [];
    this.tCiudadfacturacionActual = this.tCiudades[0] || new Ciudad();

    this.Seleccionar_Municipio_Facturacion();

  };

  Seleccionar_Municipio_Facturacion() {

    this.tCiudades = this.tMunicipiofacturacionActual.LCiudad || [];
    this.tCiudadfacturacionActual = new Ciudad();
    this.tCiudadfacturacionActual = this.tCiudades[0] || new Ciudad();
    this.Seleccionar_Ciudad_Facturacion();

  };

  Seleccionar_Ciudad_Facturacion() {

    this.tCliente.Codpais_facturacion = this.tCiudadfacturacionActual.Codpais;
    this.tCliente.Codestado_facturacion = this.tCiudadfacturacionActual.Codestado;
    this.tCliente.Codmunicipio_facturacion = this.tCiudadfacturacionActual.Codmunicipio;
    this.tCliente.Codciudad_facturacion = this.tCiudadfacturacionActual.Codciudad;

  };

}
