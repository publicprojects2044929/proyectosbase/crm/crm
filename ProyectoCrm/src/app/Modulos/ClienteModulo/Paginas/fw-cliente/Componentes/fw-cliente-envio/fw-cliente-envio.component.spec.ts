import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwClienteEnvioComponent } from './fw-cliente-envio.component';

describe('FwClienteEnvioComponent', () => {
  let component: FwClienteEnvioComponent;
  let fixture: ComponentFixture<FwClienteEnvioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwClienteEnvioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwClienteEnvioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
