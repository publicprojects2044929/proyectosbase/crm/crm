import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwClienteInformacionComponent } from './fw-cliente-informacion.component';

describe('FwClienteInformacionComponent', () => {
  let component: FwClienteInformacionComponent;
  let fixture: ComponentFixture<FwClienteInformacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwClienteInformacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwClienteInformacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
