import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwClienteFacturacionComponent } from './fw-cliente-facturacion.component';

describe('FwClienteFacturacionComponent', () => {
  let component: FwClienteFacturacionComponent;
  let fixture: ComponentFixture<FwClienteFacturacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwClienteFacturacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwClienteFacturacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
