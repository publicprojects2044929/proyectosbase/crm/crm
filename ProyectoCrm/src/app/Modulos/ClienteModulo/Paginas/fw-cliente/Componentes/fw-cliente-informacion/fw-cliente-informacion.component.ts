import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Cliente, Genero, Estatus, Usuario, Ocupacion, Situacion, TiposOpcion, EstadoCivil, Tipo, Condicion } from 'src/app/Clases/Estructura';
import { UsuariosService } from 'src/app/Modales/usuario/fw-modal-usuarios/usuarios.service';
import { ManejarFechas } from 'src/app/Manejadores/cls-procedimientos';

@Component({
  selector: 'app-fw-cliente-informacion',
  templateUrl: './fw-cliente-informacion.component.html',
  styleUrls: ['./fw-cliente-informacion.component.scss']
})
export class FwClienteInformacionComponent implements OnInit, OnChanges {

  @Input('Cliente') tCliente: Cliente = new Cliente();
  @Input('Generos') tGeneros: Genero[] = [];
  @Input('Estatus') tEstatus: Estatus[] = [];
  @Input('BloquearBuscar') tBloquearBuscar: boolean = false;
  @Input('Ocupaciones') tOcupaciones: Ocupacion[] = [];
  @Input('Situaciones') tSituaciones: Situacion[] = [];
  @Input('TiposPrecios') tTiposPrecios: TiposOpcion[] = [];
  @Input('EstadoCiviles') tEstadociviles: EstadoCivil[] = [];
  @Input('TiposClientes') tTiposClientes: Tipo[] = [];
  @Input('Condiciones') tCondiciones: Condicion[] = [];

  tManejarFecha: ManejarFechas = new ManejarFechas();

  tGeneroActual: Genero = new Genero();
  tEstatusActual: Estatus = new Estatus();
  tOcupacionActual: Ocupacion = new Ocupacion();
  tSituacionActual: Situacion = new Situacion();
  tTipoPrecioActual: TiposOpcion = new TiposOpcion();
  tEstadocivilActual: EstadoCivil = new EstadoCivil();
  tTipoClienteActual: Tipo = new Tipo();
  tCondicionActual: Condicion = new Condicion();

  constructor(private tUsuarioService: UsuariosService) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {

    if ('tGeneros' in changes) {

      this.tGeneroActual = this.tGeneros[0] || new Genero();
      this.CambiarGenero();

    }

    if ('tEstatus' in changes) {

      this.tEstatusActual = this.tEstatus[0] || new Estatus();
      this.CambiarEstatus();

    }

    if ('tOcupaciones' in changes) {

      this.tOcupacionActual = this.tOcupaciones[0] || new Ocupacion();
      this.CambiarOcupacion();

    }

    if ('tSituaciones' in changes) {

      this.tSituacionActual = this.tSituaciones[0] || new Situacion();
      this.CambiarSituacion();

    }

    if ('TiposPrecios' in changes) {

      this.tTipoPrecioActual = this.tTipoPrecioActual[0] || new TiposOpcion();
      this.CambiarTiposPrecios();

    }

    if ('tEstadociviles' in changes) {

      this.tEstadocivilActual = this.tEstadociviles[0] || new EstadoCivil();
      this.CambiarEstadoCivil();

    }

    if ('tTiposClientes' in changes) {

      this.tTipoClienteActual = this.tTiposClientes[0] || new Tipo();
      this.CambiarTipoCliente();

    }

    if ('tTiposClientes' in changes) {

      this.tTipoClienteActual = this.tTiposClientes[0] || new Tipo();
      this.CambiarTipoCliente();

    }

    if ('tCondiciones' in changes) {

      this.tCondicionActual = this.tCondiciones[0] || new Condicion();
      this.CambiarCondicion();

    }

    if ('tCliente' in changes) {

      this.tGeneroActual = this.tGeneros.find(tGenero => {
        return tGenero.Corporacion === this.tCliente.Corporacion &&
          tGenero.Empresa === this.tCliente.Empresa &&
          tGenero.Codgenero === this.tCliente.Codgenero
      }) || this.tGeneros[0] || new Genero();

      this.tEstatusActual = this.tEstatus.find(tEst => {
        return tEst.Corporacion === this.tCliente.Corporacion &&
          tEst.Empresa === this.tCliente.Empresa &&
          tEst.Codestatus === this.tCliente.Estatus
      }) || this.tEstatus[0] || new Estatus();

      this.tOcupacionActual = this.tOcupaciones.find(tOcupacion => {
        return tOcupacion.Corporacion === this.tCliente.Corporacion &&
          tOcupacion.Empresa === this.tCliente.Empresa &&
          tOcupacion.Codocupacion === this.tCliente.Codocupacion
      }) || this.tOcupaciones[0] || new Ocupacion();

      this.tSituacionActual = this.tSituaciones.find(tSituacion => {
        return tSituacion.Corporacion === this.tCliente.Corporacion &&
          tSituacion.Empresa === this.tCliente.Empresa &&
          tSituacion.Codsituacion === this.tCliente.Codsituacion
      }) || this.tSituaciones[0] || new Situacion();

      this.tTipoPrecioActual = this.tTiposPrecios.find(tTipoPrecio => {
        return tTipoPrecio.Codigo == this.tCliente.Tipoprecio
      }) || this.tTiposPrecios[0] || new TiposOpcion();

      this.tEstadocivilActual = this.tEstadociviles.find(tEstadoCivil => {
        return tEstadoCivil.Corporacion == this.tCliente.Corporacion &&
          tEstadoCivil.Empresa == this.tCliente.Empresa &&
          tEstadoCivil.Codestadocivil == this.tCliente.Codestadocivil
      }) || this.tEstadociviles[0] || new EstadoCivil();

      this.tTipoClienteActual = this.tTiposClientes.find(tTipoCliente => {
        return tTipoCliente.Codtipo == this.tCliente.Tipocliente
      }) || this.tTiposClientes[0] || new Tipo();

      this.tCondicionActual = this.tCondiciones.find(tCondicion => {
        return tCondicion.Corporacion == this.tCliente.Corporacion &&
          tCondicion.Empresa == this.tCliente.Empresa &&
          tCondicion.Codcondicion == this.tCliente.Codcondicion
      }) || this.tCondiciones[0] || new Condicion();

      if (this.tCliente.Codcliente === '') {

        this.CambiarGenero();
        this.CambiarEstatus();
        this.CambiarOcupacion();
        this.CambiarSituacion();
        this.CambiarTiposPrecios();
        this.CambiarEstadoCivil();
        this.CambiarTipoCliente();
        this.CambiarCondicion();

      }

    }

  }

  CambiarGenero() {

    this.tCliente.Codgenero = this.tGeneroActual.Codgenero;

  }

  CambiarEstatus() {
    this.tCliente.Estatus = this.tEstatusActual.Codestatus;
  }

  CambiarOcupacion() {
    this.tCliente.Codocupacion = this.tOcupacionActual.Codocupacion;
  }

  CambiarSituacion() {
    this.tCliente.Codsituacion = this.tSituacionActual.Codsituacion;
  }

  CambiarTiposPrecios() {
    this.tCliente.Tipoprecio = this.tTipoPrecioActual.Codigo;
  }

  CambiarEstadoCivil() {
    this.tCliente.Codestadocivil = this.tEstadocivilActual.Codestadocivil;
  }

  CambiarTipoCliente() {
    this.tCliente.Tipocliente = this.tTipoClienteActual.Codtipo;
  }

  CambiarCondicion() {
    this.tCliente.Codcondicion = this.tCondicionActual.Codcondicion;
  }

  AbrirModalAsesor(): void {

    this.tCliente.Codasesor = "";
    this.tCliente.Descripcion_asesor = "";

    this.tUsuarioService.AbrirModal().then(result => {

      if (result.Resultado == "S") {

        var tUsuarioModal: Usuario = result.Usuario;
        this.tCliente.Codasesor = tUsuarioModal.Codusuario;
        this.tCliente.Descripcion_asesor = tUsuarioModal.Descripcion;

      }

    }, reason => {



    });

  }

  Seleccionar_Fecha(tFecha: any): void {


    this.tCliente.Fecha_nacimientol = this.tManejarFecha.Seleccionar_Fecha(tFecha);

  };

}
