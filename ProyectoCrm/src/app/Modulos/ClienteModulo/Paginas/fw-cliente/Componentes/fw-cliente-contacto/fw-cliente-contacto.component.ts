import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ClienteTelefonos, Cliente, Empresas, Usuario } from 'src/app/Clases/Estructura';
import { ClienteTelefonoEntrada } from 'src/app/Clases/EstructuraEntrada';

@Component({
  selector: 'app-fw-cliente-contacto',
  templateUrl: './fw-cliente-contacto.component.html',
  styleUrls: ['./fw-cliente-contacto.component.scss']
})
export class FwClienteContactoComponent implements OnInit {

  @Input('Cliente') tCliente: Cliente = new Cliente();
  @Input('Empresa') tEmpresa: Empresas = new Empresas();
  @Input('Usuario') tUsuario: Usuario = new Usuario();

  @Output('EliminarTelefono') tEliminarTelefono: EventEmitter<{tTelefono: ClienteTelefonoEntrada, tPosicion: number}> = new EventEmitter();

  tClienteTelefonoActual: ClienteTelefonos = new ClienteTelefonos();
  tClienteTelefonoNuevo: ClienteTelefonos = new ClienteTelefonos();

  tPosicion: number = 0;
  tPagina: number = 1;
  tTamanoPag: number = 4;

  constructor() { }

  ngOnInit(): void {
  }

  CargarTelefono() {

    if(this.tClienteTelefonoNuevo.Telefono === '') { return; }

    let tNuevoTelefono: ClienteTelefonos = {

      "Id": 0,
      "Corporacion": this.tEmpresa.Corporacion,
      "Empresa": this.tEmpresa.Empresa,
      "Codcliente": this.tCliente.Codcliente,
      "Telefono": this.tClienteTelefonoNuevo.Telefono,
      "Principal": 0,
      "Nuevo": "S",
      "Fecha_creacion": new Date(),
      "Fecha_creacionl": 0,
      "Fecha_modificacion": new Date(),
      "Fecha_modificacionl": 0,
      "Usuariocreador": this.tUsuario.Codusuario,
      "Usuariomodificador": this.tUsuario.Codusuario

    }

    this.tCliente.LTelefonos.push(tNuevoTelefono);
    this.tClienteTelefonoNuevo.Telefono = "";

  }

  TelefonoPrincipal(tTelefonoSel: ClienteTelefonos): void {

    this.tCliente.LTelefonos.map(tTelefonoFor => {

      if (tTelefonoSel.Corporacion === tTelefonoFor.Corporacion &&
        tTelefonoSel.Empresa === tTelefonoFor.Empresa &&
        tTelefonoSel.Codcliente === tTelefonoFor.Codcliente &&
        tTelefonoSel.Telefono === tTelefonoFor.Telefono) {

        tTelefonoFor.Principal = 1;

      }
      else {

        tTelefonoFor.Principal = 0;

      }

    })

  }

  EliminarTelefono(tTelefonoSel: ClienteTelefonos, tPosicion: number) {

    if (tTelefonoSel.Nuevo === "S") {

      this.tCliente.LTelefonos.splice(tPosicion + ((this.tPagina - 1) * this.tTamanoPag), 1);

    } else {

      let tTelefono: ClienteTelefonoEntrada = {

        "Id": 0,
        "Corporacion": tTelefonoSel.Corporacion,
        "Empresa": tTelefonoSel.Empresa,
        "Codcliente": tTelefonoSel.Codcliente,
        "Telefono": tTelefonoSel.Telefono,
        "Principal": tTelefonoSel.Principal,
        "Nuevo": tTelefonoSel.Nuevo,
        "Usuariocreador": tTelefonoSel.Usuariocreador,
        "Usuariomodificador": tTelefonoSel.Usuariomodificador

      }

      this.tEliminarTelefono.emit({tTelefono, tPosicion});

    }

  }

}
