import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwClienteContactoComponent } from './fw-cliente-contacto.component';

describe('FwClienteContactoComponent', () => {
  let component: FwClienteContactoComponent;
  let fixture: ComponentFixture<FwClienteContactoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwClienteContactoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwClienteContactoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
