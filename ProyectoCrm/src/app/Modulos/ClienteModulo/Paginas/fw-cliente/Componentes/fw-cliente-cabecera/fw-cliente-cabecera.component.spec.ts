import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwClienteCabeceraComponent } from './fw-cliente-cabecera.component';

describe('FwClienteCabeceraComponent', () => {
  let component: FwClienteCabeceraComponent;
  let fixture: ComponentFixture<FwClienteCabeceraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwClienteCabeceraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwClienteCabeceraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
