import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwClienteDocumentosComponent } from './fw-cliente-documentos.component';

describe('FwClienteDocumentosComponent', () => {
  let component: FwClienteDocumentosComponent;
  let fixture: ComponentFixture<FwClienteDocumentosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwClienteDocumentosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwClienteDocumentosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
