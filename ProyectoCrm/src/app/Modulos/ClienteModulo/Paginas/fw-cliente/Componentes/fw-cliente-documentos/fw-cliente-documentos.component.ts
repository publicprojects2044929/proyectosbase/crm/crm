import { Component, OnInit, Input, ViewChild, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { Tipo, Cliente, ClienteDocumentos, Empresas, Usuario, Configuracion } from 'src/app/Clases/Estructura';
import { ManejarImagenes } from 'src/app/Manejadores/cls-procedimientos';
import { ToastrService } from 'ngx-toastr';
import { ClienteDocumentosEntrada } from 'src/app/Clases/EstructuraEntrada';

@Component({
  selector: 'app-fw-cliente-documentos',
  templateUrl: './fw-cliente-documentos.component.html',
  styleUrls: ['./fw-cliente-documentos.component.scss']
})
export class FwClienteDocumentosComponent implements OnInit, OnChanges {

  @ViewChild('flArchivo', { static: true }) tControlArchivo: any;

  @Input('Cliente') tCliente: Cliente = new Cliente();
  @Input('Empresa') tEmpresa: Empresas = new Empresas();
  @Input('TiposDocumentos') tTiposDocumentos: Tipo[] = [];
  @Input('Usuario') tUsuario: Usuario = new Usuario();
  @Input('Configuracion') tConfiguracion: Configuracion = new Configuracion();
  @Input('ManejarImagen') tManejarImagen: ManejarImagenes = new ManejarImagenes();

  @Output('EliminarDocumento') tEliminarDocumento: EventEmitter<{tDocumento: ClienteDocumentosEntrada, tPosicion: number}> = new EventEmitter();

  tPosicionDocumento: number = 0;
  tPaginaDocumento: number = 1;
  tTamanoPagDocumento: number = 4;

  tTipoDocumentoActual: Tipo = new Tipo();
  tClienteDocumentoNuevo: ClienteDocumentos = new ClienteDocumentos();
  tClienteDocumentoActual: ClienteDocumentos = new ClienteDocumentos();

  constructor(private toastr: ToastrService) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges){

    if('tTiposDocumentos' in changes){

      this.tTipoDocumentoActual = this.tTiposDocumentos[0] || new Tipo();
      this.CambiarTipoDocumento();

    }

  }

  CargarDocumento() {

    let tNuevoDocumento: ClienteDocumentos = {

      "Id": 0,
      "Corporacion": this.tEmpresa.Corporacion,
      "Empresa": this.tEmpresa.Empresa,
      "Codcliente": this.tCliente.Codcliente,
      "Tipodocumento": this.tTipoDocumentoActual.Codtipo,
      "Documento": this.tClienteDocumentoNuevo.Documento,
      "Url": this.tManejarImagen.ObtenerImagen(),
      "Principal": 0,
      "Nuevo": "S",
      "Fecha_creacion": new Date(),
      "Fecha_creacionl": 0,
      "Fecha_modificacion": new Date(),
      "Fecha_modificacionl": 0,
      "Usuariocreador": this.tUsuario.Codusuario,
      "Usuariomodificador": this.tUsuario.Codusuario

    }

    let tExisteDoc: number = this.tCliente.LDocumentos.findIndex((tDocumento, tPosicion) => {

      return tDocumento.Corporacion === tNuevoDocumento.Corporacion &&
        tDocumento.Empresa === tNuevoDocumento.Empresa &&
        tDocumento.Tipodocumento === tNuevoDocumento.Tipodocumento &&
        tDocumento.Documento === tNuevoDocumento.Documento;

    });

    if (tExisteDoc >= 0) {

      this.toastr.clear();
      this.toastr.warning('El cliente ya tiene registrado este documento.');
      return;

    }

    this.tCliente.LDocumentos.push(tNuevoDocumento);
    this.tClienteDocumentoNuevo = new ClienteDocumentos();
    this.tTipoDocumentoActual = this.tTiposDocumentos[0] || new Tipo();
    this.tManejarImagen.tImagen = "";
    this.tControlArchivo.nativeElement.value = "";

  }

  BloquearImagen(): boolean {

    if (this.tConfiguracion.Valor === "S") {

      return false;

    }
    else {

      return true;

    }

  }

  EliminarDocumento(tDocumentoSel: ClienteDocumentos, tPosicion: number) {

    if (this.tClienteDocumentoActual.Corporacion === tDocumentoSel.Corporacion &&
      this.tClienteDocumentoActual.Empresa === tDocumentoSel.Empresa &&
      this.tClienteDocumentoActual.Tipodocumento === tDocumentoSel.Tipodocumento &&
      this.tClienteDocumentoActual.Documento === tDocumentoSel.Documento) {

      this.tClienteDocumentoActual = new ClienteDocumentos();

    }

    if (tDocumentoSel.Nuevo === 'S') {

      this.tCliente.LDocumentos.splice(tPosicion, 1);

    }
    else{

      let tDocumento: ClienteDocumentosEntrada = {

        "Id": 0,
        "Corporacion": tDocumentoSel.Corporacion,
        "Empresa": tDocumentoSel.Empresa,
        "Codcliente": tDocumentoSel.Codcliente,
        "Tipodocumento": tDocumentoSel.Tipodocumento,
        "Documento": tDocumentoSel.Documento,
        "Principal": tDocumentoSel.Principal,
        "Nuevo": tDocumentoSel.Nuevo,
        "Url": tDocumentoSel.Url,
        "Imagen64": this.tManejarImagen.obtenerImagenes64(tDocumentoSel.Url),
        "Usuariocreador": tDocumentoSel.Usuariocreador,
        "Usuariomodificador": tDocumentoSel.Usuariomodificador

      }

      this.tEliminarDocumento.emit({tDocumento, tPosicion});

    }

  }

  SeleccionarDocumento(tDocumentoSel: ClienteDocumentos) {

    this.tClienteDocumentoActual = tDocumentoSel;

  }

  DocumentoPrincipal(tDocumentoSel: ClienteDocumentos): void {

    this.tCliente.LDocumentos.map(tDocumentoFor => {

      if (tDocumentoSel.Corporacion == tDocumentoFor.Corporacion &&
        tDocumentoSel.Empresa == tDocumentoFor.Empresa &&
        tDocumentoSel.Codcliente == tDocumentoFor.Codcliente &&
        tDocumentoSel.Tipodocumento == tDocumentoFor.Tipodocumento &&
        tDocumentoSel.Documento == tDocumentoFor.Documento) {

        tDocumentoFor.Principal = 1;

      }
      else {

        tDocumentoFor.Principal = 0;

      }

    })

  }

  CambiarTipoDocumento(){

    this.tClienteDocumentoNuevo.Tipodocumento = this.tTipoDocumentoActual.Codtipo;

  }

}
