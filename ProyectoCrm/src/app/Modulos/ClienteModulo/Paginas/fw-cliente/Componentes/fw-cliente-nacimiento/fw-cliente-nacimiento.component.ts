import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Pais, Estado, Municipio, Ciudad, Cliente } from 'src/app/Clases/Estructura';
import { PaisesService } from 'src/app/Modales/pais/fw-modal-pais/paises.service';
import { EstadosService } from 'src/app/Modales/estado/fw-modal-estado/estados.service';
import { MunicipiosService } from 'src/app/Modales/municipio/fw-modal-municipio/municipios.service';
import { CiudadesService } from 'src/app/Modales/ciudad/fw-modal-ciudades/ciudades.service';
import { Marcador } from 'src/app/Clases/EstructuraMapa';

@Component({
  selector: 'app-fw-cliente-nacimiento',
  templateUrl: './fw-cliente-nacimiento.component.html',
  styleUrls: ['./fw-cliente-nacimiento.component.scss']
})
export class FwClienteNacimientoComponent implements OnInit, OnChanges {

  @Input('Cliente') tCliente: Cliente = new Cliente();
  @Input('Paises') tPaises: Pais[] = [];

  tLMarcadores: Marcador[] = [];

  tEstados: Estado[] = [];
  tMunicipios: Municipio[] = [];
  tCiudades: Ciudad[] = [];

  tPaisnacimientoActual: Pais = new Pais();
  tEstadonacimientoActual: Estado = new Estado();
  tMunicipionacimientoActual: Municipio = new Municipio();
  tCiudadnacimientoActual: Ciudad = new Ciudad();

  constructor(private tPaisService: PaisesService,
    private tEstadoService: EstadosService,
    private tMunicipioService: MunicipiosService,
    private tCiudadService: CiudadesService) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {

    if ('tPaises' in changes) {

      this.tPaisnacimientoActual = this.tPaises[0] || new Pais();
      this.Seleccionar_Pais_Nacimiento();

    }

    if ('tCliente' in changes) {

      this.tLMarcadores = [];

      let tMarcador: Marcador = new Marcador();
      tMarcador.Latitud = this.tCliente.Latitud;
      tMarcador.Longitud = this.tCliente.Longitud;
      this.tLMarcadores = [...this.tLMarcadores, tMarcador];

      this.tPaisnacimientoActual = this.tPaises.find(tPais => {
        return tPais.Corporacion === this.tCliente.Corporacion &&
          tPais.Empresa === this.tCliente.Empresa &&
          tPais.Codpais === this.tCliente.Codpais
      }) || this.tPaises[0] || new Pais();

      this.tEstados = this.tPaisnacimientoActual.LEstados || [];

      this.tEstadonacimientoActual = this.tEstados.find(tEstado => {
        return tEstado.Corporacion === this.tCliente.Corporacion &&
          tEstado.Empresa === this.tCliente.Empresa &&
          tEstado.Codpais === this.tCliente.Codpais &&
          tEstado.Codestado === this.tCliente.Codestado
      }) || this.tEstados[0] || new Estado();

      this.tMunicipios = this.tEstadonacimientoActual.LMunicipio || [];

      this.tMunicipionacimientoActual = this.tMunicipios.find(tMunicipio => {
        return tMunicipio.Corporacion === this.tCliente.Corporacion &&
          tMunicipio.Empresa === this.tCliente.Empresa &&
          tMunicipio.Codpais === this.tCliente.Codpais &&
          tMunicipio.Codestado === this.tCliente.Codestado &&
          tMunicipio.Codmunicipio === this.tCliente.Codmunicipio
      }) || this.tMunicipios[0] || new Municipio();

      this.tCiudades = this.tMunicipionacimientoActual.LCiudad || [];

      this.tCiudadnacimientoActual = this.tCiudades.find(tCiudad => {
        return tCiudad.Corporacion === this.tCliente.Corporacion &&
          tCiudad.Empresa === this.tCliente.Empresa &&
          tCiudad.Codpais === this.tCliente.Codpais &&
          tCiudad.Codestado === this.tCliente.Codestado &&
          tCiudad.Codmunicipio === this.tCliente.Codmunicipio &&
          tCiudad.Codciudad === this.tCliente.Codciudad
      }) || this.tCiudades[0] || new Ciudad();

      if (this.tCliente.Codcliente === '') {

        this.Seleccionar_Pais_Nacimiento();

      }


    }

  }

  AbrirModalPais(): void {

    this.tPaisService
      .AbrirModal()
      .then(result => {

        if (result.Resultado == "S") {

          var tPaisModal: Pais = result.Pais;
          this.tPaisnacimientoActual = this.tPaises.find(tPais => {
            return tPais.Corporacion === tPaisModal.Corporacion &&
              tPais.Empresa === tPaisModal.Empresa &&
              tPais.Codpais === tPaisModal.Codpais
          }) || this.tPaises[0] || new Pais();

          this.Seleccionar_Pais_Nacimiento();

        }

      }, reason => {



      });

  }

  AbrirModalEstado(): void {

    this.tEstadoService
      .AbrirModal(this.tPaisnacimientoActual)
      .then(result => {

        if (result.Resultado == "S") {

          var tEstadoModal: Estado = result.Estado;
          this.tEstadonacimientoActual = this.tEstados.find(tEstado => {
            return tEstado.Corporacion === tEstadoModal.Corporacion &&
              tEstado.Empresa === tEstadoModal.Empresa &&
              tEstado.Codpais === tEstadoModal.Codpais &&
              tEstado.Codestado === tEstadoModal.Codestado
          }) || this.tEstados[0] || new Estado();

          this.Seleccionar_Estado_Nacimiento();

        }

      }, reason => {



      });

  }

  AbrirModalMunicipio(): void {

    this.tMunicipioService
      .AbrirModal(this.tEstadonacimientoActual)
      .then(result => {

        if (result.Resultado == "S") {

          var tMunicipioModal: Municipio = result.Municipio;
          this.tMunicipionacimientoActual = this.tMunicipios.find(tMunicipio => {
            return tMunicipio.Corporacion === tMunicipioModal.Corporacion &&
              tMunicipio.Empresa === tMunicipioModal.Empresa &&
              tMunicipio.Codpais === tMunicipioModal.Codpais &&
              tMunicipio.Codestado === tMunicipioModal.Codestado &&
              tMunicipio.Codmunicipio === tMunicipioModal.Codmunicipio
          }) || this.tMunicipios[0] || new Municipio();

          this.Seleccionar_Municipio_Nacimiento();

        }

      }, reason => {



      });

  }

  AbrirModalCiudad(): void {

    this.tCiudadService
      .AbrirModal(this.tMunicipionacimientoActual)
      .then(result => {

        if (result.Resultado == "S") {

          var tCiudadModal: Ciudad = result.Ciudad;
          this.tCiudadnacimientoActual = this.tCiudades.find(tCiudad => {
            return tCiudad.Corporacion === tCiudadModal.Corporacion &&
              tCiudad.Empresa === tCiudadModal.Empresa &&
              tCiudad.Codpais === tCiudadModal.Codpais &&
              tCiudad.Codestado === tCiudadModal.Codestado &&
              tCiudad.Codmunicipio === tCiudadModal.Codmunicipio
          }) || this.tCiudades[0] || new Ciudad();

          this.Seleccionar_Ciudad_Nacimiento();

        }

      }, reason => {



      });

  }

  Seleccionar_Pais_Nacimiento() {

    this.tEstados = this.tPaisnacimientoActual.LEstados || [];
    this.tEstadonacimientoActual = new Estado();
    this.tMunicipionacimientoActual = new Municipio();
    this.tCiudadnacimientoActual = new Ciudad();

    this.tEstadonacimientoActual = this.tEstados[0] || new Estado();

    this.Seleccionar_Estado_Nacimiento();

  }

  Seleccionar_Estado_Nacimiento() {

    this.tMunicipios = this.tEstadonacimientoActual.LMunicipio || [];
    this.tMunicipionacimientoActual = new Municipio();
    this.tCiudades = this.tMunicipionacimientoActual.LCiudad || [];
    this.tCiudadnacimientoActual = new Ciudad();
    this.tMunicipionacimientoActual = this.tMunicipios[0] || new Municipio();
    this.tCiudades = this.tMunicipionacimientoActual.LCiudad || [];
    this.tCiudadnacimientoActual = this.tCiudades[0] || new Ciudad();

    this.Seleccionar_Municipio_Nacimiento();

  };

  Seleccionar_Municipio_Nacimiento() {

    this.tCiudades = this.tMunicipionacimientoActual.LCiudad || [];
    this.tCiudadnacimientoActual = new Ciudad();
    this.tCiudadnacimientoActual = this.tCiudades[0] || new Ciudad();
    this.Seleccionar_Ciudad_Nacimiento();

  };

  Seleccionar_Ciudad_Nacimiento() {

    this.tCliente.Codpais = this.tCiudadnacimientoActual.Codpais;
    this.tCliente.Codestado = this.tCiudadnacimientoActual.Codestado;
    this.tCliente.Codmunicipio = this.tCiudadnacimientoActual.Codmunicipio;
    this.tCliente.Codciudad = this.tCiudadnacimientoActual.Codciudad;

  };

}
