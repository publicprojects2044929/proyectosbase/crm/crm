import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Cliente } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-cliente-cabecera',
  templateUrl: './fw-cliente-cabecera.component.html',
  styleUrls: ['./fw-cliente-cabecera.component.scss']
})
export class FwClienteCabeceraComponent implements OnInit {

  @Input('Cliente') tCliente: Cliente = new Cliente();
  @Input('BloquearBuscar') tBloquearBuscar: boolean = false;
  @Input('Editar') tEditar: boolean = false;

  @Output('Buscar') tBuscar: EventEmitter<string> = new EventEmitter();
  @Output('AbrirModal') tAbrirModal: EventEmitter<string> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  Buscar() {

    if (this.tCliente.Codcliente === '') { return; }

    this.tBuscar.emit(this.tCliente.Codcliente);

  }

  AbrirModal() {
    this.tAbrirModal.emit('');
  }

}
