import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwClienteNacimientoComponent } from './fw-cliente-nacimiento.component';

describe('FwClienteNacimientoComponent', () => {
  let component: FwClienteNacimientoComponent;
  let fixture: ComponentFixture<FwClienteNacimientoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwClienteNacimientoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwClienteNacimientoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
