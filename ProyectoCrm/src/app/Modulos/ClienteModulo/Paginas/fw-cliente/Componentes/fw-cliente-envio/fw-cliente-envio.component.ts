import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Pais, Estado, Municipio, Ciudad, Cliente } from 'src/app/Clases/Estructura';
import { PaisesService } from 'src/app/Modales/pais/fw-modal-pais/paises.service';
import { EstadosService } from 'src/app/Modales/estado/fw-modal-estado/estados.service';
import { MunicipiosService } from 'src/app/Modales/municipio/fw-modal-municipio/municipios.service';
import { CiudadesService } from 'src/app/Modales/ciudad/fw-modal-ciudades/ciudades.service';

@Component({
  selector: 'app-fw-cliente-envio',
  templateUrl: './fw-cliente-envio.component.html',
  styleUrls: ['./fw-cliente-envio.component.scss']
})
export class FwClienteEnvioComponent implements OnInit {

  @Input('Cliente') tCliente: Cliente = new Cliente();
  @Input('Paises') tPaises: Pais[] = [];

  tEstados: Estado[] = [];
  tMunicipios: Municipio[] = [];
  tCiudades: Ciudad[] = [];

  tPaisenvioActual: Pais = new Pais;
  tEstadoenvioActual: Estado = new Estado;
  tMunicipioenvioActual: Municipio = new Municipio;
  tCiudadenvioActual: Ciudad = new Ciudad;

  constructor(private tPaisService: PaisesService,
    private tEstadoService: EstadosService,
    private tMunicipioService: MunicipiosService,
    private tCiudadService: CiudadesService) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {

    if ('tPaises' in changes) {

      this.tPaisenvioActual = this.tPaises[0] || new Pais();
      this.Seleccionar_Pais_Envio();

    }

    if ('tCliente' in changes) {

      this.tPaisenvioActual = this.tPaises.find(tPais => {
        return tPais.Corporacion === this.tCliente.Corporacion &&
          tPais.Empresa === this.tCliente.Empresa &&
          tPais.Codpais === this.tCliente.Codpais_envio
      }) || this.tPaises[0] || new Pais();

      this.tEstados = this.tPaisenvioActual.LEstados || [];

      this.tEstadoenvioActual = this.tEstados.find(tEstado => {
        return tEstado.Corporacion === this.tCliente.Corporacion &&
          tEstado.Empresa === this.tCliente.Empresa &&
          tEstado.Codpais === this.tCliente.Codpais_envio &&
          tEstado.Codestado === this.tCliente.Codestado_envio
      }) || this.tEstados[0] || new Estado();

      this.tMunicipios = this.tEstadoenvioActual.LMunicipio || [];

      this.tMunicipioenvioActual = this.tMunicipios.find(tMunicipio => {
        return tMunicipio.Corporacion === this.tCliente.Corporacion &&
          tMunicipio.Empresa === this.tCliente.Empresa &&
          tMunicipio.Codpais === this.tCliente.Codpais_envio &&
          tMunicipio.Codestado === this.tCliente.Codestado_envio &&
          tMunicipio.Codmunicipio === this.tCliente.Codmunicipio_envio
      }) || this.tMunicipios[0] || new Municipio();

      this.tCiudades = this.tMunicipioenvioActual.LCiudad || [];

      this.tCiudadenvioActual = this.tCiudades.find(tCiudad => {
        return tCiudad.Corporacion === this.tCliente.Corporacion &&
          tCiudad.Empresa === this.tCliente.Empresa &&
          tCiudad.Codpais === this.tCliente.Codpais_envio &&
          tCiudad.Codestado === this.tCliente.Codestado_envio &&
          tCiudad.Codmunicipio === this.tCliente.Codmunicipio_envio
      }) || this.tCiudades[0] || new Ciudad();

      if (this.tCliente.Codcliente === '') {

        this.Seleccionar_Pais_Envio();

      }

    }

  }

  AbrirModalPais(): void {

    this.tPaisService
      .AbrirModal()
      .then(result => {

        if (result.Resultado == "S") {

          var tPaisModal: Pais = result.Pais;
          this.tPaisenvioActual = this.tPaises.find(tPais => {
            return tPais.Corporacion === tPaisModal.Corporacion &&
              tPais.Empresa === tPaisModal.Empresa &&
              tPais.Codpais === tPaisModal.Codpais
          }) || this.tPaises[0] || new Pais();

          this.Seleccionar_Pais_Envio();

        }

      }, reason => {



      });

  }

  AbrirModalEstado(): void {

    this.tEstadoService
      .AbrirModal(this.tPaisenvioActual)
      .then(result => {

        if (result.Resultado == "S") {

          var tEstadoModal: Estado = result.Estado;
          this.tEstadoenvioActual = this.tEstados.find(tEstado => {
            return tEstado.Corporacion === tEstadoModal.Corporacion &&
              tEstado.Empresa === tEstadoModal.Empresa &&
              tEstado.Codpais === tEstadoModal.Codpais &&
              tEstado.Codestado === tEstadoModal.Codestado
          }) || this.tEstados[0] || new Estado();

          this.Seleccionar_Estado_Envio();

        }

      }, reason => {



      });

  }

  AbrirModalMunicipio(): void {

    this.tMunicipioService
      .AbrirModal(this.tEstadoenvioActual)
      .then(result => {

        if (result.Resultado == "S") {

          var tMunicipioModal: Municipio = result.Municipio;
          this.tMunicipioenvioActual = this.tMunicipios.find(tMunicipio => {
            return tMunicipio.Corporacion === tMunicipioModal.Corporacion &&
              tMunicipio.Empresa === tMunicipioModal.Empresa &&
              tMunicipio.Codpais === tMunicipioModal.Codpais &&
              tMunicipio.Codestado === tMunicipioModal.Codestado &&
              tMunicipio.Codmunicipio === tMunicipioModal.Codmunicipio
          }) || this.tMunicipios[0] || new Municipio();

          this.Seleccionar_Municipio_Envio();

        }

      }, reason => {



      });

  }

  AbrirModalCiudad(): void {

    this.tCiudadService
      .AbrirModal(this.tMunicipioenvioActual)
      .then(result => {

        if (result.Resultado == "S") {

          var tCiudadModal: Ciudad = result.Ciudad;
          this.tCiudadenvioActual = this.tCiudades.find(tCiudad => {
            return tCiudad.Corporacion === tCiudadModal.Corporacion &&
              tCiudad.Empresa === tCiudadModal.Empresa &&
              tCiudad.Codpais === tCiudadModal.Codpais &&
              tCiudad.Codestado === tCiudadModal.Codestado &&
              tCiudad.Codmunicipio === tCiudadModal.Codmunicipio
          }) || this.tCiudades[0] || new Ciudad();

          this.Seleccionar_Ciudad_Envio();

        }

      }, reason => {



      });

  }

  Seleccionar_Pais_Envio() {

    this.tEstados = this.tPaisenvioActual.LEstados || [];
    this.tEstadoenvioActual = new Estado();
    this.tMunicipioenvioActual = new Municipio();
    this.tCiudadenvioActual = new Ciudad();

    this.tEstadoenvioActual = this.tEstados[0] || new Estado();

    this.Seleccionar_Estado_Envio();

  }

  Seleccionar_Estado_Envio() {

    this.tMunicipios = this.tEstadoenvioActual.LMunicipio || [];
    this.tMunicipioenvioActual = new Municipio();
    this.tCiudades = this.tMunicipioenvioActual.LCiudad || [];
    this.tCiudadenvioActual = new Ciudad();
    this.tMunicipioenvioActual = this.tMunicipios[0] || new Municipio();
    this.tCiudades = this.tMunicipioenvioActual.LCiudad || [];
    this.tCiudadenvioActual = this.tCiudades[0] || new Ciudad();

    this.Seleccionar_Municipio_Envio();

  };

  Seleccionar_Municipio_Envio() {

    this.tCiudades = this.tMunicipioenvioActual.LCiudad || [];
    this.tCiudadenvioActual = new Ciudad();
    this.tCiudadenvioActual = this.tCiudades[0] || new Ciudad();
    this.Seleccionar_Ciudad_Envio();

  };

  Seleccionar_Ciudad_Envio() {

    this.tCliente.Codpais_envio = this.tCiudadenvioActual.Codpais;
    this.tCliente.Codestado_envio = this.tCiudadenvioActual.Codestado;
    this.tCliente.Codmunicipio_envio = this.tCiudadenvioActual.Codmunicipio;
    this.tCliente.Codciudad_envio = this.tCiudadenvioActual.Codciudad;

  };

}
