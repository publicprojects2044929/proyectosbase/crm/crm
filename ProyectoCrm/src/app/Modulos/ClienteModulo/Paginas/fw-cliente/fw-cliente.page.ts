import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/Core/Session/session.service';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { Filtros, Empresas, Usuario, Formulario, Estatus, Tipo, Cliente, Genero, Ocupacion, EstadoCivil, Pais, Condicion, Situacion, TiposOpcion, Configuracion } from 'src/app/Clases/Estructura';
import { ClienteEntrada, ClienteTelefonoEntrada, ClienteDocumentosEntrada } from 'src/app/Clases/EstructuraEntrada';
import { ManejarImagenes } from 'src/app/Manejadores/cls-procedimientos';
import { FormularioConfiguracion } from 'src/app/Clases/EstructuraConfiguracion';
import { ToastrService } from 'ngx-toastr';
import { ErroresService } from 'src/app/Modales/mensajes/fw-modal-msj/errores.service';
import { MensajesService } from 'src/app/Modales/mensajes/fw-modal-msj/mensajes.service';
import { ConfirmacionService } from 'src/app/Modales/confirmacion/fw-modal-confirmacion/confirmacion.service';
import { ClientesService } from 'src/app/Modales/cliente/fw-modal-clientes/clientes.service'
import { environment } from 'src/environments/environment';
import { Subscription } from 'rxjs';
import { ConfiguracionService } from 'src/app/Core/Configuracion/configuracion.service';

@Component({
  selector: 'app-fw-cliente',
  templateUrl: './fw-cliente.page.html',
  styleUrls: ['./fw-cliente.page.scss']
})
export class FwClientePage implements OnInit, OnDestroy {

  tManejarImagen: ManejarImagenes = new ManejarImagenes();
  ConfiguracionFormulario: FormularioConfiguracion = new FormularioConfiguracion();
  tCodFormularioActual: string = "fCliente";
  tPermisoAgregar: boolean = false;
  tPermisoEditar: boolean = false;
  tPermisoEliminar: boolean = false;
  tHabilitarGuardar: boolean = false;
  tHabilitarEliminar: boolean = false;

  tUsuarioActual: Usuario = new Usuario();
  tUsuario$: Subscription;
  tEmpresaActual: Empresas = new Empresas();
  tEmpresa$: Subscription;

  tZoom: number = 20;

  tTitulo: string = "";
  tNuevo: boolean = true;
  tEditar: boolean = false
  tBloquearBuscar: boolean = false;

  tClienteActual: Cliente = new Cliente;

  tFormularios: Formulario[] = [];
  tEstatus: Estatus[] = [];

  tGeneros: Genero[] = [];
  tOcupaciones: Ocupacion[] = [];
  tEstadociviles: EstadoCivil[] = [];

  tPaises: Pais[] = [];

  tTipos: Tipo[] = [];

  tTiposClientes: Tipo[] = [];
  tCondiciones: Condicion[] = [];
  tSituaciones: Situacion[] = [];

  tTiposDocumentos: Tipo[] = [];

  tUsuarios: Usuario[] = [];
  tUsuario: Usuario = new Usuario;

  tTiposPrecios: TiposOpcion[] = [];

  tConfiguracion: Configuracion = new Configuracion();

  tFiltros: Filtros[] = [];
  tFiltroActual: Filtros = new Filtros;
  tFiltroBusqueda: string = "";
  nombrePattern: any = /^[a-z0-9A-ZñÑ\,\áàéèíìóòúùý\s()\-\&]*$/;
  emailPattern: any = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  tConfiguracion$: Subscription;
  tHayConfiguracionWs$: Subscription;
  tStrConfiguracion: string = 'Cliente/Cliente.Config.json';

  constructor(public router: Router,
    public Ws: ClsServiciosService,
    private tSesion: SessionService,
    private tConfiguracionService: ConfiguracionService,
    private toastr: ToastrService,
    private tClienteService: ClientesService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService,
    private tConfServices: ConfirmacionService) {

    this.tFiltroBusqueda = "";

    this.tEstatus = [

      {
        "Id": 0,
        "Corporacion": "",
        "Empresa": "",
        "Codestatus": "",
        "Descripcion": "",
        "Secuencia": 0,
        "Tabla": "",
        "Hexadecimal": "",
        "Fecha_creacion": new Date(),
        "Fecha_creacionl": 0,
        "Fecha_modificacion": new Date(),
        "Fecha_modificacionl": 0,
        "Usuariocreador": "",
        "Usuariomodificador": ""
      }

    ];

    this.tTipos = [

      {
        "Id": 0,
        "Corporacion": "",
        "Empresa": "",
        "Codtipo": "",
        "Descripcion": "",
        "Tabla": "",
        "Columna": "",
        "Abreviacion": "",
        "Hexadecimal": "",
        "Fecha_creacion": new Date(),
        "Fecha_creacionl": 0,
        "Fecha_modificacion": new Date(),
        "Fecha_modificacionl": 0,
        "Usuariocreador": "",
        "Usuariomodificador": ""
      }
    ];

    this.tGeneros = [

      {
        "Id": 0,
        "Corporacion": "",
        "Empresa": "",
        "Codgenero": "",
        "Descripcion": "",
        "Cliente": 0,
        "Articulo": 0,
        "Fecha_creacion": new Date(),
        "Fecha_creacionl": 0,
        "Fecha_modificacion": new Date(),
        "Fecha_modificacionl": 0,
        "Usuariocreador": "",
        "Usuariomodificador": ""
      }
    ];

    this.tEstadociviles = [

      {
        "Id": 0,
        "Corporacion": "",
        "Empresa": "",
        "Codestadocivil": "",
        "Descripcion": "",
        "Fecha_creacion": new Date(),
        "Fecha_creacionl": 0,
        "Fecha_modificacion": new Date(),
        "Fecha_modificacionl": 0,
        "Usuariocreador": "",
        "Usuariomodificador": ""
      }
    ];

    this.tOcupaciones = [

      {
        "Id": 0,
        "Corporacion": "",
        "Empresa": "",
        "Codocupacion": "",
        "Descripcion": "",
        "Fecha_creacion": new Date(),
        "Fecha_creacionl": 0,
        "Fecha_modificacion": new Date(),
        "Fecha_modificacionl": 0,
        "Usuariocreador": "",
        "Usuariomodificador": ""
      }
    ];

    this.tSituaciones = [

      {
        "Id": 0,
        "Corporacion": "",
        "Empresa": "",
        "Codsituacion": "",
        "Descripcion": "",
        "Fecha_creacion": new Date(),
        "Fecha_creacionl": 0,
        "Fecha_modificacion": new Date(),
        "Fecha_modificacionl": 0,
        "Usuariocreador": "",
        "Usuariomodificador": ""
      }
    ];

    this.tCondiciones = [

      {
        "Id": 0,
        "Corporacion": "",
        "Empresa": "",
        "Codcondicion": "",
        "Descripcion": "",
        "Facturas_contado": "N",
        "Facturas_credito": "N",
        "Limite_credito": 0,
        "Limite_facturas_credito": 0,
        "Limite_facturas_vencidas": 0,
        "Dias_credito": 0,
        "Fecha_creacion": new Date(),
        "Fecha_creacionl": 0,
        "Fecha_modificacion": new Date(),
        "Fecha_modificacionl": 0,
        "Usuariocreador": "",
        "Usuariomodificador": ""
      }
    ];

    this.tTiposPrecios = [

      { Codigo: "PrecioLista", Descripcion: "Precio Lista" },
      { Codigo: "Precio1", Descripcion: "Precio1" },
      { Codigo: "Precio2", Descripcion: "Precio2" },
      { Codigo: "Precio3", Descripcion: "Precio3" }

    ];

  }

  ngOnInit() {

    this.tConfiguracion$ = this.tConfiguracionService
      .Consumir_Obtener_Configuracion(this.tStrConfiguracion)
      .subscribe((tConfiguracion: FormularioConfiguracion) => {

        this.ConfiguracionFormulario = tConfiguracion;
        this.tTitulo = this.ConfiguracionFormulario.Titulo;
        this.tErroresServices.tTituloMsj = this.ConfiguracionFormulario.ModalMensaje.Titulo;
        this.tMsjServices.tTituloMsj = this.ConfiguracionFormulario.ModalMensaje.Titulo;
        this.tConfServices.tTituloConf = this.ConfiguracionFormulario.ModalConfirmacion.Titulo;

      });

    this.tEmpresa$ = this.tSesion.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    });

    this.tUsuario$ = this.tSesion.tUsuarioActual$.subscribe((tUsuario: Usuario) => {

      this.tUsuarioActual = tUsuario;

    });

    var tTienePermiso: boolean = this.tSesion.ObtenerPermisoAcceso(this.tCodFormularioActual);
    this.tPermisoAgregar = this.tSesion.ObtenerPermisoAgregar();
    this.tPermisoEditar = this.tSesion.ObtenerPermisoEditar();
    this.tPermisoEliminar = this.tSesion.ObtenerPermisoEliminar();

    if (tTienePermiso == true) {

      this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
        .subscribe((tHayConfig: boolean) => {

          if (tHayConfig === true) {

            this.BuscarConfiguracion();
            this.BuscarTipo();
            this.BuscarEstatus();
            this.BuscarGenero();
            this.BuscarAsesor();
            this.BuscarEstadosCivil();
            this.BuscarSituacion();
            this.BuscarCondicion();
            this.BuscarOcupacion();
            this.BuscarPaisEstadoMunicipio();
            this.Nuevo();

          }

        });

    }
    else {

      this.tMsjServices.AbrirModalMsj(environment.msjSinAcceso);
      this.router.navigate(['/Home']);

    }

  }

  ngOnDestroy() {

    this.tUsuario$.unsubscribe();
    this.tEmpresa$.unsubscribe();
    this.tConfiguracion$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();

  }

  //#region "Funciones de busqueda"

  AbrirModalClientes(): void {

    this.tClienteService
      .AbrirModal()
      .then(result => {

        if (result.Resultado == "S") {

          var tClienteModal: Cliente = result.Cliente;
          this.BuscarClientes(tClienteModal.Codcliente);

        }

      }, reason => {



      });

  }

  BuscarEstatus(): void {

    this.Ws
      .Estatus()
      .Consumir_Obtener_Estatus(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        'MR_CLIENTE')
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tEstatus = Respuesta.Datos;

        }


      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  BuscarGenero(): void {

    this.Ws
      .Genero()
      .Consumir_Obtener_GeneroPorCliente(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        1)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.tGeneros = Respuesta.Datos;

        }


      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  BuscarOcupacion(): void {

    this.Ws
      .Ocupacion()
      .Consumir_Obtener_Ocupacion(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tOcupaciones = Respuesta.Datos;

        }


      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  BuscarEstadosCivil(): void {

    this.Ws
      .EstadoCivil()
      .Consumir_ObtenerEstadosCivil(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.tEstadociviles = Respuesta.Datos;

        }


      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  BuscarSituacion(): void {

    this.Ws
      .Situacion()
      .Consumir_Obtener_Situacion(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.tSituaciones = Respuesta.Datos;

        }


      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  BuscarCondicion(): void {

    this.Ws
      .Condicion()
      .Consumir_Obtener_Condicion(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.tCondiciones = Respuesta.Datos;

        }


      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  BuscarAsesor(): void {

    this.Ws
      .Usuario()
      .Consumir_Obtener_UsuariNativosGrupo(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.tUsuarios = Respuesta.Datos;

        }


      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  BuscarPaisEstadoMunicipio(): void {

    this.Ws
      .Paises()
      .Consumir_Obtener_PaisFull(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa)
      .subscribe(Respuesta => {

        this.tPaises = [];

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.tPaises = Respuesta.Datos;

        }

      }, error => {


        this.tErroresServices.MostrarError(error);

      });

  }

  BuscarTipo(): void {


    this.Ws
      .Tipo()
      .Consumir_Obtener_Tipo(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        'MR_CLIENTE')
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.tTipos = Respuesta.Datos;
          this.tTiposClientes = this.tTipos.filter(tTipoFor => tTipoFor.Columna === "TIPOCLIENTE") || [];
          this.tTiposDocumentos = this.tTipos.filter(tTipoFor => tTipoFor.Columna === "TIPODOCUMENTO") || [];

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  BuscarClientes(tCodcliente: string): void {

    this.Ws.Cliente()
      .Consumir_Obtener_ClientesDetallado(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        tCodcliente)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          var tCliente: Cliente = Respuesta.Datos[0];
          this.tEditar = true;
          this.tNuevo = false;
          this.tHabilitarGuardar = this.tPermisoEditar;
          this.tHabilitarEliminar = this.tPermisoEliminar;
          this.tBloquearBuscar = true;

          this.tClienteActual = tCliente;

          for (let tAsesor of this.tUsuarios) {

            if (tAsesor.Codusuario == this.tClienteActual.Codasesor) {

              this.tClienteActual.Descripcion_asesor = tAsesor.Descripcion;
              this.tClienteActual.Codasesor = tAsesor.Codusuario;
              break;
            }
          }

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  BuscarConfiguracion(): void {

    this.Ws
      .Configuraciones()
      .Consumir_Obtener_Configuracion(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        'MR_CLIENTE',
        'IMAGEN')
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          // this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tConfiguracion = Respuesta.Datos[0];

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  //#region "CRUD"

  Nuevo_Cliente(): void {

    var tNuevoCliente: ClienteEntrada;
    tNuevoCliente = {
      "Id": 0,
      "Corporacion": this.tEmpresaActual.Corporacion,
      "Empresa": this.tEmpresaActual.Empresa,
      "Codcliente": "",
      "Descripcion": this.tClienteActual.Descripcion,
      "Tipocliente": this.tClienteActual.Tipocliente,
      "Tipoprecio": this.tClienteActual.Tipoprecio,
      "Codocupacion": this.tClienteActual.Codocupacion,
      "Codsituacion": this.tClienteActual.Codsituacion,
      "Codcondicion": this.tClienteActual.Codcondicion,
      "Codasesor": this.tClienteActual.Codasesor != "" ? this.tClienteActual.Codasesor : this.tUsuarioActual.Codusuario,
      "Codgenero": this.tClienteActual.Codgenero,
      "Codestadocivil": this.tClienteActual.Codestadocivil,
      "Email": this.tClienteActual.Email,
      "Usuario": this.tClienteActual.Usuario,
      "Clave": this.tClienteActual.Clave,
      "Codpais": this.tClienteActual.Codpais,
      "Codestado": this.tClienteActual.Codestado,
      "Codmunicipio": this.tClienteActual.Codmunicipio,
      "Codciudad": this.tClienteActual.Codciudad,
      "Direccion": this.tClienteActual.Direccion,
      "Latitud": this.tClienteActual.Latitud,
      "Longitud": this.tClienteActual.Longitud,
      "Codpais_facturacion": this.tClienteActual.Codpais_facturacion,
      "Codestado_facturacion": this.tClienteActual.Codestado_facturacion,
      "Codmunicipio_facturacion": this.tClienteActual.Codmunicipio_facturacion,
      "Codciudad_facturacion": this.tClienteActual.Codciudad_facturacion,
      "Direccion_facturacion": this.tClienteActual.Direccion_facturacion,
      "Latitud_facturacion": this.tClienteActual.Latitud_facturacion,
      "Longitud_facturacion": this.tClienteActual.Longitud_facturacion,
      "Codpais_envio": this.tClienteActual.Codpais_envio,
      "Codestado_envio": this.tClienteActual.Codestado_envio,
      "Codmunicipio_envio": this.tClienteActual.Codmunicipio_envio,
      "Codciudad_envio": this.tClienteActual.Codciudad_envio,
      "Direccion_envio": this.tClienteActual.Direccion_envio,
      "Latitud_envio": this.tClienteActual.Latitud_envio,
      "Longitud_envio": this.tClienteActual.Longitud_envio,
      "Estatus": this.tClienteActual.Estatus,
      "Fecha_nacimientol": this.tClienteActual.Fecha_nacimientol,
      "Url": this.tClienteActual.Url,
      "Codgoogle": this.tClienteActual.Codgoogle,
      "Urlgoogle": this.tClienteActual.Urlgoogle,
      "Codfacebook": this.tClienteActual.Codfacebook,
      "Urlfacebook": this.tClienteActual.Urlfacebook,
      "LTelefonos": [],
      "LDocumentos": [],
      "Usuariocreador": this.tClienteActual.Usuariocreador,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }

    this.tClienteActual.LTelefonos.forEach(tTelefonofor => {

      let tNuevo: string = "N"

      if (tTelefonofor.Nuevo === "S") {

        tNuevo = "S"

      }

      let tNuevoClienteTelefonoEntrada: ClienteTelefonoEntrada = {

        "Id": 0,
        "Corporacion": this.tEmpresaActual.Corporacion,
        "Empresa": this.tEmpresaActual.Empresa,
        "Codcliente": this.tClienteActual.Codcliente,
        "Telefono": tTelefonofor.Telefono,
        "Principal": tTelefonofor.Principal,
        "Nuevo": tNuevo,
        "Usuariocreador": tTelefonofor.Usuariocreador,
        "Usuariomodificador": tTelefonofor.Usuariomodificador

      }

      tNuevoCliente.LTelefonos.push(tNuevoClienteTelefonoEntrada);

    });

    this.tClienteActual.LDocumentos.forEach(tDocumentoFor => {

      let tNuevo: string = "N"

      if (tDocumentoFor.Nuevo === "S") {

        tNuevo = "S"

      }

      let tNuevoClienteDocumentoEntrada: ClienteDocumentosEntrada = {

        "Id": 0,
        "Corporacion": this.tEmpresaActual.Corporacion,
        "Empresa": this.tEmpresaActual.Empresa,
        "Codcliente": this.tClienteActual.Codcliente,
        "Tipodocumento": tDocumentoFor.Tipodocumento,
        "Documento": tDocumentoFor.Documento,
        "Principal": tDocumentoFor.Principal,
        "Nuevo": tNuevo,
        "Url": tDocumentoFor.Url,
        "Imagen64": this.tManejarImagen.obtenerImagenes64(tDocumentoFor.Url),
        "Usuariocreador": tDocumentoFor.Usuariocreador,
        "Usuariomodificador": tDocumentoFor.Usuariomodificador

      }

      tNuevoCliente.LDocumentos.push(tNuevoClienteDocumentoEntrada);

    });

    this.Ws.Cliente()
      .Consumir_Crear_Cliente(tNuevoCliente)
      .subscribe(Respuesta => {


        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.toastr.success('El cliente se ha creado de manera exitosa!');
          this.tFiltroActual = this.tFiltros[0];

          this.Nuevo();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Modificar_Cliente(): void {

    var tModificarCliente: ClienteEntrada;
    tModificarCliente = {
      "Id": 0,
      "Corporacion": this.tClienteActual.Corporacion,
      "Empresa": this.tClienteActual.Empresa,
      "Codcliente": this.tClienteActual.Codcliente,
      "Descripcion": this.tClienteActual.Descripcion,
      "Tipocliente": this.tClienteActual.Tipocliente,
      "Tipoprecio": this.tClienteActual.Tipoprecio,
      "Codocupacion": this.tClienteActual.Codocupacion,
      "Codsituacion": this.tClienteActual.Codsituacion,
      "Codcondicion": this.tClienteActual.Codcondicion,
      "Codasesor": this.tClienteActual.Codasesor != "" ? this.tClienteActual.Codasesor : this.tUsuarioActual.Codusuario,
      "Codgenero": this.tClienteActual.Codgenero,
      "Codestadocivil": this.tClienteActual.Codestadocivil,
      "Email": this.tClienteActual.Email,
      "Usuario": this.tClienteActual.Usuario,
      "Clave": this.tClienteActual.Clave,
      "Codpais": this.tClienteActual.Codpais,
      "Codestado": this.tClienteActual.Codestado,
      "Codmunicipio": this.tClienteActual.Codmunicipio,
      "Codciudad": this.tClienteActual.Codciudad,
      "Direccion": this.tClienteActual.Direccion,
      "Latitud": this.tClienteActual.Latitud,
      "Longitud": this.tClienteActual.Longitud,
      "Codpais_facturacion": this.tClienteActual.Codpais_facturacion,
      "Codestado_facturacion": this.tClienteActual.Codestado_facturacion,
      "Codmunicipio_facturacion": this.tClienteActual.Codmunicipio_facturacion,
      "Codciudad_facturacion": this.tClienteActual.Codciudad_facturacion,
      "Direccion_facturacion": this.tClienteActual.Direccion_facturacion,
      "Latitud_facturacion": this.tClienteActual.Latitud_facturacion,
      "Longitud_facturacion": this.tClienteActual.Longitud_facturacion,
      "Codpais_envio": this.tClienteActual.Codpais_envio,
      "Codestado_envio": this.tClienteActual.Codestado_envio,
      "Codmunicipio_envio": this.tClienteActual.Codmunicipio_envio,
      "Codciudad_envio": this.tClienteActual.Codciudad_envio,
      "Direccion_envio": this.tClienteActual.Direccion_envio,
      "Latitud_envio": this.tClienteActual.Latitud_envio,
      "Longitud_envio": this.tClienteActual.Longitud_envio,
      "Estatus": this.tClienteActual.Estatus,
      "Fecha_nacimientol": this.tClienteActual.Fecha_nacimientol,
      "Url": this.tClienteActual.Url,
      "Codgoogle": this.tClienteActual.Codgoogle,
      "Urlgoogle": this.tClienteActual.Urlgoogle,
      "Codfacebook": this.tClienteActual.Codfacebook,
      "Urlfacebook": this.tClienteActual.Urlfacebook,
      "LTelefonos": [],
      "LDocumentos": [],
      "Usuariocreador": this.tClienteActual.Usuariocreador,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }

    this.tClienteActual.LTelefonos.forEach(tTelefonoFor => {

      let tModificaClienteTelefonoEntrada: ClienteTelefonoEntrada = {

        "Id": 0,
        "Corporacion": this.tEmpresaActual.Corporacion,
        "Empresa": this.tEmpresaActual.Empresa,
        "Codcliente": this.tClienteActual.Codcliente,
        "Telefono": tTelefonoFor.Telefono,
        "Nuevo": tTelefonoFor.Nuevo,
        "Principal": tTelefonoFor.Principal,
        "Usuariocreador": tTelefonoFor.Usuariocreador,
        "Usuariomodificador": tTelefonoFor.Usuariomodificador


      }

      tModificarCliente.LTelefonos.push(tModificaClienteTelefonoEntrada);

    });

    this.tClienteActual.LDocumentos.forEach(tDocumentoFor => {

      let tModificaClienteDocumentoEntrada: ClienteDocumentosEntrada = {

        "Id": 0,
        "Corporacion": this.tEmpresaActual.Corporacion,
        "Empresa": this.tEmpresaActual.Empresa,
        "Codcliente": this.tClienteActual.Codcliente,
        "Tipodocumento": tDocumentoFor.Tipodocumento,
        "Documento": tDocumentoFor.Documento,
        "Nuevo": tDocumentoFor.Nuevo,
        "Principal": tDocumentoFor.Principal,
        "Url": tDocumentoFor.Url,
        "Imagen64": this.tManejarImagen.obtenerImagenes64(tDocumentoFor.Url),
        "Usuariocreador": tDocumentoFor.Usuariocreador,
        "Usuariomodificador": tDocumentoFor.Usuariomodificador

      }

      tModificarCliente.LDocumentos.push(tModificaClienteDocumentoEntrada);

    });

    this.Ws.Cliente()
      .Consumir_Modificar_Cliente(tModificarCliente)
      .subscribe(Respuesta => {


        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.toastr.success('El cliente se ha modificado de manera exitosa!');
          this.tFiltroActual = this.tFiltros[0];
          this.Nuevo();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Eliminar(): void {

    this.tConfServices
      .AbrirModalConf(this.ConfiguracionFormulario.ModalConfirmacion.Eliminacion)
      .then(Resultado => {

        switch (Resultado) {
          case "S": {

            this.Eliminar_Cliente();
            break;

          }
        }

      });

  }

  Eliminar_Cliente(): void {

    var tEliminarCliente: ClienteEntrada;
    tEliminarCliente = {

      "Id": 0,
      "Corporacion": this.tClienteActual.Corporacion,
      "Empresa": this.tClienteActual.Empresa,
      "Codcliente": this.tClienteActual.Codcliente,
      "Descripcion": this.tClienteActual.Descripcion,
      "Tipocliente": this.tClienteActual.Tipocliente,
      "Tipoprecio": this.tClienteActual.Tipoprecio,
      "Codocupacion": this.tClienteActual.Codocupacion,
      "Codsituacion": this.tClienteActual.Codsituacion,
      "Codcondicion": this.tClienteActual.Codcondicion,
      "Codasesor": this.tClienteActual.Codasesor != "" ? this.tClienteActual.Codasesor : this.tUsuarioActual.Codusuario,
      "Codgenero": this.tClienteActual.Codgenero,
      "Codestadocivil": this.tClienteActual.Codestadocivil,
      "Email": this.tClienteActual.Email,
      "Usuario": this.tClienteActual.Usuario,
      "Clave": this.tClienteActual.Clave,
      "Codpais": this.tClienteActual.Codpais,
      "Codestado": this.tClienteActual.Codestado,
      "Codmunicipio": this.tClienteActual.Codmunicipio,
      "Codciudad": this.tClienteActual.Codciudad,
      "Direccion": this.tClienteActual.Direccion,
      "Latitud": this.tClienteActual.Latitud,
      "Longitud": this.tClienteActual.Longitud,
      "Codpais_facturacion": this.tClienteActual.Codpais_facturacion,
      "Codestado_facturacion": this.tClienteActual.Codestado_facturacion,
      "Codmunicipio_facturacion": this.tClienteActual.Codmunicipio_facturacion,
      "Codciudad_facturacion": this.tClienteActual.Codciudad_facturacion,
      "Direccion_facturacion": this.tClienteActual.Direccion_facturacion,
      "Latitud_facturacion": this.tClienteActual.Latitud_facturacion,
      "Longitud_facturacion": this.tClienteActual.Longitud_facturacion,
      "Codpais_envio": this.tClienteActual.Codpais_envio,
      "Codestado_envio": this.tClienteActual.Codestado_envio,
      "Codmunicipio_envio": this.tClienteActual.Codmunicipio_envio,
      "Codciudad_envio": this.tClienteActual.Codciudad_envio,
      "Direccion_envio": this.tClienteActual.Direccion_envio,
      "Latitud_envio": this.tClienteActual.Latitud_envio,
      "Longitud_envio": this.tClienteActual.Longitud_envio,
      "Estatus": this.tClienteActual.Estatus,
      "Fecha_nacimientol": this.tClienteActual.Fecha_nacimientol,
      "Url": this.tClienteActual.Url,
      "Codgoogle": this.tClienteActual.Codgoogle,
      "Urlgoogle": this.tClienteActual.Urlgoogle,
      "Codfacebook": this.tClienteActual.Codfacebook,
      "Urlfacebook": this.tClienteActual.Urlfacebook,
      "LTelefonos": [],
      "LDocumentos": [],
      "Usuariocreador": this.tClienteActual.Usuariocreador,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }


    this.Ws.Cliente()
      .Consumir_Eliminar_Cliente(tEliminarCliente)
      .subscribe(Respuesta => {


        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.toastr.success('El cliente se ha eliminado de manera exitosa!');
          this.Nuevo();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });


  }
  //#endregion

  EliminarTelefono({ tTelefono, tPosicion }) {

    this.Ws
      .ClientesTelf()
      .Consumir_EliminarCliente_Telefono(tTelefono)
      .subscribe(Respuesta => {


        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.tClienteActual.LTelefonos.splice(tPosicion, 1);

        }

      }, error => {


        this.tErroresServices.MostrarError(error);

      });

  }

  Cancelar(): void {

    this.tBloquearBuscar = false;
    this.Nuevo();

  }

  validarexpression(tEmail: string): boolean {
    let isValid: boolean = false;
    isValid = this.emailPattern.test(tEmail);
    return isValid
  }

  ValidarFormulario(): boolean {

    let valido: boolean = false;

    if (this.tClienteActual.Descripcion == "") {
      this.toastr.warning('Debe ingresar un Nombre!', 'Disculpe!')
      return valido
    }

    if (this.tClienteActual.Email == "") {
      this.toastr.warning('Debe ingresar un Email!', 'Disculpe!')
      return valido
    }

    if (this.validarexpression(this.tClienteActual.Email) == false) {
      this.toastr.error('Debe ingresar un Email vàlido', 'Disculpe!')
      return valido
    }

    if (this.tClienteActual.Direccion == "") {
      this.toastr.warning('Debe ingresar una Direcciòn personal!', 'Disculpe!')
      return valido
    }

    if (this.tClienteActual.Direccion_facturacion == "") {
      this.toastr.warning('Debe ingresar una Direcciòn para Facturaciòn!', 'Disculpe!')
      return valido
    }

    if (this.tClienteActual.Direccion_envio == "") {
      this.toastr.warning('Debe ingresar una Direcciòn de Envio!', 'Disculpe!')
      return valido
    }

    return true

  }

  Agregar(): void {

    if (this.ValidarFormulario() == false) {
      return
    }

    switch (this.tNuevo) {
      case true: {

        this.tConfServices
          .AbrirModalConf(this.ConfiguracionFormulario.ModalConfirmacion.Agregar)
          .then(Resultado => {

            switch (Resultado) {
              case "S": {

                this.Nuevo_Cliente();
                break;

              }
            }

          });
        break;

      }
      default: {

        this.tConfServices
          .AbrirModalConf(this.ConfiguracionFormulario.ModalConfirmacion.Edicion)
          .then(Resultado => {

            switch (Resultado) {
              case "S": {

                this.Modificar_Cliente();
                break;

              }
            }

          });
        break;

      }
    }

  }

  Nuevo(): void {

    // this.tControlArchivo.nativeElement.value = "";
    this.tHabilitarGuardar = this.tPermisoAgregar;
    this.tHabilitarEliminar = false;
    this.tBloquearBuscar = false;
    this.tClienteActual = new Cliente();
    this.tNuevo = true;
    this.tEditar = false;
    this.toastr.info('Por favor llene los datos necesarios para registrar un nuevo cliente!');
    /*this.tMensajeMsj = "Por favor llene los datos necesarios para registrar un nuevo cliente.";
    */

  }

  EliminarDocumento({ tDocumento, tPosicion }) {

    this.Ws
      .ClienteDocumentos()
      .Consumir_EliminarCliente_Documento(tDocumento)
      .subscribe(Respuesta => {


        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tClienteActual.LDocumentos.splice(tPosicion, 1);

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });


  }

}

