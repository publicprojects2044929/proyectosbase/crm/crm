import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwClientePage } from './fw-cliente.page';

describe('FwClientePage', () => {
  let component: FwClientePage;
  let fixture: ComponentFixture<FwClientePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwClientePage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwClientePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
