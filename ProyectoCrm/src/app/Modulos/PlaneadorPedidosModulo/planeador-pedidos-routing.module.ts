import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FwPlantillaComponent  } from 'src/app/Shared/Formularios/fw-plantilla/fw-plantilla.component'
import { FwPlaneadorPedidosComponent  } from './Formularios/fw-planeador-pedidos/fw-planeador-pedidos.component';

const routes: Routes = [
  {
    path: '',
    component: FwPlantillaComponent,
    children:[
      {
        path: '',
        component: FwPlaneadorPedidosComponent,
        data:{
          Codformulario: 'fPlaneadorPedidos'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlaneadorPedidosRoutingModule { }
