import { Component, OnInit, OnDestroy } from '@angular/core';
import { SessionService } from 'src/app/Core/Session/session.service';

import { Usuario, Empresas, Filtros, Pedido, Estatus, Tipo, Reportes } from 'src/app/Clases/Estructura';

import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { FormularioConfiguracion } from 'src/app/Clases/EstructuraConfiguracion';
import { ErroresService } from 'src/app/Modales/mensajes/fw-modal-msj/errores.service';
import { MensajesService } from 'src/app/Modales/mensajes/fw-modal-msj/mensajes.service';
import { environment } from 'src/environments/environment';
import { DetallesPedidoService } from 'src/app/Modales/detalles-pedido/fw-modal-detalles-pedido/detalles-pedido.service';
import { Router } from '@angular/router';
import { Subscription, pipe } from 'rxjs';
import { ManejarFechas, ManejarPdf, ManejarArchivo } from 'src/app/Manejadores/cls-procedimientos';
import { PieService } from 'src/app/Core/Graficos/Pie/pie.service';
import { BarrasService } from 'src/app/Core/Graficos/Barras/barras.service';
import { ConfiguracionService } from 'src/app/Core/Configuracion/configuracion.service';
import { Barra, DataSetBarra, Pie, DataSetPie } from 'src/app/Clases/EstructuraGrafico';

@Component({
  selector: 'app-fw-planeador-pedidos',
  templateUrl: './fw-planeador-pedidos.component.html',
  styleUrls: ['./fw-planeador-pedidos.component.scss']
})
export class FwPlaneadorPedidosComponent implements OnInit, OnDestroy {

  tTitulo: string = "Reporte de pedidos";
  ConfiguracionFormulario: FormularioConfiguracion = new FormularioConfiguracion;
  tCodFormularioActual: string = "fPlaneadorPedidos";
  tManejarPdf: ManejarPdf = new ManejarPdf();
  tManejarArchivos: ManejarArchivo = new ManejarArchivo();

  tUsuarioActual: Usuario = new Usuario();
  tEmpresaActual: Empresas = new Empresas();
  tEmpresa$: Subscription;

  tPosicion: number = 0;
  tPagina: number = 1;
  tTamanoPag: number = 10;

  tFiltros: Filtros[] = [];
  tFiltroActual: Filtros = new Filtros;
  tFiltroBusqueda: string = "";

  tPedidos: Pedido[] = [];

  tEstatus: Estatus[] = [];
  tEstatusActuales: Estatus[] = [];
  tTipos: Tipo[] = [];
  tTiposActuales: Tipo[] = [];

  tConfiguracionEstatus: {};
  tConfiguracionTipo: {};

  tFechasCreacion: Date[] = [];
  tFiltrarFechaC: boolean = true;
  tManejarFecha: ManejarFechas = new ManejarFechas();

  tElementoTipo: any;
  tTipoPie: boolean = true;
  tDatosBarrasTipo: Barra = new Barra();
  tDatosPieTipo: Pie = new Pie();

  tElementoEstatus: any;
  tEstatusPie: boolean = true;
  tDatosBarrasEst: Barra = new Barra();
  tDatosPieEsta: Pie = new Pie();

  tConfiguracion$: Subscription;
  tHayConfiguracionWs$: Subscription;
  tStrConfiguracion: string = 'PlaneadorPedidos/PlaneadorPedidos.Config.json';

  constructor(public router: Router,
    public Ws: ClsServiciosService,
    private tSesion: SessionService,
    private tConfiguracionService: ConfiguracionService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService,
    private tDetallesPedidoService: DetallesPedidoService,
    private tPieService: PieService,
    private tBarrasService: BarrasService) {

    this.tPosicion = 0;
    this.tBarrasService.Etiqueta = "Total";

    this.tFiltros = [

      { Codfiltro: "Codpedido", Filtro: "Pedido" },
      { Codfiltro: "Descripcion_cliente", Filtro: "Nombre Cliente" },
      { Codfiltro: "Codcliente", Filtro: "Cliente" }

    ]

    this.tConfiguracionEstatus = {
      singleSelection: false,
      idField: 'Codestatus',
      textField: 'Codestatus',
      selectAllText: 'Seleccionar todos',
      unSelectAllText: 'Deseleccionar todos',
      itemsShowLimit: 3,
      allowSearchFilter: true,
      searchPlaceholderText: "Buscar",
      noDataAvailablePlaceholderText: "Sin estatus registrados",
      closeDropDownOnSelection: true
    };

    this.tConfiguracionTipo = {
      singleSelection: false,
      idField: 'Codtipo',
      textField: 'Codtipo',
      selectAllText: 'Seleccionar todos',
      unSelectAllText: 'Deseleccionar todos',
      itemsShowLimit: 3,
      allowSearchFilter: true,
      searchPlaceholderText: "Buscar",
      noDataAvailablePlaceholderText: "Sin tipos registrados",
      closeDropDownOnSelection: true
    };

  }

  ngOnInit() {

    this.tFechasCreacion = [new Date(), new Date()];

    this.tConfiguracion$ = this.tConfiguracionService
      .Consumir_Obtener_Configuracion(this.tStrConfiguracion)
      .subscribe((tConfiguracion: FormularioConfiguracion) => {

        this.ConfiguracionFormulario = tConfiguracion;
        this.tTitulo = this.ConfiguracionFormulario.Titulo;

      });

    this.tEmpresa$ = this.tSesion.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;
      this.tBarrasService.Simbolo = this.tEmpresaActual.Simbolo;

    })

    var tTienePermiso: boolean = this.tSesion.ObtenerPermisoAcceso(this.tCodFormularioActual); //false;

    if (tTienePermiso == true) {

      this.tFiltroActual = this.tFiltros[0];

      this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
        .subscribe((tHayConfig: boolean) => {

          if (tHayConfig === true) {

            this.Buscar_Estatus();
            this.Buscar_Tipo();
            this.Buscar_Pedidos();

          }

        });


    }
    else {

      this.tMsjServices.AbrirModalMsj(environment.msjSinAcceso);
      this.router.navigate(['/Home']);

    }

  }

  ngOnDestroy() {

    this.tEmpresa$.unsubscribe();
    this.tConfiguracion$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();

  }

  //#region "Funciones Modal"

  AbrirModalDetalles(tPedido: Pedido): void {

    this.tDetallesPedidoService
      .AbrirModalConf(tPedido)
      .then(result => {



      }, reason => {



      });


  }

  //#endregion

  //#region "Funciones de busqueda"

  Buscar_Estatus(): void {

    this.Ws
      .Estatus()
      .Consumir_Obtener_Estatus(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        "TR_PEDIDO")
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tEstatus = Respuesta.Datos;

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Buscar_Tipo(): void {

    this.Ws
      .Tipo()
      .Consumir_Obtener_Tipo(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        "TR_PEDIDO")
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.tTipos = Respuesta.Datos;

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Buscar_Pedidos(): void {

    var tEstatusFiltros: string = '';
    var tTipoFiltros: string = '';
    var tFechacreacionI: number = 0;
    var tFechacreacionF: number = 0;

    for (let tEstatus of this.tEstatusActuales) {

      if (tEstatusFiltros == "") {

        tEstatusFiltros = "'" + tEstatus.Codestatus + "'";

      }
      else {

        tEstatusFiltros += ", '" + tEstatus.Codestatus + "'";

      }

    }

    for (let tTipo of this.tTiposActuales) {

      if (tTipoFiltros == "") {

        tTipoFiltros = "'" + tTipo.Codtipo + "'";

      }
      else {

        tTipoFiltros += ", '" + tTipo.Codtipo + "'";

      }

    }

    if (tTipoFiltros != "") {

      tTipoFiltros = "(" + tTipoFiltros + ")";

    }

    if (tEstatusFiltros != "") {

      tEstatusFiltros = "(" + tEstatusFiltros + ")"

    }

    if (this.tFiltrarFechaC === true) {

      tFechacreacionI = this.tManejarFecha.Seleccionar_Fecha(this.tFechasCreacion[0]);
      tFechacreacionF = this.tManejarFecha.Seleccionar_Fecha(this.tFechasCreacion[1]);

    }


    this.Ws
      .Pedidos()
      .Consumir_Obtener_PedidosPlaneador(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        tEstatusFiltros,
        tTipoFiltros,
        tFechacreacionI,
        tFechacreacionF)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tPedidos = Respuesta.Datos;
          this.ArmarGraficoTipo();
          this.ArmarGraficoEstatus();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  Descargar_Reportes(tPedidoSel: Pedido): void {

    this.Ws
      .Pedidos()
      .Consumir_Obtener_PedidoRpt(tPedidoSel.Corporacion,
        tPedidoSel.Empresa,
        tPedidoSel.Codpedido)
      .subscribe(Respuesta => {


        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          var Reporte: Reportes = Respuesta.Datos[0];
          this.tManejarPdf.AbrirPdf(Reporte);

        }

      }, error => {


        this.tErroresServices.MostrarError(error);

      });

  }

  Descargar_Excel() {

    let tPedidosTemp: Pedido[] = this.tPedidos;

    tPedidosTemp.map(tPedido => {

      delete tPedido.Id;
      delete tPedido.Corporacion;
      delete tPedido.Empresa;
      delete tPedido.Usuario;
      delete tPedido.Clave;
      delete tPedido.Url;
      delete tPedido.Codgoogle
      delete tPedido.Urlgoogle
      delete tPedido.Codfacebook
      delete tPedido.Urlfacebook
      delete tPedido.Usuariocreador;
      delete tPedido.Usuariomodificador;
      delete tPedido.Fecha_creacion;
      delete tPedido.Fecha_creacionl;
      delete tPedido.Fecha_modificacion;
      delete tPedido.Fecha_modificacionl;
      delete tPedido.LArticulos;
      delete tPedido.LCupones;
      delete tPedido.LColecciones;
      delete tPedido.Venta;

    });

    this.tManejarArchivos.GenerarExcel(tPedidosTemp, 'Pedidos.xlsx', 'Pedidos');
    event.preventDefault();

  }

  SeleccionarEstatus(tEstatusSel: any): void {

    for (let tEsta of this.tEstatus) {

      if (tEsta.Codestatus == tEstatusSel) {

        this.tEstatusActuales.push(tEsta);

      }

    }

  }

  DeseleccionarEstatus(tEstatusSel: any): void {

    var jv: number = 0;

    for (let tEstatus of this.tEstatusActuales) {

      if (tEstatus.Codestatus == tEstatusSel) {

        this.tEstatusActuales.splice(jv, 1);

      }

      jv = +jv + +1;

    }

  }

  SeleccionarTipo(tTipoSel: any): void {

    for (let tTipo of this.tTipos) {

      if (tTipo.Codtipo == tTipoSel) {

        this.tTiposActuales.push(tTipo);

      }

    }

  }

  DeseleccionarTipo(tTipoSel: any): void {

    var jv: number = 0;

    for (let tTipo of this.tTiposActuales) {

      if (tTipo.Codtipo == tTipoSel) {

        this.tTiposActuales.splice(jv, 1);

      }

      jv = +jv + +1;

    }

  }

  TotalPedidos(): number {

    let tTotal: number = 0;

    this.tPedidos.forEach(tPedido => {

      tTotal += tPedido.Total;

    });

    return tTotal;

  }

  Limpiar(): void {

    this.tFechasCreacion = [new Date(), new Date()];
    this.tFiltrarFechaC = true;
    this.tEstatusActuales = [];
    this.tTiposActuales = [];
    this.Buscar_Pedidos();

  }

  ArmarGraficoTipo(): void {

    this.tDatosBarrasTipo = new Barra();
    this.tDatosPieTipo = new Pie();

    this.tTipos.forEach(tTipo => {

      let tDataSetTipo: DataSetBarra = new DataSetBarra();
      let tDataSetTipoPie: DataSetPie = new DataSetPie();

      let tContadorTipo: number = 0;
      let tAcumuladorTipo: number = 0;

      tDataSetTipo.Color = tTipo.Hexadecimal; //'#' + (Math.random() * 0xFFFFFF << 0).toString(16);
      tDataSetTipo.Etiqueta = tTipo.Codtipo;

      tDataSetTipoPie.Color = tTipo.Hexadecimal; //'#' + (Math.random() * 0xFFFFFF << 0).toString(16);
      tDataSetTipoPie.Etiqueta = tTipo.Codtipo;

      this.tPedidos.forEach(tPedido => {

        if (tPedido.Tipo === tTipo.Codtipo) {

          tContadorTipo++;
          tAcumuladorTipo += tPedido.Total;

        }

      });

      tDataSetTipo.Datos = tAcumuladorTipo.toString();
      tDataSetTipoPie.Datos.push(tContadorTipo.toString());

      this.tDatosBarrasTipo.Etiquetas.push(tTipo.Codtipo);
      this.tDatosBarrasTipo.DataSet.push(tDataSetTipo);

      this.tDatosPieTipo.Etiquetas.push(tTipo.Codtipo);
      this.tDatosPieTipo.DataSet.push(tDataSetTipoPie);

    });

    this.MostrarGraficoTipo();

  }

  MostrarGraficoTipo(): void {

    if (this.tTipoPie === true) {

      this.tElementoTipo = document.getElementById("canvasTipoPie");
      this.tPieService.ArmarGrafico(this.tElementoTipo, this.tDatosPieTipo);

    }
    else {

      this.tElementoTipo = document.getElementById("canvasTipoBarra");
      this.tBarrasService.ArmarGrafico(this.tElementoTipo, this.tDatosBarrasTipo);

    }

  }

  ArmarGraficoEstatus(): void {

    this.tDatosBarrasEst = new Barra();
    this.tDatosPieEsta = new Pie();

    this.tEstatus.forEach(tEstatus => {

      let tDataSetEst: DataSetBarra = new DataSetBarra();
      let tDataSetEstPie: DataSetPie = new DataSetPie();

      let tContadorEstatus: number = 0;
      let tAcumuladorEstatus: number = 0;

      tDataSetEst.Color = tEstatus.Hexadecimal; //'#' + (Math.random() * 0xFFFFFF << 0).toString(16);
      tDataSetEst.Etiqueta = tEstatus.Codestatus;

      tDataSetEstPie.Color = tEstatus.Hexadecimal; //'#' + (Math.random() * 0xFFFFFF << 0).toString(16);
      tDataSetEstPie.Etiqueta = tEstatus.Codestatus;

      this.tPedidos.forEach(tPedido => {

        if (tPedido.Estatus === tEstatus.Codestatus) {

          tContadorEstatus++;
          tAcumuladorEstatus += tPedido.Total;

        }

      });

      tDataSetEst.Datos = tAcumuladorEstatus.toString();
      tDataSetEstPie.Datos.push(tContadorEstatus.toString());

      this.tDatosBarrasEst.Etiquetas.push(tEstatus.Codestatus);
      this.tDatosBarrasEst.DataSet.push(tDataSetEst);

      this.tDatosPieEsta.Etiquetas.push(tEstatus.Codestatus);
      this.tDatosPieEsta.DataSet.push(tDataSetEstPie);

    });

    this.MostrarGraficoEstatus();

  }

  MostrarGraficoEstatus(): void {

    if (this.tEstatusPie === true) {

      this.tElementoEstatus = document.getElementById("canvasPieEstatus");
      this.tPieService.ArmarGrafico(this.tElementoEstatus, this.tDatosPieEsta);

    }
    else {

      this.tElementoEstatus = document.getElementById("canvasBarraEstatus");
      this.tBarrasService.ArmarGrafico(this.tElementoEstatus, this.tDatosBarrasEst);

    }

  }

}
