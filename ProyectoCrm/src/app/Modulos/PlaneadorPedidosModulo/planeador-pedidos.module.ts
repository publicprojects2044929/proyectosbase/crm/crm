import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlaneadorPedidosRoutingModule } from './planeador-pedidos-routing.module';
import { FwPlaneadorPedidosComponent } from './Formularios/fw-planeador-pedidos/fw-planeador-pedidos.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { NgSelectModule } from '@ng-select/ng-select';

import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { SharedModule } from 'src/app/Shared/shared.module'

@NgModule({
  declarations: [FwPlaneadorPedidosComponent],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    BsDatepickerModule.forRoot(),
    NgbModule,
    NgMultiSelectDropDownModule.forRoot(),
    NgSelectModule,
    PlaneadorPedidosRoutingModule
  ]
})
export class PlaneadorPedidosModule { }
