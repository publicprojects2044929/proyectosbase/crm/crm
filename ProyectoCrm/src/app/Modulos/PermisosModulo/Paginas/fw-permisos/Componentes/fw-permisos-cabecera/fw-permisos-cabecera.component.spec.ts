import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwPermisosCabeceraComponent } from './fw-permisos-cabecera.component';

describe('FwPermisosCabeceraComponent', () => {
  let component: FwPermisosCabeceraComponent;
  let fixture: ComponentFixture<FwPermisosCabeceraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwPermisosCabeceraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwPermisosCabeceraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
