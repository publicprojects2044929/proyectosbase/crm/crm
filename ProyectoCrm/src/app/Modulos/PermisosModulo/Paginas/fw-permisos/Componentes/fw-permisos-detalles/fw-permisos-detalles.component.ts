import { Component, OnInit, OnChanges, Input, SimpleChanges } from '@angular/core';
import { Filtros, Grupos, Formulario, GrupoFormularios, GrupoFormulariosTareas } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-permisos-detalles',
  templateUrl: './fw-permisos-detalles.component.html',
  styleUrls: ['./fw-permisos-detalles.component.scss']
})
export class FwPermisosDetallesComponent implements OnInit, OnChanges {

  @Input('Grupo') tGrupo: Grupos = new Grupos();

  tFiltros: Filtros[] = [];
  tFiltroActual: Filtros = new Filtros();
  tFiltroBusqueda: string = "";

  tFormularioActual: GrupoFormularios = new GrupoFormularios();

  tPaginaFormulario: number = 1;
  tTamanoPagFormulario: number = 10;

  tPaginaFormularioTareas: number = 1;
  tTamanoPagFormularioTareas: number = 10;

  constructor() {

    this.tFiltroBusqueda = "";

    this.tFiltros = [

      { Codfiltro: "Codgrupo", Filtro: "Código" },
      { Codfiltro: "Descripcion", Filtro: "Descripción" },

    ]

  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges){

    if('tGrupo' in changes){
      this.tFiltroActual = this.tFiltros[0] || new Filtros();
      this.tFormularioActual = new GrupoFormularios();
    }

  }

  Formulario_AccesoTodos(tCheckbox: any): void {

    let tAcceso: number = 0;

    if (tCheckbox == true) {

      tAcceso = 1;

    }

    this.tGrupo.LFormularios.map(tFormulario => {

      tFormulario.Acceso = tAcceso;

    });

  }

  Formulario_AgregarTodos(tCheckbox: any): void {

    let tAgregar: number = 0;

    if (tCheckbox == true) {

      tAgregar = 1;

    }

    this.tGrupo.LFormularios.map(tFormulario => {

      tFormulario.Agregar = tAgregar;

    });

  }

  Formulario_EditarTodos(tCheckbox: any): void {

    let tEditar: number = 0;

    if (tCheckbox == true) {

      tEditar = 1;

    }

    this.tGrupo.LFormularios.map(tFormulario => {

      tFormulario.Editar = tEditar;

    });

  }

  Formulario_EliminarTodos(tCheckbox: any): void {

    let tEliminar: number = 0;

    if (tCheckbox == true) {

      tEliminar = 1;

    }

    this.tGrupo.LFormularios.map(tFormulario => {

      tFormulario.Eliminar = tEliminar;

    });

  }

  Formulario_ImprimirTodos(tCheckbox: any): void {

    let tImprimir: number = 0;

    if (tCheckbox == true) {

      tImprimir = 1;

    }

    this.tGrupo.LFormularios.map(tFormulario => {

      tFormulario.Imprimir = tImprimir;

    });

  }

  Formulario_PermisoTodos(tCheckbox: any): void {

    let tPermiso: number = 0;

    if (tCheckbox == true) {

      tPermiso = 1;

    }

    this.tGrupo.LFormularios.map(tFormulario => {

      tFormulario.Permiso = tPermiso;

    });

  }


  Formulario_Acceso(tCheckbox: any, tFormulario_Sel: GrupoFormularios): void {

    if (tCheckbox == true) {

      tFormulario_Sel.Acceso = 1;

    }
    else {

      tFormulario_Sel.Acceso = 0;

    }

  }

  Formulario_Agregar(tCheckbox: any, tFormulario_Sel: GrupoFormularios): void {

    if (tCheckbox == true) {

      tFormulario_Sel.Agregar = 1;

    }
    else {

      tFormulario_Sel.Agregar = 0;

    }

  }

  Formulario_Editar(tCheckbox: any, tFormulario_Sel: GrupoFormularios): void {

    if (tCheckbox == true) {

      tFormulario_Sel.Editar = 1;

    }
    else {

      tFormulario_Sel.Editar = 0;

    }

  }

  Formulario_Eliminar(tCheckbox: any, tFormulario_Sel: GrupoFormularios): void {

    if (tCheckbox == true) {

      tFormulario_Sel.Eliminar = 1;

    }
    else {

      tFormulario_Sel.Eliminar = 0;

    }

  }

  Formulario_Imprimir(tCheckbox: any, tFormulario_Sel: GrupoFormularios): void {

    if (tCheckbox == true) {

      tFormulario_Sel.Imprimir = 1;

    }
    else {

      tFormulario_Sel.Imprimir = 0;

    }

  }

  Formulario_Permiso(tCheckbox: any, tFormulario_Sel: GrupoFormularios): void {

    if (tCheckbox == true) {

      tFormulario_Sel.Permiso = 1;

    }
    else {

      tFormulario_Sel.Permiso = 0;

    }

  }

  SeleccionarFormulario(tFormularioSel: GrupoFormularios) {

    this.tFormularioActual = tFormularioSel;

  }

  CheckTarea(tTareaSel: GrupoFormulariosTareas) {

    if (tTareaSel.Asignado === 1) {

      tTareaSel.Asignado = 0;

    }
    else {

      tTareaSel.Asignado = 1;

    }

    this.tFormularioActual.LTareas.map((tTarea: GrupoFormulariosTareas) => {

      if (tTarea.Corporacion === tTareaSel.Corporacion &&
        tTarea.Empresa === tTareaSel.Empresa &&
        tTarea.Codgrupo === tTareaSel.Codgrupo &&
        tTarea.Codformulario === tTareaSel.Codformulario &&
        tTarea.Codtarea === tTareaSel.Codtarea) {

        tTarea.Asignado = tTareaSel.Asignado;

      }

    });

    this.tGrupo.LFormularios.map(tFormulario => {

      if (tFormulario.Corporacion === this.tFormularioActual.Corporacion &&
        tFormulario.Empresa === this.tFormularioActual.Empresa &&
        tFormulario.Codgrupo == this.tFormularioActual.Codgrupo &&
        tFormulario.Codformulario === this.tFormularioActual.Codformulario) {

        tFormulario.LTareas = this.tFormularioActual.LTareas;

      }

    });

  }

}
