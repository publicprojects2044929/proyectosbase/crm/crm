import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwPermisosDetallesComponent } from './fw-permisos-detalles.component';

describe('FwPermisosDetallesComponent', () => {
  let component: FwPermisosDetallesComponent;
  let fixture: ComponentFixture<FwPermisosDetallesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwPermisosDetallesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwPermisosDetallesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
