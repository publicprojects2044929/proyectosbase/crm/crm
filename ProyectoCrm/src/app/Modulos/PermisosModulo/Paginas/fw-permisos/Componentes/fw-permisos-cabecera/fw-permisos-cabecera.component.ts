import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Grupos } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-permisos-cabecera',
  templateUrl: './fw-permisos-cabecera.component.html',
  styleUrls: ['./fw-permisos-cabecera.component.scss']
})
export class FwPermisosCabeceraComponent implements OnInit {

  @Input('Grupo') tGrupo: Grupos = new Grupos();
  @Input('Nuevo') tNuevo: boolean = false;

  @Output('Buscar') tBuscar: EventEmitter<string> = new EventEmitter();
  @Output('AbrirModal') tAbrir: EventEmitter<string> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  Buscar(){

    if(this.tGrupo.Codgrupo === '') { return; }

    this.tBuscar.emit(this.tGrupo.Codgrupo);

  }

  Abrir(){
    this.tAbrir.emit('');
  }

}
