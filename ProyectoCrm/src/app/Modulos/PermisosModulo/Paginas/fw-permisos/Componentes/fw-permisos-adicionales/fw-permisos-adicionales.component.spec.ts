import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwPermisosAdicionalesComponent } from './fw-permisos-adicionales.component';

describe('FwPermisosAdicionalesComponent', () => {
  let component: FwPermisosAdicionalesComponent;
  let fixture: ComponentFixture<FwPermisosAdicionalesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwPermisosAdicionalesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwPermisosAdicionalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
