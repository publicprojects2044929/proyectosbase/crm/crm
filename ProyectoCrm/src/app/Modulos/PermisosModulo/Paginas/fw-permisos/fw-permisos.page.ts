import { Component, OnInit, OnDestroy } from '@angular/core';
import { Empresas, Usuario, Grupos, Formulario, GrupoFormularios, GrupoFormulariosTareas } from 'src/app/Clases/Estructura';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { ErroresService } from 'src/app/Modales/mensajes/fw-modal-msj/errores.service';
import { MensajesService } from 'src/app/Modales/mensajes/fw-modal-msj/mensajes.service';
import { ConfirmacionService } from 'src/app/Modales/confirmacion/fw-modal-confirmacion/confirmacion.service';
import { ToastrService } from 'ngx-toastr';
import { SessionService } from 'src/app/Core/Session/session.service';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment'
import { GrupoFormulariosEntrada, GrupoEntradas, GrupoFormulariosTareasEntrada } from 'src/app/Clases/EstructuraEntrada';
import { Subscription } from 'rxjs';
import { ConfiguracionService } from 'src/app/Core/Configuracion/configuracion.service';
import { FormularioConfiguracion } from 'src/app/Clases/EstructuraConfiguracion';
import { GruposService } from 'src/app/Modales/grupos/fw-modal-grupos/grupos.service';

@Component({
  selector: 'app-fw-permisos',
  templateUrl: './fw-permisos.page.html',
  styleUrls: ['./fw-permisos.page.scss']
})
export class FwPermisosPage implements OnInit, OnDestroy {

  tCodFormularioActual: string = "fPermisos";
  tConfiguracionFormulario: FormularioConfiguracion = new FormularioConfiguracion();
  tPermisoAgregar: boolean = false;
  tPermisoEditar: boolean = false;
  tPermisoEliminar: boolean = false;
  tHabilitarGuardar: boolean = false;
  tHabilitarEliminar: boolean = false;

  tUsuarioActual: Usuario = new Usuario();
  tEmpresaActual: Empresas = new Empresas();
  tEmpresa$: Subscription;
  tUsuario$: Subscription;

  tTitulo: string = "Gestión de permisos de usuarios";
  tNuevo: boolean = false;
  tPosicion: number = 0;
  tPagina: number = 1;
  tTamanoPag: number = 10;

  tFormularios: Formulario[] = [];

  tGrupoActual: Grupos = new Grupos();

  tConfiguracion$: Subscription;
  tHayConfiguracionWs$: Subscription;
  tStrConfiguracion: string = 'Permisos/Permisos.Config.json';

  constructor(public Ws: ClsServiciosService,
    private router: Router,
    private tSesion: SessionService,
    private tConfiguracionService: ConfiguracionService,
    private toastr: ToastrService,
    private tGrupoService: GruposService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService,
    private tConfServices: ConfirmacionService) {

    this.tPosicion = 0;

    this.tGrupoActual.LFormularios = [];

  }

  ngOnInit() {

    this.tConfiguracion$ = this.tConfiguracionService
      .Consumir_Obtener_Configuracion(this.tStrConfiguracion)
      .subscribe((tConfiguracion: FormularioConfiguracion) => {

        this.tConfiguracionFormulario = tConfiguracion;
        this.tTitulo = this.tConfiguracionFormulario.Titulo;
        this.tErroresServices.tTituloMsj = this.tConfiguracionFormulario.ModalMensaje.Titulo;
        this.tMsjServices.tTituloMsj = this.tConfiguracionFormulario.ModalMensaje.Titulo;
        this.tConfServices.tTituloConf = this.tConfiguracionFormulario.ModalConfirmacion.Titulo;

      });

    this.tEmpresa$ = this.tSesion.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    })

    this.tUsuario$ = this.tSesion.tUsuarioActual$.subscribe((tUsuario: Usuario) => {

      this.tUsuarioActual = tUsuario;

    })

    var tTienePermiso: boolean = this.tSesion.ObtenerPermisoAcceso(this.tCodFormularioActual);
    this.tPermisoAgregar = this.tSesion.ObtenerPermisoAgregar();
    this.tPermisoEditar = this.tSesion.ObtenerPermisoEditar();
    this.tPermisoEliminar = this.tSesion.ObtenerPermisoEliminar();

    if (tTienePermiso == true) {

      this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
        .subscribe((tHayConfig: boolean) => {

          if (tHayConfig === true) {

            // this.Buscar_Grupos();
            this.Nuevo();

          }

        });

    }
    else {

      this.tMsjServices.AbrirModalMsj(environment.msjSinAcceso);
      this.router.navigate(['/Home']);

    }

  }

  ngOnDestroy() {

    this.tEmpresa$.unsubscribe();
    this.tConfiguracion$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();
    this.tUsuario$.unsubscribe();

  }

  //#region "Funciones de busqueda"

  Buscar_Grupo(tCodgrupo: string): void {

    this.Ws
      .Grupo()
      .Consumir_Obtener_GrupoPorCodigoOcultoFull(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        tCodgrupo, 'N')
      .subscribe(Respuesta => {

        this.tGrupoActual = new Grupos();

        if (Respuesta.Resultado == "N") {

          // this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);
          //

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tNuevo = false;
          this.tHabilitarGuardar = this.tPermisoEditar;
          this.tHabilitarEliminar = this.tPermisoEliminar;
          this.tGrupoActual = Respuesta.Datos[0] || new Grupos();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Buscar_Formularios(): void {

    this.Ws
      .Formulario()
      .Consumir_Obtener_FormularioCrmFull(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa)
      .subscribe(Respuesta => {

        this.tGrupoActual.LFormularios = [];

        if (Respuesta.Resultado === "N") {

          // this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);
          //

        }
        else if (Respuesta.Resultado === "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tFormularios = Respuesta.Datos || [];

          for (let tFormulario of this.tFormularios) {

            var tNuevoFormulario: GrupoFormularios;

            tNuevoFormulario = {

              "Id": 0,
              "Corporacion": tFormulario.Corporacion,
              "Empresa": tFormulario.Empresa,
              "Codgrupo": "",
              "Codformulario": tFormulario.Codformulario,
              "Codesquema": tFormulario.Codesquema,
              "Codformulariop": tFormulario.Codformulariop,
              "Descripcion": tFormulario.Descripcion,
              "Descripcion2": tFormulario.Descripcion2,
              "Tipo": tFormulario.Tipo,
              "Estilo": tFormulario.Estilo,
              "Url": tFormulario.Url,
              "Acceso": 0,
              "Agregar": 0,
              "Editar": 0,
              "Eliminar": 0,
              "Imprimir": 0,
              "Permiso": 0,
              "Fecha_creacion": new Date(),
              "Fecha_creacionl": 0,
              "Fecha_modificacion": new Date(),
              "Fecha_modificacionl": 0,
              "Usuariocreador": this.tUsuarioActual.Codusuario,
              "Usuariomodificador": this.tUsuarioActual.Codusuario,
              "LTareas": []

            }

            tFormulario.LTareas.forEach(tTarea => {

              var tNuevaTarea: GrupoFormulariosTareas;
              tNuevaTarea = {

                "Id": 0,
                "Corporacion": tTarea.Corporacion,
                "Empresa": tTarea.Empresa,
                "Codgrupo": "",
                "Codformulario": tTarea.Codformulario,
                "Codtarea": tTarea.Codtarea,
                "Descripcion": tTarea.Descripcion,
                "Asignado": 0,
                "Fecha_creacion": new Date(),
                "Fecha_creacionl": 0,
                "Fecha_modificacion": new Date(),
                "Fecha_modificacionl": 0,
                "Usuariocreador": this.tUsuarioActual.Codusuario,
                "Usuariomodificador": this.tUsuarioActual.Codusuario,

              }

              tNuevoFormulario.LTareas.push(tNuevaTarea);

            });

            this.tGrupoActual.LFormularios.push(tNuevoFormulario);

          }

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  //#region "CRUD"

  Nuevo_Grupo(): void {

    var tNuevoGrupo: GrupoEntradas;
    tNuevoGrupo = {
      "Id": 0,
      "Corporacion": this.tEmpresaActual.Corporacion,
      "Empresa": this.tEmpresaActual.Empresa,
      "Codgrupo": "",
      "Descripcion": this.tGrupoActual.Descripcion,
      "Oculto": "N",
      "LGrupoFormulario": [],
      "Usuariocreador": this.tUsuarioActual.Codusuario,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }

    for (let tFormulario of this.tGrupoActual.LFormularios) {

      if (tFormulario.Acceso == 1) {

        var tNuevoFormulario: GrupoFormulariosEntrada;
        tNuevoFormulario = {
          "Id": 0,
          "Corporacion": tFormulario.Corporacion,
          "Empresa": tFormulario.Empresa,
          "Codgrupo": tFormulario.Codgrupo,
          "Codformulario": tFormulario.Codformulario,
          "Agregar": tFormulario.Agregar,
          "Editar": tFormulario.Editar,
          "Eliminar": tFormulario.Eliminar,
          "Imprimir": tFormulario.Imprimir,
          "Permiso": tFormulario.Permiso,
          "Usuariocreador": this.tUsuarioActual.Codusuario,
          "Usuariomodificador": this.tUsuarioActual.Codusuario,
          "LTareas": []

        }

        tFormulario.LTareas.forEach(tTarea => {

          if(tTarea.Asignado === 1){

            var tNuevaTarea: GrupoFormulariosTareasEntrada;
            tNuevaTarea = {
              "Id": 0,
              "Corporacion": tTarea.Corporacion,
              "Empresa": tTarea.Empresa,
              "Codgrupo": tTarea.Codgrupo,
              "Codformulario": tTarea.Codformulario,
              "Codtarea": tTarea.Codtarea,
              "Asignado": tTarea.Asignado,
              "Usuariocreador": tTarea.Usuariocreador,
              "Usuariomodificador": tTarea.Usuariomodificador
            }
            tNuevoFormulario.LTareas.push(tNuevaTarea);

          }

        });

        tNuevoGrupo.LGrupoFormulario.push(tNuevoFormulario);

      }

    }

    this.Ws.Grupo().Consumir_Crear_Grupo(tNuevoGrupo).subscribe(Respuesta => {

      if (Respuesta.Resultado == "N") {

        this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

      }
      else if (Respuesta.Resultado == "E") {

        this.tMsjServices.AbrirModalMsj(environment.msjError);

      }
      else {

        this.toastr.clear();
        this.toastr.success('El grupo se ha creado de manera exitosa!');
        this.Nuevo();

      }

    }, error => {

      this.tErroresServices.MostrarError(error);

    });

  }

  Modificar_Grupo(): void {

    var tModificarGrupo: GrupoEntradas;

    tModificarGrupo = {
      "Id": 0,
      "Corporacion": this.tGrupoActual.Corporacion,
      "Empresa": this.tGrupoActual.Empresa,
      "Codgrupo": this.tGrupoActual.Codgrupo,
      "Descripcion": this.tGrupoActual.Descripcion,
      "Oculto": this.tGrupoActual.Oculto,
      "LGrupoFormulario": [],
      "Usuariocreador": this.tGrupoActual.Usuariocreador,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }

    for (let tFormulario of this.tGrupoActual.LFormularios) {

      if (tFormulario.Acceso === 1) {

        var tNuevoFormulario: GrupoFormulariosEntrada;
        tNuevoFormulario = {
          "Id": 0,
          "Corporacion": tFormulario.Corporacion,
          "Empresa": tFormulario.Empresa,
          "Codgrupo": tFormulario.Codgrupo,
          "Codformulario": tFormulario.Codformulario,
          "Agregar": tFormulario.Agregar,
          "Editar": tFormulario.Editar,
          "Eliminar": tFormulario.Eliminar,
          "Imprimir": tFormulario.Imprimir,
          "Permiso": tFormulario.Permiso,
          "Usuariocreador": this.tUsuarioActual.Codusuario,
          "Usuariomodificador": this.tUsuarioActual.Codusuario,
          "LTareas": []
        }

        tFormulario.LTareas.forEach(tTarea => {

          if(tTarea.Asignado === 1){

            var tNuevaTarea: GrupoFormulariosTareasEntrada;
            tNuevaTarea = {
              "Id": 0,
              "Corporacion": tTarea.Corporacion,
              "Empresa": tTarea.Empresa,
              "Codgrupo": tTarea.Codgrupo,
              "Codformulario": tTarea.Codformulario,
              "Codtarea": tTarea.Codtarea,
              "Asignado": tTarea.Asignado,
              "Usuariocreador": tTarea.Usuariocreador,
              "Usuariomodificador": tTarea.Usuariomodificador
            }
            tNuevoFormulario.LTareas.push(tNuevaTarea);

          }

        });

        tModificarGrupo.LGrupoFormulario.push(tNuevoFormulario);

      }

    }

    this.Ws.Grupo().Consumir_Modificar_Grupo(tModificarGrupo).subscribe(Respuesta => {

      if (Respuesta.Resultado == "N") {

        this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


      }
      else if (Respuesta.Resultado == "E") {

        this.tMsjServices.AbrirModalMsj("Hay un problema de conexión con el servidor. Por favor contacte con sistemas.");


      }
      else {

        this.toastr.clear();
        this.toastr.success('El grupo se ha modificado de manera exitosa!');

        // this.Buscar_Grupos();
        this.Nuevo();

      }

    }, error => {

      this.tErroresServices.MostrarError(error);

    });

  }

  Eliminar_Grupo(): void {

    var tEliminarGrupo: GrupoEntradas;
    tEliminarGrupo = {
      "Id": 0,
      "Corporacion": this.tGrupoActual.Corporacion,
      "Empresa": this.tGrupoActual.Empresa,
      "Codgrupo": this.tGrupoActual.Codgrupo,
      "Descripcion": this.tGrupoActual.Descripcion,
      "Oculto": this.tGrupoActual.Oculto,
      "LGrupoFormulario": [],
      "Usuariocreador": this.tGrupoActual.Usuariocreador,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }

    for (let tFormulario of this.tGrupoActual.LFormularios) {

      if (tFormulario.Acceso == 1) {

        var tNuevoFormulario: GrupoFormulariosEntrada;
        tNuevoFormulario = {
          "Id": 0,
          "Corporacion": tFormulario.Corporacion,
          "Empresa": tFormulario.Empresa,
          "Codgrupo": tFormulario.Codgrupo,
          "Codformulario": tFormulario.Codformulario,
          "Agregar": tFormulario.Agregar,
          "Editar": tFormulario.Editar,
          "Eliminar": tFormulario.Eliminar,
          "Imprimir": tFormulario.Imprimir,
          "Permiso": tFormulario.Permiso,
          "Usuariocreador": this.tUsuarioActual.Codusuario,
          "Usuariomodificador": this.tUsuarioActual.Codusuario,
          "LTareas": []

        }

        tFormulario.LTareas.forEach(tTarea => {

          if(tTarea.Asignado === 1){

            var tNuevaTarea: GrupoFormulariosTareasEntrada;
            tNuevaTarea = {
              "Id": 0,
              "Corporacion": tTarea.Corporacion,
              "Empresa": tTarea.Empresa,
              "Codgrupo": tTarea.Codgrupo,
              "Codformulario": tTarea.Codformulario,
              "Codtarea": tTarea.Codtarea,
              "Asignado": tTarea.Asignado,
              "Usuariocreador": tTarea.Usuariocreador,
              "Usuariomodificador": tTarea.Usuariomodificador
            }
            tNuevoFormulario.LTareas.push(tNuevaTarea);

          }

        });

        tEliminarGrupo.LGrupoFormulario.push(tNuevoFormulario);

      }

    }

    this.Ws.Grupo().Consumir_Eliminar_Grupo(tEliminarGrupo).subscribe(Respuesta => {

      if (Respuesta.Resultado == "N") {

        this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


      }
      else if (Respuesta.Resultado == "E") {

        this.tMsjServices.AbrirModalMsj("Hay un problema de conexión con el servidor. Por favor contacte con sistemas.");


      }
      else {

        this.toastr.clear();
        this.toastr.success('El grupo se ha eliminado de manera exitosa!');
        this.Nuevo();

      }

    }, error => {

      this.tErroresServices.MostrarError(error);

    });

  }

  //#endregion

  AbrirModalGrupo(): void {

    this.tGrupoService
      .AbrirModal()
      .then(result => {

        if (result.Resultado == "S") {

          var tGrupoModal: Grupos = result.Grupo;
          this.Buscar_Grupo(tGrupoModal.Codgrupo);

        }

      });

  }

  Nuevo(): void {

    this.tNuevo = true;
    this.tHabilitarGuardar = this.tPermisoAgregar;
    this.tHabilitarEliminar = false;
    this.tGrupoActual = new Grupos;
    this.tGrupoActual.LFormularios = [];

    this.Buscar_Formularios();

    this.toastr.clear();
    this.toastr.info('Por favor llene los datos necesarios para registrar una nuevo Grupo!');

  }

  Cancelar(): void {

    this.Nuevo();

  }

  ValidarFormulario(): boolean {

    let valido: boolean = false;

    if (this.tGrupoActual.Descripcion == "") {

      this.toastr.clear();
      this.toastr.warning('Debe ingresar una descripciòn!', 'Disculpe!')
      return valido

    }

    return true;
  }

  Agregar(): void {

    if (this.ValidarFormulario() == false) {
      return
    }

    switch (this.tNuevo) {
      case true: {

        this.tConfServices
          .AbrirModalConf("Confirma crear el grupo con los datos suministrados.")
          .then(Resultado => {

            switch (Resultado) {
              case "S": {

                this.Nuevo_Grupo();
                break;

              }

            }

          });

        break;

      }
      default: {

        this.tConfServices
          .AbrirModalConf("Confirma modificar el grupo con los datos suministrados.")
          .then(Resultado => {

            switch (Resultado) {
              case "S": {

                this.Modificar_Grupo();
                break;

              }

            }

          });
        break;

      }
    }

  }

  Eliminar(): void {

    this.tConfServices
      .AbrirModalConf("Confirma eliminar el grupo.")
      .then(Resultado => {

        switch (Resultado) {
          case "S": {

            this.Eliminar_Grupo();
            break;

          }

        }

      });

  }

}
