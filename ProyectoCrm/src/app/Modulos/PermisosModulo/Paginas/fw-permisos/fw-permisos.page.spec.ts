import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwPermisosPage } from './fw-permisos.page';

describe('FwPermisosPage', () => {
  let component: FwPermisosPage;
  let fixture: ComponentFixture<FwPermisosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwPermisosPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwPermisosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
