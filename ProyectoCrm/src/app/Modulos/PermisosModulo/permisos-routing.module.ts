import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FwPermisosPage } from './Paginas/fw-permisos/fw-permisos.page';
import { FwPlantillaComponent  } from 'src/app/Shared/Formularios/fw-plantilla/fw-plantilla.component';

const routes: Routes = [
  {
    path: '',
    component: FwPlantillaComponent,
    children:[
      {
        path: '',
        component: FwPermisosPage,
        data:{
          Codformulario: 'fPermisos'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PermisosRoutingModule { }
