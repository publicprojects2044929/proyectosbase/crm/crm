import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { PermisosRoutingModule } from './permisos-routing.module';
import { FwPermisosPage } from './Paginas/fw-permisos/fw-permisos.page';
import { SharedModule } from 'src/app/Shared/shared.module';
import { FwPermisosCabeceraComponent } from './Paginas/fw-permisos/Componentes/fw-permisos-cabecera/fw-permisos-cabecera.component';
import { FwPermisosDetallesComponent } from './Paginas/fw-permisos/Componentes/fw-permisos-detalles/fw-permisos-detalles.component';
import { FwPermisosAdicionalesComponent } from './Paginas/fw-permisos/Componentes/fw-permisos-adicionales/fw-permisos-adicionales.component'


@NgModule({
  declarations: [FwPermisosPage, FwPermisosCabeceraComponent, FwPermisosDetallesComponent, FwPermisosAdicionalesComponent],
  imports: [
    CommonModule,
    PermisosRoutingModule,
    SharedModule,
    FormsModule,
    NgbModule
  ]
})
export class PermisosModule { }
