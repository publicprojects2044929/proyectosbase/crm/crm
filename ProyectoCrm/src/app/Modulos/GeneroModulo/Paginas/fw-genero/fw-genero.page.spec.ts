import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwGeneroPage } from './fw-genero.page';

describe('FwGeneroPage', () => {
  let component: FwGeneroPage;
  let fixture: ComponentFixture<FwGeneroPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwGeneroPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwGeneroPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
