import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwGeneroCabeceraComponent } from './fw-genero-cabecera.component';

describe('FwGeneroCabeceraComponent', () => {
  let component: FwGeneroCabeceraComponent;
  let fixture: ComponentFixture<FwGeneroCabeceraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwGeneroCabeceraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwGeneroCabeceraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
