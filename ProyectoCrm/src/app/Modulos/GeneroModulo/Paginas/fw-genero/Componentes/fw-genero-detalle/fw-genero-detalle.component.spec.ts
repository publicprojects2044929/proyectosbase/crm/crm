import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwGeneroDetalleComponent } from './fw-genero-detalle.component';

describe('FwGeneroDetalleComponent', () => {
  let component: FwGeneroDetalleComponent;
  let fixture: ComponentFixture<FwGeneroDetalleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwGeneroDetalleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwGeneroDetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
