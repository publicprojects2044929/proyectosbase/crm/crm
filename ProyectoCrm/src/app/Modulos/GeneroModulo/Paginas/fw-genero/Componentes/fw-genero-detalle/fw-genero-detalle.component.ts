import { Component, OnInit, Input } from '@angular/core';
import { Genero } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-genero-detalle',
  templateUrl: './fw-genero-detalle.component.html',
  styleUrls: ['./fw-genero-detalle.component.scss']
})
export class FwGeneroDetalleComponent implements OnInit {

  @Input('Genero') tGenero: Genero = new Genero();

  constructor() { }

  ngOnInit(): void {
  }

  Check_Cliente(tValor: number){
    this.tGenero.Cliente = tValor;
  }

  Check_Articulo(tValor: number){
    this.tGenero.Articulo = tValor;
  }

}
