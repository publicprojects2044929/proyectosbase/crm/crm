import { Component, OnInit, Input } from '@angular/core';
import { Genero } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-genero-cabecera',
  templateUrl: './fw-genero-cabecera.component.html',
  styleUrls: ['./fw-genero-cabecera.component.scss']
})
export class FwGeneroCabeceraComponent implements OnInit {

  @Input('Genero') tGenero: Genero = new Genero();

  constructor() { }

  ngOnInit(): void {
  }

}
