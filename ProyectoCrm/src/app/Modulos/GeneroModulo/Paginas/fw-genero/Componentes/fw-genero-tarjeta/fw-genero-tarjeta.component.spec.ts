import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwGeneroTarjetaComponent } from './fw-genero-tarjeta.component';

describe('FwGeneroTarjetaComponent', () => {
  let component: FwGeneroTarjetaComponent;
  let fixture: ComponentFixture<FwGeneroTarjetaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwGeneroTarjetaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwGeneroTarjetaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
