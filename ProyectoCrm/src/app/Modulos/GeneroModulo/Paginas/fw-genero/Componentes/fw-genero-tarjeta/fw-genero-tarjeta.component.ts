import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';

@Component({
  selector: 'app-fw-genero-tarjeta',
  templateUrl: './fw-genero-tarjeta.component.html',
  styleUrls: ['./fw-genero-tarjeta.component.scss']
})
export class FwGeneroTarjetaComponent implements OnInit {

  @Input('Titulo') tTitulo: string = "";
  @Input('Descripcion') tDescripcion: string = "";
  @Input('Icono') tIcono: string = "";
  @Input('Check') tCheck: number = 0;
  @Input('Nombre') tNombre: string = "";

  @Output('Cambiar') tCambiar: EventEmitter<number> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  CambiarCheck(){

    let tEstadoNro: number = !!this.tCheck === true ? 1 : 0;

    this.tCambiar.emit(tEstadoNro);

  }

}
