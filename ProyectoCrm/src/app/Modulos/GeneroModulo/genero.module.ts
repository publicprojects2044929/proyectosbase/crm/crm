import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from 'src/app/Shared/shared.module'

import { GeneroRoutingModule } from './genero-routing.module';
import { FwGeneroPage } from './Paginas/fw-genero/fw-genero.page';
import { FwGeneroCabeceraComponent } from './Paginas/fw-genero/Componentes/fw-genero-cabecera/fw-genero-cabecera.component';
import { FwGeneroDetalleComponent } from './Paginas/fw-genero/Componentes/fw-genero-detalle/fw-genero-detalle.component';
import { FwGeneroTarjetaComponent } from './Paginas/fw-genero/Componentes/fw-genero-tarjeta/fw-genero-tarjeta.component';


@NgModule({
  declarations: [FwGeneroPage, FwGeneroCabeceraComponent, FwGeneroDetalleComponent, FwGeneroTarjetaComponent],
  imports: [
    CommonModule,
    GeneroRoutingModule,
    SharedModule,
    FormsModule,
    NgbModule
  ]
})
export class GeneroModule { }
