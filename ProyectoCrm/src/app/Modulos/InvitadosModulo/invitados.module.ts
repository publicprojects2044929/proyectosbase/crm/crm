import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { InvitadosRoutingModule } from './invitados-routing.module';
import { FwInvitadosPage } from './Paginas/fw-invitados/fw-invitados.page';

import { SharedModule } from 'src/app/Shared/shared.module';
import { FwInvitadosCabeceraComponent } from './Paginas/fw-invitados/Componentes/fw-invitados-cabecera/fw-invitados-cabecera.component';
import { FwInvitadosDetallesComponent } from './Paginas/fw-invitados/Componentes/fw-invitados-detalles/fw-invitados-detalles.component'


@NgModule({
  declarations: [FwInvitadosPage, FwInvitadosCabeceraComponent, FwInvitadosDetallesComponent],
  imports: [
    CommonModule,
    InvitadosRoutingModule,
    SharedModule,
    FormsModule,
    NgbModule
  ]
})
export class InvitadosModule { }
