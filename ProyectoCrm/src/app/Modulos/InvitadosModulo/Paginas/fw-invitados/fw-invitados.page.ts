import { Component, OnInit, OnDestroy } from '@angular/core';
import { Usuario, Empresas, Filtros, GruposUsuario, Configuracion } from 'src/app/Clases/Estructura';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/Core/Session/session.service';
import { ToastrService } from 'ngx-toastr';
import { ErroresService } from 'src/app/Modales/mensajes/fw-modal-msj/errores.service';
import { MensajesService } from 'src/app/Modales/mensajes/fw-modal-msj/mensajes.service';
import { ConfirmacionService } from 'src/app/Modales/confirmacion/fw-modal-confirmacion/confirmacion.service';
import { environment } from 'src/environments/environment';
import { UsuarioEmpresasEntrada, UsuarioGruposEntrada } from 'src/app/Clases/EstructuraEntrada';
import { Subscription } from 'rxjs';
import { ConfiguracionService } from 'src/app/Core/Configuracion/configuracion.service';
import { FormularioConfiguracion } from 'src/app/Clases/EstructuraConfiguracion';
import { InvitadosService } from 'src/app/Modales/invitado/fw-modal-invitados/invitado.service';

@Component({
  selector: 'app-fw-invitados',
  templateUrl: './fw-invitados.page.html',
  styleUrls: ['./fw-invitados.page.scss']
})
export class FwInvitadosPage implements OnInit, OnDestroy {

  tCodFormularioActual: string = "fInvitados";
  tPermisoAgregar: boolean = false;
  tPermisoEditar: boolean = false;
  tPermisoEliminar: boolean = false;
  tHabilitarGuardar: boolean = false;
  tHabilitarEliminar: boolean = false;

  tUsuarioActual: Usuario = new Usuario();
  tEmpresaActual: Empresas = new Empresas();
  tEmpresa$: Subscription;
  tUsuario$: Subscription;

  tTitulo: string = "Gestión de invitación Usuarios";
  tNuevo: boolean = true;
  tPosicion: number = 0;
  tPagina: number = 1;
  tTamanoPag: number = 5;

  tInvitado: Usuario = new Usuario;

  tFiltros: Filtros[] = [];
  tFiltroActual: Filtros = new Filtros();
  tFiltroBusqueda: string = "";

  tGruposNuevo: GruposUsuario[] = [];
  tGrupos: GruposUsuario[] = [];

  tConfiguracionFormulario: FormularioConfiguracion = new FormularioConfiguracion();
  tConfiguracion: Configuracion = new Configuracion();
  tConfiguracion$: Subscription;
  tStrConfiguracion: string = 'Invitados/Invitados.Config.json';

  tHayConfiguracionWs$: Subscription;

  constructor(public Ws: ClsServiciosService,
    private router: Router,
    private tSesion: SessionService,
    private tConfiguracionService: ConfiguracionService,
    private toastr: ToastrService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService,
    private tConfServices: ConfirmacionService,
    private tInvitadosService: InvitadosService) {

    this.tFiltroBusqueda = "";

    this.tFiltros = [

      { Codfiltro: "Codusuario", Filtro: "Código" },
      { Codfiltro: "Descripcion", Filtro: "Descripcion" },

    ]

  }

  ngOnInit() {

    this.tConfiguracion$ = this.tConfiguracionService
      .Consumir_Obtener_Configuracion(this.tStrConfiguracion)
      .subscribe((tConfiguracion: FormularioConfiguracion) => {

        this.tConfiguracionFormulario = tConfiguracion;
        this.tTitulo = this.tConfiguracionFormulario.Titulo;
        this.tErroresServices.tTituloMsj = this.tConfiguracionFormulario.ModalMensaje.Titulo; //"Practica - Clasificación2 de artículos";
        this.tMsjServices.tTituloMsj = this.tConfiguracionFormulario.ModalMensaje.Titulo; //"Practica - Clasificación2 de artículos";
        this.tConfServices.tTituloConf = this.tConfiguracionFormulario.ModalConfirmacion.Titulo; //"Practica - Clasificación2 de artículos";

      });

    this.tEmpresa$ = this.tSesion.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    })

    this.tUsuario$ = this.tSesion.tUsuarioActual$.subscribe((tUsuario: Usuario) => {

      this.tUsuarioActual = tUsuario;

    })

    var tTienePermiso: boolean = this.tSesion.ObtenerPermisoAcceso(this.tCodFormularioActual);
    this.tPermisoAgregar = this.tSesion.ObtenerPermisoAgregar();
    this.tPermisoEditar = this.tSesion.ObtenerPermisoEditar();
    this.tPermisoEliminar = this.tSesion.ObtenerPermisoEliminar();
    this.tHabilitarEliminar = this.tPermisoEliminar;

    if (tTienePermiso == true) {

      this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
        .subscribe((tHayConfig: boolean) => {

          if (tHayConfig === true) {

            this.Buscar_Grupos();
            this.Nuevo();

          }

        });

    }
    else {

      this.tMsjServices.AbrirModalMsj(environment.msjSinAcceso);
      this.router.navigate(['/Home']);

    }

  }

  ngOnDestroy() {

    this.tEmpresa$.unsubscribe();
    this.tUsuario$.unsubscribe();
    this.tConfiguracion$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();

  }

  //#region "Funciones Modal"

  AbrirModalUsuario(): void {


    this.tInvitadosService.AbrirModal().then(result => {

      if (result.Resultado == "S") {

        var tUsuarioModal: Usuario = result.Invitado;
        this.BuscarUsuario(tUsuarioModal.Codusuario);

      }

    }, reason => {



    });

  }
  //#endregion

  //#region "Funciones de busqueda"

  BuscarUsuario(tCodusuario: string): void {

    this.Ws
      .Usuario()
      .Consumir_Obtener_UsuarioInvitadosGrupo2(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        tCodusuario)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tNuevo = false;
          this.tHabilitarGuardar = this.tPermisoEditar;
          this.tInvitado = Respuesta.Datos[0] || new Usuario();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Buscar_Grupos(): void {

    this.Ws
      .Grupo()
      .Consumir_Obtener_GrupoOculto(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa,
        "N")
      .subscribe(Respuesta => {

        this.tGrupos = [];

        if (Respuesta.Resultado == "N") {

          // this.tMsjService.AbrirModalMsj(Respuesta.Mensaje);
          //

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.tGrupos = Respuesta.Datos;
          this.tInvitado.LGrupos = this.tGrupos || [];

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  //#region "CRUD"

  Nuevo_Usuario(): void {

    var tCrearEmpresaUsuario: UsuarioEmpresasEntrada;
    tCrearEmpresaUsuario = new UsuarioEmpresasEntrada();
    tCrearEmpresaUsuario = {
      "Id": 0,
      "Corporacion": this.tEmpresaActual.Corporacion,
      "Empresa": this.tEmpresaActual.Empresa,
      "Codusuario": this.tInvitado.Codusuario,
      "Nativo": "N",
      "Usuariocreador": this.tUsuarioActual.Codusuario,
      "Usuariomodificador": this.tUsuarioActual.Codusuario,
      "LUsuario_Grupos": []
    }

    for (let tGrupo of this.tInvitado.LGrupos) {

      if (tGrupo.Pertenece == 1) {

        var tCrearGrupoUsuario: UsuarioGruposEntrada;
        tCrearGrupoUsuario = {
          "Id": 0,
          "Corporacion": this.tEmpresaActual.Corporacion,
          "Empresa": this.tEmpresaActual.Empresa,
          "Codusuario": this.tInvitado.Codusuario,
          "Codgrupo": tGrupo.Codgrupo,
          "Usuariocreador": this.tUsuarioActual.Codusuario,
          "Usuariomodificador": this.tUsuarioActual.Codusuario
        }

        tCrearEmpresaUsuario.LUsuario_Grupos.push(tCrearGrupoUsuario);

      }


    }

    this.Ws.Usuario()
      .Consumir_Crear_Invitar_Usuario(tCrearEmpresaUsuario)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.toastr.clear();
          this.toastr.success('El invitado se ha creado de manera exitosa!');
          this.Nuevo();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Modificar_Usuario(): void {

    var tModificarEmpresaUsuario: UsuarioEmpresasEntrada;
    tModificarEmpresaUsuario = {
      "Id": 0,
      "Corporacion": this.tInvitado.Corporacion,
      "Empresa": this.tInvitado.Empresa,
      "Codusuario": this.tInvitado.Codusuario,
      "Nativo": "N",
      "Usuariocreador": this.tUsuarioActual.Codusuario,
      "Usuariomodificador": this.tUsuarioActual.Codusuario,
      "LUsuario_Grupos": []
    }

    for (let tGrupo of this.tInvitado.LGrupos) {

      if (tGrupo.Pertenece == 1) {

        var tModificarGrupoUsuario: UsuarioGruposEntrada;
        tModificarGrupoUsuario = {
          "Id": 0,
          "Corporacion": this.tInvitado.Corporacion,
          "Empresa": this.tInvitado.Empresa,
          "Codusuario": this.tInvitado.Codusuario,
          "Codgrupo": tGrupo.Codgrupo,
          "Usuariocreador": this.tUsuarioActual.Codusuario,
          "Usuariomodificador": this.tUsuarioActual.Codusuario
        }

        tModificarEmpresaUsuario.LUsuario_Grupos.push(tModificarGrupoUsuario);

      }


    }

    this.Ws.Usuario()
      .Consumir_Modificar_Invitar_Usuario(tModificarEmpresaUsuario)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.toastr.clear();
          this.toastr.success('El invitado se ha modificado de manera exitosa!');
          this.tFiltroActual = this.tFiltros[0];
          this.Nuevo();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Eliminar_Usuario() {

    var tEliminarUsuario: UsuarioEmpresasEntrada;
    tEliminarUsuario = {
      "Id": 0,
      "Corporacion": this.tInvitado.Corporacion,
      "Empresa": this.tInvitado.Empresa,
      "Codusuario": this.tInvitado.Codusuario,
      "Nativo": "N",
      "Usuariocreador": this.tUsuarioActual.Codusuario,
      "Usuariomodificador": this.tUsuarioActual.Codusuario,
      "LUsuario_Grupos": []
    }

    this.Ws.Usuario().Consumir_Eliminar_Invitar_Usuario(tEliminarUsuario).subscribe(Respuesta => {

      if (Respuesta.Resultado == "N") {

        this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);

      }
      else if (Respuesta.Resultado == "E") {

        this.tMsjServices.AbrirModalMsj(environment.msjError);

      }
      else {

        this.toastr.clear();
        this.toastr.success('El invitado se ha eliminado de manera exitosa!');
        this.Nuevo();

      }

    }, error => {

      this.tErroresServices.MostrarError(error);

    });

  }

  //#endregion

  Nuevo(): void {

    this.toastr.clear();
    this.toastr.info('Por favor llene los datos necesarios para registrar un nuevo Invitado!');
    this.tHabilitarGuardar = this.tPermisoAgregar;
    this.tNuevo = true;
    this.tInvitado = new Usuario();
    this.tInvitado.LGrupos = this.tGrupos || [];

  }

  Cancelar(): void {

    this.Nuevo();

  }

  Eliminar() {

    this.tConfServices
      .AbrirModalConf("¿Confirma eliminar el usuario?")
      .then(Resultado => {

        switch (Resultado) {
          case "S": {

            this.Eliminar_Usuario();
            break;

          }
        }

      });

  }

  Agregar(): void {

    switch (this.tInvitado.Nativo) {
      case "": {

        this.tConfServices
          .AbrirModalConf("Confirma crear el nuevo usuario con los datos suministrados.")
          .then(Resultado => {

            switch (Resultado) {
              case "S": {

                this.Nuevo_Usuario();
                break;

              }
            }

          });
        break;

      }
      default: {

        this.tConfServices
          .AbrirModalConf("Confirma modificar el usuario con los datos suministrados.")
          .then(Resultado => {

            switch (Resultado) {
              case "S": {

                this.Nuevo_Usuario();
                break;

              }
            }

          });
        break;

      }
    }

  }

}
