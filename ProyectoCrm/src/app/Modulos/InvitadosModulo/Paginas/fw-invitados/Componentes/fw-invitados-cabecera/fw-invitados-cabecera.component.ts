import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Usuario } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-invitados-cabecera',
  templateUrl: './fw-invitados-cabecera.component.html',
  styleUrls: ['./fw-invitados-cabecera.component.scss']
})
export class FwInvitadosCabeceraComponent implements OnInit {

  @Input('Invitado') tInvitado: Usuario = new Usuario();
  @Input('Nuevo') tNuevo: boolean = false;
  @Input('Editar') tEditar: boolean = false;

  @Output('AbrirModal') tAbrir: EventEmitter<string> = new EventEmitter();
  @Output('Buscar') tBuscar: EventEmitter<string> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  AbrirModal(){
    this.tAbrir.emit('');
  }

  Buscar(){

    if(this.tInvitado.Codusuario === '') { return; }

    this.tBuscar.emit('');

  }

}
