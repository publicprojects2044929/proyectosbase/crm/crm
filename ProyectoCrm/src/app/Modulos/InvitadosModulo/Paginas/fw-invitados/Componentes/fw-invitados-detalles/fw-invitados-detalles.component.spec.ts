import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwInvitadosDetallesComponent } from './fw-invitados-detalles.component';

describe('FwInvitadosDetallesComponent', () => {
  let component: FwInvitadosDetallesComponent;
  let fixture: ComponentFixture<FwInvitadosDetallesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwInvitadosDetallesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwInvitadosDetallesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
