import { Component, OnInit, Input } from '@angular/core';
import { GruposUsuario } from 'src/app/Clases/Estructura';
import { PermisosService } from 'src/app/Modales/permisos/fw-modal-permisos/permisos.service';

@Component({
  selector: 'app-fw-invitados-detalles',
  templateUrl: './fw-invitados-detalles.component.html',
  styleUrls: ['./fw-invitados-detalles.component.scss']
})
export class FwInvitadosDetallesComponent implements OnInit {

  @Input('Grupos') tGrupos: GruposUsuario[] = [];
  @Input('Eliminar') tEliminar: boolean = false;

  constructor(private tPermisosService: PermisosService) { }

  ngOnInit(): void {
  }

  AbrirModalPermisos(tGrupo: GruposUsuario): void {

    this.tPermisosService.AbrirModal(tGrupo).then(Respuesta => {

    });

  }

  Grupo_Pertenece(tCheckbox: any, tGrupo_Sel: GruposUsuario): void {

    if (tCheckbox == true) {

      tGrupo_Sel.Pertenece = 1;

    }
    else {

      tGrupo_Sel.Pertenece = 0;

    }

  }

}
