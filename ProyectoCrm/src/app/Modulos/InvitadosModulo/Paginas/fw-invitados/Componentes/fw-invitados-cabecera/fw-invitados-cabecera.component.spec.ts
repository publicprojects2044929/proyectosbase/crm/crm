import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwInvitadosCabeceraComponent } from './fw-invitados-cabecera.component';

describe('FwInvitadosCabeceraComponent', () => {
  let component: FwInvitadosCabeceraComponent;
  let fixture: ComponentFixture<FwInvitadosCabeceraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwInvitadosCabeceraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwInvitadosCabeceraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
