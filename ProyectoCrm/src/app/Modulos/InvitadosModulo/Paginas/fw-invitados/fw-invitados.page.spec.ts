import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwInvitadosPage } from './fw-invitados.page';

describe('FwInvitadosPage', () => {
  let component: FwInvitadosPage;
  let fixture: ComponentFixture<FwInvitadosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwInvitadosPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwInvitadosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
