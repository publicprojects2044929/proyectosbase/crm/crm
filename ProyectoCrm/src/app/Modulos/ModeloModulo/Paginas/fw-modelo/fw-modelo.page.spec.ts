import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwModeloPage } from './fw-modelo.page';

describe('FwModeloPage', () => {
  let component: FwModeloPage;
  let fixture: ComponentFixture<FwModeloPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwModeloPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwModeloPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
