import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Modelo, Fabricante, Marca } from 'src/app/Clases/Estructura';
import { FabricantesService } from 'src/app/Modales/fabricante/fw-modal-fabricantes/fabricantes.service';
import { MarcasService } from 'src/app/Modales/marca/fw-modal-marcas/marcas.service';

@Component({
  selector: 'app-fw-modelo-detalles',
  templateUrl: './fw-modelo-detalles.component.html',
  styleUrls: ['./fw-modelo-detalles.component.scss']
})
export class FwModeloDetallesComponent implements OnInit, OnChanges {

  @Input('Modelo') tModelo: Modelo = new Modelo();
  @Input('Fabricantes') tFabricantes: Fabricante[] = [];
  @Input('Nuevo') tNuevo: boolean = false;

  tFabricanteActual: Fabricante = new Fabricante();

  tMarcas: Marca[] = [];
  tMarcaActual: Marca = new Marca();

  constructor(private tFabricanteService: FabricantesService,
    private tMarcaService: MarcasService) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {

    if ('tFabricantes' in changes) {

      this.tFabricanteActual = this.tFabricantes[0] || new Fabricante();
      this.Seleccionar_Fabricante();

    }

    if ('tModelo' in changes) {

      this.tFabricanteActual = this.tFabricantes.find(tFabricante => {

        return tFabricante.Corporacion === this.tModelo.Corporacion &&
          tFabricante.Empresa === this.tModelo.Empresa &&
          tFabricante.Codfabricante === this.tModelo.Codfabricante

      }) || this.tFabricantes[0] || new Fabricante();

      this.tMarcas = this.tFabricanteActual.LMarcas || [];

      this.tMarcaActual = this.tMarcas.find(tMarca => {
        return tMarca.Corporacion === this.tModelo.Corporacion &&
          tMarca.Empresa === this.tModelo.Empresa &&
          tMarca.Codfabricante === this.tModelo.Codfabricante &&
          tMarca.Codmarca === this.tModelo.Codmarca
      }) || this.tMarcas[0] || new Marca();

      if (this.tModelo.Codmodelo === '') {

        this.Seleccionar_Marca();

      }

    }

  }

  //#region "Funciones Modal"

  AbrirModalFabricante(): void {

    this.tFabricanteService
      .AbrirModal()
      .then(result => {

        if (result.Resultado == "S") {

          var tFabricanteModal: Fabricante = result.Fabricante;
          this.tFabricanteActual = this.tFabricantes.find(tFabricante => {

            return tFabricante.Corporacion === tFabricanteModal.Corporacion &&
              tFabricante.Empresa === tFabricanteModal.Empresa &&
              tFabricante.Codfabricante === tFabricanteModal.Codfabricante

          }) || this.tFabricantes[0] || new Fabricante();
          this.Seleccionar_Fabricante();

        }

      }, reason => {



      });

  }

  AbrirModalMarca(): void {

    this.tMarcaService
      .AbrirModal(this.tFabricanteActual)
      .then(result => {

        if (result.Resultado == "S") {

          var tMarcaModal: Marca = result.Marca;
          this.tMarcaActual = this.tMarcas.find(tMarca => {
            return tMarca.Corporacion === tMarcaModal.Corporacion &&
              tMarca.Empresa === tMarcaModal.Empresa &&
              tMarca.Codfabricante === tMarcaModal.Codfabricante &&
              tMarca.Codmarca === tMarcaModal.Codmarca
          }) || this.tMarcas[0] || new Marca();
          this.Seleccionar_Marca();

        }

      }, reason => {



      });

  }

  //#endregion

  Seleccionar_Fabricante(): void {

    this.tMarcas = this.tFabricanteActual.LMarcas || [];
    this.tMarcaActual = new Marca();
    this.tMarcaActual = this.tMarcas[0] || new Marca();
    this.Seleccionar_Marca();

  }

  Seleccionar_Marca() {
    this.tModelo.Codfabricante = this.tMarcaActual.Codfabricante;
    this.tModelo.Codmarca = this.tMarcaActual.Codmarca;
  }

}
