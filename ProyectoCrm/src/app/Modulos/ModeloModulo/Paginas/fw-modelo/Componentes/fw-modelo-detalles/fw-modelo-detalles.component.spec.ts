import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwModeloDetallesComponent } from './fw-modelo-detalles.component';

describe('FwModeloDetallesComponent', () => {
  let component: FwModeloDetallesComponent;
  let fixture: ComponentFixture<FwModeloDetallesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwModeloDetallesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwModeloDetallesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
