import { Component, OnInit, Input } from '@angular/core';
import { Modelo } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-modelo-cabecera',
  templateUrl: './fw-modelo-cabecera.component.html',
  styleUrls: ['./fw-modelo-cabecera.component.scss']
})
export class FwModeloCabeceraComponent implements OnInit {

  @Input('Modelo') tModelo: Modelo = new Modelo();

  constructor() { }

  ngOnInit(): void {
  }

}
