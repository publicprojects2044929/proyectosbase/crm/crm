import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwModeloCabeceraComponent } from './fw-modelo-cabecera.component';

describe('FwModeloCabeceraComponent', () => {
  let component: FwModeloCabeceraComponent;
  let fixture: ComponentFixture<FwModeloCabeceraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwModeloCabeceraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwModeloCabeceraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
