import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { FormularioConfiguracion } from 'src/app/Clases/EstructuraConfiguracion';
import { Usuario, Empresas, Modelo, Formulario, Filtros, Fabricante, Marca } from 'src/app/Clases/Estructura';
import { SessionService } from 'src/app/Core/Session/session.service';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { ToastrService } from 'ngx-toastr';
import { ErroresService } from 'src/app/Modales/mensajes/fw-modal-msj/errores.service';
import { MensajesService } from 'src/app/Modales/mensajes/fw-modal-msj/mensajes.service';
import { ConfirmacionService } from 'src/app/Modales/confirmacion/fw-modal-confirmacion/confirmacion.service';
import { FabricantesService } from 'src/app/Modales/fabricante/fw-modal-fabricantes/fabricantes.service';
import { MarcasService } from 'src/app/Modales/marca/fw-modal-marcas/marcas.service';
import { ModeloEntrada } from 'src/app/Clases/EstructuraEntrada';
import { environment } from 'src/environments/environment';
import { Subscription } from 'rxjs';
import { ConfiguracionService } from 'src/app/Core/Configuracion/configuracion.service';

@Component({
  selector: 'app-fw-modelo',
  templateUrl: './fw-modelo.page.html',
  styleUrls: ['./fw-modelo.page.scss']
})

export class FwModeloPage implements OnInit, OnDestroy {

  tCodFormularioActual: string = "fModelo";
  tConfiguracionFormulario: FormularioConfiguracion = new FormularioConfiguracion();
  tPermisoAgregar: boolean = false;
  tPermisoEditar: boolean = false;
  tPermisoEliminar: boolean = false;
  tHabilitarGuardar: boolean = false;

  tUsuarioActual: Usuario = new Usuario();
  tEmpresaActual: Empresas = new Empresas();
  tEmpresa$: Subscription;
  tUsuario$: Subscription;

  tTitulo: string = "Modelos";
  tNuevo: boolean = true;
  tPosicion: number = 0;
  tPagina: number = 1;
  tTamanoPag: number = 5;

  tModelos: Modelo[] = [];
  tModeloActual: Modelo = new Modelo();

  tFormularios: Formulario[] = [];

  tFiltros: Filtros[] = [];
  tFiltroActual: Filtros = new Filtros();
  tFiltroBusqueda: string = "";

  tFabricantes: Fabricante[] = [];

  tConfiguracion$: Subscription;
  tHayConfiguracionWs$: Subscription;
  tStrConfiguracion: string = 'Modelo/Modelo.Config.json';

  constructor(public router: Router,
    private tSesion: SessionService,
    private tConfiguracionService: ConfiguracionService,
    public Ws: ClsServiciosService,
    private toastr: ToastrService,
    private tErroresServices: ErroresService,
    private tMsjServices: MensajesService,
    private tConfServices: ConfirmacionService) {

    this.tFiltroBusqueda = "";
    this.tPosicion = 0;

    this.tFiltros = [

      { Codfiltro: "Codmodelo", Filtro: "Modelo" },
      { Codfiltro: "Codmarca", Filtro: "Marca" },
      { Codfiltro: "Codfabricante", Filtro: "Fabricante" },
      { Codfiltro: "Descripcion", Filtro: "Descripción" },

    ]

  }

  ngOnInit() {

    this.tConfiguracion$ = this.tConfiguracionService
      .Consumir_Obtener_Configuracion(this.tStrConfiguracion)
      .subscribe((tConfiguracion: FormularioConfiguracion) => {

        this.tConfiguracionFormulario = tConfiguracion;
        this.tTitulo = this.tConfiguracionFormulario.Titulo;
        this.tErroresServices.tTituloMsj = this.tConfiguracionFormulario.ModalMensaje.Titulo;
        this.tMsjServices.tTituloMsj = this.tConfiguracionFormulario.ModalMensaje.Titulo;
        this.tConfServices.tTituloConf = this.tConfiguracionFormulario.ModalConfirmacion.Titulo;

      });

    this.tEmpresa$ = this.tSesion.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    })

    this.tUsuario$ = this.tSesion.tUsuarioActual$.subscribe((tUsuario: Usuario) => {

      this.tUsuarioActual = tUsuario;

    })

    var tTienePermiso: boolean = this.tSesion.ObtenerPermisoAcceso(this.tCodFormularioActual);
    this.tPermisoAgregar = this.tSesion.ObtenerPermisoAgregar();
    this.tPermisoEditar = this.tSesion.ObtenerPermisoEditar();
    this.tPermisoEliminar = this.tSesion.ObtenerPermisoEliminar();

    if (tTienePermiso == true) {

      this.tHayConfiguracionWs$ = this.Ws.tHayConfig$
        .subscribe((tHayConfig: boolean) => {

          if (tHayConfig === true) {

            this.Buscar_Fabricantes();
            this.Buscar_Modelos();
            this.Nuevo();

          }

        });

    }
    else {

      this.tMsjServices.AbrirModalMsj(environment.msjSinAcceso);
      this.router.navigate(['/Home']);

    }

  }

  ngOnDestroy() {

    this.tEmpresa$.unsubscribe();
    this.tUsuario$.unsubscribe();
    this.tConfiguracion$.unsubscribe();
    this.tHayConfiguracionWs$.unsubscribe();

  }

  //#region "Funciones de busqueda"

  Buscar_Modelos(): void {

    this.Ws.Modelo()
      .Consumir_Obtener_Modelo(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa)
      .subscribe(Respuesta => {

        this.tModelos = [];

        if (Respuesta.Resultado == "N") {

          // this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);
          //

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.tModelos = Respuesta.Datos;
          this.tFiltroActual = this.tFiltros[0];

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Buscar_Fabricantes(): void {

    this.Ws.Fabricante()
      .Consumir_Obtener_FabricanteFull(this.tEmpresaActual.Corporacion,
        this.tEmpresaActual.Empresa)
      .subscribe(Respuesta => {

        this.tFabricantes = [];

        if (Respuesta.Resultado == "N") {

          // this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);
          //

        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          this.tFabricantes = Respuesta.Datos;

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  //#region "CRUD"

  Nuevo_Modelo(): void {

    var tNuevoModelo: ModeloEntrada;
    tNuevoModelo = {
      "Id": 0,
      "Corporacion": this.tEmpresaActual.Corporacion,
      "Empresa": this.tEmpresaActual.Empresa,
      "Codfabricante": this.tModeloActual.Codfabricante,
      "Codmarca": this.tModeloActual.Codmarca,
      "Codmodelo": "",
      "Descripcion": this.tModeloActual.Descripcion,
      "Usuariocreador": this.tUsuarioActual.Codusuario,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }

    this.Ws.Modelo()
      .Consumir_Crear_Modelo(tNuevoModelo)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.toastr.success('El modelo se ha creado de manera exitosa!');
          this.tFiltroActual = this.tFiltros[0];
          this.Buscar_Modelos();
          this.Nuevo();

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  Modificar_Modelo(): void {

    var tModificarModelo: ModeloEntrada;
    tModificarModelo = {
      "Id": 0,
      "Corporacion": this.tModeloActual.Corporacion,
      "Empresa": this.tModeloActual.Empresa,
      "Codfabricante": this.tModeloActual.Codfabricante,
      "Codmarca": this.tModeloActual.Codmarca,
      "Codmodelo": this.tModeloActual.Codmodelo,
      "Descripcion": this.tModeloActual.Descripcion,
      "Usuariocreador": this.tModeloActual.Usuariocreador,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }



    this.Ws.Modelo()
      .Consumir_Modificar_Modelo(tModificarModelo)
      .subscribe(Respuesta => {


        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.toastr.success('El modelo se ha modificado de manera exitosa!');
          this.tFiltroActual = this.tFiltros[0];
          this.Buscar_Modelos();
          this.Nuevo();

        }

      }, error => {
        this.tErroresServices.MostrarError(error);

      });

  }

  Eliminar_Modelo(): void {

    var tEliminarModelo: ModeloEntrada;
    tEliminarModelo = {
      "Id": 0,
      "Corporacion": this.tModeloActual.Corporacion,
      "Empresa": this.tModeloActual.Empresa,
      "Codfabricante": this.tModeloActual.Codfabricante,
      "Codmarca": this.tModeloActual.Codmarca,
      "Codmodelo": this.tModeloActual.Codmodelo,
      "Descripcion": this.tModeloActual.Descripcion,
      "Usuariocreador": this.tModeloActual.Usuariocreador,
      "Usuariomodificador": this.tUsuarioActual.Codusuario
    }



    this.Ws.Modelo()
      .Consumir_Eliminar_Modelo(tEliminarModelo)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          this.tMsjServices.AbrirModalMsj(Respuesta.Mensaje);


        }
        else if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);


        }
        else {

          this.toastr.success('El modelo se ha eliminado de manera exitosa!');
          this.tFiltroActual = this.tFiltros[0];
          this.Buscar_Modelos();
          this.Nuevo();

        }

      }, error => {
        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  Nuevo(): void {

    this.tHabilitarGuardar = this.tPermisoAgregar;
    this.tNuevo = true;
    this.tModeloActual = new Modelo;

    this.toastr.info('Por favor llene los datos necesarios para registrar un nuevo Modelo!');


  }

  Seleccionar_Modelo(tModelo_Set: Modelo, tPos: number): void {

    this.tHabilitarGuardar = this.tPermisoEditar;
    this.tNuevo = false;
    this.tPosicion = tPos;
    this.tModeloActual = new Modelo();
    Object.assign(this.tModeloActual, tModelo_Set);
    this.tModeloActual.Usuariomodificador = this.tUsuarioActual.Usuariomodificador;

  }

  Seleccionar_Modelo_Eliminar(tModelo_Set: Modelo): void {

    this.tModeloActual = new Modelo();
    Object.assign(this.tModeloActual, tModelo_Set);
    this.tModeloActual.Usuariomodificador = this.tUsuarioActual.Usuariomodificador;

    this.tConfServices
      .AbrirModalConf(this.tConfiguracionFormulario.ModalConfirmacion.Eliminacion)
      .then(Resultado => {

        switch (Resultado) {
          case "S": {

            this.Eliminar_Modelo();
            break;

          }
        }

      });

  }

  Cancelar(): void {

    this.Nuevo();

  }

  ValidarFormulario(): boolean {

    let valido: boolean = false;

    if (this.tModeloActual.Descripcion == "") {

      this.toastr.warning('Debe ingresar una descripciòn!', 'Disculpe!');
      return valido;

    }

    return true;
  }

  Agregar(): void {

    if (this.ValidarFormulario() == false) {

      return;

    }

    switch (this.tNuevo) {
      case true: {

        this.tConfServices
          .AbrirModalConf(this.tConfiguracionFormulario.ModalConfirmacion.Agregar)
          .then(Resultado => {

            switch (Resultado) {
              case "S": {

                this.Nuevo_Modelo();
                break;

              }
            }

          });
        break;

      }
      default: {

        this.tConfServices
          .AbrirModalConf(this.tConfiguracionFormulario.ModalConfirmacion.Edicion)
          .then(Resultado => {

            switch (Resultado) {
              case "S": {

                this.Modificar_Modelo();
                break;

              }
            }

          });
        break;

      }
    }

  }

}
