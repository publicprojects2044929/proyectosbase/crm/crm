import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from 'src/app/Shared/shared.module';

import { ModeloRoutingModule } from './modelo-routing.module';
import { FwModeloPage } from './Paginas/fw-modelo/fw-modelo.page';
import { FwModeloCabeceraComponent } from './Paginas/fw-modelo/Componentes/fw-modelo-cabecera/fw-modelo-cabecera.component';
import { FwModeloDetallesComponent } from './Paginas/fw-modelo/Componentes/fw-modelo-detalles/fw-modelo-detalles.component';


@NgModule({
  declarations: [FwModeloPage, FwModeloCabeceraComponent, FwModeloDetallesComponent],
  imports: [
    CommonModule,
    ModeloRoutingModule,
    SharedModule,
    FormsModule,
    NgbModule
  ]
})
export class ModeloModule { }
