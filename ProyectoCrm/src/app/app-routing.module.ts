import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

import { UsuarioLogueadoGuard } from './Core/Guardias/usuario-logueado.guard';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        redirectTo: '/Login',
        pathMatch: 'full'
      },
      {
        path: 'Login',
        loadChildren: () => import('./Modulos/LoginModulo/login.module').then(m => m.LoginModule) 
      },
      {
        path: 'AjustesDeInventario',
        loadChildren: () => import('./Modulos/AjusteInventarioModulo/ajuste-inventario.module').then(m => m.AjusteInventarioModule),
        canActivate: [UsuarioLogueadoGuard]
      },
      {
        path: 'Almacen',
        loadChildren: () => import('./Modulos/AlmacenModulo/almacen.module').then(m => m.AlmacenModule),
        canActivate: [UsuarioLogueadoGuard]
      },
      {
        path: 'Articulo',
        loadChildren: () => import('./Modulos/ArticuloModulo/articulo.module').then(m => m.ArticuloModule),
        canActivate: [UsuarioLogueadoGuard]
      },
      {
        path: 'Ciudad',
        loadChildren: () => import('./Modulos/CiudadModulo/ciudad.module').then(m => m.CiudadModule),
        canActivate: [UsuarioLogueadoGuard] 
      },
      {
        path: 'Clasificacion1',
        loadChildren: () => import('./Modulos/Clasificacion1Modulo/clasificacion1.module').then(m => m.Clasificacion1Module),
        canActivate: [UsuarioLogueadoGuard]
      },
      {
        path: 'Colecciones',
        loadChildren: () => import('./Modulos/ColeccionesModulo/colecciones-modulo.module').then(m => m.ColeccionesModuloModule),
        canActivate: [UsuarioLogueadoGuard]
      },
      {
        path: 'Clasificacion2',
        loadChildren: () => import('./Modulos/Clasificacion2Modulo/clasificacion2.module').then(m => m.Clasificacion2Module),
        canActivate: [UsuarioLogueadoGuard]
      },
      {
        path: 'Clasificacion3',
        loadChildren: () => import('./Modulos/Clasificacion3Modulo/clasificacion3.module').then(m => m.Clasificacion3Module),
        canActivate: [UsuarioLogueadoGuard]
      },
      {
        path: 'Clasificacion4',
        loadChildren: () => import('./Modulos/Clasificacion4Modulo/clasificacion4.module').then(m => m.Clasificacion4Module),
        canActivate: [UsuarioLogueadoGuard]
      },
      {
        path: 'Cliente',
        loadChildren: () => import('./Modulos/ClienteModulo/cliente.module').then(m => m.ClienteModule),
        canActivate: [UsuarioLogueadoGuard]
      },
      {
        path: 'Color',
        loadChildren: () => import('./Modulos/ColorModulo/color.module').then(m => m.ColorModule),
        canActivate: [UsuarioLogueadoGuard]
      },
      {
        path: 'Condicion',
        loadChildren: () => import('./Modulos/CondicionModulo/condicion.module').then(m => m.CondicionModule),
        canActivate: [UsuarioLogueadoGuard]
      },
      {
        path: 'ConsultaArticulo',
        loadChildren: () => import('./Modulos/ConsultaArticuloModulo/consulta-articulo.module').then(m => m.ConsultaArticuloModule),
        canActivate: [UsuarioLogueadoGuard]
      },
      {
        path: 'ConsultaCliente',
        loadChildren: () => import('./Modulos/ConsultaClienteModulo/consulta-cliente.module').then(m => m.ConsultaClienteModule),
        canActivate: [UsuarioLogueadoGuard]
      },
      {
        path: 'Cotizacion',
        loadChildren: () => import('./Modulos/CotizacionModulo/cotizaciones.module').then(m => m.CotizacionesModule),
        canActivate: [UsuarioLogueadoGuard]
      },
      {
        path: 'Cupon',
        loadChildren: () => import('./Modulos/CuponModulo/cupon.module').then(m => m.CuponModule),
        canActivate: [UsuarioLogueadoGuard] 
      },
      {
        path: 'Estado',
        loadChildren: () => import('./Modulos/EstadoModulo/estado.module').then(m => m.EstadoModule),
        canActivate: [UsuarioLogueadoGuard] 
      },
      {
        path: 'EstadoCivil',
        loadChildren: () => import('./Modulos/EstadoCivilModulo/estado-civil.module').then(m => m.EstadoCivilModule),
        canActivate: [UsuarioLogueadoGuard]
      },
      {
        path: 'Fabricante',
        loadChildren: () => import('./Modulos/FabricanteModulo/fabricante.module').then(m => m.FabricanteModule),
        canActivate: [UsuarioLogueadoGuard]
      },
      {
        path: 'FormasDePagos',
        loadChildren: () => import('./Modulos/FormasDePagoModulo/formas-de-pago.module').then(m => m.FormasDePagoModule),
        canActivate: [UsuarioLogueadoGuard]
      },
      {
        path: 'Genero',
        loadChildren: () => import('./Modulos/GeneroModulo/genero.module').then(m => m.GeneroModule),
        canActivate: [UsuarioLogueadoGuard] 
      },
      {
        path: 'Home',
        loadChildren: () => import('./Modulos/HomeModulo/home.module').then(m => m.HomeModule),
        canActivate: [UsuarioLogueadoGuard] 
      },
      {
        path: 'Invitados',
        loadChildren: () => import('./Modulos/InvitadosModulo/invitados.module').then(m => m.InvitadosModule),
        canActivate: [UsuarioLogueadoGuard] 
      },
      {
        path: 'Marca',
        loadChildren: () => import('./Modulos/MarcaModulo/marcas.module').then(m => m.MarcasModule),
        canActivate: [UsuarioLogueadoGuard]
      },
      {
        path: 'Modelo',
        loadChildren: () => import('./Modulos/ModeloModulo/modelo.module').then(m => m.ModeloModule),
        canActivate: [UsuarioLogueadoGuard]
      },
      {
        path: 'Moneda',
        loadChildren: () => import('./Modulos/MonedaModulo/moneda.module').then(m => m.MonedaModule),
        canActivate: [UsuarioLogueadoGuard] 
      },
      {
        path: 'Municipio',
        loadChildren: () => import('./Modulos/MunicipioModulo/municipio.module').then(m => m.MunicipioModule),
        canActivate: [UsuarioLogueadoGuard] 
      },
      {
        path: 'Ocupacion',
        loadChildren: () => import('./Modulos/OcupacionModulo/ocupacion.module').then(m => m.OcupacionModule),
        canActivate: [UsuarioLogueadoGuard] 
      },
      {
        path: 'Pais',
        loadChildren: () => import('./Modulos/PaisModulo/pais.module').then(m => m.PaisModule),
        canActivate: [UsuarioLogueadoGuard] 
      },
      {
        path: 'Pedido',
        loadChildren: () => import('./Modulos/PedidoModulo/pedido.module').then(m => m.PedidoModule),
        canActivate: [UsuarioLogueadoGuard]
      },
      {
        path: 'Perfil',
        loadChildren: () => import('./Modulos/PerfilModulo/perfil.module').then(m => m.PerfilModule),
        canActivate: [UsuarioLogueadoGuard] 
      },
      {
        path: 'Permisos',
        loadChildren: () => import('./Modulos/PermisosModulo/permisos.module').then(m => m.PermisosModule),
        canActivate: [UsuarioLogueadoGuard] 
      },
      { 
        path: 'ReporteArticulos',
        loadChildren: () => import('./Modulos/PlaneadorArticulosModulo/planeador-articulos.module').then(m => m.PlaneadorArticulosModule),
        canActivate: [UsuarioLogueadoGuard]
      },
      { 
        path: 'ReportePedidos',
        loadChildren: () => import('./Modulos/PlaneadorPedidosModulo/planeador-pedidos.module').then(m => m.PlaneadorPedidosModule),
        canActivate: [UsuarioLogueadoGuard] 
      },
      { 
        path: 'ReporteVentas',
        loadChildren: () => import('./Modulos/PlaneadorVentasModulo/planeador-ventas.module').then(m => m.PlaneadorVentasModule),
        canActivate: [UsuarioLogueadoGuard] 
      },
      { 
        path: 'Promocion',
        loadChildren: () => import('./Modulos/PromocionModulo/promocion.module').then(m => m.PromocionModule),
        canActivate: [UsuarioLogueadoGuard] 
      },
      {
        path: 'Situacion',
        loadChildren: () => import('./Modulos/SituacionModulo/situacion.module').then(m => m.SituacionModule),
        canActivate: [UsuarioLogueadoGuard] 
      },
      {
        path: 'TiposDeAjustes',
        loadChildren: () => import('./Modulos/TipoAjusteModulo/tipo-ajuste.module').then(m => m.TipoAjusteModule),
        canActivate: [UsuarioLogueadoGuard]
      },
      {
        path: 'UnidadMedida',
        loadChildren: () => import('./Modulos/UnidadMedidaModulo/unidad-medida.module').then(m => m.UnidadMedidaModule),
        canActivate: [UsuarioLogueadoGuard]
      },
      {
        path: 'Usuarios',
        loadChildren: () => import('./Modulos/UsuariosModulo/usuarios.module').then(m => m.UsuariosModule),
        canActivate: [UsuarioLogueadoGuard] 
      },
      {
        path: 'Configuraciones',
        loadChildren: () => import('./Modulos/ConfiguracionModulo/configuracion-modulo.module').then(m => m.ConfiguracionModuloModule),
        canActivate: [UsuarioLogueadoGuard] 
      },
      {
        path: 'CambiarEmpresa',
        loadChildren: () => import('./Modulos/CambiarEmpresaModulo/cambiar-empresa-modulo.module').then(m => m.CambiarEmpresaModuloModule ),
        canActivate: [UsuarioLogueadoGuard] 
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
