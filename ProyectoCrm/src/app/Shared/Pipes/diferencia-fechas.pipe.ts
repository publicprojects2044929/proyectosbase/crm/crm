import { Pipe, PipeTransform } from '@angular/core';
import { format } from 'timeago.js';

@Pipe({
  name: 'diferenciaFechas'
})
export class DiferenciaFechasPipe implements PipeTransform {

  transform(fecha: Date): String {

    let diferencia: string = "";

    diferencia = format(fecha);

    return diferencia;

  }

}
