import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'talla'
})
export class TallaPipe implements PipeTransform {

  transform(talla: string): string {
    
    let tTallaFinal: string = "NA"

    if (talla !== "") {

      tTallaFinal = talla;

    }
    
    return tTallaFinal;

  }

}
