import { Pipe, PipeTransform, ɵConsole } from '@angular/core';
import { Filtros } from 'src/app/Clases/Estructura';

@Pipe({
  name: 'buscar'
})
export class BuscarPipe implements PipeTransform {


  transform(value: any, args?: any): any {

    if (!value) {

      return;

    }

    if (!args) {

      return value;

    }

    let tFiltroBusqueda: string = args.busquedaFiltro;
    let tValorCampo: string = "";
    let tValoresFiltrados: any = [];
    let tLongitudFiltros: number = 0;

    tLongitudFiltros = args.campoFiltro.length || 0;

    if (tFiltroBusqueda == "") {

      return value;

    }

    tFiltroBusqueda = tFiltroBusqueda.toLowerCase();

    if (tLongitudFiltros > 0) {

      let tFiltrosActuales: Filtros[] = [];
      tFiltrosActuales = args.campoFiltro;

      for(let Item of value){

        for(let tFiltroActual of tFiltrosActuales){

          tValorCampo = Item[tFiltroActual.Codfiltro];

          if (tValorCampo.toLowerCase().startsWith(tFiltroBusqueda) == true) {
  
            tValoresFiltrados.push(Item);
            break;
    
          }

        }

      }

    }
    else {

      let tFiltroActual: Filtros = new Filtros;
      tFiltroActual = args.campoFiltro;

      for (let Item of value) {

        tValorCampo = Item[tFiltroActual.Codfiltro];
  
        if (tValorCampo.toLowerCase().startsWith(tFiltroBusqueda) == true) {
  
          tValoresFiltrados.push(Item);
  
        }
  
      }

    }

    return tValoresFiltrados;

  }

}
