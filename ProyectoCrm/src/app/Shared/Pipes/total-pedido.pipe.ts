import { Pipe, PipeTransform } from '@angular/core';
import { Articulo, Cliente } from 'src/app/Clases/Estructura';
import { PrecioClientePipe } from './precio-cliente.pipe'

@Pipe({
  name: 'totalPedido'
})
export class TotalPedidoPipe implements PipeTransform {

  tPrecioClientePipe: PrecioClientePipe = new PrecioClientePipe();

  transform(tArticulo: Articulo, tCliente: Cliente, tCandidad: number = 1): number {
    let tPrecio: number = this.tPrecioClientePipe.transform(tArticulo, tCliente);
    return tPrecio * tCandidad;
  }

}
