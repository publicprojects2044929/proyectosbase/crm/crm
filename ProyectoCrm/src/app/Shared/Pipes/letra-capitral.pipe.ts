import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'letraCapitral'
})
export class LetraCapitralPipe implements PipeTransform {

  transform(value: any): any {

    if (!value) {

      return;

    }

    var tPrimerCaracter: string = value.charAt(0).toUpperCase() || "";
    var tOtrosCaracter: string = value.slice(1).toLowerCase() || "";
    let tPalabraTransf: string = tPrimerCaracter + tOtrosCaracter;

    return tPalabraTransf;

  }

}
