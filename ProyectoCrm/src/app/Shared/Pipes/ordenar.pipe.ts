import { Pipe, PipeTransform } from '@angular/core';
import { isNumber, isDate } from 'util';

@Pipe({
  name: 'ordenar'
})
export class OrdenarPipe implements PipeTransform {

  transform(value: any, args?: any): any {

    if(!value){

      return;

    }

    if(!args){

      return value;

    }

    let tValoresOrdenados : [] = value;
    let tCampoOrdenamiento : string = args.CampoOrdenamiento;
    let tTipoCampo : string = args.TipoCampo;
    let tTipoOrdenamiento : string = args.TipoOrdenamiento;

    if (tTipoCampo != "Numero" && tTipoCampo != "Fecha" && tTipoCampo != "Caracter"){

      return;

    }

    if(tTipoOrdenamiento != "Desc" && tTipoOrdenamiento != "Asc"){

      return;

    }

    if (tCampoOrdenamiento != ""){

      tValoresOrdenados.sort((a, b) => {

        if (!a[tCampoOrdenamiento]){

          return 0;

        };

        if (!b[tCampoOrdenamiento]){

          return 0;

        };

        let tValorA : any = a[tCampoOrdenamiento];
        let tValorB : any = b[tCampoOrdenamiento];

        // console.log(tValorA);
        // console.log(tValorB);
        // console.log(tTipoOrdenamiento);
        // console.log(tTipoCampo);

        if(tTipoOrdenamiento == "Desc"){

          switch(tTipoCampo) { 
            case "Numero": { 
               
              // if ((isNumber(tValorA) == false) || isNumber(tValorB) == false){

              //   return 0;

              // }
  
              return tValorB - tValorA;
  
            } 
            case "Fecha": { 
              
              // console.log(isDate(tValorA));
              // console.log(isDate(tValorB));

              // if ((isDate(tValorA) == false) || isDate(tValorB) == false){

              //   return 0;

              // }
  
              return new Date(tValorB).getTime() - new Date(tValorA).getTime();
              
            } 
            default: { 
              
              if(tValorB > tValorA){

                return -1;

              }
              else{

                return 0;

              }

            } 
         } 

        }
        else{

          switch(tTipoCampo) { 
            case "Numero": { 
               
              // if ((isNumber(tValorA) == false) || isNumber(tValorB) == false){

              //   return 0;

              // }
  
              return tValorA - tValorB;
  
            } 
            case "Fecha": { 
              
              // if ((isDate(tValorA) == false) || isDate(tValorB) == false){

              //   return 0;

              // }
  
              return new Date(tValorA).getTime() - new Date(tValorB).getTime();
              
            } 
            default: { 
              
              if(tValorA > tValorB){

                return 1;

              }
              else{
                
                return -1;

              }

            } 
         } 

        }

      })

    }

    // console.log(tValoresOrdenados);

    return tValoresOrdenados;


  }

}
