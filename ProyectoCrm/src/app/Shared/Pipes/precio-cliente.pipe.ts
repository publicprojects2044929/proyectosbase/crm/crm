import { Pipe, PipeTransform } from '@angular/core';
import { Articulo, Cliente } from 'src/app/Clases/Estructura';
import { LetraCapitralPipe } from './letra-capitral.pipe';

@Pipe({
  name: 'precioCliente'
})
export class PrecioClientePipe implements PipeTransform {

  tLetraCapital: LetraCapitralPipe = new LetraCapitralPipe();

  transform(tArticulo: Articulo, tCliente: Cliente): number {

    let tPrecioCliente: string = "Preciolista";

    if (tCliente.Tipoprecio === "") {

      tPrecioCliente = "Preciolista";

    }
    else {

      tPrecioCliente = this.tLetraCapital.transform(tCliente.Tipoprecio);

    }

    return tArticulo[tPrecioCliente] || 0;
  }

}
