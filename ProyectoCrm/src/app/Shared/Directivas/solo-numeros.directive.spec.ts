import { SoloNumerosDirective } from './solo-numeros.directive';
import { ElementRef, Injectable } from '@angular/core';
import { TestBed, async } from '@angular/core/testing';

// @Injectable()
// export class MockElementRef {
//   nativeElement: {}
// }

fdescribe('SoloNumerosDirective', () => {

  let tElementRef: ElementRef;

  beforeEach(async(() => {
    
    TestBed.configureTestingModule({
      providers: [
        {provide: ElementRef, useValue: {}}
      ]
    }).compileComponents();

    tElementRef = TestBed.get(ElementRef);

  }));

  it('should create an instance', () => {
    const directive = new SoloNumerosDirective(tElementRef);
    expect(directive).toBeTruthy();
  });

});
