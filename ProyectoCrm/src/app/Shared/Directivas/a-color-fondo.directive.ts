import { Directive, Input, ElementRef, AfterViewInit, HostListener, Renderer2 } from '@angular/core';
import { ManejarColores } from 'src/app/Manejadores/cls-procedimientos';

@Directive({
  selector: '[ColorFondo]'
})
export class AColorFondoDirective implements AfterViewInit {

  @Input('ColorFondo') tColorFondo: string;
  @Input('IgnorarClick') tIgnorar: boolean = false;

  tManejarColores: ManejarColores = new ManejarColores();
  tColorTransp: string = "";
  tSeleccionado: boolean = false;
  tElement: ElementRef;

  constructor(private el: ElementRef,
    private renderer2: Renderer2) {
    this.tSeleccionado = false;
  }

  ngAfterViewInit() {
    this.tElement = this.el.nativeElement.getElementsByClassName("fa-check")[0];
    this.tColorTransp = this.tManejarColores.hexToRGB(this.tColorFondo, 0.2);
    this.el.nativeElement.style.borderColor = this.tColorFondo;
    this.onMouseLeave();
  }

  @HostListener("mouseenter") onMouseEnter() {
    this.CambiarFondo(this.tColorFondo);
  }

  @HostListener("mouseleave") onMouseLeave() {
    this.CambiarFondo(this.tColorTransp);
  }

  @HostListener("click") onClick() {

    if (this.tIgnorar) { return; }

    this.tSeleccionado = !this.tSeleccionado;

    if (this.tSeleccionado) {

      // this.renderer2.addClass(this.el.nativeElement, 'shadow');
      if (this.tElement) {
        this.renderer2.removeClass(this.tElement, 'd-none');
      }

    }
    else {

      // this.renderer2.removeClass(this.el.nativeElement, 'shadow');
      if (this.tElement) {
        this.renderer2.addClass(this.tElement, 'd-none');
      }

    }

  }

  private CambiarFondo(tColor: string) {
    this.el.nativeElement.style.backgroundColor = tColor;
  }

}
