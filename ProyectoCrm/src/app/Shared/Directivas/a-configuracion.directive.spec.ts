import { AConfiguracionDirective } from './a-configuracion.directive';
import { ElementRef, Injectable, Renderer2 } from '@angular/core';
import { TestBed, async } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';

// @Injectable()
// export class MockElementRef {
//   nativeElement: {}
// }

// @Injectable()
// export class MockRenderer{

//   Renderer2: {}

// }

// @Injectable()
// export class MockHttp{

//   http: {}

// }

fdescribe('AConfiguracionDirective', () => {

  let tElementRef: ElementRef;
  let tRender: Renderer2;
  let tHttp: HttpClient;

  beforeEach(async(() => {
    
    TestBed.configureTestingModule({
      providers: [
        {provide: ElementRef, useValue: {}},
        {provide: Renderer2, useValue: {}},
        {provide: HttpClient, useValue: {}}
      ]
    }).compileComponents();

    tElementRef = TestBed.get(ElementRef);
    tRender = TestBed.get(Renderer2);
    tHttp = TestBed.get(HttpClient);

  }));

  it('should create an instance', () => {
    const directive = new AConfiguracionDirective(tElementRef, tRender, tHttp);
    expect(directive).toBeTruthy();
  });

});
