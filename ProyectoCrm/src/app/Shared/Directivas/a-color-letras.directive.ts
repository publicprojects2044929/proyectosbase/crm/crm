import { Directive, ElementRef, AfterViewInit, Input, HostListener } from '@angular/core';

@Directive({
  selector: '[ColorLetras]'
})
export class AColorLetrasDirective implements AfterViewInit {

  @Input('ColorLetras') tColorLetras: string;

  constructor(private el: ElementRef) { }

  ngAfterViewInit(){
    this.onMouseLeave();
  }

  @HostListener("mouseenter") onMouseEnter(){
    this.CambiarColor("white");
  }

  @HostListener("mouseleave") onMouseLeave(){
    this.CambiarColor(this.tColorLetras);
  }

  private CambiarColor(tColor: string){
    this.el.nativeElement.style.color = tColor;
  }

}
