import { Directive, ElementRef, AfterViewInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { GravatarService } from 'src/app/Core/Gravatar/gravatar.service';

@Directive({
  selector: 'img[Gravatar]'
})
export class GravatarDirective implements AfterViewInit, OnChanges {

  @Input('Gravatar') tCorreo: string;

  constructor(private el: ElementRef,
    private tGravatarService: GravatarService) { }

  ngOnChanges(changes: SimpleChanges): void {

    if ('tCorreo' in changes) {
      this.Cambiar_Gravatar();
    }

  }

  ngAfterViewInit(): void {
    this.Cambiar_Gravatar();
  }

  private Cambiar_Gravatar() {

    let tGravatarStr: string = this.tGravatarService.obtenerGravatar(this.tCorreo || '');
    this.el.nativeElement.src = tGravatarStr;

  }

}
