import { Directive, OnInit, ElementRef, Renderer2, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfiguracionControl } from 'src/app/Clases/EstructuraConfiguracion';

@Directive({
  selector: 'a[appAConfiguracion]'
})

export class AConfiguracionDirective implements OnInit {

  @Input('appAConfiguracion') tControl: any = {};

  constructor(private elRef: ElementRef, private renderer: Renderer2, private http: HttpClient) { }

  ngOnInit() {

    this.http.get<ConfiguracionControl>('Configuracion/ConfiguracionFormulario/' + this.tControl.Archivo)
      .subscribe(data => {

        setTimeout(() => {

          this.AplicarConfiguracion(data);

        }, 500)

      })

  }

  private AplicarConfiguracion(tConfiguracionArch: ConfiguracionControl) {

    for (let tControlArchivo of tConfiguracionArch.Controles) {

      if (tControlArchivo.Id == this.tControl.Control) {

        this.elRef.nativeElement.textContent = tControlArchivo.Texto;
        break;

      }

    }

  }

}
