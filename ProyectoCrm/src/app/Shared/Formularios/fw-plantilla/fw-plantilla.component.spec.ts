import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwPlantillaComponent } from './fw-plantilla.component';

describe('FwPlantillaComponent', () => {
  let component: FwPlantillaComponent;
  let fixture: ComponentFixture<FwPlantillaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwPlantillaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwPlantillaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
