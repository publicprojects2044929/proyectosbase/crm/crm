import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fw-plantilla',
  templateUrl: './fw-plantilla.component.html',
  styleUrls: ['./fw-plantilla.component.scss']
})
export class FwPlantillaComponent implements OnInit {

  tMostrarMenu: boolean = true;

  constructor() { }

  ngOnInit() {
  }

  DesplegarMenu(tEstadoMenu: boolean){

    this.tMostrarMenu = tEstadoMenu;

  }

}
