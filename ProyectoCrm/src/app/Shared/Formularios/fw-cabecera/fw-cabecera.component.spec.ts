import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwCabeceraComponent } from './fw-cabecera.component';

describe('FwCabeceraComponent', () => {
  let component: FwCabeceraComponent;
  let fixture: ComponentFixture<FwCabeceraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwCabeceraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwCabeceraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
