import { Component, OnInit, OnDestroy, Output, EventEmitter, Input } from '@angular/core';

import { SessionService } from 'src/app/Core/Session/session.service'
import { Respuesta, Empresas, Usuario, VentaFormasPagos, Venta } from 'src/app/Clases/Estructura';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { SocketService } from 'src/app/Core/Socket/socket.service';
import { DetallesVentaService } from 'src/app/Modales/detalles-venta/fw-modal-detalles-venta/detalles-venta.service';
import { ClsServiciosService } from 'src/app/Core/Servicios/cls-servicios.service';
import { environment } from 'src/environments/environment';
import { ErroresService } from 'src/app/Modales/mensajes/fw-modal-msj/errores.service';
import { ConfirmacionService } from 'src/app/Modales/confirmacion/fw-modal-confirmacion/confirmacion.service';
import { MensajesService } from 'src/app/Modales/mensajes/fw-modal-msj/mensajes.service';

@Component({
  selector: 'app-fw-cabecera',
  templateUrl: './fw-cabecera.component.html',
  styleUrls: ['./fw-cabecera.component.scss']
})
export class FwCabeceraComponent implements OnInit, OnDestroy {

  @Output('DesplegarMenu') tDesplegarMenu: EventEmitter<boolean> = new EventEmitter();
  @Input('EstadoMenu') tEstadoMenu: boolean = true;

  tEmpresaActual: Empresas = new Empresas();
  tUsuarioActual: Usuario = new Usuario();
  tPagosPendientes: VentaFormasPagos[] = [];

  tHay$: Subscription;
  tUsuario$: Subscription;
  tEmpresa$: Subscription;
  tSocketPagosPend$: Subscription;
  tHayConfiguracionSocket$: Subscription;

  constructor(public router: Router,
    private Ws: ClsServiciosService,
    private tSession: SessionService,
    private tSocket: SocketService,
    private tDetallesVentaService: DetallesVentaService,
    private tErroresServices: ErroresService,
    private tConfServices: ConfirmacionService,
    private tMsjServices: MensajesService) {

  }

  ngOnInit() {

    this.tHay$ = this.tSession.tHayUsuario$.subscribe((tHay: boolean) => {

      if (tHay === false) {

        this.router.navigate(['/']);

      }

    })

    this.tEmpresa$ = this.tSession.tEmpresaActual$.subscribe((tEmpresa: Empresas) => {

      this.tEmpresaActual = tEmpresa;

    })

    this.tUsuario$ = this.tSession.tUsuarioActual$.subscribe((tUsuario: Usuario) => {

      this.tUsuarioActual = tUsuario;

    })

    this.tHayConfiguracionSocket$ = this.tSocket.tHayConfig$
      .subscribe((tHayConfig: boolean) => {

        if (tHayConfig === true) {

          this.tSocket.emit("Ventas:PagosPendientes");

          this.tSocketPagosPend$ = this.tSocket
            .listen("Ventas:PagosPendientes")
            .subscribe((tRespuesta: Respuesta) => {

              this.tPagosPendientes = tRespuesta.Datos || [];

            });

        }

      });

  }

  ngOnDestroy() {

    this.tHay$.unsubscribe();
    this.tEmpresa$.unsubscribe();
    this.tUsuario$.unsubscribe();
    this.tSocketPagosPend$.unsubscribe();

  }

  //#region  "Busqueda"

  BuscarVenta(tVentaFormasPagoSel: VentaFormasPagos) {

    this.Ws
      .Venta()
      .Consumir_Obtener_VentaFull(tVentaFormasPagoSel.Corporacion,
        tVentaFormasPagoSel.Empresa,
        tVentaFormasPagoSel.Codventa)
      .subscribe(Respuesta => {

        if (Respuesta.Resultado == "N") {

          Respuesta.Mensaje;

        }

        if (Respuesta.Resultado == "E") {

          this.tMsjServices.AbrirModalMsj(environment.msjError);

        }
        else {

          let tVenta: Venta = Respuesta.Datos[0] || new Venta();

          this.tDetallesVentaService
            .AbrirModalConf(tVenta)
            .then(result => {



            }, reason => {



            });

        }

      }, error => {

        this.tErroresServices.MostrarError(error);

      });

  }

  //#endregion

  IrPerfil() {

    event.preventDefault();
    this.router.navigate(['/Perfil']);

  }

  IrCambiarEmpresa() {

    event.preventDefault();
    this.router.navigate(['/CambiarEmpresa']);

  }

  Salir() {

    event.preventDefault();
    this.tSession.Signout();

  }

  AbrirModalDetalles(tVentaFormasPagoSel: VentaFormasPagos): void {

    event.preventDefault();
    this.BuscarVenta(tVentaFormasPagoSel);

  }

  DesplegarMenu(){

    this.tDesplegarMenu.emit(!this.tEstadoMenu);

  }

}
