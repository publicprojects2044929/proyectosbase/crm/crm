import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { Marcador } from 'src/app/Clases/EstructuraMapa';

@Component({
  selector: 'app-fw-mapa',
  templateUrl: './fw-mapa.component.html',
  styleUrls: ['./fw-mapa.component.scss']
})
export class FwMapaComponent implements OnInit, OnChanges {

  @Input('Marcadores') tMarcadores: Marcador[] = [];
  @Input('Zoom') tZoom: number = 20;
  tMarcadorPrincipal: Marcador = new Marcador();

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(){

    this.tMarcadorPrincipal = new Marcador();
    this.tMarcadorPrincipal.Latitud = this.tMarcadores[0].Latitud;
    this.tMarcadorPrincipal.Longitud = this.tMarcadores[0].Longitud;

  }

}
