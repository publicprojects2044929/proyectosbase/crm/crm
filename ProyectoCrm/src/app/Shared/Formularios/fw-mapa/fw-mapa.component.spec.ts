import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwMapaComponent } from './fw-mapa.component';

describe('FwMapaComponent', () => {
  let component: FwMapaComponent;
  let fixture: ComponentFixture<FwMapaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwMapaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwMapaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
