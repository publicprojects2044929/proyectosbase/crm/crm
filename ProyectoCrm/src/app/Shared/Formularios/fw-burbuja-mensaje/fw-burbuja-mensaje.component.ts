import { Component, OnInit, OnChanges, Input, SimpleChanges } from '@angular/core';
import { TicketMensajes, Usuario, Empresas, Cliente } from 'src/app/Clases/Estructura';

@Component({
  selector: 'app-fw-burbuja-mensaje',
  templateUrl: './fw-burbuja-mensaje.component.html',
  styleUrls: ['./fw-burbuja-mensaje.component.scss']
})
export class FwBurbujaMensajeComponent implements OnInit, OnChanges {

  @Input("Mensaje") tMensaje: TicketMensajes = new TicketMensajes();
  @Input("Cliente") tCliente: Cliente = new Cliente();
  @Input("Usuarios") tUsuarios: Usuario[] = [];
  @Input("Usuario") tUsuario: Usuario = new Usuario();
  @Input("Empresa") tEmpresa: Empresas = new Empresas();

  tCorreoMensaje: string = ""
  tOrigenMensaje: string = ""

  constructor() { }

  ngOnInit(): void {

    this.Actualizar_Datos();

  }

  ngOnChanges(changes: SimpleChanges): void {

    if ('tCliente' in changes) {
      this.Actualizar_Datos();
    }

    if ('tUsuarios' in changes) {
      this.Actualizar_Datos();
    }

  }

  Actualizar_Datos() {

    if (this.tMensaje.De === this.tUsuario.Codusuario) {

      this.tCorreoMensaje = `${this.tUsuario.Codusuario}@${this.tEmpresa.Dominio}`;
      this.tOrigenMensaje = `[${this.tUsuario.Codusuario}] - ${this.tUsuario.Descripcion}`;

    }
    else if (this.tMensaje.De === this.tCliente.Codcliente) {

      this.tCorreoMensaje = this.tCliente.Email;
      this.tOrigenMensaje = `[${this.tCliente.Codcliente}] - ${this.tCliente.Descripcion}`;

    }
    else {

      if (this.tUsuarios) {

        let tUsuarioMensaje: Usuario = this.tUsuarios.find(tUsuarioTemp => tUsuarioTemp.Codusuario === this.tMensaje.De);
        this.tCorreoMensaje = `${tUsuarioMensaje.Codusuario}@${this.tEmpresa.Dominio}`;
        this.tOrigenMensaje = `[${tUsuarioMensaje.Codusuario}] - ${tUsuarioMensaje.Descripcion}`;

      }

    }

  }

}
