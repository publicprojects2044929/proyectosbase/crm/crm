import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwBurbujaMensajeComponent } from './fw-burbuja-mensaje.component';

describe('FwBurbujaMensajeComponent', () => {
  let component: FwBurbujaMensajeComponent;
  let fixture: ComponentFixture<FwBurbujaMensajeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwBurbujaMensajeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwBurbujaMensajeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
