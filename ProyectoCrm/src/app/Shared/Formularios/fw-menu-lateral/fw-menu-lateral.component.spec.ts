import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwMenuLateralComponent } from './fw-menu-lateral.component';

describe('FwMenuLateralComponent', () => {
  let component: FwMenuLateralComponent;
  let fixture: ComponentFixture<FwMenuLateralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwMenuLateralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwMenuLateralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
