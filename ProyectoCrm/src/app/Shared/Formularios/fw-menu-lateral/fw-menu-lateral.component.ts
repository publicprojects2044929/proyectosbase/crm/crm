import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { FormularioPadre, Usuario } from 'src/app/Clases/Estructura';
import { SessionService } from 'src/app/Core/Session/session.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-fw-menu-lateral',
  templateUrl: './fw-menu-lateral.component.html',
  styleUrls: ['./fw-menu-lateral.component.scss']
})
export class FwMenuLateralComponent implements OnInit, OnDestroy {

  tComponente: string = "";
  tModulos: FormularioPadre[] = [];
  tModulos$: Subscription;
  tUsuarioActual: Usuario = new Usuario();
  tPadreActual: string = "";
  tHome = "fHome";

  constructor(private tSession: SessionService,
    public router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {

    var tEncontrado: boolean = false;

    this.tComponente = this.route.snapshot.firstChild.data['Codformulario'] || '';

    this.tModulos$ = this.tSession.tModulosActuales$.subscribe((tModulosSession: FormularioPadre[]) => {

      this.tModulos = tModulosSession;

      for (let tModulo of this.tModulos) {

        for (let tFormulario of tModulo.LFormularios) {

          if (tFormulario.Codformulario == this.tComponente) {

            this.tPadreActual = tFormulario.Codformulariop;
            tEncontrado = true;
            break;

          }

        }

        if (tEncontrado == true) {

          break;

        }


      }

    })

  }

  ngOnDestroy() {

    this.tModulos$.unsubscribe();

  }

}
