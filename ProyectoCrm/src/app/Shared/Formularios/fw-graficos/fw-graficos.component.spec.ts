import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FwGraficosComponent } from './fw-graficos.component';

describe('FwGraficosComponent', () => {
  let component: FwGraficosComponent;
  let fixture: ComponentFixture<FwGraficosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FwGraficosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FwGraficosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
