import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AgmCoreModule } from '@agm/core';

//Formularios
import { FwCabeceraComponent } from './Formularios/fw-cabecera/fw-cabecera.component';
import { FwPlantillaComponent } from './Formularios/fw-plantilla/fw-plantilla.component';
import { FwMenuLateralComponent } from './Formularios/fw-menu-lateral/fw-menu-lateral.component';
import { FwGraficosComponent } from './Formularios/fw-graficos/fw-graficos.component';
import { FwMapaComponent } from './Formularios/fw-mapa/fw-mapa.component';

//Pipe
import { AgruparPipe } from './Pipes/agrupar.pipe'
import { BuscarPipe } from './Pipes/buscar.pipe'
import { OrdenarPipe } from './Pipes/ordenar.pipe'
import { LetraCapitralPipe } from './Pipes/letra-capitral.pipe'
import { DiferenciaFechasPipe } from './Pipes/diferencia-fechas.pipe';

//Directiva
import { AConfiguracionDirective } from './Directivas/a-configuracion.directive'
import { SoloNumerosDirective } from './Directivas/solo-numeros.directive';
import { environment } from 'src/environments/environment';
import { GravatarDirective } from './Directivas/gravatar.directive';
import { AColorFondoDirective } from './Directivas/a-color-fondo.directive';
import { AColorLetrasDirective } from './Directivas/a-color-letras.directive';
import { FwBurbujaMensajeComponent } from './Formularios/fw-burbuja-mensaje/fw-burbuja-mensaje.component';
import { TallaPipe } from './Pipes/talla.pipe';
import { PrecioClientePipe } from './Pipes/precio-cliente.pipe';
import { TotalPedidoPipe } from './Pipes/total-pedido.pipe';

@NgModule({
  declarations: [
    FwCabeceraComponent,
    FwPlantillaComponent,
    FwMenuLateralComponent,
    FwGraficosComponent,
    AgruparPipe,
    BuscarPipe,
    OrdenarPipe,
    AConfiguracionDirective,
    SoloNumerosDirective,
    LetraCapitralPipe,
    FwMapaComponent,
    GravatarDirective,
    AColorFondoDirective,
    AColorLetrasDirective,
    FwBurbujaMensajeComponent,
    DiferenciaFechasPipe,
    TallaPipe,
    PrecioClientePipe,
    TotalPedidoPipe
  ],
  imports: [
    CommonModule,
    RouterModule,
    AgmCoreModule.forRoot({
      apiKey: environment.api_key_maps
    })
  ],
  exports: [
    AgruparPipe,
    BuscarPipe,
    OrdenarPipe,
    LetraCapitralPipe,
    AConfiguracionDirective,
    SoloNumerosDirective,
    FwGraficosComponent,
    FwMapaComponent,
    GravatarDirective,
    AColorFondoDirective,
    AColorLetrasDirective,
    FwBurbujaMensajeComponent,
    DiferenciaFechasPipe,
    TallaPipe,
    PrecioClientePipe,
    TotalPedidoPipe
  ]
})
export class SharedModule { }
