import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClsServiciosService } from './Servicios/cls-servicios.service';
import { SessionService } from './Session/session.service';
import { AreaService } from './Graficos/Area/area.service';
import { PieService } from './Graficos/Pie/pie.service';
import { SocketService } from './Socket/socket.service';
import { BarrasService } from './Graficos/Barras/barras.service';
import { GravatarService } from './Gravatar/gravatar.service';
import { GraphqlModule } from './GraphQL/graphql.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    GraphqlModule
  ],
  providers: [
    ClsServiciosService,
    SessionService,
    AreaService,
    SocketService,
    PieService,
    BarrasService,
    GravatarService
  ]
})
export class CoreModule { }
