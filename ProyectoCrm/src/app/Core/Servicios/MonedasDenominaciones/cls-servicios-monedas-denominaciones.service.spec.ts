import { TestBed } from '@angular/core/testing';

import { ClsServiciosMonedasDenominacionesService } from './cls-servicios-monedas-denominaciones.service';

describe('ClsServiciosMonedasDenominacionesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosMonedasDenominacionesService = TestBed.get(ClsServiciosMonedasDenominacionesService);
    expect(service).toBeTruthy();
  });
});
