import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Respuesta } from 'src/app/Clases/Estructura';
import { MonedaDenominacionEntrada } from 'src/app/Clases/EstructuraEntrada';

export class ClsServiciosMonedasDenominacionesService {

  public tUrl : string = "";

  constructor(private tHttp: HttpClient, private tUsuario: string, private tHeaders: HttpHeaders) { 

  }

  Consumir_Eliminar_MonedaDenominacion(tDenominacion: MonedaDenominacionEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/MonedaDenominaciones/EliminarMonedaDenominacion';

    return this.tHttp.post<Respuesta>(tUrlFinal, tDenominacion, httpOptions);

  }

}
