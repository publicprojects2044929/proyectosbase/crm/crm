import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Respuesta } from 'src/app/Clases/Estructura';
import { CiudadEntrada } from 'src/app/Clases/EstructuraEntrada';

export class ClsServiciosCiudadesService {

  public tUrl: string = "";
  
  constructor(private tHttp: HttpClient, private tUsuario: string, private tHeaders: HttpHeaders) {

   }

  Consumir_Obtener_Ciudades(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/Ciudad/ObtenerCiudad?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa + 
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Obtener_CiudadPorPaisEstadoYMunicipio(Corporacion: string, Empresa: string, 
                                                  CodPais: string, CodEstado: string, 
                                                  CodMunicipio: String) {

    var tUrlFinal: string = this.tUrl + '/Ciudad/ObtenerCiudad?'
                            + 'Corporacion=' + Corporacion
                            + '&Empresa=' + Empresa
                            + '&Codpais=' + CodPais
                            + '&Codestado=' + CodEstado
                            + '&Codmunicipio=' + CodMunicipio
                            + '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Crear_Ciudad(tCiudad: CiudadEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Ciudad/CrearCiudad';

    return this.tHttp.post<Respuesta>(tUrlFinal, tCiudad, httpOptions);

  }

  Consumir_Modificar_Ciudad(tCiudad: CiudadEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Ciudad/ModificarCiudad';

    return this.tHttp.put<Respuesta>(tUrlFinal, tCiudad, httpOptions);

  }

  Consumir_Eliminar_Ciudad(tCiudad: CiudadEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Ciudad/EliminarCiudad';

    return this.tHttp.post<Respuesta>(tUrlFinal, tCiudad, httpOptions);


  }


}
