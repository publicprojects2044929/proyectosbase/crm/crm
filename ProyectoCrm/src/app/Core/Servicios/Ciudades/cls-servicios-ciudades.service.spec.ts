import { TestBed } from '@angular/core/testing';

import { ClsServiciosCiudadesService } from './cls-servicios-ciudades.service';

describe('ClsServiciosCiudadesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosCiudadesService = TestBed.get(ClsServiciosCiudadesService);
    expect(service).toBeTruthy();
  });
});
