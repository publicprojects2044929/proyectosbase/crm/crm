import { TestBed } from '@angular/core/testing';

import { ClsServiciosArticulosInventarioService } from './cls-servicios-articulos-inventario.service';

describe('ClsServiciosArticulosInventarioService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosArticulosInventarioService = TestBed.get(ClsServiciosArticulosInventarioService);
    expect(service).toBeTruthy();
  });
});
