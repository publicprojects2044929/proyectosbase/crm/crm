import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Respuesta } from 'src/app/Clases/Estructura';

export class ClsServiciosArticulosInventarioService {

  public tUrl: string = "";

  constructor(private tHttp: HttpClient, 
              private tUsuario: string, 
              private tHeaders: HttpHeaders) {

  }

  Consumir_Obtener_ArticulosInventario(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/ArticuloInventario/ObtenerArticuloInventario?' +
      'Corporacion=' + Corporacion +
      '&Empresa=' + Empresa +
      '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Obtener_ArticulosInventario2(Corporacion: string, Empresa: string, Codarticulo: string) {

    var tUrlFinal: string = this.tUrl + '/ArticuloInventario/ObtenerArticuloInventario?' +
      'Corporacion=' + Corporacion +
      '&Empresa=' + Empresa +
      '&Codarticulo=' + Codarticulo +
      '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

}
