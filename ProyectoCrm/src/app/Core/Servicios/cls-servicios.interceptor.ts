import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { tap, map, finalize, catchError } from 'rxjs/operators';
import { Respuesta } from 'src/app/Clases/Estructura';
import { SessionService } from '../Session/session.service';
import { Router } from '@angular/router';

@Injectable()
export class ClsServiciosInterceptor implements HttpInterceptor {

    constructor(private tSession: SessionService, private router: Router) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        if(!req) { return; }

        req = this.AgregarToken(req);

        return next.handle(req).pipe(

            tap((event: HttpEvent<any>) => {

                if (event instanceof HttpResponse) {

                    if ('Resultado' in event.body) {

                        let tRespuesta: Respuesta = event.body;

                        if (tRespuesta.Resultado === 'E') {



                        }

                    }

                }

            }),
            catchError((error: HttpErrorResponse) => {

                if (error.status === 401){

                    this.router.navigate['/Login'];
                    return;

                }
                return throwError(error);

            })

        );

    }

    AgregarToken(request: HttpRequest<any>): HttpRequest<any> {

        if (this.tSession.tTokenJwt !== '') {

            request = request.clone({

                setHeaders: {
                    'Authorization': `Bearer ${ this.tSession.tTokenJwt }`
                },

            });

        }

        return request;

    }

}
