import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class ClsGoogleMapsService {

  tUrlGoogle : string = "https://maps.googleapis.com/maps/api/geocode/json?address=";

  constructor(private http : HttpClient) { }

  public Consumir_Obtener_LatitudLongitud(tDireccion : string, tCidudad : string, tPais : string){

    let tUrlFinal : string = this.tUrlGoogle + 
                            tDireccion.replace('\n', '') + ',' + 
                            tCidudad + ',' + 
                            tPais + 'CA&key=AIzaSyAo_DS6Rzjrl9PoNvb04OCCnyhAcaN797g';

    return this.http.get(tUrlFinal);

  }

}
