import { TestBed } from '@angular/core/testing';

import { ClsServiciosFabricanteService } from './cls-servicios-fabricante.service';

describe('ClsServiciosFabricanteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosFabricanteService = TestBed.get(ClsServiciosFabricanteService);
    expect(service).toBeTruthy();
  });
});
