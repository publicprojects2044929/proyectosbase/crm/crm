import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Respuesta } from 'src/app/Clases/Estructura';
import { FabricanteEntrada } from 'src/app/Clases/EstructuraEntrada';

export class ClsServiciosFabricanteService {

  public tUrl: string = "";

  constructor(private tHttp: HttpClient, private tUsuario: string, private tHeaders: HttpHeaders) { 

  }

  Consumir_Obtener_FabricanteFull(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/Fabricante/ObtenerFabricanteFull?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa + 
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Obtener_Fabricante(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/Fabricante/ObtenerFabricante?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa + 
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Crear_Fabricante(tFabricante: FabricanteEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Fabricante/CrearFabricante';

    return this.tHttp.post<Respuesta>(tUrlFinal, tFabricante, httpOptions);

  }

  Consumir_Modificar_Fabricante(tFabricante: FabricanteEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Fabricante/ModificarFabricante';

    return this.tHttp.put<Respuesta>(tUrlFinal, tFabricante, httpOptions);

  }

  Consumir_Eliminar_Fabricante(tFabricante: FabricanteEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Fabricante/EliminarFabricante';

    return this.tHttp.post<Respuesta>(tUrlFinal, tFabricante, httpOptions);

  }

}
