import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Respuesta } from 'src/app/Clases/Estructura';
import { ColeccionEntrada } from 'src/app/Clases/EstructuraEntrada';

export class ClsServiciosColeccionService {

  public tUrl: string = "";

  constructor(private tHttp: HttpClient,
    private tUsuario: string,
    private tHeaders: HttpHeaders) {

  }

  Consumir_ObtenerColecciones(Corporacion: string,
    Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/Coleccion/ObtenerColeccion?' +
      'Corporacion=' + Corporacion +
      '&Empresa=' + Empresa +
      '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_ObtenerColeccionesFull(Corporacion: string,
    Empresa: string,
    Codcoleccion: string) {

    var tUrlFinal: string = this.tUrl + '/Coleccion/ObtenerColeccionFull?' +
      'Corporacion=' + Corporacion +
      '&Empresa=' + Empresa +
      '&Codcoleccion=' + Codcoleccion +
      '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_ObtenerColecciones_Vigentes(Corporacion: string,
    Empresa: string,
    Codcoleccion: string) {

    var tUrlFinal: string = this.tUrl + '/Coleccion/ObtenerColeccionVigenteFull?' +
      'Corporacion=' + Corporacion +
      '&Empresa=' + Empresa +
      '&Codcoleccion=' + Codcoleccion +
      '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Crear_Coleccion(tColeccion: ColeccionEntrada) {

    const tHttpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Coleccion/CrearColeccion';

    return this.tHttp.post<Respuesta>(tUrlFinal, tColeccion, tHttpOptions);

  }

  Consumir_Modificar_Coleccion(tColeccion: ColeccionEntrada) {

    const tHttpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Coleccion/ModificarColeccion';

    return this.tHttp.put<Respuesta>(tUrlFinal, tColeccion, tHttpOptions);

  }

  Consumir_Eliminar_Coleccion(tColeccion: ColeccionEntrada) {

    const tHttpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Coleccion/EliminarColeccion';

    return this.tHttp.post<Respuesta>(tUrlFinal, tColeccion, tHttpOptions);

  }

}
