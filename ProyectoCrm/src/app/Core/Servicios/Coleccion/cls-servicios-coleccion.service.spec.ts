import { TestBed } from '@angular/core/testing';

import { ClsServiciosColeccionService } from './cls-servicios-coleccion.service';

describe('ClsServiciosColeccionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosColeccionService = TestBed.get(ClsServiciosColeccionService);
    expect(service).toBeTruthy();
  });
});
