import { TestBed } from '@angular/core/testing';

import { ClsServiciosColoresService } from './cls-servicios-colores.service';

describe('ClsServiciosColoresService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosColoresService = TestBed.get(ClsServiciosColoresService);
    expect(service).toBeTruthy();
  });
});
