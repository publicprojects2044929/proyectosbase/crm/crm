import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Respuesta } from 'src/app/Clases/Estructura';
import { ColorEntrada } from 'src/app/Clases/EstructuraEntrada';

export class ClsServiciosColoresService {

  public tUrl: string = "";
 

  constructor(private tHttp: HttpClient, private tUsuario: string, private tHeaders: HttpHeaders) { 
  }

  Consumir_Obtener_Colores(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/Color/ObtenerColor?' +
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa + 
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Obtener_ColoresFull(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/Color/ObtenerColorFull?' +
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa + 
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Crear_Color(tColor: ColorEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Color/CrearColor';

    return this.tHttp.post<Respuesta>(tUrlFinal, tColor, httpOptions);

  }

  Consumir_Modificar_Color(tColor: ColorEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Color/ModificarColor';

    return this.tHttp.put<Respuesta>(tUrlFinal, tColor, httpOptions);

  }

  Consumir_Eliminar_Color(tColor: ColorEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Color/EliminarColor';

    return this.tHttp.post<Respuesta>(tUrlFinal, tColor, httpOptions);

  }

}
