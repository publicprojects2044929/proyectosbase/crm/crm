import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Respuesta } from 'src/app/Clases/Estructura';
import { TicketEntrada } from 'src/app/Clases/EstructuraEntrada';

export class ClsServiciosTicketService {

  public tUrl: string = "";

  constructor(private tHttp: HttpClient, private tUsuario: string, private tHeaders: HttpHeaders) {

  }

  Consumir_Obtener_Ticket_Full(Corporacion: string, Empresa: string, Codticket: string){

    var tUrlFinal: string = this.tUrl + '/Ticket/ObtenerTicketFull?' +
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa +
                            '&Codticket=' + Codticket +
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Crear_Ticket(tTicket: TicketEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Ticket/CrearTicket';

    return this.tHttp.post<Respuesta>(tUrlFinal, tTicket, httpOptions);

  }

}
