import { TestBed } from '@angular/core/testing';

import { ClsServiciosTicketService } from './cls-servicios-ticket.service';

describe('ClsServiciosTicketService', () => {
  let service: ClsServiciosTicketService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ClsServiciosTicketService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
