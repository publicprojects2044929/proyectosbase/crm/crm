import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Respuesta } from 'src/app/Clases/Estructura';
import { GeneroEntrada } from 'src/app/Clases/EstructuraEntrada';

export class ClsServiciosGenerosService {

  public tUrl : string = "";

  constructor(private tHttp: HttpClient, 
              private tUsuario: string, 
              private tHeaders: HttpHeaders) { 

  }

  Consumir_Obtener_Genero(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/Genero/ObtenerGenero?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa + 
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Obtener_GeneroPorCliente(Corporacion: string, Empresa: string, Cliente: number) {

    var tUrlFinal: string = this.tUrl + '/Genero/ObtenerGeneroPorCliente?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa + 
                            '&Cliente=' + Cliente + 
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Crear_Genero(tGenero: GeneroEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Genero/CrearGenero';

    return this.tHttp.post<Respuesta>(tUrlFinal, tGenero, httpOptions);

  }

  Consumir_Modificar_Genero(tGenero: GeneroEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Genero/ModificarGenero';

    return this.tHttp.put<Respuesta>(tUrlFinal, tGenero, httpOptions);

  }

  Consumir_Eliminar_Genero(tGenero: GeneroEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Genero/EliminarGenero';

    return this.tHttp.post<Respuesta>(tUrlFinal, tGenero, httpOptions);

  }

}
