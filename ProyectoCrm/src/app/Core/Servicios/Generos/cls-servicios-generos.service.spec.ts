import { TestBed } from '@angular/core/testing';

import { ClsServiciosGenerosService } from './cls-servicios-generos.service';

describe('ClsServiciosGenerosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosGenerosService = TestBed.get(ClsServiciosGenerosService);
    expect(service).toBeTruthy();
  });
});
