import { TestBed } from '@angular/core/testing';

import { ClsServiciosMonedasService } from './cls-servicios-monedas.service';

describe('ClsServiciosMonedasService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosMonedasService = TestBed.get(ClsServiciosMonedasService);
    expect(service).toBeTruthy();
  });
});
