import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Respuesta } from 'src/app/Clases/Estructura';
import { MonedaEntrada } from 'src/app/Clases/EstructuraEntrada';

export class ClsServiciosMonedasService {

  public tUrl: string = "";

  constructor(private tHttp: HttpClient, private tUsuario: string, private tHeaders: HttpHeaders) {

  }
  
  Consumir_Obtener_Moneda(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/Moneda/ObtenerMoneda?' +
      'Corporacion=' + Corporacion +
      '&Empresa=' + Empresa +
      '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Obtener_MonedaFull(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/Moneda/ObtenerMonedaFull?' +
      'Corporacion=' + Corporacion +
      '&Empresa=' + Empresa +
      '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Obtener_MonedaFullPorCodigo(Corporacion: string,
    Empresa: string,
    Codmoneda: string) {

    var tUrlFinal: string = this.tUrl + '/Moneda/ObtenerMonedaFull?' +
      'Corporacion=' + Corporacion +
      '&Empresa=' + Empresa +
      '&Codmoneda=' + Codmoneda +
      '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Crear_Moneda(tMoneda: MonedaEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };


    var tUrlFinal: string = this.tUrl + '/Moneda/CrearMoneda';

    return this.tHttp.post<Respuesta>(tUrlFinal, tMoneda, httpOptions);

  }

  Consumir_Modificar_Moneda(tMoneda: MonedaEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Moneda/ModificarMoneda';

    return this.tHttp.put<Respuesta>(tUrlFinal, tMoneda, httpOptions);

  }

  Consumir_Eliminar_Moneda(tMoneda: MonedaEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Moneda/EliminarMoneda';

    return this.tHttp.post<Respuesta>(tUrlFinal, tMoneda, httpOptions);

  }

}
