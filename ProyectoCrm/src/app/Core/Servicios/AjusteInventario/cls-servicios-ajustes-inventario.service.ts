import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Respuesta } from 'src/app/Clases/Estructura';
import { AjusteInventarioEntrada } from 'src/app/Clases/EstructuraEntrada';

export class ClsServiciosAjustesInventarioService {

  public tUrl: string = "";

  constructor(private tHttp: HttpClient, private tUsuario: string, private tHeaders: HttpHeaders) {

  }

  Consumir_Obtener_AjusteInventarioPorArticulo(Corporacion: string, Empresa: string, Codarticulo: string) {

    var tUrlFinal: string = this.tUrl + '/AjusteInventario/ObtenerAjusteInventarioPorArticulo?' +
      'Corporacion=' + Corporacion +
      '&Empresa=' + Empresa +
      '&Codarticulo=' + Codarticulo +
      '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Crear_Articulo(tAjusteInv: AjusteInventarioEntrada) {

    const tHttpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/AjusteInventario/CrearAjusteInventario';

    return this.tHttp.post<Respuesta>(tUrlFinal, tAjusteInv, tHttpOptions);

  }
  
}
