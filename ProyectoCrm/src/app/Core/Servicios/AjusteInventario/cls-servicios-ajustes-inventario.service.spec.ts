import { TestBed } from '@angular/core/testing';

import { ClsServiciosAjustesInventarioService } from './cls-servicios-ajustes-inventario.service';

describe('ClsServiciosAjustesInventarioService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosAjustesInventarioService = TestBed.get(ClsServiciosAjustesInventarioService);
    expect(service).toBeTruthy();
  });
});
