import { TestBed } from '@angular/core/testing';

import { ClsServiciosColoresHexadecimalesService } from './cls-servicios-colores-hexadecimales.service';

describe('ClsServiciosColoresHexadecimalesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosColoresHexadecimalesService = TestBed.get(ClsServiciosColoresHexadecimalesService);
    expect(service).toBeTruthy();
  });
});
