import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Respuesta } from 'src/app/Clases/Estructura';
import { ColorEntrada, ColorHexadecimalesEntrada } from 'src/app/Clases/EstructuraEntrada';

export class ClsServiciosColoresHexadecimalesService {

  public tUrl: string = "";

  constructor(private tHttp: HttpClient, private tUsuario: string, private tHeaders: HttpHeaders) { 
  }

  Consumir_Crear_ColorHexadecimal(tColorHexa: ColorHexadecimalesEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/ColorHexadecimales/CrearColorHexadecimal';

    return this.tHttp.post<Respuesta>(tUrlFinal, tColorHexa, httpOptions);

  }

  Consumir_Modificar_ColorHexadecimal(tColorHexa: ColorHexadecimalesEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/ColorHexadecimales/ModificarColorHexadecimal';

    return this.tHttp.put<Respuesta>(tUrlFinal, tColorHexa, httpOptions);

  }

  Consumir_Eliminar_ColorHexadecimal(tColorHexa: ColorHexadecimalesEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/ColorHexadecimales/EliminarColorHexadecimal';

    return this.tHttp.post<Respuesta>(tUrlFinal, tColorHexa, httpOptions);

  }

}
