import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Respuesta } from 'src/app/Clases/Estructura';
import { MonedaTasaEntrada } from 'src/app/Clases/EstructuraEntrada';

export class ClsServiciosMonedasTasasService {

  public tUrl : string = "";

  constructor(private tHttp: HttpClient, private tUsuario: string, private tHeaders: HttpHeaders) { 

  }

  Consumir_Obtener_Moneda_TasasPorOirgenActivo(Corporacion: string, Empresa: string, Codorigen : string) {

    var tUrlFinal: string = this.tUrl + '/MonedaTasas/ObtenerMonedaTasasPorOrigenActivo?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa + 
                            '&Codorigen=' + Codorigen + 
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Obtener_Moneda_TasasPorDestinoActivo(Corporacion: string, Empresa: string, Coddestino : string) {

    var tUrlFinal: string = this.tUrl + '/MonedaTasas/ObtenerMonedaTasasPorDestinoActivo?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa + 
                            '&Coddestino=' + Coddestino + 
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Crear_MonedaTasa(tTasa: MonedaTasaEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/MonedaTasas/CrearMonedaTasas';

    return this.tHttp.post<Respuesta>(tUrlFinal, tTasa, httpOptions);

  }

}
