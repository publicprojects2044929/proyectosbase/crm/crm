import { TestBed } from '@angular/core/testing';

import { ClsServiciosMonedasTasasService } from './cls-servicios-monedas-tasas.service';

describe('ClsServiciosMonedasTasasService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosMonedasTasasService = TestBed.get(ClsServiciosMonedasTasasService);
    expect(service).toBeTruthy();
  });
});
