import { TestBed } from '@angular/core/testing';

import { ClsServiciosConfiguracionesService } from './cls-servicios-configuraciones.service';

describe('ClsServiciosConfiguracionesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosConfiguracionesService = TestBed.get(ClsServiciosConfiguracionesService);
    expect(service).toBeTruthy();
  });
});
