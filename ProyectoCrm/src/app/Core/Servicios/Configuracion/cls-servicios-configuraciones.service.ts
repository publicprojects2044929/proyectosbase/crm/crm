import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Respuesta } from 'src/app/Clases/Estructura';
import { ConfiguracionEntrada } from 'src/app/Clases/EstructuraEntrada';

export class ClsServiciosConfiguracionesService {

  public tUrl : string = "";

  constructor(private tHttp: HttpClient, private tUsuario: string, private tHeaders: HttpHeaders) { 

    

  }

  Consumir_Obtener_Configuracion(Corporacion: string, Empresa: string, Tabla: string,
    Columna: string) {

    var tUrlFinal: string = this.tUrl + '/Configuracion/ObtenerConfiguracion?' +
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa +
                            '&Tabla=' + Tabla +
                            '&Columna=' + Columna +
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Modificar_Configuracion(tConfiguracion: ConfiguracionEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Configuracion/ModificarConfiguracion';

    return this.tHttp.put<Respuesta>(tUrlFinal, tConfiguracion, httpOptions);

  }

}
