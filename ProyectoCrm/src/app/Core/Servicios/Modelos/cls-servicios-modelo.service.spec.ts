import { TestBed } from '@angular/core/testing';

import { ClsServiciosModeloService } from './cls-servicios-modelo.service';

describe('ClsServiciosModeloService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosModeloService = TestBed.get(ClsServiciosModeloService);
    expect(service).toBeTruthy();
  });
});
