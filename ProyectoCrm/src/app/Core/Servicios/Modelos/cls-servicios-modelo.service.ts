import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Respuesta } from 'src/app/Clases/Estructura';
import { ModeloEntrada } from 'src/app/Clases/EstructuraEntrada';

export class ClsServiciosModeloService {

  public tUrl: string = "";

  constructor(private tHttp: HttpClient, private tUsuario: string, private tHeaders: HttpHeaders) {

  }

  Consumir_Obtener_Modelo(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/Modelo/ObtenerModelo?' +
      'Corporacion=' + Corporacion +
      '&Empresa=' + Empresa +
      '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Obtener_ModeloPorFabricanteYMarca(Corporacion: string, Empresa: string, Codfabricante: string, Codmarca: string) {

    var tUrlFinal: string = this.tUrl + '/Modelo/ObtenerModelo?'
      + 'Corporacion=' + Corporacion
      + '&Empresa=' + Empresa
      + '&Codfabricante=' + Codfabricante
      + '&Codmarca=' + Codmarca
      + '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Crear_Modelo(tModelo: ModeloEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Modelo/CrearModelo';

    return this.tHttp.post<Respuesta>(tUrlFinal, tModelo, httpOptions);

  }

  Consumir_Modificar_Modelo(tModelo: ModeloEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Modelo/ModificarModelo';

    return this.tHttp.put<Respuesta>(tUrlFinal, tModelo, httpOptions);

  }

  Consumir_Eliminar_Modelo(tModelo: ModeloEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Modelo/EliminarModelo';

    return this.tHttp.post<Respuesta>(tUrlFinal, tModelo, httpOptions);

  }

}
