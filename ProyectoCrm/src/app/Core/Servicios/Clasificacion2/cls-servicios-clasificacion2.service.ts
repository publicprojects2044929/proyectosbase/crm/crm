import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Respuesta } from 'src/app/Clases/Estructura';
import { Clasificacion2Entrada } from 'src/app/Clases/EstructuraEntrada';

export class ClsServiciosClasificacion2Service {

  public tUrl: string = "";

  constructor(private tHttp: HttpClient, private tUsuario: string, private tHeaders: HttpHeaders) { 

  }

  Consumir_Obtener_Clasificacion2(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/Clasificacion2/ObtenerClasificacion2?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa + 
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Obtener_Clasificacion2PorClasificacion1(Corporacion: string, Empresa: string, Codclasificacion1: string) {

    var tUrlFinal: string = this.tUrl + '/Clasificacion2/ObtenerClasificacion2?'
                            + 'Corporacion=' + Corporacion
                            + '&Empresa=' + Empresa
                            + '&Codclasificacion1=' + Codclasificacion1
                            + '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Crear_Clasificacion2(tClasificacion2: Clasificacion2Entrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Clasificacion2/CrearClasificacion2';

    return this.tHttp.post<Respuesta>(tUrlFinal, tClasificacion2, httpOptions);

  }

  Consumir_Modificar_Clasificacion2(tClasificacion2: Clasificacion2Entrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Clasificacion2/ModificarClasificacion2';

    return this.tHttp.put<Respuesta>(tUrlFinal, tClasificacion2, httpOptions);

  }

  Consumir_Eliminar_Clasificacion2(tClasificacion2: Clasificacion2Entrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Clasificacion2/EliminarClasificacion2';

    return this.tHttp.post<Respuesta>(tUrlFinal, tClasificacion2, httpOptions);

  }
 
}
