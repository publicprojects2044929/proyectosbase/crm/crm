import { TestBed } from '@angular/core/testing';

import { ClsServiciosClasificacion2Service } from './cls-servicios-clasificacion2.service';

describe('ClsServiciosClasificacion2Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosClasificacion2Service = TestBed.get(ClsServiciosClasificacion2Service);
    expect(service).toBeTruthy();
  });
});
