import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Respuesta } from 'src/app/Clases/Estructura';
import { ClienteDocumentosEntrada } from 'src/app/Clases/EstructuraEntrada';

export class ClsServiciosClientesDocumentosService {

  public tUrl: string = "";

  constructor(private tHttp: HttpClient, private tUsuario: string, private tHeaders: HttpHeaders) {

  }

  Consumir_EliminarCliente_Documento(tClienteDocumento: ClienteDocumentosEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/ClienteDocumentos/EliminarClienteDocumentos';

    return this.tHttp.post<Respuesta>(tUrlFinal, tClienteDocumento, httpOptions);

  }

}
