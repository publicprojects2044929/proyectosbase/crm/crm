import { TestBed } from '@angular/core/testing';

import { ClsServiciosClientesDocumentosService } from './cls-servicios-clientes-documentos.service';

describe('ClsServiciosClientesDocumentosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosClientesDocumentosService = TestBed.get(ClsServiciosClientesDocumentosService);
    expect(service).toBeTruthy();
  });
});
