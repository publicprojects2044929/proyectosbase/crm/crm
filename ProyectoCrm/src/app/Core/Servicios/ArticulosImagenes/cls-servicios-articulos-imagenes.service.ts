import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Respuesta } from 'src/app/Clases/Estructura';
import { ArticuloImagenEntrada } from 'src/app/Clases/EstructuraEntrada';

export class ClsServiciosArticulosImagenesService {

  public tUrl: string = "";

  constructor(private tHttp: HttpClient, private tUsuario: string, private tHeaders: HttpHeaders) {

  }

  Consumir_Eliminar_ArticuloImagen(tArticuloImagen: ArticuloImagenEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/ArticuloImagenes/EliminarArticuloImagenes';

    return this.tHttp.post<Respuesta>(tUrlFinal, tArticuloImagen, httpOptions);

  }

}
