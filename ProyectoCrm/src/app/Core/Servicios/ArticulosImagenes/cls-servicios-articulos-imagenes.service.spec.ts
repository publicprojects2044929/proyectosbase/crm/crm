import { TestBed } from '@angular/core/testing';

import { ClsServiciosArticulosImagenesService } from './cls-servicios-articulos-imagenes.service';

describe('ClsServiciosArticulosImagenesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosArticulosImagenesService = TestBed.get(ClsServiciosArticulosImagenesService);
    expect(service).toBeTruthy();
  });
});
