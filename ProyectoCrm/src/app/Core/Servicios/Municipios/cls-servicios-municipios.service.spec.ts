import { TestBed } from '@angular/core/testing';

import { ClsServiciosMunicipiosService } from './cls-servicios-municipios.service';

describe('ClsServiciosMunicipiosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosMunicipiosService = TestBed.get(ClsServiciosMunicipiosService);
    expect(service).toBeTruthy();
  });
});
