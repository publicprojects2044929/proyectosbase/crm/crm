import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Respuesta } from 'src/app/Clases/Estructura';
import { MunicipioEntrada } from 'src/app/Clases/EstructuraEntrada';

export class ClsServiciosMunicipiosService {

  public tUrl: string = "";

  constructor(private tHttp: HttpClient, private tUsuario: string, private tHeaders: HttpHeaders) { 

  }

  Consumir_Obtener_Municipios(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/Municipio/ObtenerMunicipio?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa + 
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Obtener_MunicipioPorPaisYEstado(Corporacion: string, Empresa: string, CodPais: string, CodEstado: string) {

    var tUrlFinal: string = this.tUrl + '/Municipio/ObtenerMunicipio?'
                            + 'Corporacion=' + Corporacion
                            + '&Empresa=' + Empresa
                            + '&Codpais=' + CodPais
                            + '&Codestado=' + CodEstado
                            + '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Crear_Municipio(tMunicipio: MunicipioEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Municipio/CrearMunicipio';

    return this.tHttp.post<Respuesta>(tUrlFinal, tMunicipio, httpOptions);

  }

  Consumir_Modificar_Municipio(tMunicipio: MunicipioEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Municipio/ModificarMunicipio';

    return this.tHttp.put<Respuesta>(tUrlFinal, tMunicipio, httpOptions);

  }

  Consumir_Eliminar_Municipio(tMunicipio: MunicipioEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Municipio/EliminarMunicipio';

    return this.tHttp.post<Respuesta>(tUrlFinal, tMunicipio, httpOptions);

  }

}
