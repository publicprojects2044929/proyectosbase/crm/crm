import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Respuesta } from 'src/app/Clases/Estructura';
import { SituacionEntrada } from 'src/app/Clases/EstructuraEntrada';

export class ClsServiciosSituacionService {

  public tUrl: string = "";

  constructor(private tHttp: HttpClient, private tUsuario: string, private tHeaders: HttpHeaders) {

  }

  Consumir_Obtener_Situacion(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/Situacion/ObtenerSituacion?' +
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa +
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Crear_Situacion(tSituacion: SituacionEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Situacion/CrearSituacion';

    return this.tHttp.post<Respuesta>(tUrlFinal, tSituacion, httpOptions);

  }

  Consumir_Modificar_Situacion(tSituacion: SituacionEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Situacion/ModificarSituacion';

    return this.tHttp.put<Respuesta>(tUrlFinal, tSituacion, httpOptions);

  }

  Consumir_Eliminar_Situacion(tSituacion: SituacionEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Situacion/EliminarSituacion';

    return this.tHttp.post<Respuesta>(tUrlFinal, tSituacion, httpOptions);


  }

}
