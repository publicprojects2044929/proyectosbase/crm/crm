import { TestBed } from '@angular/core/testing';

import { ClsServiciosSituacionService } from './cls-servicios-situacion.service';

describe('ClsServiciosSituacionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosSituacionService = TestBed.get(ClsServiciosSituacionService);
    expect(service).toBeTruthy();
  });
});
