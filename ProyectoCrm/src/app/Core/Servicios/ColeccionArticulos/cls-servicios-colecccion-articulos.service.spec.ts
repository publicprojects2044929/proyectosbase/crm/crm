import { TestBed } from '@angular/core/testing';

import { ClsServiciosColecccionArticulosService } from './cls-servicios-colecccion-articulos.service';

describe('ColecccionArticulosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosColecccionArticulosService = TestBed.get(ClsServiciosColecccionArticulosService);
    expect(service).toBeTruthy();
  });
});
