import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ColeccionArticulosEntrada } from 'src/app/Clases/EstructuraEntrada';
import { Respuesta } from 'src/app/Clases/Estructura';

export class ClsServiciosColecccionArticulosService {

  public tUrl: string = "";

  constructor(private tHttp: HttpClient,
    private tUsuario: string,
    private tHeaders: HttpHeaders) {

  }

  Consumir_Crear_ColeccionArticulo(tColeccionArticulos: ColeccionArticulosEntrada) {

    const tHttpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/ColeccionArticulos/CrearColeccionArticulo';

    return this.tHttp.post<Respuesta>(tUrlFinal, tColeccionArticulos, tHttpOptions);

  }

  Consumir_Modificar_ColeccionArticulo(tColeccionArticulos: ColeccionArticulosEntrada) {

    const tHttpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/ColeccionArticulos/ModificarColeccionArticulo';

    return this.tHttp.put<Respuesta>(tUrlFinal, tColeccionArticulos, tHttpOptions);

  }

  Consumir_Eliminar_ColeccionArticulo(tColeccionArticulos: ColeccionArticulosEntrada) {

    const tHttpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/ColeccionArticulos/EliminarColeccionArticulos';

    return this.tHttp.post<Respuesta>(tUrlFinal, tColeccionArticulos, tHttpOptions);

  }
  
}
