import { TestBed } from '@angular/core/testing';

import { ClsServiciosClasificacion1Service } from './cls-servicios-clasificacion1.service';

describe('ClsServiciosClasificacion1Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosClasificacion1Service = TestBed.get(ClsServiciosClasificacion1Service);
    expect(service).toBeTruthy();
  });
});
