import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Respuesta } from 'src/app/Clases/Estructura';
import { Clasificacion1Entrada } from 'src/app/Clases/EstructuraEntrada';

export class ClsServiciosClasificacion1Service {

  public tUrl: string = "";
  
  constructor(private tHttp: HttpClient, private tUsuario: string, private tHeaders: HttpHeaders) { 

    
  }

  Consumir_Obtener_Clasificacion1(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/Clasificacion1/ObtenerClasificacion1?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa + 
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Obtener_Clasificacion1Nivel2(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/Clasificacion1/ObtenerClasificacion1Nivel2?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa + 
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Obtener_Clasificacion1Nivel3(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/Clasificacion1/ObtenerClasificacion1Nivel3?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa + 
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Obtener_Clasificacion1Full(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/Clasificacion1/ObtenerClasificacion1NivelFull?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa + 
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Obtener_Clasificacion1Full4(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/Clasificacion1/ObtenerClasificacion1NivelFull4?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa + 
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Crear_Clasificacion1(tClasificacion1: Clasificacion1Entrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Clasificacion1/CrearClasificacion1';

    return this.tHttp.post<Respuesta>(tUrlFinal, tClasificacion1, httpOptions);

  }

  Consumir_Modificar_Clasificacion1(tClasificacion1: Clasificacion1Entrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Clasificacion1/ModificarClasificacion1';

    return this.tHttp.put<Respuesta>(tUrlFinal, tClasificacion1, httpOptions);

  }

  Consumir_Eliminar_Clasificacion1(tClasificacion1: Clasificacion1Entrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Clasificacion1/EliminarClasificacion1';

    return this.tHttp.post<Respuesta>(tUrlFinal, tClasificacion1, httpOptions);

  }

}
