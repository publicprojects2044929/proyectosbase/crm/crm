import { TestBed } from '@angular/core/testing';

import { ClsServiciosClientesTelefonosService } from './cls-servicios-clientes-telefonos.service';

describe('ClsServiciosClientesTelefonosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosClientesTelefonosService = TestBed.get(ClsServiciosClientesTelefonosService);
    expect(service).toBeTruthy();
  });
});
