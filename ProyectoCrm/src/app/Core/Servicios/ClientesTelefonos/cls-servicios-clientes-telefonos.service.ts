import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Respuesta } from 'src/app/Clases/Estructura';
import { ClienteTelefonoEntrada } from 'src/app/Clases/EstructuraEntrada';

export class ClsServiciosClientesTelefonosService {

  public tUrl : string = "";

  constructor(private tHttp: HttpClient, private tUsuario: string, private tHeaders: HttpHeaders) { 

  }

  Consumir_ObtenerCliente_Telefonoc(Corporacion: string, Empresa: string, Codcliente: string) {

    var tUrlFinal: string = this.tUrl + '/Cliente_Telefonos/ObtenerCliente_Telefonos?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa + 
                            '&Codcliente=' + Codcliente + 
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_EliminarCliente_Telefono(tClienteTelefono: ClienteTelefonoEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Cliente_Telefonos/EliminarCliente_Telefonos';
    
    return this.tHttp.post<Respuesta>(tUrlFinal, tClienteTelefono, httpOptions);

  }

}
