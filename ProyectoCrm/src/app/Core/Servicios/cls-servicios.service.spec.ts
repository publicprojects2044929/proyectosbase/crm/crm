import { TestBed } from '@angular/core/testing';

import { ClsServiciosService } from './cls-servicios.service';

describe('ClsServiciosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosService = TestBed.get(ClsServiciosService);
    expect(service).toBeTruthy();
  });
});
