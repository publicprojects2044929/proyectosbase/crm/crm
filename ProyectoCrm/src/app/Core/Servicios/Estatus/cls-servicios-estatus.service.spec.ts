import { TestBed } from '@angular/core/testing';

import { ClsServiciosEstatusService } from './cls-servicios-estatus.service';

describe('ClsServiciosEstatusService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosEstatusService = TestBed.get(ClsServiciosEstatusService);
    expect(service).toBeTruthy();
  });
});
