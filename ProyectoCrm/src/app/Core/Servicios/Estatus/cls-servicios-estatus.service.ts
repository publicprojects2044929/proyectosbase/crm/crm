import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Respuesta } from 'src/app/Clases/Estructura';
import { EstatusEntrada } from 'src/app/Clases/EstructuraEntrada';

export class ClsServiciosEstatusService {

  public tUrl : string = "";

  constructor(private tHttp: HttpClient, private tUsuario: string, private tHeaders: HttpHeaders) { 

  }

  Consumir_Obtener_Estatus(Corporacion: string, Empresa: string, Tabla: string) {

    var tUrlFinal: string = this.tUrl + '/Estatus/ObtenerEstatusPorTabla?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa + 
                            '&Tabla=' + Tabla + 
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Modificar_Estatus(tEstatus: EstatusEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Estatus/ModificarEstatus';

    return this.tHttp.put<Respuesta>(tUrlFinal, tEstatus, httpOptions);

  }
  
}
