import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Respuesta } from 'src/app/Clases/Estructura';
import { CuponEntrada } from 'src/app/Clases/EstructuraEntrada';

export class ClsServiciosCuponService {

  public tUrl: string = "";

  constructor(private tHttp: HttpClient, private tUsuario: string, private tHeaders: HttpHeaders) { 

  }

  Consumir_Obtener_Cupones(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/Cupon/ObtenerCupon?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa + 
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);


  }

  Consumir_Crear_Cupon(tCupon: CuponEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Cupon/CrearCupon';

    return this.tHttp.post<Respuesta>(tUrlFinal, tCupon, httpOptions);

  }

  Consumir_Modificar_Cupon(tCupon: CuponEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Cupon/ModificarCupon';

    return this.tHttp.put<Respuesta>(tUrlFinal, tCupon, httpOptions);

  }

  Consumir_Eliminar_Cupon(tCupon: CuponEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Cupon/EliminarCupon';

    return this.tHttp.post<Respuesta>(tUrlFinal, tCupon, httpOptions);

  }

}
