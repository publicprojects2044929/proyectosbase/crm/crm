import { TestBed } from '@angular/core/testing';

import { ClsServiciosCuponService } from './cls-servicios-cupon.service';

describe('ClsServiciosCuponService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosCuponService = TestBed.get(ClsServiciosCuponService);
    expect(service).toBeTruthy();
  });
});
