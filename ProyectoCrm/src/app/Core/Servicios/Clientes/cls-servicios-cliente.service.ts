import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Respuesta } from 'src/app/Clases/Estructura';
import { ClienteEntrada } from 'src/app/Clases/EstructuraEntrada';

export class ClsServiciosClienteService {

  public tUrl: string = "";

  constructor(private tHttp: HttpClient, private tUsuario: string, private tHeaders: HttpHeaders) { 

  }

  Consumir_Obtener_Clientes(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/Cliente/ObtenerCliente?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa + 
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);


  }

  Consumir_Obtener_Clientes2(Corporacion: string, Empresa: string, Codcliente: string) {

    var tUrlFinal: string = this.tUrl + '/Cliente/ObtenerCliente?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa + 
                            '&Codcliente=' + Codcliente + 
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);


  }

  Consumir_Obtener_ClientesDetallado(Corporacion: string, Empresa: string, Codcliente: string) {

    var tUrlFinal: string = this.tUrl + '/Cliente/ObtenerClienteDetallado?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa + 
                            '&Codcliente=' + Codcliente + 
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);


  }

  Consumir_Crear_Cliente(tCliente: ClienteEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Cliente/CrearCliente';

    return this.tHttp.post<Respuesta>(tUrlFinal, tCliente, httpOptions);

  }

  Consumir_Modificar_Cliente(tCliente: ClienteEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Cliente/ModificarCliente';

    return this.tHttp.put<Respuesta>(tUrlFinal, tCliente, httpOptions);

  }

  Consumir_Eliminar_Cliente(tCliente: ClienteEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Cliente/EliminarCliente';

    return this.tHttp.post<Respuesta>(tUrlFinal, tCliente, httpOptions);


  }

}
