import { TestBed } from '@angular/core/testing';

import { ClsServiciosClienteService } from './cls-servicios-cliente.service';

describe('ClsServiciosClienteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosClienteService = TestBed.get(ClsServiciosClienteService);
    expect(service).toBeTruthy();
  });
});
