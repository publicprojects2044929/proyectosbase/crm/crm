import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Respuesta } from 'src/app/Clases/Estructura';
import { OcupacionEntrada } from 'src/app/Clases/EstructuraEntrada';

export class ClsServiciosOcupacionService {

  public tUrl: string = "";

  constructor(private tHttp: HttpClient, private tUsuario: string, private tHeaders: HttpHeaders) { 

  }

  Consumir_Obtener_Ocupacion(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/Ocupacion/ObtenerOcupacion?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa + 
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }


  Consumir_Crear_Ocupacion(tOcupacion: OcupacionEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Ocupacion/CrearOcupacion';

    return this.tHttp.post<Respuesta>(tUrlFinal, tOcupacion, httpOptions);

  }

  Consumir_Modificar_Ocupacion(tOcupacion: OcupacionEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Ocupacion/ModificarOcupacion';

    return this.tHttp.put<Respuesta>(tUrlFinal, tOcupacion, httpOptions);

  }

  Consumir_Eliminar_Ocupacion(tOcupacion: OcupacionEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Ocupacion/EliminarOcupacion';

    return this.tHttp.post<Respuesta>(tUrlFinal, tOcupacion, httpOptions);


  }

}
