import { TestBed } from '@angular/core/testing';

import { ClsServiciosOcupacionService } from './cls-servicios-ocupacion.service';

describe('ClsServiciosPaisesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosOcupacionService = TestBed.get(ClsServiciosOcupacionService);
    expect(service).toBeTruthy();
  });
});
