import { TestBed } from '@angular/core/testing';

import { ClsServiciosTipoService } from './cls-servicios-tipo.service';

describe('ClsServiciosTipoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosTipoService = TestBed.get(ClsServiciosTipoService);
    expect(service).toBeTruthy();
  });
});
