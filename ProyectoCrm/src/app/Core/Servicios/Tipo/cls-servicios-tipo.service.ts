import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Respuesta } from 'src/app/Clases/Estructura';
import { TipoEntrada } from 'src/app/Clases/EstructuraEntrada';

export class ClsServiciosTipoService {

  public tUrl : string = "";

  constructor(private tHttp: HttpClient, private tUsuario: string, private tHeaders: HttpHeaders) { 

  }

  Consumir_Obtener_Tipo(Corporacion: string, Empresa: string, Tabla: string) {

    var tUrlFinal: string = this.tUrl + '/Tipo/ObtenerTipo?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa + 
                            '&Tabla=' + Tabla + 
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Obtener_TipoColumna(Corporacion: string, Empresa: string, Tabla: string, Columna: string) {

    var tUrlFinal: string = this.tUrl + '/Tipo/ObtenerTipo?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa + 
                            '&Tabla=' + Tabla + 
                            '&Columna=' + Columna +
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Modificar_Tipo(tTipo: TipoEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Tipo/ModificarTipo';

    return this.tHttp.put<Respuesta>(tUrlFinal, tTipo, httpOptions);

  }

}
