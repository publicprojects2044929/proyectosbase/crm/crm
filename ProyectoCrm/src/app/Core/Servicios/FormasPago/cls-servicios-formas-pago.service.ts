import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Respuesta } from 'src/app/Clases/Estructura';
import { Forma_PagosEntrada } from 'src/app/Clases/EstructuraEntrada';

export class ClsServiciosFormasPagoService {

  public tUrl: string = "";

  constructor(private tHttp: HttpClient, private tUsuario: string, private tHeaders: HttpHeaders) { 

  }

  Consumir_Obtener_FormasPago(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/FormaPago/ObtenerFormaPago?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa + 
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Obtener_FormasPagoCrm(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/FormaPago/ObtenerFormaPagoCrm?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa + 
                            '&Crm=S' +
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Crear_FormasPago(tFormasPago: Forma_PagosEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/FormaPago/CrearFormaPago';

    return this.tHttp.post<Respuesta>(tUrlFinal, tFormasPago, httpOptions);

  }

  Consumir_Modificar_FormasPago(tFormasPago: Forma_PagosEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/FormaPago/ModificarFormaPago';

    return this.tHttp.put<Respuesta>(tUrlFinal, tFormasPago, httpOptions);

  }

  Consumir_Eliminar_FormasPago(tFormasPago: Forma_PagosEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/FormaPago/EliminarFormaPago';

    return this.tHttp.post<Respuesta>(tUrlFinal, tFormasPago, httpOptions);

  }

}
