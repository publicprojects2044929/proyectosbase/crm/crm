import { TestBed } from '@angular/core/testing';

import { ClsServiciosFormasPagoService } from './cls-servicios-formas-pago.service';

describe('ClsServiciosFormasPagoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosFormasPagoService = TestBed.get(ClsServiciosFormasPagoService);
    expect(service).toBeTruthy();
  });
});
