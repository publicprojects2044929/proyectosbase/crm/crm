import { TestBed } from '@angular/core/testing';

import { ClsServiciosUsuariosService } from './cls-servicios-usuarios.service';

describe('ClsServiciosUsuariosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosUsuariosService = TestBed.get(ClsServiciosUsuariosService);
    expect(service).toBeTruthy();
  });
});
