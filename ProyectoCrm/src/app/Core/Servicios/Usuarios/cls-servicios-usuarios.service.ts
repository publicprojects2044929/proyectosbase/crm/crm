import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Respuesta } from 'src/app/Clases/Estructura';
import { UsuarioEntrada, UsuarioEmpresasEntrada } from 'src/app/Clases/EstructuraEntrada';

export class ClsServiciosUsuariosService {

  public tUrl: string = "";

  constructor(private tHttp: HttpClient,
    private tUsuario: string,
    private tHeaders: HttpHeaders) {

  }

  Consumir_Usuario_Login(Corporacion: string, Empresa: string, Codusuario: string, Clave: string) {

    var tUrlFinal: string = this.tUrl + '/Usuario/Login?' +
      'Corporacion=' + Corporacion +
      '&Empresa=' + Empresa +
      '&Codusuario=' + Codusuario +
      '&Clave=' + Clave +
      '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Cambiar_Clave(tUsuario: UsuarioEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Usuario/CambiarClave';

    return this.tHttp.post<Respuesta>(tUrlFinal, tUsuario, httpOptions);

  }

  Consumir_Restablecer_Clave(tUsuario: UsuarioEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Usuario/RestablecerClave';

    return this.tHttp.put<Respuesta>(tUrlFinal, tUsuario, httpOptions);

  }

  Consumir_Obtener_Usuario(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/Usuario/ObtenerUsuario?' +
      'Corporacion=' + Corporacion +
      '&Empresa=' + Empresa +
      '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Obtener_UsuarioEmpresa(Corporacion: string, Codusuario: string) {

    var tUrlFinal: string = this.tUrl + '/Usuario/ObtenerUsuarioEmpresas?' +
      'Corporacion=' + Corporacion +
      '&Codusuario=' + Codusuario +
      '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Obtener_UsuarioGrupo(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/Usuario/ObtenerUsuarioGrupos?' +
      'Corporacion=' + Corporacion +
      '&Empresa=' + Empresa +
      '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Obtener_UsuarioActual(Corporacion: string, Empresa: string, Codusuario: string) {

    var tUrlFinal: string = this.tUrl + '/Usuario/ObtenerUsuarioGrupos?' +
      'Corporacion=' + Corporacion +
      '&Empresa=' + Empresa +
      '&Codusuario=' + Codusuario +
      '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Cambiar_Empresa(Corporacion: string, Empresa: string, Codusuario: string) {

    var tUrlFinal: string = this.tUrl + '/Usuario/CambiarEmpresa?' +
      'Corporacion=' + Corporacion +
      '&Empresa=' + Empresa +
      '&Codusuario=' + Codusuario +
      '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }


  Consumir_Obtener_UsuariNativosGrupo(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/Usuario/ObtenerUsuarioNativosGrupos?' +
      'Corporacion=' + Corporacion +
      '&Empresa=' + Empresa +
      '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Obtener_UsuariNativosOcultoGrupo(Corporacion: string, Empresa: string, Codusuario: string, Oculto: string) {

    var tUrlFinal: string = this.tUrl + '/Usuario/ObtenerUsuarioNativosOcultoGrupos?' +
      'Corporacion=' + Corporacion +
      '&Empresa=' + Empresa +
      '&Codusuario=' + Codusuario +
      '&Oculto=' + Oculto +
      '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Obtener_UsuarioInvitadosGrupo(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/Usuario/ObtenerUsuarioInvitadosGrupos?' +
      'Corporacion=' + Corporacion +
      '&Empresa=' + Empresa +
      '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Obtener_UsuarioInvitadosGrupo2(Corporacion: string, Empresa: string, Codusuario: string) {

    var tUrlFinal: string = this.tUrl + '/Usuario/ObtenerUsuarioInvitadosGrupos?' +
      'Corporacion=' + Corporacion +
      '&Empresa=' + Empresa +
      '&Codusuario=' + Codusuario +
      '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Crear_Usuario(Usuario: UsuarioEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };


    var tUrlFinal: string = this.tUrl + '/Usuario/CrearUsuario';

    return this.tHttp.post<Respuesta>(tUrlFinal, Usuario, httpOptions);

  }

  Consumir_Modificar_Usuario(Usuario: UsuarioEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };


    var tUrlFinal: string = this.tUrl + '/Usuario/ModificarUsuario';

    return this.tHttp.put<Respuesta>(tUrlFinal, Usuario, httpOptions);

  }

  Consumir_Eliminar_Usuario(Usuario: UsuarioEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };


    var tUrlFinal: string = this.tUrl + '/Usuario/EliminarUsuario';

    return this.tHttp.post<Respuesta>(tUrlFinal, Usuario, httpOptions);

  }

  Consumir_Crear_Invitar_Usuario(Usuario: UsuarioEmpresasEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };


    var tUrlFinal: string = this.tUrl + '/Usuario/CrearInvitarUsuario';

    return this.tHttp.post<Respuesta>(tUrlFinal, Usuario, httpOptions);

  }

  Consumir_Modificar_Invitar_Usuario(Usuario: UsuarioEmpresasEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Usuario/ModificarInvitarUsuario';

    return this.tHttp.put<Respuesta>(tUrlFinal, Usuario, httpOptions);

  }

  Consumir_Eliminar_Invitar_Usuario(Usuario: UsuarioEmpresasEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Usuario/EliminarInvitarUsuario';

    return this.tHttp.post<Respuesta>(tUrlFinal, Usuario, httpOptions);

  }

}
