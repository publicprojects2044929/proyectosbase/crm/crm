import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Respuesta } from 'src/app/Clases/Estructura';
import { AlmacenEntrada } from 'src/app/Clases/EstructuraEntrada';

export class ClsServiciosAlmacenService {

  public tUrl: string = "";

  constructor(private tHttp: HttpClient, private tUsuario: string, private tHeaders: HttpHeaders) { 

  }

  Consumir_Obtener_Almacen(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/Almacen/ObtenerAlmacen?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa + 
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }


  Consumir_Crear_Almacen(tAlmacen: AlmacenEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Almacen/CrearAlmacen';

    return this.tHttp.post<Respuesta>(tUrlFinal, tAlmacen, httpOptions);

  }

  Consumir_Modificar_Almacen(tAlmacen: AlmacenEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Almacen/ModificarAlmacen';

    return this.tHttp.put<Respuesta>(tUrlFinal, tAlmacen, httpOptions);

  }

  Consumir_Eliminar_Almacen(tAlmacen: AlmacenEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Almacen/EliminarAlmacen';

    return this.tHttp.post<Respuesta>(tUrlFinal, tAlmacen, httpOptions);


  }

}
