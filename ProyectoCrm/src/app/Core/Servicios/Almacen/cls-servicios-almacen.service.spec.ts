import { TestBed } from '@angular/core/testing';

import { ClsServiciosAlmacenService } from './cls-servicios-almacen.service';

describe('ClsServiciosAlmacenService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosAlmacenService = TestBed.get(ClsServiciosAlmacenService);
    expect(service).toBeTruthy();
  });
});
