import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Respuesta } from 'src/app/Clases/Estructura';
import { PromocionEntrada } from 'src/app/Clases/EstructuraEntrada';

export class ClsServiciosPromocionesService {

  public tUrl: string = "";

  constructor(private tHttp: HttpClient, private tUsuario: string, private tHeaders: HttpHeaders) {

  }

  Consumir_Obtener_Promocion(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/Promocion/ObtenerPromocion?' +
      'Corporacion=' + Corporacion +
      '&Empresa=' + Empresa +
      '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Crear_Promocion(tPromocion: PromocionEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Promocion/CrearPromocion';

    return this.tHttp.post<Respuesta>(tUrlFinal, tPromocion, httpOptions);

  }

  Consumir_Modificar_Promocion(tPromocion: PromocionEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Promocion/ModificarPromocion';

    return this.tHttp.put<Respuesta>(tUrlFinal, tPromocion, httpOptions);

  }

  Consumir_Eliminar_Promocion(tPromocion: PromocionEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Promocion/EliminarPromocion';

    return this.tHttp.post<Respuesta>(tUrlFinal, tPromocion, httpOptions);


  }

}
