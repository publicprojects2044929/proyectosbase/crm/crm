import { TestBed } from '@angular/core/testing';

import { ClsServiciosPromocionesService } from './cls-servicios-promociones.service';

describe('ClsServiciosPromocionesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosPromocionesService = TestBed.get(ClsServiciosPromocionesService);
    expect(service).toBeTruthy();
  });
});
