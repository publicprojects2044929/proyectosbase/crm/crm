import { TestBed } from '@angular/core/testing';

import { ClsServiciosEstadoCivilService } from './cls-servicios-estado-civil.service';

describe('ClsServiciosEstadoCivilService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosEstadoCivilService = TestBed.get(ClsServiciosEstadoCivilService);
    expect(service).toBeTruthy();
  });
});
