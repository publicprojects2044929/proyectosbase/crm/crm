import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Respuesta } from 'src/app/Clases/Estructura';
import { EstadoCivilEntrada } from 'src/app/Clases/EstructuraEntrada';

export class ClsServiciosEstadoCivilService {

  public tUrl : string = "";

  constructor(private tHttp: HttpClient, private tUsuario: string, private tHeaders: HttpHeaders) { 

  }

  Consumir_ObtenerEstadosCivil(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/Estado_Civil/ObtenerEstado_Civil?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa + 
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Crear_EstadoCivil(tEstadoCivil: EstadoCivilEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Estado_Civil/CrearEstado_Civil';

    return this.tHttp.post<Respuesta>(tUrlFinal, tEstadoCivil, httpOptions);

  }

  Consumir_Modificar_EstadoCivil(tEstadoCivil: EstadoCivilEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Estado_Civil/ModificarEstado_Civil';

    return this.tHttp.put<Respuesta>(tUrlFinal, tEstadoCivil, httpOptions);

  }

  Consumir_Eliminar_EstadoCivil(tEstadoCivil: EstadoCivilEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Estado_Civil/EliminarEstado_Civil';

    return this.tHttp.post<Respuesta>(tUrlFinal, tEstadoCivil, httpOptions);


  }

}
