import { TestBed } from '@angular/core/testing';

import { ClsServiciosFormularioService } from './cls-servicios-formulario.service';

describe('ClsServiciosFormularioService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosFormularioService = TestBed.get(ClsServiciosFormularioService);
    expect(service).toBeTruthy();
  });
});
