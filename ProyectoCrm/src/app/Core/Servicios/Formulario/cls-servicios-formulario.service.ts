import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Respuesta } from 'src/app/Clases/Estructura';

export class ClsServiciosFormularioService {

  public tUrl : string = "";

  constructor(private tHttp: HttpClient, private tUsuario: string, private tHeaders: HttpHeaders) { 

  }

  Consumir_Obtener_FormularioCrm(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/FormulariosHijos/ObtenerFormulariosHijosCrm?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa + 
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Obtener_FormularioCrmFull(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/FormulariosHijos/ObtenerFormulariosHijosCrmFull?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa + 
                            '&Usuario=' + this.tUsuario;

    // var tUrlFinal: string = `${ this.tUrl }/FormulariosHijos/ObtenerFormulariosHijosCrmFull?
    //                           Corporacion=${ Corporacion }
    //                           &Empresa=${ Empresa }
    //                           &Usuario=${ this.tUsuario }`;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

}
