import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Respuesta } from 'src/app/Clases/Estructura';
import { Clasificacion3Entrada } from 'src/app/Clases/EstructuraEntrada';

export class ClsServiciosClasificacion3Service {

  public tUrl: string = "";

  constructor(private tHttp: HttpClient, private tUsuario: string, private tHeaders: HttpHeaders) { 

  }

  Consumir_Obtener_Clasificacion3(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/Clasificacion3/ObtenerClasificacion3?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa + 
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Obtener_Clasificacion3PorClasificacion1YClasificacion2(Corporacion: string, Empresa: string, Codclasificacion1: string, Codclasificacion2: string) {

    var tUrlFinal: string = this.tUrl + '/Clasificacion3/ObtenerClasificacion3?'
                            + 'Corporacion=' + Corporacion
                            + '&Empresa=' + Empresa
                            + '&Codclasificacion1=' + Codclasificacion1
                            + '&Codclasificacion2=' + Codclasificacion2
                            + '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Crear_Clasificacion3(tClasificacion3: Clasificacion3Entrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Clasificacion3/CrearClasificacion3';

    return this.tHttp.post<Respuesta>(tUrlFinal, tClasificacion3, httpOptions);

  }

  Consumir_Modificar_Clasificacion3(tClasificacion3: Clasificacion3Entrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Clasificacion3/ModificarClasificacion3';

    return this.tHttp.put<Respuesta>(tUrlFinal, tClasificacion3, httpOptions);

  }

  Consumir_Eliminar_Clasificacion3(tClasificacion3: Clasificacion3Entrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Clasificacion3/EliminarClasificacion3';

    return this.tHttp.post<Respuesta>(tUrlFinal, tClasificacion3, httpOptions);

  }
  
}
