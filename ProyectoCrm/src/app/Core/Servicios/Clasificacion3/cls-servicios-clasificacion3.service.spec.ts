import { TestBed } from '@angular/core/testing';

import { ClsServiciosClasificacion3Service } from './cls-servicios-clasificacion3.service';

describe('ClsServiciosClasificacion3Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosClasificacion3Service = TestBed.get(ClsServiciosClasificacion3Service);
    expect(service).toBeTruthy();
  });
});
