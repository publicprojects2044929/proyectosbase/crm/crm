import { TestBed } from '@angular/core/testing';

import { ClsServiciosPedidosService } from './cls-servicios-pedidos.service';

describe('ClsServiciosPedidosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosPedidosService = TestBed.get(ClsServiciosPedidosService);
    expect(service).toBeTruthy();
  });
});
