import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Respuesta } from 'src/app/Clases/Estructura';
import { PedidoEntrada } from 'src/app/Clases/EstructuraEntrada';

export class ClsServiciosPedidosService {

  public tUrl: string = "";

  constructor(private tHttp: HttpClient, private tUsuario: string, private tHeaders: HttpHeaders) { 

  }

  Consumir_Obtener_Pedido(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/Pedido/ObtenerPedido?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa + 
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Obtener_PedidoFull(Corporacion: string, Empresa: string, Codpedido: string) {

    var tUrlFinal: string = this.tUrl + '/Pedido/ObtenerPedidoFull?' +
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa +
                            '&Codpedido=' + Codpedido +
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Obtener_PedidoRpt(Corporacion: string, Empresa: string, Codpedido: string) {

    var tUrlFinal: string = this.tUrl + '/Pedido/ObtenerPedidoRpt?' +
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa +
                            '&Codpedido=' + Codpedido +
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Obtener_PedidosActivos(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/Pedido/ObtenerPedidoActivos?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa + 
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Obtener_PedidosPlaneador(Corporacion: string, Empresa: string, Estatus: string, Tipo: string,FechaI: number, FechaF: number) {

    var tUrlFinal: string = this.tUrl + '/Pedido/PlaneadorPedido' +
                            '?Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa +
                            '&Estatus=' + Estatus +
                            '&Tipo=' + Tipo +
                            '&FechacreacionI=' + FechaI +
                            '&FechacreacionF=' + FechaF +
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Crear_Pedido(tPedido: PedidoEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Pedido/CrearPedido';

    return this.tHttp.post<Respuesta>(tUrlFinal, tPedido, httpOptions);

  }

  Consumir_Crear_PedidoColecciones(tPedido: PedidoEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Pedido/CrearPedidoColeccion';

    return this.tHttp.post<Respuesta>(tUrlFinal, tPedido, httpOptions);

  }

  Consumir_Anular_Pedido(tPedido: PedidoEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Pedido/AnularPedido';

    return this.tHttp.put<Respuesta>(tUrlFinal, tPedido, httpOptions);

  }

  Consumir_Aprobar_Pedido(tPedido: PedidoEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Pedido/AprobarPedido';

    return this.tHttp.put<Respuesta>(tUrlFinal, tPedido, httpOptions);

  }

  Consumir_Cerrar_Pedido(tPedido: PedidoEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Pedido/CerrarPedido';

    return this.tHttp.put<Respuesta>(tUrlFinal, tPedido, httpOptions);

  }

  Consumir_Concluir_Pedido(tPedido: PedidoEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Pedido/ConcluirPedido';

    return this.tHttp.put<Respuesta>(tUrlFinal, tPedido, httpOptions);

  }

}
