import { TestBed } from '@angular/core/testing';

import { ClsServiciosCondicionService } from './cls-servicios-condicion.service';

describe('ClsServiciosCondicionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosCondicionService = TestBed.get(ClsServiciosCondicionService);
    expect(service).toBeTruthy();
  });
});
