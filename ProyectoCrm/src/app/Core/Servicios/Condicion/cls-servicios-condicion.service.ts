import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Respuesta } from 'src/app/Clases/Estructura';
import { CondicionEntrada } from 'src/app/Clases/EstructuraEntrada';

export class ClsServiciosCondicionService {

  public tUrl: string = "";

  constructor(private tHttp: HttpClient, private tUsuario: string, private tHeaders: HttpHeaders) { 

  }

  Consumir_Obtener_Condicion(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/Condicion/ObtenerCondicion?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa + 
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }


  Consumir_Crear_Condicion(tCondicion: CondicionEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Condicion/CrearCondicion';

    return this.tHttp.post<Respuesta>(tUrlFinal, tCondicion, httpOptions);

  }

  Consumir_Modificar_Condicion(tCondicion: CondicionEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Condicion/ModificarCondicion';

    return this.tHttp.put<Respuesta>(tUrlFinal, tCondicion, httpOptions);

  }

  Consumir_Eliminar_Condicion(tCondicion: CondicionEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Condicion/EliminarCondicion';

    return this.tHttp.post<Respuesta>(tUrlFinal, tCondicion, httpOptions);


  }

}
