import { TestBed } from '@angular/core/testing';

import { ClsServiciosArticulosService } from './cls-servicios-articulos.service';

describe('ClsServiciosArticulosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosArticulosService = TestBed.get(ClsServiciosArticulosService);
    expect(service).toBeTruthy();
  });
});
