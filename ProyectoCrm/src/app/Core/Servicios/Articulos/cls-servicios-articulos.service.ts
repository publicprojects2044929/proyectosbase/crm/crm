import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Respuesta } from 'src/app/Clases/Estructura';
import { ArticuloEntrada } from 'src/app/Clases/EstructuraEntrada';

export class ClsServiciosArticulosService {

  public tUrl: string = "";

  constructor(private tHttp: HttpClient, private tUsuario: string, private tHeaders: HttpHeaders) {

  }

  Consumir_Obtener_Articulos(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/Articulo/ObtenerArticulo?' +
      'Corporacion=' + Corporacion +
      '&Empresa=' + Empresa +
      '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Obtener_Articulo(Corporacion: string, Empresa: string, Codarticulo: string) {

    var tUrlFinal: string = this.tUrl + '/Articulo/ObtenerArticulo?' +
      'Corporacion=' + Corporacion +
      '&Empresa=' + Empresa +
      '&Codarticulo=' + Codarticulo +
      '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Obtener_ArticuloInventario(Corporacion: string, Empresa: string, Codarticulo: string) {

    var tUrlFinal: string = this.tUrl + '/Articulo/ObtenerArticuloInventario?' +
      'Corporacion=' + Corporacion +
      '&Empresa=' + Empresa +
      '&Codarticulo=' + Codarticulo +
      '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Obtener_ArticuloDetallado(Corporacion: string, Empresa: string, Codarticulo: string) {

    var tUrlFinal: string = this.tUrl + '/Articulo/ObtenerArticuloDetallado?' +
      'Corporacion=' + Corporacion +
      '&Empresa=' + Empresa +
      '&Codarticulo=' + Codarticulo +
      '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Obtener_ArticulosPlaneador(Corporacion: string, Empresa: string,
    Codclasificacion1: string, Codclasificacion2: string, Codclasificacion3: string,
    Codclasificacion4: string,Codfabricante: string, Codmarca: string, Codmodelo: string) {

    var tUrlFinal: string = this.tUrl + '/Articulo/PlaneadorArticulo' +
      '?Corporacion=' + Corporacion +
      '&Empresa=' + Empresa +
      '&Codclasificacion1=' + Codclasificacion1 +
      '&Codclasificacion2=' + Codclasificacion2 +
      '&Codclasificacion3=' + Codclasificacion3 +
      '&Codclasificacion4=' + Codclasificacion4 +
      '&Codfabricante=' + Codfabricante +
      '&Codmarca=' + Codmarca +
      '&Codmodelo=' + Codmodelo +
      '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Obtener_ArticuloRpt(Corporacion: string, Empresa: string, Codarticulo: string) {

    var tUrlFinal: string = this.tUrl + '/Articulo/ObtenerArticuloRpt?' +
      'Corporacion=' + Corporacion +
      '&Empresa=' + Empresa +
      '&Codarticulo=' + Codarticulo +
      '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Crear_Articulo(tArticulo: ArticuloEntrada) {

    const tHttpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Articulo/CrearArticulo';

    return this.tHttp.post<Respuesta>(tUrlFinal, tArticulo, tHttpOptions);

  }

  Consumir_Crear_Articulo2(tArticulo: ArticuloEntrada[]) {

    const tHttpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Articulo/CrearArticulo2';

    return this.tHttp.post<Respuesta>(tUrlFinal, tArticulo, tHttpOptions);

  }

  Consumir_Modificar_Articulo(tArticulo: ArticuloEntrada) {

    const tHttpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Articulo/ModificarArticulo';

    return this.tHttp.put<Respuesta>(tUrlFinal, tArticulo, tHttpOptions);

  }

  Consumir_Eliminar_Articulo(tArticulo: ArticuloEntrada) {

    const tHttpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Articulo/EliminarArticulo';

    return this.tHttp.post<Respuesta>(tUrlFinal, tArticulo, tHttpOptions);

  }

}
