import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Respuesta } from 'src/app/Clases/Estructura';
import { VentaEntrada } from 'src/app/Clases/EstructuraEntrada';

export class ClsServicioVentasService {

  public tUrl: string = "";

  constructor(private tHttp: HttpClient, private tUsuario: string, private tHeaders: HttpHeaders) {

  }

  Consumir_Crear_Venta(tVenta: VentaEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Venta/CrearVenta';

    return this.tHttp.post<Respuesta>(tUrlFinal, tVenta, httpOptions);

  }

  Consumir_Obtener_VentaFull(Corporacion: string, Empresa: string, Codventa: string) {

    var tUrlFinal: string = this.tUrl + '/Venta/ObtenerVentaFull' +
                            '?Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa +
                            '&Codventa=' + Codventa +
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Obtener_VentaPlaneador(Corporacion: string, Empresa: string, Estatus: string, Situacion: string, Tipo: string, FechaI: number, FechaF: number, FechaVI: number, FechaVF: number) {

    var tUrlFinal: string = this.tUrl + '/Venta/PlaneadorVenta' +
      '?Corporacion=' + Corporacion +
      '&Empresa=' + Empresa +
      '&Estatus=' + Estatus +
      '&Situacion=' + Situacion +
      '&Tipo=' + Tipo +
      '&FechacreacionI=' + FechaI +
      '&FechacreacionF=' + FechaF +
      '&FechavencimientoI=' + FechaVI +
      '&FechavencimientoF=' + FechaVF +
      '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Obtener_VentaRpt(Corporacion: string, Empresa: string, Codventa: string) {

    var tUrlFinal: string = this.tUrl + '/Venta/ObtenerVentarRpt?' +
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa +
                            '&Codventa=' + Codventa +
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

}
