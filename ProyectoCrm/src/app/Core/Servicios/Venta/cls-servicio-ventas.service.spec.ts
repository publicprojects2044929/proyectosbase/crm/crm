import { TestBed } from '@angular/core/testing';

import { ClsServicioVentasService } from './cls-servicio-ventas.service';

describe('ClsServicioVentasService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServicioVentasService = TestBed.get(ClsServicioVentasService);
    expect(service).toBeTruthy();
  });
});
