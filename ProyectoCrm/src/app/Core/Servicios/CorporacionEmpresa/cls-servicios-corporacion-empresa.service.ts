import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Respuesta } from 'src/app/Clases/Estructura';

export class ClsServiciosCorporacionEmpresaService {

  public tUrl : string = "";

  constructor(private tHttp: HttpClient, private tUsuario: string, private tHeaders: HttpHeaders) { 


  }

  //#region "Corporacion.api"

  Consumir_Obtener_Corporacion() {

    var tUrlFinal: string = this.tUrl + '/Corporacion/ObtenerCorporacion?Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  //#endregion "Corporacion.api"

  //#region "Empresa.api"

  Consumir_Obtener_Empresa() {

    var tUrlFinal: string = this.tUrl + '/Empresa/ObtenerEmpresas?Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  //#endregion "Empresa.api"
  
}
