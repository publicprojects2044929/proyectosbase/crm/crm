import { TestBed } from '@angular/core/testing';

import { ClsServiciosCorporacionEmpresaService } from './cls-servicios-corporacion-empresa.service';

describe('ClsServiciosCorporacionEmpresaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosCorporacionEmpresaService = TestBed.get(ClsServiciosCorporacionEmpresaService);
    expect(service).toBeTruthy();
  });
});
