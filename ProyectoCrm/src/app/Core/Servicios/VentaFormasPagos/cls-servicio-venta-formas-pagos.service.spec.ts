import { TestBed } from '@angular/core/testing';

import { ClsServicioVentaFormasPagosService } from './cls-servicio-venta-formas-pagos.service';

describe('ClsServicioVentaFormasPagosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServicioVentaFormasPagosService = TestBed.get(ClsServicioVentaFormasPagosService);
    expect(service).toBeTruthy();
  });
});
