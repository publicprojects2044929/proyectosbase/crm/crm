import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Respuesta } from 'src/app/Clases/Estructura';
import { VentaFormasPagosEntrada } from 'src/app/Clases/EstructuraEntrada';

export class ClsServicioVentaFormasPagosService {

  public tUrl: string = "";

  constructor(private tHttp: HttpClient, private tUsuario: string, private tHeaders: HttpHeaders) { 

  }


  Consumir_Obtener_VentaFormasPagos(Corporacion: string, Empresa: string, Codventa: string) {

    var tUrlFinal: string = this.tUrl + '/VentaFormasPago/ObtenerVentaFormaPago' +
                            '?Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa +
                            '&Codventa=' + Codventa +
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Obtener_VentaFormasPagosRpt(Corporacion: string, Empresa: string, Codventa: string, Codformapago: string, Item: number) {

    var tUrlFinal: string = this.tUrl + '/VentaFormasPago/ObtenerVentaFormaPagoRpt' +
                            '?Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa +
                            '&Codventa=' + Codventa +
                            '&Codformapago=' + Codformapago +
                            '&Item=' + Item +
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Aprobar_VentaFormasPagos(VentaFormasP: VentaFormasPagosEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/VentaFormasPago/AprobarVentaFormaPago';

    return this.tHttp.put<Respuesta>(tUrlFinal, VentaFormasP, httpOptions);
  }

  Rechazar_VentaFormasPagos(VentaFormasP: VentaFormasPagosEntrada) {
    
    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/VentaFormasPago/RechazarVentaFormaPago';

    return this.tHttp.put<Respuesta>(tUrlFinal, VentaFormasP, httpOptions);

  }

  Consumir_Crear_VentaFormasPagos(VentaFormasP: VentaFormasPagosEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/VentaFormasPago/CrearVentaFormaPago';

    return this.tHttp.post<Respuesta>(tUrlFinal, VentaFormasP, httpOptions);

  }

}
