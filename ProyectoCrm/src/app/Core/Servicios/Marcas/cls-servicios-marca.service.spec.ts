import { TestBed } from '@angular/core/testing';

import { ClsServiciosMarcaService } from './cls-servicios-marca.service';

describe('ClsServiciosMarcaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosMarcaService = TestBed.get(ClsServiciosMarcaService);
    expect(service).toBeTruthy();
  });
});
