import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Respuesta } from 'src/app/Clases/Estructura';
import { MarcaEntrada } from 'src/app/Clases/EstructuraEntrada';

export class ClsServiciosMarcaService {

  public tUrl: string = "";

  constructor(private tHttp: HttpClient, private tUsuario: string, private tHeaders: HttpHeaders) { 

  }

  Consumir_Obtener_Marca(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/Marca/ObtenerMarca?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa + 
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Obtener_MarcaPorFabricante(Corporacion: string, Empresa: string, Codfabricante: string) {

    var tUrlFinal: string = this.tUrl + '/Marca/ObtenerMarca?'
                            + 'Corporacion=' + Corporacion
                            + '&Empresa=' + Empresa
                            + '&Codfabricante=' + Codfabricante
                            + '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Crear_Marca(tMarca: MarcaEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Marca/CrearMarca';

    return this.tHttp.post<Respuesta>(tUrlFinal, tMarca, httpOptions);

  }

  Consumir_Modificar_Marca(tMarca: MarcaEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Marca/ModificarMarca';

    return this.tHttp.put<Respuesta>(tUrlFinal, tMarca, httpOptions);

  }

  Consumir_Eliminar_Marca(tMarca: MarcaEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Marca/EliminarMarca';

    return this.tHttp.post<Respuesta>(tUrlFinal, tMarca, httpOptions);

  }

}
