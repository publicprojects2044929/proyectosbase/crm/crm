import { TestBed } from '@angular/core/testing';

import { ClsServiciosEstadosService } from './cls-servicios-estados.service';

describe('ClsServiciosEstadosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosEstadosService = TestBed.get(ClsServiciosEstadosService);
    expect(service).toBeTruthy();
  });
});
