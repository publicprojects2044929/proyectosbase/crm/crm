import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Respuesta } from 'src/app/Clases/Estructura';
import { EstadoEntrada } from 'src/app/Clases/EstructuraEntrada';

export class ClsServiciosEstadosService {

  public tUrl: string = "";

  constructor(private tHttp: HttpClient, private tUsuario: string, private tHeaders: HttpHeaders) { 

  }

  Consumir_Obtener_Estados(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/Estado/ObtenerEstado?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa + 
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Obtener_EstadosPorPais(Corporacion: string, Empresa: string, CodPais: string) {

    var tUrlFinal: string = this.tUrl + '/Estado/ObtenerEstado?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa + 
                            '&Codpais=' + CodPais +
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Crear_Estado(tEstado: EstadoEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Estado/CrearEstado';

    return this.tHttp.post<Respuesta>(tUrlFinal, tEstado, httpOptions);

  }

  Consumir_Modificar_Estado(tEstado: EstadoEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Estado/ModificarEstado';

    return this.tHttp.put<Respuesta>(tUrlFinal, tEstado, httpOptions);

  }

  Consumir_Eliminar_Estado(tEstado: EstadoEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Estado/EliminarEstado';

    return this.tHttp.post<Respuesta>(tUrlFinal, tEstado, httpOptions);


  }

}
