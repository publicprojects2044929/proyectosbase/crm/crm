import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Respuesta } from 'src/app/Clases/Estructura';
import { GrupoEntradas } from 'src/app/Clases/EstructuraEntrada';

export class ClsServiciosGruposService {

  public tUrl : string = "";

  constructor(private tHttp: HttpClient, private tUsuario: string, private tHeaders: HttpHeaders) { 

  }

  Consumir_Obtener_Grupo(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/Grupo/ObtenerGrupo?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa + 
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Obtener_Grupo_Full(Corporacion: string, Empresa: string, Codgrupo: string) {

    var tUrlFinal: string = this.tUrl + '/Grupo/ObtenerGrupoFull?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa + 
                            '&Codgrupo=' + Codgrupo + 
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Obtener_GrupoOculto(Corporacion: string, Empresa: string, Oculto: string) {

    var tUrlFinal: string = this.tUrl + '/Grupo/ObtenerGrupoOculto?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa +
                            '&Oculto=' + Oculto + 
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Obtener_GrupoPorCodigoOcultoFull(Corporacion: string, Empresa: string, Codgrupo: string, Oculto: string) {

    var tUrlFinal: string = this.tUrl + '/Grupo/ObtenerGrupoOcultoFull?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa + 
                            '&Codgrupo='+ Codgrupo +
                            '&Oculto=' + Oculto + 
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Crear_Grupo(tGrupo: GrupoEntradas) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Grupo/CrearGrupo';

    return this.tHttp.post<Respuesta>(tUrlFinal, tGrupo, httpOptions);

  }

  Consumir_Modificar_Grupo(tGrupo: GrupoEntradas) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Grupo/ModificarGrupo';

    return this.tHttp.put<Respuesta>(tUrlFinal, tGrupo, httpOptions);

  }

  Consumir_Eliminar_Grupo(tGrupo: GrupoEntradas) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Grupo/EliminarGrupo';

    return this.tHttp.post<Respuesta>(tUrlFinal, tGrupo, httpOptions);

  }

}
