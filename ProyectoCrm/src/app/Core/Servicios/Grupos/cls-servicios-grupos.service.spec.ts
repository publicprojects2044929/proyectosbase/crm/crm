import { TestBed } from '@angular/core/testing';

import { ClsServiciosGruposService } from './cls-servicios-grupos.service';

describe('ClsServiciosGruposService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosGruposService = TestBed.get(ClsServiciosGruposService);
    expect(service).toBeTruthy();
  });
});
