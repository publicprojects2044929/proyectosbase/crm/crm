import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Respuesta } from 'src/app/Clases/Estructura';

export class ClsServiciosTablaService {

  public tUrl: string = "";

  constructor(private tHttp: HttpClient, private tUsuario: string, private tHeaders: HttpHeaders) { 

  }

  Consumir_Obtener_Tabla(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/Tabala/ObtenerTablaConfiguracion?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa + 
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Obtener_TablaEstatus(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/Tabala/ObtenerTablaEstatus?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa + 
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Obtener_TablaTipo(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/Tabala/ObtenerTablaTipo?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa + 
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }
  
}
