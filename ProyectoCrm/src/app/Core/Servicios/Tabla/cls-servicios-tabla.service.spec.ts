import { TestBed } from '@angular/core/testing';

import { ClsServiciosTablaService } from './cls-servicios-tabla.service';

describe('ClsServiciosTablaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosTablaService = TestBed.get(ClsServiciosTablaService);
    expect(service).toBeTruthy();
  });
});
