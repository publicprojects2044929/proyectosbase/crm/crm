import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Respuesta } from 'src/app/Clases/Estructura';
import { PedidoArticuloEntrada } from 'src/app/Clases/EstructuraEntrada';

export class ClsServiciosPedidosArticulosService {

  public tUrl: string = "";

  constructor(private tHttp: HttpClient, private tUsuario: string, private tHeaders: HttpHeaders) { 

  }

  Consumir_Crear_PedidoArticulo(tPedidoArticulo: PedidoArticuloEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Pedido/CrearPedidoArticulo';

    return this.tHttp.post<Respuesta>(tUrlFinal, tPedidoArticulo, httpOptions);

  }

  Consumir_Modificar_PedidoArticulo(tPedidoArticulo: PedidoArticuloEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Pedido/ModificarPedidoArticulo';

    return this.tHttp.put<Respuesta>(tUrlFinal, tPedidoArticulo, httpOptions);

  }

  Consumir_Eliminar_PedidoArticulo(tPedidoArticulo: PedidoArticuloEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Pedido/EliminarPedidoArticulo';

    return this.tHttp.post<Respuesta>(tUrlFinal, tPedidoArticulo, httpOptions);

  }

}
