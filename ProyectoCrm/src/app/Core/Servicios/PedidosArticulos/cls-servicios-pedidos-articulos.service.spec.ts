import { TestBed } from '@angular/core/testing';

import { ClsServiciosPedidosArticulosService } from './cls-servicios-pedidos-articulos.service';

describe('ClsServiciosPedidosArticulosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosPedidosArticulosService = TestBed.get(ClsServiciosPedidosArticulosService);
    expect(service).toBeTruthy();
  });
});
