import { HttpClient, HttpHeaders } from '@angular/common/http';
import { PedidoColeccionesEntrada } from 'src/app/Clases/EstructuraEntrada';
import { Respuesta } from 'src/app/Clases/Estructura';

export class ClsServiciosPedidoColeccionService {

  public tUrl: string = "";

  constructor(private tHttp: HttpClient, private tUsuario: string, private tHeaders: HttpHeaders) {

  }

  Consumir_Obtener_PedidoColeccionesFull(Corporacion: string,
    Empresa: string,
    Codpedido: string,
    Codcoleccion: string) {

    var tUrlFinal: string = this.tUrl + '/PedidoColecciones/ObtenerPedidoColeccionesFull?' +
      'Corporacion=' + Corporacion +
      '&Empresa=' + Empresa +
      '&Codpedido=' + Codpedido +
      '&Codcoleccion=' + Codcoleccion +
      '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);



  }

  Consumir_Crear_PedidoColecciones(tPedido: PedidoColeccionesEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/PedidoColecciones/CrearPedidoColeccion';

    return this.tHttp.post<Respuesta>(tUrlFinal, tPedido, httpOptions);

  }

  Consumir_Eliminar_PedidoColecciones(tPedido: PedidoColeccionesEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/PedidoColecciones/EliminarPedidoColeccion';

    return this.tHttp.post<Respuesta>(tUrlFinal, tPedido, httpOptions);

  }

}
