import { TestBed } from '@angular/core/testing';

import { ClsServiciosPedidoColeccionService } from './cls-servicios-pedido-coleccion.service';

describe('ClsServiciosPedidoColeccionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosPedidoColeccionService = TestBed.get(ClsServiciosPedidoColeccionService);
    expect(service).toBeTruthy();
  });
});
