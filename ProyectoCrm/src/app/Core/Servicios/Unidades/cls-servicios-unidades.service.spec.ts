import { TestBed } from '@angular/core/testing';

import { ClsServiciosUnidadesService } from './cls-servicios-unidades.service';

describe('ClsServiciosUnidadesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosUnidadesService = TestBed.get(ClsServiciosUnidadesService);
    expect(service).toBeTruthy();
  });
});
