import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Respuesta } from 'src/app/Clases/Estructura';
import { UnidadMedidaEntrada } from 'src/app/Clases/EstructuraEntrada';

export class ClsServiciosUnidadesService {

  public tUrl: string = "";

  constructor(private tHttp: HttpClient, private tUsuario: string, private tHeaders: HttpHeaders) { 

  }

  Consumir_Obtener_UnidadesMedida(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/UnidadMedida/ObtenerUnidadMedida?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa + 
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Crear_UnidadMedida(tUnidad: UnidadMedidaEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/UnidadMedida/CrearUnidadMedida';

    return this.tHttp.post<Respuesta>(tUrlFinal, tUnidad, httpOptions);

  }

  Consumir_Modificar_UnidadMedida(tUnidad: UnidadMedidaEntrada) {
    
    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/UnidadMedida/ModificarUnidadMedida';

    return this.tHttp.put<Respuesta>(tUrlFinal, tUnidad, httpOptions);

  }

  Consumir_Eliminar_UnidadMedida(tUnidad: UnidadMedidaEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/UnidadMedida/EliminarUnidadMedida';

    return this.tHttp.post<Respuesta>(tUrlFinal, tUnidad, httpOptions);

  }

}
