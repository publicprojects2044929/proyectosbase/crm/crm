import { TestBed } from '@angular/core/testing';

import { ClsServiciosPedidosArticulosColeccionesService } from './cls-servicios-pedidos-articulos-colecciones.service';

describe('ClsServiciosPedidosArticulosColeccionesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosPedidosArticulosColeccionesService = TestBed.get(ClsServiciosPedidosArticulosColeccionesService);
    expect(service).toBeTruthy();
  });
});
