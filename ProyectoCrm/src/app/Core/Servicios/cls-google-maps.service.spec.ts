import { TestBed } from '@angular/core/testing';

import { ClsGoogleMapsService } from './cls-google-maps.service';

describe('ClsGoogleMapsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsGoogleMapsService = TestBed.get(ClsGoogleMapsService);
    expect(service).toBeTruthy();
  });
});
