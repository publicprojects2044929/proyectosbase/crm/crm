import { TestBed } from '@angular/core/testing';

import { ClsServiciosPaisesService } from './cls-servicios-paises.service';

describe('ClsServiciosPaisesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosPaisesService = TestBed.get(ClsServiciosPaisesService);
    expect(service).toBeTruthy();
  });
});
