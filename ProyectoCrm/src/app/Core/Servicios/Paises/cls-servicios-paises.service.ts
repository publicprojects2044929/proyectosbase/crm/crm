import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Respuesta } from 'src/app/Clases/Estructura';
import { PaisEntrada } from 'src/app/Clases/EstructuraEntrada';

export class ClsServiciosPaisesService {

  public tUrl: string = "";

  constructor(private tHttp: HttpClient, private tUsuario: string, private tHeaders: HttpHeaders) { 

  }

  Consumir_Obtener_Paises(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/Pais/ObtenerPais?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa + 
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Obtener_PaisEstado(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/Pais/ObtenerPaisEstado?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa +
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Obtener_PaisEstadoMunicipio(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/Pais/ObtenerPaisEstadoMunicipio?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa +
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Obtener_PaisFull(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/Pais/ObtenerPaisFull?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa +
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Crear_Pais(tPais: PaisEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Pais/CrearPais';

    return this.tHttp.post<Respuesta>(tUrlFinal, tPais, httpOptions);

  }

  Consumir_Modificar_Pais(tPais: PaisEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Pais/ModificarPais';

    return this.tHttp.put<Respuesta>(tUrlFinal, tPais, httpOptions);

  }

  Consumir_Eliminar_Pais(tPais: PaisEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Pais/EliminarPais';

    return this.tHttp.post<Respuesta>(tUrlFinal, tPais, httpOptions);


  }

}
