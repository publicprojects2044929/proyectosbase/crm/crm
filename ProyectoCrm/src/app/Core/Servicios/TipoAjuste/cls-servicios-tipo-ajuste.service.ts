import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Respuesta } from 'src/app/Clases/Estructura';
import { TipoAjustesInventarioEntrada } from 'src/app/Clases/EstructuraEntrada';

export class ClsServiciosTipoAjusteService {

  public tUrl: string = "";
 
  constructor(private tHttp: HttpClient, private tUsuario: string, private tHeaders: HttpHeaders) { 

  }

  Consumir_Obtener_TipoAjuste(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/TipoAjusteInventario/ObtenerTipoAjusteInventario?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa + 
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }


  Consumir_Crear_TipoAjuste(tTipoAjustes: TipoAjustesInventarioEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/TipoAjusteInventario/CrearTipoAjusteInventario';

    return this.tHttp.post<Respuesta>(tUrlFinal, tTipoAjustes, httpOptions);

  }

  Consumir_Modificar_TipoAjuste(tTipoAjustes: TipoAjustesInventarioEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/TipoAjusteInventario/ModificarTipoAjusteInventario';

    return this.tHttp.put<Respuesta>(tUrlFinal, tTipoAjustes, httpOptions);

  }

  Consumir_Eliminar_TipoAjuste(tTipoAjustes: TipoAjustesInventarioEntrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/TipoAjusteInventario/EliminarTipoAjusteInventario';

    return this.tHttp.post<Respuesta>(tUrlFinal, tTipoAjustes, httpOptions);

  }

}
