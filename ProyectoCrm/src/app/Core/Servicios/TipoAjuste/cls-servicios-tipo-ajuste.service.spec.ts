import { TestBed } from '@angular/core/testing';

import { ClsServiciosTipoAjusteService } from './cls-servicios-tipo-ajuste.service';

describe('ClsServiciosTipoAjusteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosTipoAjusteService = TestBed.get(ClsServiciosTipoAjusteService);
    expect(service).toBeTruthy();
  });
});
