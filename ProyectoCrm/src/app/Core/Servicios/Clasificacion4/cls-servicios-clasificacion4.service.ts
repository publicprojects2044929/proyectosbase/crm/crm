import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Respuesta } from 'src/app/Clases/Estructura';
import { Clasificacion4Entrada } from 'src/app/Clases/EstructuraEntrada';

export class ClsServiciosClasificacion4Service {

  public tUrl: string = "";

  constructor(private tHttp: HttpClient, private tUsuario: string, private tHeaders: HttpHeaders) { 

  }

  Consumir_Obtener_Clasificacion4(Corporacion: string, Empresa: string) {

    var tUrlFinal: string = this.tUrl + '/Clasificacion4/ObtenerClasificacion4?' + 
                            'Corporacion=' + Corporacion +
                            '&Empresa=' + Empresa + 
                            '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Obtener_Clasificacion4PorClasificacion3PorClasificacion1YClasificacion2(Corporacion: string, Empresa: string, Codclasificacion1: string, Codclasificacion2: string, Codclasificacion3: string) {

    var tUrlFinal: string = this.tUrl + '/Clasificacion4/ObtenerClasificacion4?'
                            + 'Corporacion=' + Corporacion
                            + '&Empresa=' + Empresa
                            + '&Codclasificacion1=' + Codclasificacion1
                            + '&Codclasificacion2=' + Codclasificacion2
                            + '&Codclasificacion3=' + Codclasificacion3
                            + '&Usuario=' + this.tUsuario;

    return this.tHttp.get<Respuesta>(tUrlFinal);

  }

  Consumir_Crear_Clasificacion4(tClasificacion4: Clasificacion4Entrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Clasificacion4/CrearClasificacion4';

    return this.tHttp.post<Respuesta>(tUrlFinal, tClasificacion4, httpOptions);

  }

  Consumir_Modificar_Clasificacion4(tClasificacion4: Clasificacion4Entrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Clasificacion4/ModificarClasificacion4';

    return this.tHttp.put<Respuesta>(tUrlFinal, tClasificacion4, httpOptions);

  }

  Consumir_Eliminar_Clasificacion4(tClasificacion4: Clasificacion4Entrada) {

    const httpOptions = {
      headers: this.tHeaders
    };

    var tUrlFinal: string = this.tUrl + '/Clasificacion4/EliminarClasificacion4';

    return this.tHttp.post<Respuesta>(tUrlFinal, tClasificacion4, httpOptions);

  }
  
}
