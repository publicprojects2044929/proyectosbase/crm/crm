import { TestBed } from '@angular/core/testing';

import { ClsServiciosClasificacion4Service } from './cls-servicios-clasificacion4.service';

describe('ClsServiciosClasificacion4Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClsServiciosClasificacion4Service = TestBed.get(ClsServiciosClasificacion4Service);
    expect(service).toBeTruthy();
  });
});
