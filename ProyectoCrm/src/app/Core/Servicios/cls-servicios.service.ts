import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment'

import { Configuracion } from 'src/app/Clases/EstructuraConfiguracion';
import { Empresas, Usuario } from 'src/app/Clases/Estructura';
import { SessionService } from '../Session/session.service';

import { ClsServiciosCorporacionEmpresaService } from './CorporacionEmpresa/cls-servicios-corporacion-empresa.service';
import { ClsServiciosUsuariosService } from './Usuarios/cls-servicios-usuarios.service';
import { ClsServiciosGruposService } from './Grupos/cls-servicios-grupos.service';
import { ClsServiciosEstatusService } from './Estatus/cls-servicios-estatus.service';
import { ClsServiciosTipoService } from './Tipo/cls-servicios-tipo.service';
import { ClsServiciosFormularioService } from './Formulario/cls-servicios-formulario.service';
import { ClsServiciosClasificacion1Service } from './Clasificacion1/cls-servicios-clasificacion1.service';
import { ClsServiciosClasificacion2Service } from './Clasificacion2/cls-servicios-clasificacion2.service';
import { ClsServiciosClasificacion3Service } from './Clasificacion3/cls-servicios-clasificacion3.service'
import { ClsServiciosClasificacion4Service } from './Clasificacion4/cls-servicios-clasificacion4.service';
import { ClsServiciosColoresService } from './Colores/cls-servicios-colores.service';
import { ClsServiciosUnidadesService } from './Unidades/cls-servicios-unidades.service';
import { ClsServiciosFabricanteService } from './Fabricante/cls-servicios-fabricante.service';
import { ClsServiciosMarcaService } from './Marcas/cls-servicios-marca.service';
import { ClsServiciosModeloService } from './Modelos/cls-servicios-modelo.service';
import { ClsServiciosCuponService } from './Cupones/cls-servicios-cupon.service';
import { ClsServiciosClienteService } from './Clientes/cls-servicios-cliente.service';
import { ClsServiciosArticulosService } from './Articulos/cls-servicios-articulos.service';
import { ClsServiciosArticulosImagenesService } from './ArticulosImagenes/cls-servicios-articulos-imagenes.service';
import { ClsServiciosAlmacenService } from './Almacen/cls-servicios-almacen.service';
import { ClsServiciosPromocionesService } from './Promociones/cls-servicios-promociones.service';
import { ClsServiciosConfiguracionesService } from './Configuracion/cls-servicios-configuraciones.service';
import { ClsServiciosPaisesService } from './Paises/cls-servicios-paises.service';
import { ClsServiciosEstadosService } from './Estados/cls-servicios-estados.service';
import { ClsServiciosMunicipiosService } from './Municipios/cls-servicios-municipios.service';
import { ClsServiciosCiudadesService } from './Ciudades/cls-servicios-ciudades.service';
import { ClsServiciosMonedasService } from './Monedas/cls-servicios-monedas.service';
import { ClsServiciosMonedasDenominacionesService } from './MonedasDenominaciones/cls-servicios-monedas-denominaciones.service';
import { ClsServiciosMonedasTasasService } from './MonedasTasas/cls-servicios-monedas-tasas.service';
import { ClsServiciosPedidosService } from './Pedidos/cls-servicios-pedidos.service';
import { ClsServiciosPedidosArticulosService } from './PedidosArticulos/cls-servicios-pedidos-articulos.service';
import { ClsServiciosGenerosService } from './Generos/cls-servicios-generos.service';
import { ClsServiciosEstadoCivilService } from './EstadoCivil/cls-servicios-estado-civil.service';
import { ClsServiciosClientesTelefonosService } from './ClientesTelefonos/cls-servicios-clientes-telefonos.service';
import { ClsServiciosTipoAjusteService } from './TipoAjuste/cls-servicios-tipo-ajuste.service';
import { ClsServiciosFormasPagoService } from './FormasPago/cls-servicios-formas-pago.service';
import { ClsServiciosOcupacionService } from './Ocupacion/cls-servicios-ocupacion.service';
import { ClsServiciosSituacionService } from './Situacion/cls-servicios-situacion.service';
import { ClsServiciosCondicionService } from './Condicion/cls-servicios-condicion.service';
import { ClsServicioVentasService } from './Venta/cls-servicio-ventas.service';
import { ClsServiciosArticulosInventarioService } from './ArticulosInventario/cls-servicios-articulos-inventario.service';
import { ClsServiciosAjustesInventarioService } from './AjusteInventario/cls-servicios-ajustes-inventario.service';
import { ClsServicioVentaFormasPagosService } from './VentaFormasPagos/cls-servicio-venta-formas-pagos.service';
import { ClsServiciosColeccionService } from './Coleccion/cls-servicios-coleccion.service';
import { ClsServiciosPedidoColeccionService } from './PedidosColeccion/cls-servicios-pedido-coleccion.service';
import { ClsServiciosPedidosArticulosColeccionesService } from './PedidosArticulosColecciones/cls-servicios-pedidos-articulos-colecciones.service';
import { ClsServiciosColecccionArticulosService } from './ColeccionArticulos/cls-servicios-colecccion-articulos.service'
import { BehaviorSubject, Observable } from 'rxjs';
import { ClsServiciosTablaService } from './Tabla/cls-servicios-tabla.service';
import { ClsServiciosClientesDocumentosService } from './ClientesDocumentos/cls-servicios-clientes-documentos.service';
import { ClsServiciosColoresHexadecimalesService } from './ColoresHexadecimales/cls-servicios-colores-hexadecimales.service';
import { ClsServiciosTicketService } from './Ticket/cls-servicios-ticket.service';

@Injectable({
  providedIn: 'root'
})
export class ClsServiciosService {

  private tHayConfig = new BehaviorSubject<boolean>(false);
  public tHayConfig$: Observable<boolean> = this.tHayConfig.asObservable();

  private Url: string = "";
  // private Url: string = "/Api";
  // private Url: string = "http://192.168.4.10:8091/Api";
  // private Url: string = "http://50.63.14.104:8091/Api";
  private tUsuarioActual: Usuario = new Usuario();
  private tEmpresaActual: Empresas = new Empresas();
  private tConfiguracion: Configuracion;
  private tServicioCorporacion: ClsServiciosCorporacionEmpresaService;
  private tServicioUsuario: ClsServiciosUsuariosService;
  private tServicioGrupo: ClsServiciosGruposService;
  private tServicioEstatus: ClsServiciosEstatusService;
  private tServicioTipo: ClsServiciosTipoService;
  private tServicioFormulario: ClsServiciosFormularioService;
  private tServicioClasificacion1: ClsServiciosClasificacion1Service;
  private tServicioClasificacion2: ClsServiciosClasificacion2Service;
  private tServicioClasificacion3: ClsServiciosClasificacion3Service;
  private tServicioClasificacion4: ClsServiciosClasificacion4Service;
  private tServicioColor: ClsServiciosColoresService;
  private tServicioUnidad: ClsServiciosUnidadesService;
  private tServicioFabricante: ClsServiciosFabricanteService;
  private tServicioMarca: ClsServiciosMarcaService;
  private tServicioModelo: ClsServiciosModeloService;
  private tServicioCupon: ClsServiciosCuponService;
  private tServicioCliente: ClsServiciosClienteService;
  private tServicioArticulo: ClsServiciosArticulosService;
  private tServicioArticuloImagenes: ClsServiciosArticulosImagenesService;
  private tServicioAlmacen: ClsServiciosAlmacenService;
  private tServicioPromociones: ClsServiciosPromocionesService;
  private tServicioConfiguracion: ClsServiciosConfiguracionesService;
  private tServicioPaises: ClsServiciosPaisesService;
  private tServicioEstados: ClsServiciosEstadosService;
  private tServicioMunicipios: ClsServiciosMunicipiosService;
  private tServicioCiudades: ClsServiciosCiudadesService;
  private tServicioMonedas: ClsServiciosMonedasService;
  private tServicioMonedasDeno: ClsServiciosMonedasDenominacionesService;
  private tServicioMonedasTasas: ClsServiciosMonedasTasasService;
  private tServicioPedidos: ClsServiciosPedidosService;
  private tServicioPedidosArt: ClsServiciosPedidosArticulosService;
  private tServicioGenero: ClsServiciosGenerosService;
  private tServicioEstadoCivil: ClsServiciosEstadoCivilService;
  private tServiciosClientesTelf: ClsServiciosClientesTelefonosService;
  private tServiciosTipoAjuste: ClsServiciosTipoAjusteService;
  private tServiciosFormasPago: ClsServiciosFormasPagoService;
  private tServiciosOcupacion: ClsServiciosOcupacionService;
  private tServiciosSituacion: ClsServiciosSituacionService;
  private tServiciosCondicion: ClsServiciosCondicionService;
  private tServiciosVenta: ClsServicioVentasService;
  private tServicioArticuloInv: ClsServiciosArticulosInventarioService;
  private tServicioAjusteInv: ClsServiciosAjustesInventarioService;
  private tServicioVentaFormaPago: ClsServicioVentaFormasPagosService;
  private tServicioColeccion: ClsServiciosColeccionService;
  private tServicioPedicoColeccion: ClsServiciosPedidoColeccionService;
  private tServicioPedidoArtColecciones: ClsServiciosPedidosArticulosColeccionesService;
  private tServicioColeccionesArticulos: ClsServiciosColecccionArticulosService;
  private tServicioTablas: ClsServiciosTablaService;
  private tServicioClienteDocumentos: ClsServiciosClientesDocumentosService;
  private tServicioColorHexadecimal: ClsServiciosColoresHexadecimalesService;
  private tServicioTicket: ClsServiciosTicketService;
  private Headers = new HttpHeaders;

  constructor(private http: HttpClient,
    private tSession: SessionService) {

    this.tSession.tUsuarioActual$.subscribe((Usuario: Usuario) => {

      this.tUsuarioActual = Usuario;

    });

    this.tSession.tEmpresaActual$.subscribe((Empresa: Empresas) => {

      this.tEmpresaActual = Empresa;

    });

    this.Headers = new HttpHeaders({

      'content-type': 'application/json'

    });

    this.tServicioCorporacion = new ClsServiciosCorporacionEmpresaService(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServicioUsuario = new ClsServiciosUsuariosService(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServicioGrupo = new ClsServiciosGruposService(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServicioEstatus = new ClsServiciosEstatusService(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServicioTipo = new ClsServiciosTipoService(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServicioFormulario = new ClsServiciosFormularioService(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServicioClasificacion1 = new ClsServiciosClasificacion1Service(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServicioClasificacion2 = new ClsServiciosClasificacion2Service(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServicioClasificacion3 = new ClsServiciosClasificacion3Service(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServicioClasificacion4 = new ClsServiciosClasificacion4Service(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServicioColor = new ClsServiciosColoresService(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServicioUnidad = new ClsServiciosUnidadesService(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServicioFabricante = new ClsServiciosFabricanteService(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServicioMarca = new ClsServiciosMarcaService(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServicioModelo = new ClsServiciosModeloService(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServicioCupon = new ClsServiciosCuponService(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServicioCliente = new ClsServiciosClienteService(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServicioArticulo = new ClsServiciosArticulosService(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServicioArticuloImagenes = new ClsServiciosArticulosImagenesService(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServicioAlmacen = new ClsServiciosAlmacenService(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServicioPromociones = new ClsServiciosPromocionesService(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServicioConfiguracion = new ClsServiciosConfiguracionesService(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServicioPaises = new ClsServiciosPaisesService(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServicioEstados = new ClsServiciosEstadosService(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServicioMunicipios = new ClsServiciosMunicipiosService(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServicioCiudades = new ClsServiciosCiudadesService(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServicioMonedas = new ClsServiciosMonedasService(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServicioMonedasDeno = new ClsServiciosMonedasDenominacionesService(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServicioMonedasTasas = new ClsServiciosMonedasTasasService(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServicioPedidos = new ClsServiciosPedidosService(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServicioPedidosArt = new ClsServiciosPedidosArticulosService(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServicioGenero = new ClsServiciosGenerosService(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServicioEstadoCivil = new ClsServiciosEstadoCivilService(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServiciosClientesTelf = new ClsServiciosClientesTelefonosService(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServiciosTipoAjuste = new ClsServiciosTipoAjusteService(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServiciosFormasPago = new ClsServiciosFormasPagoService(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServiciosOcupacion = new ClsServiciosOcupacionService(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServiciosSituacion = new ClsServiciosSituacionService(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServiciosCondicion = new ClsServiciosCondicionService(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServiciosVenta = new ClsServicioVentasService(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServicioArticuloInv = new ClsServiciosArticulosInventarioService(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServicioAjusteInv = new ClsServiciosAjustesInventarioService(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServicioVentaFormaPago = new ClsServicioVentaFormasPagosService(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServicioColeccion = new ClsServiciosColeccionService(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServicioPedicoColeccion = new ClsServiciosPedidoColeccionService(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServicioPedidoArtColecciones = new ClsServiciosPedidosArticulosColeccionesService(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServicioColeccionesArticulos = new ClsServiciosColecccionArticulosService(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServicioTablas = new ClsServiciosTablaService(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServicioClienteDocumentos = new ClsServiciosClientesDocumentosService(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServicioColorHexadecimal = new ClsServiciosColoresHexadecimalesService(http, this.tUsuarioActual.Codusuario, this.Headers);
    this.tServicioTicket = new ClsServiciosTicketService(http, this.tUsuarioActual.Codusuario, this.Headers);

    // this.InicializarServicios();

    this.tHayConfig.next(false);

    http.get<Configuracion>('Configuracion/' + environment.ArchivoConfig)
      .subscribe(data => {

        this.tConfiguracion = data;
        this.Url = this.tConfiguracion.UrlWs;
        this.InicializarServicios();
        this.tHayConfig.next(true);

      });

  }

  private InicializarServicios() {

    this.InicializarCorporacionEmpresa();
    this.InicializarUsuario();
    this.InicializarGrupo();
    this.InicializarEstatus();
    this.InicializarTipo();
    this.InicializarFormulario();
    this.InicializarClasificacion1();
    this.InicializarClasificacion2();
    this.InicializarClasificacion3();
    this.InicializarClasificacion4();
    this.InicializarColor();
    this.InicializarUnidad();
    this.InicializarFabricante();
    this.InicializarMarca();
    this.InicializarModelo();
    this.InicializarCupon();
    this.InicializarCliente();
    this.InicializarArticulo();
    this.InicializarArticuloImagenes();
    this.InicializarArticuloInventario();
    this.InicializarAlmacen();
    this.InicializarPromociones();
    this.InicializarConfiguraciones();
    this.InicializarPaises();
    this.InicializarEstados();
    this.InicializarMunicipios();
    this.InicializarCiudades();
    this.InicializarMonedas();
    this.InicializarMonedasDeno();
    this.InicializarMonedasTasas();
    this.InicializarPedidos();
    this.InicializarPedidosArt();
    this.InicializarGenero();
    this.InicializarEstadoCivil();
    this.InicializarClientesTelf();
    this.InicializarTipoAjuste();
    this.InicializarFormasPago();
    this.InicializarOcupacion();
    this.InicializarCondicion();
    this.InicializarSituacion();
    this.InicializarVenta();
    this.InicializarAjusteInventario();
    this.InicializarVentaFormasPago();
    this.InicializarColeccion();
    this.InicializarPedidoColeccion();
    this.InicializarPedidoArticulosColecciones();
    this.InicializarColeccionArticulos();
    this.InicializarTabla();
    this.InicializarClienteDocumentos();
    this.InicializarColorHexadecimal();
    this.InicializarTicket();

  }

  //#region "CorporacionEmpresa.api"

  private InicializarCorporacionEmpresa() {

    this.tServicioCorporacion.tUrl = this.Url;

  }

  CorporacionEmpresa(): ClsServiciosCorporacionEmpresaService {

    return this.tServicioCorporacion;

  }

  //#endregion

  //#region "Usuario.api"

  private InicializarUsuario() {

    this.tServicioUsuario.tUrl = this.Url;

  }

  Usuario(): ClsServiciosUsuariosService {

    return this.tServicioUsuario;

  }

  //#endregion "Usuario.api"

  //#region "Grupos.api"

  private InicializarGrupo() {

    this.tServicioGrupo.tUrl = this.Url;

  }

  Grupo(): ClsServiciosGruposService {

    return this.tServicioGrupo;

  }

  //#endregion "Grupos.api"

  //#region "Estatus.api"

  private InicializarEstatus() {

    this.tServicioEstatus.tUrl = this.Url;

  }

  Estatus(): ClsServiciosEstatusService {

    return this.tServicioEstatus;

  }

  //#endregion

  //#region "Tipo.api"

  private InicializarTipo() {

    this.tServicioTipo.tUrl = this.Url;

  }

  Tipo(): ClsServiciosTipoService {

    return this.tServicioTipo;

  }

  //#endregion

  //#region "Formulario.api"

  private InicializarFormulario() {

    this.tServicioFormulario.tUrl = this.Url;

  }

  Formulario(): ClsServiciosFormularioService {

    return this.tServicioFormulario;

  }

  //#endregion "Formulario.api"

  //#region "Clasificacion1.api"

  private InicializarClasificacion1() {

    this.tServicioClasificacion1.tUrl = this.Url;

  }

  Clasificacion1(): ClsServiciosClasificacion1Service {

    return this.tServicioClasificacion1;

  }

  //#endregion

  //#region "Clasificacion2.api"

  private InicializarClasificacion2() {

    this.tServicioClasificacion2.tUrl = this.Url;

  }

  Clasificacion2(): ClsServiciosClasificacion2Service {

    return this.tServicioClasificacion2;

  }

  //#endregion

  //#region "Clasificacion3.api"

  private InicializarClasificacion3() {

    this.tServicioClasificacion3.tUrl = this.Url;

  }

  Clasificacion3(): ClsServiciosClasificacion3Service {

    return this.tServicioClasificacion3;

  }

  //#endregion

  //#region "Clasificacion4.api"

  private InicializarClasificacion4() {

    this.tServicioClasificacion4.tUrl = this.Url;

  }

  Clasificacion4(): ClsServiciosClasificacion4Service {

    return this.tServicioClasificacion4;

  }

  //#endregion

  //#region "Colores.api"

  private InicializarColor() {

    this.tServicioColor.tUrl = this.Url;

  }

  Color(): ClsServiciosColoresService {

    return this.tServicioColor;

  }

  //#endregion

  //#region "Unidades.api"

  private InicializarUnidad() {

    this.tServicioUnidad.tUrl = this.Url;

  }

  Unidad(): ClsServiciosUnidadesService {

    return this.tServicioUnidad;

  }

  //#endregion

  //#region "Fabricantes.api"

  private InicializarFabricante() {

    this.tServicioFabricante.tUrl = this.Url;

  }

  Fabricante(): ClsServiciosFabricanteService {

    return this.tServicioFabricante;

  }

  //#endregion

  //#region "Marcas.api"

  private InicializarMarca() {

    this.tServicioMarca.tUrl = this.Url;

  }

  Marca(): ClsServiciosMarcaService {

    return this.tServicioMarca;

  }

  //#endregion

  //#region "Modelos.api"

  private InicializarModelo() {

    this.tServicioModelo.tUrl = this.Url;

  }

  Modelo(): ClsServiciosModeloService {

    return this.tServicioModelo;

  }

  //#endregion

  //#region "Cupones.api"

  private InicializarCupon() {

    this.tServicioCupon.tUrl = this.Url;

  }

  Cupon(): ClsServiciosCuponService {

    return this.tServicioCupon;

  }

  //#endregion

  //#region "Cliente.api"

  private InicializarCliente() {

    this.tServicioCliente.tUrl = this.Url;

  }

  Cliente(): ClsServiciosClienteService {

    return this.tServicioCliente;

  }

  //#endregion

  //#region "Articulo.api"

  private InicializarArticulo() {

    this.tServicioArticulo.tUrl = this.Url;

  }

  Articulo(): ClsServiciosArticulosService {

    return this.tServicioArticulo;

  }

  //#endregion

  //#region "ArticulosImagenes.api"

  private InicializarArticuloImagenes() {

    this.tServicioArticuloImagenes.tUrl = this.Url;

  }

  ArticuloImagenes(): ClsServiciosArticulosImagenesService {

    return this.tServicioArticuloImagenes;

  }

  //#endregion

  //#region "ArticulosInventario.api"

  private InicializarArticuloInventario() {

    this.tServicioArticuloInv.tUrl = this.Url;

  }

  ArticuloInventario(): ClsServiciosArticulosInventarioService {

    return this.tServicioArticuloInv;

  }

  //#endregion

  //#region "Almacen.api"

  private InicializarAlmacen() {

    this.tServicioAlmacen.tUrl = this.Url;

  }

  Almacen(): ClsServiciosAlmacenService {

    return this.tServicioAlmacen;

  }

  //#endregion

  //#region "Promocion.api"

  private InicializarPromociones() {

    this.tServicioPromociones.tUrl = this.Url;

  }

  Promociones(): ClsServiciosPromocionesService {

    return this.tServicioPromociones;

  }

  //#endregion

  //#region "Configuracion.api"

  private InicializarConfiguraciones() {

    this.tServicioConfiguracion.tUrl = this.Url;

  }

  Configuraciones(): ClsServiciosConfiguracionesService {

    return this.tServicioConfiguracion;

  }

  //#endregion

  //#region "Pais.api"

  private InicializarPaises() {

    this.tServicioPaises.tUrl = this.Url;

  }

  Paises(): ClsServiciosPaisesService {

    return this.tServicioPaises;

  }

  //#endregion

  //#region "Estado.api"

  private InicializarEstados() {

    this.tServicioEstados.tUrl = this.Url;

  }

  Estados(): ClsServiciosEstadosService {

    return this.tServicioEstados;

  }

  //#endregion

  //#region "Municipio.api"

  private InicializarMunicipios() {

    this.tServicioMunicipios.tUrl = this.Url;

  }

  Municipios(): ClsServiciosMunicipiosService {

    return this.tServicioMunicipios;

  }

  //#endregion

  //#region "Ciudad.api"

  private InicializarCiudades() {

    this.tServicioCiudades.tUrl = this.Url;

  }

  Ciudades(): ClsServiciosCiudadesService {

    return this.tServicioCiudades;

  }

  //#endregion

  //#region "Moneda.api"

  private InicializarMonedas() {

    this.tServicioMonedas.tUrl = this.Url;

  }

  Monedas(): ClsServiciosMonedasService {

    return this.tServicioMonedas;

  }

  //#endregion

  //#region "MonedaDenominacion.api"

  private InicializarMonedasDeno() {

    this.tServicioMonedasDeno.tUrl = this.Url;

  }

  MonedasDeno(): ClsServiciosMonedasDenominacionesService {

    return this.tServicioMonedasDeno;

  }

  //#endregion

  //#region "MonedaTasa.api"

  private InicializarMonedasTasas() {

    this.tServicioMonedasTasas.tUrl = this.Url;

  }

  MonedasTasas(): ClsServiciosMonedasTasasService {

    return this.tServicioMonedasTasas;

  }

  //#endregion

  //#region "Pedidos.api"

  private InicializarPedidos() {

    this.tServicioPedidos.tUrl = this.Url;

  }

  Pedidos(): ClsServiciosPedidosService {

    return this.tServicioPedidos;

  }

  //#endregion

  //#region "PedidosArticulo.api"

  private InicializarPedidosArt() {

    this.tServicioPedidosArt.tUrl = this.Url;

  }

  PedidosArticulos(): ClsServiciosPedidosArticulosService {

    return this.tServicioPedidosArt;

  }

  //#endregion

  //#region "Genero.api"

  private InicializarGenero() {

    this.tServicioGenero.tUrl = this.Url;

  }

  Genero(): ClsServiciosGenerosService {

    return this.tServicioGenero;

  }

  //#endregion

  //#region "EstadosCivil.api"

  private InicializarEstadoCivil() {

    this.tServicioEstadoCivil.tUrl = this.Url;

  }

  EstadoCivil(): ClsServiciosEstadoCivilService {

    return this.tServicioEstadoCivil;

  }

  //#endregion

  //#region "Cliente_Telefonos.api"

  private InicializarClientesTelf() {

    this.tServiciosClientesTelf.tUrl = this.Url;

  }

  ClientesTelf(): ClsServiciosClientesTelefonosService {

    return this.tServiciosClientesTelf;

  }


  //#endregion


  //#region "TipoAjuste.api"

  private InicializarTipoAjuste() {

    this.tServiciosTipoAjuste.tUrl = this.Url;

  }

  TipoAjuste(): ClsServiciosTipoAjusteService {

    return this.tServiciosTipoAjuste;

  }

  //#endregion


  //#region "FormasPago.api"

  private InicializarFormasPago() {

    this.tServiciosFormasPago.tUrl = this.Url;

  }

  FormasPago(): ClsServiciosFormasPagoService {

    return this.tServiciosFormasPago;

  }

  //#endregion

  //#region "Ocupacion.api"

  private InicializarOcupacion() {

    this.tServiciosOcupacion.tUrl = this.Url;

  }

  Ocupacion(): ClsServiciosOcupacionService {

    return this.tServiciosOcupacion;

  }
  //#endregion

  //#region "Situacion.api"

  private InicializarSituacion() {

    this.tServiciosSituacion.tUrl = this.Url;

  }

  Situacion(): ClsServiciosSituacionService {

    return this.tServiciosSituacion;

  }
  //#endregion


  //#region "Condicion.api"

  private InicializarCondicion() {

    this.tServiciosCondicion.tUrl = this.Url;

  }

  Condicion(): ClsServiciosCondicionService {

    return this.tServiciosCondicion;

  }

  //#endregion

  //#region "Venta.api"

  private InicializarVenta() {

    this.tServiciosVenta.tUrl = this.Url;

  }

  Venta(): ClsServicioVentasService {

    return this.tServiciosVenta;

  }

  //#endregion

  //#region "AjusteInventario.api"

  private InicializarAjusteInventario() {

    this.tServicioAjusteInv.tUrl = this.Url;

  }

  AjusteInventario(): ClsServiciosAjustesInventarioService {

    return this.tServicioAjusteInv;

  }

  //#endregion

  //#region "Coleccion.api"

  private InicializarColeccion() {

    this.tServicioColeccion.tUrl = this.Url;

  }

  Coleccion(): ClsServiciosColeccionService {

    return this.tServicioColeccion;

  }

  //#endregion

  //#region "PedidoColeccion.api"

  private InicializarPedidoColeccion() {

    this.tServicioPedicoColeccion.tUrl = this.Url;

  }

  PedidoColeccion(): ClsServiciosPedidoColeccionService {

    return this.tServicioPedicoColeccion;

  }

  //#endregion

  //#region "PedidoColeccion.api"

  private InicializarPedidoArticulosColecciones() {

    this.tServicioPedidoArtColecciones.tUrl = this.Url;

  }

  PedidoArticulosColecciones(): ClsServiciosPedidosArticulosColeccionesService {

    return this.tServicioPedidoArtColecciones;

  }

  //#endregion

  //#region "VentaFormasPago.api"

  private InicializarVentaFormasPago() {

    this.tServicioVentaFormaPago.tUrl = this.Url;

  }

  VentaFormasPagos(): ClsServicioVentaFormasPagosService {

    return this.tServicioVentaFormaPago;

  }

  //#endregion

  //#region "ColeccionArticulos.api"

  private InicializarColeccionArticulos() {

    this.tServicioColeccionesArticulos.tUrl = this.Url;

  }

  ColeccionArticulos(): ClsServiciosColecccionArticulosService {

    return this.tServicioColeccionesArticulos;

  }

  //#endregion

  //#region "Tabla.api"

  private InicializarTabla() {

    this.tServicioTablas.tUrl = this.Url;

  }

  Tabla(): ClsServiciosTablaService {

    return this.tServicioTablas;

  }

  //#endregion

  //#region "ClienteDocumentos.api"

  private InicializarClienteDocumentos() {

    this.tServicioClienteDocumentos.tUrl = this.Url;

  }

  ClienteDocumentos(): ClsServiciosClientesDocumentosService {

    return this.tServicioClienteDocumentos;

  }

  //#endregion

  //#region "ColorHexadecimal.api"

  private InicializarColorHexadecimal() {

    this.tServicioColorHexadecimal.tUrl = this.Url;

  }

  ColorHexadecimal(): ClsServiciosColoresHexadecimalesService {

    return this.tServicioColorHexadecimal;

  }

  //#endregion

  //#region "Ticket.api"

  private InicializarTicket() {

    this.tServicioTicket.tUrl = this.Url;

  }

  Ticket(): ClsServiciosTicketService {

    return this.tServicioTicket;

  }

  //#endregion

}
