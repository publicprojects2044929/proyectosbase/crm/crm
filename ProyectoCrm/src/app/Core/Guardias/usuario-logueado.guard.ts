import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { SessionService } from 'src/app/Core/Session/session.service';

@Injectable({
  providedIn: 'root'
})
export class UsuarioLogueadoGuard implements CanActivate {

  private tUsuarioLogueado: boolean = false;

  constructor(private router: Router,
    private tSession: SessionService) {


    this.tSession.tHayUsuario$.subscribe(tHayUsuario => {

      this.tUsuarioLogueado = tHayUsuario;

    })

  }


  canActivate() {

    if (this.tUsuarioLogueado === false) {

      this.router.navigate(['/']);
      return false;

    }
    else {

      return true;

    }

  }

}
