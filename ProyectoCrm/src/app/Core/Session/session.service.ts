import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Empresas, Usuario, FormularioPadre, Respuesta, Licencia, FormularioTareas } from '../../Clases/Estructura'
import { environment } from '../../../environments/environment'
import * as CryptoJS from 'crypto-js';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  private tHayUsuario = new BehaviorSubject<boolean>(false);
  public tHayUsuario$: Observable<boolean> = this.tHayUsuario.asObservable();

  private tUsuario: Usuario = new Usuario();
  private tUsuarioActual = new BehaviorSubject<Usuario>(new Usuario());
  public tUsuarioActual$: Observable<Usuario> = this.tUsuarioActual.asObservable();

  private tModulosActuales = new BehaviorSubject<FormularioPadre[]>([]);
  public tModulosActuales$: Observable<FormularioPadre[]> = this.tModulosActuales.asObservable();

  private tEmpresaActual = new BehaviorSubject<Empresas>(new Empresas());
  public tEmpresaActual$: Observable<Empresas> = this.tEmpresaActual.asObservable();

  private tPermisoAgregar: boolean = false;
  private tPermisoEditar: boolean = false;
  private tPermisoEliminar: boolean = false;
  private tTareasAdicionales: FormularioTareas[] = [];
  public tTokenJwt: string = ""

  constructor() {

    this.tUsuario = new Usuario();

    let tHayUsuariotorage: string = window.sessionStorage.getItem("HayUsuario") || "";
    let tExisteUsuario: string = "N";

    let tUsuarioStorage: string = window.sessionStorage.getItem("UsuarioActual") || "";
    let tUsuarioTemp: Usuario = new Usuario();

    let tEmpresaStorage: string = window.sessionStorage.getItem("EmpresaActual") || "";
    let tEmpresaTemp: Empresas = new Empresas();

    let tModulosStorage: string = window.sessionStorage.getItem("ModulosActual") || "";
    let tModulosTemp: FormularioPadre[] = [];

    if (tHayUsuariotorage !== "") {

      let tExisteUsuarioDec: string = CryptoJS.AES
        .decrypt(tHayUsuariotorage, environment.ClaveSecreta)
        .toString(CryptoJS.enc.Utf8);

      tExisteUsuario = tExisteUsuarioDec;

    }

    if (tUsuarioStorage !== "") {

      let tUsuarioDec: string = CryptoJS.AES
        .decrypt(tUsuarioStorage, environment.ClaveSecreta)
        .toString(CryptoJS.enc.Utf8);

      tUsuarioTemp = JSON.parse(tUsuarioDec);

    }

    if (tEmpresaStorage !== "") {

      let tEmpresaDec: string = CryptoJS.AES
        .decrypt(tEmpresaStorage, environment.ClaveSecreta)
        .toString(CryptoJS.enc.Utf8);

      tEmpresaTemp = JSON.parse(tEmpresaDec);

    }

    if (tModulosStorage !== "") {

      let tModulosDec: string = CryptoJS.AES
        .decrypt(tModulosStorage, environment.ClaveSecreta)
        .toString(CryptoJS.enc.Utf8);

      tModulosTemp = JSON.parse(tModulosDec);

    }

    if (tExisteUsuario === "S") {

      this.tUsuario = tUsuarioTemp;
      this.tTokenJwt = this.tUsuario.TokenJwt;
      this.tHayUsuario.next(true);
      this.tUsuarioActual.next(tUsuarioTemp);
      this.tEmpresaActual.next(tEmpresaTemp);
      this.tModulosActuales.next(tModulosTemp);

    }

  }

  Login(tUsuarioLogin: Usuario, tEmpresaLogin: Empresas) {

    let tUsuarioActualEnc: string = CryptoJS.AES.encrypt(JSON.stringify(tUsuarioLogin), environment.ClaveSecreta).toString();
    let tHayUsuarioEnc: string = CryptoJS.AES.encrypt("S", environment.ClaveSecreta).toString();
    let tModulosActualEnc: string = CryptoJS.AES.encrypt(JSON.stringify(tUsuarioLogin.LModulos), environment.ClaveSecreta).toString();

    window.sessionStorage.setItem("UsuarioActual", tUsuarioActualEnc);
    window.sessionStorage.setItem("ModulosActual", tModulosActualEnc);
    window.sessionStorage.setItem("HayUsuario", tHayUsuarioEnc);

    this.tUsuario = tUsuarioLogin;
    this.tTokenJwt = this.tUsuario.TokenJwt;
    this.tHayUsuario.next(true);
    this.tUsuarioActual.next(tUsuarioLogin);
    this.tModulosActuales.next(tUsuarioLogin.LModulos);
    this.ActualizarEmpresa(tEmpresaLogin);

  }

  Signout() {

    let tUsuarioActualEnc: string = CryptoJS.AES.encrypt(JSON.stringify(new Usuario()), environment.ClaveSecreta).toString();
    let tHayUsuarioEnc: string = CryptoJS.AES.encrypt("N", environment.ClaveSecreta).toString();

    window.sessionStorage.setItem("UsuarioActual", tUsuarioActualEnc);
    window.sessionStorage.setItem("HayUsuario", tHayUsuarioEnc);

    this.tUsuario = new Usuario();
    this.tTokenJwt = "";
    this.tHayUsuario.next(false);
    this.tUsuarioActual.next(new Usuario());
    this.tModulosActuales.next([]);
    this.ActualizarEmpresa(new Empresas());

  }

  ActualizarEmpresa(tEmpresaUsuario: Empresas) {

    let tEmpresaActualEnc: string = CryptoJS.AES.encrypt(JSON.stringify(tEmpresaUsuario), environment.ClaveSecreta).toString();

    window.sessionStorage.setItem("EmpresaActual", tEmpresaActualEnc);
    this.tEmpresaActual.next(tEmpresaUsuario);

  }

  ObtenerPermisoAcceso(tCodformularioActual: string): boolean {

    let tTienePermiso: boolean = false;
    this.tTareasAdicionales = [];

    for (let tModulos of this.tUsuario.LModulos) {

      for (let tFormulario of tModulos.LFormularios) {

        if (tFormulario.Codformulario === tCodformularioActual) {

          tTienePermiso = true;

          if (tFormulario.Agregar == 1) {

            this.tPermisoAgregar = true;

          }

          if (tFormulario.Editar == 1) {

            this.tPermisoEditar = true;

          }

          if (tFormulario.Eliminar == 1) {

            this.tPermisoEliminar = true;

          }

          this.tTareasAdicionales = tFormulario.LTareas || [];

          break;

        }

      }

    }

    return tTienePermiso;

  }

  public ObtenerPermisoAgregar(): boolean {

    return this.tPermisoAgregar;

  }

  public ObtenerPermisoEditar(): boolean {

    return this.tPermisoEditar;

  }

  public ObtenerPermisoEliminar(): boolean {

    return this.tPermisoEliminar;

  }

  public ObtenerTareaAdicional(tCodtarea: string): boolean {

    let tPermisoTarea: boolean = false;
    let tPosicion: number = this.tTareasAdicionales.findIndex(tTarea => tTarea.Codtarea.toLowerCase() === tCodtarea.toLowerCase());

    if (tPosicion >= 0) {
      tPermisoTarea = true;
    }

    return tPermisoTarea;

  }

  public VerificarLicencia(tEmpresa: Empresas): any {

    let tRespuesta = {
      Resultado: true,
      Mensaje: ""
    };

    if (tEmpresa.Licencia === '') {

      tRespuesta.Mensaje = `La empresa ${tEmpresa.Descripcion} no tiene registrada una licencia.`;
      tRespuesta.Resultado = false;

    }
    else {

      let tLicenciaTemp: Licencia = new Licencia();
      let tLicenciaDec: string = CryptoJS.AES
        .decrypt(tEmpresa.Licencia, environment.ClaveSecreta)
        .toString(CryptoJS.enc.Utf8);

      tLicenciaTemp = JSON.parse(tLicenciaDec) || new Licencia();

      if (tEmpresa.Fecha_licencial > tLicenciaTemp.Fechal) {

        tRespuesta.Mensaje = `La empresa ${tEmpresa.Descripcion} tiene la licencia vencida.`;
        tRespuesta.Resultado = false;

      }

    }

    return tRespuesta;

  }

}
