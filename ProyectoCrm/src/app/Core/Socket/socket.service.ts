import { Injectable } from '@angular/core';
import io from 'socket.io-client';
import { HttpClient } from '@angular/common/http';
import { Configuracion } from 'src/app/Clases/EstructuraConfiguracion';
import { Observable, BehaviorSubject } from 'rxjs';
import { SessionService } from '../Session/session.service';
import { Empresas, Usuario } from 'src/app/Clases/Estructura';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SocketService {

  private tHayConfig = new BehaviorSubject<boolean>(false);
  public tHayConfig$: Observable<boolean> = this.tHayConfig.asObservable();

  private tSocket: any;
  private tUrl: string = "";
  // private tUrl: string = "http://192.168.4.10:3000";
  // private tUrl: string = "http://192.168.0.9:3000";
  // private tUrl: string = "http://50.63.14.104:3000";
  private tConfiguracion: Configuracion = new Configuracion();
  private tUsuarioActual: Usuario = new Usuario();
  private tEmpresaActual: Empresas = new Empresas();

  constructor(private http: HttpClient,
    private tSession: SessionService) {

    this.tSession.tUsuarioActual$.subscribe((Usuario: Usuario) => {

      this.tUsuarioActual = Usuario;

    });

    this.tSession.tEmpresaActual$.subscribe((Empresa: Empresas) => {

      this.tEmpresaActual = Empresa;

    });

    this.tHayConfig.next(false);

    http.get<Configuracion>('Configuracion/' + environment.ArchivoConfig)
      .subscribe(data => {

        this.tConfiguracion = data;
        this.tUrl = this.tConfiguracion.UrlSocket;
        // this.tSocket = io(this.tUrl);

        this.tSocket = io(this.tUrl, {
          reconnection: false,
          query: 'Corporacion=' + this.tEmpresaActual.Corporacion
            + '&Empresa=' + this.tEmpresaActual.Empresa
            + '&Usuario=' + this.tUsuarioActual.Codusuario
        });

        this.tHayConfig.next(true);

      });

  }

  join(tCanal: string) {

    this.tSocket.join(tCanal);

  }

  listen(tEvento: string) {

    return new Observable((suscribe) => {

      this.tSocket.on(tEvento, (data) => {

        suscribe.next(data);

      })

    });

  }

  emit(tEvento: string, data?: any) {

    this.tSocket.emit(tEvento, data);

  }

  disconnect(){

    this.tSocket.emit("disconnect");

  }

}
