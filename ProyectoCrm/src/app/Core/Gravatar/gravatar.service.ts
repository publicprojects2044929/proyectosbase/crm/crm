import { Injectable } from '@angular/core';
import md5 from 'md5';

@Injectable({
  providedIn: 'root'
})
export class GravatarService {

  constructor() { }

  obtenerGravatar(correo: string): string{

    const urlBase = 'http://www.gravatar.com/avatar/';
    const correoFormat = correo.trim().toLowerCase();
    const hash = md5(correoFormat, {
      encoding: "binary"
    });

    return `${urlBase}${hash}?f=y&d=identicon`;

  }

}
