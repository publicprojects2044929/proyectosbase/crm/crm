import { Injectable } from '@angular/core';
import { Chart } from 'chart.js';
import { Linea } from 'src/app/Clases/EstructuraGrafico';

@Injectable({
  providedIn: 'root'
})
export class LineaService {

  private tChart;

  constructor() { }

  ArmarGrafico(tElemento: any, tDatosLinea: Linea){

    this.tChart = new Chart(tElemento, {
      type: 'line',
      data: {
        labels: tDatosLinea.Etiquetas,
        datasets: tDatosLinea.DataSet.map(tDato => {
          return{
            label: tDato.Etiqueta,
            data: tDato.Datos,
            borderColor: tDato.Color
          }
        })
      }
    });

  }

}
