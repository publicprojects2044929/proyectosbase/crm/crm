import { Injectable } from '@angular/core';
import { Chart } from 'chart.js';
import { Pie } from 'src/app/Clases/EstructuraGrafico';
import { ManejarColores } from 'src/app/Manejadores/cls-procedimientos';

@Injectable({
  providedIn: 'root'
})
export class PieService {

  private tManejarColores: ManejarColores = new ManejarColores();

  constructor() { }

  ArmarGrafico(tElemento: any, tDatosPie: Pie) {

    var myPieChart = new Chart(tElemento, {
      type: 'pie',
      data: {
        labels: tDatosPie.Etiquetas,
        datasets: [{
          data: tDatosPie.DataSet.map(tDato => tDato.Datos),
          backgroundColor: tDatosPie.DataSet.map(tDato => {
            return this.tManejarColores.hexToRGB(tDato.Color, 0.7);
          }),
          hoverBackgroundColor: tDatosPie.DataSet.map(tDato => tDato.Color),
          hoverBorderColor: tDatosPie.DataSet.map(tDato => tDato.Color),
          borderWidth: 2
        }]
      },
      options: {
        maintainAspectRatio: false,
        tooltips: {
          backgroundColor: "rgb(255,255,255)",
          bodyFontColor: "#858796",
          borderColor: '#dddfeb',
          borderWidth: 1,
          xPadding: 15,
          yPadding: 15,
          displayColors: false,
          caretPadding: 10,
        },
        legend: {
          display: false
        },
        cutoutPercentage: 80,
      },
    });

  }

}
