import { Injectable } from '@angular/core';
import { Chart } from 'chart.js';
import { Barra } from 'src/app/Clases/EstructuraGrafico';
import { ManejarColores } from 'src/app/Manejadores/cls-procedimientos';

@Injectable({
  providedIn: 'root'
})
export class BarrasService {

  private tManejarColores: ManejarColores = new ManejarColores();
  private tEtiqueta: string = "";
  private tSimbolo: string = "";
  private tAspectoRatio: boolean = false;
  private tMostrarLeyenda: boolean = false;

  constructor() { }

  set Etiqueta(Eti: string) {

    this.tEtiqueta = Eti;

  }

  set Simbolo(simb: string) {

    this.tSimbolo = simb;

  }

  set AspectoRatio(ratio: boolean) {

    this.tAspectoRatio = ratio;

  }

  set MostrarLeyenda(ley: boolean) {

    this.tMostrarLeyenda = ley;

  }

  ArmarGrafico(tElemento: any, tDatosBarra: Barra) {

    var myBarChart = new Chart(tElemento, {
      type: 'bar',
      data: {
        labels: tDatosBarra.Etiquetas,
        datasets: [
          {
            label: "",
            data: tDatosBarra.DataSet.map(tDato => tDato.Datos),
            backgroundColor: tDatosBarra.DataSet.map(tDato => { return this.tManejarColores.hexToRGB(tDato.Color, 0.7) }),
            hoverBackgroundColor: tDatosBarra.DataSet.map(tDato => tDato.Color),
            borderColor: tDatosBarra.DataSet.map(tDato => tDato.Color),
            borderWidth: 2,
            fill: false,
          }
        ],
        // datasets: tDatosBarra.DataSet.map(tDato => {
        //   return {
        //     label: tDato.Etiqueta,
        //     backgroundColor: this.hexToRGB(tDato.Color, 0.7),
        //     hoverBackgroundColor: tDato.Color,
        //     borderColor: tDato.Color,
        //     borderWidth: 2,
        //     fill: false,
        //     data: tDato.Datos
        //   }
        // }),
      },
      options: {
        maintainAspectRatio: this.tAspectoRatio,
        layout: {
          padding: {
            left: 10,
            right: 25,
            top: 25,
            bottom: 0
          }
        },
        scales: {
          xAxes: [{
            gridLines: {
              display: false,
              drawBorder: false
            }
          }],
        },
        legend: {
          display: this.tMostrarLeyenda
        },
        tooltips: {
          titleMarginBottom: 10,
          titleFontColor: '#6e707e',
          titleFontSize: 14,
          backgroundColor: "rgb(255,255,255)",
          bodyFontColor: "#858796",
          borderColor: '#dddfeb',
          borderWidth: 1,
          xPadding: 15,
          yPadding: 15,
          displayColors: false,
          caretPadding: 10,
          callbacks: {
            label: function (tooltipItem, chart) {
              var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
              return `${datasetLabel} ${tooltipItem.yLabel}`;
            }
          }
        },
      }
    });

  }

}
