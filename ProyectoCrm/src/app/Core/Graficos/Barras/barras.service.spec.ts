import { TestBed } from '@angular/core/testing';

import { BarrasService } from './barras.service';

describe('BarrasService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BarrasService = TestBed.get(BarrasService);
    expect(service).toBeTruthy();
  });
});
