import { Injectable } from '@angular/core';
import { Query } from 'apollo-angular';
import gql from 'graphql-tag';
import { mensajesResponse, totalesResponse } from './types'

@Injectable({
  providedIn: 'root'
})
export class getMensajes extends Query<mensajesResponse>{

  document = gql`
    query{
          getMensajes{
            _id,
            Corporacion,
            Empresa,
            Codticket,
            De,
            Texto,
            Fecha,
            Fechal
          }
        }
  `;

}

@Injectable({
  providedIn: 'root'
})
export class getMensajesByCorporacion extends Query<mensajesResponse>{

  document = gql`
    query getMensajesByCorporacion($Corporacion: String!){
      getMensajesByCorporacion(Corporacion: $Corporacion){
        _id
        Corporacion
        Empresa
        Codticket
        De
        Texto
        Fecha
        Fechal
      }
    }
  `;

}

@Injectable({
  providedIn: 'root'
})
export class getMensajesByCorporacionAndEmpresa extends Query<mensajesResponse>{

  document = gql`
    query getMensajesByCorporacionAndEmpresa($Corporacion: String!, $Empresa: String!){
      getMensajesByCorporacionEmpresa(Corporacion: $Corporacion, Empresa: $Empresa){
        _id
        Corporacion
        Empresa
        Codticket
        De
        Texto
        Fecha
        Fechal
      }
    }
  `;

}

@Injectable({
  providedIn: 'root'
})
export class getMensajesByCorporacionAndEmpresaAndCodticket extends Query<mensajesResponse>{

  document = gql`
    query getMensajesByCorporacionEmpresaCodticket($Corporacion: String!, $Empresa: String!, $Codticket: String!){
      getMensajesByCorporacionEmpresaCodticket(Corporacion: $Corporacion, Empresa: $Empresa, Codticket: $Codticket){
        _id
        Corporacion
        Empresa
        Codticket
        De
        Texto
        Fecha
        Fechal
      }
    }
  `;

}

@Injectable({
  providedIn: 'root'
})
export class getTicketTotales extends Query<totalesResponse>{

  document = gql`
    query getTicketTotales($Corporacion: String!, $Empresa: String!, $Codticket: [String]!){
      getTicketTotales(Corporacion: $Corporacion, Empresa: $Empresa, Codticket: $Codticket){
        Corporacion
        Empresa
        Codticket
        TotalMensajes
      }
    }
  `;

}
