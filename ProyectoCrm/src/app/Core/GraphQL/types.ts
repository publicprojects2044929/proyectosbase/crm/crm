import { TicketMensajes } from 'src/app/Clases/Estructura';

export interface mensajesResponse {
  getMensajesByCorporacionEmpresaCodticket: TicketMensajes[];
}

export interface totalesResponse {
  getTicketTotales: any;
}
