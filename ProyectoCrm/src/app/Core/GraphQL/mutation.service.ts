import { Injectable } from '@angular/core';
import { Mutation } from 'apollo-angular';
import gql from 'graphql-tag';

@Injectable({
  providedIn: 'root'
})
export class insertMensaje extends Mutation {
  document = gql`
    mutation insertMensaje($Mensaje: MensajeInput!) {
      insertMensaje(input: $Mensaje) {
        _id
        Codticket
        De
        Texto
      }
    }`;
}
