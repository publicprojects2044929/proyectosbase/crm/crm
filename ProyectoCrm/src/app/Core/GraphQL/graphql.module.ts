import { NgModule } from '@angular/core';
import { ApolloModule, Apollo } from "apollo-angular";
import { HttpLinkModule, HttpLink } from "apollo-angular-link-http";
import { InMemoryCache } from "apollo-cache-inmemory";
import {
  getMensajes,
  getMensajesByCorporacion,
  getMensajesByCorporacionAndEmpresa,
  getMensajesByCorporacionAndEmpresaAndCodticket,
  getTicketTotales
} from './query.service';
import {
  insertMensaje
} from './mutation.service';
import { environment } from 'src/environments/environment';

@NgModule({
  declarations: [],
  imports: [
    ApolloModule,
    HttpLinkModule
  ],
  providers: [
    // {
    //   provide: APOLLO_OPTIONS,
    //   useFactory: (httpLink: HttpLink) => {
    //     return {
    //       cache: new InMemoryCache(),
    //       link: httpLink.create({
    //         uri: environment.urlGraphQl
    //       }),
    //       defaultOptions: {
    //         watchQuery: {
    //             fetchPolicy: 'cache-and-network',
    //             errorPolicy: 'ignore',
    //         },
    //         query: {
    //             fetchPolicy: 'network-only',
    //             errorPolicy: 'all',
    //         },
    //         mutate: {
    //             errorPolicy: 'all'
    //         }
    //     }
    //   },
    //   deps: [HttpLink]
    // },
    getMensajes,
    getMensajesByCorporacion,
    getMensajesByCorporacionAndEmpresa,
    getMensajesByCorporacionAndEmpresaAndCodticket,
    getTicketTotales,
    insertMensaje
  ]
})
export class GraphqlModule {
  constructor(apollo: Apollo, httpLink: HttpLink) {

    const http = httpLink.create({ uri: environment.urlGraphQl });

    // const authMiddleware = new ApolloLink((operation, forward) => {
    //   // add the authorization to the headers
    //   operation.setContext({
    //     headers: new HttpHeaders().set('Tipo', 'graphql')
    //   });

    //   return forward(operation);
    // });

    apollo.create({
      link: http,
      cache: new InMemoryCache(),
      defaultOptions: {
        watchQuery: {
          fetchPolicy: 'network-only'
        },
        query: {
          fetchPolicy: 'network-only'
        },
        mutate: {
          errorPolicy: 'all'
        }
      }
    });
  }
}
