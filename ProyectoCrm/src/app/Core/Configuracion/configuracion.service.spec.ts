import { TestBed, getTestBed } from '@angular/core/testing';
import { ConfiguracionService } from './configuracion.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'
import { FormularioConfiguracion } from 'src/app/Clases/EstructuraConfiguracion';

fdescribe('Prueba de servicios de configuración', () => {

  let tServiceConfig: ConfiguracionService;
  let injector: TestBed;
  let httpMock: HttpTestingController;

  beforeEach(() => {

    TestBed.configureTestingModule({
      providers: [ConfiguracionService],
      imports: [HttpClientTestingModule]
    });

  });

  beforeEach(() => {

    injector = getTestBed();
    tServiceConfig = TestBed.get(ConfiguracionService);
    httpMock = injector.get(HttpTestingController);

  });

  afterAll(() => {

    injector = null;
    tServiceConfig = null;
    httpMock = null;

  });

  it('Creación del  servicio', () => {

    expect(tServiceConfig).toBeTruthy();

  });

  describe('Haciendo Get de la configuración', () => {

    it('Ejecutando el get de la configuración', () => {

      const tArchivo: string = "";
      const resultado: FormularioConfiguracion = new FormularioConfiguracion();

      tServiceConfig.Consumir_Obtener_Configuracion(tArchivo).subscribe(respuesta => {

        expect(respuesta).toBe(resultado);

      });

      const req = httpMock.expectOne(tServiceConfig.tUrl + tArchivo);
      expect(req.request.method).toBe('GET');

      req.flush(resultado);

    });

  });

});
