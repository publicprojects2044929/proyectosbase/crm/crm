import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormularioConfiguracion } from 'src/app/Clases/EstructuraConfiguracion';

@Injectable({
  providedIn: 'root'
})
export class ConfiguracionService {

  tUrl: string = "Configuracion/";

  constructor(private http: HttpClient) {

  }

  Consumir_Obtener_Configuracion(tFormulario: string) {

    var tUrlFinal = this.tUrl + tFormulario;

    return this.http.get<FormularioConfiguracion>(tUrlFinal);

  }

}
