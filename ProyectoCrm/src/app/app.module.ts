import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgHttpLoaderModule } from 'ng-http-loader';
import { ToastrModule } from 'ngx-toastr';
import { CoreModule } from 'src/app/Core/core.module';

//Modal
import { MensajesModule } from 'src/app/Modales/mensajes/mensajes.module'
import { ConfirmacionModule } from 'src/app/Modales/confirmacion/confirmacion.module'
import { PermisosModalModule } from 'src/app/Modales/permisos/permisos.module'
import { PaisModalModule } from 'src/app/Modales/pais/pais.module'
import { EstadoModalModule } from './Modales/estado/estado.module';
import { MunicipioModalModule } from './Modales/municipio/municipio.module';
import { ArticuloModalModule } from './Modales/articulo/articulo.module';
import { AlmacenModalModule } from './Modales/almacen/almacen.module';
import { Clasificacion1ModalModule } from './Modales/clasificacion1/clasificacion1.module';
import { Clasificacion2ModalModule } from './Modales/clasificacion2/clasificacion2.module';
import { Clasificacion3ModalModule } from './Modales/clasificacion3/clasificacion3.module';
import { Clasificacion4ModalModule } from './Modales/clasificacion4/clasificacion4.module';
import { ClienteModalModule } from './Modales/cliente/cliente.module';
import { ColorModalModule } from './Modales/color/color.module';
import { FabricanteModalModule } from './Modales/fabricante/fabricante.module';
import { MarcaModalModule } from './Modales/marca/marca.module';
import { ModeloModalModule } from './Modales/modelo/modelo.module';
import { UnidadMedidaModalModule } from './Modales/unidad-medida/unidad-medida.module';
import { UsuarioModalModule } from './Modales/usuario/usuario.module';
import { InvitadoModalModule } from './Modales/invitado/invitado.module';
import { AjusteInventarioModalModule } from './Modales/AjusteInventario/ajuste-inventario.module';
import { AjusteInventarioPorArchivoModalModule } from './Modales/AjusteInventarioPorArchivo/ajuste-inventario-por-archivo.module';
import { MonedaModalModule } from './Modales/moneda/moneda.module';
import { PedidoArticulosModalModule } from './Modales/PedidoArticulos/pedido-articulos.module';
import { CargarArticuloModalModule } from './Modales/cargar-articulos/cargar-articulos.module';
import { DetallesPedidoModalModule } from './Modales/detalles-pedido/detalles-pedido.module';
import { DetallesVentaModalModule} from './Modales/detalles-venta/detalles-venta.module';
import { PedidoColeccionesModalModule } from './Modales/PedidoColecciones/pedido-colecciones.module';
import { DetallesColeccionModule } from './Modales/DetallesColeccion/detalles-coleccion.module'
import { FacturacionModalModule } from './Modales/facturacion/facturacion.module';
import { CiudadModalModule } from './Modales/ciudad/ciudad.module';
import { FichaClienteModalModule } from './Modales/ficha-cliente/fichacliente.module';
import { ColeccionesModalModule } from './Modales/colecciones/colecciones.module';
import { ConfirmacionRechazoPagoModule } from 'src/app/Modales/confirmacion-rechazo-pago/confirmacion-rechazo-pago.module';
import { GruposModalModule } from 'src/app/Modales/grupos/grupos.module';
import { TicketMensajesModule } from 'src/app/Modales/ticket-mensajes/ticket-mensajes.module';
import { ClsServiciosInterceptor } from './Core/Servicios/cls-servicios.interceptor';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    NgHttpLoaderModule.forRoot(),
    CoreModule,
    ToastrModule.forRoot(),
    NgbModule
  ],
  exports: [
    MensajesModule,
    ConfirmacionModule,
    PermisosModalModule,
    PaisModalModule,
    EstadoModalModule,
    MunicipioModalModule,
    ArticuloModalModule,
    AlmacenModalModule,
    Clasificacion1ModalModule,
    Clasificacion2ModalModule,
    Clasificacion3ModalModule,
    Clasificacion4ModalModule,
    ClienteModalModule,
    ColorModalModule,
    FabricanteModalModule,
    MarcaModalModule,
    ModeloModalModule,
    UnidadMedidaModalModule,
    UsuarioModalModule,
    InvitadoModalModule,
    AjusteInventarioModalModule,
    AjusteInventarioPorArchivoModalModule,
    MonedaModalModule,
    PedidoArticulosModalModule,
    CargarArticuloModalModule,
    DetallesPedidoModalModule,
    DetallesVentaModalModule,
    PedidoColeccionesModalModule,
    DetallesColeccionModule,
    FacturacionModalModule,
    CiudadModalModule,
    FichaClienteModalModule,
    ColeccionesModalModule,
    ConfirmacionRechazoPagoModule,
    GruposModalModule,
    TicketMensajesModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ClsServiciosInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
