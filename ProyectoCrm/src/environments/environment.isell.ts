// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  api_key_maps: "AIzaSyDzVNYxkp7zbldvQtiry0nAq-K4qTWg1cY",
  msjError: "Hay un problema de conexión con el servidor. Por favor contacte con sistemas.",
  msjSinAcceso: "Discúlpe, no tiene permisos para acceder a este modulo.",
  ClaveSecreta: "jmvs21297617",
  ArchivoConfig: "config.isell.json",
  urlGraphQl: "http://localhost:3000/api",
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
